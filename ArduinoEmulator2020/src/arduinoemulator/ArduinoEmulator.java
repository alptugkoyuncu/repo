/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arduinoemulator;

import arduinoemulator.pt1000.TempEvent;
import arduinoemulator.pt1000.Ads1248Part;
import arduinoemulator.pt1000.TemperatureListener;
import arduinoemulator.record.RecordSet;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import data.ActionCode;
import static data.ActionCode.*;
import data.ClientPackage;
import data.GeneralSystemConstant;
import data.NetworkPackage;
import data.ServerPackage;
import java.io.DataInputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ISIK
 */
public class ArduinoEmulator implements Runnable, TemperatureListener, SerialPortDataListener {

    private SerialPort serial;
    private RecordSet recs;
    private boolean emulatorRunning;
    private boolean terminated;
    private PrintStream ps;
    private Ads1248Part tc;
    private Set<ArduinoListener> arduinoListener;
    private NetworkPackage clientPackage,serverPackage;
    

    public RecordSet getRecs() {
        return recs;
    }

    public void setRecs(RecordSet recs) {
        this.recs = recs;
    }

    public SerialPort getSerial() {
        return serial;
    }
    public void removeSerial(){
        if(this.serial==null)
            return;
        this.serial.closePort();
        while(serial.isOpen()){
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(ArduinoEmulator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.ps.close();
        this.ps=null;
        this.serial.removeDataListener();
        this.serial=null;
    }
    public void setSerial(SerialPort newSerial) {
        if(this.serial==newSerial)
            return;
        removeSerial();
        if(newSerial==null)
            return;
        this.serial = newSerial;
        this.serial.addDataListener(this);
        this.serial.openPort();
        ps = new PrintStream(serial.getOutputStream());
    }

    public Ads1248Part getTc() {
        return tc;
    }
    public void removeTc(){
        if(this.tc==null){
            return;
        }
        this.tc.stop();
        while(this.tc.isAlive()){
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(ArduinoEmulator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.tc=null;
    }
    public void setTc(Ads1248Part newTc) {
        if(newTc==this.tc)
            return;
        removeTc();
        if(newTc==null)
            return;
        this.tc = newTc;
        this.tc.addTemperatureListener(this);
        new Thread(tc).start();
    }

    public ArduinoEmulator() {
        recs = new RecordSet();
        arduinoListener = new HashSet<>();
        clientPackage = new ClientPackage();
    }

    public void addArduinoListener(ArduinoListener l) {
        this.arduinoListener.add(l);
    }

    public void removeArduinoListener(ArduinoListener l) {
        this.arduinoListener.remove(l);
    }

    private void fireArduinoEvent(int type, NetworkPackage pck) {
        ArduinoEvent ev = new ArduinoEvent(pck, type);
        for (ArduinoListener l : arduinoListener) {
            l.arduinoEventFired(ev);
        }
    }
    public void startEmulator(){
        this.terminated=false;
        new Thread(this).start();
    }
    public synchronized void stopEmulator() {
        this.removeSerial();
        this.removeTc();
        this.emulatorRunning = false;
        while (!terminated) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(ArduinoEmulator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public synchronized boolean isEmulatorRunning() {
        return emulatorRunning;
    }

    private void arduinoSetup() {
        System.out.println("Setup begins");
        emulatorRunning = true;
        System.out.println("setup ends");

    }

    public synchronized void sendPacket(ServerPackage np) {
        if (serial == null || !serial.isOpen() || ps == null ||np==null || !np.isFinished()) {
            return;
        }
        byte[] b=np.getMessageInByteFormat();
        if(b.length==0)
            return;
        ps.write(b, 0, b.length);
        ps.flush();
        System.out.println("Data sent : "+np.getMessage());
        fireArduinoEvent(ArduinoEvent.DATA_SENT, np);
    }

    private ServerPackage getActions() {
        ServerPackage respond = new ServerPackage();
        int actCode = clientPackage.getActionCode();
        switch (actCode) {
            case ActionCode.AC_ESP_STARTED:
                System.out.println("Esp started");
                return null;
            case ActionCode.AC_ID_ACCEPTED:
                System.out.println("id accepted");
                return null;
            case ActionCode.AC_BOARD_FREE_MEMORY:
                return null;
            case ActionCode.AC_ID_REFUSED:
                System.out.println("id refused");
                return null;
            case ActionCode.AC_ID_REQUESTED: {
                respond.setTargetId(GeneralSystemConstant.SERVER_ID);
                respond.setActionCode(ActionCode.AC_SEND_ID);
                respond.addData(recs.getId() + "");
                respond.finishPacket();
                return respond;
            }
            case ActionCode.AC_RESET_ESP:
                System.err.println("esp reset");
                return null;
            case ActionCode.AC_SEND_BOARD_LAST_RESET_TIME:
                System.out.println("board last reset time");
                return null;
            case ActionCode.AC_SEND_ID:
                System.out.println("send id");
                return null;
            case ActionCode.AC_SEND_IP:
                respond.setTargetId(GeneralSystemConstant.ESP_ID);
                respond.setActionCode(AC_SEND_IP);
                respond.addData(recs.getIp());
                respond.finishPacket();
                return respond;
            case AC_SEND_PASS:
                respond.setTargetId(GeneralSystemConstant.ESP_ID);
                respond.setActionCode(AC_SEND_PASS);
                respond.addData(recs.getPassword());
                respond.finishPacket();
                return respond;
            case AC_SEND_PORT: {
                respond.setTargetId(GeneralSystemConstant.ESP_ID);
                respond.setActionCode(AC_SEND_PORT);
                respond.addData(recs.getPort() + "");
                respond.finishPacket();
                return respond;
            }
            case AC_SEND_WIFI_SSID: {
                respond.setTargetId(GeneralSystemConstant.ESP_ID);
                respond.setActionCode(AC_SEND_WIFI_SSID);
                respond.addData(recs.getWifi_ssid());
                respond.finishPacket();
                return respond;
            }
            case AC_REQUEST_PING: {
                respond.setTargetId(GeneralSystemConstant.SERVER_ID);
                respond.setActionCode(AC_REQUEST_PING);
                respond.finishPacket();
                return respond;
            }
            case AC_SEND_TEMP0:
                System.out.println("send temp0");
                break;
            case AC_SEND_TEMP1:
                System.out.println("send temp1");
                break;
            case AC_SEND_TEMP2:
                System.out.println("send temp2");
                break;
            case AC_SEND_TEMP3:
                System.out.println("send temp3");
                break;
            case AC_SERVER_CONNECTED:
                System.out.println("Server connection established");
                break;
            case AC_SERVER_CONNECTION_FAILED:
                System.out.println("Server connection failed");
                break;
            case AC_SERVER_DISCONNECTED:
                System.out.println("Server connection disconnected");
                break;
            case AC_WIFI_CONNECTED:
                System.out.println("wifi connected");
                break;
            case AC_WIFI_CONNECTION_FAILED:
                System.out.println("wifi connection failed");
                break;
            case AC_WIFI_DISCONNECTED:
                System.out.println("wifi disconnected");
                break;
            case AC_CLIENT_CONNECTED:
                System.out.println("client connected");
                break;
            case AC_CLIENT_DISCONNECTED:
                System.out.println("client disconnected");
                break;
            case AC_REQUEST_FOCUED_PROVIDERS:
                System.out.println("request focus providers");
                break;
            case AC_ESP_DEBUG:
                System.out.println("ESP DEBUG : " + clientPackage.getMessage());
                break;
            case AC_INVALID_CODE:
            default:
                System.out.println("Action code kayitli degil : Ac=" + actCode);
                return null;
        }
        return null;
    }

    private void arduinoLoop() {
        byte[] buffer = new byte[250];
        try {
            while (serial != null && serial.isOpen() && serial.bytesAvailable() > 0) {
                int s = serial.readBytes(buffer, serial.bytesAvailable());
                for (int i = 0; i < s; i++) {
                    clientPackage.addByte(buffer[i]);
                    if (clientPackage.isFinished()) {
                        fireArduinoEvent(ArduinoEvent.DATA_RECEIVED, clientPackage);
                        NetworkPackage respond = getActions();
                        if (respond != null) {
                            this.sendPacket((ServerPackage) respond);
                            Thread.sleep(10);
                        }
//                        clientPackage.reset();
                        clientPackage = new ClientPackage();
                    }
                }
            }
            Thread.sleep(10);
        } catch (InterruptedException ex) {
            Logger.getLogger(ArduinoEmulator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ArduinoEmulator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {

        arduinoSetup();
        

        //loop kisimi
        while (isEmulatorRunning()) {
            arduinoLoop();
        }
        terminated=true;
    }

    @Override
    public void newTemparuteCreated(TempEvent ev) {
        NetworkPackage respond = new ServerPackage();
        System.out.println(String.format("%s%d = %.2f", "new Temp at ",ev.getTempIndex(),ev.getVal()));
        int actionCode;
        switch (ev.getTempIndex()) {
            case 0:
                actionCode = AC_SEND_TEMP0;
                break;
            case 1:
                actionCode = AC_SEND_TEMP1;
                break;
            case 2:
                actionCode = AC_SEND_TEMP2;
                break;
            case 3:
                actionCode = AC_SEND_TEMP3;
                break;
            default:
                actionCode = AC_INVALID_CODE;
                break;
        }
        respond.setActionCode(actionCode);
        respond.setTargetId(GeneralSystemConstant.SERVER_ID);
        respond.addData(String.format("%.2f", ev.getVal()));
        respond.finishPacket();
        this.sendPacket((ServerPackage) respond);
    }

    @Override
    public int getListeningEvents() {
        return SerialPort.LISTENING_EVENT_DATA_WRITTEN;
    }

    @Override
    public void serialEvent(SerialPortEvent spe) {
        if (spe.getEventType() == SerialPort.LISTENING_EVENT_DATA_WRITTEN) {
            System.out.println("all data sent");
        }

    }

}
