/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arduinoemulator;

import data.NetworkPackage;
import java.util.EventObject;

/**
 *
 * @author ISIK
 */
class ArduinoEvent extends EventObject{
    public final static int DATA_SENT=1;
    public final static int DATA_RECEIVED=2;
    private int eventType;
    public ArduinoEvent(NetworkPackage pck,int evenType) {
        super(pck);
        this.eventType=evenType;
    }

    public int getEventType() {
        return eventType;
    }
    @Override
    public NetworkPackage getSource() {
        return (NetworkPackage) super.getSource(); //To change body of generated methods, choose Tools | Templates.
    }

}
