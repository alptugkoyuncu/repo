/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arduinoemulator.pt1000;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ISIK
 */
public class Ads1248Part implements Runnable {

    Set<TemperatureListener> listener;
    private boolean running;
    private boolean alive;
    private int tempCount;

    public int getTempCount() {
        return tempCount;
    }

    public void setTempCount(int tempCount) {
        if (tempCount < 0) {
            this.tempCount = 1;
        } else {
            this.tempCount = tempCount + 1;//board sensor icin 1 
        }
    }

    private synchronized boolean isRunning() {
        return running;
    }

    private synchronized void setRunning(boolean running) {
        this.running = running;
    }

    public Ads1248Part() {
        listener = new HashSet<>();
        alive = false;
    }

    public void addTemperatureListener(TemperatureListener listener) {
        this.listener.add(listener);
    }

    public void removeTemperatureListener(TemperatureListener l) {
        this.listener.remove(l);
    }

    public void fireNewTemperatureCreatedEvent(int tempIndex, double val) {
        TempEvent evt = new TempEvent(this, tempIndex, val);
        for (TemperatureListener l : listener) {
            l.newTemparuteCreated(evt);
        }
    }

    public void stop() {
        this.listener.clear();
        this.setRunning(false);
    }

    @Override
    public void run() {
        alive = true;
        running=true;
        Double[] val = new Double[tempCount];
        for (int i = 0; i < tempCount; i++) {
            val[i] = 10.0;
        }
        int tempIndex = 0;
        while (this.isRunning()) {

            val[tempIndex] = getVal(val[tempIndex]);
            fireNewTemperatureCreatedEvent(tempIndex, val[tempIndex]);
            tempIndex++;
            tempIndex = tempIndex % tempCount;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Ads1248Part.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        alive = false;
    }

    public boolean isAlive() {
        return alive;
    }

    private double getVal(double val) {
        return val + Math.random() * 0.1 - 0.05;
    }
}
