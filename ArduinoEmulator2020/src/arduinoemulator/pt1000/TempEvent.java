/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arduinoemulator.pt1000;

import java.util.EventObject;

/**
 *
 * @author ISIK
 */
public class TempEvent extends EventObject{
    double val;
    int tempIndex;

    public TempEvent(Ads1248Part source, int tempIndex,double val ) {
        super(source);
        this.val = val;
        this.tempIndex = tempIndex;
    }

    public double getVal() {
        return val;
    }

    public int getTempIndex() {
        return tempIndex;
    }

    @Override
    public Ads1248Part getSource() {
        return (Ads1248Part) super.getSource(); //To change body of generated methods, choose Tools | Templates.
    }
}
