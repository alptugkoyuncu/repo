/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arduinoemulator.record;

/**
 *
 * @author ISIK
 */
public class RecordSet {
    private String wifi_ssid;
    private String password;
    private String ip;
    private int port;
    private int id;


    public RecordSet() {
        this.setId(1001);
        this.setIp("192.168.1.11");
        this.setPort(5555);
        this.setPassword("5458798185");
        this.setWifi_ssid("KOYUNCU_CATI");
    }
    public String getWifi_ssid() {
        return wifi_ssid;
    }

    public String getPassword() {
        return password;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public int getId() {
        return id;
    }

    public void setWifi_ssid(String wifi_ssid) {
        this.wifi_ssid = wifi_ssid;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
