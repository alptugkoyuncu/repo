/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca_calculator;

/**
 *
 * @author alptug
 */
public enum Attributes {
    Corners(1,"Corners",Attributes.TECHNICAL_ATTRIBUTE,false),
    Crossing(2,"Crossing",Attributes.TECHNICAL_ATTRIBUTE,false),
    Dribbling(3,"Dribbling",Attributes.TECHNICAL_ATTRIBUTE,false),
    Finishing(4,"Finising",Attributes.TECHNICAL_ATTRIBUTE,false),
    First_Touch(5,"First Touch",Attributes.TECHNICAL_ATTRIBUTE,false),
    Free_Kicks(6,"Free_Kicks",Attributes.TECHNICAL_ATTRIBUTE,false),
    Heading(7,"Heading",Attributes.TECHNICAL_ATTRIBUTE,false),
    Long_shots(8,"Long Shots",Attributes.TECHNICAL_ATTRIBUTE,false),
    Long_Throws(9,"Long Throws",Attributes.TECHNICAL_ATTRIBUTE,false),
    Marking(10,"Marking",Attributes.TECHNICAL_ATTRIBUTE,false),
    Passing(11,"Passing",Attributes.TECHNICAL_ATTRIBUTE,false),
    Penalty_taking(12,"Penalty Taking",Attributes.TECHNICAL_ATTRIBUTE,false),
    Tacling(13,"Tackling",Attributes.TECHNICAL_ATTRIBUTE,false),
    Technique(14,"Technique",Attributes.TECHNICAL_ATTRIBUTE,false),
    Aggression(15,"Aggression",Attributes.MENTAL_ATTRIBUTE,false),
    Anticipation(16,"Anticipation",Attributes.MENTAL_ATTRIBUTE,false),
    Bravery(17,"Bravery",Attributes.MENTAL_ATTRIBUTE,false),
    Composure(18,"Composure",Attributes.MENTAL_ATTRIBUTE,false),
    Concentration(19,"Concentration",Attributes.MENTAL_ATTRIBUTE,false),
    Decisions(20,"Decisions",Attributes.MENTAL_ATTRIBUTE,false),
    Determination(21,"Determination",Attributes.MENTAL_ATTRIBUTE,false),
    Flair(22,"Flair",Attributes.MENTAL_ATTRIBUTE,false),
    Leadership(23,"Leadership",Attributes.MENTAL_ATTRIBUTE,false),
    Off_The_Ball(24,"Off The Ball",Attributes.MENTAL_ATTRIBUTE,false),
    Positioning(25,"Positioning",Attributes.MENTAL_ATTRIBUTE,false),
    Teamwork(26,"Teamwork",Attributes.MENTAL_ATTRIBUTE,false),
    Vision(27,"Vision",Attributes.MENTAL_ATTRIBUTE,false),
    Work_rate(28,"Work Rate",Attributes.MENTAL_ATTRIBUTE,false),
    Acceleration(29,"Acceleration",Attributes.PHYSICAL_ATTRIBUTE,false),
    Agility(30,"Agility",Attributes.PHYSICAL_ATTRIBUTE,false),
    Balance(31,"Balance",Attributes.PHYSICAL_ATTRIBUTE,false),
    Jumping_reach(32,"Jumping Reach",Attributes.PHYSICAL_ATTRIBUTE,false),
    Natural_fitness(33,"Natural Fitness",Attributes.PHYSICAL_ATTRIBUTE,false),
    Pace(34,"Pace",Attributes.PHYSICAL_ATTRIBUTE,false),
    Stamina(35,"Stamina",Attributes.PHYSICAL_ATTRIBUTE,false),
    Strength(36,"Strength",Attributes.PHYSICAL_ATTRIBUTE,false);
    
    public final static byte TECHNICAL_ATTRIBUTE=0;
    public final static byte MENTAL_ATTRIBUTE=1;
    public final static byte PHYSICAL_ATTRIBUTE=2;
    private Attributes(int id,String title,byte type,boolean hidden){
        this.id=id;
        this.title=title;
        this.hidden=hidden;
        this.type=type;
    }
    private final int id;
    private final byte type;
    private final String title;
    private final boolean hidden;

    public int getId() {
        return id;
    }

    public byte getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public boolean isHidden() {
        return hidden;
    }
    
}
