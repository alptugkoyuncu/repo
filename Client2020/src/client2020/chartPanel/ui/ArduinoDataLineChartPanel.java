/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.chartPanel.ui;

import client2020.chartPanel.data.ArduinoDataHistory;
import client2020.chartPanel.data.ArduinoDataHistory.DataList;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EventObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JPanel;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.internal.chartpart.Chart;
import org.knowm.xchart.style.colors.ChartColor;
import org.knowm.xchart.style.colors.XChartSeriesColors;
import org.knowm.xchart.style.lines.SeriesLines;
import org.knowm.xchart.style.markers.SeriesMarkers;
import socket.events.ClientDataEvent;
import socket.events.ClientSocketListener;

/**
 *
 * @author ISIK
 */
public class ArduinoDataLineChartPanel extends XChartPanel<XYChart> implements ClientSocketListener{

    ArduinoDataHistory arduinoData;
    JPanel chartPanel;
    double min, max;
    private final static Color[] colorList = {Color.BLACK, Color.BLUE, Color.GREEN, Color.RED, Color.YELLOW};

    public ArduinoDataLineChartPanel(ArduinoDataHistory data) {
       super(createChart());
       this.arduinoData=data;
       XYChart chart=this.getChart();
        for(Integer actionCode : arduinoData.getActionCodeList()){
            DataList dl = arduinoData.getDataList(actionCode);
            XYSeries series = chart.addSeries(actionCode+"", dl.getDateArray(),dl.getValueArray());
            if(min>series.getYMin()+5)
                min = series.getYMin();
            if(max<series.getYMax()-5)
                max=series.getYMax();
            series.setLineColor(getColor(actionCode));
//            series.setMarkerColor(Color.ORANGE);
            series.setMarker(SeriesMarkers.NONE);
            series.setLineStyle(SeriesLines.SOLID);
        }
        chart.getStyler().setYAxisMin(min-5);
        chart.getStyler().setYAxisMax(max+5);
    }

    public JPanel getChartPanel() {
        return chartPanel;
    }

    private static XYChart createChart() {

        // Create Chart
    XYChart        chart = new XYChartBuilder().width(800).height(600).title("LineChart03").xAxisTitle("X").yAxisTitle("Y").build();

        // Customize Chart
        chart.getStyler().setPlotBackgroundColor(ChartColor.getAWTColor(ChartColor.GREY));
        chart.getStyler().setPlotGridLinesColor(new Color(255, 255, 255));
        chart.getStyler().setChartBackgroundColor(Color.WHITE);
        chart.getStyler().setLegendBackgroundColor(Color.PINK);
        chart.getStyler().setChartFontColor(Color.MAGENTA);
        chart.getStyler().setChartTitleBoxBackgroundColor(new Color(0, 222, 0));
        chart.getStyler().setChartTitleBoxVisible(true);
        chart.getStyler().setChartTitleBoxBorderColor(Color.BLACK);
        chart.getStyler().setPlotGridLinesVisible(false);

        chart.getStyler().setAxisTickPadding(20);

        chart.getStyler().setAxisTickMarkLength(15);

        chart.getStyler().setPlotMargin(20);

        chart.getStyler().setChartTitleFont(new Font(Font.MONOSPACED, Font.BOLD, 24));
//        chart.getStyler().setLegendFont(new Font(Font.SERIF, Font.PLAIN, 18));
//        chart.getStyler().setLegendPosition(LegendPosition.InsideSE);
//        chart.getStyler().setLegendSeriesLineLength(12);
        chart.getStyler().setAxisTitleFont(new Font(Font.SANS_SERIF, Font.ITALIC, 18));
        chart.getStyler().setAxisTickLabelsFont(new Font(Font.SERIF, Font.PLAIN, 11));
        chart.getStyler().setDatePattern("HH-mm");
        chart.getStyler().setDecimalPattern("#0.00");

        
//        // generates linear data
//        List<Double> ydata2 = new ArrayList<Double>();
//        double tmp = 3.0;
//        double tmp2 = 5.0;
//        for (int i = 0; i < 60 * 5; i++) {
//            c.add(Calendar.MINUTE, 1);
//            xData.add(c.getTime());
//            if (tmp < min) {
//                min = min - 5;
//                chart.getStyler().setYAxisMin(min);
//            }
//            if (tmp > max) {
//                max += 5;
//                chart.getStyler().setYAxisMax(max);
//            }
//            yData.add(tmp);
//            ydata2.add(tmp2);
//            tmp = getTemp(tmp);
//            tmp2 = getTemp(tmp2);
//        }
//    DateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
//    Date date = null;
//    for (int i = 1; i <= 1000; i++) {
// 
//      try {
//        date = sdf.parse(i + ".10.2008");
//      } catch (ParseException e) {
//        e.printStackTrace();
//      }
//      xData.add(date);
//      yData.add(Math.random() * i);
//    }
        // Series
//        XYSeries series = chart.addSeries("Fake Data", xData, yData);
//
//        series.setLineColor(XChartSeriesColors.BLUE);
////    series.setMarkerColor(Color.ORANGE);
//        series.setMarker(SeriesMarkers.NONE);
//        series.setLineStyle(SeriesLines.SOLID);
//
//        XYSeries series2 = chart.addSeries("asd", xData, ydata2);
//        series.setLineColor(XChartSeriesColors.GREEN);
//        series2.setMarker(SeriesMarkers.NONE);
//        series.setLineStyle(SeriesLines.SOLID);
        return chart;
    }


    public void setValue(int actionCode, double value) {
        XYChart chart = this.getChart();
        if(value<min){
            min=value;
            chart.getStyler().setYAxisMin(min-5);
        }else if(value>max){
            max = value;
            chart.getStyler().setYAxisMax(max+5);
        }
//        this.arduinoData.addSensorData(actionCode, dt, value);
        String seriesName = actionCode + "";
        XYSeries series;
        DataList dl = this.arduinoData.getDataList(actionCode);
        if (chart.getSeriesMap().containsKey(seriesName)) {
            chart.updateXYSeries(seriesName, dl.getDateArray(), dl.getValueArray(),null);
            
        } else {
            series = chart.addSeries(seriesName, dl.getDateArray(),dl.getValueArray());
            series.setLineColor(getColor(actionCode));
//            series.setMarkerColor(Color.ORANGE);
            series.setMarker(SeriesMarkers.NONE);
            series.setLineStyle(SeriesLines.SOLID);
        }
        this.chartPanel.repaint();
//        Color c = getColor(actionCode);

    }

    private Color getColor(int actionCode) {
        return colorList[actionCode % colorList.length];
    }

    @Override
    public void clientConnected(EventObject obj) {
    }

    @Override
    public void clientDisconnected(EventObject obj) {
    }

    @Override
    public void dataReceived(ClientDataEvent evt) {
   }

    @Override
    public void dataSent(ClientDataEvent evt) {
   }

    @Override
    public void clientIdAccepted(EventObject obj) {
    }

    @Override
    public void clientIdRefused(EventObject obj) {
    }
    
    
}
