/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.chartPanel.ui;

import client2020.chartPanel.data.ArduinoDataHistory;
import java.awt.GridLayout;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JPanel;

/**
 *
 * @author ISIK
 */
public class GraphPanel extends JPanel {

    Map<Integer, JPanel> panelList;
    Map<Integer, ArduinoDataHistory> arduinoDataMap;

    public GraphPanel() {
        this(null);
    }

    public GraphPanel(Map<Integer, ArduinoDataHistory> map) {
        this.setLayout(new GridLayout(0, 1));
        this.panelList = new TreeMap<Integer, JPanel>();
        this.setArduinoDataMap(arduinoDataMap);
    }

    public void setArduinoDataMap(Map<Integer, ArduinoDataHistory> arduinoDataMap) {
        this.arduinoDataMap = arduinoDataMap;
        this.removeAll();
        if (this.arduinoDataMap != null) {
            for (Integer id : this.arduinoDataMap.keySet()) {
                ArduinoDataLineChartPanel pnl = new ArduinoDataLineChartPanel(this.arduinoDataMap.get(id));
                panelList.put(id, pnl);
            }
        }else{
            this.panelList.clear();
        }
        for (JPanel pnl : this.panelList.values()) {
            this.add(pnl);
        }

    }

}
