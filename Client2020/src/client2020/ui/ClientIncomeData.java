/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.ui;

import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author ISIK
 */
public class ClientIncomeData {
    int senderId;
    Map<Integer,String> receivedData;
    public ClientIncomeData(int senderId){
        this.senderId=senderId;
        receivedData = new TreeMap<>();
    }
    public boolean setData(Integer actionCode, String data){
        return receivedData.put(senderId, data)!=null;
    }
}
