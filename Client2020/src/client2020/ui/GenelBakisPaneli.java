/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.ui;

import data.ActionCode;
import data.NetworkPackage;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JPanel;
import socket.events.ClientDataEvent;
import socket.events.ClientSocketListener;

/**
 *
 * @author ISIK
 */
public class GenelBakisPaneli extends JPanel implements ClientSocketListener {

    private final static long OUT_OF_UPDATE_MS = 60000;
    Map<Integer, Map<Integer, DataPanel>> clientPanelsMap;
    Map<String, JPanel> itemList; //datapanel id si clientid ve actioncode birlesmesi 1001 10 ->100110 clientPanel sadece clientId
    private final static SimpleDateFormat dateFormatter;

    static {
        dateFormatter = new SimpleDateFormat("HH:mm:ss (DD/MM/YY)");
    }

    public GenelBakisPaneli() {
        clientPanelsMap = new TreeMap<>();
//        itemList = new TreeSet<JPanel>();
        itemList = new TreeMap<>();
        this.setLayout(new GridLayout(0, 1));
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Iterator<JPanel> list = itemList.values().iterator();
                    Date dt = new Date();
                    while (list.hasNext()) {
                        JPanel pnl = list.next();
                        if (pnl instanceof DataPanel) {
                            long diff = dt.getTime() - ((DataPanel) pnl).getLastUpdate().getTime();
                            ((DataPanel) pnl).setOutOfUpdate(diff > OUT_OF_UPDATE_MS);
                        }
                    }
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(GenelBakisPaneli.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }).start();
    }

    private void addClientPanel(ClientPanel pnl) {
        int i = 0;
        for (; i < this.getComponentCount(); i++) {
            ClientPanel p = (ClientPanel) this.getComponent(i);
            if (p.clientId > pnl.clientId) {
                break;
            }
        }
        this.add(pnl, i);
    }

    public void removeClientPanel(String id) {
        System.out.println("id :" + id);
        ClientPanel cp = (ClientPanel) this.itemList.remove(id);
        Iterator<String> ids = itemList.keySet().iterator();
        while (ids.hasNext()) {
            if (ids.next().startsWith(id)) {
                ids.remove();
            }
        }
        this.remove(cp);
        this.validate();
        this.repaint();
    }

    public void addNewData(Integer clientId, Integer actionCode, String data) {
        this.addNewData(clientId, actionCode, data, null);
    }

    public void addNewData(Integer clientId, Integer actionCode, String data, Date dt) {
        String key = clientId + "" + actionCode;
        DataPanel dataPanel = (DataPanel) this.itemList.get(key);
        if (dataPanel == null) {
            dataPanel = new DataPanel(actionCode, data);
            this.itemList.put(key, dataPanel);
            String key2 = clientId + "";
            ClientPanel clientPanel = (ClientPanel) this.itemList.get(key2);
            if (clientPanel == null) {
                clientPanel = new ClientPanel(clientId);
                this.addClientPanel(clientPanel);
                this.itemList.put(key2, clientPanel);

            }
            clientPanel.addDataPanel(dataPanel);
            this.revalidate();
            this.repaint();
        } else {
            dataPanel.setValue(data);
        }
        dataPanel.setLastUpdate(dt);
    }

    /*
    public void newData(Integer clientId, Integer actionCode, String data) {
        Map<Integer,DataPanel> dataPanelMap = this.clientPanelsMap.get(clientId);
        if(dataPanelMap==null){
            dataPanelMap = new TreeMap<>();
        }
        DataPanel dataPanel=dataPanelMap.get(actionCode);
        if(dataPanel==null){
            dataPanel = new DataPanel(getActionCodeReferValue(actionCode), data);
        }else
            dataPanel.setValue(data);
    }
     */

    private String getClientIdReferValue(Integer clientId) {
        return clientId + "";
    }

    private String getActionCodeReferValue(Integer actionCode) {
        return actionCode + "";
    }

    @Override
    public void clientConnected(EventObject obj) {
    }

    @Override
    public void clientDisconnected(EventObject obj) {
    }

    @Override
    public void dataReceived(ClientDataEvent evt) {
        NetworkPackage pck = (NetworkPackage) evt.getSource();
        if (ActionCode.isSensorActionCode(pck.getActionCode())) {
            addNewData(pck.getHostId(), pck.getActionCode(), pck.getDataArray().get(0), pck.getMessageDate());
        }
    }

    @Override
    public void dataSent(ClientDataEvent evt) {
    }

    @Override
    public void clientIdAccepted(EventObject obj) {
    }

    @Override
    public void clientIdRefused(EventObject obj) {
    }

    class ClientPanel extends JPanel {

        int clientId;

        public ClientPanel(int clientId) {
            this.clientId = clientId;
            this.setLayout(new FlowLayout(FlowLayout.LEFT));
            setTitle();
        }

        public void setTitle() {
            String title = GenelBakisPaneli.this.getClientIdReferValue(clientId);
            setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), title));
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof ClientPanel) {
                return ((ClientPanel) obj).clientId == this.clientId;
            }
            return false;
        }

        private void addDataPanel(DataPanel dataPanel) {
            int i = 0;
            for (; i < this.getComponentCount(); i++) {
                DataPanel pnl = (DataPanel) this.getComponent(i);
                if (pnl.actionCode > dataPanel.actionCode) {
                    break;
                }
            }
            this.add(dataPanel, i);
//            this.repaint();
        }

        private void removeDataPanel(String id) {
            DataPanel pnl = (DataPanel) itemList.get(id);
            this.remove(pnl);
        }

    }

    class DataPanel extends JPanel {

        JLabel jlValue;
        Integer actionCode;
        Date lastUpdate;
        boolean outOfUpdate;

        public Date getLastUpdate() {
            return lastUpdate;
        }

        public boolean isOutOfUpdate() {
            return outOfUpdate;
        }

        public void setOutOfUpdate(boolean outOfUpdate) {
            boolean flag = this.outOfUpdate != outOfUpdate;
            this.outOfUpdate = outOfUpdate;
            if (flag) {
                processOutOfUpdate();
            }
        }

        private void processOutOfUpdate() {
            if (outOfUpdate) {
                this.jlValue.setForeground(Color.red);
            } else {
                this.jlValue.setForeground(Color.BLACK);
            }
        }

        public void setLastUpdate(Date lastUpdate) {
            this.lastUpdate = lastUpdate;
            if (lastUpdate == null) {
                return;
            }
            this.setToolTipText(dateFormatter.format(lastUpdate));
        }

        public DataPanel(Integer actionCode, String value) {
            this.actionCode = actionCode;
            this.setLayout(new FlowLayout(FlowLayout.CENTER));
            jlValue = new JLabel(value);
            this.add(jlValue);
            this.setTitle();
        }

        private void setTitle() {
            String title = GenelBakisPaneli.this.getActionCodeReferValue(actionCode);
//            setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), title));
            setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), title));
        }

        public void setValue(String value) {
            this.jlValue.setText(value);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof DataPanel) {
                return ((DataPanel) obj).actionCode == actionCode;
            }
            return false;
        }

    }
}
