/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.ui;

import data.NetworkPackage;
import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EventObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import log.LogEvent;
import log.LogListener;
import socket.events.ClientDataEvent;
import socket.events.ClientSocketListener;

/**
 *
 * @author ISIK
 */
public class LogPanel extends javax.swing.JPanel implements ClientSocketListener,LogListener{

    public final static Color DEBUG_COLOR = Color.ORANGE;
    public final static Color INFO_COLOR = Color.green;
    public final static Color ERROR_COLOR = Color.red;
    public final static Color DATA_OUT_COLOR = Color.blue;
    public final static Color DATA_IN_COLOR=Color.BLACK;
    public final static Color UNDEFINED_COLOR = Color.yellow;
    /**
     * Creates new form LogPanel
     */
    SimpleAttributeSet attributeSet;
    final Document doc;

    public LogPanel() {
        attributeSet = new SimpleAttributeSet();
        initComponents();
        doc = this.jTextPane1.getDocument();
    }

    public void appendMessage(String message,Color c,Date dt){
        if(dt==null)
            dt = new Date();
        StyleConstants.setForeground(attributeSet, c);
//       jScrollPane2.setViewportView(jTextPane1);
        SimpleDateFormat formatter = new SimpleDateFormat("(DD) HH:mm:ss");
        String m = formatter.format(dt)+" : "+message;
        try {
            if(doc.getLength()!=0)
                doc.insertString(doc.getLength(), System.lineSeparator(), attributeSet);
            doc.insertString(doc.getLength(), m, attributeSet);
            if(jCheckBox1.isSelected())
            jTextPane1.setCaretPosition(doc.getLength()-1);
            
        } catch (BadLocationException ex) {
            Logger.getLogger(LogPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void appendMessage(String message, Color c) {
        this.appendMessage(message, c,null);
    }

    public boolean isAutoScrollable() {
        return this.jCheckBox1.isSelected();
    }

    public void setAutoScollable(boolean autoScrollable) {
        this.jCheckBox1.setSelected(autoScrollable);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCheckBox1 = new javax.swing.JCheckBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();

        setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "asd"));

        jCheckBox1.setText("AutoScroll");

        jScrollPane2.setViewportView(jTextPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jCheckBox1)
                        .addGap(0, 293, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jCheckBox1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextPane jTextPane1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void clientConnected(EventObject obj) {
        appendMessage("Client connected", INFO_COLOR);
    }

    @Override
    public void clientDisconnected(EventObject obj) {
        appendMessage("Client Disconnected", INFO_COLOR);
    }

    @Override
    public void dataReceived(ClientDataEvent evt) {
        NetworkPackage pck = (NetworkPackage) evt.getSource();
        appendMessage(pck.getMessage(), DATA_IN_COLOR,pck.getMessageDate());
    }

    @Override
    public void dataSent(ClientDataEvent evt) {
        
        NetworkPackage pck = (NetworkPackage) evt.getSource();
        appendMessage(pck.getMessage(), DATA_OUT_COLOR,pck.getMessageDate());
    }

    @Override
    public void clientIdAccepted(EventObject obj) {
        appendMessage("client id accepted by server", INFO_COLOR);
    }

    @Override
    public void clientIdRefused(EventObject obj) {
        appendMessage("client id refused by server", ERROR_COLOR);
    }

    @Override
    public void logOccured(LogEvent evt) {     
        Color c;
        switch (evt.getType()) {
            case LogEvent.DEBUG:
                c = DEBUG_COLOR;
                break;
            case LogEvent.ERROR:
                c=ERROR_COLOR;
                break;
            default:
                c=INFO_COLOR;
                break;
        }
        appendMessage(evt.getMessage(), c);
    }

}
