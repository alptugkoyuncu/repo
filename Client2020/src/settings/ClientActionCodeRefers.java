/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package settings;

import client2020.ui.MainFrame;

/**
 *
 * @author ISIK
 */
public class ClientActionCodeRefers extends AbstractSeraSettings{
    
    public static String getReferValue(int clientId,int actionCode){
        ClientActionCodeRefers s = new ClientActionCodeRefers(clientId,actionCode);
        return s.getRefervalue();
                
    }
    public static boolean saveReferValue(int clientId,int actionCode,String val){
        ClientActionCodeRefers s = new ClientActionCodeRefers(clientId,actionCode);
        s.setReferValue(val);
        return s.save();
        
    }
    
    int clientId;
    int actionCode;
    public ClientActionCodeRefers(int clientId,int actionCode){
        super(MainFrame.PROPERTIES_FOLDER_NAME,"refer name.properties");
        this.actionCode=actionCode;
        this.clientId=clientId;
    }
    public String getRefervalue(){
        return this.values.get(getPropName());
    }
    public void setReferValue(String value){
        this.values.put(getPropName(), value);
    }
    private String getPropName(){
        return clientId+"."+actionCode;
    }
    

    @Override
    public void setDefaultValues() {
  
    }
    
    
}
