/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package settings;

import client2020.ui.MainFrame;

/**
 *
 * @author ISIK
 */
public class SocketSettings extends AbstractSeraSettings {

    public final static String PN_HOST_IP = "soc.ip";
    public final static String PN_PORT = "soc.port";
    public final static String PN_ID = "soc.id";

    public SocketSettings() {
        super(MainFrame.PROPERTIES_FOLDER_NAME, "settings.properties");
    }

    public Integer getID() {
        String id = this.values.get(PN_ID);
        try {
            return Integer.parseInt(id);
        } catch (NumberFormatException ex) {
            return null;
        }
    }
    public void setId(Integer id){
        this.values.put(PN_ID, id+"");
    }
    public Integer getPort() {
        String port = this.values.get(PN_PORT);
        try {
            return Integer.parseInt(port);
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public void setPort(Integer port) {
        this.values.put(PN_PORT, port + "");
    }

    public String getIp() {
        return this.values.get(PN_HOST_IP);
    }

    public void setIp(String ip) {
        this.values.put(PN_HOST_IP, ip);
    }

    @Override
    public void setDefaultValues() {
        this.setIp("192.168.1.11");
        this.setPort(5555);
    }

}
