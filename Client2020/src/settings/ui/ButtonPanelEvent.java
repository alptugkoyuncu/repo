/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package settings.ui;

import javax.swing.JOptionPane;

/**
 *
 * @author ISIK
 */
public class ButtonPanelEvent {
    public final static int EVENT_SUCCESS=JOptionPane.INFORMATION_MESSAGE;
    public final static int  EVENT_FAILED=JOptionPane.ERROR_MESSAGE;
    private int type;
    private String message;

    public ButtonPanelEvent(int type, String message) {
        this.type = type;
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }
    public void createOptionPaneMessage(){
        JOptionPane.showMessageDialog(null, message, null,type);
    }
}
