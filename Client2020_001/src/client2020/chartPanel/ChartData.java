/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.chartPanel;

import db.AlanBean;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ISIK
 */
public class ChartData {
    private ArrayList<Double> values;
    private ArrayList<Date> dates;
    AlanBean alan;

    public AlanBean getAlan() {
        return alan;
    }

    public void setAlan(AlanBean alan) {
        this.alan = alan;
    }
    
    public ChartData() {
        this.values = new ArrayList<>();
        this.dates= new ArrayList<>();
    }

   

   public void addData(Double value,Date date){
       this.values.add(value);
       this.dates.add(date);
   }
    public ArrayList<Double> getValues() {
        return values;
    }

    public ArrayList<Date> getDates() {
        return dates;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");
        for(int i=0; i<values.size(); i++){
            builder.append(i);
            builder.append(" : ");
            builder.append(f.format(dates.get(i)));
            builder.append("->");
            builder.append(values.get(i));
            builder.append("\n");
        }
    
    return builder.toString();
    }
}
