/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.chartPanel;

import client2020.ui.MainFrame;
import db.AlanBean;
import db.DBConnection;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EventObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.internal.chartpart.Chart;
import org.knowm.xchart.internal.series.Series;
import org.knowm.xchart.style.colors.ChartColor;
import org.knowm.xchart.style.lines.SeriesLines;
import org.knowm.xchart.style.markers.SeriesMarkers;
import socket.events.ClientDataEvent;
import socket.events.ClientSocketListener;

/**
 *
 * @author ISIK
 */
public class TempDataLineChart {

    private final static long MINUTE_5 = 300000L;
    private final static long MINUTE_10 = 600000L;
    private final static long MINUTE_30 = 1800000L;
    private final static long MINUTE_60 = 3600000L;
    private final static long MINUTE_120 = 7200000L;
    private final XYChart chart;
    ChartData arduinoData;
    JPanel chartPanel;
//    XChartPanel<XYChart> chartPanel;
    private final List<Color> colorList;

    private List<Double> borders;
    private List<ChartData> chartDataList;
    boolean minBorder, maxBorder;
    private TempDataSettings tempDataSettings;
    XChartPanel<XYChart> panel;

    private void addChartData(AlanBean alan, Date startDate, Date finishDate, int tempType, Integer limit) {
        DBConnection conn = MainFrame.DB_CONNECTION;
        long aralik = (finishDate.getTime() - startDate.getTime()) / limit;
        if (aralik < MINUTE_5) {
            aralik = MINUTE_5;
        } else if (aralik < MINUTE_10) {
            aralik = MINUTE_10;
        } else if (aralik < MINUTE_30) {
            aralik = MINUTE_30;
        } else if (aralik < MINUTE_60) {
            aralik = MINUTE_60;
        } else {
            aralik = MINUTE_120;
        }
        chartDataList.add(conn.getTempData(alan, startDate, finishDate, aralik, tempType));
    }

    public TempDataLineChart() {
        borders = new ArrayList<>();
        colorList = new ArrayList<>();
//       colorList.add(Color.black);//borderlar icin kullaniliyor
        colorList.add(Color.BLUE);
        colorList.add(Color.GREEN);
//        colorList.add(Color.RED); min max border icin

        colorList.add(Color.DARK_GRAY);
        colorList.add(Color.CYAN);
        colorList.add(Color.GRAY);
        colorList.add(Color.MAGENTA);
        colorList.add(Color.ORANGE);
        colorList.add(Color.PINK);
        colorList.add(Color.YELLOW);
        chartDataList = new ArrayList<>();
        chart = new XYChartBuilder().width(1600).height(800).title("LineChart03").xAxisTitle("X").yAxisTitle("Y").build();
        createChart();
    }

    public void clear() {
        chartDataList.clear();
        borders.clear();
        chart.getSeriesMap().clear();
    }
//

    public JPanel getChartPanel() {

        return panel;

    }

    public void drawChart() {
        double min = 100.0, max = -30.0;
        for (AlanBean a : this.tempDataSettings.getSeciliAlanlar()) {
            this.addChartData(a, this.tempDataSettings.getBaslangicTarihi(), this.getTempDataSettings().getBitisTarihi(), this.tempDataSettings.getTempType(), this.getTempDataSettings().getDataLimit());
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.tempDataSettings.getBaslangicTarihi());
        int date = cal.get(Calendar.DATE);
        cal.setTime(this.tempDataSettings.getBitisTarihi());
        if (date != cal.get(Calendar.DATE)) {
            chart.getStyler().setDatePattern("(dd)\n HH:mm");
        } else {
            chart.getStyler().setDatePattern("HH:mm");

        }

        for (ChartData chartData : chartDataList) {
            String title = chartData.getAlan().getBolge().getName() + "-" + chartData.getAlan().getName();
            XYSeries series = chart.addSeries(title + " data", chartData.getDates(), chartData.getValues());
            series.setLabel(title + "\nMin : " + series.getYMin() + " Max : " + series.getYMax());

            Color c = retrieveNextColor();
            series.setLineColor(c);
            series.setMarker(SeriesMarkers.NONE);
            series.setLineStyle(SeriesLines.SOLID);

            if (min > series.getYMin()) {
                min = series.getYMin();
            }
            if (series.getYMax() > max) {
                max = series.getYMax();
            }
        }

        chart.getStyler().setYAxisMin(min - 2);
        chart.getStyler().setYAxisMax(max + 2);
        if (chartDataList.isEmpty()) {
            return;
        }

        if (this.tempDataSettings.isMinBorderAvailable()) {
            List<Double> borderData = new ArrayList<>();
            for (int i = 0; i < chartDataList.get(0).getDates().size(); i++) {
                if (i % 20 < 10) {
                    borderData.add(min);
                } else {
                    borderData.add(null);
                }
            }
            XYSeries minSeries = chart.addSeries("Min = " + min + "", chartDataList.get(0).getDates(), borderData);
            minSeries.setLineColor(Color.RED);
            minSeries.setMarker(SeriesMarkers.NONE);
            minSeries.setLineStyle(SeriesLines.SOLID);
        }

        if (this.tempDataSettings.isMaxBorderAvailable()) {
            List<Double> borderData = new ArrayList<>();
            for (int i = 0; i < chartDataList.get(0).getDates().size(); i++) {
                if (i % 20 < 10) {
                    borderData.add(max);
                } else {
                    borderData.add(null);
                }
            }
            XYSeries maxSeries = chart.addSeries("Max = " + max + "", chartDataList.get(0).getDates(), borderData);
            maxSeries.setLineColor(Color.RED);
            maxSeries.setMarker(SeriesMarkers.NONE);
            maxSeries.setLineStyle(SeriesLines.SOLID);
        }
        for (Double border : borders) {
//            
            if (border < min || border > max) {
                continue;
            }
            List<Double> borderData = new ArrayList<>();
            for (int i = 0; i < chartDataList.get(0).getDates().size(); i++) {
                if (i % 20 < 10) {
                    borderData.add(border);
                } else {
                    borderData.add(null);
                }
            }
            XYSeries series2 = chart.addSeries("border = " + border + "", chartDataList.get(0).getDates(), borderData);
            series2.setLineColor(Color.BLACK);
            series2.setMarker(SeriesMarkers.NONE);
            series2.setLineStyle(SeriesLines.SOLID);
        }
        panel.repaint();
//        panel.validate();
    }

    private void createChart() {

        chart.getStyler().setPlotBackgroundColor(ChartColor.getAWTColor(ChartColor.GREY));
        chart.getStyler().setPlotGridLinesColor(new Color(255, 255, 255));
        chart.getStyler().setChartBackgroundColor(Color.WHITE);
        chart.getStyler().setLegendBackgroundColor(Color.PINK);
        chart.getStyler().setChartFontColor(Color.MAGENTA);
        chart.getStyler().setChartTitleBoxBackgroundColor(new Color(0, 222, 0));
        chart.getStyler().setChartTitleBoxVisible(true);
        chart.getStyler().setChartTitleBoxBorderColor(Color.BLACK);
        chart.getStyler().setPlotGridLinesVisible(false);

        chart.getStyler().setAxisTickPadding(2);

        chart.getStyler().setAxisTickMarkLength(1);

        chart.getStyler().setPlotMargin(2);

        chart.getStyler().setChartTitleFont(new Font(Font.MONOSPACED, Font.BOLD, 24));
        chart.getStyler().setAxisTitleFont(new Font(Font.SANS_SERIF, Font.ITALIC, 18));
        chart.getStyler().setAxisTickLabelsFont(new Font(Font.SERIF, Font.PLAIN, 11));

        chart.getStyler().setDecimalPattern("#0.0");

        panel = new XChartPanel<>(this.chart);
    }

    private Color retrieveNextColor() {
        Color c = colorList.size() > 0 ? colorList.remove(0) : Color.BLACK;
        colorList.add(c);
        return c;
    }

    public TempDataSettings getTempDataSettings() {
        return tempDataSettings;
    }

    public void setTempDataSettings(TempDataSettings tempDataSettings) {
        this.tempDataSettings = tempDataSettings;
    }

}
