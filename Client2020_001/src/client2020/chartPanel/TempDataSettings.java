/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package client2020.chartPanel;

import db.AlanBean;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 *
 * @author alptug
 */
public class TempDataSettings {
    private List<Double> sinirlar;
    private boolean minBorderAvailable;
    private boolean maxBorderAvailable;
    private Date baslangicTarihi;
    private Date bitisTarihi;
    private List<AlanBean> seciliAlanlar;
    private int tempType;
    private int dataLimit;
    public TempDataSettings() {
        dataLimit=500;
    }
    

    public void setDataLimit(int dataLimit) {
        this.dataLimit = dataLimit;
    }

    public int getDataLimit() {
        return dataLimit;
    }
    public List<Double> getSinirlar() {
        return sinirlar;
    }

    public void setSinirlar(List<Double> sinirlar) {
        this.sinirlar = sinirlar;
    }

    public boolean isMinBorderAvailable() {
        return minBorderAvailable;
    }

    public void setMinBorderAvailable(boolean minBorderAvailable) {
        this.minBorderAvailable = minBorderAvailable;
    }

    public boolean isMaxBorderAvailable() {
        return maxBorderAvailable;
    }

    public void setMaxBorderAvailable(boolean maxBorderAvailable) {
        this.maxBorderAvailable = maxBorderAvailable;
    }

    public Date getBaslangicTarihi() {
        return baslangicTarihi;
    }

    public void setBaslangicTarihi(Date baslangicTarihi) {
        this.baslangicTarihi = baslangicTarihi;
    }

    public Date getBitisTarihi() {
        return bitisTarihi;
    }

    public void setBitisTarihi(Date bitisTarihi) {
        this.bitisTarihi = bitisTarihi;
    }

    public List<AlanBean> getSeciliAlanlar() {
        return seciliAlanlar;
    }

    public void setSeciliAlanlar(List<AlanBean> seciliAlanlar) {
        this.seciliAlanlar = seciliAlanlar;
    }

    public int getTempType() {
        return tempType;
    }

    public void setTempType(int tempType) {
        this.tempType = tempType;
    }
}
