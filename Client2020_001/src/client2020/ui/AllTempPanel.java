/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.ui;

import static data.ActionCode.*;
import data.ClientPackage;
import data.GeneralSystemConstant;
import data.NetworkPackage;
import java.net.Socket;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;
import socket.ClientSocket;
import socket.ClientType;
import socket.events.ClientDataEvent;
import socket.events.ClientSocketListener;

/**
 *
 * @author ISIK
 */
public class AllTempPanel extends JPanel implements ClientSocketListener {

    Map<Integer, TempPanel> panelMap;
    ClientSocket socket;

    public AllTempPanel(ClientSocket socket) {
        panelMap = new HashMap<>();
        this.socket = socket;
    }

    @Override
    public void clientConnected(EventObject obj) {

    }

    @Override
    public void clientDisconnected(EventObject obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean addPanel(String clientId) {
        if (clientId == null || clientId.isBlank()) {
            return false;
        }
        try {
            Integer id = Integer.parseInt(clientId);
            if (isTempClient(id)) {
                TempPanel pnl = new TempPanel(socket.getClientId());
                this.removePanel(clientId);
                this.panelMap.put(socket.getClientId(), pnl);
                this.add(pnl);
            }
        } catch (Exception ex) {
            return false;
        }
        return true;
    }
    private boolean isTempClient(Integer id){
        return ClientType.getClientType(id)==ClientType.TEMP_CONTROL;
    }
    public boolean removePanel(String clientId) {
        try {
            Integer id = Integer.parseInt(clientId);
            if (isTempClient(id)) {
                TempPanel pnl = this.panelMap.remove(id);
                if (pnl != null) {
                    this.remove(pnl);
                }
            }
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    @Override
    public void dataReceived(ClientDataEvent evt) {
        ClientPackage pck = (ClientPackage) evt.getSource();
        int hostId = pck.getHostId();
        if (!(hostId == GeneralSystemConstant.SERVER_ID || isTempClient(hostId))) {
            return;
        }
        switch (pck.getActionCode()) {
            case AC_CLIENT_DISCONNECTED: {
                this.panelMap.clear();
                this.removeAll();
                this.validate();
                break;
            }
            case AC_FRIEND_CONNECTED: {
                if (!pck.getDataArray().isEmpty()) {
                    this.addPanel(pck.getDataArray().get(0));
                    this.validate();
                }
                break;
            }
            case AC_FRIEND_DISCONNECTED: {
                if (!pck.getDataArray().isEmpty()) {
                    this.removePanel(pck.getDataArray().get(0));
                    this.validate();
                }
                break;
            }
            case AC_GET_ALL_CONNECTED_CLIENTS_ID:{
                for(String clientId : pck.getDataArray()){
                    try{
                        Integer id = Integer.parseInt(clientId);
                        
                    }catch(Exception ex){
                        
                    }
                }
            }
        }
    }

    /*      
    public final static int AC_GET_ALL_CONNECTED_CLIENTS_ID = 5;
    public final static int AC_SERVER_IS_ALIVE = 6;

    public final static int AC_RESET_ARDUINO = 9;
    //temp kontrol data info
    public final static int AC_SEND_TEMP_ALL = 10;
    public final static int AC_SEND_TEMP0 = 11;
    public final static int AC_SEND_TEMP1 = 12;
    public final static int AC_SEND_TEMP2 = 13;
    public final static int AC_SEND_TEMP3 = 14;

    //havuz kontrol data info
    public final static int AC_SEND_HAVUZ_ESP_ALL_DATA = 20;
    public final static int AC_SEND_RELAY_STATE = 21;
    public final static int AC_SEND_OUTER_TIMER_STATE = 22;
    public final static int AC_SEND_ARDUINO_RELAY_CONTROL_STATE = 23;
    public final static int AC_SEND_PRESSURE_SENSOR_VALUE = 24;

// actions for havuzkontrol
    public final static int AC_DISABLE_OUTER_TIMER = 30;
    public final static int AC_ENABLE_OUTER_TIMER = 31;
    public final static int AC_SET_ROLE_AKTIF = 32;
    public final static int AC_SET_ROLE_INAKTIF = 33; // todo first disable outer timer if outer timer enable you can not disable if outer timer starts the relay
    public final static int AC_SEND_PRESSURE_CALIBRATION_VALUES = 34;

    public final static int AC_REQUEST_FOCUED_PROVIDERS = 60;
    public final static int AC_SEND_BOARD_LAST_RESET_TIME = 61;
    public final static int AC_BOARD_FREE_MEMORY = 62;

    public final static int AC_BOARD_SENSOR_ACTION_CODES = 63;

//    public final static int AC_REQUEST_PING = 99;
    public final static int AC_SEND_PING = 100;

    public final static int AC_SEND_WIFI_SSID = 101;
    public final static int AC_SEND_PASS = 102;
    public final static int AC_SEND_IP = 103;
    public final static int AC_SEND_PORT = 104;
    public final static int AC_SEND_ID = 105;

    public final static int AC_ESP_STARTED = 106;
    public final static int AC_RESET_ESP = 107;

    public final static int AC_WIFI_CONNECTED = 108;
    public final static int AC_WIFI_DISCONNECTED = 109;
    public final static int AC_WIFI_CONNECTION_FAILED = 110;

    public final static int AC_SERVER_CONNECTED = 111;
    public final static int AC_SERVER_DISCONNECTED = 112;
    public final static int AC_SERVER_CONNECTION_FAILED = 113;

    public final static int AC_ID_REQUESTED = 114;
    public final static int AC_ID_ACCEPTED = 115;
    public final static int AC_ID_REFUSED = 116;

    public final static int AC_DB_GET_HAVUZ_DATA = 190;
    public final static int AC_ESP_DEBUG = 200;

    public final static int AC_SET_WIFI_SSID = 201;
    public final static int AC_SET_WIFI_PASS = 202;
    public final static int AC_SET_SERVER_IP = 203;
    public final static int AC_SET_SERVER_PORT = 204;
    public final static int AC_SET_CLIENT_ID = 205;
    public final static int AC_RESET_NETWORK_PARAMS_RECORD = 206;
    public final static int AC_SAVE_NETWORK_PARAMS_RECORD = 207;
    public final static int AC_LOAD_NETWORK_PARAMS_RECORD = 208;

    public final static int AC_SET_PRESSURE_CALIBRATION_VALUES = 219;
    public final static int AC_SAVE_PRESSURE_CALIBRATION_VALUES = 220;
    ;public final static int AC_LOAD_PRESSURE_CALIBRATON_VALUES = 221;
    public final static int AC_RESET_PRESSURE_CALIBRATION_VALUES = 222;
     */
    @Override
    public void dataSent(ClientDataEvent evt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clientIdAccepted(EventObject obj) {

    }

    @Override
    public void clientIdRefused(EventObject obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
