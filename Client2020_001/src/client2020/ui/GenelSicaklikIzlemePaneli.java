/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package client2020.ui;

import client2020.chartPanel.GraphFrame;
import client2020.chartPanel.RestrictionPanel;
import static data.ActionCode.AC_SEND_TEMP0;
import static data.ActionCode.AC_SEND_TEMP1;
import static data.ActionCode.AC_SEND_TEMP2;
import static data.ActionCode.AC_SEND_TEMP3;
import static data.ActionCode.AC_SEND_TEMP_ALL;
import data.NetworkPackage;
import db.AlanBean;
import db.BolgeBean;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventObject;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import socket.ClientType;
import socket.events.ClientDataEvent;
import socket.events.ClientSocketListener;

/**
 *
 * @author alptug
 */
public class GenelSicaklikIzlemePaneli extends JPanel implements ClientSocketListener {

    List<AlanBean> alanList;
    List<BolgeBean> bolgeList;
    List<BolgePanel> bolgePanelList = null;
    List<AlanPanel> alanPanelList = null;

    public GenelSicaklikIzlemePaneli() {
        loadAlanBolgeBilgisi();
        alanPanelList = new ArrayList<>();
        bolgePanelList = new ArrayList<>();
        updateLayout();
    }

    public final void updateLayout() {
        this.setLayout(new GridLayout(0, 1));
        for (BolgeBean b : bolgeList) {
            List<AlanBean> l = new ArrayList<>();
            for (AlanBean ab : alanList) {
                if (b.equals(ab.getBolge())) {
                    l.add(ab);
                }
            }
            BolgePanel bp = new BolgePanel(b, l);
            bolgePanelList.add(bp);
            this.add(bp);
        }
        this.validate();
    }

    private void loadAlanBolgeBilgisi() {
        if (MainFrame.DB_CONNECTION.isValid()) {
            alanList = MainFrame.DB_CONNECTION.getAllAlan();
            bolgeList = MainFrame.DB_CONNECTION.getAllBolge();
        } else {
//            readAllAlanBolgeFromFile();
            alanList = new ArrayList<>();
            bolgeList = new ArrayList<>();
        }
    }

    private void writeAllAlanBolgeToFile() {

    }

    private void readAllAlanBolgeFromFile() {

    }

    public void updateAlanPanel(int clientId, int actionCode, String value) {
        System.out.println("clientid : "+clientId+" actioncode : "+actionCode+" value:"+value);
        boolean hasAlan = false;
        for (AlanPanel pnl : alanPanelList) {
            AlanBean a = pnl.getAlan();
            if (a.getActionCode() == actionCode && a.getIdclient() == clientId) {
                hasAlan = true;
                pnl.setValue(value);
                break;
            }
        }
        if (!hasAlan) {
            //db ayri bolge ve alan yarat
            int id = clientId % 100;
            id = id * 100;
            id += actionCode;
            BolgeBean bolgeBean = new BolgeBean(clientId, clientId + "");
            AlanBean alan = new AlanBean(id, bolgeBean, id+"", clientId, actionCode);
            this.alanList.add(alan);
            if (!bolgeList.contains(bolgeBean)) {
                this.bolgeList.add(bolgeBean);
                //bolge yoksa once bolge yarat sonra da panale ekle
//                this.bolgePanelList.add(new BolgePanel(bean));
                ArrayList<AlanBean> l = new ArrayList<>();
                l.add(alan);
                BolgePanel bp = new BolgePanel(bolgeBean,l);
                this.add(bp);
                this.bolgePanelList.add(bp);
            } else {
                for (BolgePanel bp : bolgePanelList) {
                    if (bp.bolge.equals(bolgeBean)) {
                        bp.addAlanPanel(alan);
                        bp.validate();
                    }
                }
            }
            this.validate();
        }
    }

    @Override
    public void clientConnected(EventObject obj) {

    }

    @Override
    public void clientDisconnected(EventObject obj) {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void dataReceived(ClientDataEvent evt) {
        NetworkPackage pck = (NetworkPackage) evt.getSource();
        List<String> dataArray = pck.getDataArray();
        if (dataArray == null || dataArray.isEmpty()) {
            return;
        }
        Integer actionCode = pck.getActionCode();
        Integer clientId = pck.getHostId();
        switch (actionCode) {
            case AC_SEND_TEMP_ALL: {
                if (dataArray.size() < 4) {
                    return;
                }
                updateAlanPanel(clientId, AC_SEND_TEMP0, dataArray.get(0));
                updateAlanPanel(clientId, AC_SEND_TEMP1, dataArray.get(1));
                updateAlanPanel(clientId, AC_SEND_TEMP2, dataArray.get(2));
                updateAlanPanel(clientId, AC_SEND_TEMP3, dataArray.get(3));
                break;
            }
            case AC_SEND_TEMP0:
            case AC_SEND_TEMP1:
            case AC_SEND_TEMP2:
            case AC_SEND_TEMP3:
                updateAlanPanel(clientId, actionCode, dataArray.get(0));
                break;
            default:
                break;
        }
    }

    @Override
    public void dataSent(ClientDataEvent evt) {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void clientIdAccepted(EventObject obj) {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void clientIdRefused(EventObject obj) {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    class BolgePanel extends JPanel {

        JPanel mainPanel, buttonPanel;
        JButton btnShowChat;
        BolgeBean bolge;

        public BolgePanel(BolgeBean bolge, List<AlanBean> l) {
            this.bolge = bolge;
            this.setLayout(new BorderLayout());
            mainPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
            btnShowChat = new JButton("Show chart");
            buttonPanel.add(btnShowChat);
            btnShowChat.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    java.awt.EventQueue.invokeLater(new Runnable() {
                        public void run() {
//                            new GraphFrame(new RestrictionPanel(clientId)).setVisible(true);
                        }
                    });
                }
            });
            for (AlanBean ab : l) {
                addAlanPanel(ab);
            }
            this.add(mainPanel, BorderLayout.CENTER);
            this.add(buttonPanel, BorderLayout.EAST);
            setTitle(this.bolge.getName());
        }

        public final void addAlanPanel(AlanBean alan) {
            AlanPanel pnl = new AlanPanel(alan);
            alanPanelList.add(pnl);
            this.mainPanel.add(pnl);
        }

        private void setTitle(String title) {
            setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), title));

        }
    }

    class AlanPanel extends JPanel {

        JLabel jlValue;
        AlanBean alan;
        Date lastUpdate;

        public AlanPanel(AlanBean alan) {
            this.alan = alan;
//            setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), title));
            String title = alan.getName();

            setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), title));
            jlValue = new JLabel();
            jlValue.setText("----");
            this.add(jlValue);

        }

        public void setValue(String val) {
            this.jlValue.setText(val);
            this.lastUpdate = new Date();
        }

        public Date getLastUpdate() {
            return lastUpdate;
        }

        public AlanBean getAlan() {
            return alan;
        }
    }
}
