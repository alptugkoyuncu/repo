/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.ui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Date;

/**
 *
 * @author ISIK
 */
public class HavuzKontrolData {

    private double minBasinc;

    private boolean externalTimerEnable;
    private boolean arduinoRoleKontrolEnable;
    private boolean roleActive;

    private boolean pressureEnough;
    
    private double pressure;

    public static final String PROP_BASINC = "basinc";
    public static final String PROP_ARDUINOROLEKONTROLENABLE = "arduinoRoleKontrolEnable";
    public static final String PROP_EXTERNALTIMERENABLE = "externalTimerEnable";
    public static final String PROP_ROLEACTIVE = "roleActive";

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    /**
     * Get the value of pressure
     *
     * @return the value of pressure
     */
    public double getPressure() {
        return pressure;
    }

    /**
     * Set the value of pressure
     *
     * @param pressure new value of pressure
     */
    public void setPressure(double pressure) {
        double oldBasinc = this.pressure;
        this.pressure = pressure;
        this.setPressureEnough(pressure);
        propertyChangeSupport.firePropertyChange(PROP_BASINC, oldBasinc, pressure);
    }

    public static final String PROP_PRESSUREENOUGH = "pressureEnough";

    /**
     * Get the value of pressureEnough
     *
     * @return the value of pressureEnough
     */
    public boolean isPressureEnough() {
        return pressureEnough;
    }

    /**
     * Set the value of pressureEnough
     *
     * @param pressure new value of pressureEnough
     */
    private void setPressureEnough(double pressure) {
        boolean temp = pressure >= this.minBasinc;
        boolean oldPressureEnough = this.pressureEnough;
        this.pressureEnough = temp;
        if (this.pressureEnough != oldPressureEnough) {
            propertyChangeSupport.firePropertyChange(PROP_PRESSUREENOUGH, oldPressureEnough, this.pressureEnough);
        }
    }


    public double getMinBasinc() {
        return minBasinc;
    }

    public void setMinBasinc(double minBasinc) {
        this.minBasinc = minBasinc;
    }

    /**
     * Get the value of roleActive
     *
     * @return the value of roleActive
     */
    public boolean isRoleActive() {
        return roleActive;
    }

    /**
     * Set the value of roleActive
     *
     * @param roleActive new value of roleActive
     */
    public void setRoleActive(boolean roleActive) {
        boolean oldRoleActive = this.roleActive;
        this.roleActive = roleActive;
        if (this.roleActive != oldRoleActive) {
            propertyChangeSupport.firePropertyChange(PROP_ROLEACTIVE, oldRoleActive, roleActive);
        }
    }

    /**
     * Get the value of arduinoRoleKontrolEnable
     *
     * @return the value of arduinoRoleKontrolEnable
     */
    public boolean isArduinoRoleKontrolEnable() {
        return arduinoRoleKontrolEnable;
    }

    /**
     * Set the value of arduinoRoleKontrolEnable
     *
     * @param arduinoRoleKontrolEnable new value of arduinoRoleKontrolEnable
     */
    public void setArduinoRoleKontrolEnable(boolean arduinoRoleKontrolEnable) {
        boolean oldArduinoRoleKontrolEnable = this.arduinoRoleKontrolEnable;
        this.arduinoRoleKontrolEnable = arduinoRoleKontrolEnable;
        if (this.arduinoRoleKontrolEnable != oldArduinoRoleKontrolEnable) {
            propertyChangeSupport.firePropertyChange(PROP_ARDUINOROLEKONTROLENABLE, oldArduinoRoleKontrolEnable, arduinoRoleKontrolEnable);
        }
    }

    /**
     * Get the value of externalTimerEnable
     *
     * @return the value of externalTimerEnable
     */
    public boolean isExternalTimerEnable() {
        return externalTimerEnable;
    }

    /**
     * Set the value of externalTimerEnable
     *
     * @param externalTimerEnable new value of externalTimerEnable
     */
    public void setExternalTimerEnable(boolean externalTimerEnable) {
        boolean oldExternalTimerEnable = this.externalTimerEnable;
        this.externalTimerEnable = externalTimerEnable;
        if (this.externalTimerEnable != oldExternalTimerEnable) {
            propertyChangeSupport.firePropertyChange(PROP_EXTERNALTIMERENABLE, oldExternalTimerEnable, externalTimerEnable);
        }
    }

    /**
     * Add PropertyChangeListener.
     *
     * @param listener
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Remove PropertyChangeListener.
     *
     * @param listener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

}

class Sulama implements Comparable {

    Date baslama;
    Date bitis;


    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof Sulama) {
            Sulama s = (Sulama) obj;
            return this.baslama.compareTo(s.baslama) == 0 && this.bitis.compareTo(s.bitis) == 0;
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(Object o) {
        if (o != null && o instanceof Sulama) {
            Sulama s = (Sulama) o;
            int val = this.baslama.compareTo(s.baslama);
            return val == 0 ? this.bitis.compareTo(s.bitis) : val;
        } else {
            return 1;
        }
    }
}
