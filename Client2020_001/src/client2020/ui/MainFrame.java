/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.ui;

import client2020.chartPanel.GraphFrame;
import client2020.ui.alarm.AlarmListPanel;
import client2020.ui.tablePanel.ClientListTableModel;
import data.ActionCode;
import static data.ActionCode.*;
import data.NetworkPackage;
import db.DBConnection;
import db.MySqlServerConnection;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.EventObject;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import settings.ClientServerDBSettings;
import settings.SocketSettings;
import settings.ui.ButtonPanelEvent;
import settings.ui.SettingsPanel;
import socket.ClientSocket;
import socket.events.ClientDataEvent;
import socket.events.ClientSocketListener;

/**
 *
 * @author ISIK
 */
public class MainFrame extends javax.swing.JFrame implements ClientSocketListener {

    public final static String PROPERTIES_FOLDER_NAME = "Sera Client";
    /**
     * Creates new form MainPanel
     */

    ClientSocket socket;


    LogPanel logPanel;
    ClientListTableModel mdl;
//    GenelBakisPaneli genelBakisPaneli;
   GenelSicaklikIzlemePaneli sicaklikIzlemePaneli;
    HavuzKontrolPanel havuzKontrolPanel;
    HavuzKontrolData havuzKontrolData;
//    AllTempPanel allTempPanel;
    public final static SocketSettings SOCKET_SETTINGS;
    public final static ClientServerDBSettings DB_SETTINGS;
    public final static DBConnection DB_CONNECTION;

    static {
        SOCKET_SETTINGS = new SocketSettings();
        SOCKET_SETTINGS.load();
        DB_SETTINGS = new ClientServerDBSettings();
        DB_SETTINGS.load();
        DB_CONNECTION = new MySqlServerConnection(DB_SETTINGS);
        DBConnection.setConnectionEnabled(true);
    }

    public MainFrame() {
        
        havuzKontrolData = new HavuzKontrolData();
 
        sicaklikIzlemePaneli = new GenelSicaklikIzlemePaneli();
//        genelBakisPaneli = new GenelBakisPaneli();
        logPanel = new LogPanel();
        
        socket = new ClientSocket();
        mdl = new ClientListTableModel();
        havuzKontrolPanel = new HavuzKontrolPanel(havuzKontrolData,socket);
        
        initComponents();
        this.jCheckBoxMenuItem1.setSelected(SOCKET_SETTINGS.isAutoConnectAktif());
        this.jTabbedPane1.add(new JScrollPane(sicaklikIzlemePaneli), "Genel Bakis");
//        this.jTabbedPane1.addgenelBakisPaneli);
        this.jTabbedPane1.add("Log", logPanel);
        this.jTabbedPane1.add("Havuz Kontrol", havuzKontrolPanel);
//        this.jTabbedPane1.add(new JScrollPane(new JTable(mdl)), "Table Panel");
//        this.jTabbedPane1.add(graphPanel, "Graph Panel");
        socket.addClientSocketListener(this);
        socket.addClientSocketListener(sicaklikIzlemePaneli);
//        socket.addClientSocketListener(genelBakisPaneli);
        socket.addClientSocketListener(logPanel);
        
        if (SOCKET_SETTINGS.isAutoConnectAktif()) {
            startAutoConnectThread();
        }
        pack();
    }

    public final void startAutoConnectThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (MainFrame.this.jCheckBoxMenuItem1.isSelected()) {
                    if (!socket.isConnected()) {
                        connectServer(false);
                    }
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }).start();
    }

    public void connectServer(boolean enableWarnings) {
        this.jmConnect.setEnabled(false);

        if (socket.isConnected()) {
            this.jmDisconnect.setEnabled(true);
            if (enableWarnings) {
                JOptionPane.showMessageDialog(rootPane, "Once soket baglantisini kapatin");
            }
            return;
        }

        if (SOCKET_SETTINGS.load()) {
            this.socket.setConnectionValues(SOCKET_SETTINGS.getIp(), SOCKET_SETTINGS.getPort(), SOCKET_SETTINGS.getID());
            try {
                this.socket.start();

            } catch (Exception ex) {
                this.jmConnect.setEnabled(true);
                if (enableWarnings) {
                    JOptionPane.showMessageDialog(rootPane, "Socket baglanti hatasi Message:" + ex.getMessage());
                }
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            this.jmConnect.setEnabled(true);
            if (enableWarnings) {
                JOptionPane.showMessageDialog(rootPane, "Socket argumanlari dosyadan okunamiyor");
            }
            return;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        jmConnect = new javax.swing.JMenuItem();
        jmDisconnect = new javax.swing.JMenuItem();
        jmExit = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jmSettings = new javax.swing.JMenuItem();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jMenu2.setText("File");

        jmConnect.setText("Bağlan");
        jmConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmConnectActionPerformed(evt);
            }
        });
        jMenu2.add(jmConnect);

        jmDisconnect.setText("Bağlantıyı Kapat");
        jmDisconnect.setEnabled(false);
        jmDisconnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmDisconnectActionPerformed(evt);
            }
        });
        jMenu2.add(jmDisconnect);

        jmExit.setText("Çıkış");
        jmExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmExitActionPerformed(evt);
            }
        });
        jMenu2.add(jmExit);

        jMenuBar1.add(jMenu2);

        jMenu1.setText("View");

        jMenuItem1.setText("Show Temp. Chart");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Alarm Listesi");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu3.setText("Settings");

        jmSettings.setText("Ayarlar");
        jmSettings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmSettingsActionPerformed(evt);
            }
        });
        jMenu3.add(jmSettings);

        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText("Otomatik Bağlan");
        jCheckBoxMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem1ActionPerformed(evt);
            }
        });
        jMenu3.add(jCheckBoxMenuItem1);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        socket.terminate();
    }//GEN-LAST:event_formWindowClosing

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        // TODO add your handling code here:
     
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void jmExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmExitActionPerformed
        // TODO add your handling code here:
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }//GEN-LAST:event_jmExitActionPerformed

    private void jmSettingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmSettingsActionPerformed
        // TODO add your handling code here:
        this.setEnabled(false);
        final JFrame f = new JFrame();
        f.add(new SettingsPanel());
        f.pack();
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                f.setVisible(true);
            }
        });
        f.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                MainFrame.this.setEnabled(true);
                MainFrame.this.toFront();
            }

        }
        );
    }//GEN-LAST:event_jmSettingsActionPerformed

    private void jmDisconnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmDisconnectActionPerformed
        // TODO add your handling code here:
        this.jmDisconnect.setEnabled(false);
        socket.terminate();
    }//GEN-LAST:event_jmDisconnectActionPerformed

    private void jmConnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmConnectActionPerformed
        // TODO add your handling code here:
        connectServer(true);
    }//GEN-LAST:event_jmConnectActionPerformed

    private void jCheckBoxMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem1ActionPerformed
        // TODO add your handling code here:
        System.out.println("asdasd");
        boolean flag = jCheckBoxMenuItem1.isSelected();
        SOCKET_SETTINGS.setAutoConnectAktf(flag);
        SOCKET_SETTINGS.save();
        if (flag) {
            startAutoConnectThread();
        }

    }//GEN-LAST:event_jCheckBoxMenuItem1ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
         java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GraphFrame().setVisible(true);
            }
        });
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        JFrame iframe = new JFrame();
        iframe.add(new AlarmListPanel());
        iframe.pack();
        iframe.setVisible(true);
        iframe.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
        
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JMenuItem jmConnect;
    private javax.swing.JMenuItem jmDisconnect;
    private javax.swing.JMenuItem jmExit;
    private javax.swing.JMenuItem jmSettings;
    // End of variables declaration//GEN-END:variables

    @Override
    public void clientConnected(EventObject obj) {
        this.jmDisconnect.setEnabled(true);
        this.jmConnect.setEnabled(false);
    }

    @Override
    public void clientDisconnected(EventObject obj) {
        this.jmConnect.setEnabled(true);
        this.jmDisconnect.setEnabled(false);
    }

    @Override
    public void dataReceived(ClientDataEvent evt) {
        NetworkPackage pck = (NetworkPackage) evt.getSource();
        switch (pck.getActionCode()) {
            case AC_CLIENT_DISCONNECTED: {
//                int id = Integer.parseInt(pck.getDataArray().get(0));
//                mdl.removeClient(id);
                break;
            }
            case AC_CLIENT_CONNECTED: {
//                List<String> arr = pck.getDataArray();
//                if (!arr.isEmpty()) {
//                    int id = Integer.parseInt(arr.get(0));
//                    mdl.addData(id);
//                }
//                break;
            }
            case AC_FRIEND_CONNECTED: {

                String strID = pck.getDataArray().get(0);
                int id = Integer.parseInt(strID);
                //daha onceden baglanilip baglanti kapandiysa ve yeniden baglanildiysa eski veriler gitmemesi icin kontrol edilir
               
                break;
            }
            case AC_FRIEND_DISCONNECTED: {
                String strId = pck.getDataArray().get(0);
                int id = Integer.parseInt(strId);

                //verileri korumak icin silinmiyor
//                genelBakisPaneli.removeClientPanel(strId);
//                arduinoDataMap.remove(id);
            }

        }
//        if (ActionCode.isSensorActionCode(pck.getActionCode())) {
        if (pck.getActionCode() >= 10 && pck.getActionCode() < 20) {

        } else if (pck.getActionCode() >= 20 && pck.getActionCode() < 40) {
            List<String> dataArray = pck.getDataArray();
            if (dataArray == null && dataArray.size() != 4) {
                System.err.println("havuz kontrolden gelen data sikintili kontrol et " + dataArray);
                return;
            }
            try{
                boolean roleAktif = Integer.parseInt(dataArray.get(0))>0;
                havuzKontrolData.setRoleActive(roleAktif);
                boolean disTimerAktif = Integer.parseInt(dataArray.get(1))>0;
                havuzKontrolData.setExternalTimerEnable(disTimerAktif);
                boolean arduinoKontolAktif = Integer.parseInt(dataArray.get(2))>0;
                havuzKontrolData.setArduinoRoleKontrolEnable(arduinoKontolAktif);
                Double basinc = Double.parseDouble(dataArray.get(3));
                havuzKontrolData.setPressure(basinc);
            }catch(NumberFormatException ex){
                System.err.println("Havuz kontrolden gelen datalar parse edilemedi"+dataArray);
            }
        }
    }

    @Override
    public void dataSent(ClientDataEvent evt) {
    }

    @Override
    public void clientIdAccepted(EventObject obj) {
    }

    @Override
    public void clientIdRefused(EventObject obj) {
    }

}
