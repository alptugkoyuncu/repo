/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.ui;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author ISIK
 */
public class SulamaBilgisi {
      private final int id;
        private final Date baslamaZamani;
        private final Date bitisZamani;
        private final int none;
        private final int low;
        private final int normal;

        public SulamaBilgisi(int id, long baslama, long bitis,  int none, int low, int normal) {
            this.id = id;
            this.baslamaZamani = new Date(baslama);
            this.bitisZamani = new Date(bitis);
           
            this.none = none;
            this.low = low;
            this.normal = normal;
        }

        public int getSure() {
            return getNone()+getLow()+getNormal();
        }

        public int getNone() {
            return none;
        }

        public int getNormal() {
            return normal;
        }

        public int getLow() {
            return low;
        }
        public Date getBitisZamani() {
            return bitisZamani;
        }

        public Date getBaslamaZamani() {
            return baslamaZamani;
        }

        public int getId() {
            return id;
        }

        @Override
        public String toString() {
            SimpleDateFormat f = new SimpleDateFormat("DD-MM-YY HH:mm:ss");
            return "id : "+id+" ST:"+f.format(baslamaZamani)+" FT:"+f.format(bitisZamani)+" Sure:"+getSure()+" Basincsiz:"+none+" AzBasincli:"+low+" Basincli:"+normal;
        }
}
