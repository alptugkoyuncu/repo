/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.ui.alarm;

import db.AlanBean;
import db.BolgeBean;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author ISIK
 */
public class AlanBeanListCellRenderer implements ListCellRenderer<AlanBean> {

    @Override
    public Component getListCellRendererComponent(JList<? extends AlanBean> list, AlanBean value, int index, boolean isSelected, boolean cellHasFocus) {
        JLabel lbl = new JLabel();
        lbl.setOpaque(true);
        if (value == null) {
            lbl.setText("");
        } else {

            lbl.setText(value.getBolge().getName()+"-"+value.getName());
            lbl.setToolTipText(value.getIdclient()+ "-" + value.getActionCode());
        }
        if (isSelected) {
            lbl.setForeground(list.getSelectionForeground());
            lbl.setBackground(list.getSelectionBackground());
        } else {
            lbl.setForeground(list.getForeground());
            lbl.setBackground(list.getBackground());
        }

        return lbl;
    }

}
