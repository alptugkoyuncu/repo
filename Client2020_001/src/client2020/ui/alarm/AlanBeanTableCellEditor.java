/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.ui.alarm;

import client2020.ui.MainFrame;
import db.AlanBean;
import db.BolgeBean;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author ISIK
 */
public class AlanBeanTableCellEditor extends AbstractCellEditor
            implements TableCellEditor, ActionListener {

        List<AlanBean> list;
        AlanBean alan;

        public AlanBeanTableCellEditor(List<AlanBean> list) {
            this.list = list;
        }
        public AlanBeanTableCellEditor(){
            this.list = MainFrame.DB_CONNECTION.getAllAlan();
        }
        @Override
        public Object getCellEditorValue() {
            return this.alan;
        }
        
        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            if (value instanceof AlanBean) {
                this.alan = (AlanBean) value;
            }

            JComboBox<AlanBean> comboCountry = new JComboBox<AlanBean>();
            comboCountry.setRenderer(new AlanBeanListCellRenderer());
            for (AlanBean a : list) {
                comboCountry.addItem(a);
            }

            comboCountry.setSelectedItem(alan);
            comboCountry.addActionListener(this);

            if (isSelected) {
                comboCountry.setBackground(table.getSelectionBackground());
            } else {
                comboCountry.setBackground(table.getSelectionForeground());
            }

            return comboCountry;
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            JComboBox<BolgeBean> comboCountry = (JComboBox<BolgeBean>) event.getSource();
            this.alan = (AlanBean) comboCountry.getSelectedItem();
            this.stopCellEditing();
        }

    }
