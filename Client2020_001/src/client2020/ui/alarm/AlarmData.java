/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.ui.alarm;

import db.AlanBean;
import db.BolgeBean;
import java.util.Date;

/**
 *
 * @author ISIK
 */
class AlarmData {

    private AlanBean alan;
    private Double minValue;
    private boolean aktif;
    private Date delayTo;

    public AlarmData(){
        this.alan=null;
        this.minValue=null;
        this.aktif=false;
        this.delayTo=null;
    }
    public AlarmData(AlanBean alan, double minValue, boolean aktif, Date delayTo) {
        this.alan = alan;
        this.minValue = minValue;
        this.aktif = aktif;
        this.delayTo = delayTo;
    }

    public Double getMinValue() {
        return minValue;
    }

    public void setMinValue(Double minValue) {
        this.minValue = minValue;
    }

    public boolean isAktif() {
        return aktif;
    }

    public void setAktif(boolean aktif) {
        this.aktif = aktif;
    }

    public Date getDelayTo() {
        return delayTo;
    }

    public void setDelayTo(Date delayTo) {
        this.delayTo = delayTo;
    }

    public AlanBean getAlan() {
        return alan;
    }

    public void setAlan(AlanBean alan) {
        this.alan = alan;
    }

}
