/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2020.ui.tablePanel;

import static data.ActionCode.AC_CLIENT_CONNECTED;
import static data.ActionCode.AC_CLIENT_DISCONNECTED;
import static data.ActionCode.AC_FRIEND_CONNECTED;
import static data.ActionCode.AC_FRIEND_DISCONNECTED;
import data.NetworkPackage;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import socket.events.ClientDataEvent;
import socket.events.ClientSocketListener;

/**
 *
 * @author ISIK
 */
public class ClientListTableModel extends AbstractTableModel implements ClientSocketListener{
    List<Integer> cols;
    String colBas="Index";
    String colSon="Son Guncelleme";
    List<ArduinoData> data;
    public ClientListTableModel() {
        cols = new ArrayList<>();  
        data = new ArrayList<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ClientListTableModel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    for(int i=0; i<data.size();i++){
                        data.get(i).increaseLastUpdateSecond();
                        fireTableCellUpdated(i, getColumnCount()-1);
                    }
                }
            }
        }).start();
    }

    void setData(int hostId, Integer actionCode, String value) {
        boolean hasId=false;
        if(!cols.contains(actionCode)){
            cols.add(actionCode);
            fireTableStructureChanged();
        }
        for(int i=0; i<data.size();i++){
            if(data.get(i).id==hostId){
                data.get(i).acValueMappng.put(actionCode, value);
                data.get(i).lastUpdatedSec=0;
                fireTableRowsUpdated(i, i);
                hasId=true;
                break;
            }
        }
        
        if(!hasId){
            ArduinoData d = new ArduinoData();
            d.id=hostId;
            d.acValueMappng.put(actionCode, value);
            this.data.add(d);
            int size= this.data.size();
            this.fireTableRowsInserted(size-1, size-1);
        }
    }

    @Override
    public void clientConnected(EventObject obj) {
   }

    @Override
    public void clientDisconnected(EventObject obj) {
    }

    @Override
    public void dataReceived(ClientDataEvent evt) {
    NetworkPackage pck = (NetworkPackage) evt.getSource();
        switch (pck.getActionCode()) {
            case AC_CLIENT_DISCONNECTED: {
//                int id = Integer.parseInt(pck.getDataArray().get(0));
//                mdl.removeClient(id);
                break;
            }
            case AC_CLIENT_CONNECTED: {
//                List<String> arr = pck.getDataArray();
//                if (!arr.isEmpty()) {
//                    int id = Integer.parseInt(arr.get(0));
//                    mdl.addData(id);
//                }
//                break;
            }
            case AC_FRIEND_CONNECTED:{
                
               String strID = pck.getDataArray().get(0);
               int id = Integer.parseInt(strID);
               addClient(id);
               break;
            }
            case AC_FRIEND_DISCONNECTED:{
                String strId = pck.getDataArray().get(0);
                int id = Integer.parseInt(strId);
                //verileri korumak icin silinmiyor
//                removeClient(id);
            }

        }
        if (pck.getActionCode() < 60 && pck.getActionCode() >= 10) {
            int hostId = pck.getHostId();
            setData(hostId, pck.getActionCode(), pck.getDataArray().get(0));
           
        }
    }

    @Override
    public void dataSent(ClientDataEvent evt) {
   }

    @Override
    public void clientIdAccepted(EventObject obj) {
    }

    @Override
    public void clientIdRefused(EventObject obj) {
   }
    public class ArduinoData{
        Integer id;
        Map<Integer,String> acValueMappng;
        long lastUpdatedSec;

        
        public ArduinoData() {
            id=null;
            acValueMappng = new HashMap<>();
            lastUpdatedSec=0;
        }

        private void increaseLastUpdateSecond() {
            lastUpdatedSec++;
        }
    }
    @Override
    public int getRowCount() {
        return data.size();
    }
    public void addClient(Integer id){
        for(ArduinoData dt :data)
            if(dt.id==id)
                return;
        ArduinoData dt = new ArduinoData();
        dt.id=id;
        this.data.add(dt);
        fireTableRowsInserted(data.size()-1, data.size()-1);
    }
    public void removeClient(int id){
        for(int i=0; i<data.size();i++){
            if(data.get(i).id==id){
                this.data.remove(i);
                fireTableRowsDeleted(i, i);
                return;
            }
        }
    }
    @Override
    public int getColumnCount() {
        return cols.size()+2;
    }

    @Override
    public String getColumnName(int column) {
        if(column==0)
            return colBas;
        else if(column==getColumnCount()-1)
            return colSon;
        else{
            return cols.get(column-1)+"";
        }
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ArduinoData ad = this.data.get(rowIndex);
        if(columnIndex==0){
            return ad.id;
        }else if(columnIndex==getColumnCount()-1)
            return ad.lastUpdatedSec;
        else{
            String colName = getColumnName(columnIndex);
            int ac = Integer.parseInt(colName);
            return ad.acValueMappng.get(ac);
        }
    }
       
}
