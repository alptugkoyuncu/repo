/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package db;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author alptug
 */
public class AlanBean implements Serializable{
    private Integer idalan;
    private BolgeBean bolge;
    private String name;
    private Integer idclient;
    private Integer actionCode;

    public AlanBean(Integer idalan, BolgeBean bolge, String name, Integer idclient, Integer actionCode) {
        this.idalan = idalan;
        this.bolge = bolge;
        this.name = name;
        this.idclient = idclient;
        this.actionCode = actionCode;
    }

    public AlanBean() {
        this.idalan=null;
        this.bolge = null;
        this.name = null;
        this.idclient = null;
        this.actionCode = null;
    }

    public Integer getIdalan() {
        return idalan;
    }

    public void setIdalan(Integer idalan) {
        this.idalan = idalan;
    }

    public BolgeBean getBolge() {
        return bolge;
    }

    public void setBolge(BolgeBean bolge) {
        this.bolge = bolge;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdclient() {
        return idclient;
    }

    public void setIdclient(Integer idclient) {
        this.idclient = idclient;
    }

    public Integer getActionCode() {
        return actionCode;
    }

    public void setActionCode(Integer actionCode) {
        this.actionCode = actionCode;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.idalan);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlanBean other = (AlanBean) obj;
        return Objects.equals(this.idalan, other.idalan);
    }

    @Override
    public String toString() {
        return "AlanBean{" + "idalan=" + idalan + ", bolge=" + bolge + ", name=" + name + ", idclient=" + idclient + ", actionCode=" + actionCode + '}';
    }
}
