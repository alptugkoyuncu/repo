/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author ISIK
 */
public class BolgeBean implements Serializable{
    private Integer idbolge;
    private String name;

    public BolgeBean(Integer idbolge, String name) {
        this.idbolge = idbolge;
        this.name = name;
    }

    public BolgeBean() {
        this.idbolge = null;
        this.name = null;
    }

    public Integer getIdbolge() {
        return idbolge;
    }

    public void setIdbolge(Integer idbolge) {
        this.idbolge = idbolge;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.idbolge);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BolgeBean other = (BolgeBean) obj;
        return Objects.equals(this.idbolge, other.idbolge);
    }

    @Override
    public String toString() {
        return "BolgeBean{" + "idbolge=" + idbolge + ", name=" + name + '}';
    }
}
