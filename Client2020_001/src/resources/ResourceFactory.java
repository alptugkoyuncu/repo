/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.awt.Image;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author ISIK
 */
public class ResourceFactory {
    public static final String TOGGLE_SWITCH_ON="toggle_switch_on.png";
    public static final String TOGGLE_SWITCH_OFF="toggle_switch_off.png";
    private static Image getImage(String fileName,String description){
        URL imageURL = ResourceFactory.class.getResource(fileName);

        if (imageURL == null) {
            System.err.println("Resource not found: " + fileName);
            return null;
        } else {
            return (new ImageIcon(imageURL,description).getImage());
        }
    }
    public static Image getSwitchOnIcon(String description){
        return getImage("./images/"+TOGGLE_SWITCH_ON, description);
    }
    public static Image getSwitchOffIcon(String description){
        return getImage("./images/"+TOGGLE_SWITCH_OFF,description);
    }
}