
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ISIK
 */
public class TestDecimal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        DecimalFormatSymbols sys = new DecimalFormatSymbols(Locale.US);
        sys.setDecimalSeparator('`');
        
        DecimalFormat df = new DecimalFormat("#.#",sys);
        Double d = 1233333.33333;
        System.err.println(df.format(d));
        
    }
    
   
}
