/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Color;
import java.awt.Font;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler.LegendPosition;
import org.knowm.xchart.style.colors.ChartColor;
import org.knowm.xchart.style.colors.XChartSeriesColors;
import org.knowm.xchart.style.lines.SeriesLines;
import org.knowm.xchart.style.markers.SeriesMarkers;

/**
 *
 * @author alptug
 */
/**
 * Customized Chart
 *
 * Demonstrates the following:
 * <ul>
 * <li>
 * Extensive Chart Customization
 */
public class TestDynamicXChart extends JFrame {

    double min = -5.0;
    double max = 10.0;
    List<Date> xData = new ArrayList<Date>();
    List<Double> yData = new ArrayList<Double>();
    XChartPanel<XYChart> pnl;
    Calendar c = Calendar.getInstance();

    public TestDynamicXChart() {
        pnl = new XChartPanel<XYChart>(this.getChart());
        this.add(pnl);
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      }

    public static void main(String[] args) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TestDynamicXChart().setVisible(true);
            }
        });
        /*
    ExampleChart<XYChart> exampleChart = new LineChart03();
    XYChart chart = exampleChart.getChart();
    new SwingWrapper<XYChart>(chart).displayChart();
         */
    }

    public XYChart getChart() {

        // Create Chart
        XYChart chart = new XYChartBuilder().width(800).height(600).title("LineChart03").xAxisTitle("X").yAxisTitle("Y").build();

        // Customize Chart
        chart.getStyler().setPlotBackgroundColor(ChartColor.getAWTColor(ChartColor.GREY));
        chart.getStyler().setPlotGridLinesColor(new Color(255, 255, 255));
        chart.getStyler().setChartBackgroundColor(Color.WHITE);
        chart.getStyler().setLegendBackgroundColor(Color.PINK);
        chart.getStyler().setChartFontColor(Color.MAGENTA);
        chart.getStyler().setChartTitleBoxBackgroundColor(new Color(0, 222, 0));
        chart.getStyler().setChartTitleBoxVisible(true);
        chart.getStyler().setChartTitleBoxBorderColor(Color.BLACK);
        chart.getStyler().setPlotGridLinesVisible(false);

        chart.getStyler().setAxisTickPadding(20);

        chart.getStyler().setAxisTickMarkLength(15);

        chart.getStyler().setPlotMargin(20);

        chart.getStyler().setChartTitleFont(new Font(Font.MONOSPACED, Font.BOLD, 24));
//        chart.getStyler().setLegendFont(new Font(Font.SERIF, Font.PLAIN, 18));
//        chart.getStyler().setLegendPosition(LegendPosition.InsideSE);
//        chart.getStyler().setLegendSeriesLineLength(12);
        chart.getStyler().setAxisTitleFont(new Font(Font.SANS_SERIF, Font.ITALIC, 18));
        chart.getStyler().setAxisTickLabelsFont(new Font(Font.SERIF, Font.PLAIN, 11));
        chart.getStyler().setDatePattern("dd HH-mm");
        chart.getStyler().setDecimalPattern("#0.00");

        chart.getStyler().setYAxisMin(min);
        chart.getStyler().setYAxisMax(max);
       
        // generates linear data
        List<Double> ydata2=new ArrayList<Double>();
        double tmp = 3.0;
        double tmp2=5.0;
        for (int i = 0; i < 100; i++) {
            c.add(Calendar.MINUTE, 10);
            xData.add(c.getTime());
            if (tmp < min) {
                min = min - 5;
                chart.getStyler().setYAxisMin(min);
            }
            if (tmp > max) {
                max += 5;
                chart.getStyler().setYAxisMax(max);
            }
            yData.add(tmp);
            ydata2.add(tmp2);
            tmp = getTemp(tmp);
            tmp2 = getTemp(tmp2);
        }
//    DateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
//    Date date = null;
//    for (int i = 1; i <= 1000; i++) {
// 
//      try {
//        date = sdf.parse(i + ".10.2008");
//      } catch (ParseException e) {
//        e.printStackTrace();
//      }
//      xData.add(date);
//      yData.add(Math.random() * i);
//    }

        // Series
        XYSeries series = chart.addSeries("1001", xData, yData);

        series.setLineColor(XChartSeriesColors.BLUE);
    series.setMarkerColor(Color.BLACK);
        series.setMarker(SeriesMarkers.SQUARE);
        series.setLineStyle(SeriesLines.SOLID);
        
        XYSeries series2 = chart.addSeries("1002",xData, ydata2);
        series.setLineColor(XChartSeriesColors.GREEN);
        series2.setMarker(SeriesMarkers.SQUARE);
        series.setLineStyle(SeriesLines.SOLID);
        return chart;
    }

    private Double getTemp(double d) {
        return d + Math.random() - .5;
    }
}
