package connection;

import connection.ConnectionEvent.ConnectionMessage;
import data.DataTransferEvent;
import data.DataTransferListener;
import data.NetworkClientPackage;
import data.NetworkPackage;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import my_logger.L;
import util.*;
import data.NetworkServerPackage;
import network.ActionCode;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class ClientConnection implements Runnable, network.ActionCode, network.GeneralSystemConstant {

    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
     */
    ArrayList<DataTransferListener> dataTransferListener;
    ArrayList<ConnectionListener> connectionListener;
    Socket socket;
    DataInputStream in;
    PrintStream ps;
    private boolean connected;
    private boolean runAlways;
    private long lastReceived, lastSent;
    private String ip;
    private Integer port;
    private Short connectionID;

    public ClientConnection(String ip, Integer port, Short connectionID) {
        this.connected = false;
        this.runAlways = false;
        this.connectionID = connectionID;
        this.ip = ip;
        this.port = port;
        connectionListener = new ArrayList<ConnectionListener>();
        dataTransferListener = new ArrayList<DataTransferListener>();
    }

    public ClientConnection() {
        this(null, null, null);
    }

    public void setConnectionValues(String ip, int port, short connectionID) {
        this.connectionID = connectionID;
        this.ip = ip;
        this.port = port;
    }

    public void setConnectionID(Short connectionID) {
        this.connectionID = connectionID;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void start() {
        this.runAlways = false;
        this.connected = false;
        if (ip == null || port == null || connectionID == null) {
            L.err("Connection argumensts Hatali! ip:" + ip + " port:" + port + "Connection Type: " + connectionID);
            return;
        }
        new Thread(this).start();
    }

    private boolean sentHeadear() {
        L.debug("Start header");
        long maxWaitSecond = 60000;
        byte err = 0;
        int errTry = 10;
        long st = System.currentTimeMillis();
        short b;
        while ((System.currentTimeMillis() - st < maxWaitSecond) && errTry > 0) {
            try {
                if (in.available() > 0) {
                    b = in.readShort();
                    L.debug("Yeni data : " + b);
                    if (b == REQUEST_CLIENT_TYPE) {
                        ps.write(ByteConversion.fromShort(this.connectionID, false));
                        ps.flush();
                    } else if (b == this.connectionID) {
                        ps.write(ByteConversion.fromShort(CLIENT_TYPE_OK, false));
                        ps.flush();
                        L.debug("Header transfer completed");
                        this.fireConnectionEstablishedEvent(new ConnectionEvent(ConnectionMessage.HEADER_CHECK_OK, this));
                        return true;
                    } else {
                        ps.write(ByteConversion.fromShort(this.connectionID, false));
                        ps.flush();
                        errTry--;
                    }
                } else {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException ex) {
                        ConnectionEvent evt = new ConnectionEvent(ConnectionMessage.CONNECTION_FAILED, this);
                        evt.setDetailedMessage(ex.getMessage());
                        this.fireConnectionFailedEvent(evt);
                        L.err("Thread Hatasi\n" + ex.getMessage());
                    }
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                ConnectionEvent evt = new ConnectionEvent(ConnectionMessage.CONNECTION_FAILED, this);
                evt.setDetailedMessage(e.getMessage());
                this.fireConnectionFailedEvent(evt);
                L.err(e.getMessage());
            }

        }

        L.err("Header transfer failed");
        this.fireConnectionFailedEvent(new ConnectionEvent(ConnectionMessage.HEADER_CHECK_FAILED, this));
        return false;
//        this.connectionEstablished = true;

    }

    public void terminate() {
        this.runAlways = false;
        while (connected) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(ClientConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void run() {
        boolean flag = false;
        try {
            socket = new Socket(ip, port);
            this.fireConnectionEstablishedEvent(new ConnectionEvent(ConnectionMessage.CONNECTED_TO_SERVER, this));
            L.debug("Socket cration completed at" + ip + "\\" + port);
            flag = true;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            L.err("Socket creation failed\nip = " + ip + "\\" + port);

            ConnectionEvent evt = new ConnectionEvent(ConnectionMessage.FAILED_TO_CONNECT_SERVER, this);
            evt.setDetailedMessage(e.getMessage());
            this.fireConnectionFailedEvent(evt);
            // return false;
        }

        try {
            if (flag) {
                try {
                    in = new DataInputStream(socket.getInputStream());
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    L.err(e.getMessage());
                    ConnectionEvent evt = new ConnectionEvent(ConnectionMessage.CONNECTION_FAILED, this);
                    evt.setDetailedMessage(e.getMessage());
                    evt.setMyMessage("Data input Stream olusturulamadi");
                    flag = false;
                }
            }
            if (flag) {
                try {
                    ps = new PrintStream(socket.getOutputStream());
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    L.err(e1.getMessage());
                    ConnectionEvent evt = new ConnectionEvent(ConnectionMessage.CONNECTION_FAILED, this);
                    evt.setDetailedMessage(e1.getMessage());
                    evt.setMyMessage("Print Stream olusturulamadi");
                    flag = false;
                    // return false;
                }
            }

            if (flag) {
                L.debug("Connection Established\nWaiting for arranging connection header");
                flag = this.sentHeadear();
            }
            String cvp;


            /*
             * notice: inputStream.read() will block if no data return
             */
            data.NetworkServerPackage pckServer = new NetworkServerPackage();
            data.NetworkClientPackage pckClient = new NetworkClientPackage();
            
            if (flag) {
                lastReceived = System.currentTimeMillis();
                lastSent = 0;
                this.connected = true;
                this.runAlways = true;
                
                pckServer.setSender(this.connectionID);
                pckServer.addReceiverList(SERVER_ID);
                pckServer.setAction(ActionCode.CLIENT_CONNECTED);
                this.sent(pckServer);
            }
            while (this.runAlways && flag) {
                {
                    long curr = System.currentTimeMillis();
                    if (curr - lastSent > PING_WAIT_MS) {
                        pckServer.reset();
                        pckServer.setSender(this.connectionID);
                        pckServer.addReceiverList(SERVER_ID);
                        pckServer.setAction(SENT_PING);
                        this.sent(pckServer);
                    }
                    if (curr - lastReceived > 3 * PING_WAIT_MS) {
                        break;
                    }
                }
                if (in != null && in.available() > 0) {
                    cvp = in.readLine();
                    pckClient.setNetworkData(cvp);
                    pckClient.setReceiver(this.connectionID);
                    this.lastReceived = System.currentTimeMillis();
                    L.debug("New Data : " + pckClient);
                    this.fireDataReceivedEvent(pckClient);

                }
                 Thread t;
                 
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
//                    Logger.getLogger(WifiConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
//            e.printStackTrace();
            L.err("UnknownHostException: " + e.toString());

        } catch (IOException e) {
            // TODO Auto-generated catch block
//            e.printStackTrace();
            L.err("IOException: " + e.toString());
        }
        try {
            if (in != null) {
                in.close();
            }
        } catch (IOException ex) {
            L.err("Input Stream kapatma hatasi : " + ex.getMessage());
        }
        if (ps != null) {
            ps.close();
        }
        try {
            if (socket != null) {
                socket.close();
            }
        } catch (IOException ex) {
            L.err("Socket kapatma hatasi : " + ex.getMessage());
        }
        this.connected = false;

        this.fireConnectionFailedEvent(new ConnectionEvent(ConnectionMessage.CONNECTION_CLOSED, this));
        L.debug("Connection terminate");
    }

    public synchronized void sent(NetworkServerPackage pck) {
        if (!this.connected) {
            L.err("Print Stream null");
            return;
        }
        ps.println(pck.getNetworkData());
        L.debug("Data sent =>" + pck);
        ps.flush();
        this.lastSent = System.currentTimeMillis();
        this.fireDataSentEvent(pck);
//        super.fireEvent(DataTransferEvent.DATA_SENT);

//        ps.close();    
    }

    private void fireDataReceivedEvent(NetworkPackage pck) {

        data.DataTransferEvent evt = new DataTransferEvent(this, pck);
        for (DataTransferListener l : dataTransferListener) {
            l.dataReceived(evt);
        }
    }

    private void fireDataSentEvent(NetworkPackage pck) {
        data.DataTransferEvent evt = new DataTransferEvent(this, pck);
        for (DataTransferListener l : dataTransferListener) {
            l.dataSent(evt);
        }
    }

    private void fireConnectionEstablishedEvent(ConnectionEvent evt) {
        for (ConnectionListener l : connectionListener) {
            l.connectionEstabished(evt);
        }
    }

    private void fireConnectionFailedEvent(ConnectionEvent evt) {
        for (ConnectionListener l : connectionListener) {
            l.connectionTerminated(evt);
        }
    }

    public void addDataTransferListener(DataTransferListener l) {
        this.dataTransferListener.add(l);
    }

    public void removeDataTransferListener(DataTransferListener l) {
        this.dataTransferListener.remove(l);
    }

    public void addConnectionListener(ConnectionListener l) {
        this.connectionListener.add(l);
    }

    public void removeConnectionListener(ConnectionListener l) {
        this.connectionListener.remove(l);
    }

    public synchronized boolean isConnected() {
        return connected;
    }
}
