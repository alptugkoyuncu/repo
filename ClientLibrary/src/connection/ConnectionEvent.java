
package connection;


import java.util.EventObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class ConnectionEvent extends EventObject {

    public enum ConnectionMessage {
        CONNECTED_TO_SERVER, FAILED_TO_CONNECT_SERVER, HEADER_CHECK_OK, HEADER_CHECK_FAILED, CONNECTION_ESTABLISHED, CONNECTION_FAILED, CONNECTION_CLOSED
    }
    private ConnectionMessage message;
    private String DetailedMessage;
    private String myMessage;
    public ConnectionEvent(ConnectionMessage msg, Object source) {
        super(source);
        this.message = msg;
    }

    public ConnectionMessage getMessage() {
        return message;
    }

    public String getDetailedMessage() {
        return DetailedMessage;
    }

    public void setDetailedMessage(String DetailedMessage) {
        this.DetailedMessage = DetailedMessage;
    }

    public String getMyMessage() {
        return myMessage;
    }

    public void setMyMessage(String myMessage) {
        this.myMessage = myMessage;
    }

}
