package connection;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alptug
 */
public interface ConnectionListener {
    public void connectionEstabished(ConnectionEvent event);
    public void connectionTerminated(ConnectionEvent event);
}
