/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.Date;
import java.util.Properties;

/**
 *
 * @author alptug
 */
public class ArduinoData implements Comparable<ArduinoData>{
    private final Short id;
    private Double val1;
    private Double val2;
    private Double val3;
    private String name;
    private Date lastUpdateTime;
    public ArduinoData(Short id) {
        this.id=id;
        this.name=ClientConfigData.readProviderName(id);
        this.lastUpdateTime=new Date();
    }
    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }
    public void setLastUpdateTime(){
        this.lastUpdateTime = new Date();
    }
    public Short getId() {
        return id;
    }

    public Double getVal1() {
        return val1;
    }
    
    public void setVal1(Double val1) {
        this.val1 = val1;
        this.setLastUpdateTime();
    }

    public Double getVal2() {
        return val2;
    }

    public void setVal2(Double val2) {
        this.val2 = val2;
        this.setLastUpdateTime();
    }

    public Double getVal3() {
        return val3;
    }

    public void setVal3(Double val3) {
        this.val3 = val3;
        this.setLastUpdateTime();
    }

    public String getName() {
        return name==null ? id+"":name;
    }

    public void setName(String name) {
        this.name = name;
        ClientConfigData.saveProviderName(this.id, name);
    }
    public Date getLastUpdateTime(){
        return this.lastUpdateTime;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null)
            return false;
        ArduinoData d=(ArduinoData) obj;
        return d.getId().shortValue()==this.id;
    }

    @Override
    public int compareTo(ArduinoData o) {
        return this.id.compareTo(o.getId());
    }

}
