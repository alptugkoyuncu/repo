/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/**
 *
 * @author alptug
 */
public class ClientConfigData {

    private static String fileName = "config.properties";
    public final static String MAIN_FOLDER;
    static String ip;
    static Integer port;
    static Short clientId;
    private static boolean fileCreated = false;

    static {
        MAIN_FOLDER = new JFileChooser().getCurrentDirectory().getPath() + File.separator + "Sera Izleme";
        createFile();
    }

    private static void createFile() {
        
        File folder = new File(MAIN_FOLDER);

        if (!folder.exists()) {
            folder.mkdirs();
        }

        File f = new File(MAIN_FOLDER + File.separator + fileName);
        if (!f.exists()) {
            try {
                f.createNewFile();
                fileCreated = true;
            } catch (IOException ex) {
                fileCreated = false;
                Logger.getLogger(ClientConfigData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else
            fileCreated=true;
        if (fileCreated) {
            fileName = MAIN_FOLDER + File.separator + fileName;
        }
    }

    private ClientConfigData() {
    }

    private static Properties loadProp() {
        if (!fileCreated) {
            createFile();
        }
        if (!fileCreated) {
            System.err.println("File not created");
        }
        Properties prop = null;
        try (FileInputStream fis = new FileInputStream(fileName)) {
            prop = new Properties();
            prop.load(fis);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClientConfigData.class.getName()).log(Level.SEVERE, null, ex);
            prop = null;
        } catch (IOException ex) {
            Logger.getLogger(ClientConfigData.class.getName()).log(Level.SEVERE, null, ex);
            prop = null;
        }
        return prop;
    }

    public static SocketProp getSocketProps() {
        SocketProp sp = new SocketProp();
        Properties prop = loadProp();
        sp.setClientId(Short.parseShort(prop.getProperty(SocketProp.PROP_ID, sp.getClientId() + "")));
        sp.setIp(prop.getProperty(SocketProp.PROP_IP, sp.getIp()));
        sp.setPort(Integer.parseInt(prop.getProperty(SocketProp.PROP_PORT, sp.getPort() + "")));
        return sp;
    }

    public static boolean writeSocketProps(SocketProp sp) {
        Properties prop = loadProp();
        if (prop == null) {
            return false;
        }
        try (FileOutputStream out = new FileOutputStream(fileName)) {
            // set the properties value
            prop.setProperty(SocketProp.PROP_IP, sp.getIp());
            prop.setProperty(SocketProp.PROP_PORT, sp.getPort() + "");
            prop.setProperty(SocketProp.PROP_ID, sp.getClientId() + "");

            // save properties to project root folder
            prop.store(out, null);
            return true;
        } catch (IOException io) {
            io.printStackTrace();
        }
        return false;
    }

    public static LocalDbProp getLocalDbProp() {
        LocalDbProp lp = new LocalDbProp();
        return lp;
    }


    public static ServerDbProp getServerDbProp() {
        Properties prop = loadProp();
        ServerDbProp sp = new ServerDbProp();
        sp.setDriver(prop.getProperty(ServerDbProp.PROP_DRIVER, sp.getDriver()));
        sp.setPassword(prop.getProperty(ServerDbProp.PROP_PASSWORD, sp.getDriver()));
        sp.setUrl(prop.getProperty(ServerDbProp.PROP_URL, sp.getUrl()));
        sp.setUserName(prop.getProperty(ServerDbProp.PROP_USERNAME, sp.getUserName()));
        return sp;
    }

    public static boolean writeServerDpProp(ServerDbProp sp) {
        Properties prop = loadProp();
        if (prop == null) {
            return false;
        }
        try (FileOutputStream out = new FileOutputStream(fileName)) {
            prop.setProperty(ServerDbProp.PROP_DRIVER, sp.getDriver());
            prop.setProperty(ServerDbProp.PROP_PASSWORD, sp.getPassword());
            prop.setProperty(ServerDbProp.PROP_URL, sp.getUrl());
            prop.setProperty(ServerDbProp.PROP_USERNAME, sp.getUserName());
            prop.store(out, null);
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClientConfigData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClientConfigData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static String readProviderName(short id) {
        Properties prop = loadProp();
        if (prop == null) {
            return null;
        }
        // get the property value and print it out
        String name = prop.getProperty(id + "");

        return name;
    }

    public static void saveProviderName(short id, String name) {
        Properties prop = loadProp();
        try (FileOutputStream output = new FileOutputStream(fileName)) {
            // set the properties value
            prop.setProperty(id + "", name);
            // save properties to project root folder
            prop.store(output, null);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClientConfigData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClientConfigData.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
