/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import static data.ClientConfigData.MAIN_FOLDER;
import java.io.File;
/**
 *
 * @author alptug
 */
public class LocalDbProp {

    private final static String DRIVER_NAME ="org.sqlite.JDBC" ;
    private final static String DB_NAME = "sera.db";

    public final static String getUrl() {
        return "JDBC:sqlite:" + MAIN_FOLDER + File.separator + DB_NAME;
    }

    public static String getDriverName() {
        return DRIVER_NAME;
    }

    LocalDbProp() {
    }

}
