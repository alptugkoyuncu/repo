/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author alptug
 */
public class ServerDbProp {
    final static String PROP_DRIVER="server.db.driver";
    final static String PROP_PASSWORD="server.db.password";
    final static String PROP_URL="server.db.url";
    final static String PROP_USERNAME="server.db.username";
    private String driver;
    private String password;
    private String url;
    private String userName;

    ServerDbProp() {
    }
    
    public void resetDefault(){
        this.driver="com.mysql.jdbc.Driver";
        this.password="19791981";
        this.url="jdbc:mysql://localhost:3306/sera?useUnicode=true&characterEncoding=utf8";
        this.userName="client";
    }
    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
