/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.Properties;

/**
 *
 * @author alptug
 */
public class SocketProp {

    final static String PROP_IP = "socket.ip";
    final static String PROP_PORT = "socket.port";
    final static String PROP_ID = "socket.id";
    private String ip;
    private Integer port;
    private Short clientId;

    SocketProp() {
        resetDefault();
    }

    public void resetDefault() {
        ip = "192.168.1.11";
        port = 5555;
        clientId = 2001;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public short getClientId() {
        return clientId;
    }

    public void setClientId(short clientId) {
        this.clientId = clientId;
    }
}
