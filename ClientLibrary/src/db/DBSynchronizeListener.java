/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.util.EventListener;

/**
 *
 * @author alptug
 */
public interface DBSynchronizeListener extends EventListener{
    public void synchronizationStarted();
    public void synchronizationCompleted(long totalUpdatedRow,long totalUnsynchronizedRow);
    public void synchronizationFailed(long totalUpdatedRow,long totalUnsynchronizedRow);
    public void unsynchronizedRowCounted(long totalUnSynchronizedRow);
    public void synchronizedRowUpdated(short clientId,long clientSynchronizedRow,long clientKalanRow);
}
