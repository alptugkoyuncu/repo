package db;

import data.LocalDbProp;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import my_logger.L;
import network.ActionCode;
import network.GeneralSystemConstant;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class LocalDbConnection {

    
    private final static DBDateRecordInterval recordInteval = DBDateRecordInterval.PER_MINUTE;

    static {
        createLocalDb();

    }

    private LocalDbConnection() {

    }

    static Connection openConnection() {
        try {
//            Class.forName(LocalDbProp.getDriverName());
            Connection conn = DriverManager.getConnection(LocalDbProp.getUrl());
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                L.debug("The driver name is " + meta.getDriverName());
            }
            return conn;
        } catch (SQLException e) {
            L.err(e.getMessage());
        }
        return null;
    }
//    public static boolean 

    public static boolean insertData(data.NetworkClientPackage pck) {
        //package kayita uygun degilse cikis
        if (pck == null || pck.getSender() == GeneralSystemConstant.SERVER_ID || pck.getAction() == null || pck.getData() == null || pck.getData().isEmpty()) {
            return false;
        }
        Short providerId = pck.getSender();
        return insertData(providerId, pck.getAction(), pck.getData().get(0), pck.getCreationTime().getTime());
    }

    private static boolean isLocalDbRecord(short actionCode) {
        switch (actionCode) {
            case ActionCode.SENT_TEMP1:
            case ActionCode.SENT_TEMP2:
            case ActionCode.SENT_TEMP3:
                return true;
            default:
                return false;
        }
    }

    public static String getVersion() {
        Connection conn = LocalDbConnection.openConnection();
        String ver = "Version : ";
        try (Statement st = conn.createStatement()) {
            ResultSet rs = st.executeQuery("Select sqlite_version()");
            if (rs.next()) {
                ver += rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(LocalDbConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ver;
    }

    public static List readclientActionValue(Integer maxRow, Date minimumDate, Date maximumDate) {
        String limit = "";
        if (maxRow != null) {
            limit = "Limit " + maxRow;
        }
        String where = "";
        if (minimumDate != null) {
            where = "where createdAt > " + minimumDate.getTime();
            if (maximumDate != null) {
                where += " and createdAt<= " + maximumDate.getTime();
            }
        } else {
            if (maximumDate != null) {
                where = " where createdAt<= " + maximumDate.getTime();
            }
        }

        List<ArrayList> list = new ArrayList<ArrayList>();
        Connection conn = openConnection();
        if (conn == null) {
            return null;
        }
        String query = "Select idClient,actionCode,value,createdAt from client_action_value " + where + " " + limit;
        try (Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(query);) {
            ArrayList l;
            while (rs.next()) {
                l = new ArrayList();
                l.add(rs.getShort("idClient"));
                l.add(rs.getShort("actionCode"));
                l.add(rs.getLong("createdAt"));
                list.add(l);
            }
        } catch (SQLException ex) {
            Logger.getLogger(LocalDbConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(LocalDbConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return list;
    }

    /**
     * providerId actıonCode creationTime satırından birebir aynısı olursa
     * databasede ekleme yerine güncelleme yapılır. Value değeri en düşük olan
     * değer database de güncellenir. Normal zamanda hiçbir zaman yukarıdaki
     * üçlüden database bulunamazken record interval değerini yüksek tutarsak
     * Örneğin 1 dk gibi database'e değerler 1dklık olarak yuvarlanır 1 dk
     * içinde gelen birden fazla aynı providerId ile actinCodelu değerlerin
     * creatinTimeları eşitleneceği için birden fazla row oluşmuş olur. Bu
     * durumda en düşük value değeri satırda güncellenir.Bu sayede db deki veri
     * miktarından hatıra sayılır bir azalma olur. recordInterval değeri başta
     * final statik olarak belirlenmiştir ve 1dk aralıklıdır.
     *
     * @param providerId
     * @param actionCode
     * @param val
     * @param creationTime
     * @return ekleme işlemi başarılı olursa true başarısız olursa false
     * gönderir
     */
    public static boolean insertData(short providerId, short actionCode, double val, long creationTime) {
        if (!LocalDbConnection.isLocalDbRecord(actionCode)) {
            return false;
        }
        Connection conn = openConnection();
        if (conn == null) {
            return false;
        }
        boolean flag = false;
        DecimalFormatSymbols sym = new DecimalFormatSymbols();
        sym.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.##", sym);
        String sql = "INSERT INTO client_action_value(idClient,actionCode,value,createdAt)"
                + " VALUES ("
                + providerId + ","
                + actionCode + ","
                + df.format(val) + ","
                + recordInteval.convert(creationTime) + ")"
                + " ON CONFLICT(idClient,actionCode,createdAt) "
                + " do update set "
                + " value = min(" + df.format(val) + ","
                + " excluded.value)";
//        Calendar c = Calendar.getInstance();
//        c.setTime(creationTime);
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try (Statement st = conn.createStatement();) {
//            conn.setAutoCommit(false);
//            st.executeUpdate(sql);
            st.addBatch(sql);
            int[] i = st.executeBatch();
            if (i[0] >= 0) {
                flag = true;
            }
//                        conn.commit();
//conn.setAutoCommit(true);
        } catch (SQLException e) {
            System.out.println(sql + "\n" + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(LocalDbConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return flag;
    }

    public static boolean insertData(short idClient, short actionCode, double val) {
        return insertData(idClient, actionCode, val, new Date().getTime());
    }

    private static void createLocalDb() {
        // SQLite connection string

        String sql[] = {
            "PRAGMA foreign_keys = off;",
            "BEGIN TRANSACTION;",
            //-- Table: client_action_value
            "CREATE TABLE IF NOT EXISTS client_action_value (idClient INT (2) NOT NULL, actionCode INT (2) NOT NULL, value DOUBLE NOT NULL, createdAt BIGINT NOT NULL);",
            //-- Table: senkronizasyon
            "CREATE TABLE IF NOT EXISTS senkronizasyon (idSenkronizasyon INTEGER PRIMARY KEY AUTOINCREMENT, idClient INT (2) NOT NULL UNIQUE ON CONFLICT REPLACE, createdAt BIGINT NOT NULL);",
            //-- Index: createdDesc
            "CREATE INDEX IF NOT EXISTS createdDesc ON client_action_value (createdAt DESC);",
            //-- Index: unique1
            "CREATE UNIQUE INDEX IF NOT EXISTS unique1 ON client_action_value (idClient, createdAt, actionCode);",
            "COMMIT TRANSACTION;",
            "PRAGMA foreign_keys = on;"
        };
        // SQL statement for creating a new table
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = openConnection();
            if (conn == null) {
                return;
            }
            stmt = conn.createStatement();
            for (String s : sql) {
                stmt.execute(s);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(LocalDbConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
