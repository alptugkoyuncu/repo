/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import data.ClientConfigData;
import data.ServerDbProp;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alptug
 */
public class ServerDbSnkronizasyon implements Runnable {

    public final static int TABLE_SYNCHRONIZING = 0;
    public final static int TABLE_NOT_SYNCHRONIZED = 1;
    public final static int TABLE_SYNCHRONIZED = 2;

    private static boolean synchronizing;
    private Long unsynchronizedRows;
    private Long senkronizedRows;
    Set<Short> providers;
    private Set<DBSynchronizeListener> listeners;

    static {
        synchronizing = false;
    }

    public ServerDbSnkronizasyon(Set<Short> providers) {
        listeners = new HashSet<DBSynchronizeListener>();
        this.unsynchronizedRows = -1L;
        this.senkronizedRows = -1L;
        this.setProviders(providers);
    }

    public synchronized void addDBSynchronizeListener(DBSynchronizeListener l) {
        this.listeners.add(l);
    }

    public synchronized void removeDBSynchronizeListener(DBSynchronizeListener l) {
        this.listeners.remove(l);
    }

    private void fireSynchronizationStarted() {
        for (DBSynchronizeListener l : listeners) {
            l.synchronizationStarted();
        }
    }

    private void fireUnsynchronizedRowCounted(long rowCount) {
        for (DBSynchronizeListener l : listeners) {
            l.unsynchronizedRowCounted(rowCount);
        }
    }

    private void fireRowSynchronized(short clientId, long synchronizedRow, long clientUnsynchronizedRow) {
        for (DBSynchronizeListener l : listeners) {
            l.synchronizedRowUpdated(clientId, synchronizedRow, clientUnsynchronizedRow);
        }
    }

    private void fireSynchronizationCompleted(long totalUpdatedRow, long totalUnsynchronizedRow) {
        for (DBSynchronizeListener l : listeners) {
            l.synchronizationCompleted(totalUpdatedRow, totalUnsynchronizedRow);
        }
    }

    private void fireSynchronizationFailed(long totalUpdatedRow, long totalUnsynchronizedRow) {
        for (DBSynchronizeListener l : listeners) {
            l.synchronizationFailed(totalUpdatedRow, totalUnsynchronizedRow);
        }
    }

    public void setProviders(Set<Short> providers) {
        if (providers == null || providers.isEmpty()) {
            throw new NullPointerException("Provider null veya boş olamaz");
        }
        this.providers = providers;
    }

    /**
     * server database ile senkronizasyona baslar.Her clientid icin ayri sorgu
     * gonderilip ayri ayri senkronize edilir senkronizasyonu da senkronizasyon
     * da kaydeder. (son senkronize edilen datadan clientid ve created_date
     * verileri ile) yeni bir senkronizasyona baslandiginda en son kayit edilen
     * senkronizasyon tarihinden baslandigi icin eski tarihte eksik veri varsa
     * kontrol edilmez.
     *
     */
    public void startSenk() {
        new Thread(this).start();
    }

    public static synchronized boolean isSynchronizing() {
        return synchronizing;
    }

    private Connection openConnection() {
        Connection conn = null;
        ServerDbProp sp = ClientConfigData.getServerDbProp();
        try {
            Class.forName(sp.getDriver()).newInstance();
            try {
                conn = DriverManager.getConnection(sp.getUrl(), sp.getUserName(), sp.getPassword());
                return conn;
            } catch (SQLException ex) {
                Logger.getLogger(ServerDbSnkronizasyon.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ServerDbSnkronizasyon.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ServerDbSnkronizasyon.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ServerDbSnkronizasyon.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    private synchronized void setSynchronizing(boolean synchronizing) {
        ServerDbSnkronizasyon.synchronizing = synchronizing;
    }

    /**
     * @return -1 başka bi yerde işlem yapılıyorsa yoksa senkronize edilmemiş
     * row döner
     *
     */
    public long getUnSynchronizedRows() throws Exception {
        if (isSynchronizing()) {
            return -1;
        }
        this.setSynchronizing(true);
        Map<Short, Long> providerUpdateMap = this.getProviderUpdateMap();
        String q;

        Connection conn = openConnection();
        if (conn == null) {
            throw new Exception("Server db connection is null");
        }
        long rows = 0L;
        try (Statement st = conn.createStatement()) {
            for (Short pid : providers) {
                q = "Select count(*) from client_action_value where idClient= " + pid;
                if (providerUpdateMap.containsKey(pid)) {
                    q += " and createdAt>" + providerUpdateMap.get(pid);
                }

                try (ResultSet res = st.executeQuery(q)) {
                    if (res.next()) {
                        rows += res.getLong(1);
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServerDbSnkronizasyon.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.setSynchronizing(false);
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(ServerDbSnkronizasyon.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rows;
    }

    private Map<Short, Long> getProviderUpdateMap() {

        Map<Short, Long> providerUpdateMap = new HashMap<Short, Long>();
        Connection lconn = LocalDbConnection.openConnection();
        //birden fazla kayit varsa son kayiti al
        String q = "Select idClient,createdAt from senkronizasyon order by createdAt desc";
        try (Statement st = lconn.createStatement(); ResultSet set = st.executeQuery(q)) {
            while (set.next()) {
                Short id = set.getShort(1);
                Long senkDate = set.getLong(2);
                providerUpdateMap.put(id, senkDate);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServerDbSnkronizasyon.class.getName()).log(Level.SEVERE, null, ex);
        }
        return providerUpdateMap;
    }

    /**
     *
     * @deprecated StartSenk() fonksiyonunda thread oluşturulup thread bu
     * fonksiyonu çağırır. bu fonksiyonun doğruda çağrılması tavsiye edilmez.
     */
    @Deprecated
    @Override
    public void run() {

        this.fireSynchronizationStarted();
        String q;
        Map<Short, Long> providerUpdateMap = getProviderUpdateMap();
        try {
            this.unsynchronizedRows = getUnSynchronizedRows();
            this.fireUnsynchronizedRowCounted(this.unsynchronizedRows);
        } catch (Exception ex) {
            Logger.getLogger(ServerDbSnkronizasyon.class.getName()).log(Level.SEVERE, null, ex);
            this.setSynchronizing(false);
            this.fireSynchronizationFailed(-1, -1);
            return;
        }
        if (this.unsynchronizedRows == 0) {
            this.setSynchronizing(false);
            this.fireSynchronizationCompleted(0, 0);
            return;
        } else {
            while (isSynchronizing()) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ServerDbSnkronizasyon.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            this.setSynchronizing(true);
        }
//        //tablonun boş olmasi ihtimaline karsi her data sifirlanir
//        for (Short providerId : providers) {
//            providerUpdateMap.put(providerId, 0L);
//        }

        Connection conn = openConnection();
        if (conn == null) {
            throw new NullPointerException("Server db connection is null");
        }
        Long startIndex = 0L;
        this.senkronizedRows = 0L;
        DecimalFormatSymbols sym = new DecimalFormatSymbols();
        sym.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.##", sym);
        Connection lconn = LocalDbConnection.openConnection();

        try (Statement server_st = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                Statement local_st = lconn.createStatement(); //                    PreparedStatement l_ps = lconn.prepareStatement("INSERT OR IGNORE INTO client_action_value(idClient,actionCode,value,createdAt) VALUES(?,?,?,?)");
                ) {
            int limit = 1000;
            for (Short pid : providers) {
                this.senkronizedRows += startIndex;
                long maxDt = 0;
                while (true) {
                    String sq = "Select actionCode,value,createdAt From client_action_value ";
                    String whereClause = " Where idClient = " + pid;
                    if (providerUpdateMap.containsKey(pid)) {
                        maxDt=providerUpdateMap.get(pid);
                        whereClause += " and createdAt > " +maxDt;
                        
                    }
                    String orderByCaluse = " ORDER BY createdAt ASC ";
                    String limitClause = " LIMIT 0," + limit;
                    ResultSet sSet = server_st.executeQuery(sq + whereClause + orderByCaluse + limitClause);
                    

                    String sql = "INSERT OR IGNORE INTO client_action_value(idClient,actionCode,value,createdAt) VALUES (" + pid + ",";

                    if (sSet.next()) {
                        lconn.setAutoCommit(false);
                        do {
                            long dt = sSet.getLong("createdAt");
                            if (maxDt < dt) {
                                maxDt = dt;
                            }
                            local_st.addBatch(sql + sSet.getShort("actionCode") + "," + df.format(sSet.getDouble("value")) + "," + dt + ")");

                        } while (sSet.next());

                        local_st.addBatch("INSERT INTO senkronizasyon(idClient,createdAt) VALUES(" + pid + "," + maxDt + ")");
                        int rows[] = local_st.executeBatch();
                        int row=0;
                        for (int i=0; i<rows.length-1;i++) {
                            if (rows[i]>0) {
                                row++;
                            }
                        }
                        this.senkronizedRows+=row;
                        lconn.commit();
                        lconn.setAutoCommit(true);
                        providerUpdateMap.put(pid, maxDt);
                        this.fireRowSynchronized(pid, row, this.unsynchronizedRows-this.senkronizedRows);
                    }else
                        break;
                }
            }
            this.fireSynchronizationCompleted(this.senkronizedRows, this.unsynchronizedRows - this.senkronizedRows);
        } catch (SQLException ex) {
            Logger.getLogger(ServerDbSnkronizasyon.class.getName()).log(Level.SEVERE, null, ex);
            this.fireSynchronizationFailed(this.senkronizedRows, this.unsynchronizedRows - this.senkronizedRows);
        } finally {
            setSynchronizing(false);
        }

        try {
            lconn.close();
        } catch (SQLException ex) {
            Logger.getLogger(ServerDbSnkronizasyon.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(ServerDbSnkronizasyon.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private long bigDataSynchronize() {
        throw new UnsupportedOperationException("Method for processing BIG amount of rows");
    }

    private long smallDataSynchronize() {
        throw new UnsupportedOperationException("Method for processing SMALL amount of rows");
    }
}
