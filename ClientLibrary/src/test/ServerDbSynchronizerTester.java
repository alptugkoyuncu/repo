/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import db.DBSynchronizeListener;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alptug
 */
public class ServerDbSynchronizerTester implements db.DBSynchronizeListener{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ServerDbSynchronizerTester tester = new ServerDbSynchronizerTester();
        Set<Short> p = new HashSet<Short>();
        p.add((short)1001);
        p.add((short)1002);
        p.add((short)1003);
        db.ServerDbSnkronizasyon s = new db.ServerDbSnkronizasyon(p);
        s.addDBSynchronizeListener(tester);
//        try {
//            System.out.println("Senkronize edilecek dosya : "+s.getUnSynchronizedRows());
//        } catch (Exception ex) {
//            Logger.getLogger(ServerDbSynchronizerTester.class.getName()).log(Level.SEVERE, null, ex);
//        }
        s.startSenk();
    }
        @Override
        public void synchronizationStarted() {
            System.out.println("Senkronizasyona baslandi");
        }

        @Override
        public void synchronizationCompleted(long totalUpdatedRow, long totalUnsynchronizedRow) {
            System.out.println("Synchronization Completed(Updated Row: "+totalUpdatedRow+"Unsynchronized Row : "
                    +totalUnsynchronizedRow+")");
        }

        @Override
        public void synchronizationFailed(long totalUpdatedRow, long totalUnsynchronizedRow) {
            System.out.println("Senkronizasyuon failed : senkronize edilen : "+totalUpdatedRow+" Kalan :  "+totalUnsynchronizedRow);
        }

        @Override
        public void unsynchronizedRowCounted(long totalUnSynchronizedRow) {
            System.out.println("Senkronize edilecek row count:" +totalUnSynchronizedRow);
        }

        @Override
        public void synchronizedRowUpdated(short clientId, long clientSynchronizedRow, long clientKalanRow) {
            System.out.println("Senkronize edildi : client id :"+clientId + " rows: "+clientSynchronizedRow+"\\"+clientKalanRow);
        }
        
    
}
