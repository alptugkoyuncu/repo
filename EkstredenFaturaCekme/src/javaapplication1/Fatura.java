/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author alptu
 */
public class Fatura {

    java.util.Date tarih;
    String firmaAdi;
    String faturaNo;
    Double fiyat;

    public Fatura(java.util.Date tarih, String name, String faturaNo, Double fiyat) {
        this.tarih = tarih;
        this.firmaAdi = name.trim();
        this.faturaNo = faturaNo.trim();
        this.fiyat = fiyat;
    }

    public Date getTarih() {
        return tarih;
    }

    public String getFirmaAdi() {
        return firmaAdi;
    }

    public String getFaturaNo() {
        return faturaNo;
    }

    public Double getFiyat() {
        return fiyat;
    }

    @Override
    public boolean equals(Object obj) {
            Fatura f = (Fatura) obj;
            return this.getFirmaAdi().equalsIgnoreCase(f.getFirmaAdi()) && this.getFaturaNo().equalsIgnoreCase(f.getFaturaNo())&& f.getTarih().compareTo(tarih)==0&& Objects.equals(f.getFiyat(), fiyat);
        
    }

}
