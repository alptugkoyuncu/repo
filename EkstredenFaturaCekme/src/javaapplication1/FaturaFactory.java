/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author alptu
 */
public class FaturaFactory {

    private final static String className = "FaturaFactory";
    List<String> tokens;
    private final String tarihAyiraci;
    private final int[] tarihTokenleri;
    private final int gunSirasi;
    private final int aySirasi;
    private final int yilSirasi;
    private final int[] fautaNoTokenleri;
    private final int[] firmaAdiTokenleri;
    private final int fiyatTokeni;
    private final String fiyatRegex;
    private final char fiyatFloatAyiraci;
    private final static char SYSTEM_FLOAT_AYIRACI = '.';

    public FaturaFactory(String tarihAyiraci, int[] tarihTokenleri, int gunSirasi, int aySirasi, int yilSirasi, int[] fautaNoTokenleri, int[] firmaAdiTokenleri, int fiyatTokeni, char fiyatFloatAyiraci, String fiyatRegex) {
        this.tarihAyiraci = tarihAyiraci;
        this.tarihTokenleri = tarihTokenleri;
        this.gunSirasi = gunSirasi;
        this.aySirasi = aySirasi;
        this.yilSirasi = yilSirasi;
        this.fautaNoTokenleri = fautaNoTokenleri;
        this.firmaAdiTokenleri = firmaAdiTokenleri;
        this.fiyatTokeni = fiyatTokeni;
        this.fiyatRegex = fiyatRegex;
        this.fiyatFloatAyiraci = fiyatFloatAyiraci;
    }

    public Fatura createFatura(String line) {
        this.crateTokens(line);
        return new Fatura(this.getDate(), this.getFirmaAdi(), this.getFaturaNo(), this.getFiyatWithoutRegex());
    }

    private void crateTokens(String line) {
        String[] tmps = line.split(" ");
        tokens = new ArrayList<>();
        for (String tmp : tmps) {
            if (!tmp.isBlank()) {
                tokens.add(tmp);
            }
        }
    }

    private java.util.Date getDate() {
        String methodName = "Process Date";
        String tarih = "";
        for (int index : tarihTokenleri) {
            if (index < 0) {
                index += tokens.size();
            }
            tarih += tokens.get(index);
        }
        String[] tarihTokens = tarih.split(tarihAyiraci);
        int gun, ay, yil;
        try {
            gun = Integer.parseInt(tarihTokens[gunSirasi]);
        } catch (Exception ex) {
            System.err.println(className + " " + methodName + " : gun parse hatasi");
            gun = 1;
        }
        try {
            ay = Integer.parseInt(tarihTokens[aySirasi]);
        } catch (Exception ex) {
            System.err.println(className + " " + methodName + " : ay parse hatasi");
            ay = 0;
        }
        try {
            yil = Integer.parseInt(tarihTokens[yilSirasi]);
        } catch (Exception ex) {
            System.err.println(className + " " + methodName + " : yil parse hatasi");
            yil = 1990;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(Calendar.MONTH, ay - 1);
        cal.set(Calendar.YEAR, yil);
        cal.set(Calendar.DATE, gun);
        return cal.getTime();
    }

    private String getFirmaAdi() {
        String firmaAdi = "";
        for (int index : firmaAdiTokenleri) {
            if (index < 0) {
                index += tokens.size();
            }
            firmaAdi += tokens.get(index) + " ";
        }
        return firmaAdi.trim();
    }

    private String getFaturaNo() {
        String faturaNo = "";
        for (int index : fautaNoTokenleri) {
            if (index < 0) {
                index += tokens.size();
            }
            faturaNo += tokens.get(index) + " ";
        }
        return faturaNo.trim();
    }

    private Double getFiyatWithoutRegex() {
        String methodName = "getFiyatWithoutRegex";
        int index = fiyatTokeni < 0 ? fiyatTokeni + tokens.size() : fiyatTokeni;
        String fiyat = tokens.get(index);
        String tmp = "";
        boolean findfiyatFloatAyiraci = false;
        for (int i = fiyat.length() - 1; i >= 0; i--) {
            char ch = fiyat.charAt(i);
            if (ch >= '0' && ch <= '9') {
                tmp = ch+tmp;
            } else {
                if (findfiyatFloatAyiraci == false) {
                    if (ch == '.' || ch == ',') {
                        findfiyatFloatAyiraci=true;
                        tmp=SYSTEM_FLOAT_AYIRACI+tmp;
                    }
                }
            }
        }
        try {
            return (Double.parseDouble(tmp));
        } catch (NumberFormatException ex) {
            System.err.println(className + " " + methodName + " : fiyat stringi double cevrilemedi Parse edilemeyen String: " + tmp);
            return null;
        }
    }
}
