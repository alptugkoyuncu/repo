package GubreKayit;

import gubrehesaplama.AbstractKarisimGubre;
import main.GubreFormPanel;
import gubrehesaplama.GubreFormu;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class GubreKayitFrame extends javax.swing.JFrame {

    EnumMap<GubreFormu, GubreFormPanel> formListesi;
    AbstractKarisimGubre gubre;
    JTextField txtKilo;
    JTextField txtSulama;
    JTextField txtYer;
    JTextArea txtEkstra;
    JFileChooser jfc;
    JButton btnKaydet, btnReset;
    int errCode;

    /**
     * Creates new form GubreKayitFrame
     */
    public GubreKayitFrame(AbstractKarisimGubre gubre) {
        btnKaydet = new JButton("Kaydet");
        btnReset = new JButton("Reset");

        this.gubre = gubre;
        txtKilo = new JTextField(5);
        txtKilo.setName("Kilo");
        txtSulama = new JTextField(5);
        txtSulama.setName("Su Miktari(dakika/dekar)");
        txtYer = new JTextField(5);
        txtYer.setName("Yer");

        txtEkstra = new JTextArea(2, 10);
        txtEkstra.setName("Ekstra");

        this.setLayout(new BorderLayout());
        JPanel northPanel = new JPanel();

        TitledComponent tc;
        tc = new TitledComponent(txtYer);
        northPanel.add(tc);

        tc = new TitledComponent(txtKilo);
        northPanel.add(tc);

        tc = new TitledComponent(txtSulama);
        northPanel.add(tc);

        tc = new TitledComponent(txtEkstra);
        northPanel.add(tc);
        this.add(BorderLayout.NORTH, northPanel);

        JPanel centerPanel = new JPanel();
        formListesi = new EnumMap<>(GubreFormu.class);
        for (GubreFormu gf : GubreFormu.values()) {
            GubreFormPanel pnl = new GubreFormPanel(gf);
            formListesi.put(gf, pnl);
            centerPanel.add(pnl);
        }
        this.add(BorderLayout.CENTER, centerPanel);

        JPanel southPanel = new JPanel(new FlowLayout());
        southPanel.add(btnReset);
        southPanel.add(btnKaydet);
        this.add(BorderLayout.SOUTH, southPanel);

        btnReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GubreKayitFrame.this.reset();
            }
        });
        btnKaydet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean flag = GubreKayitFrame.this.kaydet();
                String sonuc = "Kayit " + (flag ? "Basarili" : "Basarisiz(" + errCode + ")");
                JOptionPane.showMessageDialog(rootPane, sonuc);

            }
        });
        this.reset();
        this.pack();

    }

    private boolean kaydet() {
        jfc = new JFileChooser();
        int opt = jfc.showDialog(this.getRootPane(), "Aç");
        File f;
        switch (opt) {
            case JFileChooser.APPROVE_OPTION:
                f = jfc.getSelectedFile();
                break;
            default:
                errCode = 1;
                return false;
        }
        if (this.txtYer.getText().trim().equalsIgnoreCase("")) {
            errCode = 2;
            return false;
        }

        String tKilo = this.txtKilo.getText();
        tKilo=tKilo.replace(',', '.');
        double kilo;
        try {
            kilo = Double.parseDouble(tKilo);
        } catch (NumberFormatException ex) {
            errCode = 3;
            return false;
        }
        String oran = "";
        for (GubreFormPanel pnl : this.formListesi.values()) {
            oran += pnl.getGubreFormu() + "=" +Math.round(pnl.getIstenenDeger())  + ";";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String tarih = sdf.format(new java.util.Date());
        String yer = this.txtYer.getText().trim();
        String sulama = this.txtSulama.getText().trim();
        if (sulama.equalsIgnoreCase("")) {
            sulama = "-";
        }
        String extra = this.txtEkstra.getText().trim();
        if (extra.equalsIgnoreCase("")) {
            extra = "-";
        }
        String line = tarih + " "+yer + " " +Math.round(kilo) + " " + oran + " " + sulama + " " + extra + System.lineSeparator();

        try (FileWriter fw = new FileWriter(f, true)) {
            fw.append(line);
            fw.close();
        } catch (IOException ex) {
            errCode = 4;
            Logger.getLogger(GubreKayitFrame.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    private void reset() {
        this.txtEkstra.setText("");
        this.txtSulama.setText("");
        this.txtKilo.setText("");
        for (GubreFormPanel pnl : this.formListesi.values()) {
            pnl.setIstenenDeger(0);
        }
        if (this.gubre != null) {
            this.txtKilo.setText(String.format("%1$.2f", this.gubre.getKarisimKilosu()));
            for (GubreFormu gf : this.gubre.getKarisimGubreFormlari()) {
                this.formListesi.get(gf).setIstenenDeger(gubre.getKarisimGubreFormOrani(gf));
            }
        }
    }

    class TitledComponent extends JPanel {

        Component c;

        public TitledComponent(Component c) {
            this.setLayout(new FlowLayout(0, 0, 0));
            this.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), c.getName()));
            this.add(c);
            this.setToolTipText(c.getName());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 420, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 322, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
