/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gubrehesaplama;

/**
 *
 * @author alptug
 */
public abstract class AbstractGubre {

    public enum GubreType {
        karisim, azotlu, fosfatli, potasli, magnezyumlu, kalsiyumlu
    };
    String name;
    GubreType type;

    public AbstractGubre(String name) {
        this.name=name;
    }
    public void setType(GubreType type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GubreType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public abstract double getIcerikOrani(GubreFormu gf);
    public abstract GubreFormu[] getIcerik();
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AbstractGubre) {
            return ((AbstractGubre) obj).getName().equalsIgnoreCase(this.getName());
        }
        return false; //To change body of generated methods, choose Tools | Templates.
    }
}
