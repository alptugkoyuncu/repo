/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gubrehesaplama;

/**
 *
 * @author alptug
 */
public abstract class AbstractKarisimGubre {
    private double karisimKilosu;

    public void setKarisimKilosu(double karisimKilosu) {
        this.karisimKilosu = karisimKilosu;
    }

    public double getKarisimKilosu() {
        return karisimKilosu;
    }
    public abstract void setKarisimGubreFormu(GubreFormu gf, double oran);
    public abstract double getKarisimGubreFormOrani(GubreFormu gf);
    public abstract GubreFormu[] getKarisimGubreFormlari();
    
    public abstract void addGubre(AbstractGubre g,double miktar);
    public abstract AbstractGubre[] getKullanilanGubreler();
    public abstract double getKullanilanGubreMiktari(AbstractGubre g);
    
    public abstract void karisimiHazirla();
}
