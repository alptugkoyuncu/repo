/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gubrehesaplama;

import java.util.EnumMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author alptug
 */
public class AdvancedKarisimGubre extends AbstractKarisimGubre {

    private final static double MAX_FORM_ORAN_SAPMA = 0.01;
    Set<HazirlananGubre> gubreListesi;
    //istenen GubreFormu orani ve istenen kilonun çarpımına eşittir
    EnumMap<GubreFormu, Double> istenenFormMiktari;
    //koyulan gubrelerin form orani ve kilo çarpımına eşittir
    EnumMap<GubreFormu, Double> hazirlananFormMiktari;

    public AdvancedKarisimGubre() {
        this.gubreListesi = new HashSet<>();
        istenenFormMiktari = new EnumMap<>(GubreFormu.class);
        hazirlananFormMiktari = new EnumMap<>(GubreFormu.class);
        this.setKarisimKilosu(1);
    }

    @Override
    public void setKarisimGubreFormu(GubreFormu gf, double oran) {
        oran = oran * this.getKarisimKilosu();
        this.istenenFormMiktari.put(gf, oran);
    }

    @Override
    public void setKarisimKilosu(double karisimKilosu) {
        if (karisimKilosu < 0) {
            super.setKarisimKilosu(1); //To change body of generated methods, choose Tools | Templates.
        } else {
            for (GubreFormu gf : this.istenenFormMiktari.keySet()) {
                double newVal = karisimKilosu * this.istenenFormMiktari.get(gf) / this.getKarisimKilosu();
                this.istenenFormMiktari.put(gf, newVal);
            }
            super.setKarisimKilosu(karisimKilosu);
        }
    }

    @Override
    public double getKarisimGubreFormOrani(GubreFormu gf) {
        return this.hazirlananFormMiktari.get(gf) / this.getKarisimKilosu();
    }

    @Override
    public GubreFormu[] getKarisimGubreFormlari() {
        return this.hazirlananFormMiktari.keySet().toArray(new GubreFormu[0]);
    }

    @Override
    public void addGubre(AbstractGubre g, double miktar) {
        this.addGubre(g, miktar, miktar > 0);
    }

    public void addGubre(AbstractGubre g, double miktar, boolean isFixed) {
        miktar = miktar > 0.0 ? miktar : 0.0;
        gubreListesi.add(new HazirlananGubre(g, miktar, isFixed));
    }

    @Override
    public AbstractGubre[] getKullanilanGubreler() {
        AbstractGubre[] list = new AbstractGubre[this.gubreListesi.size()];
        int i = 0;
        for (HazirlananGubre hg : this.gubreListesi) {
            list[i++] = hg.getGubre();
        }
        return list;
    }

    @Override
    public double getKullanilanGubreMiktari(AbstractGubre g) {
        for (HazirlananGubre hg : this.gubreListesi) {
            if (hg.getGubre().equals(g)) {
                return hg.miktar;
            }
        }
        return 0;
    }

// istenilen herhangi bir gubre formu icin tek bir gubre varsa geri dondur 
    private HazirlananGubre firstProcess(GubreFormu gf) {
        HazirlananGubre t = null;
        for (HazirlananGubre hg : gubreListesi) {
            if (hg.isFinished()) {
                continue;
            }
            for (GubreFormu f : hg.getGubre().getIcerik()) {
                if (f.equals(gf)) {
                    if (t == null) {
                        t = hg;
                    } else {
                        return null;
                    }
                }
            }
        }
        return t;
    }

    ;
    
    private void karisimaKat(HazirlananGubre hg, double miktar) {
        for (GubreFormu gf : hg.getGubre().getIcerik()) {
            Double oldMiktar = this.hazirlananFormMiktari.get(gf);
            Double oran = hg.getGubre().getIcerikOrani(gf);
            oldMiktar = (oldMiktar == null) ? (oran * miktar) : (oldMiktar + oran * miktar);
            this.hazirlananFormMiktari.put(gf, oldMiktar);
        }
    }

    private double getHazirlanacakFormFarki(GubreFormu gf) {
        Double hazirlanmis = this.hazirlananFormMiktari.get(gf);
        Double istenen = this.istenenFormMiktari.get(gf);
        return hazirlanmis == null ? istenen : istenen - hazirlanmis;
    }

    @Override
    public void karisimiHazirla() {

//zorunlulari tanka ekle
        for (HazirlananGubre hg : gubreListesi) {
            if (hg.isFinished()) {
                this.karisimaKat(hg, hg.getMiktar());
            }
        }
        /*
// istenilen herhangi bir gubre formu icin tek bir gubre varsa onlari tanka ekle
        boolean gotoBas;
        do {
            gotoBas = false;
            for (GubreFormu gf : this.istenenFormMiktari.keySet()) {
                double fark = this.getHazirlanacakFormFarki(gf);
                if (fark > 0) {
                    HazirlananGubre hg = this.firstProcess(gf);
                    if (hg != null) {
                        double gerekliMiktar = fark / hg.getGubre().getIcerikOrani(gf);
                        this.karisimaKat(hg, gerekliMiktar);
                        hg.setFinished(true);
                        gotoBas = true;
                    }
                }
            }
  } while (gotoBas);
         */
        this.lastProcess();
        System.out.println(this.istenenFormMiktari);
        System.out.println(this.hazirlananFormMiktari);
        System.out.println(this.gubreListesi);
//   kalanlari en yuksek formu az az indirerek
//   formlara 0'a ya da kabuledilebilir farka ininceye kadar devam edilir
    }

    //eksikler eklenir
//   kalanlari en yuksek formu az az indirerek
//   formlara 0'a ya da kabuledilebilir farka ininceye kadar devam edilir 
    private void lastProcess() {
        double sapma = MAX_FORM_ORAN_SAPMA * this.getKarisimKilosu();
        double maxFormEksikligi = 0;
        GubreFormu maxForm = null;
        HazirlananGubre target = null;
        do {
            maxFormEksikligi = 0;
            target = null;
            for (GubreFormu gf : this.istenenFormMiktari.keySet()) {
                double temp = this.getHazirlanacakFormFarki(gf);
                if (temp > maxFormEksikligi) {
                    maxForm = gf;
                    maxFormEksikligi = temp;
                }
            }
            if (maxFormEksikligi > sapma) {
                Iterator<HazirlananGubre> it = this.gubreListesi.iterator();
                HazirlananGubre tmp = null;
                while (it.hasNext()) {
                    tmp = it.next();
                    if (!tmp.isFinished() && tmp.getGubre().getIcerikOrani(maxForm) > 0) {
                        target = tmp;
                        break;
                    }
                }
                while (it.hasNext()) {
                    tmp = it.next();
                    if (!tmp.isFinished()) {
                        if (tmp.getGubre().getIcerikOrani(maxForm) > target.getGubre().getIcerikOrani(maxForm)) {
                            target = tmp;
                        }
                    }
                }
                if (target != null) {
                    double miktar;
                    if (maxFormEksikligi > 100) {
                        miktar = 0.1;
                    } else if (maxFormEksikligi > 50) {
                        miktar = 0.05;
                    } else {
                        miktar = 0.01;
                    }
                    this.karisimaKat(target, miktar);
                      target.increaseMiktar(miktar);
                }
            }

        } while (target != null);
    }

    class HazirlananGubre {

        final AbstractGubre gubre;
        Double miktar;
        boolean finished;

        public HazirlananGubre(AbstractGubre gubre, Double miktar, boolean finished) {
            this.gubre = gubre;
            this.miktar = miktar;
            this.finished = finished;
        }

        public AbstractGubre getGubre() {
            return gubre;
        }

        public Double getMiktar() {
            return miktar;
        }

        public void setMiktar(Double miktar) {
            this.miktar = miktar;
        }

        public boolean isFinished() {
            return finished;
        }

        public void setFinished(boolean finished) {
            this.finished = finished;
        }

        private void increaseMiktar(double miktar) {
            this.setMiktar(this.miktar == null ? miktar : this.miktar + miktar);
        }

        @Override
        public String toString() {
            return gubre + " : " + this.miktar + " " + finished;
        }
    }
}
