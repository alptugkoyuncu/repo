/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gubrehesaplama;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;

/**
 *
 * @author alptug
 */
public class Gubre extends AbstractGubre {

    public final static Gubre g33, g305, gMap, gMKP, gPN, g1040, g0051, gCalnit, gMagNit, gMagSul, g201020, g105210, g17727, gNitAsit;
    EnumMap<GubreFormu, Double> icerik;

    static {
        g105210 = new Gubre("10-52-10");
        g105210.addNewGubreForm(GubreFormu.azot, 10.0);
        g105210.addNewGubreForm(GubreFormu.fosfor, 52.0);
        g105210.addNewGubreForm(GubreFormu.potasyum, 10.0);
        g105210.setType(GubreType.fosfatli);

        g17727 = new Gubre("17-7-27");
        g17727.addNewGubreForm(GubreFormu.azot, 17.0);
        g17727.addNewGubreForm(GubreFormu.fosfor, 7.0);
        g17727.addNewGubreForm(GubreFormu.potasyum, 27.0);
        g17727.setType(GubreType.karisim);

        g201020 = new Gubre("20-10-20");
        g201020.addNewGubreForm(GubreFormu.azot, 20.0);
        g201020.addNewGubreForm(GubreFormu.fosfor, 10.0);
        g201020.addNewGubreForm(GubreFormu.potasyum, 20.0);
        g201020.setType(GubreType.karisim);

        g33 = new Gubre("33'lük");
        g33.addNewGubreForm(GubreFormu.azot, 33.0);
        g33.setType(GubreType.azotlu);
        g305 = new Gubre("30-5-0");
        g305.addNewGubreForm(GubreFormu.azot, 30.0);
        g305.addNewGubreForm(GubreFormu.fosfor, 5.0);
        g305.setType(GubreType.azotlu);

        gMap = new Gubre("Map");
        gMap.addNewGubreForm(GubreFormu.azot, 12.0);
        gMap.addNewGubreForm(GubreFormu.fosfor, 61.0);
        gMap.setType(GubreType.fosfatli);

        gMKP = new Gubre("MKP");
        gMKP.addNewGubreForm(GubreFormu.fosfor, 52.0);
        gMKP.addNewGubreForm(GubreFormu.potasyum, 34.0);
        gMKP.setType(GubreType.fosfatli);

        gPN = new Gubre("Pot. Nit.");
        gPN.addNewGubreForm(GubreFormu.azot, 13.5);
        gPN.addNewGubreForm(GubreFormu.potasyum, 45.5);
        gPN.setType(GubreType.potasli);

        g1040 = new Gubre("10-0-40");
        g1040.addNewGubreForm(GubreFormu.azot, 10.0);
        g1040.addNewGubreForm(GubreFormu.potasyum, 40.0);
        g1040.setType(GubreType.potasli);

        g0051 = new Gubre("Pot. Sul.");
        g0051.addNewGubreForm(GubreFormu.potasyum, 51.0);
        g0051.addNewGubreForm(GubreFormu.sulfat, 18.0);
        g0051.setType(GubreType.potasli);

        gCalnit = new Gubre("Cal. Nit.");
        gCalnit.addNewGubreForm(GubreFormu.azot, 15.5);
        gCalnit.addNewGubreForm(GubreFormu.kalsiyum, 26.5);
        gCalnit.setType(GubreType.kalsiyumlu);

        gMagNit = new Gubre("Mag. Nit.");
        gMagNit.addNewGubreForm(GubreFormu.azot, 10.0);
        gMagNit.addNewGubreForm(GubreFormu.magnezyum, 15.0);
        gMagNit.setType(GubreType.magnezyumlu);

        gMagSul = new Gubre("Mag. Sül.");
        gMagSul.addNewGubreForm(GubreFormu.sulfat, 12.0);
        gMagSul.addNewGubreForm(GubreFormu.magnezyum, 16.0);
        gMagSul.setType(GubreType.magnezyumlu);

        gNitAsit = new Gubre("Nitril Asit");
        gNitAsit.addNewGubreForm(GubreFormu.azot, 12.5);
    }

    public Gubre(String name) {
        this(name, new EnumMap<>(GubreFormu.class));
    }

    public static void saveJSon(String fileName) {
        List<Gubre> list = new ArrayList<>();
        list.add(g33);
        list.add(g1040);
        PrintWriter pw = null;
        try {
            JSONArray ja = new JSONArray();
            for (Gubre g : list) {
                JSONObject jo = new JSONObject();
                jo.put("Name", g.getName());
                jo.put("Type", g.getType().toString());
                jo.put("Icerik", g.getIcerikMap());
                ja.add(jo);
            }
            pw = new PrintWriter(fileName);
            pw.write(ja.toJSONString());
            pw.flush();
            pw.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Gubre.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            pw.close();
        }
    }

    public static Gubre[] loadJSon(String fileName) {
        List<Gubre> list = new ArrayList<>();
        try {
            System.out.println("read");
            Object obj = new JSONParser().parse(new FileReader(fileName));
            System.out.println("read");
            JSONArray ja = (JSONArray) obj;

            for (Object t : ja) {
                JSONObject jo = (JSONObject) t;
                Gubre g = new Gubre((String) jo.get("Name"));
                g.setType(GubreType.valueOf((String) jo.get("Type")));
                Map<String, Double> m = (Map) jo.get("Icerik");
                for (String s : m.keySet()) {
                    try {
                        g.addNewGubreForm(GubreFormu.valueOf(s), m.get(s));
                    } catch (IllegalArgumentException ex) {

                    }
                }
                list.add(g);
            }
        } catch (IOException ex) {
            Logger.getLogger(Gubre.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Gubre.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(list);
        return list.toArray(new Gubre[0]);
    }

    public Gubre(String name, EnumMap<GubreFormu, Double> icerik) {
        super(name);
        this.icerik = icerik;
    }

    public void addNewGubreForm(GubreFormu e, Double val) {
        icerik.put(e, val);
    }

    public void removeGubreForm(GubreFormu e) {
        icerik.remove(e);
    }

    public EnumMap<GubreFormu, Double> getIcerikMap() {
        return this.icerik;
    }

    @Override
    public GubreFormu[] getIcerik() {
        return icerik.keySet().toArray(new GubreFormu[0]);
    }

    @Override
    public double getIcerikOrani(GubreFormu gf) {
        if (this.icerik.containsKey(gf)) {
            Double val = icerik.get(gf);
            return val != null && val > 0 ? val : 0;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return name + " : " + this.icerik.toString();
    }

}
