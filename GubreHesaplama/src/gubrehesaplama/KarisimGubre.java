/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gubrehesaplama;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author alptug
 */
public class KarisimGubre extends AbstractKarisimGubre {

    EnumMap<GubreFormu, Double> istenilenFormOranMap;
    Map<AbstractGubre, Double> avaibleGubreMap;
    EnumMap<GubreFormu, Double> hazirlanan;

    public KarisimGubre() {
        istenilenFormOranMap = new EnumMap<>(GubreFormu.class);
        hazirlanan = new EnumMap<>(GubreFormu.class);
        avaibleGubreMap = new HashMap<AbstractGubre, Double>();
    }

    private void calculateOneForm(GubreFormu v, AbstractGubre.GubreType gt) {
        Double iVal = istenilenFormOranMap.get(v);
        if (iVal == null || iVal <= 0.0) {
            return;
        }

        for (AbstractGubre g : avaibleGubreMap.keySet()) {
            if (hazirlanan.get(v) != null && iVal <= hazirlanan.get(v)) {
                break;
            }
            if (g.getType() == gt) {
                Double oran = g.getIcerikOrani(v);
                Double miktar;
                if (hazirlanan.get(v) == null) {
                    hazirlanan.put(v, 0.0);
                }
                miktar = (iVal - hazirlanan.get(v)) / oran;
                Double oldGubre = avaibleGubreMap.get(g);
                if (oldGubre == null) {
                    oldGubre = 0.0;
                }
                avaibleGubreMap.put(g, oldGubre + miktar);
                System.out.println(g.getType() + " Gubresi konulacak");
                for (GubreFormu gf : g.getIcerik()) {
                    Double oldVal = hazirlanan.get(gf);
                    if (oldVal == null) {
                        oldVal = 0.0;
                    }
                    hazirlanan.put(gf, oldVal + g.getIcerikOrani(gf) * miktar);
                }
            }
        }
    }

    @Override
    public void setKarisimGubreFormu(GubreFormu gf, double oran) {
        istenilenFormOranMap.put(gf, oran);
    }

    @Override
    public double getKarisimGubreFormOrani(GubreFormu gf) {
        return this.hazirlanan.get(gf);
    }

    @Override
    public GubreFormu[] getKarisimGubreFormlari() {
        return (GubreFormu[]) this.hazirlanan.keySet().toArray();
    }

    @Override
    public void addGubre(AbstractGubre g, double miktar) {
        avaibleGubreMap.put(g, miktar < 0.0 ? 0.0 : miktar);
    }

    @Override
    public AbstractGubre[] getKullanilanGubreler() {
        return this.avaibleGubreMap.keySet().toArray(new AbstractGubre[0]);
    }

    @Override
    public double getKullanilanGubreMiktari(AbstractGubre g) {
        return this.avaibleGubreMap.containsKey(g) ? this.avaibleGubreMap.get(g) : 0;
    }

    @Override
    public void karisimiHazirla() {
        //sabit kilolu karisimlari hesapla
        for (GubreFormu gf : istenilenFormOranMap.keySet()) {
            double val = this.istenilenFormOranMap.get(gf) * this.getKarisimKilosu();
            this.istenilenFormOranMap.put(gf, val);
        }
        for (AbstractGubre g : this.avaibleGubreMap.keySet()) {
            Double miktar = this.avaibleGubreMap.get(g);
            if (miktar == null || miktar <= 0.0) {
                continue;
            }
            for (GubreFormu gf : g.getIcerik()) {
                Double oran = g.getIcerikOrani(gf);
                Double oldVal = this.hazirlanan.get(gf);
                if (oldVal == null) {
                    oldVal = 0.0;
                }
                this.hazirlanan.put(gf, oldVal + (miktar * oran));
            }

        }
        //eksik kalan miktarlar karisimda kullanilabilecek saf gubrelerden elde edilsin
        //fosfor hesaplansin
        this.calculateOneForm(GubreFormu.fosfor, Gubre.GubreType.fosfatli);
        //potasli hesaplansin
        this.calculateOneForm(GubreFormu.potasyum, Gubre.GubreType.potasli);
        //azotlu hesaplansin
        this.calculateOneForm(GubreFormu.azot, Gubre.GubreType.azotlu);

//fosfor hesaplansin
        System.out.println(istenilenFormOranMap);
        System.out.println(hazirlanan);
        System.out.println(avaibleGubreMap);
    }
}
