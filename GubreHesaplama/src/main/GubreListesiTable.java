package main;


import gubrehesaplama.AbstractGubre;
import gubrehesaplama.Gubre;
import java.awt.Component;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class GubreListesiTable extends JTable {
    MyTableModel mdl = new MyTableModel();
    public GubreListesiTable() {
        super();
        super.setModel(mdl);
        super.getColumnModel().getColumn(0).setCellRenderer(new MyTableCellRenderer());
    }
    public Map<AbstractGubre,Double> getAllSelectedGubres(){
        Map<AbstractGubre,Double> m = new HashMap<>();
        for(int i=0; i<mdl.getRowCount();i++){
            if((Boolean)mdl.getValueAt(i, 3)){
                AbstractGubre g = (AbstractGubre) mdl.getValueAt(i,0);
                Double val = (Double) mdl.getValueAt(i, 2);
                if(g==null) continue;
                if(val==null) val=0.0;
                m.put(g, val);
            }
        }
        return m;
    }
    private class MyTableCellRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); //To change body of generated methods, choose Tools | Templates.

            switch (column) {
                case 0:
                    ((JLabel) c).setText(((Gubre) value).getName());
                    break;
//                case 1:
//                    ((JLabel) c).setText(((Gubre) value).getType().toString());
//                    break;
                default:
            }
            return c;
        }
    }
    public class MyTableModel extends AbstractTableModel {

        final List<AbstractGubre> gubre;
        Double[] value;
        Boolean[] checked;

        public MyTableModel() {
            gubre = new ArrayList<>();
            gubre.add(Gubre.g33);
            gubre.add(Gubre.g305);
            gubre.add(Gubre.gMKP);
            gubre.add(Gubre.gMap);
            gubre.add(Gubre.g1040);
            gubre.add(Gubre.gPN);
            gubre.add(Gubre.g0051);
            gubre.add(Gubre.gCalnit);
            gubre.add(Gubre.gMagNit);
            gubre.add(Gubre.gMagSul);
            gubre.add(Gubre.g201020);
            gubre.add(Gubre.g105210);
            gubre.add(Gubre.g17727);
            gubre.add(Gubre.gNitAsit);
                    
            value = new Double[gubre.size()];
            checked = new Boolean[gubre.size()];
            for (int i = 0; i < checked.length; i++) {
                checked[i] = false;
            }

        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 2:
                    return Double.class;
                case 3:
                    return Boolean.class;
                default:
                    return String.class;
            }
        }

        @Override
        public int getRowCount() {
            return gubre.size();
        }

        @Override
        public int getColumnCount() {
            return 4;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return columnIndex == 2 || columnIndex == 3;
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            switch (columnIndex) {
                case 2: {
                    Double val = (Double) aValue;
                    value[rowIndex] = val;
                    if (val == null && checked[rowIndex]) {
                        this.setValueAt(false, rowIndex, 3);
                    }
                    if (val != null && val > 0 && !checked[rowIndex]) {
                        this.setValueAt(true, rowIndex, 3);
                    }
                    break;
                }
                case 3: {
                    Boolean val = (Boolean) aValue;
                    checked[rowIndex] = val;
                    if (!val) {
                        this.setValueAt(null, rowIndex, 2);
                    }
                    break;
                }
            }
            this.fireTableDataChanged();
        }

        public void setGubreDegeri(Gubre g, Double val) {
            int index = this.gubre.indexOf(g);
            if (index == -1) {
                return;
            }
            this.setValueAt(val, index, 2);

        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return gubre.get(rowIndex);
                case 1:
                    return gubre.get(rowIndex).getType();
                case 2:
                    return value[rowIndex];
                case 3:
                    return checked[rowIndex];
                default:
                    return null;
            }

        }
    }

}
