/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

/**
 *
 * @author alptug
 */
public class MyDesktopPane extends JFrame {

    
    JPanel mainPanel,bottomPanel;
    JInternalFrame mainFrame;
    Container currContainer;

    public MyDesktopPane() {
        currContainer = null;
        mainFrame = new JInternalFrame("asd", true, true, true, true);
        mainFrame.setLocation(0, 0);
        mainFrame.setLayout(new FlowLayout());
        bottomPanel = new JPanel();
        this.setLayout(new BorderLayout());
        this.add(mainFrame, BorderLayout.CENTER);
        this.add(bottomPanel, BorderLayout.SOUTH);
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void setMainPane(Container ct) {
        if (currContainer != null) {
            mainFrame.remove(currContainer);
        }
        currContainer = ct;
        if (ct != null) {
            mainFrame.getContentPane().add(currContainer);
        }
        mainFrame.pack();
        mainFrame.show();
        this.pack();
    }
    
}
