/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.datamodel;

import data.DatabaseObject;
import data.Gubre;
import data.GubreSinifi;
import java.awt.Component;
import java.util.Collection;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author alptug
 */
public class GubreDataModel extends AbstractTableModel implements TableCellRenderer {

    List<Gubre> gubreListesi;
    String[] columnNames;

    public GubreDataModel(Collection<Gubre> list) {
        this.gubreListesi = (List<Gubre>) list;
        columnNames = new String[]{"Aktif", "Adı", "Kısaltma", "Sınıf", "Değişiklik"};
    }

    public void setGubreListesi(List<Gubre> list) {
        this.gubreListesi = list;
        this.fireTableDataChanged();
    }

    public Gubre getGubreByRowIndex(int rowIndex) {
        if (rowIndex >= 0 && rowIndex < gubreListesi.size()) {
            return this.gubreListesi.get(rowIndex);
        } else {
            return null;
        }
    }

    public GubreDataModel() {
        this(DatabaseObject.listGubre);
    }

    @Override
    public int getRowCount() {
//        return (this.gubreListesi == null ? 0 : this.gubreListesi.size()) + 1;
    return this.gubreListesi.size();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Boolean.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return GubreSinifi.class;
            case 4:
                return Boolean.class;
            default:
                return String.class;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Gubre g = this.getGubreByRowIndex(rowIndex);
        
        switch (columnIndex) {
            case 0:
                return g.isAktif();
            case 1:
                return g.getName();
            case 2:
                return g.getKisaltma();
            case 3:
                return g.getSinifi();
            case 4:
                return true;
            default:
                return null;
        }
    }


    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        
        JLabel l = new JLabel();
        l.setOpaque(true);
        GubreSinifi gs = (GubreSinifi) value;
        l.setText(gs == null ? "" : gs.getName());
        return l;

    }

    public MyTableCellEditor getEditor() {
        return new MyTableCellEditor();
    }

    class MyTableCellEditor extends DefaultCellEditor {

        GubreSinifComboboxModel mdl = new GubreSinifComboboxModel();

        public MyTableCellEditor() {
            super(new JComboBox(new GubreSinifComboboxModel()));
            JComboBox jc = (JComboBox) this.getComponent();
            jc.setRenderer(new GubreSinifComboboxModel());
        }

    }

}
