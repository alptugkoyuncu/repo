/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.datamodel;

import data.DatabaseObject;
import static data.DatabaseObject.listGubreSinif;
import data.GubreIcerik;
import data.GubreSinifi;
import java.awt.Component;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author alptug
 */
public class GubreIcerikComboBoxModel extends DefaultComboBoxModel implements ListCellRenderer {

        protected DefaultListCellRenderer dr = new DefaultListCellRenderer();
        public GubreIcerikComboBoxModel() {
            
            for(GubreIcerik gs:DatabaseObject.listGubreIcerik){
                if(gs.isParent()){
                    this.insertElementAt(gs, 0);
                }else
                    this.addElement(gs);
            }
        }
        public void refresh(){
            super.removeAllElements();
            for(GubreIcerik gs:DatabaseObject.listGubreIcerik){
                if(gs.isParent()){
                    this.insertElementAt(gs, 0);
                }else
                    this.addElement(gs);
            }
        }
        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            JLabel lbl = (JLabel) dr.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            GubreIcerik gi = (GubreIcerik) value;
            lbl.setText(gi != null ? gi.getIcerikAdi() : "");
            return lbl;
        }
    }
