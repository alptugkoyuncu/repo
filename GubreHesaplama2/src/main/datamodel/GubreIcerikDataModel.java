/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.datamodel;

import data.DatabaseObject;
import data.Gubre;
import data.GubreIcerik;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author alptug
 */
public class GubreIcerikDataModel extends AbstractTableModel {

    Object[][] values2;
    List<GubreIcerik> gubreIcerikList;
    Double[] values;
//    List<GubreIcerik> icerikMiktarMap;
    String[] columnNames;
    Gubre gubre;

    public GubreIcerikDataModel(List<GubreIcerik> gubreIcerikListesi, Gubre g) {
        this.gubre = null;
        columnNames = new String[]{"içerik Adı", "Miktar"};
        this.setGubreIcerikList(gubreIcerikListesi);
        this.setGubre(g);

    }

    public GubreIcerikDataModel() {
        this(DatabaseObject.listGubreIcerik, null);
    }

    /*
    public GubreIcerikDataModel(Gubre g) {
        this.gubre = g;
        if (this.gubre != null) {
            this.icerikMiktarMap = gubre.getIcerik().keySet().toArray(new GubreIcerik[0]);
        }else
            this.icerikMiktarMap=null;
        columnNames = new String[]{"İçerik Adı", "Miktar"};
    }
     */
    public GubreIcerikDataModel(List<GubreIcerik> l) {
        this(l, null);
    }

    @Override
    public int getRowCount() {
        return gubreIcerikList == null ? 0 : gubreIcerikList.size();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (this.gubreIcerikList == null || this.gubreIcerikList.isEmpty()) {
            return null;
        }
        GubreIcerik gi = this.gubreIcerikList.get(rowIndex);

        System.out.println(rowIndex);
        if (gi == null) {
            return null;
        }
        switch (columnIndex) {
            case 0:
                return gi.getIcerikAdi();
            case 1:
                return values[rowIndex];
            default:
                return null;
        }
    }

    public Gubre getGubre() {
        return gubre;
    }

    public List<GubreIcerik> getGubreIcerikList() {
        return gubreIcerikList;
    }

    public void setGubreIcerikList(List<GubreIcerik> gl) {
        this.gubreIcerikList = gl;
        if (this.gubreIcerikList != null) {
            values = new Double[this.gubreIcerikList.size()];
        }
        updateValues();
        this.fireTableDataChanged();
    }

    public void clearData() {
        if (this.gubreIcerikList != null) {
            values = new Double[gubreIcerikList.size()];
        }
    }

    private void updateValues() {
        clearData();
        if (this.gubre != null && this.gubre.getIcerik() != null && !this.gubre.getIcerik().isEmpty()) {
            Map<GubreIcerik, Double> val = this.gubre.getIcerik();
            for (GubreIcerik gi : val.keySet()) {
                System.out.println(this.gubreIcerikList);
                int index = this.gubreIcerikList.indexOf(gi);
                if (index >= 0 && index < values.length) {
                    values[index] = val.get(gi);
                } else {
                    System.err.println("index hatasi : " + index);
                }
            }
        }
    }

    public void setGubre(Gubre g) {
        this.gubre = g;
        updateValues();
        this.fireTableDataChanged();
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (gubre == null) {
            return;
        }
        try {
            Double val = Double.parseDouble((String) aValue);
            gubre.addIcerik(this.gubreIcerikList.get(rowIndex), val);
            values[rowIndex] = val > 0 ? val : null;
            this.fireTableCellUpdated(rowIndex, columnIndex);
        } catch (NumberFormatException ex) {

        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 1;
    }

    public GubreIcerik getGubreIcerikByRowIndex(int selectedRow) {
        return this.gubreIcerikList.get(selectedRow);
    }

}
