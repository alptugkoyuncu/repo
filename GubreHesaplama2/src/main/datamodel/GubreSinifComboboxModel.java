/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.datamodel;

import static data.DatabaseObject.listGubreSinif;
import data.GubreSinifi;
import java.awt.Component;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author alptug
 */
public class GubreSinifComboboxModel extends DefaultComboBoxModel implements ListCellRenderer {

        protected DefaultListCellRenderer dr = new DefaultListCellRenderer();

        public GubreSinifComboboxModel() {
            for (GubreSinifi gs : listGubreSinif) {
                this.addElement(gs);
            }
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            JLabel lbl = (JLabel) dr.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            GubreSinifi gs = (GubreSinifi) value;
            lbl.setText(gs != null ? gs.getName() : "");
            return lbl;
        }
    }
