/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.ui;

import data.DatabaseObject;
import data.Gubre;
import data.GubreIcerik;
import data.GubreSinifi;
import db.DatabaseConnection;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import main.datamodel.GubreDataModel;
import main.datamodel.GubreIcerikDataModel;
import main.listeners.DatabaseDMLListener;

/**
 *
 * @author alptug
 */
public class GubreGubreIcerikKayitPanel extends JPanel{

    private final JScrollPane pane1;
    private final JScrollPane pane2;
    private final JTable gubreTable;
    private final JTable gubreIcerikTable;
    GubreDataModel gubreDataModel;
    GubreIcerikDataModel giDataModel;
    
    final GubreKayitPanel gk;
    final GubreIcerikKayitPanel gik;
    public GubreGubreIcerikKayitPanel() {
        gk = new GubreKayitPanel();
        gk.addDatabaseDMLListener(new DatabaseGubreTableListener());
        
        gik = new GubreIcerikKayitPanel();
        gik.addDatabaseDMLListener(new DatabaseGubreIcerikTableListener());
        
        gubreDataModel = new GubreDataModel();
        giDataModel =new GubreIcerikDataModel();
        gubreTable = new JTable(gubreDataModel);
        gubreIcerikTable = new JTable(giDataModel);
        pane1 = new JScrollPane(gubreTable);
        pane2 = new JScrollPane(gubreIcerikTable);
        
        pane1.setPreferredSize(new Dimension(100,200));
        pane1.setBorder(BorderFactory.createTitledBorder(BorderFactory.createRaisedSoftBevelBorder(),"Gübre Listesi"));
        pane2.setPreferredSize(new Dimension(100,200));
        pane2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createRaisedBevelBorder(),"Gübreye ait İçerik Listesi"));
        JPanel left = new JPanel();
        left.setLayout(new BorderLayout(5,5));
        left.add(pane1,BorderLayout.CENTER);
        left.add(gk,BorderLayout.SOUTH);
        
        JPanel right = new JPanel();
        right.setLayout(new BorderLayout());
        right.add(pane2,BorderLayout.CENTER);
        right.add(gik, BorderLayout.SOUTH);

        setLayout(new GridLayout(1, 2));
        add(left);
        add(right);
        
        
        this.gubreTable.setDefaultRenderer(GubreSinifi.class, (TableCellRenderer) gubreDataModel);
        this.gubreTable.setDefaultEditor(GubreSinifi.class, gubreDataModel.getEditor());
        this.gubreTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.gubreTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    return;
                }
                System.out.println("lsm");
                Gubre g=gubreDataModel.getGubreByRowIndex(gubreTable.getSelectedRow());
                giDataModel.setGubre(g);
                gk.setGubre(g);

            }
        });
        this.gubreIcerikTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.gubreIcerikTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(e.getValueIsAdjusting())
                    return;
                int index= gubreIcerikTable.getSelectedRow();
                GubreIcerik gi = index==-1? null :giDataModel.getGubreIcerikByRowIndex(gubreIcerikTable.getSelectedRow());
                gik.setGubreIcerik(gi);
            }
        });
    } 
    class DatabaseGubreTableListener implements DatabaseDMLListener{
        @Override
        public void tableDataChanged(DatabaseObject obj) {
            gubreTable.clearSelection();
            DatabaseConnection.LoadGubreBilgileri();
            gubreDataModel.fireTableDataChanged();
        }
        
    }
    class DatabaseGubreIcerikTableListener implements DatabaseDMLListener{

        @Override
        public void tableDataChanged(DatabaseObject obj) {
            gubreIcerikTable.clearSelection();
            DatabaseConnection.LoadGubreBilgileri();
            giDataModel.setGubreIcerikList(DatabaseObject.listGubreIcerik);
        }
        
    }
}
