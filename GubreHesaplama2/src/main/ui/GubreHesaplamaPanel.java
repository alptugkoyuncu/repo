/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.ui;

import data.DatabaseObject;
import data.Gubre;
import data.GubreIcerik;
import gubre_hazirlama.AbstractKarisimGubre;
import gubre_hazirlama.AdvancedKarisimGubre;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author alptug
 */
public class GubreHesaplamaPanel extends javax.swing.JPanel {

    GubreIcerikListPanel gubreIcerikPanel;
    javax.swing.JFormattedTextField toplam;
    JButton hesapla, yeniButton;
    JTable gubreSecimTable;
    GubreSecimTableModel tableModel;
    AbstractKarisimGubre ag;
    ActionListener l;

    public double getToplamIstenenKilo() {
        try {
            return ((Number) toplam.getValue()).doubleValue();
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            return -1;
        } catch (NullPointerException ex2) {
            ex2.printStackTrace();
            return -2;
        }
    }

    public AbstractKarisimGubre getKarisimGubre() {
        return ag;
    }

    public void setKarisimGubre(AbstractKarisimGubre ag) {
        if (ag == null) {
            this.ag = new AdvancedKarisimGubre();
        } else {
            this.ag = ag;
        }
        this.ag.removeActionListener(l);
        this.ag.addActionListener(l);
        fill();

    }

    private void fill() {
        int i = 0;

        tableModel.reset();
        for (GubreIcerik gi : DatabaseObject.listGubreIcerik) {
            gubreIcerikPanel.setHazirlananOran(gi, 0.0);
        }

        for (Gubre g : ag.getKullanilanGubreler()) {
            int index = tableModel.getGubreIndex(g);
            tableModel.setValueAt(ag.getKullanilanGubreMiktari(g), index, 2);
        }
        for (GubreIcerik gi : ag.getKarisimGubreFormlari()) {
            gubreIcerikPanel.setHazirlananOran(gi, ag.getKarisimGubreFormOrani(gi));
        }
        this.toplam.setValue(ag.getKarisimKilosu());

    }

    public void hesaplaButtonClicked(ActionEvent e) {
        double val = this.getToplamIstenenKilo();
        if (val < 0) {
            return;
        }
        ag.reset();
        ag.setKarisimKilosu(val);
        Map<Gubre, Double> secilenGubreler = tableModel.getSecilenGubreler();
        Map<GubreIcerik, Double> istenenFormlar = gubreIcerikPanel.getIstenenOran();
        for (Gubre g : secilenGubreler.keySet()) {
            ag.addGubre(g, secilenGubreler.get(g));
        }
        for (GubreIcerik gi : istenenFormlar.keySet()) {
            ag.addKarisimGubreFormu(gi, istenenFormlar.get(gi));
        }

        new Thread(ag).start();

    }

    /**
     * Creates new form GubreHesaplamaPanel
     */
    public GubreHesaplamaPanel() {
        this(null);
    }

    public GubreHesaplamaPanel(AbstractKarisimGubre g) {
        toplam = new javax.swing.JFormattedTextField();
        toplam.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        toplam.setPreferredSize(new Dimension(80, 40));
        toplam.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK, 1), "Toplam"));
        toplam.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        toplam.selectAll();
                    }
                });
            }

            @Override
            public void focusLost(FocusEvent e) {
            }
        });
        hesapla = new JButton("Hesapla");
        hesapla.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hesaplaButtonClicked(e);
            }
        });
        yeniButton = new JButton("Yeni");
        yeniButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setKarisimGubre(null);
            }

        });
        JPanel pnl = new JPanel(new GridLayout(1, 2));
        pnl.add(toplam);
        pnl.add(hesapla);

        JPanel pnl2 = new JPanel(new BorderLayout());
        pnl2.add(pnl, BorderLayout.CENTER);
        pnl2.add(yeniButton, BorderLayout.SOUTH);

        gubreIcerikPanel = new GubreIcerikListPanel(DatabaseObject.listGubreIcerik);
        jPanel1 = new JPanel();
        jPanel1.setLayout(new BorderLayout(60, 20));
        jPanel1.add(new JScrollPane(gubreIcerikPanel), BorderLayout.CENTER);
        jPanel1.add(pnl2, BorderLayout.LINE_END);

        tableModel = new GubreSecimTableModel(DatabaseObject.listGubre);
        gubreSecimTable = new JTable(tableModel);
        JScrollPane jsp = new JScrollPane(gubreSecimTable);

        this.setLayout(new BorderLayout(20, 20));
        this.add(jsp, BorderLayout.CENTER);
        this.add(jPanel1, BorderLayout.NORTH);
        l = new KarisimActionListener();
        this.setKarisimGubre(g);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 507, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 256, Short.MAX_VALUE)
        );

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

    class GubreSecimTableModel extends AbstractTableModel {

        Double values[];
        boolean[] secilmis;
        List<Gubre> gubreListesi;

        public GubreSecimTableModel(List<Gubre> gubreListesi) {
            this.gubreListesi = gubreListesi;
            values = new Double[gubreListesi.size()];
            secilmis = new boolean[gubreListesi.size()];
        }

        @Override
        public int getRowCount() {
            return gubreListesi == null ? 0 : gubreListesi.size();
        }

        @Override
        public int getColumnCount() {
            return 4;
        }

        public Map<Gubre, Double> getSecilenGubreler() {
            Map<Gubre, Double> m = new HashMap<Gubre, Double>();
            for (int i = 0; i < this.getRowCount(); i++) {
                if (secilmis[i]) {
                    m.put(gubreListesi.get(i), values[i] == null ? 0 : values[i]);
                }
            }
            return m;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Gubre g = gubreListesi.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    return g.getKisaltma();
                case 1:
                    return g.getSinifi().getName();
                case 2:
                    return values[rowIndex];
                case 3:
                    return secilmis[rowIndex];
                default:
                    return null;
            }
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return columnIndex == 2 || columnIndex == 3;
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 2:
                    return Double.class;
                case 3:
                    return Boolean.class;
                default:
                    return String.class;
            }
        }

        public void reset() {
            values = new Double[gubreListesi.size()];
            secilmis = new boolean[gubreListesi.size()];
            fireTableDataChanged();
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            switch (columnIndex) {
                case 2: {
                    values[rowIndex] = (Double) aValue;
                    secilmis[rowIndex] = values[rowIndex] != null;
                    break;
                }
                case 3:
                    boolean val = (boolean) aValue;
                    secilmis[rowIndex] = val;
                    if (!val) {
                        values[rowIndex] = null;
                    }
                    break;
            }
            this.fireTableRowsUpdated(rowIndex, rowIndex);
        }

        private int getGubreIndex(Gubre g) {
            return this.gubreListesi.indexOf(g);
        }
    }

    private class KarisimActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand() == AbstractKarisimGubre.GUBRE_KARISIMI_BASLADI) {
                System.out.println("Karisim Basladi");
//                GubreHesaplamaPanel.this.setEnabled(false);
            } else {
//                GubreHesaplamaPanel.this.setEnabled(true);
                if (e.getActionCommand() == AbstractKarisimGubre.GUBRE_KARISIMI_HAZIRLANDI) {
                    System.out.println("Karisim hazirlandi");

                } else {
                    System.out.println("Karisim iptal");
                }
                fill();
                JOptionPane.showMessageDialog(null, e.getActionCommand());
            }
        }

    }
}
