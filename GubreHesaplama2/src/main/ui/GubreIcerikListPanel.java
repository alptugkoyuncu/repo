/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.ui;

import data.DatabaseObject;
import data.GubreIcerik;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author alptug
 */
public class GubreIcerikListPanel extends JPanel {
    private List<GubreIcerik> list;
    GubreIcerikPanel[] pnlList;
    public GubreIcerikListPanel(List<GubreIcerik> l) {
        setList(l);
    }

    public List<GubreIcerik> getList() {
        return list;
    }

    public void setList(List<GubreIcerik> list) {
        this.list = list;
        this.createPanel();
    }
    public void setIstenenOran(GubreIcerik gi,double value){
        for(GubreIcerikPanel pnl:pnlList)
            if(pnl.getGubreIcerik().equals(gi)){
                pnl.setIstenenOran(value);
                return;
            }
    }
    public Map<GubreIcerik,Double> getIstenenOran(){
        Map<GubreIcerik,Double> val = new HashMap<GubreIcerik,Double>();
        for(GubreIcerikPanel pnl:pnlList){
            if(pnl!=null && pnl.getIstenenOran()!=null && pnl.getIstenenOran()>0)
                val.put(pnl.getGubreIcerik(), pnl.getIstenenOran());
        }
        return val;
    }
    public void setHazirlananOran(GubreIcerik gi,double value){
        for(GubreIcerikPanel pnl:pnlList)
            if(pnl.getGubreIcerik().equals(gi)){
                pnl.setHazirlananOran(value);
                return;
            }
    }
    public Map<GubreIcerik,Double> getHazirlananOran(){
        Map<GubreIcerik,Double> val = new HashMap<GubreIcerik,Double>();
        for(GubreIcerikPanel pnl:pnlList){
            if(pnl!=null && pnl.getHazirlananOran()!=null && pnl.getHazirlananOran()>0)
                val.put(pnl.getGubreIcerik(), pnl.getHazirlananOran());
        }
        return val;
    }
    private void createPanel(){
        this.removeAll();
        pnlList=null;
        Map<GubreIcerik,List> parentChild=new TreeMap<GubreIcerik,List>();
        // gubreIcerikleri parentchild iliskisine gore  siralar;
        // mapin key kisminda parent child kisminda ise parentin cocuklarinin listesi olur.
        for(GubreIcerik gi:list){
            if(gi.isParent()){
                parentChild.put(gi, new ArrayList<GubreIcerik>());
            }
        }
        for(GubreIcerik gi:list){
            if(!gi.isParent()){
                GubreIcerik parent = (GubreIcerik) DatabaseObject.findByDbId(list, gi.getParentId());
                List l  = parentChild.get(parent);
                l.add(gi);
            }
        }
        pnlList =new GubreIcerikPanel[parentChild.keySet().size()];
        int i=0;
        GridLayout l = new GridLayout(1, pnlList.length,5,5);
        
        this.setLayout(l);
        for(GubreIcerik gi:parentChild.keySet()){
            pnlList[i]=new GubreIcerikPanel(gi,parentChild.get(gi));
            this.add(pnlList[i]);
            i++;
        }
        this.validate();
    }
    
    class GubreIcerikPanel extends JPanel{
        private final GubreIcerik gubreIcerik;
        private final List<GubreIcerik> childList;
        private Double istenenOran;
        private Double hazirlananOran;
        private final JTextField jtxtGubreIcerik;
        private final JLabel jlblGubreIcerik;
        public GubreIcerikPanel(GubreIcerik gi,List<GubreIcerik> childs){
            
            this.childList=childs;
            this.gubreIcerik=gi;
            jtxtGubreIcerik=new JTextField();
            setIstenenOran(0.0);
            jtxtGubreIcerik.addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent e) {
                    jtxtGubreIcerik.selectAll();
                }

                @Override
                public void focusLost(FocusEvent e) {
                    try{
                        setIstenenOran(Double.parseDouble(jtxtGubreIcerik.getText()));
                    }catch(Exception ex){
                        setIstenenOran(0.0);
                    }
                }
            });
            jlblGubreIcerik = new JLabel("0.0");
            setBorder();
            this.setLayout(new BorderLayout());
            add(jtxtGubreIcerik,BorderLayout.CENTER);
            add(jlblGubreIcerik,BorderLayout.SOUTH);
        }
        private void setBorder(){
            setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLoweredSoftBevelBorder(), gubreIcerik.getIcerikAdi()));
        }

        public Double getIstenenOran() {
            return istenenOran;
        }

        public void setIstenenOran(Double istenenOran) {
            this.istenenOran = istenenOran;
            jtxtGubreIcerik.setText(String.format("%.2f",this.istenenOran));
        }

        public Double getHazirlananOran() {
            return hazirlananOran;
        }

        public void setHazirlananOran(Double hazirlananOran) {
            this.hazirlananOran = hazirlananOran;
            jlblGubreIcerik.setText(String.format("%.2f",this.hazirlananOran));
        }
        public GubreIcerik getGubreIcerik() {
            return gubreIcerik;
        }
    }
    
}
