/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.ui;

import data.DatabaseObject;
import data.Gubre;
import data.GubreSinifi;
import db.DatabaseOperationResult;
import db.DatabaseOperationResultList;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import main.datamodel.GubreSinifComboboxModel;
import main.listeners.DatabaseDMLListener;

/**
 *
 * @author alptug
 */
public class GubreKayitPanel extends javax.swing.JPanel {

    GubreSinifComboboxModel mdl = new GubreSinifComboboxModel();
    Gubre g;
    ButtonPanel buttonPanel;
    List<DatabaseDMLListener> listeners = new ArrayList<DatabaseDMLListener>();

    /**
     * Creates new form GubreKayit
     */
    public GubreKayitPanel() {
        this(null);
    }

    public void addDatabaseDMLListener(DatabaseDMLListener l) {
        this.listeners.add(l);
    }

    public void removerDatabaseDMLListener(DatabaseDMLListener l) {
        this.listeners.remove(l);
    }

    private void fireActionEvent(DatabaseObject obj) {
        for (DatabaseDMLListener l : listeners) {
            l.tableDataChanged(obj);
        }
    }

    public GubreKayitPanel(Gubre g) {
        buttonPanel = new ButtonPanel() {
            @Override
            public boolean newButtonClicked() {
                GubreKayitPanel.this.g = null;
                fillAreas();
                return true;
            }

            @Override
            public boolean addButtonClicked() {
                boolean flag = GubreKayitPanel.this.save();

                fillAreas();
                if (flag) {
                    fireActionEvent(g);
                }
                return flag;
            }

            @Override
            public boolean deleteButtonClicked() {
                boolean flag = GubreKayitPanel.this.delete();
                fillAreas();
                if (flag) {
                    fireActionEvent(g);
                }
                return flag;
            }

            @Override
            public boolean updateButtonClicked() {
                boolean flag = GubreKayitPanel.this.save();
                fillAreas();
                if (flag) {
                    fireActionEvent(g);
                }
                return flag;
            }
        };

        initComponents();
        this.add(mainPanel, BorderLayout.CENTER);
        this.add(buttonPanel, BorderLayout.SOUTH);
        this.setGubre(g);
    }

    public void setGubre(Gubre g) {
        this.g = g;
        if (g == null) {
            buttonPanel.setPosition(ButtonPanel.YENI_EKLEME_POSITION);
        } else {
            buttonPanel.setPosition(ButtonPanel.GUNCELLEME_SILME_POSITION);
        }
        fillAreas();
    }

    private void fillAreas() {
        if (g != null) {
            this.jTextField1.setText(g.getName());
            this.jTextField2.setText(g.getKisaltma());
            this.jComboBox1.setSelectedItem(g.getSinifi());
            this.jCheckBox1.setSelected(g.isAktif());
        } else {
            this.jTextField1.setText("");
            this.jTextField2.setText("");
            this.jComboBox1.setSelectedIndex(-1);
            this.jCheckBox1.setSelected(true);
        }
    }

    private boolean delete() {
        boolean flag;
        String msg = "";
        if (g == null || this.g.getId() < 0) {
            msg = "Silinecek Gübre Bulunamadı";
            flag = false;
        } else {
            DatabaseOperationResult res = g.delete();
            flag = res == DatabaseOperationResult.OperationComplete || res == DatabaseOperationResult.DeleteComplete;
            msg = DatabaseOperationResultList.getMesagges();
        }
        JOptionPane.showMessageDialog(null, msg);
        return flag;
    }

    private boolean save() {
        String n = this.jTextField1.getText();
        String k = this.jTextField2.getText();
        GubreSinifi gs = (GubreSinifi) this.jComboBox1.getSelectedItem();
        int id = g != null && g.getId() > 0 ? g.getId() : -1;
        try {

            if (g == null || id < 0) {
                if (g == null) {
                    g = new Gubre();
                }
            }
            g.setName(n);
            g.setKisaltma(k);
            g.setSinifi(gs);
            g.setAktif(jCheckBox1.isSelected());

            DatabaseOperationResult opt;
            if (id > 0) {
                opt = g.update();
            } else {
                opt = g.insert();
            }
            JOptionPane.showMessageDialog(null, DatabaseOperationResultList.getMesagges());
            return opt == opt.OperationComplete || opt == opt.InsertComplete || opt == opt.UpdateComplete;
        } catch (Exception ex) {
            JOptionPane.showConfirmDialog(null, ex.getMessage());
            return false;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jCheckBox1 = new javax.swing.JCheckBox();
        jLabel4 = new javax.swing.JLabel();

        mainPanel.setPreferredSize(new java.awt.Dimension(250, 150));

        jLabel2.setText("Kısaltma");

        jLabel3.setText("Gübre Sınıfı");

        jComboBox1.setModel(mdl);
        jComboBox1.setRenderer(mdl);
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Adı");

        jLabel4.setText("Aktif");

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(mainPanelLayout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addGap(37, 37, 37))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jComboBox1, 0, 118, Short.MAX_VALUE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                    .addComponent(jCheckBox1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(29, Short.MAX_VALUE))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBox1)
                    .addComponent(jLabel4))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "Gübre Kayıt"));
        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JPanel mainPanel;
    // End of variables declaration//GEN-END:variables
}
