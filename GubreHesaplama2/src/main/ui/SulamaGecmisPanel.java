/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.ui;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author alptug
 */
public class SulamaGecmisPanel extends JPanel{
    SulamaGecmisTablePanel tablePanel;
    SulamaKayitveGubrelemeHesaplamaPanel kayitveHesaplamaPanel;
    
    public SulamaGecmisPanel() {
        this.setLayout(new BorderLayout());
        tablePanel = new SulamaGecmisTablePanel();
        kayitveHesaplamaPanel =new SulamaKayitveGubrelemeHesaplamaPanel();
        this.add(tablePanel,BorderLayout.CENTER);
        this.add(kayitveHesaplamaPanel,BorderLayout.EAST);
        ListSelectionModel lsm= tablePanel.getTable().getSelectionModel();
        lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lsm.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(e.getValueIsAdjusting()){
                    return;
                }
                int index=tablePanel.getTable().convertRowIndexToModel(tablePanel.getTable().getSelectedRow());
                kayitveHesaplamaPanel.setSulama(tablePanel.getElementAt(index), true);
            }
        });
    }
    
}
