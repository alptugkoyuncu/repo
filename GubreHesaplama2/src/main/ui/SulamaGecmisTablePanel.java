/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.ui;

import data.DatabaseObject;
import data.Sera;
import data.Sulama;
import db.DatabaseConnection;
import gubre_hazirlama.AbstractKarisimGubre;
import gubre_hazirlama.AdvancedKarisimGubre;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

/**
 *
 * @author alptug
 */
public class SulamaGecmisTablePanel extends JPanel {

    JTable table;
    SulamaTableModel mdl;

    public JTable getTable() {
        return table;
    }

    public SulamaGecmisTablePanel() {
        mdl = new SulamaTableModel();
        table = new JTable(mdl);
        table.setDefaultRenderer(AbstractKarisimGubre.class, new KarisimGubreTableCellRenderer());
        table.setDefaultRenderer(Sera.class, new SeraTableCellRenderer());
        table.setAutoCreateRowSorter(true);
        doUI();
    }

    public Sulama getElementAt(int tableRowIndex) {
        return mdl.sulamaList.get(tableRowIndex);
    }

    private void doUI() {
        this.setLayout(new BorderLayout());
        JScrollPane jsp = new JScrollPane(table);
        this.add(jsp, BorderLayout.CENTER);
    }

    class SulamaTableModel extends AbstractTableModel {

        List<TableColumn> columns;
        private List<Sulama> sulamaList;

        public SulamaTableModel() {
            columns = new ArrayList<TableColumn>();
//            SeraTableColumn col = new SeraTableColumn("   ");

//            col.setResizable(false);
//            col.sizeWidthToFit();
//            columns.add(col);
            columns.add(new SeraTableColumn("Tarih"));
            columns.add(new SeraTableColumn("Sera"));
            columns.add(new SeraTableColumn("Süre"));
            columns.add(new SeraTableColumn("kilo"));
            columns.add(new SeraTableColumn("Gübre Oranı"));
            DatabaseConnection.loadSulamaBilgileri(0, 0);
            sulamaList = DatabaseObject.listSulama;
            System.err.println(sulamaList);
            sulamaList.sort(new Comparator<Sulama>() {
                @Override
                public int compare(Sulama o1, Sulama o2) {
                    return o2.compareTo(o1);
                }
            });
        }

        @Override
        public int getRowCount() {
            return sulamaList.size();
        }

        @Override
        public String getColumnName(int column) {
            return (String) columns.get(column).getHeaderValue();
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return java.util.Date.class;
                case 1:
                    return Sera.class;
                case 2:
                    return Integer.class;
                case 3:
                    return Double.class;
                case 4:
                    return AbstractKarisimGubre.class;
                default:
                    return String.class;
            }
        }

        @Override
        public int getColumnCount() {
            return columns.size();// : columns.size() - 1;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Sulama s = sulamaList.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    return s.getDt();
                case 1:
                    return s.getSera();
                case 2:
                    return s.getSulam_suresi();
                case 3:
                    return s.getToplmaGubreKilosu();
                case 4:
                    return (AdvancedKarisimGubre) AdvancedKarisimGubre.createAdvancedKarisimGubre(s.getGubreMiktari(), s.getToplmaGubreKilosu());
                default:
                    return null;
            }
        }

        class SeraTableColumn extends TableColumn {

            String title;

            public SeraTableColumn(String title) {
                super();
                this.setHeaderValue(title);
                this.title = title;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

        }
    }

    class KarisimGubreTableCellRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JLabel l = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); //To change body of generated methods, choose Tools | Templates.
            AdvancedKarisimGubre ag = (AdvancedKarisimGubre) value;
            l.setText(ag.getKompositGubreLabel());
            return l;
        }
    }

    class SeraTableCellRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            JLabel l = (JLabel) c;
            Sera s = (Sera) value;
            l.setText(s.getKisaltma());
            return l;
        }
    }
}
