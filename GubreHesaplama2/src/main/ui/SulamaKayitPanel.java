/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.ui;

import data.DatabaseObject;
import data.Sera;
import data.Sulama;
import java.awt.Component;
import java.awt.Dimension;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author alptug
 */
public class SulamaKayitPanel extends JPanel {

    UtilDateModel dateModel;
    JFormattedTextField sulamaSuresi;
    DefaultListModel<Sera> listModel;
    Sulama sulama;
    JComboBox<Sera> seraCmbo;

    JDatePanelImpl datePanel;
    JDatePickerImpl datePicker;

    public SulamaKayitPanel() {
        this(null);
    }

    public Sulama getSulama() {
        Date dt = dateModel.getValue();
        Sera sera = (Sera) seraCmbo.getSelectedItem();
        Integer sure =  ((Number) sulamaSuresi.getValue()).intValue();
        if(sure==null)
            return null;
        sulama.setDt(dt);
        sulama.setSera(sera);
        sulama.setSulam_suresi(sure);
        return sulama;
    }

    public void setSulama(Sulama sulama) {
        this.sulama = sulama;
        if (this.sulama == null) {
            this.sulama = new Sulama();
            seraCmbo.setSelectedIndex(-1);
            sulamaSuresi.setValue(null);
            dateModel.setValue(new Date());
        } else {
            seraCmbo.setSelectedItem(sulama.getSera());
            sulamaSuresi.setValue(sulama.getSulam_suresi());
            dateModel.setValue(sulama.getDt());
        }
        dateModel.setSelected(true);
    }

    private void doUI() {
        this.setLayout(new BoxLayout(SulamaKayitPanel.this, BoxLayout.X_AXIS));
        JPanel labelBox = new JPanel();
        labelBox.setLayout(new BoxLayout(labelBox, BoxLayout.Y_AXIS));
        labelBox.setAlignmentY(TOP_ALIGNMENT);

        JPanel dataBox = new JPanel();
        dataBox.setLayout(new BoxLayout(dataBox, BoxLayout.Y_AXIS));
        dataBox.setAlignmentY(TOP_ALIGNMENT);
        dataBox.setAlignmentX(LEFT_ALIGNMENT);

        JLabel l = new JLabel("Tarih : ");
        l.setAlignmentX(LEFT_ALIGNMENT);
        labelBox.add(Box.createRigidArea(new Dimension(0, 4)));
        labelBox.add(l);
        labelBox.add(Box.createRigidArea(new Dimension(0, 6)));

        datePicker.setAlignmentX(LEFT_ALIGNMENT);
        dataBox.add(datePicker);

        l = new JLabel("Sulama Suresi : ");
        l.setAlignmentX(LEFT_ALIGNMENT);
        labelBox.add(Box.createRigidArea(new Dimension(0, 6)));
        labelBox.add(l);
        labelBox.add(Box.createRigidArea(new Dimension(0, 6)));
        sulamaSuresi.setAlignmentX(LEFT_ALIGNMENT);
        dataBox.add(sulamaSuresi);

        l = new JLabel("Sera Listesi : ");
        l.setAlignmentX(LEFT_ALIGNMENT);
        labelBox.add(Box.createRigidArea(new Dimension(0, 6)));
        labelBox.add(l);
        labelBox.add(Box.createRigidArea(new Dimension(0, 6)));
        seraCmbo.setAlignmentX(LEFT_ALIGNMENT);
        dataBox.add(seraCmbo);

        this.add(labelBox);
        this.add(dataBox);
        this.add(Box.createRigidArea(new Dimension(6, 6)));

        this.add(Box.createHorizontalGlue());
    }

    public SulamaKayitPanel(Sulama sulama) {
        dateModel = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        datePanel = new JDatePanelImpl(dateModel, p);
        datePicker = new JDatePickerImpl(datePanel, new LabelDateFormatter());

        sulamaSuresi = new JFormattedTextField();
        sulamaSuresi.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getIntegerInstance())));

        seraCmbo = new JComboBox<Sera>();
        seraCmbo.setRenderer(new SeraCellRenderer());
        for (Sera s : DatabaseObject.listSera) {
            seraCmbo.addItem(s);
        }

        this.setSulama(sulama);

        doUI();
    }

    private class LabelDateFormatter extends AbstractFormatter {

        private String datePatern = "dd/MM/yyyy";

        private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePatern);

        @Override
        public Object stringToValue(String text) throws ParseException {
            return dateFormatter.parseObject(text);
        }

        @Override
        public String valueToString(Object value) throws ParseException {
            if (value != null) {
                Calendar cal = (Calendar) value;
                return dateFormatter.format(cal.getTime());
            }

            return "";
        }
    }

    class SeraCellRenderer extends DefaultListCellRenderer {

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            String val = value == null ? null : ((Sera) value).getAdi();
            return super.getListCellRendererComponent(list, val, index, isSelected, cellHasFocus); //To change body of generated methods, choose Tools | Templates.
        }
    }

}
