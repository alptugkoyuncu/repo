/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.ui;

import data.Gubre;
import data.Sulama;
import gubre_hazirlama.AbstractKarisimGubre;
import gubre_hazirlama.AdvancedKarisimGubre;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

/**
 *
 * @author alptug
 */
public class SulamaKayitveGubrelemeHesaplamaPanel extends JPanel {

    GubreHesaplamaPanel gubreHesaplama;
    SulamaKayitPanel sulamaKayitPanel;
    ButtonPanel buttonPanel;

    public SulamaKayitveGubrelemeHesaplamaPanel() {
        this(null);
    }

    public SulamaKayitveGubrelemeHesaplamaPanel(Sulama s) {
        sulamaKayitPanel = new SulamaKayitPanel();
        gubreHesaplama = new GubreHesaplamaPanel();
        buttonPanel = new ButtonPanel() {
            @Override
            public boolean newButtonClicked() {
                setSulama(null,false);
                return true;
            }

            @Override
            public boolean addButtonClicked() {
                Sulama sulama = getSulama();
                if (sulama != null) {
                    sulama.insert();
                }
                return true;
            }

            @Override
            public boolean deleteButtonClicked() {

                Sulama sulama = getSulama();
                if (sulama != null) {
                    sulama.delete();
                }
                setSulama(null,true);
                return true;
            }

            @Override
            public boolean updateButtonClicked() {

                Sulama sulama = getSulama();
                if (sulama != null) {
                    sulama.update();
                }
                return true;
            }

        };
        if (s == null || s.getGubreMiktari() == null || s.getGubreMiktari().isEmpty()) {
            buttonPanel.setPosition(ButtonPanel.YENI_EKLEME_POSITION);
        } else {
            buttonPanel.setPosition(ButtonPanel.GUNCELLEME_SILME_POSITION);
        }
        doUI();
        setSulama(s,true);
    }

    public void setSulama(Sulama sulama, boolean karisimHesaplaYenilensinMi) {
        buttonPanel.setPosition(sulama == null ? ButtonPanel.YENI_EKLEME_POSITION : ButtonPanel.GUNCELLEME_SILME_POSITION);
        this.sulamaKayitPanel.setSulama(sulama);
        if (karisimHesaplaYenilensinMi) {
            AbstractKarisimGubre ag = (sulama != null && sulama.getGubreMiktari() != null) ? AdvancedKarisimGubre.createAdvancedKarisimGubre(sulama.getGubreMiktari(), sulama.getToplmaGubreKilosu()) : new AdvancedKarisimGubre();
            this.gubreHesaplama.setKarisimGubre(ag);
        }
    }

    public Sulama getSulama() {
        Sulama sulama = this.sulamaKayitPanel.getSulama();
        AbstractKarisimGubre ag = this.gubreHesaplama.getKarisimGubre();
        int miliTime = 0;
        while (ag.getPosition() == AbstractKarisimGubre.GUBRE_KARISIMI_BASLADI) {
            try {
                Thread.sleep(10);
                miliTime += 10;
                if (miliTime > 10000) {
                    return null;
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(SulamaKayitveGubrelemeHesaplamaPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Map<Gubre, Double> gubreMiktari = new HashMap<Gubre, Double>();
        for (Gubre g : ag.getKullanilanGubreler()) {
            gubreMiktari.put(g, ag.getKullanilanGubreMiktari(g));
        }
        sulama.setGubreMiktari(gubreMiktari);
        sulama.setToplmaGubreKilosu(ag.getKarisimKilosu());
        return sulama;
    }

    private void doUI() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        gubreHesaplama.setAlignmentX(LEFT_ALIGNMENT);
        this.add(gubreHesaplama);
        sulamaKayitPanel.setAlignmentX(LEFT_ALIGNMENT);
        this.add(sulamaKayitPanel);
        buttonPanel.setAlignmentX(LEFT_ALIGNMENT);
        this.add(buttonPanel);
    }
}
