/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alptug
 */
public class PTD {
    int type;
    double alfa=0.00385;
    public PTD(int type){
        if(this.type!=1000 || this.type!=100 || this.type!=500)
            this.type=1000;
        else
        this.type = type;
    }
    public double getPTDRes(double t){
        return  1000*(1+getA()*t+getB()*t*t);
    }
    public double getTemp(double res){
        return ((res/(double)type)-1)/(double)alfa;
    }
     private  double getA(){
        return 3.9083*Math.pow(10, -3); 
    }
    private double getB(){
        return -5.775*Math.pow(10, -7);
    }
    private double getC(){
        return -4.183*Math.pow(10, -12);
    }
}
