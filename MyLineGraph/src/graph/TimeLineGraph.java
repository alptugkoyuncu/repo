package graph;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Date;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alptug
 */
public class TimeLineGraph extends JPanel{
    private Double[] xCoordinate;
    private Date[] yCoordinate;
    private int xMax,xMin,yMax,yMin,xOrigin,yOrigin;
    
    private final static int BOSLUK=20;
    public TimeLineGraph(){
    }
    private void printCoordinate(Graphics2D g){
        g.setColor(Color.BLACK);
        // xcoordinate
        g.drawLine(BOSLUK, this.getSize().height/2, this.getSize().width-BOSLUK, this.getSize().height/2);
        //y coordinate
        g.drawLine(BOSLUK, BOSLUK, BOSLUK, this.getSize().height-BOSLUK);
    }
    @Override
    public void paint(Graphics g) {
        super.paint(g); //To change body of generated methods, choose Tools | Templates.
        printCoordinate((Graphics2D) g);
    }
    
    private int getXValue(double val){
        return 0;
    }
    private int getYValue(double val){
        return 0;
    }
}
