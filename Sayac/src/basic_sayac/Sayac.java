package basic_sayac;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class Sayac {

    private int kalanSaat, kalanDakika, kalanSaniye;
    int gecenSure;
    private Timer timer;
    boolean aktif;
    List<SayacListener> listeners;

    public Sayac() {
        this(0, 0, 0);
    }

    public void addSayacListener(SayacListener l) {
        this.listeners.add(l);
    }

    public boolean removeSayacListener(SayacListener l) {
       return this.listeners
                .remove(l);
    }

    private void fireSayacStartedEvent() {
        for (SayacListener l : listeners) {
            l.sayacStarted(kalanSaat, kalanDakika, kalanSaniye);
        }
    }

    public int getKalanSaat() {
        return kalanSaat;
    }

    public int getKalanDakika() {
        return kalanDakika;
    }

    public int getKalanSaniye() {
        return kalanSaniye;
    }

    private void fireSayacStoppedEvent() {

        for (SayacListener l : listeners) {
            l.sayacStopped(kalanSaat, kalanDakika, kalanSaniye);
        }
    }

    private void fireSecondPassedEvent() {
        for (SayacListener l : listeners) {
            l.secondPassed(kalanSaat, kalanDakika, kalanSaniye);
        }
    }

    public Sayac(int saat, int dakika, int saniye) {
        this.kalanSaat = 0;
        this.kalanDakika = 0;
        this.kalanSaniye = 0;
        
        this.kalanSaniye = saniye % 60;
        dakika += saniye / 60;
        this.kalanDakika = dakika % 60;
        saat += dakika / 60;
        this.kalanSaat = saat;
        listeners = new ArrayList<>();
        this.aktif = false;
    }

    public Sayac(int dakika, int saniye) {
        this(0, dakika, saniye);
    }

    public Sayac(int saniye) {
        this(0, 0, saniye);
    }

    public void setSayac(int saat, int dakika, int saniye) throws Exception {
        if (isAktif()) {
            throw new Exception("Timer is working. While timer is working can not change settings");
        } else {
            this.kalanSaat = 0;
            this.kalanDakika = 0;
            this.kalanSaniye = 0;

            this.kalanSaniye = saniye % 60;
            dakika += saniye / 60;
            this.kalanDakika = dakika % 60;
            saat += dakika / 60;
            this.kalanSaat = saat;
        }
    }

    public void start() {
        if (kalanSaat == 0 && kalanDakika == 0 && kalanSaniye == 0) {
            return;
        }
        this.setAktif(true);
        timer = new Timer();
        timer.scheduleAtFixedRate(new SayacTimerTask(), 0, 1000);

        this.fireSayacStartedEvent();
    }

    public void stop() {
        this.setAktif(false);
        timer.cancel();
        fireSayacStoppedEvent();
    }

    private synchronized void setAktif(boolean aktif) {
        this.aktif = aktif;
    }

    public synchronized boolean isAktif() {
        return aktif;
    }

    private class SayacTimerTask extends TimerTask {

        @Override
        public void run() {
            if (isAktif() && kalanSaat == 0 && kalanDakika == 0 && kalanSaniye == 0) {
                stop();
            } else {
                kalanSaniye--;
                gecenSure++;
                if (kalanSaniye < 0) {
                    kalanSaniye = 59;
                    kalanDakika--;
                }
                if (kalanDakika < 0) {
                    kalanDakika = 59;
                    kalanSaat--;
                }
                fireSecondPassedEvent();
                if (kalanSaat < 0 || (kalanSaat <= 0 && kalanDakika <= 0 && kalanSaniye <= 0)) {
                    kalanSaat = 0;
                    kalanDakika = 0;
                    kalanSaniye = 0;
                    stop();
                }
            }
        }

    }
}
