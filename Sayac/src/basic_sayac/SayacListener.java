package basic_sayac;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alptug
 */
public interface SayacListener {
    public void sayacStarted(int h,int m,int s);
    public void sayacStopped(int h,int m,int s);
    public void secondPassed(int h,int m, int s);
}
