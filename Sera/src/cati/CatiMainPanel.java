/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cati;

import java.util.Date;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import main.AbstractSeraMainPanel;
import main.SeraTablePanel;

/**
 *
 * @author alptug
 */
public class CatiMainPanel extends AbstractSeraMainPanel{
    SeraTablePanel tp;
    CatiKayitPanel kayitPanel;
    public CatiMainPanel(){
        super();
        tp = new SeraTablePanel(new CatiTableModel());
        kayitPanel = new CatiKayitPanel();
        super.addKayitPanel(kayitPanel);
        super.addTablePanel(tp);
        
        tp.getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tp.getTable().getSelectionModel().addListSelectionListener(this);
        
    }

    @Override
    protected void tableToKayitPanel(int index) {
        CatiTableModel tableModel = (CatiTableModel)tp.getTable().getModel();
      System.out.println(tableModel.getRowId(index) + " " + tableModel.getValueAt(index, 1) + " "
              + tableModel.getValueAt(index, 2) + " " +tableModel.getValueAt(index,4) + " " + tableModel.getValueAt(index, 8));
        kayitPanel.setCatiId(tableModel.getRowId(index));
        kayitPanel.setAdi((String)tableModel.getValueAt(index,0));
        kayitPanel.setAlani((Float)tableModel.getValueAt(index,1));
        kayitPanel.setMasraf((Float)tableModel.getValueAt(index,3));
        kayitPanel.setTarihi((Date)tableModel.getValueAt(index, 7));
    }   
}
