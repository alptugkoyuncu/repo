/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cati;

import cati.db.RowData_Cati;
import java.util.Date;
import main.AbstractSeraTableModel;

/**
 *
 * @author alptug
 */
public class CatiTableModel extends AbstractSeraTableModel {
    private final static String[] h = {"Sera Adı","Toplam Alan","Sera Özellikleri","Yapım Masrafı","Bakım Masrafı","Ürün Masrafları","Toplam Kazanç","Yapılış Tarihi","Bilgiler"};
    public CatiTableModel(){
        this(false);
    }
    
    public CatiTableModel(boolean hasPaging){
        super(hasPaging);
        super.headings = h;
        this.load();
    }

    @Override
    public void load() {
    RowData_Cati rd = new RowData_Cati();
        Object[][] temp =rd.getData();
        this.rowData = temp == null ? new Object[0][0] : rd.getData();
    }

    @Override
    public void unload() {
        this.rowData = null;
        System.gc();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return rowData[rowIndex][columnIndex+1];
    }
    
    @Override
    public Integer getRowId(int rowIndex){
        return (Integer) this.rowData[rowIndex][0];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        
        switch(columnIndex){
//      case 1 : case 3 : case 4 :case 5 :case 6 :return Float.class;
            case 7 : return java.sql.Date.class;
            case 2: case 8: return Integer.class;
            default : return String.class;
        }
    }
    
}

