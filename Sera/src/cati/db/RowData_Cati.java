/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cati.db;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.db.DBConnection;
import util.JCalendar;

/**
 *
 * @author alptug
 */
public class RowData_Cati extends DBConnection {

    @Override
    public Object[][] getData(String[] cols) {
        if(cols.length<=0)
            return null;
                throw new UnsupportedOperationException("Not supported yet.");

        
    }
//(select count(*) from bilgi where id = kisiler_id and tablo_adi = 'kisiler')  as bilgi "
    @Override
    public Object[][] getData() {
        String[] cols = {"cati_id","adi","alani","ozellikleri","yapilis_masrafi",
            "bakim_masrafi","urun_masraflari","toplam_kazanc","tarihi","bilgiler"};
      
        String query=
                "Select "
                + " cati.cati_id,"
                + " adi,"
                + " alani,"
                + " ( Select count(*) from cati_TO_cati_ozellikleri where cati.cati_id = cati_TO_cati_ozellikleri.cati_id ) AS ozellikleri,"
                + " yapilis_masrafi,"
                + " ( SELECT 'YAPIM ASAMASINDA' ) AS bakim_masrafi,"
                + " ( SELECT 'YAPIM ASAMASINDA' ) AS urun_masraflari,"
                + " ( SELECT 'YAPIM ASAMASINDA' ) AS toplam_kazanc,"
                + " tarihi,"
                + " ( SELECT count(*) from bilgi where id = cati.cati_id AND tablo_adi = 'cati') AS bilgiler"
                + " FROM"
                +   " cati"
                + " LEFT OUTER JOIN "
                +   " cati_TO_cati_ozellikleri "
                + " ON"
                + " cati_TO_cati_ozellikleri.cati_id = cati.cati_id";
         try {
            return getData(query, cols);
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Cati.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public int insertData(Object[] data) {
        String query= 
                "INSERT INTO "
                + "cati "
                + " (adi,alani,tarihi,yapilis_masrafi)"
                + " VALUES('" + (String)data[0] + "',"+(Float)data[1] + ",'" + (String)data[2] + "'," + (Float)data[3] 
                + ")";
        System.out.println(query);
        return this.processQuery(query);
       
    }

    @Override
    public boolean updateData(int id, Object[] data) {
        String query =
                "UPDATE "
                +   "cati "
                +"SET "
                + " adi = '" + (String)data[0] 
                + "', alani = " + (Float)data[1]
                + ", tarihi = " + ((JCalendar)data[2]).sqlMakeDate()
                + ", yapilis_masrafi = " + (Float)data[3]
                +" WHERE "
                + " cati_id = " + id;
        
        return super.processQuery(query) >=1;
    }

    @Override
    public int deleteData(int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
