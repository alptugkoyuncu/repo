/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cati.db;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.db.DBConnection;

/**
 *
 * @author alptug
 */
public class RowData_CatiOzellikleri extends DBConnection{

    public Object[][] getDataForSpecificCati(int catiId){
        String[] cols = {"cati_TO_cati_ozellikleri_id","cati_ozellikleri_id","ozellik","grup_id","tarih"};
        String query = "Select "
                +   "cati_TO_cati_ozellikleri_id,"
                +   "cati_ozellikleri_id,"
                +   "ozellik,"
                +   "grup_id,"
                +   "tarih"
                + "FROM "
                +   "cati"
                + "LEFT OUTER JOIN "
                +   "cati_TO_cati_ozellikleri"
                + "ON"
                +   "cati.cati_id = cati_TO_cati_ozellikleri_id"
                + "INNER JOIN "
                +   "cati_ozellikleri"
                + "ON"
                + " cati_TO_cati_ozellikleri_id = cati_ozellikleri_id"
                + "WHERE"
                +   "cati.cati_id = " + catiId;
        try {
            return getData(query, cols);
        } catch (SQLException ex) {
            Logger.getLogger(RowData_CatiOzellikleri.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
                
    }
    @Override
    public Object[][] getData(String[] cols) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object[][] getData() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int insertData(Object[] data) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean updateData(int id, Object[] data) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int deleteData(int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
