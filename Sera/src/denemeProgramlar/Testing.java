/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package denemeProgramlar;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author alptug
 */
public class Testing {

    class A {
    }

    class B {

        public B() {
        }
    ;

    }class C extends A {
    }

    public static void main(String[] args) {
        Number s = null;
        Integer d = 2;
        HashMap<String, Double> map = new HashMap<String, Double>();
        ValueComparator bvc = new ValueComparator(map);
        TreeMap<String, Double> sorted_map = new TreeMap<String, Double>(bvc);

        map.put("A", 99.5);
        map.put("B", 67.4);
        map.put("C", 67.4);
        map.put("D", 67.3);

        System.out.println("unsorted map: " + map);

        sorted_map.putAll(map);

        System.out.println("results: " + sorted_map);
        System.out.println(new java.util.Date(1000 * 60 * 60 * 30));
        System.out.println(new java.sql.Date(1000 * 60 * 60 * 30));


    }
}

class ValueComparator implements Comparator<String> {

    Map<String, Double> base;

    public ValueComparator(Map<String, Double> base) {
        this.base = base;
    }

    // Note: this comparator imposes orderings that are inconsistent with equals.    
    @Override
    public int compare(String a, String b) {
        return this.compareStrings(a, b) * -1;
    }
    //a once is 1 b once is -1 ayni ise 0

    private int compareStrings(String a, String b) {
        String c = a.toLowerCase();
        String d = b.toLowerCase();
        int len = a.length() >= b.length() ? b.length() : a.length();
        for (int i = 0; i < len; i++) {
            if (c.charAt(i) > d.charAt(i)) {
                return -1;
            } else if (d.charAt(i) > c.charAt(i)) {
                return 1;
            }
        }
        return a.length() > b.length() ? -1 : 1;
    }
}
