/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ilaclama;

import ilaclama.db.RowData_Ilaclama;
import javax.swing.ListSelectionModel;
import main.AbstractSeraMainPanel;
import main.SeraTablePanel;
import util.UsefulFunctions;

/**
 *
 * @author alptug
 */
public class IlaclamaMainPanel extends AbstractSeraMainPanel {

    SeraTablePanel stp;
    IlaclamaKayitPanel kp;

    public IlaclamaMainPanel() {
        stp = new SeraTablePanel(new IlaclamaTableModel());
        kp = new IlaclamaKayitPanel();
        super.addTablePanel(stp);
        super.addKayitPanel(kp);
        stp.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        stp.getTable().getSelectionModel().addListSelectionListener(this);
    }

    @Override
    protected void tableToKayitPanel(int index) {
        kp.yeni();
        RowData_Ilaclama rd = new RowData_Ilaclama();
        IlaclamaTableModel itm = ((IlaclamaTableModel) stp.getTable().getModel());
        java.sql.Date date = new java.sql.Date(((java.util.Date) itm.getValueAt(index, 0)).getTime());
        String query;
        String[] cols;
        //
        Object[][] data;//butun ilaclama bilgileri;
        Object[][] allCati = rd.getSeraInfo();// kayitli butun catilari db den alir
        Object[][] allHastalik = rd.getHastalikInfo();
        Object[][] allIlacForHastalik = null;
        Integer[] selectedCati = new Integer[allCati.length];
        Integer[][] selectedHastalik = new Integer[allCati.length][allHastalik.length];
        Integer[][][] selectedIlac = new Integer[allCati.length][allHastalik.length][];

        UsefulFunctions.resetNumber(selectedCati);
        
        for (int i = 0; i < selectedHastalik.length; i++) {
            selectedHastalik[i] = new Integer[allHastalik.length];
            UsefulFunctions.resetNumber(selectedHastalik[i]);
        }
        
        //secili catilari ayarla
        query = "SELECT cati_id from ilaclama where ilaclama_tarihi = '" + date+"'";
        cols = new String[]{"cati_id"};
        data = rd.getCustomData(query, cols);

        for (int i = 0; data != null && i < data.length; i++) {
            Integer catiId = (Integer) data[i][0];
            for (int j = 0; j < allCati.length; j++) {
                if ((Integer) allCati[j][0] == catiId) {
                    selectedCati[j] = catiId;
                }
            }
        }

        //her cati icin olan secili hastaliklari ayarla
        cols = new String[]{"hastalik_id"};
        for (int i = 0; i < selectedHastalik.length; i++) {
            if (selectedCati[i] == 0) {
                continue;
            }
            query = "SELECT hastalik_id FROM ilaclama "
                    + " where "
                    + "ilaclama_tarihi = '" + date + "' "
                    + "and "
                    + " cati_id = " + selectedCati[i];
            data = rd.getCustomData(query, cols);
            for (int j = 0; j < allHastalik.length; j++) {
                int hastalikId = (Integer) allHastalik[j][0];
                for (int k = 0; k < data.length; k++) {
                    if ((Integer) data[k][0] == hastalikId) {
                        selectedHastalik[i][j] = hastalikId;
                    }
                }
            }
        }

        //her catida olan hastaliklar icin kullanilmis ilaclari ayarla
        for (int i = 0; i < allCati.length; i++) {
            selectedIlac[i] = new Integer[allHastalik.length][];
            for (int j = 0; j < allHastalik.length; j++) {
                int hastalikId =(Integer)allHastalik[j][0];
                allIlacForHastalik = rd.getIlacInfo(hastalikId);
                selectedIlac[i][j] = new Integer[allIlacForHastalik.length];
                UsefulFunctions.resetNumber(selectedIlac[i][j]);
                if(selectedHastalik[i][j]<=0)
                    continue;
                query = "SELECT ilac_id from ilaclama "
                        + "where"
                        + " ilaclama_tarihi = '" + date + "' "
                        + " and cati_id = " + selectedCati[i] + " "
                        + " and  hastalik_id = " + selectedHastalik[i][j];
                data = rd.getCustomData(query, new String[]{"ilac_id"});

                for (int k = 0; k < selectedIlac[i][j].length; k++) {
                    int ilacId = (Integer) allIlacForHastalik[k][0];
                    for (int m = 0; m < data.length; m++) {
                        if (ilacId == (Integer) data[m][0]) {
                            selectedIlac[i][j][k] = ilacId;
                        }
                    }
                }
            }
        }
        kp.setSelectedCati(selectedCati);
        kp.setSelectedHastalik(selectedHastalik);
        kp.setSelectedIlac(selectedIlac);
     /*   Map m = new TreeMap();
        for(int i=0; i<selectedIlac.length; i++){
            Map l2 = new TreeMap();
            for(int j=0; j<selectedIlac[i].length;j++){
                List l3 = new ArrayList();
                for(int k = 0; k<selectedIlac[i][j].length; k++)
                    l3.add(selectedIlac[i][j][k]);
                l2.put(selectedHastalik[i][j],l3);
            }
            m.put(selectedCati[i],l2);
        }
        System.out.println("LIST : " + m);
        ArrayList list = new ArrayList();
        for(int i=0; i<selectedCati.length; i++)
            list.add(selectedCati[i]);
        System.out.println("LIST : " + list);
        m = new TreeMap();
        for(int i=0; i<selectedHastalik.length; i++){
            ArrayList l = new ArrayList();
            for(int j=0; j<selectedHastalik[i].length; j++){
                l.add(selectedHastalik[i][j]);
            }
            m.put(selectedCati[i],l);
        }
        System.out.println("LIST : " + m);
*/
        
    }
    
}
