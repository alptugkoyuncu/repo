/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ilaclama;

import ilaclama.db.RowData_Ilaclama;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import main.AbstractSeraTableModel;

/**
 *
 * @author alptug
 */
public class IlaclamaTableModel extends AbstractSeraTableModel {

    public IlaclamaTableModel() {
        this(false);
    }

    public IlaclamaTableModel(boolean hasPaging) {
        super(false);
        this.rowData = this.arrangeRowData(new RowData_Ilaclama().getTableData());

        this.headings = new String[]{"Tarih", "Sera", "Hastalık", "İlaç"};
    }

    @Override
    public void load() {
        this.rowData = this.arrangeRowData(new RowData_Ilaclama().getTableData());
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return this.rowData[rowIndex][columnIndex];
    }

    public static Object[][] arrangeRowData(Object[][] rd) {
        Map<Date, ArrayList> map = new HashMap<Date, ArrayList>();
        for (int i = 0; i < rd.length; i++) {
            Date d = (Date) rd[i][4];
            List l;
            if (map.containsKey(d)) {
                l = map.get(d);
                l.add(rd[i]);
            } else {
                l = new ArrayList();
                l.add(rd[i]);
                map.put(d, (ArrayList) l);
            }
        }
        // mapi sirala
        TreeMap<Date, ArrayList> sorted_map = new TreeMap<Date, ArrayList>(map);
        Object[][] last = new Object[sorted_map.size()][4];
        int i = 0;
        for (Date d : sorted_map.keySet()) {
            ArrayList l = sorted_map.get(d);
            Object[] o;
            Set<String> cati = new HashSet<String>();
            Set<String> hastalik = new HashSet<String>();
            Set<String> ilac = new HashSet<String>();
            for (int j = 0; j < l.size(); j++) {
                o = (Object[]) l.get(j);
                cati.add((String) o[5]);
                hastalik.add((String) o[6]);
                ilac.add((String) o[7]);
                
            }
            last[i] = new Object[4];
            last[i][0] = d;
            last[i][1] = joint(cati.toArray());
            last[i][2] = joint(hastalik.toArray());
            last[i][3] = joint(ilac.toArray());
            i++;
        }
        return last;
    }
    public static String joint(Object[] o){
        if(o==null || o.length<=0)
            return "";
        String str="";
        for(int i=0; i<o.length-1; i++)
            str+=o[i]+",";
        return str+o[o.length-1];
    } 
}
