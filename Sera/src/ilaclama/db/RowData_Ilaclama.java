/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ilaclama.db;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.db.DBConnection;

/**
 *
 * @author alptug
 */
public class RowData_Ilaclama extends DBConnection{

    public Object[][] getSeraInfo() {
        String query = "SELECT "
                + "cati_id,adi "
                + " FROM "
                + " cati ";
        try { 
            return getData(query,new String[]{"cati_id","adi"});
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Ilaclama.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Object[][] getHastalikInfo() {
        String query = "SELECT "
                + "hastalik_id,hastalik_adi,(Select 0) as miktar "
                + " FROM "
                + " hastalik ";
        try { 
            return getData(query,new String[]{"hastalik_id","hastalik_adi","miktar"});
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Ilaclama.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Object[][] getIlacInfo(Integer id) {
        String query =  "SELECT "
                + " ilac.ilac_id, ilac_adi "
                + "FROM"
                + " hastalik_ilac_iliskisi hi  "
                + " INNER JOIN ilac "
                + " ON "
                + " ilac.ilac_id = hi.ilac_id "
                + " WHERE hi.hastalik_id = " + id;
        try {
            return getData(query,new String[]{"ilac_id","ilac_adi"});
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Ilaclama.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
    }

    @Override
    public Object[][] getData(String[] cols) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object[][] getData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insertData(Object[] data) {
        String query = "INSERT INTO "
                + "ilaclama "
                + "(cati_id,hastalik_id,ilac_id,ilaclama_tarihi)"
                + " VALUES "
                + "("+ 
                (Integer)data[1]+"," + (Integer)data[2] + ","+(Integer)data[3]+","+(String)data[4]+
                ")";
        return super.processQuery(query);
    }

    @Override
    public boolean updateData(int id, Object[] data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int deleteData(int id) {
        String query = "DELETE FROM ilaclama "
                + "WHERE ilaclama_id = " + id;
        return super.processQuery(query);
    }
    public Integer[] getIdByDate(String date){
        String query = "SELECT ilaclama_id FROM ilaclama WHERE ilaclama_tarihi = "+ date;
        String[] cols = {"ilaclama_tarihi"};
        try {
            return (Integer[])this.getData(query, cols)[0];
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Ilaclama.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Object[][] getTableData() {
        String[] cols = 
                new String[]{"ilaclama_id","cati_id","hastalik_id","ilac_id","ilaclama_tarihi",
                        "cati_adi","hastalik_adi","ilac_adi"};
        String query="Select ilaclama.*,cati.adi as cati_adi,hastalik.hastalik_adi,ilac.ilac_adi "
                + " FROM ilaclama "
                + " left outer join cati "
                + " ON cati.cati_id = ilaclama.cati_id "
                + " LEFT OUTER JOIN hastalik "
                + " ON hastalik.hastalik_id = ilaclama.hastalik_id "
                + " LEFT OUTER JOIN ilac "
                + " ON ilac.ilac_id = ilaclama.ilac_id Order By ilaclama.ilaclama_tarihi ASC "
                ;
        try {
            return super.getData(query, cols);
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Ilaclama.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }   
    }
    public Object[][] getCustomData(String query,String[] cols){
        try {
            return super.getData(query,cols);
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Ilaclama.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
