/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package iscilik;

import java.sql.Date;
import kisi.db.RowData_Kisi;

/**
 *
 * @author alptug
 */
public class Isciler {
    String adi,soyadi,telefon;
    Date kayitTarihi;
    Boolean durum;
    Integer id;
    public Isciler(String adi, String soyadi, String telefon, Date kayitTarihi, Boolean durum, Integer id) {
        this.adi = adi;
        this.soyadi = soyadi;
        this.telefon = telefon;
        this.kayitTarihi = kayitTarihi;
        this.durum = durum;
        this.id = id;
    }
    public static Isciler[] getAllIsciler(){
        RowData_Kisi rd = new RowData_Kisi();
//                    String[] colNames = new String[]{"kisiler_id", "type", "adi", "soyadi", "telefon", "kayit_tarihi", "durum", "bilgi"};
   
        Object[][] o = rd.getData();
        Isciler[] k = new Isciler[o.length];
        for(int i=0; i<k.length; i++){
            Object[] t = o[i];
            k[i] = new Isciler((String)t[2],(String)t[3],(String)t[4],(Date)t[5],(Boolean)t[6],(Integer)t[0]);
        }
        return k;
    }
    public String getAdi() {
        return adi;
    }

    public Boolean getDurum() {
        return durum;
    }

    public Integer getId() {
        return id;
    }

    public Date getKayitTarihi() {
        return kayitTarihi;
    }

    public String getSoyadi() {
        return soyadi;
    }

    public String getTelefon() {
        return telefon;
    }
    public String getDbColumnHeader(){
        return this.getId()+"_"+ this.getAdi();
    }
}
