/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package iscilik;

import iscilik.db.RowData_Is;
import iscilik.db.RowData_Iscilik;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.BadLocationException;
import main.KayitFormIslemleri;
import main.AbstractSeraTableModel;
import urun.db.RowData_Urun;
import util.JCalendar;
import util.MyComboBoxModel;
import util.MyComboBoxRenderer;

/**
 *
 * @author alptug
 */
public class IscilikKayitPanel extends javax.swing.JPanel implements KayitFormIslemleri {

    /**
     * Creates new form IscilikKayitPanel
     */
    private Float yarimGunlukMaxCalismaSaati;
    MyComboBoxModel mdlUrun, mdlIs;
    MyComboBoxRenderer comboRenderer;
    MyCellEditor myCellEditor = new MyCellEditor();
    Map<Integer, Map> calismaSaatleri = new HashMap<Integer, Map>();//sera sayisi,is sayisi,insan sayisi,am-pm
//    Float[][][][] calismaSaatleri;
    private int isSayisi;
    private int catiSayisi;
    private int isciSayisi;
    IsTableModel tmIs = new IsTableModel(null);
    YerTableModel tmYer = new YerTableModel(calismaSaatleri);
    IsciTableModel tmIsci = new IsciTableModel(null);

    public IscilikKayitPanel() {
        mdlIs = new MyComboBoxModel(new RowData_Is().getData(), 0);
        tmYer = new YerTableModel(calismaSaatleri);
        this.setGunlukMaxCalismaSaati();
        initComponents();
        this.jtSera.setModel(tmYer);
        this.jtSera.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.jtSera.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    return;
                }
                if (jtSera.getSelectedRow() == -1) {
                    jtIs.clearSelection();
                    jtIs.setModel(new DefaultTableModel());
                } else {
                    Integer selectedItemId = tmYer.getMahsulId(jtSera.getSelectedRow());
                    Map<Integer, Map> m2 = tmYer.getMap().get(selectedItemId);
                    tmIs.setMap(m2);
                    jtIs.setModel(tmIs);
                }
            }
        });

        this.jtIs.setModel(new DefaultTableModel());
        this.jtIs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.jtIs.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    return;
                }
                if (jtIs.getSelectedRow() == -1) {
                    jtIsci.clearSelection();
                    jtIsci.setModel(new DefaultTableModel());
                } else {
                    Integer selectedItemId = tmIs.getIsId(jtIs.getSelectedRow());
                    Map<Integer, Map> m2 = tmIs.getMap();
                    tmIsci = new IsciTableModel(m2 == null ? null : m2.get(selectedItemId));
                    jtIsci.setModel(tmIsci);
                }
            }
        });
        this.jtIsci.setModel(new DefaultTableModel());
        this.jtIsci.setDefaultRenderer(CalismaSaatiPaneli.class, new MyRenderer());
        this.jtIsci.setDefaultEditor(CalismaSaatiPaneli.class, myCellEditor);
        this.jtIsci.setRowHeight((int) (this.jtIsci.getRowHeight() * 1.5));

    }

    public void loadCalismaSaatleri(java.util.Date dt) {
        ((JCalendar) this.jcDate).setDate(dt);
        Object[][] val = new RowData_Iscilik().getDataByDate(util.UsefulFunctions.sqlMakeDateFunc(dt));
        for (int i = 0; i < val.length; i++) {
            Float f = (Float) val[i][4];
            this.updateValues(f, (Integer) val[i][0], (Integer) val[i][1], (Integer) val[i][2], (String) val[i][3]);
        }
    }

    private void setGunlukMaxCalismaSaati() {
        this.yarimGunlukMaxCalismaSaati = new Float(4.0);
    }

    public Float getYarimGunlukMaxCalismaSaati() {
        if(this.yarimGunlukMaxCalismaSaati==null || this.yarimGunlukMaxCalismaSaati==0)
            return new Float(4);
        return yarimGunlukMaxCalismaSaati;
    }
    public int getGunlukCalismaUcret(java.util.Date dt) {
        return RowData_Iscilik.getGunlukCalismaUcreti(util.UsefulFunctions.sqlMakeDateFunc(dt));
    }

    /**
     *
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jcDate = new JCalendar(this);
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtIsci = new javax.swing.JTable();
        genelCalismaSaati = new CalismaSaatiTextField();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtSera = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jtIs = new javax.swing.JTable();

        jLabel1.setText("Tarih");

        jLabel2.setText("Çalışma Saatleri");
        jLabel2.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jLabel4.setText("Sera");

        jLabel5.setText("İş");

        jScrollPane2.setViewportView(jtIsci);

        genelCalismaSaati.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                genelCalismaSaatiActionPerformed(evt);
            }
        });

        jLabel3.setText("Genel");

        jScrollPane1.setViewportView(jtSera);

        jScrollPane3.setViewportView(jtIs);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 362, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 10, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jcDate, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(128, 128, 128)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(18, 18, 18)
                                                    .addComponent(genelCalismaSaati, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(genelCalismaSaati, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(243, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void genelCalismaSaatiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_genelCalismaSaatiActionPerformed
        // TODO add your handling code here:
        CalismaSaatiTextField tmp = (CalismaSaatiTextField) this.genelCalismaSaati;
        IsciTableModel tm = (IsciTableModel) this.jtIsci.getModel();
        if (!tmp.isValidValue()) {
            return;
        }
        int rows = tm.getRowCount();
        Float val = tmp.getValue();
        for (int i = 0; i < rows; i++) {
            tm.setValueAt(val, i, 1);
            tm.setValueAt(val, i, 2);
        }

    }//GEN-LAST:event_genelCalismaSaatiActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField genelCalismaSaati;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jcDate;
    private javax.swing.JTable jtIs;
    private javax.swing.JTable jtIsci;
    private javax.swing.JTable jtSera;
    // End of variables declaration//GEN-END:variables

    @Override
    public Object[] ekle() {
        Object[] data = new Object[8];
        data[0] = -1;
        data[4] = ((JCalendar) this.jcDate).sqlMakeDate();
        RowData_Iscilik rd = new RowData_Iscilik();
        for (Integer yerlerId : this.calismaSaatleri.keySet()) {
            data[3] = yerlerId;
            Map<Integer, Map> isler = this.calismaSaatleri.get(yerlerId);
            for (Integer islerId : isler.keySet()) {
                data[2] = islerId;
                Map<Integer, Map> isciler = isler.get(islerId);
                for (Integer iscilerId : isciler.keySet()) {
                    data[1] = iscilerId;
                    Map<String, Float> zamanlar = isciler.get(iscilerId);
                    for (String t : zamanlar.keySet()) {
                        data[7] = t;
                        data[5] = zamanlar.get(t);
                        data[6] = (this.getGunlukCalismaUcret(((JCalendar) this.jcDate).getDate().getTime())*zamanlar.get(t))/this.getGunlukMaxCalismaSaati();
                        if ((Float) data[5] == 0.0) {
                            continue;
                        }
                        rd.insertData(data);
                    }
                }
            }
        }
        //table icin datalari gonder
        return null;
    }

    @Override
    public int yeni() {
        for (Integer yerler : this.calismaSaatleri.keySet()) {
            Map<Integer, Map> isler = this.calismaSaatleri.get(yerler);
            for (Integer isId : isler.keySet()) {
                Map<Integer, Map> isciler = isler.get(isId);
                for (Integer isciId : isciler.keySet()) {
                    Map<String, Float> zaman = isciler.get(isId);
                    if (zaman != null) {
                        zaman.clear();
                    }
                    zaman = null;
                }
                isciler.clear();
                isciler = null;
            }
            isler.clear();
            isler = null;
        }
        this.calismaSaatleri.clear();
        jtSera.clearSelection();
        jtSera.repaint();
        ((JCalendar) this.jcDate).setDate(new java.util.Date());
        ((CalismaSaatiTextField) this.genelCalismaSaati).setValue(new Float(0.0));
        return 0;
    }

    @Override
    public Object[] guncelle() {
        String date = ((JCalendar) this.jcDate).sqlMakeDate();
        new RowData_Iscilik().deleteData(date);
        return this.ekle();
    }

    public float getGunlukMaxCalismaSaati() {
        return this.getYarimGunlukMaxCalismaSaati()*2;
    }

    class IsciTableModel extends AbstractSeraTableModel {

        Isciler[] isciler;
        String[] cols;
        Map<Integer, Map> map;
        CalismaSaatiPaneli[][] calismaSaatiPaneli;

        public Map<Integer, Map> getMap() {
            return map;
        }

        public void setMap(Map<Integer, Map> map) {
            this.map = map;
            this.refreshTable();
        }

        @Deprecated
        public Map<String, Float> addTimeToSaatler(Integer key, String time, Float val) {
            Map<String, Float> m;
            if (this.map != null) {
                m = this.map.get(key);
                if (m == null) {
                    m = new HashMap<String, Float>();
                }
            } else {
                this.map = new HashMap<Integer, Map>();
                m = new HashMap<String, Float>();
            }
            m.put(time, val);
            return m;

        }

        private Map<String, Float> removeTimeFromSaatler(int key, String time) {
            Map<String, Float> m;
            if (this.map == null) {
                return null;
            }
            m = this.map.get(key);
            if (m == null) {
                return null;
            }
            m.remove(time);
            if (m.isEmpty()) {
                return m;
            }

            return null;

        }

        public Map<String, Float> addSaatlerToIsciler(Map<Integer, Map> m, int isciId, String time, Float val) {
            this.map = m;
            Map<String, Float> temp = this.map.get(isciId);
            if (temp == null) {
                temp = new HashMap<String, Float>();
            }
            temp.put(time, val);
            this.map.put(isciId, temp);
            return temp;
        }

        public Map<Integer, Map> removeSaatlerFromIsciler(int key) {
            if (this.map == null) {
                return null;
            }
            this.map.remove(key);
            if (this.map.isEmpty()) {
                return this.map;
            }
            return null;
        }

        public IsciTableModel(Map<Integer, Map> m) {
            cols = new String[]{"Adı", "AM", "PM"};
            this.map = m;
            this.load();
        }

        @Override
        public String getColumnName(int column) {
            return cols[column]; //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public int getRowCount() {
            return isciler != null ? isciler.length : 0;
        }

        @Override
        public int getColumnCount() {
            return cols.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
//            System.err.println("isci table model get value");

            switch (columnIndex) {
                case 0:
                    return isciler[rowIndex].getAdi() + " " + isciler[rowIndex].getSoyadi();
                case 1:
                    return calismaSaatiPaneli[rowIndex][0];
                case 2:
                    return calismaSaatiPaneli[rowIndex][1];
                default:
                    return null;
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return String.class;
                case 1:
                    return CalismaSaatiPaneli.class;
                case 2:
                    return CalismaSaatiPaneli.class;
                default:
                    return String.class;
            }
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            Float val = (Float) aValue;
            if (columnIndex != 0) {
                String time = columnIndex == 1 ? "AM" : "PM";
                int keyYer = tmYer.getMahsulId(jtSera.getSelectedRow());
                int keyIs = tmIs.getIsId(jtIs.getSelectedRow());
                int keyIsci = isciler[rowIndex].getId();
                updateValues(val, keyYer, keyIs, keyIsci, time);
                this.calismaSaatiPaneli[rowIndex][columnIndex - 1].setValue(val);
                this.fireTableCellUpdated(rowIndex, columnIndex);
            }
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return columnIndex != 0;
        }

        @Override
        public void load() {
            isciler = Isciler.getAllIsciler();
            calismaSaatiPaneli = new CalismaSaatiPaneli[isciler.length][2];
            for (int i = 0; i < calismaSaatiPaneli.length; i++) {
                Float amValue = new Float(0.0);
                Float pmValue = new Float(0.0);
                if (this.map != null && !this.map.isEmpty()) {
                    Map<String, Float> m = this.map.get(isciler[i].getId());
                    if (m != null && !m.isEmpty()) {
                        amValue = m.get("AM") != null ? m.get("AM") : new Float(0.0);
                        pmValue = m.get("PM") != null ? m.get("PM") : new Float(0.0);
                    }
                }
                calismaSaatiPaneli[i] = new CalismaSaatiPaneli[2];
                calismaSaatiPaneli[i][0] = new CalismaSaatiPaneli(amValue);
                calismaSaatiPaneli[i][1] = new CalismaSaatiPaneli(pmValue);
            }
        }
    }

    class CalismaSaatiTextField extends JTextField {

        private String slash = "/";
        Float value;

        public CalismaSaatiTextField() {
            this(new Float(0.0));
        }

        public CalismaSaatiTextField(Float value) {
            this.setValue(value);
            this.getCaret().addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    caretPositionChanged(e);
                }
            });
            this.addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent e) {
                    int index = getSlashPosition();
                    JTextField tmp = (JTextField) e.getComponent();
                    tmp.setSelectionStart(0);
                    tmp.setSelectionEnd(index);
                }

                @Override
                public void focusLost(FocusEvent e) {
                }
            });

        }

        private void caretPositionChanged(ChangeEvent e) {

            String txt = this.slash + getYarimGunlukMaxCalismaSaati();
            int pos = this.getCaretPosition();
            if (pos == 0) {
                return;
            }
            String temp;
            try {
                temp = this.getText(0, pos);
            } catch (BadLocationException ex) {
                if (this.getSlashPosition() > 0) {
                    this.setCaretPosition(this.getSlashPosition());
                } else {
                    this.setCaretPosition(0);
                }
                Logger.getLogger(IscilikKayitPanel.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
            if (temp.indexOf(this.slash) != -1) {
                if (this.getSlashPosition() > 0) {
                    this.setCaretPosition(this.getSlashPosition());
                } else {
                    this.setCaretPosition(0);
                }
            }
        }

        private int getSlashPosition() {
            String tmp = this.getText();
            return tmp.indexOf(this.slash);
        }

        public void setValue(Float value) {
            this.value = value;
            this.setText(value + slash + getYarimGunlukMaxCalismaSaati());
        }

        public Float getValue() {
            isValidValue();
            return value;
        }

        @Override
        public void setText(String t) {
            super.setText(t); //To change body of generated methods, choose Tools | Templates.
            this.value = Float.parseFloat(t.trim().substring(0, getSlashPosition()));
            
        }

        public boolean isValidValue() {
            try {
                value = Float.parseFloat(this.getText().trim().substring(0, getSlashPosition()));
                return true;
            } catch (Exception ex) {
                System.err.println("saat values is not valid");
                value = new Float(0.0);
                this.setValue(value);
                return false;
            }
        }
    }

    class CalismaSaatiPaneli extends JPanel {

        private CalismaSaatiTextField txt;
        private JCheckBox chk = new JCheckBox();

        public CalismaSaatiPaneli() {
            this(new Float(0.0));
        }

        private CalismaSaatiPaneli(Float value) {
            txt = new CalismaSaatiTextField();
            this.setValue(value);
            this.setLayout(new BorderLayout());
            this.add(chk, BorderLayout.WEST);
            this.add(txt, BorderLayout.CENTER);
            chk.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (chk.isSelected()) {
                        Float d = txt.getValue() == new Float(0.0) ? new Float(4.0) : txt.getValue();
                        txt.setValue(d);
                    } else {
                        txt.setValue(new Float(0.0));
                    }
                }
            });

        }

        public void setCalismaSaati() {
        }

        public void setValue(Float aFloat) {
            this.txt.setValue(aFloat);
            this.chk.setSelected(aFloat != 0);
        }

        public Float getCalismaSaati() {
            return this.txt.getValue();
        }

        public boolean isValidValue() {
            return this.txt.isValidValue();
        }
    }

    private class MyRenderer extends CalismaSaatiPaneli implements TableCellRenderer {

        public MyRenderer() {
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            return (CalismaSaatiPaneli) value;
        }
    }

    class MyCellEditor extends AbstractCellEditor implements TableCellEditor {

        CalismaSaatiPaneli pnl;
        Float val;

        @Override
        public Object getCellEditorValue() {
            return pnl.getCalismaSaati();
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            pnl = (CalismaSaatiPaneli) value;
            for (ActionListener l : pnl.chk.getListeners(ActionListener.class)) {
                pnl.chk.removeActionListener(l);
            }
            pnl.chk.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (pnl.chk.isSelected()) {
                        Float d = pnl.txt.getValue() == new Float(0.0) ? new Float(4.0) : pnl.txt.getValue();
                        pnl.txt.setValue(d);
                        System.err.println(d);
                    } else {
                        pnl.txt.setValue(new Float(0.0));
                    }
                    stopCellEditing();
                }
            });
            return pnl;
        }

        @Override
        public boolean stopCellEditing() {
            if (pnl.isValidValue()) {
                super.fireEditingStopped();
            } else {
                super.fireEditingCanceled();
            }
            return true; //To change body of generated methods, choose Tools | Templates.
        }
    }

    class YerTableModel extends AbstractSeraTableModel {

        Map<Integer, Map> map;

        public Map<Integer, Map> getMap() {
            return map;
        }

        public void setMap(Map<Integer, Map> map) {
            this.map = map;
            this.refreshTable();
        }

        public Map<Integer, Map> removeIslerFromYerler(int key) {
            if (this.map == null) {
                return null;
            }
            this.map.remove(key);
            Integer rowIndex = this.getRowIndexByMahsulId(key);
            this.fireTableRowsUpdated(rowIndex, rowIndex);
            if (this.map.isEmpty()) {
                return this.map;
            }
            return null;
        }

        public Map<Integer, Map> addIslertoYerler(Integer yerId) {
            if (this.map == null) {
                this.map = new HashMap<Integer, Map>();
            }
            Map<Integer, Map> m = this.map.get(yerId);
            if (m == null) {
                m = new HashMap<Integer, Map>();
            }
            this.map.put(yerId, m);
            try {
                Integer rowIndex = this.getRowIndexByMahsulId(yerId);
                this.fireTableRowsUpdated(rowIndex, rowIndex);
            } catch (Exception e) {
                System.err.println("YER TABLE MODEL : Row index bulunamadi");
                this.fireTableDataChanged();
            } finally {
                return m;
            }
        }

        public YerTableModel(Map<Integer, Map> m) {
            this.headings = new String[]{"", "Yer", "#İş"};
            this.map = m;
            this.load();
        }

        @Override
        public void load() {
            RowData_Urun dbUrun = new RowData_Urun();
            dbUrun.sokulmemisUrunler(true);
            this.rowData = dbUrun.getData();
            if (map == null) {
                this.map = new HashMap<Integer, Map>();
            }
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
//            System.err.println("yer table model get value");

            Integer id = this.getMahsulId(rowIndex);
            Map<Integer, Map> m = this.map.get(id);
            switch (columnIndex) {
                case 0:
                    return m == null || m.isEmpty() ? false : true;
                case 1:
                    return this.rowData[rowIndex][4] + "-" + this.rowData[rowIndex][5];
                case 2:
                    return m != null ? m.size() : 0;
                default:
                    return null;
            }
        }

        public Integer getMahsulId(int rowIndex) {
            return (Integer) this.rowData[rowIndex][0];
        }

        public Integer getRowIndexByMahsulId(int mahsulId) {
            for (int i = 0; i < this.rowData.length; i++) {
                if (mahsulId == (Integer)this.rowData[i][0]) {
                    return i;
                }
            }
            return -1;
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            if (columnIndex == 0) {
                return Boolean.class;
            } else {
                return String.class;
            }
        }
    }

    class IsTableModel extends AbstractSeraTableModel {

        Map<Integer, Map> map;

        public Map<Integer, Map> getMap() {
            return map;
        }

        public void setMap(Map<Integer, Map> map) {
            this.map = map;
            this.refreshTable();
        }

        public Map<Integer, Map> removeIscilerFromIsler(int isId) {
            if (this.map == null) {
                return null;
            }
            this.map.remove(isId);
            int rowIndex = this.getRowIndexByIsId(isId);
            this.fireTableRowsUpdated(rowIndex, rowIndex);
            if (this.map.isEmpty()) {
                return this.map;
            }
            return null;
        }

        public Map<Integer, Map> addIscilerToIsler(Map<Integer, Map> m, int isId) {

            this.map = m;
            Map<Integer, Map> temp = this.map.get(isId);
            if (temp == null) {
                temp = new HashMap<Integer, Map>();
            }
            this.map.put(isId, temp);
            try {
                int rowIndex = this.getRowIndexByIsId(isId);
//                System.out.println(rowIndex + " addIsciToMap : " + this.map);
                this.fireTableRowsUpdated(rowIndex, rowIndex);
            } catch (Exception e) {
                System.err.println("IS TABLE MODEL : ROW INDEX BULUNAMADI");
                this.fireTableDataChanged();
            } finally {
                return temp;
            }
        }

        public IsTableModel(Map<Integer, Map> m) {
            this.headings = new String[]{"", "İş", "# Çalışan"};
            this.map = m;
            this.load();
        }

        @Override
        public void load() {
            this.rowData = new RowData_Is().getData();
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            System.err.println("is table model get value");
            Integer id = this.getIsId(rowIndex);

            switch (columnIndex) {
                case 0:
                    return this.map == null || this.map.get(id) == null || this.map.get(id).isEmpty() ? false : true;
                case 1:
                    return this.rowData[rowIndex][1];
                case 2:
                    return this.map == null || this.map.get(id) == null ? 0 : this.map.get(id).size();
                default:
                    return null;
            }
        }

        public Integer getIsId(int rowIndex) {
            return (Integer) this.rowData[rowIndex][0];
        }

        public Integer getRowIndexByIsId(int isId) {
            for (int i = 0; i < this.rowData.length; i++) {
                if (isId == (Integer)this.rowData[i][0]) {
                    return i;
                }
            }
            return -1;
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return Boolean.class;
                case 2:
                    return Integer.class;
                default:
                    return String.class;
            }
        }
    }

    private void updateValues(Float val, int yerRowId, int isRowId, int isciRowId, String time) {
        if (val == 0) {
            this.removeFromMap(yerRowId, isRowId, isciRowId, time);
            return;
        }
        System.out.println("UPDATES VALUES :\nyerRowID : " + yerRowId + "; isRowId : " + isRowId + "; isciRowID : " + isciRowId + "; time : " + time + "; val = " + val);
        Map m;
        m = tmYer.addIslertoYerler(yerRowId);
        m = tmIs.addIscilerToIsler(m, isRowId);
        tmIsci.addSaatlerToIsciler(m, isciRowId, time, val);
        System.out.println("MAP : " + calismaSaatleri);
    }

    private void removeFromMap(int yerRowId, int isRowId, int isciRowId, String time) {
        System.out.println("REMOVE VALUES :\nyerRowID : " + yerRowId + "; isRowId : " + isRowId + "; isciRowID : " + isciRowId + "; time : " + time);

        Map m;
        m = tmIsci.removeTimeFromSaatler(isciRowId, time);
        if (m != null) {
            m = tmIsci.removeSaatlerFromIsciler(isciRowId);
            if (m != null) {
                m = tmIs.removeIscilerFromIsler(isRowId);
                if (m != null) {
                    tmYer.removeIslerFromYerler(yerRowId);
                }
            }
        }
        System.out.println("MAP : " + tmYer.getMap());
    }
}
