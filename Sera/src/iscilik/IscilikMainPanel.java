/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package iscilik;

import main.AbstractSeraMainPanel;
import main.SeraTablePanel;

/**
 *
 * @author alptug
 */
public class IscilikMainPanel extends AbstractSeraMainPanel {

    IscilikKayitPanel ikp;
    SeraTablePanel stp;

    public IscilikMainPanel() {
        stp = new SeraTablePanel(new IscilikTableModel());
        ikp = new IscilikKayitPanel();
        super.addTablePanel(stp);
        super.addKayitPanel(ikp);
//        this.getKayitPanel().setEnabled(true);
        stp.getTable().getSelectionModel().addListSelectionListener(this);
        stp.setToplamPanel(true);
    }

    @Override
    protected void tableToKayitPanel(int index) {
        // secili satirla ayni tarihtedeki diger satirlari sec
        try {
            ikp.yeni();
        } catch (Exception ex) {
        }
        java.sql.Date dt = (java.sql.Date) stp.getTable().getValueAt(index, 0);
        
        for (int i = 0; i < stp.getTable().getRowCount(); i++) {
            if (i == index) {
                continue;
            }
            if (dt.compareTo((java.sql.Date) stp.getTable().getValueAt(i, 0)) == 0) {
                stp.getTable().addRowSelectionInterval(i, i);
            }

        }
        stp.getTable().repaint();
        ikp.loadCalismaSaatleri(dt);
      
    }
}
