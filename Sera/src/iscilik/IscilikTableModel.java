/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package iscilik;

import iscilik.db.RowData_Iscilik;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import main.AbstractSeraTableModel;

/**
 *
 * @author alptug
 */
public class IscilikTableModel extends AbstractSeraTableModel {

//    Map<String, Isciler> map = new HashMap<String, Isciler>();
    RowData_Iscilik rd = new RowData_Iscilik();
   Isciler[] isciler;
   public IscilikTableModel() {
        isciler = Isciler.getAllIsciler();
        ArrayList<String> h = new ArrayList<String>();
        h.add("Tarih");
        h.add("Zaman");
        h.add("Yer");
        h.add("Çeşit");
        h.add("İş");
        int i=5;
        for (Isciler isci : isciler) {
//            map.put(isci.getDbColumnHeader(), isci);
            h.add(isci.getAdi());
            this.setToplamSutunuFunc(i++, TOPLAM);
        }
        this.headings = h.toArray(new String[0]);
//        rd.setGroupBy("iscilik.tarih");
        rd.setGroupBy("iscilik.tarih ,iscilik.zaman, iscilik.is_id , iscilik.mahsul_id");

        this.rowData = rd.getData();
    }

    @Override
    public Number setToplamFunction(int colNumber) {
        if(colNumber<5)
            return null;
        RowData_Iscilik rd = new RowData_Iscilik();
        Object[][] data = rd.getIscilikBilgisiForIsci(isciler[colNumber-5].getId());
        double d=0;
        for(int i=0; i<data.length;i++)
            d+=(Float)data[i][6];
        return d;
    }
    @Override
    public void load() {
        this.rowData = rd.getData();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return this.rowData[rowIndex][columnIndex];
    }
}
