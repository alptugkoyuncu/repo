/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package iscilik.db;

import iscilik.Isciler;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.db.DBConnection;
import util.JCalendar;

/**
 *
 * @author alptug
 */
public class RowData_Iscilik extends DBConnection {

    String query;//selectten baslayip from ve inner joinleri icieren sorgu kisimi
    String limit, groupBy, where, have;//varsa limit,groupby, where
    String[] cols = null;

    public RowData_Iscilik() {
           
}

    @Override
    public Object[][] getData(String[] cols) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insertData(Object[] data) {
        String query = "INSERT INTO iscilik "
                + "(isci_id,is_id,mahsul_id,tarih,calisma_saati,ucret,zaman) "
                + "VALUES"
                + "("+(Integer)data[1] + "," +(Integer)data[2] + "," + (Integer)data[3]+"," +(String)data[4] + ","+(Float)data[5] + ","+(Float)data[6] + ",'"+(String)data[7]+"')";
        data[0] = this.processQuery(query);
        return (Integer)data[0];
    }

    @Override
    public boolean updateData(int id, Object[] data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int deleteData(int id) {
        String query = "DELETE FROM iscilik where iscilik_id = " + id;
        return this.processQuery(query);
    }
    public Integer[] deleteData(String makeDateFunc){
        String query = "SELECT iscilik_id from iscilik where tarih = " + makeDateFunc;
        Object[][] data;
        Integer[] ids;
        try {
            data = super.getData(query, new String[]{"iscilik_id"});
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Iscilik.class.getName()).log(Level.SEVERE, null, ex);
            data = new Object[0][0];
        }
        ids = new Integer[data.length];
        for(int i=0; i<data.length;i++){
            ids[i] = this.deleteData((Integer)data[i][0]);
        }
        return ids;
    }
    @Override
    public Object[][] getData() {
        query = "";
        try {
            Isciler[] isciler = Isciler.getAllIsciler();
            if (isciler.length == 0) {
                return null;
            }
            if (cols == null || cols.length == 0) {
                cols = new String[isciler.length + 5];
                cols[0] = "tarih";
                cols[1] = "zaman";
                cols[2] = "yer";
                cols[3] = "cesit";
                cols[4] = "is_adi";
                for (int i = 0; i < isciler.length; i++) {
                    cols[i + 5] = isciler[i].getDbColumnHeader();
                }

            }
            String tempQuery = "";
            for (int i = 0; i < isciler.length - 1; i++) {
                tempQuery += "SUM( IF(k.kisiler_id = "
                        + isciler[i].getId()
                        + ",calisma_saati,0.0)) AS " + isciler[i].getDbColumnHeader()+ ",";
            }

            tempQuery += "SUM( IF(k.kisiler_id = "
                    + isciler[isciler.length - 1].getId()
                    + ",calisma_saati,0.0)) AS " + isciler[isciler.length-1].getDbColumnHeader() + "";
            query = "SELECT "
                    + "    iscilik.tarih as tarih,"
                    + "     iscilik.zaman as zaman,"
                    + "    i.adi as is_adi ,"
                    + "    c.adi AS yer,"
                    + "    cesit_adi as cesit, "
                    + tempQuery
                    + " FROM "
                    + "    iscilik "
                    + "        left outer join "
                    + "    kisiler k ON iscilik.isci_id = k.kisiler_id "
                    + "        left outer join "
                    + "    `is` i ON i.is_id = iscilik.is_id "
                    + "        left outer join "
                    + "    mahsul m ON m.mahsul_id = iscilik.mahsul_id "
                    + "        left outer join "
                    + "    cesit ON cesit.cesit_id = m.cesit_id "
                    + "        left outer join "
                    + "    cati c ON c.cati_id = m.cati_id ";

             return this.getData(query, cols);
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Iscilik.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
        }
    }

    public static Integer getGunlukCalismaUcreti(String now){
        
        String query = "SELECT ucret FROM iscilik_ucret where date <= " + now + " ORDER BY date desc";
        try {
            Object[][] d = new RowData_Iscilik().getData(query, new String[]{"ucret"});
            return (Integer) d[0][0];
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Iscilik.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
        
    }
    public Object[][] getDataByDate(String dateFunction){
        String cols[] = new String[]{"mahsul_id","is_id","isci_id","zaman","calisma_saati"};
        String query = "select mahsul_id,is_id,isci_id,zaman,calisma_saati FROM iscilik where tarih = "+ dateFunction;
        try {
            return super.getData(query, cols);
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Iscilik.class.getName()).log(Level.SEVERE, null, ex);
            return new Object[0][0];
        }
    }

    public Object[][] getIscilikBilgisiForIsci(Integer id) {
        String query = "SELECT "
                + "iscilik.iscilik_id,"
                + "iscilik.isci_id,"
                + "iscilik.is_id,"
                + "iscilik.mahsul_id,"
                + "iscilik.tarih,"
                + "iscilik.calisma_saati,"
                + "iscilik.ucret,"
                + "iscilik.zaman "
                + "FROM iscilik where iscilik.isci_id= "+id;
        try {
            return this.getData(query,new String[]{"iscilik_id","isci_id","is_id","mahsul_id","tarih","calisma_saati","ucret","zaman"});
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Iscilik.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
