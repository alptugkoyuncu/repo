/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * KisiKayitPanel.java
 *
 * Created on 11.May.2012, 12:35:54
 */
package kisi;

import kisi.db.RowData_Kisi;
import kisi.db.RowData_kisiCesitleri;
import main.KayitFormIslemleri;
import util.JCalendar;
import util.My2DArray;

/**
 *
 * @author ISIK
 */
public class KisiKayitPanel extends javax.swing.JPanel implements KayitFormIslemleri {

    private Object[] data;
    private Object[] colNames;
    private Object[][] kisiCesitleri_values; // ilk kolon id ikinci kolon datayi verir
    private int id;

    /** Creates new form KisiKayitPanel */
    public KisiKayitPanel() {
        kisiCesitleri_values = new RowData_kisiCesitleri().getData(new String[]{"kisi_cesitleri_id", "type"});
        initComponents();

    }

    public void setAdi(String name) {
        this.jtAdi.setText(name);
    }

    public void setSoyadi(String surname) {
        this.jtSoyAdi.setText(surname);
    }

    public void setTelefon(String tel) {
        this.jtTelefon.setText(tel);
    }

    public void setDate(java.util.Date date) {

        JCalendar jc = (JCalendar) this.jtDate;
        jc.setDate(date);
    }

    public void setDurum(boolean durum) {
        this.jcheckDurum.setSelected(durum);
    }

    public void setPozisyon(Object val) {
        this.jcomboPozisyon.setSelectedItem(val);
    }

    public void setId(int id) {
        this.id = id;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jcomboPozisyon = new javax.swing.JComboBox();
        jtAdi = new javax.swing.JTextField();
        jtSoyAdi = new javax.swing.JTextField();
        jtTelefon = new javax.swing.JTextField();
        jcheckDurum = new javax.swing.JCheckBox();
        jtDate = new JCalendar(this);

        jLabel1.setText("Adı");

        jLabel2.setText("Soyadı");

        jLabel3.setText("Telefon");

        jLabel4.setText("Pozisyon");

        jLabel5.setText("Başlangıç Tarihi");

        jLabel6.setText("Durum");

        jcomboPozisyon.setModel(new javax.swing.DefaultComboBoxModel(My2DArray.selectedColumn(this.kisiCesitleri_values, 1) ));

        jcheckDurum.setSelected(true);
        jcheckDurum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcheckDurumActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jtDate)
                    .addComponent(jcomboPozisyon, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jtAdi)
                    .addComponent(jtSoyAdi)
                    .addComponent(jtTelefon)
                    .addComponent(jcheckDurum, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE))
                .addContainerGap(34, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addComponent(jcomboPozisyon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jtAdi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jtSoyAdi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jtTelefon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jtDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6)
                    .addComponent(jcheckDurum))
                .addContainerGap(19, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

private void jcheckDurumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcheckDurumActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jcheckDurumActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JCheckBox jcheckDurum;
    private javax.swing.JComboBox jcomboPozisyon;
    private javax.swing.JTextField jtAdi;
    private javax.swing.JTextField jtDate;
    private javax.swing.JTextField jtSoyAdi;
    private javax.swing.JTextField jtTelefon;
    // End of variables declaration//GEN-END:variables

    private void setDataForQuery() {
        data = new Object[6];

        //adi bilgisi
        data[0] = new String(this.jtAdi.getText());

        //SOyadi bilgisi
        data[1] = new String(this.jtSoyAdi.getText());

        //telefon bilgisi
        data[2] = new String(this.jtTelefon.getText());
        JCalendar date = (JCalendar) this.jtDate;
        data[3] = (String) date.sqlMakeDate();
        data[4] = this.jcheckDurum.isSelected();
        System.out.println(kisiCesitleri_values[this.jcomboPozisyon.getSelectedIndex()][0]);
        data[5] = kisiCesitleri_values[this.jcomboPozisyon.getSelectedIndex()][0];

    }
    private Object[] getDataForTable(int id){
        Object[] temp = new Object[8];
        temp[0] = id;
        temp[1] = this.jcomboPozisyon.getSelectedItem();
        temp[2] = data[0];
        temp[3] = data[1];
        temp[4] = data[2];
        JCalendar date = (JCalendar) this.jtDate;
        temp[5] = date.getDate().getTime();
        temp[6] = data[4];
        temp[7] = data[5];
        return temp;
    
    }
    @Override
    public Object[] ekle() {
        RowData_Kisi d = new RowData_Kisi();
        setDataForQuery();
         d.insertData(data);
        return getDataForTable(id);
        //yeni data table modele gondermek icin
    }

    @Override
    public int yeni() {
        this.jtAdi.setText("");
        this.jtSoyAdi.setText("");
        this.jtTelefon.setText("");
        this.jcomboPozisyon.setSelectedIndex(0);
        this.jcheckDurum.setSelected(true);
        JCalendar dt = (JCalendar) this.jtDate;
        java.util.Date d = new java.util.Date();
        dt.setDate(d);
        return 1;
    }

    @Override
    public Object[] guncelle() {
        RowData_Kisi d = new RowData_Kisi();
        setDataForQuery();
        System.out.println("Guncelleme id : " + this.id);
        d.updateData(this.id,data);
        return getDataForTable(id);
        
    }

    
    
}
