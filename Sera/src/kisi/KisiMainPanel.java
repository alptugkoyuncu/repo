/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kisi;

import main.AbstractSeraMainPanel;
import main.SeraTablePanel;
import javax.swing.ListSelectionModel;

/**
 *
 * @author ISIK
 */
public class KisiMainPanel extends AbstractSeraMainPanel{
    SeraTablePanel tp;
    KisiKayitPanel kisiKayitPanel;
    public KisiMainPanel(){
        super();
        tp = new SeraTablePanel(new KisiTableModel());
        kisiKayitPanel = new KisiKayitPanel();
        super.addKayitPanel(kisiKayitPanel);
        super.addTablePanel(tp);
        
        tp.getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tp.getTable().getSelectionModel().addListSelectionListener(this);
        
    }

    @Override
    protected void tableToKayitPanel(int index) {
        KisiTableModel m = (KisiTableModel) tp.getTable().getModel();
        kisiKayitPanel.setId((Integer)m.getRowId(index));
        kisiKayitPanel.setPozisyon(m.getValueAt(index, 0));
        kisiKayitPanel.setAdi((String) m.getValueAt(index, 1));
        kisiKayitPanel.setSoyadi((String) m.getValueAt(index, 2));
        kisiKayitPanel.setTelefon((String) m.getValueAt(index, 3));
        kisiKayitPanel.setDate((java.util.Date) m.getValueAt(index, 4));
        kisiKayitPanel.setDurum((Boolean) m.getValueAt(index, 5));
        
    }
}
