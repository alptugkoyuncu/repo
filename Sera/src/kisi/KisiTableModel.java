/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kisi;

import java.util.Date;
import javax.swing.JComboBox;
import kisi.db.RowData_Kisi;
import main.AbstractSeraTableModel;

/**
 *
 * @author ISIK
 */
public class KisiTableModel extends AbstractSeraTableModel {
    private final static String[] titles = new String[]{"Pozisyon", "Ad", "Soyad", "Telefon", "Başlangıç Tarihi", "Durum", "Bilgi"};
    
    public KisiTableModel() {
        this(false);

        //  this.fireTableRowsInserted(0, rowData.length);
//        rowData = d.getData(colDBNames);
    }

    public KisiTableModel(boolean b) {
        super(b);
        super.headings = titles;
        rowData = new RowData_Kisi().getData();
    }

    @Override
    public void load() {
        rowData = new RowData_Kisi().getData();
    }

    @Override
    public void unload() {
        rowData = null;
        System.gc();
    }

    /*
     * @Override public void addRow(Object[] d) { My2DArray arr = new
     * My2DArray(rowData); arr.addItem(d); rowData = arr.getValue();
     * My2DArray.printArray(rowData); super.addRow(d); }
     *
     * @Override public void insertRow(int rowId, Object[] d) { rowData[rowId] =
     * d; super.insertRow(rowId, d);
    }
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        //ilk veri id ve gizli olacagi icin hep bir sonraki veri alinir 
        return rowData[rowIndex][columnIndex + 1];
    }

    @Override
    public Integer getRowId(int rowIndex) {
        return Integer.parseInt((rowData[rowIndex][0]).toString());
    }

    @Override
    public Class getColumnClass(int c) {
        Class t;
        switch (c) {
            case 0:
                t = JComboBox.class;
                break;
            case 4:
                t = Date.class;
                break;
            case 5:
                t = Boolean.class;
                break;
            default:
                t = String.class;
        }
        return t;
    }
}