package kisi.db;



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.db.DBConnection;

/**
 *
 * @author ISIK
 */
public class RowData_Kisi extends DBConnection {
    public final static int ORTAK = 1;
    public final static int ISCI = 2;
    public final static int ZIRAATCI=3;
    public final static int KOMISYONCU=4;
    String query;

    public RowData_Kisi(){
        this.setKisiCesidi(ISCI);
    }
    public void setKisiCesidi(int type){
        String tmp = "kisi_cesitleri.kisi_cesitleri_id = " + type;
        this.setWhere(tmp);
    }
    @Override
    public Object[][] getData(String[] cols) {

        if (cols == null || cols.length == 0) {
            query = " Select * from kisiler ";
        } else {
            query = "Select";
            for (int i = 0; i < cols.length - 1; i++) {
                query += " " + cols[i] + ",";
            }
            query += " " + cols[cols.length - 1] + " from kisiler ";
        }
        query += " INNER JOIN kisi_cesitleri "
                + " on kisiler.kisi_cesitleri_id = kisi_cesitleri.kisi_cesitleri_id where type='isci'";

        //System.out.println(query);
        Object t[][] = null;
        try {
            t = getData(query, cols);
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Kisi.class.getName()).log(Level.SEVERE, null, ex);
        } //res.close();
        //conn.close();
        finally {
            return t;
        }


    }

    @Override
    public Object[][] getData() {
        // Tablw Model sutunlari {"Pozisyon","Ad","Soyad","Telefon","Başlangıç Tarihi","Durum","Bilgi"};

        String[] colNames = new String[]{"kisiler_id", "type", "adi", "soyadi", "telefon", "kayit_tarihi", "durum", "bilgi"};
        query = "SELECT "
                + "kisiler_id,type,adi,soyadi,telefon,kayit_tarihi,durum,(select count(*) from bilgi where id = kisiler_id and tablo_adi = 'kisiler')  as bilgi "
                + "FROM "
                + "kisiler "
                + "INNER JOIN "
                + "kisi_cesitleri "
                + "ON "
                + "kisiler.kisi_cesitleri_id = kisi_cesitleri.kisi_cesitleri_id ";
        this.setOrderBy("kisiler.kisiler_id");
        System.err.println(query);
        try {
            return getData(query, colNames);
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Kisi.class.getName()).log(Level.SEVERE, null, ex);
            return new Object[0][0];
        }
    }

    @Override
    public int insertData(Object[] data) {
        System.out.println(data[0] + " " + data[1] + " " + data[2] + " " + data[3] + " " + data[4] + " " + data[5]);
        query = "Insert into kisiler "
                + "(adi,soyadi,telefon,kayit_tarihi,durum,kisi_cesitleri_id) "
                //+ "VALUES('ismihan','s','1231231233','12.12.2001',1,1)"
                + "Values ('" + (String) data[0] + "','" + (String) data[1] + "','" + (String) data[2] + "','"
                + (String) data[3] + "'," + (Boolean) data[4] + "," + data[5] + ")";
        //",(Select kisi_cesitleri_id from kisi_cesitleri where type = "+(String)data[5]+"))";


        return super.processQuery(query);
        /*res.moveToInsertRow();
        res.updateString("adi", (String) data[0]);
        res.updateString("soyadi", (String) data[1]);
        res.updateString("telefon", (String) data[2]);
        res.updateDate("kayit_tarihi", (Date) data[3]);
        res.updateBoolean("durum", (Boolean)data[4]);
        res.updateInt("kisi_cesitleri_id", (Integer) data[5]);
        res.insertRow();
        res.beforeFirst();
         * */

    }

    @Override
    public boolean updateData(int id, Object[] data) {
        query = "UPDATE "
                + "kisiler "
                + "SET "
                + " adi = '" + data[0]
                + "', soyadi = '" + data[1]
                + "', telefon = '" + data[2]
                + "', kayit_tarihi = '" + data[3]
                + "', durum = " + data[4]
                + ", kisi_cesitleri_id = " + data[5]
                + " WHERE kisiler_id = " + id;
        System.out.println("Update Query : \n\t" + query);
        return super.processQuery(query) >= 0;
    }

    @Override
    public int deleteData(int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
