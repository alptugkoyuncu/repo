package kisi.db;



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import main.db.DBConnection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ISIK
 */
public class RowData_kisiCesitleri extends DBConnection {

    @Override
    public Object[][] getData(String[] cols) {
        String query = "";
        Object temp[][] = null;
        if (cols == null || cols.length == 0) {
            query = "Select * from kisi_cesitleri";
        } else {
            query = "Select ";
            for (int i = 0; i < cols.length - 1; i++) {
                query += cols[i] + ", ";
            }
            query += cols[cols.length - 1] + " from kisi_cesitleri ";
        }
        System.out.println(query);
        try {
            temp = getData(query, cols);
        } catch (SQLException ex) {
            Logger.getLogger(RowData_kisiCesitleri.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return temp;
        }

    }

    @Override
    public Object[][] getData() {
        return this.getData(null);
    }

    @Override
    public int insertData(Object[] data) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean updateData(int id, Object[] data) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int deleteData(int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
