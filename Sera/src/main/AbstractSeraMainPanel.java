/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author ISIK
 */
public abstract class AbstractSeraMainPanel extends JPanel implements ListSelectionListener {

    private KayitPanel kayitPanel = null;
    private SeraTablePanel tp;
    private SeraSearchPanel searchPanel;

    public AbstractSeraMainPanel(SeraTablePanel t, JPanel kayitPanel, SeraSearchPanel searchPanel) {
        this.setLayout(new BorderLayout());
        addSearchPanel(searchPanel);
        addTablePanel(t);
        addKayitPanel(kayitPanel);

    }

    public AbstractSeraMainPanel() {
        this.setLayout(new BorderLayout());
    }

    public void addTablePanel(SeraTablePanel tp) {
        if (tp == null) {
            return;
        }
        this.tp = tp;

        this.add(this.tp, BorderLayout.CENTER);

        // this.add(this.table.getTableHeader(),BorderLayout.PAGE_START);
        // this.add(this.table,BorderLayout.CENTER);
    }

    public void addKayitPanel(JPanel kayitPanel) {
        if (kayitPanel == null) {
            return;
        }
        if (this.kayitPanel != null) {
            this.remove(this.kayitPanel);
        }

        this.kayitPanel = new KayitPanel(kayitPanel);
        this.add(this.kayitPanel, BorderLayout.EAST);

    }

    public SeraTablePanel getTablePanel() {
        return this.tp;
    }

    public KayitPanel getKayitPanel() {
        return kayitPanel;
    }

    private void addSearchPanel(SeraSearchPanel searchPanel) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            return;
        }
        ListSelectionModel m = (ListSelectionModel) e.getSource();
        if (m.isSelectionEmpty()) {
            return;
        }
        int count = m.getMaxSelectionIndex() - m.getMinSelectionIndex();

        //Single selection
        if (count == 0) {
            tableToKayitPanel(m.getMinSelectionIndex());
            KayitPanel p = this.getKayitPanel();
            if (!p.isOpen()) {
                p.toggleButton();
            }
            p.setIslemDurum(KayitPanel.GUNCELLE);
        }
        //  System.out.println("------------ "+i+" -------------");

    }

    protected abstract void tableToKayitPanel(int index);
}
