/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import com.sun.org.apache.xpath.internal.functions.Function;
import java.util.HashMap;
import java.util.Map;
import javax.swing.table.AbstractTableModel;
import util.My2DArray;

/**
 *
 * @author DELL
 */
public abstract class AbstractSeraTableModel extends AbstractTableModel implements AggregrateFunctions {

    public int pageOffSet;
    public int pageSize;
    public boolean paging;
    public String[] headings = null;
    public Object[][] rowData = null;
    private int pageCount;
    private Map<Integer, Integer> colFunc = new HashMap<Integer, Integer>();

    public AbstractSeraTableModel() {
        this(false);
    }

    public AbstractSeraTableModel(boolean paging) {
        this.setPaging(paging);
    }

    public abstract void load();

    public void unload() {
        this.rowData = null;
        System.gc();
    }
    //return all rows cleaned

    public int removeAll() {
        if (this.rowData == null) {
            return 0;
        }
        int count = this.rowData.length;
        unload();
        this.fireTableDataChanged();
        return count;
    }

    public void refreshTable() {
        unload();
        load();
        this.fireTableDataChanged();
    }

    public void insertRow(int rowIndex, Object[] values) {
        My2DArray t = new My2DArray(rowData);
        t.insertItem(values, rowIndex);
        this.rowData = t.getValue();
        t.printArray(rowData);
        this.fireTableDataChanged();
    }

    public void addRow(Object[] values) {
        My2DArray t = new My2DArray(rowData);
        t.addItem(values);
        this.rowData = t.getValue();
        this.fireTableDataChanged();
    }

    public void updateRow(int rowIndex, Object[] values) {
        this.rowData[rowIndex] = values;
        this.fireTableRowsUpdated(rowIndex, rowIndex);
    }

    public Integer removeRowAt(int rowId) {
        My2DArray arr = new My2DArray(rowData);
        arr.removeItemAt(rowId);
        this.rowData = arr.getValue();
        this.fireTableRowsDeleted(rowId, rowId);
        return rowId;
    }

    public Integer getRowId(int rowIndex) {
        return (Integer) this.rowData[rowIndex][0];
    }

    @Override
    public boolean isCellEditable(int r, int c) {
        return false;
    }

    @Override
    public String getColumnName(int col) {
        return headings == null ? "" : headings[col];
    }

    @Override
    public int getColumnCount() {
        return headings == null ? 0 : headings.length;
    }

    @Override
    public int getRowCount() {

        return rowData == null ? 0 : rowData.length;
    }

    public void setPageSize(int size) {
        this.pageSize = size;
        this.paging = true;
    }

    public int getPageSize() {
        return this.hasPaging() ? this.pageSize : -1;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getPageCount() {
        return this.pageCount;
    }

    public void setPaging(boolean paging) {
        this.paging = paging;
    }

    public boolean hasPaging() {
        return this.paging;
    }

    public void setPageOffSet(int offSet) {
        this.pageOffSet = offSet;
    }

    public int getPageOffset() {
        return this.pageOffSet;
    }

    public void nextPage() {
    }

    public void previousPage() {
    }

    public void firstPage() {
    }

    public void lastPage() {
    }

    public void pageAt(int pageNumber) {
    }

    public Number setToplamFunction(int colNumber) {
        double d = 0;
        for (int i = 0; i < this.getRowCount(); i++) {
            d += ((Number) this.getValueAt(i, colNumber)).doubleValue();
        }
        return d;
    }

    public Number setOrtalamaFunction(int colNumber) {
        double d = 0;
        for (int i = 0; i < this.getRowCount(); i++) {
            d += ((Number) this.getValueAt(i, colNumber)).doubleValue();
        }
        return d / colNumber;
    }

    public Number getToplamSutunuValue(int colNumber) {
        Integer index = this.colFunc.get(colNumber);
        if (index == null) {
            return null;
        }
        try {
            Number val = (Number) this.getValueAt(0, colNumber);

        } catch (Exception e) {
            return null;
        }
        double d = 0;
        switch (index) {
            case TOPLAM: {
                return setToplamFunction(colNumber);
            }
            case ORTALAMA: {
                return setOrtalamaFunction(colNumber);
            }
            default:
                return null;
        }
    }

    public void setToplamSutunuFunc(int colNumber, int func) {
        this.colFunc.put(colNumber, func);
    }

}
