/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author alptug
 */
public class KayitButtonPanel extends JPanel{
    JButton jbtYeni,jbtIslem;
    public KayitButtonPanel(){
        this.setLayout(new FlowLayout());
        jbtYeni = new JButton("Yeni");

        jbtIslem = new JButton("Ekle");
        this.add(jbtYeni);
        this.add(jbtIslem);
        
    }
    public JButton getIslemButton() {
        return this.jbtIslem;
    }

    public JButton getYeniButton() {
        return this.jbtYeni;
    }
}
