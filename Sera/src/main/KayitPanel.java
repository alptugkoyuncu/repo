/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import main.listeners.IslemActionListener;
import main.listeners.YeniButtonListener;
import Main.listeners.ToggleActionListener;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author ISIK
 */
public class KayitPanel extends JPanel {

    public final static int YENI = 1;
    public final static int GUNCELLE = 2;
    public final static int  EKLE = 3;
    
    JButton jbToggle = new JButton();
    Boolean isOpen = true;
    JPanel kayitForm;
    KayitButtonPanel buttonPanel;
    
    private int islemDurumu;
    public KayitPanel(JPanel kayitForm) {
        this.kayitForm = kayitForm;
        buttonPanel = new KayitButtonPanel();
        

        this.setLayout(new BorderLayout());
        this.add(jbToggle, BorderLayout.NORTH);
        this.add(this.kayitForm, BorderLayout.CENTER);
        this.add(buttonPanel, BorderLayout.SOUTH);

        toggleButton();
        this.setIslemDurum(KayitPanel.YENI);

        ActionListener l = new ToggleActionListener(this);
        jbToggle.addActionListener(l);

        ActionListener kayitbtListener = new YeniButtonListener(this);
        this.buttonPanel.getYeniButton().addActionListener(kayitbtListener);
        ActionListener islembtListener = new IslemActionListener(this);
        this.buttonPanel.getIslemButton().addActionListener(islembtListener);

    }

    public void toggleButton() {
        isOpen = !isOpen;
        jbToggle.setText(isOpen ? ">>>" : "<<<");
        jbToggle.setToolTipText(isOpen ? "Kayıt Panelini gizlemek için tıklayınız" : "Kayıt Panelini göstermek için tıklayınız");
        kayitForm.setVisible(isOpen);
        buttonPanel.setVisible(isOpen);
    }

    public boolean isOpen() {
        return this.isOpen;
    }

    public JPanel getForm() {
        return kayitForm;
    }

    

    public void setIslemDurum(int i) {
        switch (i) {
            case KayitPanel.YENI:
                this.buttonPanel.getIslemButton().setText("Ekle");
                this.islemDurumu = KayitPanel.YENI;
                break;
            case KayitPanel.GUNCELLE:
                this.buttonPanel.getIslemButton().setText("Güncelle");
                this.islemDurumu = KayitPanel.GUNCELLE;
                break;
            case KayitPanel.EKLE:
                this.islemDurumu = KayitPanel.EKLE;
            default:
                break;
        }
    }

    public int getIslemDurumu() {
        return this.islemDurumu;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled); //To change body of generated methods, choose Tools | Templates.
        if(enabled==false && isOpen){
            this.toggleButton();
        }
        this.jbToggle.setEnabled(enabled);
    }

    public KayitButtonPanel getButtonPanel() {
        return buttonPanel;
    }
}
