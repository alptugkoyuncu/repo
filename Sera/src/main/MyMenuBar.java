/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.BorderLayout;
import java.awt.Container;
import kisi.KisiKayitPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.*;
import cati.CatiKayitPanel;

/**
 *
 * @author ISIK
 */
public class MyMenuBar extends JMenuBar implements ActionListener {
    private JMenu menu, submenu;
    private JMenuItem seraGirisi;
    private AbstractSeraMainPanel mainFrame;
    public MyMenuBar(AbstractSeraMainPanel mainFrame) {
       this.mainFrame = mainFrame;
       System.out.println(mainFrame);
//Create the menu bar.

//Build the first menu.
        menu = new JMenu("Ana Menu");
      //  menu.setMnemonic(KeyEvent.VK_A);
      //  menu.getAccessibleContext().setAccessibleDescription(
  //              "The only menu in this program that has menu items");
//a group of JMenuItems
        seraGirisi = new JMenuItem("Yeni Sera Girişi",
                KeyEvent.VK_T);
        seraGirisi.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_1, ActionEvent.ALT_MASK));
        seraGirisi.getAccessibleContext().setAccessibleDescription(
                "This doesn't really do anything");
        menu.add(seraGirisi);
        this.add(menu);
        seraGirisi.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == seraGirisi)
            openSeraKayitPanel();
    }
    private void openSeraKayitPanel(){
        JInternalFrame f = new JInternalFrame();
        f.add(new CatiKayitPanel());
       this.mainFrame.getTablePanel().add(f);
        f.pack();
        f.setVisible(true);
        f.setOpaque(true);
        
    }
    
}
