/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author DELL
 */
public class SeraTablePanel extends JPanel {

    JTable table;
    AbstractSeraTableModel tableModel;
    JButton jbtFirst, jbtLast, jbtNext, jbtPrevious;
    JTextField jtfPageOffset;
    JLabel jlPageSeperator, jlPageCount;
    JPanel southPanel = new JPanel();
    ToplamPanelForTable toplamPanel;

    public SeraTablePanel(AbstractSeraTableModel model) {
        this.tableModel = model;
        initializeComponents();
    }

    public SeraTablePanel() {
        this(null);
    }

    private void initializeComponents() {

        this.table = new JTable(tableModel);
        this.southPanel.setLayout(new BorderLayout());
        this.setLayout(new BorderLayout());
        JScrollPane sp = new JScrollPane(table);
        table.setFillsViewportHeight(true);
        this.add(table.getTableHeader(), BorderLayout.PAGE_START);
        this.add(table, BorderLayout.CENTER);
        // sayfalama panelini oluştur
        if (this.tableModel.hasPaging()) {
            this.southPanel.add(createPagingPanel(), BorderLayout.SOUTH);
        }
        this.add(this.southPanel, BorderLayout.SOUTH);
    }

    public void setToplamPanel(boolean isFlag) {
        if (isFlag) {
            toplamPanel = new ToplamPanelForTable(this.table);
            this.southPanel.add(toplamPanel, BorderLayout.NORTH);
        } else {
            if (this.toplamPanel == null) {
                return;
            }
            this.southPanel.remove(toplamPanel);
            toplamPanel = null;
        }
        this.southPanel.validate();
    }

    private JPanel createPagingPanel() {
        JPanel p = new JPanel();
        p.setLayout(new FlowLayout());

        jbtFirst = new JButton("|<<");
        jbtFirst.setToolTipText("Başa dön");
        jbtLast = new JButton(">>|");
        jbtLast.setToolTipText("Sona git");
        jbtNext = new JButton(">>");
        jbtNext.setToolTipText("Sonraki");
        jbtPrevious = new JButton("<<");
        jbtPrevious.setToolTipText("Önceki");

        this.jlPageSeperator = new JLabel(" / ");
        this.jlPageCount = new JLabel(" " + tableModel.getPageCount() + " ");
        this.jtfPageOffset = new JTextField("" + tableModel.getPageOffset());

        p.add(jbtFirst);
        p.add(jbtPrevious);
        p.add(jtfPageOffset);
        p.add(jlPageSeperator);
        p.add(jlPageCount);
        p.add(jbtNext);
        p.add(jbtLast);


        return p;
    }

    public JTable getTable() {
        return this.table;
    }

    public AbstractSeraTableModel getModel() {
        return this.tableModel;
    }

    class ToplamPanelForTable extends JPanel implements TableColumnModelListener {

        List<JLabel> labels = new ArrayList<JLabel>();
        GridBagLayout gbLayout;
        JTable tableFor;

        public ToplamPanelForTable(JTable t) {
            gbLayout = new GridBagLayout();
            this.setLayout(gbLayout);
            this.tableFor = t;
            this.tableFor.getColumnModel().addColumnModelListener(this);
            GridBagConstraints c = new GridBagConstraints();
            try {
                Enumeration<TableColumn> e = this.tableFor.getColumnModel().getColumns();
                int i = 0;
                while (e.hasMoreElements()) {
                    TableColumn col = e.nextElement();
                    c.fill = GridBagConstraints.HORIZONTAL;
                    c.gridx = i;
                    c.gridy = 0;
                    c.weightx = col.getWidth();
                    JLabel l = new JLabel();
                    l.setBorder(BorderFactory.createTitledBorder((String) col.getHeaderValue()));
                    this.add(l, c);
                    this.labels.add(l);
                    i++;
                }
            } catch (Exception e) {
            }
            this.updateAllLabelValues();
            this.tableFor.getModel().addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                    updateAllLabelValues();
                }
            });
        }

        public void updateLabelSizes() {
            GridBagConstraints c = new GridBagConstraints();
            try {
                Enumeration<TableColumn> e = this.tableFor.getColumnModel().getColumns();
                int i = 0;
                while (e.hasMoreElements()) {
                    TableColumn col = e.nextElement();
                    c.fill = GridBagConstraints.HORIZONTAL;
                    c.gridx = i;
                    c.gridy = 0;
                    c.weightx = col.getPreferredWidth();
                    this.add(this.labels.get(i), c);
                    i++;
                }
            } catch (Exception e) {
            }
            this.validate();
        }

        public GridBagConstraints getConstraintForLabelAtIndex(int position) {
            GridBagConstraints c = this.gbLayout.getConstraints(this.labels.get(position));
            return c;
        }

        @Override
        public void columnAdded(TableColumnModelEvent e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void columnRemoved(TableColumnModelEvent e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void columnMoved(TableColumnModelEvent e) {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void columnMarginChanged(ChangeEvent e) {
            this.updateLabelSizes();
            this.validate();
        }

        @Override
        public void columnSelectionChanged(ListSelectionEvent e) {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private void updateLabelValue(int i, JLabel l) {
            AbstractSeraTableModel tm = (AbstractSeraTableModel) (AbstractTableModel) this.tableFor.getModel();
            Number val = tm.getToplamSutunuValue(i);
            System.err.println("UPDATE LABEL VAL " + val);
            l.setText(val == null ? "" : "" + val);
            
        }

        private void updateAllLabelValues() {
            for (int i = 0; i < this.tableFor.getColumnCount(); i++) {
                updateLabelValue(i, this.labels.get(i));
            }
        }
    }
}
