/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.db;

import util.My2DArray;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ISIK
 */
public abstract class DBConnection {

    protected Connection conn = null; //Bağlantı nesnemiz
    private String url = "jdbc:mysql://localhost:3306/";//veritabanının adresi ve portu
    private String dbName = "sera"; //veritabanının ismi
    private String properties = "?useUnicode=true&characterEncoding=utf8"; //Türkçe karakter problemi yaşamamak için
    private String driver = "com.mysql.jdbc.Driver";//MySQL-Java bağlantısını sağlayan JDBC sürücüsü
    private String userName = "root"; //veritabanı için kullanıcı adı
    private String password = "19791981"; //kullanıcı şifresi
    private ResultSet res; // sorgulardan dönecek kayıtlar (sonuç kümesi) bu nesne içerisinde tutulacak   
    private Statement st;
    private String query;//selectten baslayip from ve inner joinleri icieren sorgu kisimi
    private String limit, groupBy, where, have, orderBy;//varsa limit,groupby, where

    public void setHave(String have) {
        this.have = have;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public void setGroupBy(String groupBy) {
        this.groupBy = groupBy;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getWhere() {
        return this.where != null && !"".equals(this.where.trim()) ? this.where.trim() : "";
    }

    public String getLimit() {
        return this.limit != null && !"".equals(this.limit.trim()) ? this.limit.trim() : "";
    }

    public String getQuery() {
        return query;
    }

    public String getHave() {
        return !this.getGroupBy().equals("") && this.have != null && !this.have.trim().equals("") ? this.have.trim() : "";
    }

    public String getGroupBy() {
        return this.groupBy != null ? this.groupBy.trim() : "";
    }

    public String getOrderBy() {
        return this.orderBy != null && !this.orderBy.trim().equals("") ? this.orderBy.trim() : "";
    }

    public DBConnection() {
    }

    private void openConn() {
        try {
            try {
                Class.forName(driver).newInstance();
            } catch (InstantiationException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            conn = DriverManager.getConnection(url + dbName + properties, userName, password);//bağlantı açılıyor
            try {
                st = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Connection Opened : `Kisiler`");

    }

    public abstract Object[][] getData(String[] cols);

    public abstract Object[][] getData();

    private String unionQuery() {
        String s = this.getQuery();
        s += " ";
        s += !this.getGroupBy().trim().equals("") ? " GROUP BY " + this.getGroupBy() : "";
        s += " ";
        s += !this.getGroupBy().equals("") && !this.getHave().equals("") ? " HAVING " + this.getHave() : "";
        s += " ";
        s += !"".equals(this.getWhere()) ? " WHERE " + this.getWhere() : "";
        s += " ";
        s += !this.getOrderBy().equals("") ? "ORDER BY " + this.getOrderBy() : "";
        s += " ";
        s += !this.getLimit().equals("") ? " LIMIT " + this.getLimit() : "";
        System.err.println(s);
        return s;
    }

    protected Object[][] getData(String query, String[] cols) throws SQLException {
        this.openConn();
        res = st.executeQuery(unionQuery(query));
        My2DArray temp = new My2DArray();
        if (cols != null && cols.length != 0) {
            int i = 0;

            while (res.next()) {
                System.out.println(i++);
                Object[] c = new Object[cols.length];
                for (int j = 0; j < cols.length; j++) {
                    String s = cols[j];
                    try {
                        c[j] = res.getObject(s) != null ? res.getObject(s) : "";
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                temp.addItem(c);
            }
            this.closeConn();
            return temp.getValue();

        } else {
            this.closeConn();
            return null;
        }

        //Main.MainFrame.yaz(temp.getValue());
    }

    // return generated id by the database
    // return -1 if there is a problem
    public abstract int insertData(Object[] data);

    protected int insertData(Object[] data, int a) {
        return -1;
    }

    protected int processQuery(String query) {
        this.openConn();
        int id = -1;
        try {
            id = st.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            System.out.println("Eklenen dosya id : " + id);
            System.out.println(query);

        } catch (SQLException ex) {
            System.err.println(query);
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.closeConn();
        return id;
    }

    public abstract boolean updateData(int id, Object[] data);

    public abstract int deleteData(int id);

    public void closeConn() {
        try {
            System.out.println("Connection Closed : `Kisiler`");
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String unionQuery(String query) {
        this.query = query;
        return this.unionQuery();
    }
}
