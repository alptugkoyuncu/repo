/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.listeners;

import main.KayitFormIslemleri;
import main.KayitPanel;
import main.AbstractSeraMainPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import main.AbstractSeraTableModel;

/**
 *
 * @author ISIK
 */
public class IslemActionListener implements ActionListener {

    main.KayitPanel kayitPanel;
    KayitFormIslemleri kayitForm;

    public IslemActionListener(main.KayitPanel p) {
        kayitPanel = p;
        kayitForm = (KayitFormIslemleri) p.getForm();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object[] temp;
        JTable t = ((AbstractSeraMainPanel) kayitPanel.getParent()).getTablePanel().getTable();
        AbstractSeraTableModel tm = (AbstractSeraTableModel) t.getModel();
        switch (kayitPanel.getIslemDurumu()) {
            case KayitPanel.YENI:
                temp = kayitForm.ekle();
                if (temp != null) {
                    tm.addRow(temp);
                } else {
                    tm.refreshTable();
                }
                System.out.println("asdas");
                break;
            case KayitPanel.GUNCELLE:
                temp = kayitForm.guncelle();
                int rowIndex = t.getSelectedRow();
                if (temp != null) {
                    tm.updateRow(rowIndex, temp);
                } else {
                    tm.refreshTable();
                }
                break;
            default:
                System.err.println("Kayit Islem Durumu Bulunamadi");
                break;

        }
        kayitPanel.toggleButton();
    }
}
