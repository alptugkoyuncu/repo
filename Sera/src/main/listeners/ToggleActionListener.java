/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Main.listeners;

import main.KayitPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import main.listeners.YeniButtonListener;

/**
 *
 * @author ISIK
 */
public class ToggleActionListener implements ActionListener {

    KayitPanel pnl;

    public ToggleActionListener(KayitPanel pnl) {
        this.pnl = pnl;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(!pnl.isOpen()){
            ActionListener l = new YeniButtonListener(pnl);
            l.actionPerformed(e);
        }
        pnl.toggleButton();
    }
}
