/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import main.KayitFormIslemleri;
import main.KayitPanel;
import main.AbstractSeraMainPanel;

/**
 *
 * @author ISIK
 */
public class YeniButtonListener implements ActionListener {

    KayitPanel kayitPanel;
    KayitFormIslemleri kayitForm;

    public YeniButtonListener(main.KayitPanel p) {
        kayitPanel = p;
        kayitForm = (KayitFormIslemleri) p.getForm();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        kayitForm.yeni();
        try {
            ((AbstractSeraMainPanel) kayitPanel.getParent()).getTablePanel().getTable().clearSelection();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        kayitPanel.setIslemDurum(KayitPanel.YENI);
    }
}
