/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package satis;

import javax.swing.JPanel;
import main.AbstractSeraMainPanel;
import main.AbstractSeraTableModel;
import main.KayitPanel;
import main.SeraTablePanel;
import satis.SatisKayitPanel.UrunTableModel;
import util.MyComboBoxModel;

/**
 *
 * @author alptug
 */
public class CikmaMainPanel extends AbstractSeraMainPanel {
    CikmaKayitPanel pnl;
    
    public CikmaMainPanel(UrunTableModel tm, int cikma) {
        pnl = new CikmaKayitPanel(tm,cikma);
        initializeClass();
        this.getKayitPanel().setIslemDurum(KayitPanel.YENI);
    }
    public CikmaMainPanel(int cikmaSatisTuruId){
        pnl = new CikmaKayitPanel(cikmaSatisTuruId);
        initializeClass();
        this.getKayitPanel().setIslemDurum(KayitPanel.GUNCELLE);
    }
    private void initializeClass(){
        this.addKayitPanel(pnl);
        if(!this.getKayitPanel().isOpen()){
            this.getKayitPanel().toggleButton();
        }
        this.addTablePanel(new SeraTablePanel(new AbstractSeraTableModel() {
            @Override
            public void load() {
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                return "";
            }
        }));
        this.getTablePanel().setVisible(false); 
    }
    class CikmaKayitPanel extends SatisKayitPanel {

        private int toplamCikma;
        public CikmaKayitPanel(UrunTableModel tm,int topCikma) {
            super();
            this.jcTarih.getDocument().removeDocumentListener(l);
            this.jftxtBrut.setEditable(false);
            this.setToplamCikma(topCikma);
            UrunTableModel t = (UrunTableModel) this.jtUrunler.getModel();
            for(int i=0; i<t.getRowCount();i++){
                for(int j=0; j<tm.getRowCount(); j++){
                    if(t.getRowId(i)==tm.getRowId(j)){
                        t.setParentId(i,tm.getSatisId(j));
                        t.setValueAt(tm.getValueAt(j, 2), i, 2);
                        break;
                    }
                }
            }
            this.jtUrunler.setEnabled(false);
            
        }

        private CikmaKayitPanel(int cikmaSatisTuruId) {
            super();
            this.jcTarih.getDocument().removeDocumentListener(l);
            this.jftxtBrut.setEditable(false);
            this.loadExistingValues(cikmaSatisTuruId);
        }

        @Override
        public int yeni() {
            int a = super.yeni(); //To change body of generated methods, choose Tools | Templates.
            this.jftxtBrut.setValue(toplamCikma);
            this.jcTarih.getDocument().removeDocumentListener(l);
            return a;
        }

        @Override
        public Object[] ekle() {
             super.ekle(); //To change body of generated methods, choose Tools | Templates.
             return null;
        }
        

        @Override
        public Object[] guncelle() {
             super.guncelle(); //To change body of generated methods, choose Tools | Templates.
             return null;
        }
        public void setToplamCikma(int toplamCikma) {
            this.toplamCikma = toplamCikma;
            this.jftxtBrut.setValue(toplamCikma);
        }
        
    }

    @Override
    protected void tableToKayitPanel(int index) {
    }
}
