/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package satis;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import kisi.db.RowData_Kisi;
import main.AbstractSeraTableModel;
import main.KayitButtonPanel;
import main.KayitFormIslemleri;
import main.KayitPanel;
import satis.db.RowData_Satis;
import satis.db.RowData_SatisTuru;
import urun.db.RowData_Urun;
import util.JCalendar;
import util.MyComboBoxModel;
import util.MyComboBoxRenderer;

/**
 *
 * @author alptug
 */
public class SatisKayitPanel extends javax.swing.JPanel implements KayitFormIslemleri {

    /**
     * Creates new form SatisKayitPanel
     */
    private int satisTuruId = -1;
    DocumentListener l = new TarihDocumentListener();

    public SatisKayitPanel() {
        initComponents();
        this.jcTarih.getDocument().addDocumentListener(l);
        RowData_Kisi rd = new RowData_Kisi();
        rd.setKisiCesidi(RowData_Kisi.KOMISYONCU);
//        RowData_
        this.jcbKomisyoncu.setModel(new MyComboBoxModel(rd.getData(), 0));
        this.jcbKomisyoncu.setRenderer(new MyComboBoxRenderer(new Integer[]{2, 3}));
        this.jcbSatisTuru.setModel(new MyComboBoxModel(new RowData_SatisTuru().getData(), 0));
        this.jcbSatisTuru.setRenderer(new MyComboBoxRenderer(2));
        this.jpnlCikma.setVisible(false);
        this.jftxtNet.getDocument().addDocumentListener(new CalculateKazancDocumentListener());
        this.jftxtBFiyat.getDocument().addDocumentListener(new CalculateKazancDocumentListener());
        this.jftCikma.addPropertyChangeListener("value", new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (jftCikma.getValue() != null && ((Integer) jftCikma.getValue()) != 0) {
                    jpnlCikma.setVisible(true);
                    if (getRecordedCikmaSatisTuruId() > 0) {
                        jbtCikma.setText("Güncelle");
                    } else {
                        jbtCikma.setText("Ekle");
                    }
                } else {
                    jpnlCikma.setVisible(false);
                }
            }
        });

//        this.jftCikma.getDocument().addDocumentListener(new DocumentListener() {
//            @Override
//            public void insertUpdate(DocumentEvent e) {
//                if (jftCikma.isEditValid()) {
//                    jpnlCikma.setVisible(jftCikma.getValue() != null && ((Integer) jftCikma.getValue()) != 0);
//
//                }
//            }
//
//            @Override
//            public void removeUpdate(DocumentEvent e) {
//            }
//
//            @Override
//            public void changedUpdate(DocumentEvent e) {
//            }
//        });
        this.jbtCikma.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                openCikmaPanel();
            }
        });
    }

    public void openCikmaPanel() {
        int cikma = (Integer) this.jftCikma.getValue();
        final JFrame f = new JFrame("Çıkma Ekleme");
        f.setLayout(new BorderLayout());
        int cikmaSatisTuruId = this.getRecordedCikmaSatisTuruId();
        CikmaMainPanel pnl;
        if (cikmaSatisTuruId <= 0) {
            pnl = new CikmaMainPanel((UrunTableModel) this.jtUrunler.getModel(), cikma);
        } else {
            pnl = new CikmaMainPanel(cikmaSatisTuruId);
        }
        f.add(pnl, BorderLayout.CENTER);
        f.pack();
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        final ArrayList<Container> c = new ArrayList<Container>();

        Container l = this;
        final SatisMainPanel smp;
        int index=-1;
        while (l != null) {
            c.add(l);
            try{
            l = l.getParent();
            }catch(Exception e){
                l=null;
            }
        }
        for( int i=0; i<c.size(); i++){
            if(c.get(i) instanceof SatisMainPanel){
                index=i;
                break;
            }
        }
        smp = (SatisMainPanel) (index==-1 ? null : c.get(index));
        for (Container k : c) {
            k.setEnabled(false);
        }
        pnl.getKayitPanel().getButtonPanel().getIslemButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.dispose();
                ((AbstractSeraTableModel)smp.getTablePanel().getTable().getModel()).refreshTable();
                smp.setFocusable(true);
            }
        });
        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                for (Container m : c) {
                    m.setEnabled(true);
                }
            }
        });
    }

    public void loadExistingValues(int satisTuruId) {
        this.jcTarih.getDocument().removeDocumentListener(l);
        this.satisTuruId = satisTuruId;
        RowData_Satis rd = new RowData_Satis();
        Object[][] o = rd.getDataBySatisTuru(satisTuruId);
        UrunTableModel tm = (UrunTableModel) this.jtUrunler.getModel();
        if (o.length > 0) {
            this.jftxtBFiyat.setValue((Float) o[0][11]);
            ((JCalendar) this.jcTarih).setDate((java.sql.Date) o[0][13]);
            this.updateUrunlerTable((java.sql.Date) o[0][13]);
            ((MyComboBoxModel) this.jcbKomisyoncu.getModel()).setSelectedItemById((Integer) o[0][3]);
            ((MyComboBoxModel) this.jcbSatisTuru.getModel()).setSelectedItemById((Integer) o[0][2]);

        }
        Double topKazanc = 0.0;
        Double topNet = 0.0;
        Double topBrut = 0.0;

        for (int i = 0; i < o.length; i++) {
            System.err.println(satisTuruId);
            Integer urunId = (Integer) o[i][1];
            Integer satisId = (Integer) o[i][0];
            Integer parentId = (Integer) o[i][14];
            Double net = (Double) o[i][10];
            topKazanc += (Double) o[i][12];
            topNet += net;
            topBrut += (Double) o[i][9];
            for (int j = 0; j < this.jtUrunler.getRowCount(); j++) {
                if (urunId == tm.getRowId(j)) {
                    tm.setSatisId(j, satisId);
                    tm.setParentId(j, parentId);
                    tm.setValueAt(net, j, 2);
                }
            }
        }
        int cikma = ((Double) (topBrut - topNet)).intValue();
        this.jftCikma.setValue(cikma);
        this.jftxtBrut.setValue(topBrut.intValue());
        this.jftxtKazanc.setValue(topKazanc.intValue());
        this.jftxtNet.setValue(topNet.intValue());
        this.jcTarih.getDocument().addDocumentListener(l);
    }

    private void updateUrunlerTable(java.util.Date dt) {
        UrunTableModel m = (UrunTableModel) this.jtUrunler.getModel();
        m.getRd().setWhere("");
        m.getRd().setTarih(util.UsefulFunctions.sqlMakeDateFunc(dt));
        m.refreshTable();
    }

    private void updateUrunlerTable(java.sql.Date dt) {
        UrunTableModel m = (UrunTableModel) this.jtUrunler.getModel();
        m.getRd().setWhere("");
        m.getRd().setTarih(util.UsefulFunctions.sqlMakeDateFunc(dt));
        m.refreshTable();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jcTarih = new JCalendar(this);
        jcbSatisTuru = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jcbKomisyoncu = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtUrunler = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jftxtBrut = new javax.swing.JFormattedTextField();
        jftxtNet = new javax.swing.JFormattedTextField();
        jftxtBFiyat = new javax.swing.JFormattedTextField();
        jftxtKazanc = new javax.swing.JFormattedTextField();
        jpnlCikma = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jbtCikma = new javax.swing.JButton();
        jftCikma = new javax.swing.JFormattedTextField();

        jcbSatisTuru.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jcbSatisTuru.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbSatisTuruActionPerformed(evt);
            }
        });

        jLabel1.setText("Satış Türü");

        jLabel2.setText("Komisyoncu");

        jcbKomisyoncu.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jtUrunler.setModel(new UrunTableModel());
        jScrollPane1.setViewportView(jtUrunler);

        jLabel3.setText("Ürünler");

        jLabel4.setText("Tarih");

        jLabel5.setText("Brüt Ağırlık");

        jLabel6.setText("Net Ağırlık");

        jLabel7.setText("Birim Fiyat");

        jLabel8.setText("Alınan Miktar");

        jftxtBrut.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getIntegerInstance())));

        jftxtNet.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getIntegerInstance())));

        jftxtBFiyat.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));

        jftxtKazanc.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));

        jLabel9.setForeground(new java.awt.Color(255, 0, 0));
        jLabel9.setText("ÇIKMA = ");

        jbtCikma.setForeground(new java.awt.Color(255, 0, 0));
        jbtCikma.setText("EKLE");

        jftCikma.setEditable(false);
        jftCikma.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getIntegerInstance())));

        javax.swing.GroupLayout jpnlCikmaLayout = new javax.swing.GroupLayout(jpnlCikma);
        jpnlCikma.setLayout(jpnlCikmaLayout);
        jpnlCikmaLayout.setHorizontalGroup(
            jpnlCikmaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnlCikmaLayout.createSequentialGroup()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addComponent(jftCikma, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jbtCikma))
        );
        jpnlCikmaLayout.setVerticalGroup(
            jpnlCikmaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnlCikmaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jbtCikma)
                .addComponent(jLabel9)
                .addComponent(jftCikma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jpnlCikma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(82, 82, 82))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                                        .addGap(43, 43, 43)))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel7)
                                    .addGap(81, 81, 81)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(69, 69, 69)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jftxtBrut, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                            .addComponent(jftxtNet)
                            .addComponent(jftxtBFiyat)
                            .addComponent(jftxtKazanc)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel4))
                                .addGap(68, 68, 68)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jcbSatisTuru, 0, 81, Short.MAX_VALUE)
                                    .addComponent(jcbKomisyoncu, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jcTarih)))
                            .addComponent(jLabel3))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(19, 19, 19))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jcTarih, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbSatisTuru, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jcbKomisyoncu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jftxtBrut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jftxtNet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jftxtBFiyat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jftxtKazanc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jpnlCikma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(103, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jcbSatisTuruActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbSatisTuruActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcbSatisTuruActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton jbtCikma;
    protected javax.swing.JTextField jcTarih;
    private javax.swing.JComboBox jcbKomisyoncu;
    protected javax.swing.JComboBox jcbSatisTuru;
    private javax.swing.JFormattedTextField jftCikma;
    private javax.swing.JFormattedTextField jftxtBFiyat;
    protected javax.swing.JFormattedTextField jftxtBrut;
    private javax.swing.JFormattedTextField jftxtKazanc;
    protected javax.swing.JFormattedTextField jftxtNet;
    private javax.swing.JPanel jpnlCikma;
    protected javax.swing.JTable jtUrunler;
    // End of variables declaration//GEN-END:variables

    @Override
    public Object[] ekle() {
        satisTuruId = satisTuruId <= 0 ? RowData_Satis.getLastSatisTuruId() + 1 : satisTuruId;
        MyComboBoxModel cm, cm2;
        Object[] data = new Object[11];
        RowData_Satis rd = new RowData_Satis();
        //satis cinsi id
        cm = (MyComboBoxModel) this.jcbSatisTuru.getModel();
        data[2] = cm.getSelectedItemId();
        // komisyoncu id
        cm2 = (MyComboBoxModel) this.jcbKomisyoncu.getModel();
        data[3] = cm2.getSelectedItemId();

//        tarih
        data[8] = ((JCalendar) this.jcTarih).getDate().getTime();
// satis turu
        data[10] = satisTuruId;

        UrunTableModel tm = (UrunTableModel) this.jtUrunler.getModel();
        double topOran = tm.getToplamOran();
        System.err.println(topOran);
        for (int i = 0; i < tm.getRowCount() && topOran != 0; i++) {
            if ((Double) tm.getValueAt(i, 2) == 0) {
                continue;
            }
            double birimOran = (Double) tm.getValueAt(i, 2) / topOran;
            //mahsul id
            data[1] = tm.getRowId(i);
            //brut
            data[4] = (birimOran * ((Number) this.jftxtBrut.getValue()).longValue());
            //net
            data[5] = (birimOran * ((Number) this.jftxtNet.getValue()).longValue());
            //birim fiyat
            data[6] = ((Number) this.jftxtBFiyat.getValue()).floatValue();
            //kazanc
            data[7] = (Double) (birimOran * ((Number) this.jftxtKazanc.getValue()).longValue());
            //parent id
            data[9] = tm.getParentId(i);
            System.err.println(Arrays.asList(data));
            data[0] = rd.insertData(data);
            tm.setSatisId(i, (Integer) data[0]);
        }
        return null;
    }

    @Override
    public int yeni() {
        this.jcTarih.getDocument().removeDocumentListener(l);
        ((UrunTableModel) this.jtUrunler.getModel()).refreshTable();
        ((JCalendar) this.jcTarih).setDate(new java.util.Date());
        this.jftCikma.setValue(0);
        this.jftxtBFiyat.setValue(0);
        this.jftxtBrut.setValue(0);
        this.jftxtKazanc.setValue(0);
        this.jftxtNet.setValue(0);
        this.jcTarih.getDocument().addDocumentListener(l);
        this.satisTuruId = -1;
        this.jbtCikma.setText("Ekle");
        return 0;
    }

    @Override
    public Object[] guncelle() {
        if (satisTuruId < -1) {
            System.err.println("HATA : satis turu 0 dan buyuk olmasi gerekiyor");
            return null;
        }
        new RowData_Satis().deleteDataBySatisTuru(this.satisTuruId);
        this.ekle();
        return null;

    }

    protected class UrunTableModel extends AbstractSeraTableModel {

        RowData_Urun rd;
        private Double[] oran;
        public Integer[] parentIds;
        public Integer[] satisIds;

        public Double getToplamOran() {
            double top = 0.0;
            for (Double i : oran) {
                top += i;
            }
            return top;
        }

        public UrunTableModel() {
            rd = new RowData_Urun();
            this.headings = new String[]{"Yer", "Ürün", "Oran"};
            this.load();
        }

        @Override
        public void load() {
            this.rowData = rd.getData();
            oran = new Double[this.rowData.length];
            util.UsefulFunctions.resetNumber(oran);
            parentIds = new Integer[this.rowData.length];
            util.UsefulFunctions.resetNumber(parentIds);
            satisIds = new Integer[this.rowData.length];
            util.UsefulFunctions.resetNumber(satisIds);
        }

        public Integer getSatisId(int rowIndex) {
            return this.satisIds[rowIndex];
        }

        public Integer getParentId(int rowIndex) {
            return this.parentIds[rowIndex];
        }

        public void setAllSatisId(Integer[] satisId) {
            this.satisIds = satisId;
        }

        public void SetAllParentIds(Integer[] parentId) {
            this.parentIds = parentId;
        }

        public void setSatisId(int rowIndex, Integer val) {
            this.satisIds[rowIndex] = val;
        }

        public void setParentId(int rowIndex, Integer val) {
            this.parentIds[rowIndex] = val;
        }

        public RowData_Urun getRd() {
            return rd;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object[] tmp = this.rowData[rowIndex];
            switch (columnIndex) {
                case 0:
                    return tmp[4];
                case 1:
                    return tmp[5];
                case 2:
                    return oran[rowIndex];
                default:
                    return null;
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 2:
                    return Double.class;
                default:
                    return String.class;
            }
        }

        @Override
        public boolean isCellEditable(int r, int c) {
            return c == 2;
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            if (columnIndex == 2) {
                this.oran[rowIndex] = (Double) aValue;
                this.fireTableCellUpdated(rowIndex, columnIndex);
            }
        }

        private Integer[] getAllParentIds() {
            return this.parentIds;
        }

        private Integer[] getAllSatisIds() {
            return this.satisIds;
        }
    }

    private void calculateKazanc() {
        double kazanc = 0;
        try {
            double net = ((Number) this.jftxtNet.getValue()).doubleValue();
            double fiyat = ((Number) this.jftxtBFiyat.getValue()).floatValue();
            kazanc = net * fiyat;
        } catch (Exception e) {
        } finally {
            this.jftxtKazanc.setValue(kazanc);
        }
    }

    class CalculateKazancDocumentListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            calculateKazanc();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }
    }

    private class TarihDocumentListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            int option = JOptionPane.showConfirmDialog(null, "Ürünler tablosu bu tarihe göre değiştirilsin mi?", "", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_OPTION) {
                updateUrunlerTable(((JCalendar) jcTarih).getDate().getTime());
            }
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }
    }

    /**
     * cikma varsa bu fonksiyon cagiriliyor burada eger o satislarin cikmasi
     * satildiysa butonda ekle yerine guncelle yazilacak ve sayfa acildiginda
     * cikmalarin oldugu veriler olacak her zamanki gibi oranlar ve brut kilo
     * degistirilemeyecek
     */
    private int getRecordedCikmaSatisTuruId() {
        RowData_Satis rd = new RowData_Satis();
        UrunTableModel tm = (UrunTableModel) this.jtUrunler.getModel();
        Integer[] parentIds = tm.getAllSatisIds();
        rd.setWhere("");
        Object[][] tmp = rd.getDataByParentIds(parentIds);
        return tmp != null && tmp.length != 0 ? (Integer) tmp[0][15] : -1;
    }
}
