/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package satis;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import main.AbstractSeraMainPanel;
import main.AbstractSeraTableModel;
import main.SeraTablePanel;
import util.AgirlikBirimiCellRenderer;
import util.MoneyCellRenderer;

/**
 *
 * @author alptug
 */
public class SatisMainPanel extends AbstractSeraMainPanel {

    SatisKayitPanel kayitPanel;
    SeraTablePanel tablePanel;

    public SatisMainPanel() {
        kayitPanel = new SatisKayitPanel();
        tablePanel = new SeraTablePanel(new SatisTableModel());
        super.addKayitPanel(kayitPanel);
        super.addTablePanel(tablePanel);
        JTable t = tablePanel.getTable();
        t.getSelectionModel().addListSelectionListener(this);
        t.getColumnModel().getColumn(3).setCellRenderer(new MoneyCellRenderer());
        t.getColumnModel().getColumn(5).setCellRenderer(new MoneyCellRenderer());
        t.getColumnModel().getColumn(4).setCellRenderer(new AgirlikBirimiCellRenderer());
        tablePanel.setToplamPanel(true);
//        tablePanel.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//        tablePanel.getTable().setDefaultRenderer(Object.class, new RowRenderer());

    }

    @Override
    protected void tableToKayitPanel(int index) {
        SatisTableModel stm = (SatisTableModel) this.tablePanel.getModel();
        int satisTuruId = stm.getSatisTuruId(index);
        JTable tb = tablePanel.getTable();
        for (int i = 0; i < tb.getRowCount(); i++) {
            if (i == index) {
                continue;
            }
            if (stm.getSatisTuruId(i) == satisTuruId) {
                tb.addRowSelectionInterval(i, i);
            }
        }
        tb.repaint();
        kayitPanel.loadExistingValues(satisTuruId);

    }
    
    
    //HATALI

    class RowRenderer extends DefaultTableCellRenderer {
        
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component c  = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            SatisTableModel tm = (SatisTableModel) table.getModel();
            int satisTuruId = tm.getSatisTuruId(row);
            for(int i=0; i<table.getRowCount(); i++){
                if(tm.getSatisTuruId(i)==satisTuruId)
                    c.setForeground(table.getSelectionForeground());
                else
                    c.setForeground(Color.LIGHT_GRAY);
            }
            return c;
        }
    }
}
