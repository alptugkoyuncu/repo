/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package satis;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import main.AbstractSeraTableModel;
import satis.db.RowData_Satis;

/**
 *
 * @author alptug
 */
public class SatisTableModel extends AbstractSeraTableModel{

    RowData_Satis rd;

    public SatisTableModel() {
        super(true);
        rd = new RowData_Satis();
        rd.listeyiSirala(RowData_Satis.TARIHE_ANA_SATISA_GORE_SIRALA);
        this.headings = new String[]{
            "Tarih", "Yer", "Ürün", "Alınan Miktar", "Net", "Birim Fiyat","Komisyoncu","Satış Türü"};
//        String[] cols = 
//                new String[]{"satis_id","mahsul_id","satis_cinsi_id","komisyoncu_id"
//                ,"satis_turu","cati_adi","cesit_adi","komisyoncu_adi"
//                ,"komisyoncu_soyadi","brut","net"
//                 ,"birim_fiyat","alinan_miktar","satis_tarihi","parent_id"
//                };
        this.load();
        this.setToplamSutunuFunc(3, TOPLAM);
        this.setToplamSutunuFunc(4, TOPLAM);
        this.setToplamSutunuFunc(5, ORTALAMA);
    }

    @Override
    public Number setOrtalamaFunction(int colNumber) {
        return this.getToplamSutunuValue(3).doubleValue()/this.getToplamSutunuValue(4).doubleValue();
    }
    @Override
    public void load() {
        this.rowData = rd.getData();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object[] temp = this.rowData[rowIndex];
        switch (columnIndex) {
            case 0:
                return temp[13];
            case 1:
                return temp[5];
            case 2:
                return temp[6];
            case 3:
                return temp[12];
            case 4:
                return temp[10];
            case 5:
                return temp[11];
            case 6: return util.UsefulFunctions.basHarfBuyuk((String) temp[7]) + " " +((String)temp[8]).toUpperCase();
            case 7:
                return temp[4];
            default:
                return null;
        }
    }
    public Integer getParentId(int rowIndex){
        return (Integer) this.rowData[rowIndex][14];
    }
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return java.sql.Date.class;
            case 1:
            case 2:
                return String.class;
            case 3:
                return Double.class;
            case 4:
                return Double.class;
            case 5:
                return Float.class;
            default:
                return String.class;
        }
    }
    public Integer getSatisTuruId(int rowIndex){
        return (Integer) this.rowData[rowIndex][15];
    }
}
