/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package satis.db;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.db.DBConnection;

/**
 * //
 *
 * @author alptug BD CONNECTION'a eklenmis where limit gibi query eklemelere
 * gore hazirlandi
 */
public class RowData_Satis extends DBConnection {

    public final static int TARIHE_GORE_SIRALA = 0;
    public final static int TARIHE_ANA_SATISA_GORE_SIRALA = 1;
    public final static String ARTAN_SIRALAMA = "ASC";
    public final static String AZALAN_SIRALAMA = "DESC";

    public RowData_Satis() {
        super.setQuery("");
    }

    @Override
    public Object[][] getData(String[] cols) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object[][] getData() {
        String[] cols = new String[]{
            "satis_id", "mahsul_id", "satis_cinsi_id", "komisyoncu_id",
            "satis_turu", "cati_adi", "cesit_adi", "komisyoncu_adi",
            "komisyoncu_soyadi", "brut", "net", "birim_fiyat",
            "alinan_miktar", "satis_tarihi", "satis_parent_id", "satis_turu_id"
        };
        String q;
        q = "SELECT "
                + "s.satis_id,"
                + "s.mahsul_id,"
                + "s.satis_cinsi_id,"
                + "s.komisyoncu_id,"
                + "sc.satis_turu,"
                + "c.adi as cati_adi,"
                + "mc.cesit_adi,"
                + "k.adi as komisyoncu_adi,"
                + "k.soyadi as komisyoncu_soyadi,"
                + "s.brut,"
                + "s.net,"
                + "s.birim_fiyat,"
                + "s.alinan_miktar,"
                + "s.satis_tarihi,"
                + "s.satis_parent_id,"
                + "s.satis_turu_id "
                + " FROM "
                + "satis s"
                + " LEFT OUTER JOIN "
                + "satis_cinsi sc "
                + " ON "
                + "sc. satis_cinsi_id = s.satis_cinsi_id and isLeaf=true "
                + " INNER JOIN "
                + " kisiler k "
                + " ON "
                + " k.kisiler_id = s.komisyoncu_id"
                + " INNER JOIN "
                + " mahsul m "
                + " ON "
                + " m.mahsul_id = s.mahsul_id "
                + " INNER JOIN "
                + "cesit mc "
                + " ON "
                + "mc.cesit_id = m.cesit_id "
                + " INNER JOIN "
                + "cati c "
                + " ON c.cati_id = m.cati_id ";
        try {
            return this.getData(q, cols);
        } catch (SQLException ex) {
            System.err.println(super.getQuery());
            Logger.getLogger(RowData_Satis.class.getName()).log(Level.SEVERE, null, ex);
            return new Object[0][0];
        }
    }

    public void listeyiSirala(int siralamaId, String siralamaDuzeni) {
        switch (siralamaId) {
            case TARIHE_GORE_SIRALA:
                super.setOrderBy("satis_tarihi " + siralamaDuzeni);
                break;
            case TARIHE_ANA_SATISA_GORE_SIRALA:
                super.setOrderBy("satis_tarihi " + siralamaDuzeni + ", s.satis_id ASC ");
                break;
            default:
                super.setOrderBy(null);
        }
    }

    public void listeyiSirala(int siralamaId) {
        this.listeyiSirala(siralamaId, AZALAN_SIRALAMA);
    }

    @Override
    public int insertData(Object[] data) {
        String query = "Insert into satis (mahsul_id,satis_cinsi_id,komisyoncu_id,brut,net,birim_fiyat,alinan_miktar,satis_tarihi,satis_parent_id,satis_turu_id)"
                + " VALUES(" + (Integer) data[1] + "," + (Integer) data[2] + "," + (Integer) data[3] + "," + (Double) data[4] + "," + (Double) data[5] + "," + (Float) data[6] + "," + (Double) data[7] + "," + util.UsefulFunctions.sqlMakeDateFunc((java.util.Date) data[8]) + "," + (Integer) data[9] + "," + (Integer) data[10] + ")";
        System.err.println(query);
        data[0] = this.processQuery(query);
        return (Integer) data[0];
    }

    public static Integer getLastSatisTuruId() {
        String query = "select max(satis_turu_id)  as a from satis";
        Object[][] o = null;
        try {
            o = new RowData_Satis().getData(query, new String[]{"a"});
            if (o == null || o[0][0] == null) {
                return 0;
            } else {
                return (Integer) o[0][0];
            }
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public boolean updateData(int id, Object[] data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int deleteData(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Object[][] getDataBySatisTuru(int satisTuruId) {
        this.setWhere("s.satis_turu_id = " + satisTuruId);
        return this.getData();
    }

    public Object[][] getDataByParentIds(Integer[] parentId) {
        String s = "";
        for (Integer id : parentId) {
            if (id > 0) {
                s += id + ",";
            }
        }
        if (s.length() == 0) {
            return null;
        }
        s = s.substring(0, s.length() - 1);
        this.setWhere("s.satis_parent_id IN(" + s + ")");
        return this.getData();
    }

    public void deleteDataBySatisTuru(int satisTuruId) {
        this.processQuery("DELETE FROM satis where satis_turu_id = " + satisTuruId);
    }
}
