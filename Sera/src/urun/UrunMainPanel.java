 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package urun;

import javax.swing.ListSelectionModel;
import main.AbstractSeraMainPanel;
import main.SeraTablePanel;

/**
 *
 * @author alptug
 */
public class UrunMainPanel extends AbstractSeraMainPanel{
    SeraTablePanel tp ;
    UrunKayitPanel kayitPanel;
    public UrunMainPanel(){
        super();
        tp = new SeraTablePanel(new UrunTableModel());
        kayitPanel = new UrunKayitPanel();
        super.addKayitPanel(kayitPanel);
        super.addTablePanel(tp);
        
        tp.getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tp.getTable().getSelectionModel().addListSelectionListener(this);
    }
    @Override
    protected void tableToKayitPanel(int index) {
        UrunTableModel tm = (UrunTableModel) this.tp.getTable().getModel();
        kayitPanel.setData(tm.rowData[index]);
    }
    
}
