/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package urun;

import java.util.Date;
import main.AbstractSeraTableModel;
import urun.db.RowData_Urun;

/**
 *
 * @author alptug
 */
class UrunTableModel extends AbstractSeraTableModel {

    private final static String[] h = new String[]{"Sebze Adı", "Sera Adi", "Ürün Adı", "Anaç", "toplam tonaj", "genel fiyat ort.", "top. alinan para", "Dikim Tarihi", "Söküm Tarihi"};

    public UrunTableModel() {
        this(false);
    }

    public UrunTableModel(boolean hasPaging) {
        super(hasPaging);

        this.headings = h;
        String[] cols = new String[]{
            "sebzeAdi",
            "urunAdi",
            "seraAdi",
            "toplamTonaj",
            "genelFiyatOrtalamasi",
            "toplamAlinanPara",
            "dikimTarihi",
            "sokumTarihi",
            "bilgi"
        };
        this.load();
    }

    @Override
    public void load() {
        this.unload();
        this.rowData = new RowData_Urun().getData();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return this.rowData[rowIndex][columnIndex + 3];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        try {
            switch (columnIndex) {
                case 0:
                case 1:
                case 2:
                case 3:
                    return String.class;
//                case 4: case 5: case 6: return Float.class;
                case 7:
                    return java.sql.Date.class;
                case 9:
                    return java.sql.Date.class;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            return super.getColumnClass(columnIndex); //To change body of generated methods, choose Tools | Templates.

        }
    }
}
