/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package urun.db;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.db.DBConnection;
import util.JCalendar;

/**
 *
 * @author alptug
 */
public class RowData_Urun extends DBConnection {

    @Override
    public Object[][] getData(String[] cols) {
        throw new UnsupportedOperationException("Not supported yet.");

    }

    //<editor-fold defaultstate="collapsed" desc="flag=true : sokulmemis mahsuller, flag=false : sokulmus mahsuller ">
    public void sokulmemisUrunler(boolean flag) {
        String tmp = " sokum_tarihi " + (flag ? " is NULL " : " is not null ");
        if (this.getWhere().equalsIgnoreCase("")) {
            this.setWhere(tmp);
        } else {
            this.setWhere(this.getWhere()+" AND " + tmp);
        }
    }
    //</editor-fold>

    public void setTarih(String makeDate) {
        
        String tmp = " dikim_tarihi < " + makeDate + " and (sokum_tarihi is null or sokum_tarihi >=" + makeDate+")";
        if (this.getWhere().equalsIgnoreCase("")) {
            this.setWhere(tmp);
        } else {
            this.setWhere(this.getWhere()+" AND " + tmp);
        }
    }

    public void setAktif(boolean flag) {
         String tmp = " isAktif is "+flag;
        if (this.getWhere().equalsIgnoreCase("")) {
            this.setWhere(tmp);
        } else {
            this.setWhere(this.getWhere()+" AND " + tmp);
        }
    }

    @Override
    public Object[][] getData() {
        String[] cols = new String[]{
            "mahsul_id",
            "cati_id",
            "cesit_id",
            "sebzeAdi",
            "seraAdi",
            "urunAdı",
            "anac",
            "toplamTonaj",
            "genelFiyatOrtalamasi",
            "toplamAlinanPara",
            "dikimTarihi",
            "sokumTarihi",
            "bilgi"
        };
        String query = ""
                + "SELECT"
                + " mahsul.mahsul_id,"
                + " mahsul.cati_id,"
                + " mahsul.cesit_id,"
                + " sebze_adi as sebzeAdi,"
                + " cati.adi as seraAdi,"
                + " cesit_adi as urunAdi,"
                + " anac, "
                + "(  SELECT 'YAPIM ASAMASINDA') as toplamTonaj, "
                + "(  SELECT 'YAPIM ASAMASINDA') as genelFiyatOrtalamasi, "
                + "(  SELECT 'YAPIM ASAMASINDA') as toplamAlinanPara, "
                + " dikim_tarihi as dikimTarihi,"
                + " sokum_tarihi as sokumTarihi,"
                + " ( SELECT count(*) FROM bilgi WHERE bilgi.id = mahsul.mahsul_id AND bilgi.tablo_adi = 'mahsul') as bilgi "
                + " FROM "
                + " mahsul"
                + " LEFT OUTER JOIN "
                + " cati"
                + " ON  "
                + " mahsul.cati_id = cati.cati_id "
                + " LEFT OUTER JOIN "
                + " cesit "
                + " ON "
                + " cesit.cesit_id = mahsul.cesit_id";
        try {
            System.out.println(query);
            return super.getData(query, cols);
        } catch (SQLException ex) {
            System.err.println(query);
            Logger.getLogger(RowData_Urun.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Object[][] getAllCesit() {
        String[] cols = new String[]{"cesit_id", "cesit_adi", "sebze_adi", "anac"};
        String query = "SELECT * FROM cesit";
        try {
            return super.getData(query, cols);
        } catch (SQLException ex) {
            Logger.getLogger(RowData_Urun.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    @Override
    public int insertData(Object[] data) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean updateData(int id, Object[] data) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int deleteData(int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int mahsulEkle(Object[] d) {
        String query = "INSERT INTO mahsul "
                + "(cesit_id,cati_id,dikim_tarihi,sokum_tarihi)"
                + " VALUES("
                + d[0] + ","
                + d[1] + ","
                + ((JCalendar) d[2]).sqlMakeDate() + ","
                + ((JCalendar) d[3]).sqlMakeDate()
                + ")";
        System.out.println(query);
        return this.processQuery(query);
    }

    public int mahsulGuncelle(Object[] data, Integer mahsulId) {
        String query = "UPDATE "
                + " mahsul "
                + " SET "
                + " cesit_id = " + (Integer) data[0]
                + ", cati_id = " + (Integer) data[1]
                + ", dikim_tarihi = " + ((JCalendar) data[2]).sqlMakeDate()
                + ", sokum_tarihi = " + ((JCalendar) data[3]).sqlMakeDate()
                + " WHERE mahsul_id = " + mahsulId;
        return this.processQuery(query);
    }
}
