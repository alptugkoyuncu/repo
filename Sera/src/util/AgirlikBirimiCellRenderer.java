/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author alptug
 */
public class AgirlikBirimiCellRenderer implements TableCellRenderer {
    public final static int TON=0;
    public final static int KG=1;
    private int agirlikBirimi;

    public AgirlikBirimiCellRenderer() {
        this(KG);
    }
    
    public AgirlikBirimiCellRenderer(int agirlikBirimi){
        this.agirlikBirimi=agirlikBirimi;
    }
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        String str = "" + value+ " " +this.getAgirlikBirimi();
        JLabel lbl = new JLabel(str);
        lbl.setOpaque(row%2==1?true:false);
        if (isSelected) {
            lbl.setBackground(table.getSelectionBackground());
            lbl.setForeground(table.getSelectionForeground());
            lbl.setOpaque(true);
        }
        else{
            lbl.setBackground(table.getBackground());
            lbl.setForeground(table.getForeground());
        }
        return lbl;
    }
    private String getAgirlikBirimi(){
        switch(this.agirlikBirimi){
            case TON: return "ton";
            case KG: return "KG";
            default : return "";
        }
    }
}
