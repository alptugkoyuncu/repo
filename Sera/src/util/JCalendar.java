/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import util.listeners.MyMouseListener;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JTextField;
import util.listeners.MyCalendarComponentListener;

/**
 *
 * @author ISIK
 */
public class JCalendar extends JTextField {

    private MyCalendar mc;
    private MouseListener ml;

    public JCalendar(Container ContainerOfIF) {
        mc = new MyCalendar(this);
        setText(mc.getDate());
        this.setEditable(false);
        this.setLayout(new FlowLayout());
        ContainerOfIF.add(mc);
        //this.setLayout(new BorderLayout());
        //this.add(dt,BorderLayout.CENTER);
        //this.add(mc,BorderLayout.NORTH);
        ml = new MyMouseListener(this);
        this.addMouseListener(ml);

        this.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
        mc.addComponentListener(new MyCalendarComponentListener(this));
    }

    public void setDate(java.sql.Date sqlDate) {
        if (sqlDate == null) {
            this.setText((GregorianCalendar) null);
            this.mc.updateDate(null);
            return;
        }
        java.util.Date d = new java.util.Date(sqlDate.getTime());
        this.setDate(d);
    }

    @Override
    public void setEnabled(boolean flag) {
        super.setEnabled(flag);
        if (flag) {
            this.addMouseListener(ml);
        } else {
            this.removeMouseListener(ml);
        }


    }

    @Deprecated
    public void setText(String t) {
        super.setText(t); //To change body of generated methods, choose Tools | Templates.
    }

    private void setText(GregorianCalendar gc) {
        if(gc==null){
            super.setText("");
            return;
        }
        String day = gc.get(GregorianCalendar.DATE) > 9 ? Integer.toString(gc.get(GregorianCalendar.DATE)) : "0"+Integer.toString(gc.get(GregorianCalendar.DATE));
        String month =  gc.get(GregorianCalendar.MONTH) > 8 ? Integer.toString(gc.get(GregorianCalendar.MONTH)) : "0"+Integer.toString(gc.get(GregorianCalendar.MONTH)+1);
        super.setText(
                day
                + "-"
                + month
                + "-"
                + gc.get(GregorianCalendar.YEAR));

    }

    public boolean isValidText() {
        String s[] = this.getText().split("-");
        if (s.length != 3 && s[0].length() != 2 && s[1].length() != 2 && s[2].length() != 4) {
            return false;
        }
        for (int i = 0; i < s.length; i++) {
            for (int j = 0; j < s[i].length(); j++) {
                if (!Character.isDigit(s[i].charAt(j))) {
                    return false;
                }

            }
        }
        return true;

    }

    public void openCalendar() {
        mc.setVisible(true);
    }

    public void closeCalendar() {
        this.setText(mc.getDate());
    }

    public GregorianCalendar getDate() {
        return this.mc.getDate();
    }

    public void setDate(java.util.Date utilDate) {
        if (utilDate == null) {
            this.setText((GregorianCalendar) null);
            this.mc.updateDate(null);
            return;
        }
        this.mc.updateDate(getGregorian(utilDate));
        this.setText(getGregorian(utilDate));


    }

    private GregorianCalendar getGregorian(java.util.Date d) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(d);
        return gc;
    }

    /**
     * sqlde tarihi olusturacak fonksiyonu olusturur kullanimi : SELECT new
     * GregorianCalendar(selectedDate).makedate() seklindedir
     *
     * @return
     */
    public String sqlMakeDate() {
        
        GregorianCalendar gc = this.getDate();
        if(gc==null)
            return null;
        return "MAKEDATE(" + gc.get(Calendar.YEAR) + "," + gc.get(Calendar.DAY_OF_YEAR) + ")";
    }

    public java.sql.Date toSqlDate() {
        GregorianCalendar gc = this.getDate();
        if(gc==null)
            return  null;
        java.sql.Date d = new java.sql.Date(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DAY_OF_MONTH));
        return d;
    }
}
