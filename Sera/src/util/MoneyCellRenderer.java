/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.Component;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author alptug
 */
public class MoneyCellRenderer implements TableCellRenderer {

    public final static int TURK_LIRASI = 0;
    public final static int BASTA = -1;
    public final static int SONDA = -2;
    private int simge;
    private ImageIcon image;
    private int simgeYeri;
    private String simgeStr="";

    public MoneyCellRenderer() {
        this(TURK_LIRASI, SONDA);
    }

    public MoneyCellRenderer(int simge) {
        this(simge, SONDA);
    }

    public MoneyCellRenderer(int simge, int simgeYeri) {
//        this.setSimge(simge);
        this.setSimgeYeri(simgeYeri);
    }

    public void setSimgeYeri(int simgeYeri) {
        this.simgeYeri = simgeYeri;
    }

    public void setSimge(int simge) {
        this.simge = simge;
        try {
            BufferedImage ic = ImageIO.read(this.getImageFile(simge));
            image = new ImageIcon(ic);
            simgeStr="";
        } catch (IOException ex) {
            Logger.getLogger(MoneyCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
            image = null;
            simgeStr = this.getSimgeString(simge);
        }
    }

    private File getImageFile(int simge) {
        switch (simge) {
            case TURK_LIRASI:
                return new File("");
            default:
                return new File("");
        }
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        String str = "" + value;
        
        JLabel lbl = new JLabel();
        lbl.setOpaque(true);
        if(this.simgeYeri==BASTA){
            str=this.simgeStr+" "+str;
        }else{
            str=str+" " +this.simgeStr;
        }
       lbl.setText(str);
        lbl.setIcon(image);
        
        lbl.setIconTextGap(1);
        lbl.setOpaque(row%2==1?true:false);
        if (isSelected) {
            lbl.setBackground(table.getSelectionBackground());
            lbl.setForeground(table.getSelectionForeground());
            lbl.setOpaque(true);
        }
        else{
            lbl.setBackground(table.getBackground());
            lbl.setForeground(table.getForeground());
        }
        return lbl;
    }

    private String getSimgeString(int simge) {
        switch(simge){
            case TURK_LIRASI: return "TL";
            default: return "";
        }
    }
}
