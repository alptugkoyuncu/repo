/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author ISIK
 */
public class My2DArray {

    private int row, col;
    private Object[][] value;

    public My2DArray(int row, int col) {
        this.value = new Object[row][col];
    }

    public My2DArray(Object[][] arr) {
        this.setValue(arr);
    }

    public My2DArray() {
        this(0, 0);
    }

    public void setValue(Object[][] arr) {
        if (arr == null || arr.length == 0) {
            this.row = 0;
            this.col = 0;
            this.value = new Object[0][0];
        } else {
            this.value = new Object[arr.length][arr[0].length];
            for (int i = 0; i < arr.length; i++) {
                System.arraycopy(arr[i], 0, this.value[i], 0, arr[i].length);
            }
        }
    }

    public void addItem(Object[] item) {
        if (value.length == 0) {
            this.value = new Object[1][item.length];
            this.value[0] = item;
        } else {
            if (item.length != this.value[0].length) {
                JOptionPane.showConfirmDialog(null, item.length + "   " + this.value[0].length);
                return;
            }
            Object[][] temp = new Object[this.value.length + 1][item.length];
            for (int i = 0; i < value.length; i++) {
                System.arraycopy(value[i], 0, temp[i], 0, value[i].length);
            }

            System.arraycopy(item, 0, temp[temp.length - 1], 0, item.length);
            this.value = temp;
        }
    }

    public void insertItem(Object[] item, int index) {
        if (index < 0) {
            JOptionPane.showConfirmDialog(null, item.length + "   " + this.value[0].length);
            return;

        }
        if (index >= value.length) {
            JOptionPane.showConfirmDialog(null, item.length + "   " + this.value[0].length);
            addItem(item);
            return;
        }
        if (item.length != this.value[0].length) {
            JOptionPane.showConfirmDialog(null, item.length + "   " + this.value[0].length);
            return;
        }
        Object[][] temp = new Object[this.value.length + 1][item.length];
        int i = 0;
        for (; i < index; i++) {
            System.arraycopy(value[i], 0, temp[i], 0, value[i].length);
        }
        System.arraycopy(item, 0, temp[i++], 0, item.length);
        for (; i < value.length + 1; i++) {
            System.arraycopy(value[i - 1], 0, temp[i], 0, value[i - 1].length);
        }
        this.value = null;
        System.gc();
        this.value = temp;
    }

    public int removeItemAt(int index) {
        if (index < 0 || index >= this.value.length) {
            return -1;
        }
        if (index == 1) {
            this.value = null;
            return index;
        }
        Object[][] temp = new Object[this.value.length - 1][this.value[0].length];

        int i = 0;
        for (; i < index; i++) {
            System.arraycopy(value[i], 0, temp[i], 0, value[i].length);
        }
        for (; i < value.length - 1; i++) {
            System.arraycopy(value[i + 1], 0, temp[i], 0, value[i].length);
        }
        this.value = null;
        System.gc();
        this.value = temp;
        return index;
    }

    public Object removeItem(Object[] val) {
        if (this.value.length <= 0 || this.value[0].length != val.length) {
            return null;
        }
        int index;
        boolean find = false;
        for (int i = 0; i < value.length; i++) {
            find = true;
            index = i;
            for (int j = 0; j < value[i].length; j++) {
                if (value[i][j] != val[j]) {
                    find = false;
                }
            }
            if (find) {
                removeItemAt(index);
                return val;
            }
        }
        return null;
    }

    public Object[][] getValue() {
        return this.value;
    }

    public static Object[][] rowToColumn(Object[][] a) {
        Object[][] temp = new Object[a[0].length][a.length];
        for (int i = 0; i < a[0].length; i++) {
            for (int j = 0; j < a.length; j++) {
                temp[i][j] = a[j][i];
            }
        }

        return temp;
    }

    public static Object[] getSelectedRow(Object[][] a, int rowIndex) {
        Object[] selectedRow = new Object[a[rowIndex].length];
        System.arraycopy(a[rowIndex], 0, selectedRow, 0, a.length);
        return selectedRow;
    }

    public static Object[] selectedColumn(Object[][] a, int colIndex) {
        Object[] selectedCol = new Object[a.length];
        for (int i = 0; i < a.length; i++) {
            selectedCol[i] = a[i][colIndex];
        }
        return selectedCol;
    }

    public static void printArray(Object[][] s) {
        for (int i = 0; i < s.length; i++) {
            for (int j = 0; j < s[i].length; j++) {
                System.out.print(s[i][j] + "\t");
            }
            System.out.println("");
        }
    }

    public static List toList(Object[][] o) {
        List l = new ArrayList();
        for (int i = 0; i < o.length; i++) {
            List t = new ArrayList();
            for (int j = 0; j < o[i].length; j++) {
                t.add(o[i][j]);
            }
            l.add(t);
        }

        return l;
    }
}
