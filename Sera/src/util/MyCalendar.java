/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
import javax.swing.event.ChangeListener;
import util.listeners.BugunActionListener;
import util.listeners.MonthSelectorListener;
import util.listeners.YearSelectorListener;

/**
 *
 * @author ISIK
 */
public class MyCalendar extends JInternalFrame {

    GregorianCalendar gc;
    private JComboBox monthSelector;
    private JSpinner yearSelector;
    private DayListPanel daySelector;
    private Container container;
    private JButton jbBugun;
    public MyCalendar(Container f) {
       // JDesktopPane d = new JDesktopPane();
       // d.add(this);
        container = f;
        gc = new GregorianCalendar();
        int year = gc.get(Calendar.YEAR);
         
        monthSelector = new JComboBox(new MonthComboModel());
        monthSelector.setSelectedIndex(gc.get(Calendar.MONTH));
        ArrayList<Integer> list = new ArrayList<Integer>();
        for(int i=2000; i<year+5; i++)
            list.add(i);
        yearSelector = new JSpinner(new SpinnerListModel(list));
        yearSelector.setValue(gc.get(GregorianCalendar.YEAR));
       
        daySelector = new DayListPanel(this);

        JPanel ustPanel = new JPanel();
        ustPanel.setLayout(new FlowLayout());

        ustPanel.add(monthSelector);
        ustPanel.add(yearSelector);

        jbBugun = new JButton("Bugün");

        this.setLayout(new BorderLayout());
        this.add(ustPanel, BorderLayout.NORTH);
        this.add(daySelector, BorderLayout.CENTER);
        this.add(jbBugun,BorderLayout.SOUTH);
        
        jbBugun.addActionListener(new BugunActionListener(this));
        ItemListener msl = new MonthSelectorListener(this);
        monthSelector.addItemListener(msl);

        ChangeListener ysl = new YearSelectorListener(this);
        yearSelector.addChangeListener(ysl);
        
        this.pack();
    }
    public void updateDate(GregorianCalendar gc){
        if(gc==null)
            return;
        this.gc = gc;
        monthSelector.setSelectedIndex(gc.get(Calendar.MONTH));
        yearSelector.setValue(gc.get(GregorianCalendar.YEAR));
        this.daySelector.updateDate();
    }
    public void setDay(int day) {
        setDateValue(Calendar.DATE, day);
    }

    public void setMonth(int month) {
        setDateValue(Calendar.MONTH, month);
    }

    public void setYear(int year) {
        this.setDateValue(Calendar.YEAR, year);
    }

    private void setDateValue(int type, int value) {
        gc.set(type, value);
        this.daySelector.updateDate();
    }

    public int getDay() {
        return gc.get(Calendar.DATE);
    }

    public int getMonth() {
        return gc.get(Calendar.MONTH);
    }

    public int getYear() {
        return gc.get(Calendar.YEAR);
    }

    public GregorianCalendar getDate() {
        return gc;
    }

    public int getDayOfYear() {
        return gc.get(GregorianCalendar.DAY_OF_YEAR);
    }

    class DayLabel extends JLabel {

        boolean isDay;
        boolean isSelected;

        public DayLabel() {
            this.setBackground(Color.lightGray);
            this.setPreferredSize(new Dimension(25, 25));
            this.setHorizontalAlignment(JLabel.CENTER);
        }

        public void setIsDay(boolean flag) {
            this.isDay = flag;
        }
    }

    class DayListPanel extends JPanel {

        private JPanel weekNumbersPanel;
        private JPanel dayNamesPanel;
        private JPanel dayListPanel;
        private DayLabel[] weekNumbersLabels = new DayLabel[6];
        private DayLabel[] dayListLabels = new DayLabel[42];
        private int curDay;
        private int dayAmountInMonth;
        private int weekAmountInMonth;
        private int startDay;
        private int startWeek;
        private final String[] days = {"pzt", "salı", "çar", "per", "cum", "ctz", "pz"};
        private DayLabel selectedLabel = null;
        MouseListener ml;

        public DayListPanel(MyCalendar IFCalendar) {
            this.updateData();
            ml = new DaySelectorListener(this,IFCalendar);
            weekNumbersPanel = new JPanel();
            weekNumbersPanel.setLayout(new GridLayout(0, 1));

            //initialize week numbers and append to the week numbers panel
            for (int i = 0; i < weekNumbersLabels.length; i++) {
                weekNumbersLabels[i] = new DayLabel();
                weekNumbersLabels[i].setBackground(Color.YELLOW);
                weekNumbersLabels[i].setForeground(Color.red);
                weekNumbersLabels[i].setOpaque(true);
                weekNumbersLabels[i].setIsDay(false);
                weekNumbersPanel.add(weekNumbersLabels[i]);
            }

            dayListPanel = new JPanel();
            dayListPanel.setLayout(new GridLayout(0, 7));
            for (int i = 0; i < dayListLabels.length; i++) {
                dayListLabels[i] = new DayLabel();
                dayListPanel.add(dayListLabels[i]);
            }


            dayNamesPanel = new JPanel();
            dayNamesPanel.setLayout(new GridLayout(1, 8));
            this.setDayNames();

            this.setLayout(new BorderLayout());
            this.add(dayNamesPanel, BorderLayout.NORTH);
            this.add(weekNumbersPanel, BorderLayout.WEST);
            this.add(dayListPanel, BorderLayout.CENTER);
            
            updateDate();
        }

        private void setDayNames() {
            DayLabel l = new DayLabel();
            l.setBackground(Color.YELLOW);
            l.setForeground(Color.red);
            l.setOpaque(true);
            l.setIsDay(false);
            dayNamesPanel.add(l);
            for (int i = 0; i < 7; i++) {
                l = new DayLabel();
                l.setBackground(Color.YELLOW);
                l.setForeground(Color.red);
                l.setText(days[i]);
                l.setOpaque(true);
                l.setIsDay(false);
                dayNamesPanel.add(l);
            }
        }

        private void updateData() {
            curDay = gc.get(GregorianCalendar.DAY_OF_MONTH);
            this.dayAmountInMonth = gc.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
            this.weekAmountInMonth = gc.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH);
            this.startWeek = gc.get(GregorianCalendar.WEEK_OF_YEAR);
            gc.set(GregorianCalendar.DATE, 1);
            this.startDay = gc.get(GregorianCalendar.DAY_OF_WEEK);
            gc.set(GregorianCalendar.DATE, curDay);

            // pazari ilk gun olarak kabul ettigi icin
            if (startDay < GregorianCalendar.MONDAY) {
                startDay += 7;
            }

        }

        public void updateDate() {
            this.updateData();
            this.updateWeekNumbers();
            this.updateDayList();
        }

        public void setSelectedLabel(DayLabel dl) {
            if (this.selectedLabel != null) {
                deSelect();
            }
            dl.setOpaque(true);
            gc.set(GregorianCalendar.DATE, Integer.parseInt(dl.getText()));
            this.selectedLabel = dl;
            dl.repaint();
        }

        private void deSelect() {

            this.selectedLabel.setOpaque(false);
            this.selectedLabel.repaint();
            this.selectedLabel = null;
        }

        private void updateWeekNumbers() {
            for (int i = 0; i < this.weekAmountInMonth; i++) {
                weekNumbersLabels[i].setVisible(true);
                weekNumbersLabels[i].setText("" + this.startWeek++);
            }
            for (int i = this.weekAmountInMonth; i < weekNumbersLabels.length; i++) {
                weekNumbersLabels[i].setVisible(false);
            }
        }

        private void updateDayList() {
            int i = 0;
            while (i < (startDay - GregorianCalendar.MONDAY)) {
                dayListLabels[i].setText("");
                dayListLabels[i].setIsDay(false);
                dayListLabels[i].removeMouseListener(ml);
                i++;
            }
            System.out.println(i);
            int j = 0;
            while (j < this.dayAmountInMonth) {
                
                dayListLabels[i + j].setText("" + (j + 1));
                dayListLabels[i+j].removeMouseListener(ml);
                dayListLabels[i + j].addMouseListener(ml);
                dayListLabels[i + j].setIsDay(true);
                j++;
            }
            for (int k = i + j; k < dayListLabels.length; k++) {
                dayListLabels[k].setText("");
                dayListLabels[k].removeMouseListener(ml);
            }
            
            //call setSelected function for current day
            setSelectedLabel(dayListLabels[startDay - GregorianCalendar.MONDAY + curDay - 1]);
        }
    }

    class MonthComboModel extends DefaultComboBoxModel {

        Month[] months = new Month[]{
            new Month("Ocak", Calendar.JANUARY),
            new Month("Şubat", Calendar.FEBRUARY),
            new Month("Mart", Calendar.MARCH),
            new Month("Nisan", Calendar.APRIL),
            new Month("Mayıs", Calendar.MAY),
            new Month("Haziran", Calendar.JUNE),
            new Month("Temmuz", Calendar.JULY),
            new Month("Ağustos", Calendar.AUGUST),
            new Month("Eylül", Calendar.SEPTEMBER),
            new Month("Ekim", Calendar.OCTOBER),
            new Month("Kasım", Calendar.NOVEMBER),
            new Month("Aralık", Calendar.DECEMBER)
        };

        public MonthComboModel() {
            for (int i = 0; i < months.length; i++) {
                this.addElement(months[i]);
            }
        }
    }

    private static class DaySelectorListener implements MouseListener {

        DayListPanel dlp;
        MyCalendar mf;
        public DaySelectorListener(DayListPanel aThis,MyCalendar mf) {
            this.dlp = aThis;
            this.mf = mf;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            dlp.setSelectedLabel((DayLabel) e.getSource());
           // if (e.getClickCount() > 1) 
                    mf.setVisible(false);
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
    }

    public class Month {

        private String name;
        private Integer value;

        public Month(String name, int value) {
            this.name = name;
            this.value = value;
        }

        public String toString() {
            return this.name;
        }

        public Integer getValue() {
            return this.value;
        }
    }
}
