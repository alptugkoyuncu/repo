package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.DefaultComboBoxModel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class MyComboBoxModel extends DefaultComboBoxModel {

    private int idPosition;

    public MyComboBoxModel(List l, int idPosition) {
        super(l.toArray());
        this.idPosition = idPosition;
    }
    public MyComboBoxModel(Object[][] d, int idPosition){
        
        for(int i=0; i<d.length; i++){
            ArrayList l = new ArrayList();
            l.addAll(Arrays.asList(d[i]));
            super.addElement(l);
        }
        this.idPosition = idPosition;
    }
    public MyComboBoxModel(List l) {
        this(l, 0);
    }
    public MyComboBoxModel() {
        this(new ArrayList(), 0);
    }

    public MyComboBoxModel(int idPosition) {
        this(new ArrayList(), 0);
    }

    public void setIdPosition(int idPosition) {
        this.idPosition = idPosition;
    }

    public int getIdPosition() {
        return idPosition;
    }

    public void setSelectedItemById(int id) {
        for (int i = 0; i < this.getSize(); i++) {
            ArrayList l = (ArrayList) this.getElementAt(i);
            if (id == (Integer)l.get(this.idPosition)) {
                super.setSelectedItem(l);
            }
        }
    }

    public int getSelectedItemId() {
        ArrayList l = null;
        Object o = null;
        try{
            
        l = (ArrayList) super.getSelectedItem();
        o = l.get(this.getIdPosition());
        if (o.getClass() == Integer.class) {
            return ((Integer) o);
        }
        else if(o.getClass() == String.class)
            return Integer.parseInt((String)o);
        else
            return -1;
        }catch(Exception ex){
            System.err.println(l);
            System.err.println(o);
            return -1;
        }
    }

    public void setSelectedItemAt(int i) {
        if (i >= super.getSize()) {
            return;
        } else {
            super.setSelectedItem(super.getElementAt(i));
        }
    }
    public void add(List l){
        for(int i=0; i<l.size(); i++)
        this.addElement(l.get(i));
    }
    
}
