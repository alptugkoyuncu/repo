/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author alptug
 */
public class MyComboBoxRenderer extends JLabel implements ListCellRenderer {

    Integer[] valuePositions;
    String ayirac=" ";
    public MyComboBoxRenderer() {
        this(new Integer[]{0});
    }
    public MyComboBoxRenderer(Integer valuePosition){
        this(new Integer[]{valuePosition});
    }
    public MyComboBoxRenderer(Integer[] valuePositions) {
        this(valuePositions," ");

    }

    public MyComboBoxRenderer(Integer[] valuePositions,String ayirac) {
        this.valuePositions = valuePositions;
        setOpaque(true);
//        setHorizontalAlignment(CENTER);
        setVerticalAlignment(CENTER);
        this.setMinimumSize(new Dimension(28, 20));
        this.setPreferredSize(this.getMinimumSize());
        this.ayirac = ayirac;

    }
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

        try {
            List l = (ArrayList) value;

            String s = "" + l.get(this.valuePositions[0]);
            for (int i = 1; i < this.valuePositions.length; i++) {
                s += ayirac + l.get(this.valuePositions[i]);
            }
            this.setText(s);
        } catch (Exception e) {
            System.err.println(e);
            this.setText("");
        }
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        return this;
    }
}
