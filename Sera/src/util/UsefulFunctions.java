/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author alptug
 */
public class UsefulFunctions {
    private UsefulFunctions(){
    }
    public static String sqlMakeDateFunc(java.util.Date utilDate){
        Calendar gc = new GregorianCalendar();
        gc.setTime(utilDate);
        return sqlMakeDateFunc(gc);
    }
    public static String sqlMakeDateFunc(java.sql.Date sqlDate){
        Calendar c = new GregorianCalendar();
        c.setTimeInMillis(sqlDate.getTime());
        return sqlMakeDateFunc(c);
    }
    public static String sqlMakeDateFunc(Calendar c){
        return "MAKEDATE(" + c.get(Calendar.YEAR) + "," + c.get(Calendar.DAY_OF_YEAR) + ")";
    }
    public static void resetBoolean(Boolean[] a) {
        for (int i = 0; i < a.length; i++) {
            a[i] = false;
        }
    }

    public static void resetNumber(Integer[] a) {
        for (int i = 0; i < a.length; i++) {
            a[i] = 0;
        }
    }
    public static void resetNumber(Float[] a){
        for (int i = 0; i < a.length; i++) {
            a[i] = new Float(0);
        }
    }
    public static void resetNumber(Double[] a){
        for (int i = 0; i < a.length; i++) {
            a[i] = new Double(0);
        }
    }
    public static String basHarfBuyuk(String s){
        if(s==null)
            return null;
        if(s.length()==0)
            return "";
        if(s.length()==1)
            return s.toUpperCase();
        return s.substring(0,1).toUpperCase() + s.substring(1).toLowerCase();
    }
   
}
