/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;
import util.MyCalendar;

/**
 *
 * @author ISIK
 */
public class BugunActionListener implements ActionListener{
    MyCalendar mc;
    public BugunActionListener(MyCalendar aThis) {
        mc = aThis;
        System.out.println("Listener : BugunActionListener" );
    }

    @Override
    public void actionPerformed(ActionEvent e) {
            mc.updateDate(new GregorianCalendar());
            mc.setVisible(false);
    }
    
}
