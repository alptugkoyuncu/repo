/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util.listeners;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JComboBox;
import util.MyCalendar;

/**
 *
 * @author ISIK
 */
public class MonthSelectorListener implements ItemListener{
    MyCalendar mc;
    public MonthSelectorListener(MyCalendar mc) {
        this.mc = mc;
        System.out.println("Listener : MonthSelectorListener" );

    }

        @Override
        public void itemStateChanged(ItemEvent e) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                System.out.println("selected");
                MyCalendar.Month m = (MyCalendar.Month) ((JComboBox) e.getSource()).getSelectedItem();
                mc.setMonth(m.getValue());
            }
        }
}
