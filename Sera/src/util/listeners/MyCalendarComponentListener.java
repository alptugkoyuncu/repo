/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util.listeners;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import util.JCalendar;
import util.JCalendar;
import util.MyCalendar;

/**
 *
 * @author ISIK
 */
public class MyCalendarComponentListener implements ComponentListener {
    JCalendar jc;
    public MyCalendarComponentListener(JCalendar aThis) {
        this.jc = aThis;
        System.out.println("Listener : MyCalendarComponentListener" );

    }

    @Override
    public void componentResized(ComponentEvent e) {
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentShown(ComponentEvent e) {
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        MyCalendar mc = (MyCalendar)e.getComponent();
        jc.closeCalendar();
    }
    
}
