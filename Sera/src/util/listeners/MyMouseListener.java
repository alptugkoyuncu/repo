/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import util.JCalendar;

/**
 *
 * @author ISIK
 */
public class MyMouseListener implements MouseListener {

    JCalendar f;

    public MyMouseListener(JCalendar jc) {
        f = jc;
        System.out.println("Listener : MyMouseListener" );

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        f.openCalendar();
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
