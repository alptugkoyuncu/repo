/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util.listeners;

import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import util.MyCalendar;

/**
 *
 * @author ISIK
 */
public class YearSelectorListener implements ChangeListener{
    MyCalendar mc;
    public YearSelectorListener(MyCalendar mc) {
        this.mc = mc;
        System.out.println("Listener : YearSelectorListener" );
    }

        @Override
        public void stateChanged(ChangeEvent e) {
            JSpinner js = (JSpinner) e.getSource();
            mc.setYear((Integer) js.getValue());
        }
}
