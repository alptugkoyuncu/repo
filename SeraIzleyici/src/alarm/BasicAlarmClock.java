/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alarm;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author akoyuncu
 */
public abstract class BasicAlarmClock {

    URL soundurl;
    protected Clip clip;
    protected int startPosition = 0;
    protected int delay = 0;
    protected AudioInputStream audioIn;
    public final static int LOOP_CONTINOUSLY = Clip.LOOP_CONTINUOUSLY;
    int loopCount = 0;
    int playCount=0;
    private int position = 0;

    public abstract void setSystemVolume(int i);

    public abstract void increaseSystemVolume();

    public abstract void decreaseSystemVolume();

    public void setLoop(int miktar) {
        this.loopCount = miktar;
    }

    public BasicAlarmClock(String fileName) {
        this.setSoundFile(fileName);
        
    }
    private void loop(){
        if(position==0)
            return;
        if(loopCount==LOOP_CONTINOUSLY || playCount<loopCount){
            
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(BasicAlarmClock.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(this.position==0)
                return;
            this.start();
            playCount++;
        }
        else
            this.stop();
    }
    public void open() {
        if (this.soundurl == null) {
            System.err.println("Önce dosyayı belirleyiniz");
            return;
        }
        boolean hata = false;
        try {
            audioIn = AudioSystem.getAudioInputStream(this.soundurl);
            try {
                clip = AudioSystem.getClip();
               
                clip.open(audioIn);
            } catch (LineUnavailableException ex) {
                Logger.getLogger(BasicAlarmClock.class.getName()).log(Level.SEVERE, null, ex);
                hata = true;
            }
        } catch (UnsupportedAudioFileException ex) {
            hata = true;
            Logger.getLogger(BasicAlarmClock.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            hata = true;
            Logger.getLogger(BasicAlarmClock.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (hata) {
            System.err.println("Dosya açılamıyor");
        }
         this.clip.addLineListener(new LineListener() {
                    @Override
                    public void update(LineEvent le) {
                        System.err.println("Frame pos = "+le.getFramePosition());
                        if(clip.isRunning()){
                            System.out.println("Frame running");
                        }else{
                            loop();
                        }
                    }
                });
    }

    private synchronized void setPosition(int pos) {
        this.position = pos;
    }

    public void close() {
        position = 0;
        if (clip.isActive() || clip.isRunning()) {
            clip.stop();
        }
        clip.flush();
        try {
            audioIn.close();
        } catch (IOException ex) {
            Logger.getLogger(BasicAlarmClock.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setSoundFile(String fileName) {
        this.soundurl = this.getClass().getClassLoader().getResource("sound/" + fileName);
        System.out.println(this.soundurl);
    }

    public String getSoundFile() {
        String files[] = this.soundurl.getFile().split("/");
        return files[files.length - 1];
    }

    public void start() {
        this.position=1;
        if (clip.isRunning()) {
            clip.setFramePosition(0);
            System.out.println("clip is running");
        } else {
            clip.setFramePosition(startPosition);
            System.out.println("start position = " + startPosition);

        }
        if (loopCount > 1 || loopCount == LOOP_CONTINOUSLY) {
            System.out.println("loop= " + loopCount);
            clip.start();    

        } else {
            System.out.println("start= ");

            clip.start();
        }
    }

    public boolean isRunning() {
        return this.clip.isRunning();
    }

    public void stop() {
        System.out.println("stopped");
        if (clip.isRunning()) {
            clip.stop();
        }
        this.position=0;
        this.playCount=0;
    }

    public void pause() {
        try {
            this.startPosition = this.clip.getFramePosition();
        } catch (Exception e) {
            System.err.println(e);
            this.startPosition = 0;
        }
    }

    public void resume() {
        if (this.delay > 0) {
            return;
        }
        if (clip.isRunning()) {
            System.err.println("clip is running");
            return;
        }
        if (clip == null) {
            System.err.println("clip is null");
            return;
        }
        clip.setFramePosition(startPosition);
        clip.start();
    }

    public void delay(int sec) {
        this.delay = sec;
        this.pause();
        try {
            this.wait(this.delay);
        } catch (InterruptedException ex) {
            System.err.println(ex);
        }
        this.delay = 0;
        this.resume();
    }
}
