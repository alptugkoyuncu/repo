/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alarm;

/**
 *
 * @author akoyuncu
 */
public class MyAlarmClock extends BasicAlarmClock{
    Float alarmActivateValue;
    Float currentValue; 
   
    public MyAlarmClock() {
        super("jackhammer-04.wav");
        alarmActivateValue=null;
        currentValue=null;
    }
    public void setAlarmActivateValue(Float alarmActivateValue){
        this.alarmActivateValue=alarmActivateValue;
    }
    public void updateValue(Float value){
        this.currentValue=value;
        
        if((alarmActivateValue==null || currentValue==null) &&super.isRunning())
            super.stop();
        
    }
    private void check(){
        if(super.isRunning()){
            if(alarmActivateValue==null || currentValue==null || currentValue>=alarmActivateValue){
                super.stop();
            }
        }else{
            
        }
    }
    @Override
    public void setSystemVolume(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void increaseSystemVolume() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void decreaseSystemVolume() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
