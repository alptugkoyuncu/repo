/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera_izleyici;

import java.util.Calendar;

/**
 *
 * @author WIN7
 */
public class BasicTime implements Comparable {

    int saat;
    int dakika;
    int saniye;

    public BasicTime() {
        this.setTimeByDate(Calendar.getInstance());
    }

    public BasicTime(int saat, int dakika, int saniye) {
        this.setSaat(saat);
        this.setDakika(dakika);
        this.setSaniye(saniye);
    }

    public BasicTime setTimeByDate(Calendar c) {
        saat = c.get(Calendar.HOUR_OF_DAY);
        dakika = c.get(Calendar.MINUTE);
        saniye = 0;
        return this;
    }

    public void setSaat(int s) {
        this.saat = s > 23 || s < 0 ? 0 : s;
    }

    public void setDakika(int d) {
        this.dakika = d > 59 || d < 0 ? 0 : d;
    }

    public void setSaniye(int s) {
        this.saniye = s > 59 || s < 0 ? 0 : s;
    }

    public int getSaniye() {
        return saniye;
    }

    public int getSaat() {
        return saat;
    }

    public int getDakika() {
        return dakika;
    }

    public String toShortTime() {
        return this.getSaat() + ":" + this.getDakika();
    }

    public String toLongTime() {
        return this.toShortTime() + ":" + this.getSaniye();
    }

    @Override
    public int compareTo(Object o) {
        BasicTime t = (BasicTime) o;
        if (this.getSaat() > t.getSaat()) {
            return 1;
        }
        if (this.getSaat() < t.getSaat()) {
            return -1;
        }
        if (this.getDakika() > t.getDakika()) {
            return 1;
        }
        if (this.getDakika() < t.getDakika()) {
            return -1;
        }
        if (this.getSaniye() > t.getSaniye()) {
            return 1;
        }
        if (this.getSaniye() < t.getSaniye()) {
            return -1;
        }
        return 0;
    }

    public static String getAy(int a) {
        String ay;
        switch (a) {
            case 0:
                ay = "Ocak";
                break;
            case 1:
                ay = "Subat";
                break;
            case 2:
                ay = "Mart";
                break;
            case 3:
                ay = "Nisan";
                break;
            case 4:
                ay = "Mayis";
                break;
            case 5:
                ay = "Haziran";
                break;
            case 6:
                ay = "Temmuz";
                break;
            case 7:
                ay = "Agustos";
                break;
            case 8:
                ay = "Eylul";
                break;
            case 9:
                ay = "Ekim";
                break;
            case 10:
                ay = "Kasim";
                break;
            case 11:
                ay = "Aralik";
                break;
            default:
                ay = "tanimsiz";
                break;
        }
        return ay;
    }
}
