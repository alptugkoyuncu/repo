package sera_izleyici;

public interface Defines {
    public final static byte ARDUINO_PRINT_DATA=1;
    public final static byte ARDUINO_INFO_DATA=2;
    public final static byte ARDUINO_RESPOND_DATA=3;
    public final static byte ARDUINO_STOP=4;
    
    public final static byte ANDROID_START=5;
    public final static byte ANDROID_STOP=6;
    public final static byte ANDROID_TIMELESS_START=7;
    
    public final static int MAXIMUM_CALISMA_SURESI = 3600;//1 SAAT
    public final static int MINIMUM_CALISMA_SURE = 0;
    
    public final static boolean WITH_DRIVER = true;
    public final static boolean DEBUG =true;
    public final static boolean PRINTLN=true;
    
}
