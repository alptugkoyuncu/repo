/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera_izleyici.Sera;

/**
 *
 * @author WIN7
 */
public interface ArduinoActionConstraints {

    public final static int ACTION_GECERSIZ_KOMUT = 0;
   public final static byte ACTION_CLIENT_RESTARTED=1;
//sera acma kapama komutlari
    public final static int ACTION_HEMEN_DURDUR = 11;
    public final static int ACTION_TAM_AC = 12;
    public final static int ACTION_TAM_KAPAT = 13;
    public final static int ACTION_CALISTIR = 14;
    public final static int ACTION_GET_SERA_ACMA_INFO = 15;
    public final static int ACTION_SET_MAXIMUM_ACIKLIK_MIKTARI = 16;
    public final static int ACTION_SENT_MAXIMUM_ACIKLIK_MIKTARI = 17;
//sera bilgilerini dosyalama komutlari
    public final static int ACTION_LISTEDEN_SIL = 21;
    public final static int ACTION_LISTEDEN_SIL2 = 22;
    public final static int ACTION_LISTEYE_EKLE = 23;
    public final static int ACTION_LISTEYI_GUNCELLE = 24;
    public final static int ACTION_RESET_TIMER_LIST_AND_FILE = 25;
    public final static int ACTION_GET_TIMER_LIST_SIZE = 26;
    public final static int ACTION_GET_TIMER_LIST_ITEM_AT = 27;
    public final static int ACTION_LOAD_TIMER_LIST = 28;
    public final static int ACTIN_PRINT_TIMER_LIST = 29;
//saat ve memory ayarlama komutlari
    public final static int ACTION_INITIALIZE_CLOCK = 31;
    public final static int ACTION_PRINT_TIME = 32;
    public final static int ACTION_PRINT_MEMORY = 33;
}
