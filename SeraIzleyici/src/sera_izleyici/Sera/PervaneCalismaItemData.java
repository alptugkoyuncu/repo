/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera_izleyici.Sera;

import sera_izleyici.BasicTime;

/**
 *
 * @author WIN7
 */
public class PervaneCalismaItemData implements Comparable{
    boolean isAktif;
    BasicTime baslamaZamani;
    int istenilenAciklikMiktari;
    public PervaneCalismaItemData(BasicTime t,int aciklikMiktari,boolean isAktif){
        this.baslamaZamani = t;
        this.istenilenAciklikMiktari = aciklikMiktari;
        this.isAktif = isAktif;
    }

    public void setIsAktif(boolean isAktif) {
        this.isAktif = isAktif;
    }

    public void setBaslamaZamani(BasicTime baslamaZamani) {
        this.baslamaZamani = baslamaZamani;
    }

    public void setAciklikMiktari(int aciklikMiktari) {
        this.istenilenAciklikMiktari = aciklikMiktari;
    }
    public boolean isIsAktif() {
        return isAktif;
    }

    public BasicTime getBaslamaZamani() {
        return baslamaZamani;
    }

    public int getAciklikMiktari() {
        return istenilenAciklikMiktari;
    }

    @Override
    public int compareTo(Object o) {
        PervaneCalismaItemData dt = (PervaneCalismaItemData) o;
        return this.getBaslamaZamani().compareTo(dt.getBaslamaZamani());
    }

    @Override
    public boolean equals(Object obj) {
        return this.compareTo(obj)==0;
    }
}
