/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera_izleyici.Sera;

import java.util.ArrayList;
import java.util.Collections;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import sera_izleyici.BasicTime;

/**
 *
 * @author WIN7
 */
public class PervaneCalismaTableModel extends AbstractTableModel {

    private ArrayList<PervaneCalismaItemData> l;
    private final String[] columnNames = {"Aktif","Başlama Saati","Süre(sn)"};
    private boolean editable=true;
    public PervaneCalismaTableModel() {
        l = new ArrayList<>();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        PervaneCalismaItemData dt = this.l.get(rowIndex);
        if(columnIndex == 0){
            dt.setIsAktif((boolean) aValue);
        }
        this.fireTableCellUpdated(rowIndex, columnIndex);
    }
    @Override
    public String getColumnName(int column) {
        return columnNames[column]; //To change body of generated methods, choose Tools | Templates.
    }
    public void insertItem(PervaneCalismaItemData dt,int index){
        
        this.l.add(index, dt);
//        Collections.sort(l);
        this.fireTableRowsInserted(index, index);
//        return this.l.indexOf(l);
    }
    public int removeItemByTime(BasicTime t){
        for(int i=0; i<this.getRowCount();i++)
            if(this.l.get(i).getBaslamaZamani().equals(t)){
                this.removeItemAt(i);
                this.fireTableRowsDeleted(i, i);
                return i;
            }
        return -1;
    }
    public PervaneCalismaItemData removeItemAt(int index){
        if(index<0 || index>l.size())
            return null;
        PervaneCalismaItemData o =  this.l.remove(index);
        this.fireTableRowsDeleted(index, index);
        return o;
    }
    public boolean removeItem(PervaneCalismaItemData dt){
        int index = this.l.indexOf(dt);
        return this.removeItemAt(index)!=null;
    }
    public PervaneCalismaItemData getPervaneCalismaData(int rowIndex){
        if(rowIndex<0 || rowIndex>=this.l.size())
            return null;
        return this.l.get(rowIndex);
    }
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return Boolean.class;
        } else {
            return super.getColumnClass(columnIndex); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public int getRowCount() {
        return l.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        PervaneCalismaItemData dt = l.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return dt.isIsAktif();
            case 1:
                return dt.baslamaZamani.toShortTime();
            case 2:
                return dt.istenilenAciklikMiktari;
            default:
                return "";
        }
    }

    void removeAll() {
        this.l.clear();
        this.fireTableDataChanged();
    }

}
