/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera_izleyici.Sera;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JFormattedTextField;
import sera_izleyici.BasicTime;

/**
 *
 * @author WIN7
 */
public class TimeField extends JFormattedTextField {

        public TimeField() {
            this(new BasicTime());

        }

        public TimeField(BasicTime t) {
            super(DateFormat.getTimeInstance(DateFormat.SHORT));
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, t.getSaat());
            c.set(Calendar.MINUTE, t.getDakika());
            c.set(Calendar.SECOND, t.getSaniye());
            this.setValue(c.getTime());
        }
        
        public BasicTime getTime() {
            Date d = (Date) super.getValue();
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            return new BasicTime().setTimeByDate(c);
        }
        
        public void setTime(BasicTime t) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, t.getSaat());
            c.set(Calendar.MINUTE, t.getDakika());
            c.set(Calendar.SECOND, t.getSaniye());
            this.setValue(c.getTime());
        }
        public void setTime(Date d){
            this.setValue(d);
        }
    }
