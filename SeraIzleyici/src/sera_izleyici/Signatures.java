package sera_izleyici;



public interface Signatures {
	public final static boolean isCelcius = true;
	public final static boolean isKmh = true;
	public final static boolean isMM = true;
	public final static char DEGREE_SIGNATURE = (char)176;
	public final static String TEMPERATURE_SIGNATURE = Signatures.DEGREE_SIGNATURE+ (Signatures.isCelcius?  "C" :"F") ;
	public final static String WIND_SPEED_SIGNATURE = Signatures.isKmh ? "Km/h" : "Mile/h";
	public final static String RAIN_SIGNATURE = Signatures.isMM ? "mm/dk" : "";
	public final static String WIND_DIRECTION_SIGNATURE = Signatures.DEGREE_SIGNATURE+"";
	public final static String HUMIDITY_SIGNATURE = "%";
	
}
