package sera_izleyici.connection;

import java.util.EnumMap;
import java.util.Map;

public class GeneralSystemDefines {

    public enum CONNECTION_TYPE {
        SERVER((byte)0), ARDUINO((byte)1), USER((byte)2);
        private final byte value;

        CONNECTION_TYPE(byte val) {
            this.value = val;
        }
        public byte getValue(){
            return value;
        }

        public static CONNECTION_TYPE getElement(byte val) {
            for(CONNECTION_TYPE ct:CONNECTION_TYPE.values())
                if(ct.getValue()==val)
                    return ct;
            return null;
        }
        
    }
    public final static byte REQUEST_ID = 50;
    public final static byte ID_OK=51;
    public final static Map<CONNECTION_TYPE, CONNECTION_TYPE> mapping = new EnumMap<CONNECTION_TYPE, CONNECTION_TYPE>(CONNECTION_TYPE.class
    );

}
