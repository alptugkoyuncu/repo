/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera_izleyici.connection;

import java.util.ArrayList;
import sera_izleyici.connection.listeners.DataTransferEvent;
import sera_izleyici.connection.listeners.DataReceiveListener;
import sera_izleyici.connection.listeners.DataSentListener;
import util.Log;
import util.OsCheck;

/**
 *
 * @author WIN7
 */
public abstract class SeraConnection {

    private GeneralSystemDefines.CONNECTION_TYPE connectionType;
    Object data;
    
    private ArrayList<DataReceiveListener> _listener = new ArrayList<DataReceiveListener>();

    public String getServerEndLine(){
        OsCheck.OSType osType = OsCheck.getOperatingSystemType();
        switch(osType){
            case Windows: return "\r\n";
            case Linux : return "\n";
            case MacOS: return "\r";
                case Other: default:
                    throw new UnsupportedOperationException("Operating System no foun");
        }
    }
    public void setConnectionType(GeneralSystemDefines.CONNECTION_TYPE connectionType) {
        this.connectionType = connectionType;
    }

    public GeneralSystemDefines.CONNECTION_TYPE getConnectionType() {
        return connectionType;
    }

    public void addListener(DataReceiveListener l) {
        this._listener.add(l);
    }

    public void removeListener(DataReceiveListener l) {
        this._listener.remove(l);
    }

    public abstract void start();

    public abstract void sent(String data);

    protected void fireEvent(int transferType) {
        DataTransferEvent evt = new DataTransferEvent(this, data, transferType);
        for (DataReceiveListener l : _listener) {
            l.dataReceiveComplete(evt);
        }
    }
}
