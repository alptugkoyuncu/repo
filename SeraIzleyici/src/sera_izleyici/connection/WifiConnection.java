/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera_izleyici.connection;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import sera_izleyici.connection.listeners.DataTransferEvent;
import util.Log;

/**
 * wifi socket baglanti saglar client kismi
 *
 * @author WIN7
 */
public class WifiConnection extends SeraConnection implements Runnable {

    private String ip;
    private int port;
    Socket socket;
    DataInputStream in;
    PrintStream ps;
    private final static long PING_WAIT_SECOND = 60000;
    private long lastReceived, lastSent;
    public final static byte ACTION_SEND_PING = 0;

    public WifiConnection(String ip, int port, GeneralSystemDefines.CONNECTION_TYPE connectionType) {
        super.setConnectionType(connectionType);
        this.ip = ip;
        this.port = port;
        try {
            socket = new Socket(ip, port);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.l("Socket creation failed\nip = " + ip + "\\" + port);
            e.printStackTrace();
            // return false;
        }

    }

    private boolean sentHeadear() {
        Log.l("Start header");
        long maxWaitSecond = 60000;
        byte err = 0;
        int errTry = 10;
        long st = System.currentTimeMillis();
        while ((System.currentTimeMillis() - st < maxWaitSecond) && errTry > 0) {
            try {
                if (in.available() > 0) {
                    byte b = in.readByte();
                    Log.l("Yeni data : " + b);
                    if (b == GeneralSystemDefines.REQUEST_ID) {
                        ps.write(this.getConnectionType().getValue());
                        ps.flush();
                    } else if (b == this.getConnectionType().getValue()) {
                        ps.write(GeneralSystemDefines.ID_OK);
                        ps.flush();
                        Log.l("Header transfer completed");
                        return true;
                    } else {
                        ps.write(this.getConnectionType().getValue());
                        ps.flush();
                        errTry--;
                    }
                } else {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(WifiConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                Log.l(e.getMessage());
                e.printStackTrace();
            }

        }

        Log.l("Header transfer failed");
        return false;
//        this.connectionEstablished = true;

    }

    @Override
    public void run() {
        try {
            try {
                in = new DataInputStream(socket.getInputStream());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                Log.l(e.getMessage());
                e.printStackTrace();
                // return false;
            }

            try {
                ps = new PrintStream(socket.getOutputStream());
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                Log.l(e1.getMessage());
                e1.printStackTrace();
                // return false;
            }
            Log.l("Connection Established\nWaiting for arranging connection header");

            boolean flag = this.sentHeadear();
            Log.l("FLAG : " + flag);
            String cvp;


            /*
             * notice: inputStream.read() will block if no data return
             */
            lastReceived = System.currentTimeMillis();
            lastSent = 0;
            while (flag) {
                {
                    long curr = System.currentTimeMillis();
                    if (curr - lastSent > PING_WAIT_SECOND) {
                        this.sent("<" + ACTION_SEND_PING + ">");
                    }
                    if (curr - lastReceived > 3 * PING_WAIT_SECOND) {
                        break;
                    }
                }
                if (in != null && in.available() > 0) {
                    Log.l("New Data :");
                    cvp = in.readLine();
                    this.lastReceived = System.currentTimeMillis();
                    this.data = cvp;
                    Log.l(cvp);
                    this.fireEvent(DataTransferEvent.DATA_RECEIVED);

                }
                try {
                    Thread.sleep(5);
                } catch (InterruptedException ex) {
//                    Logger.getLogger(WifiConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.l("UnknownHostException: " + e.toString());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.l("IOException: " + e.toString());
        }
        try {
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(WifiConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        ps.close();
        try {
            socket.close();
        } catch (IOException ex) {
            Logger.getLogger(WifiConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void sent(String data) {
        this.data = data;
        ps.println(data);
        System.out.println("Data sent =>" + data);
        ps.flush();
        this.lastSent = System.currentTimeMillis();
        super.fireEvent(DataTransferEvent.DATA_SENT);

//        ps.close();    
    }

    @Override
    public void start() {
        new Thread(this).start();
    }

}
