package sera_izleyici.connection.connection_data;

import sera_izleyici.connection.connection_data.WheatherSensorData;

public class ConnectionData {

    private final WheatherSensorData wheatherSensorData;
    public String dataWithoutFirstHeader;

    public ConnectionData() {
        wheatherSensorData = new WheatherSensorData();
        wheatherSensorData.setSplitter(" ");
    }

    public boolean updateData(String data) {
        System.out.println("data : "+data);
        try{
        int start = data.indexOf("<");
        int end = data.indexOf(">");
        if (start < 0 || end < 0 || end - start < 2 || data.length() != end + 1) {
            System.out.println("Data da sorun var : " + data);
            return false;
        }
        data = data.replace("<", "");
        data = data.replace(">", "");
        String[] temp = data.split(":");
        if (temp.length < 2) {
            System.out.println("Data da header sorun var : " + data);
            return false;
        }
        byte header = Byte.parseByte(temp[0].trim());

        String d = data.replaceFirst(temp[0] + ":", "");
        dataWithoutFirstHeader = d;
        switch (header) {
            case 1:{
                String temp2 = dataWithoutFirstHeader.split(":")[1];
                this.wheatherSensorData.updateInComeData(temp2);
                break;
            }
            case 2:
                break;
            default:
                System.err.println("Data headar not found : " + data);
                break;
        }
        }catch(Exception e){
            System.err.println("Data da sorun var : " + data);
            return false;
        }
        return true;
    }

    /**
     * @return the wheatherSensorData
     */
    public final WheatherSensorData getWheatherSensorData() {
        return wheatherSensorData;
    }
}
