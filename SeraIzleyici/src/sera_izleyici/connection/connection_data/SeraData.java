package sera_izleyici.connection.connection_data;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import sera_izleyici.Sera.PervaneCalismaItemData;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author WIN7
 */
public class SeraData {
    public static final int PROPERTY_ACIKLIK_MIKTARI = 1;
    public static final int PROPERTY_ISTENILEN_MIKTAR = 2;
    public static final int PROPERTY_DURUM = 3;
    public List<PropertyChangeListener> _listener;
    public static final int DURUYOR = 0;
    public static final int ACILIYOR = 1;
    public static final int KAPANIYOR = 2;
    private String splitter;
    private int aciklikMiktari;
    private int istenilenMiktar;
    private int durum;
    int index;
    PervaneCalismaItemData pervaneCalismaItemData;

    public void addPropertyChangListener(PropertyChangeListener l){
        this._listener.add(l);
    }
    
    public boolean removePropertyChangeListener(PropertyChangeListener l ){
        return this._listener.remove(l);
    }
    public SeraData(String inComeData) {
        this(inComeData, " ");
    }

    public SeraData() {
        this(null," ");
    }

    public SeraData(String inComeData, String splitter) {
        this.setSplitter(splitter);
        this.updateInComeData(inComeData);
        _listener = new ArrayList<PropertyChangeListener>();
    }

    private void setSplitter(String splitter) {
        this.splitter = splitter;
    }

    public void updateInComeData(String inComeData) {
        String[] temp;
        temp = inComeData.split(":");
        if(temp.length!=2)
            return;
        String[] header1 = temp[0].split(splitter);
        String[] data = temp[1].split(splitter);
        int act = Integer.parseInt(header1[0]);
        
    }

    public void setAciklikMiktari(int aciklikMiktari) {
        if(this.aciklikMiktari == aciklikMiktari)
            return;
        this.aciklikMiktari = aciklikMiktari;
        this.fireEvent(PROPERTY_ACIKLIK_MIKTARI, aciklikMiktari);
    }

    public void setIstenilenMiktar(int istenilenMiktar) {
        if(this.istenilenMiktar==istenilenMiktar)
            return;
        this.istenilenMiktar = istenilenMiktar;
        this.fireEvent(PROPERTY_ISTENILEN_MIKTAR, istenilenMiktar);
    }

    public void setDurum(int durum) {
        if(this.durum==durum)
            return;
        this.durum = durum;
        this.fireEvent(PROPERTY_DURUM, durum);
    }
    private void fireEvent(int propertyName,Object data){
        EventObject evt = new EventObject(this);
        for(PropertyChangeListener l : this._listener)
            l.propertyChanged(evt,propertyName,data);
    }
    public int getDurum() {
        return durum;
    }

    public int getAciklikMiktari() {
        return aciklikMiktari;
    }

    public int getIstenilenMiktar() {
        return istenilenMiktar;
    }
}
