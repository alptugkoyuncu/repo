package sera_izleyici.connection.connection_data;

import java.util.ArrayList;

public class WheatherSensorData {
	public final static int SENSOR_AMOUNT = 9;
	private float dsTemp;
	private float DHT21_1_temp;
        private float DHT21_2_temp;
	private float DHT21_1_hum;
        private float DHT21_2_hum;
	private float rain;
	private float windDirection;
	private float windSpeed;
	private float windGauge;
	private String splitter;
	
	WheatherSensorData(String inComeData){
		this(inComeData," ");
	}
	WheatherSensorData(){
		
	}
	WheatherSensorData(String inComeData,String splitter){
		this.setSplitter(splitter);
		this.updateInComeData(inComeData);
	}
	void updateInComeData(String inComeData){
		if(this.splitter==null)
			this.setSplitter(" ");
		String[] temp = inComeData.trim().split(splitter);
                
                ArrayList<String> sensors = new ArrayList<>();
                for(int i=0; i<temp.length;i++){
                    if(!temp[i].trim().equalsIgnoreCase(""))
                        sensors.add(temp[i]);
                }
		if(sensors.size()!=SENSOR_AMOUNT){
			System.out.println("Data miktarinda hata : " + temp.length);
			return;
		}
		int i=0;
		this.dsTemp = this.convertToFloat(sensors.get(i++));
		this.DHT21_1_temp = this.convertToFloat(sensors.get(i++));
                this.DHT21_2_temp = this.convertToFloat(sensors.get(i++));
		this.DHT21_1_hum = this.convertToFloat(sensors.get(i++));
                this.DHT21_2_hum = this.convertToFloat(sensors.get(i++));
		this.rain = this.convertToFloat(sensors.get(i++));
		this.windSpeed = this.convertToFloat(sensors.get(i++));
		this.windDirection = this.convertToFloat(sensors.get(i++));
		this.windGauge = this.convertToFloat(sensors.get(i++));
	}
	public void setSplitter(String splitter){
		this.splitter = splitter;
	}
        
    	private float convertToFloat(String val){
		try{
			return Float.parseFloat(val);
		}catch(Exception e){
			return 0;
		}
	}

    public float getDsTemp() {
        return dsTemp;
    }

    public float getDHT21_1_temp() {
        return DHT21_1_temp;
    }

    public float getDHT21_2_temp() {
        return DHT21_2_temp;
    }

    public float getDHT21_1_hum() {
        return DHT21_1_hum;
    }

    public float getDHT21_2_hum() {
        return DHT21_2_hum;
    }

    public float getRain() {
        return rain;
    }

    public float getWindDirection() {
        return windDirection;
    }

    public float getWindSpeed() {
        return windSpeed;
    }

    public float getWindGauge() {
        return windGauge;
    }
	
}
