/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera_izleyici.connection.listeners;

import java.util.EventObject;

/**
 *
 * @author WIN7
 */
public class DataTransferEvent extends EventObject{
    public static int DATA_SENT =1;
    public static int DATA_RECEIVED = 2;
    private Object data;
    private int transferType;
    public DataTransferEvent(Object source,Object data,int transferType) {
        super(source);
        this.data = data;
        this.transferType = transferType;
    }
    
    public Object getData(){
        return this.data;
    }
    public int getTransferType(){
        return this.transferType;
    }
}
