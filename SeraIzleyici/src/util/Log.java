/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.Calendar;

/**
 *
 * @author WIN7
 */
public class Log {
    public static void l(String s){
        System.out.println(Log.getTime() + "->"+s);
    }
    public static String getTime(){
        Calendar c = Calendar.getInstance();
        return getCalendarValue(c.get(Calendar.HOUR_OF_DAY))+":"+getCalendarValue(c.get(Calendar.MINUTE))+":" +getCalendarValue(c.get(Calendar.SECOND));
    }
    private static String getCalendarValue(int temp){
        return temp<10 ? "0"+temp : temp+"";
    }
}
