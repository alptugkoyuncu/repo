/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import de.progra.charting.CoordSystem;
import de.progra.charting.model.ChartDataModel;
import de.progra.charting.render.PlotChartRenderer;

/**
 *
 * @author alptug
 */
public class ChartRenderer extends PlotChartRenderer{
    
    public ChartRenderer(CoordSystem cs, ChartDataModel cdm) {
        super(cs, cdm);
        super.shapeSize=5.0;
    }
    
}
