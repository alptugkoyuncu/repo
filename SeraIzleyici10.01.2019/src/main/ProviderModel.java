/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import data.ArduinoData;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import data.DataTransferEvent;
import data.DataTransferListener;
import data.NetworkClientPackage;
import data.NetworkPackage;
import java.awt.Color;
import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import my_logger.L;
import network.ActionCode;
import network.GeneralSystemConstant;

/**
 *
 * @author alptug
 */
public class ProviderModel extends AbstractTableModel implements DataTransferListener {

    List<ArduinoData> data;
    String[] columnName;

    public ProviderModel() {
        data = new ArrayList<ArduinoData>();
        columnName = new String[]{"ID", "Temp 1", "Temp 2", "Temp 3", "Guncelleme Zamani"};
    }

    /**
     *
     * @param d
     * @return
     */
    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public String getColumnName(int column) {
        return columnName[column];
    }

    @Override
    public int getColumnCount() {
        return columnName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ArduinoData dt = data.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return dt.getName();
            case 1:
                return dt.getVal1();
            case 2:
                return dt.getVal2();
            case 3:
                return dt.getVal3();
            case 4:
                return dt.getLastUpdateTime();
            default:
                return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
            case 2:
            case 3:
                return Double.class;
                
            case 4:
                return java.util.Date.class;
        }
        return super.getColumnClass(columnIndex); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (columnIndex != 0 || rowIndex >= data.size()) {
            return;
        }
        String name = (String) aValue;
        this.data.get(rowIndex).setName(name);
        this.fireTableCellUpdated(rowIndex, columnIndex);

    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 0;
    }

    @Override
    public void dataReceived(DataTransferEvent evt) {
        NetworkClientPackage pck = (NetworkClientPackage) evt.getNetworkPackage();
        Short id = pck.getSender();
        if (id == GeneralSystemConstant.SERVER_ID) {
            return;
        }
        int index = getPackageIndex(pck.getSender());
        ArduinoData d = index == -1 ? new ArduinoData(id) : data.get(index);

        if (index == -1) {
            this.data.add(d);
            Collections.sort(data);
            this.fireTableDataChanged();
//            this.fireTableRowsInserted(data.size() - 1, data.size() - 1);
        }
        switch (pck.getAction()) {
            case ActionCode.CLIENT_CONNECTED: {
                return;
            }
            case ActionCode.CLIENT_DISCONNECTED: {
                this.data.remove(d);
                this.fireTableDataChanged();
                return;
            }
            case ActionCode.SENT_TEMP1: {
                d.setVal1(pck.getData().get(0));
                break;
            }
            case ActionCode.SENT_TEMP2: {
                L.debug("temp2");
                d.setVal2(pck.getData().get(0));

                break;
            }
            case ActionCode.SENT_TEMP3: {
                L.debug("temp3");
                d.setVal3(pck.getData().get(0));

            }
            default:
                break;
        }

        this.fireTableRowsUpdated(index, index);
    }

    private int getPackageIndex(Short id) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getId().shortValue() == id) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void dataSent(DataTransferEvent evt) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
