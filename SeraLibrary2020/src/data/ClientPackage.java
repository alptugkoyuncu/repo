/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.List;

/**
 * Client tarafindan okunan mesaj(server dan gonderilen)
 * message type
 * <hostId:Ac:data1;data2;...>
 *
 * @author ISIK
 */
public class ClientPackage extends NetworkPackage {

    @Deprecated
    @Override
    public void setHostId(Integer hostId) {
        super.setHostId(hostId); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public void setMessage(String clientMessage, Integer targetId) throws Exception {
        super.setTargetId(targetId);
//        message = message;
        int basIndex = clientMessage.indexOf(BASLANGIC_AYIRACI);
        int sonIndex = clientMessage.indexOf(BITIS_AYIRACI, basIndex);
        if (basIndex < 0 || sonIndex < 0) {
            setPackageHasError();
            throw new Exception("Data sorunlu baslangic veya bitis ayiraci yok\n MESSAGE : " +" Message : ["+clientMessage+"]");
//            return false;
        }
        String rawData = clientMessage.substring(basIndex + 1, sonIndex);

        String[] anaData = rawData.split(ANA_AYIRAC + "");
        if (anaData.length < MIN_ANA_AYIRAC_MIKTARI) {
            setPackageHasError();
            throw new Exception("Ana Data miktari hatali : " + anaData.length + "\\" + MIN_ANA_AYIRAC_MIKTARI+" Message : ["+clientMessage+"]");
            
        }
        //receiverlari bul ve ayarla
        String[] tmp = anaData[CLIENT_ID_ANA_AYIRAC_INDEXI].split(ARA_AYIRAC + "");
        //server
        if (tmp.length != CLIENT_ID_MIKTARI) {
            setPackageHasError();
            throw new Exception("host id sayisi 1 den baska olamaz : " + tmp.length+" Message : ["+clientMessage+"]");
            
        } else {
            Integer id;
            try {
                id = Integer.parseInt(tmp[0]);
                super.setHostId(id);
            } catch (NumberFormatException ex) {
                setPackageHasError();
                throw new Exception("Sender id sayi degil :" + tmp[0]+" Message : ["+clientMessage+"]");
              
            }
        }
        tmp = anaData[ACTION_CODE_ANA_AYIRAC_INDEX].split(ARA_AYIRAC + "");
        if (tmp.length != ACTION_CODE_MIKTARI) {
            setPackageHasError();
            throw new Exception("action code miktari uyumsuz " + tmp.length + "\\" + ACTION_CODE_MIKTARI+" Message : ["+clientMessage+"]");
            
        } else {
            try {
                Integer actionCode = Integer.parseInt(tmp[0]);
                setActionCode(actionCode);
            } catch (NumberFormatException ex) {
                setPackageHasError();
                throw new Exception("Action id sayi degil :" + tmp[0]+" Message : ["+clientMessage+"]");
            }
        }
        if (anaData.length > DATA_ANA_AYIRAC_INDEXI) {
            for(String s  : anaData[DATA_ANA_AYIRAC_INDEXI].split(ARA_AYIRAC+"")){
                addData(s);
            }
        }
        finishPacket();
    }

    @Override
    public String getMessage() {
        if(!isFinished()){
            return null;
        }
        StringBuilder builder = new StringBuilder();
        builder.append(BASLANGIC_AYIRACI);
        builder.append(getHostId());
        builder.append(ANA_AYIRAC);
        builder.append(getActionCode());
        builder.append(ANA_AYIRAC);
        List<String> arr = getDataArray();
        if (!arr.isEmpty()) {
            builder.append(arr.get(0));
            for (int i = 1; i < arr.size(); i++) {
                builder.append(ARA_AYIRAC);
                builder.append(arr.get(i));
            }
        }
        builder.append(BITIS_AYIRACI);
        return builder.toString();
    }
}
