/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author alptug
 */
public interface GeneralSystemConstant {
//    public final static int MAX_SERVER_CLIENT_HANDLER_NUMBER=20;
//    public final static int SERVER_DATA_INCOME_MAX_WAIT_MS=180000;
    
    public final static long MAX_NUMBER_OF_SERVER_ID_REQUEST=30;
    public final static long MAX_TIME_SERVER_ID_REQUEST_INTERVAL=2000;
    public final static long MAX_CLIENT_ID_COMFIRMATION_TIME=MAX_NUMBER_OF_SERVER_ID_REQUEST*MAX_TIME_SERVER_ID_REQUEST_INTERVAL+5000;
    public final static long PING_SENT_INTERVAL=5000;
    public final static long PING_WAIT_MS = 60000;
    public final static int SERVER_ID= 1000;//SERVER ID SABIT DIGERLERI DOSYADAN OKUNUYOR
    public final static Double ERROR_VALUE=-100.0;
    public final static int ESP_ID=999;
    
    public final static int SENSOR_START_ACTION_CODE =10;
    public final static int SENSOR_END_ACTION_CODE=59;
}
