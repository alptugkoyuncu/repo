/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ISIK
 */
public abstract class NetworkPackage {
    public final static char ANA_AYIRAC = ':';
    public final static char ARA_AYIRAC = ';';
    public final static char BASLANGIC_AYIRACI = '<';
    public final static char BITIS_AYIRACI = '>';
    
    protected final static int MIN_ANA_AYIRAC_MIKTARI = 2;
    protected final static int ACTION_CODE_ANA_AYIRAC_INDEX=1;
    protected final static int CLIENT_ID_ANA_AYIRAC_INDEXI=0;
    protected final static int DATA_ANA_AYIRAC_INDEXI=2;
    protected final static int ACTION_CODE_MIKTARI = 1;
    protected final static int CLIENT_ID_MIKTARI=1;
    private static int createCount=0;
    private static int finalizeCount=0;
    private Integer hostId;
    private Integer targetId;
    private Integer actionCode;
  private List<String> dataArray;
    private StringBuilder buffer;
    private boolean finished;
    private boolean error;
    private Date messageDate;
    private int kacinci;

    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder();
        buff.append("h:");
        buff.append(hostId);
        buff.append("\nt:");
        buff.append(targetId);
        buff.append("\nac:");
        buff.append(actionCode);
        buff.append("\ndata:");
        buff.append(dataArray);
        buff.append("\nd:");
        buff.append(messageDate);
        buff.append("\nF:");
        buff.append(isFinished());
        return buff.toString();
    }
    public Date getMessageDate() {
        return messageDate;
    }

    
    protected void setPackageHasError(){
        this.error=true;
    }
    public boolean isFinished() {
        return finished;
    }

    public Integer getActionCode() {
        return actionCode;
    }

    public void setActionCode(Integer actionCode) {
        this.actionCode = actionCode;
    }
  
    public NetworkPackage() {
        hostId=null;
        targetId=null;
        finished=false;
        dataArray = new ArrayList<>();
        buffer = new StringBuilder();
        NetworkPackage.createCount++;
        kacinci = NetworkPackage.createCount;
        System.out.println(kacinci + ". Network Package created");
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(kacinci + ". finalized");
        NetworkPackage.finalizeCount++;
        System.out.println("Total finalized : "+NetworkPackage.finalizeCount);
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
        
    }
    public void finishPacket(){
        this.messageDate =new Date();
        this.finished=true;
    }
    public void reset(){
    hostId=null;
        targetId=null;
        dataArray = new ArrayList<>();
        buffer.delete(0, buffer.length());
        finished=false;
    }
    public abstract void setMessage(String message,Integer id) throws Exception;
    public abstract String getMessage();
    
    
    public byte[] getMessageInByteFormat(){
        return getMessage()==null ? new byte[0]: getMessage().getBytes();
    }
    
    public boolean addByte(byte b) throws Exception{
        if(finished)
            return false;
        if(b<32 || b>127)
            return false;
        //baslangic
        if(buffer.length()==0 &&b!=BASLANGIC_AYIRACI)
            return false;
        buffer.append((char)b);
        if(b==BITIS_AYIRACI){
            finished=true;
            Integer id=null;
            if(this instanceof ClientPackage)
                id=getTargetId();
            if(this instanceof  ServerPackage)
                id=getHostId();
            this.setMessage(buffer.toString(), id);
            buffer.delete(0, buffer.length());
        }
        return true;
    }
    public Integer getHostId() {
        return hostId;
    }

    public void setHostId(Integer hostId) {
        this.hostId = hostId;
    }

    public Integer getTargetId() {
        return targetId;
    }

    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    public List<String> getDataArray() {
        return dataArray;
    }

    public void addData(String data) {
        this.dataArray.add(data);
    }

    public void setDataArray(List<String> dataArray) {
        this.dataArray.clear();
        this.dataArray.addAll(dataArray);
    }

}
