/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import static data.NetworkPackage.ARA_AYIRAC;
import static data.NetworkPackage.BASLANGIC_AYIRACI;
import static data.NetworkPackage.BITIS_AYIRACI;
import java.util.List;
import static data.NetworkPackage.MIN_ANA_AYIRAC_MIKTARI;

/**
 * server tarafindan okunan( client tarafindan gonderilen message)
 * <targetId:ac:d1;d2;...>
 *
 * @author ISIK
 */
public class ServerPackage extends NetworkPackage {

    @Deprecated
    @Override
    public void setTargetId(Integer targetId) {
        super.setTargetId(targetId); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public void setMessage(String serverMessage, Integer hostId) throws Exception {
        setHostId(hostId);
        int basIndex = serverMessage.indexOf(BASLANGIC_AYIRACI);
        int sonIndex = serverMessage.indexOf(BITIS_AYIRACI, basIndex);
        if (basIndex < 0 || sonIndex < 0) {
            setPackageHasError();
            throw new Exception("Data sorunlu baslangic veya bitis ayiraci yok  Message : [" + serverMessage + "]");

        }
        String rawData = serverMessage.substring(basIndex + 1, sonIndex);
        String[] anaData = rawData.split(ANA_AYIRAC + "");
        if (anaData.length < MIN_ANA_AYIRAC_MIKTARI) {
            setPackageHasError();
            throw new Exception("Ana Data miktari hatali : Message :" + anaData.length + "\\" + MIN_ANA_AYIRAC_MIKTARI + " Message : [" + serverMessage + "]");
        }
        //receiverlari bul ve ayarla
        String[] tmp = anaData[CLIENT_ID_ANA_AYIRAC_INDEXI].split(ARA_AYIRAC + "");
        //server
        if (tmp.length != CLIENT_ID_MIKTARI) {
            setPackageHasError();
            throw new Exception("Target client 1 den farkli olamaz : " + tmp.length + " Message : [" + serverMessage + "]");
        } else {
            Integer id;
            try {
                id = Integer.parseInt(tmp[0]);
                setTargetId(id);
            } catch (NumberFormatException ex) {
                setPackageHasError();
                throw new Exception("Receiver id sayi degil :" + tmp[0] + " Message : [" + serverMessage + "]");
            }

        }
        tmp = anaData[ACTION_CODE_ANA_AYIRAC_INDEX].split(ARA_AYIRAC + "");
        if (tmp.length != ACTION_CODE_MIKTARI) {
            setPackageHasError();
            throw new Exception("action code miktari uyumsuz " + tmp.length + "\\" + ACTION_CODE_MIKTARI + " Message : [" + serverMessage + "]");
        } else {
            try {
                Integer actionCode = Integer.parseInt(tmp[0]);
                setActionCode(actionCode);
            } catch (NumberFormatException ex) {
                setPackageHasError();
                throw new Exception("Action id sayi degil :" + tmp[0] + " Message : [" + serverMessage + "]");
            }
        }
        if (anaData.length > DATA_ANA_AYIRAC_INDEXI) {
            for (String s : anaData[DATA_ANA_AYIRAC_INDEXI].split(ARA_AYIRAC + "")) {
                this.addData(s);
            }
        }
        finishPacket();
    }

    @Override
    public String getMessage() {
        if (!isFinished()) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        builder.append(BASLANGIC_AYIRACI);
        builder.append(getTargetId());
        builder.append(ANA_AYIRAC);
        builder.append(getActionCode());
        builder.append(ANA_AYIRAC);
        List<String> arr = getDataArray();
        if (!arr.isEmpty()) {
            builder.append(arr.get(0));
            for (int i = 1; i < arr.size(); i++) {
                builder.append(ARA_AYIRAC);
                builder.append(arr.get(i));
            }
        }
        builder.append(BITIS_AYIRACI);
        return builder.toString();
    }

}
