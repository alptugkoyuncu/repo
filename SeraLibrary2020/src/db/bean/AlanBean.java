/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package db.bean;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author alptug
 */
public class AlanBean implements Serializable,idNamePair,Clonable {
    private Integer id;
    private BolgeBean bolge;
    private String name;
    private Integer idClient;
    private Integer actionCode;

    public AlanBean() {
        this.id = null;
        this.bolge=null;
        this.name= null;
        this.idClient=null;
        this.actionCode =null;
    }

    public AlanBean(Integer id, BolgeBean bolge, String name, Integer idClient, Integer actionCode) {
        this.id = id;
        this.bolge = bolge;
        this.name = name;
        this.idClient = idClient;
        this.actionCode = actionCode;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BolgeBean getBolge() {
        return bolge;
    }

    public void setBolge(BolgeBean bolge) {
        this.bolge = bolge;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    public Integer getActionCode() {
        return actionCode;
    }

    public void setActionCode(Integer actionCode) {
        this.actionCode = actionCode;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlanBean other = (AlanBean) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Alan{" + "id=" + id + ", bolge=" + bolge + ", name=" + name + ", idClient=" + idClient + ", actionCode=" + actionCode + '}';
    }

    @Override
    public AlanBean getClone() {
        return new AlanBean(id, bolge, name, idClient, actionCode);
    }
    
}
