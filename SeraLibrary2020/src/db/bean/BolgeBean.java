/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package db.bean;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author alptug
 */
public class BolgeBean implements Serializable,idNamePair{
    private Integer id;
    private String name;

    public BolgeBean() {
    }

    public BolgeBean(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BolgeBean other = (BolgeBean) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Bolge{" + "id=" + id + ", name=" + name + '}';
    }
    
}
