/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package db.bean;

/**
 *
 * @author alptug
 */
public interface Clonable<T> {
    public  T getClone();
}
