/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.bean;

//import org.json.JSONObject;

/**
 *
 * @author ISIK
 */
public class SulamaBean implements java.io.Serializable {

    private int id;
    private long baslama;
    private long bitis;
    private int none;
    private int low;
    private int normal;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getBaslama() {
        return baslama;
    }

    public void setBaslama(long baslama) {
        this.baslama = baslama;
    }

    public long getBitis() {
        return bitis;
    }

    public void setBitis(long bitis) {
        this.bitis = bitis;
    }

    public int getNone() {
        return none;
    }

    public void setNone(int none) {
        this.none = none;
    }

    public int getLow() {
        return low;
    }

    public void setLow(int low) {
        this.low = low;
    }

    public int getNormal() {
        return normal;
    }

    public void setNormal(int normal) {
        this.normal = normal;
    }

    public SulamaBean() {
    }

    public SulamaBean(int id, long baslama, long bitis, int none, int low, int normal) {
        this.id = id;
        this.baslama = baslama;
        this.bitis = bitis;
        this.none = none;
        this.low = low;
        this.normal = normal;
    }


//        @Override
//        public String toString() {
//            SimpleDateFormat f = new SimpleDateFormat("DD-MM-YY HH:mm:ss");
//            return "id : "+id+" ST:"+f.format(baslama)+" FT:"+f.format(bitis)+" Sure:"+sure+" Basincsiz:"+none+" AzBasincli:"+low+" Basincli:"+normal;
//        }
//        public JSONObject getJSONObject(){
//            JSONObject obj = new JSONObject();
//            obj.put("id", id);
//            obj.put("ST", baslama);
//            obj.put("FT", bitis);
//            obj.put("B",none);
//            obj.put("AB", low);
//            obj.put("TB", normal);
//            return obj;
//        }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SulamaBean other = (SulamaBean) obj;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return "SulamaBean{" + "id=" + id + ", baslama=" + baslama + ", bitis=" + bitis + ", none=" + none + ", low=" + low + ", normal=" + normal + '}';
    }
    
}
