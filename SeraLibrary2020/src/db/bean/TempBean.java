/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.bean;

import java.io.Serializable;

/**
 *
 * @author ISIK
 */
public class TempBean implements Serializable{
    private int id;
    private AlanBean alan;
    private String value;
    private long date;

    public TempBean() {
    }

    public TempBean(int id, AlanBean alan, String value, long date) {
        this.id = id;
        this.alan = alan;
        this.value = value;
        this.date = date;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AlanBean getAlan() {
        return alan;
    }

    public void setAlan(AlanBean alan) {
        this.alan = alan;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TempBean other = (TempBean) obj;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TempBean{");
        sb.append("id=").append(id);
        sb.append(", alan=").append(alan);
        sb.append(", value=").append(value);
        sb.append(", date=").append(date);
        sb.append('}');
        return sb.toString();
    }
    
}
