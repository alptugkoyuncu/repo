/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package log;

import java.util.EventObject;

/**
 *
 * @author ISIK
 */
public class LogEvent extends EventObject{
    public final static int INFO=1;
    public final static int DEBUG=2;
    public final static int ERROR=3;
    private final int type;
    private final String message;

    public int getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public LogEvent(Object source,int type, String message) {
        super(source);
        this.type = type;
        this.message = message;
    }
    
}
