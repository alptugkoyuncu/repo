/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package settings;

/**
 *
 * @author ISIK
 */
public class ServerDBSettings extends AbstractSeraSettings{
     final static String PROP_DRIVER = "db.driver";
    final static String PROP_PASSWORD = "db.password";
    final static String PROP_URL = "db.url";
    final static String PROP_USERNAME = "db.username";

    public ServerDBSettings(String folderName, String fileName) {
        super(folderName, fileName);
    }

    public String getDriver() {
        return this.values.get(PROP_DRIVER);
    }

    public void setDriver(String driver) {
        this.values.put(PROP_DRIVER, driver);
    }

    public String getPassword() {
        return this.values.get(PROP_PASSWORD);
    }

    public void setPassword(String password) {
        this.values.put(PROP_PASSWORD, password);
    }

    public String getUrl() {
        return this.values.get(PROP_URL);
    }

    public void setUrl(String url) {
        this.values.put(PROP_URL, url);
    }

    public String getUserName() {
        return this.values.get(PROP_USERNAME);
    }

    public void setUserName(String userName) {
        this.values.put(PROP_USERNAME, userName);
    }


    @Override
    public void setDefaultValues() {
 this.setDriver("com.mysql.jdbc.Driver");
        this.setUrl("jdbc\\:mysql\\://78.189.226.60\\:3306/sera_kontrol?useUnicode\\=true&characterEncoding=UTF-8&useLegacyDatetimeCode\\=false&serverTimezone\\=Turkey");
        this.setUserName("user1");
        this.setPassword("19791981");
    }

    @Override
    protected boolean isEmpty() {
        return !(this.values.containsKey(PROP_DRIVER) && this.values.containsKey(PROP_PASSWORD) && this.values.containsKey(PROP_URL) && this.values.containsKey(PROP_USERNAME));
    }

}
