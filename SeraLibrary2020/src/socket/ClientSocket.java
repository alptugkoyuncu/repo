package socket;

import data.ActionCode;
import data.ClientPackage;
import data.GeneralSystemConstant;
import data.NetworkPackage;
import data.ServerPackage;
import log.LogEvent;
import log.LogListener;
import socket.events.ClientDataEvent;
import socket.events.ClientSocketListener;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.EventObject;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class ClientSocket implements Runnable {

    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
     */
    Set<ClientSocketListener> clientSocketListeners;
    Set<LogListener> logListeners;
    Socket socket;
    DataInputStream in;
    PrintStream ps;
    private boolean connected;
    private boolean runAlways;
//    private long lastReceived, lastSent;
    private String ip;
    private Integer port;
    private Integer clientId;
    private final static int PING_WAIT_MS = 60000;
    public final static long INCOME_MAX_WAIT_SECOND = 180000;
    NetworkPackage clientPackage;
    NetworkPackage serverPackage;
    private Date lastSentPackage, lastReceivedPackage;

    public Integer getClientId() {
        return clientId;
    }

    public synchronized boolean isConnected() {
        return connected;
    }

    private synchronized void setConnected(boolean connected) {
        this.connected = connected;
    }

    public ClientSocket(String ip, Integer port, Integer clientId) {
        this.clientId = clientId;
        this.ip = ip;
        this.port = port;
        clientSocketListeners = new HashSet<>();
        this.logListeners = new HashSet<>();
        clientPackage = new ClientPackage();
    }

    public ClientSocket() {
        this(null, null, null);
    }

    public void setConnectionValues(String ip, int port, Integer clientId) {
        this.clientId = clientId;
        this.ip = ip;
        this.port = port;
    }

    public void setConnectionID(Integer clientId) {
        this.clientId = clientId;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public String getIp() {
        return ip;
    }

    public void start() throws Exception {
        this.runAlways = false;
        this.setConnected(false);
        if (ip == null || port == null || clientId == null) {
            fireLogOccuredEvent(LogEvent.ERROR, "Connection argumensts Hatali! ip:" + ip + " port:" + port + "Connection Type: " + clientId);
            return;
        }
        new Thread(this).start();
    }

    public void terminate() {
        this.runAlways = false;
    }

    @Override
    public void run() {
        this.setConnected(true);
        serverPackage = new ServerPackage();
        clientPackage = new ClientPackage();
        try {
            socket = new Socket(ip, port);
            socket.setSoTimeout(100);
            socket.setKeepAlive(true);
            socket.setTcpNoDelay(true);
            fireLogOccuredEvent(LogEvent.INFO, "Socket cration completed at" + ip + "\\" + port);
            fireClientConnectedEvent();
            runAlways = true;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            runAlways = false;
            fireLogOccuredEvent(LogEvent.ERROR, "Socket creation failed ip = " + ip + "\\" + port + "\n" + e.getMessage());
            fireClientDisconnectedEvent();
//            this.fireConnectionFailedEvent(evt);
        }

        if (runAlways) {
            try {
                in = new DataInputStream(socket.getInputStream());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                fireLogOccuredEvent(LogEvent.ERROR, "Data Input Stream olusturulamadi\n" + e.getMessage());
                runAlways = false;
            }
        }
        if (runAlways) {
            try {
                ps = new PrintStream(socket.getOutputStream());
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                fireLogOccuredEvent(LogEvent.ERROR, "Print Stream olusturulamadi\n" + e1.getMessage());
                runAlways = false;
                // return false;
            }
        }

        //client id kayit isiyle ugrasiliyor
       lastReceivedPackage = new Date();
        boolean waitId = true;
        while (runAlways && waitId) {
            Date now = new Date();
            if (now.getTime()-lastReceivedPackage.getTime() > GeneralSystemConstant.MAX_CLIENT_ID_COMFIRMATION_TIME) {
                runAlways = false;
                fireLogOccuredEvent(LogEvent.ERROR, "Id bekleme de zaman asimi");
                break;
            }
            this.receiveNetworkClientPackage();
            if (clientPackage.isFinished()) {
                int ac = clientPackage.getActionCode();
                switch (ac) {
                    case ActionCode.AC_ID_REQUESTED: {
//                        serverPackage.reset();
                        serverPackage = new ServerPackage();
                        serverPackage.setTargetId(GeneralSystemConstant.SERVER_ID);
                        serverPackage.setHostId(this.getClientId());
                        serverPackage.setActionCode(ActionCode.AC_SEND_ID);
                        serverPackage.addData(this.getClientId() + "");
                        serverPackage.finishPacket();
                        this.sentNetworkServerPackage(serverPackage);
                        break;
                    }
                    case ActionCode.AC_ID_ACCEPTED: {
                        fireClientIdAcceptedEvent();
                        waitId = false;
                        break;
                    }
                    case ActionCode.AC_ID_REFUSED: {
                        fireClientIdRefusedEvent();
                        runAlways = false;
                        waitId = false;
                        break;

                    }
                    default:
                        fireLogOccuredEvent(LogEvent.DEBUG, "id onaylama isleminde hatali bir action code geldi : " + ac);
                        break;
                }
//                clientPackage.reset();
                clientPackage = new ClientPackage();
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException ex) {
//                Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        lastReceivedPackage = new Date();
        lastSentPackage = new Date();
        boolean connectionTerminatedByServer = false;
        while (this.runAlways) {
            {
                long curr = new Date().getTime();
                if (curr - lastSentPackage.getTime() > PING_WAIT_MS) {
//                    serverPackage.reset();
                    serverPackage = new ServerPackage();
                    serverPackage.setTargetId(GeneralSystemConstant.SERVER_ID);
                    serverPackage.setHostId(this.getClientId());
                    serverPackage.setActionCode(ActionCode.AC_SEND_PING);
                    serverPackage.finishPacket();
                    this.sentNetworkServerPackage(serverPackage);
                }
                if (curr - lastReceivedPackage.getTime() >INCOME_MAX_WAIT_SECOND) {
                    fireLogOccuredEvent(LogEvent.INFO, "Ping bekleme zaman asimina ugradi Client thread kapatiliyor");
                    this.terminate();
                    break;
                }
            }
            this.receiveNetworkClientPackage();
            if (clientPackage.isFinished()) {
                switch (clientPackage.getActionCode()) {
                    case ActionCode.AC_CLIENT_DISCONNECTED: {
                        connectionTerminatedByServer = true;
                        this.terminate();
                        break;
                    }

                    default:
                        break;
                }
//                clientPackage.reset();
                clientPackage = new ClientPackage();
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
//        serverPackage.reset();
        if (!connectionTerminatedByServer) {
            serverPackage = new ServerPackage();
            serverPackage.setHostId(this.getClientId());
            serverPackage.setTargetId(GeneralSystemConstant.SERVER_ID);
            serverPackage.setActionCode(ActionCode.AC_CLIENT_DISCONNECTED);
            serverPackage.addData(this.getClientId() + "");
            serverPackage.finishPacket();
            this.sentNetworkServerPackage(serverPackage);
        }
        try {
            if (in != null) {
                in.close();
            }
        } catch (IOException ex) {
            fireLogOccuredEvent(LogEvent.ERROR, "Input Stream kapatma hatasi : " + ex.getMessage());
        }
        if (ps != null) {
            ps.close();
        }
        try {
            if (socket != null) {
                socket.close();
            }
        } catch (IOException ex) {
            fireLogOccuredEvent(LogEvent.ERROR, "Socket kapatma hatasi : " + ex.getMessage());

        }
        fireClientDisconnectedEvent();
        this.setConnected(false);
    }

    public synchronized void sentNetworkServerPackage(NetworkPackage pck) {
        if (!this.isConnected()) {
            fireLogOccuredEvent(LogEvent.ERROR, "Packet gonderme hatasi : Baglanti kapali");
            this.terminate();
            return;
        }
        if (this.socket == null) {
            fireLogOccuredEvent(LogEvent.ERROR, "Packget gonderme hatasi :Socket null");
            this.terminate();
            return;
        }
        if (this.socket.isClosed()) {
            fireLogOccuredEvent(LogEvent.ERROR, "Packget gonderme hatasi :Socket kapali");
            this.terminate();
            return;
        }
        if (ps == null) {
            fireLogOccuredEvent(LogEvent.ERROR, "Packget gonderme hatasi : printStream kapali");
            this.terminate();
            return;
        }
        if (pck == null) {
            fireLogOccuredEvent(LogEvent.ERROR, "Packget gonderme hatasi : paket null");
            return;
        } else if (!pck.isFinished()) {
            fireLogOccuredEvent(LogEvent.ERROR, "Packet tamamlanmamis Packet : [" + pck.getMessage() + "]");
            return;
        }

//        ps.print(pck.getMessage());
        byte[] b = pck.getMessageInByteFormat();
        if (b.length == 0) {
            fireLogOccuredEvent(LogEvent.INFO, "packet bos");
            return;
        }
        ps.write(b, 0, b.length);
//        L.debug("Data sent =>" + pck);
        ps.flush();
        this.lastSentPackage = new Date();
        this.fireDataSentEvent(pck);
    }

    public boolean receiveNetworkClientPackage() {
        if (!this.isConnected()) {
            fireLogOccuredEvent(LogEvent.ERROR, "Packet alma hatasi : Connection kapali");
            this.terminate();
            return false;
        }
        if (socket == null) {
            fireLogOccuredEvent(LogEvent.ERROR, "Packet alma hatasi : socket null");
            this.terminate();
            return false;
        }
        if (socket.isClosed()) {
            fireLogOccuredEvent(LogEvent.ERROR, "Packet alma hatasi : socket kapali");
            this.terminate();
            return false;
        }
        if (in == null) {
            fireLogOccuredEvent(LogEvent.ERROR, "Packet alma hatasi : Input stream null Client thread sonlandiriliyor");
            this.terminate();
            return false;
        }
        try {

            if (in.available() > 0) {
                long blockms = 100;
                long start = System.currentTimeMillis();
                while (System.currentTimeMillis() - start < blockms) {
                    byte b = in.readByte();
                    clientPackage.addByte(b);
                    if (clientPackage.isFinished()) {
                        lastReceivedPackage = new Date();
//                        clientPackage.bufferToMessage(this.getClientId());
                        clientPackage.setTargetId(clientId);
                        fireDataReceivedEvent(clientPackage);
                        return true;
                    }
                }
            }
        }catch(SocketTimeoutException ex){
        } catch (IOException ex) {
            fireLogOccuredEvent(LogEvent.ERROR, "Input stream de hata var: " + ex.getMessage());
        } catch (Exception ex) {
            fireLogOccuredEvent(LogEvent.ERROR, "Received pakcet de sorun var : " + ex.getMessage());
        }
        return false;
    }

    private void fireClientIdAcceptedEvent() {
        for (ClientSocketListener l : clientSocketListeners) {
            l.clientIdAccepted(new EventObject(this));
        }
    }

    private void fireClientIdRefusedEvent() {
        for (ClientSocketListener l : clientSocketListeners) {
            l.clientIdRefused(new EventObject(this));
        }
    }

    private void fireLogOccuredEvent(int type, String message) {
        for (LogListener l : logListeners) {
            l.logOccured(new LogEvent(this, type, message));
        }
    }

    private void fireDataReceivedEvent(NetworkPackage pck) {
        for (ClientSocketListener l : clientSocketListeners) {
            l.dataReceived(new ClientDataEvent(pck));
        }
    }

    private void fireDataSentEvent(NetworkPackage pck) {
        for (ClientSocketListener l : clientSocketListeners) {
            l.dataSent(new ClientDataEvent(pck));
        }
    }

    private void fireClientConnectedEvent() {
        for (ClientSocketListener l : clientSocketListeners) {
            l.clientConnected(new EventObject(this));
        }
    }

    private void fireClientDisconnectedEvent() {
        for (ClientSocketListener l : clientSocketListeners) {
            l.clientDisconnected(new EventObject(this));
        }
    }

    public void addClientSocketListener(ClientSocketListener l) {
        this.clientSocketListeners.add(l);
    }

    public void removeClientSocketListener(ClientSocketListener l) {
        this.clientSocketListeners.remove(l);
    }

    public void addLogListener(LogListener l) {
        this.logListeners.add(l);
    }

    public void removeLogListener(LogListener l) {
        this.logListeners.remove(l);
    }
}
