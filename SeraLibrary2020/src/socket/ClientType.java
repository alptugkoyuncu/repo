/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket;

/**
 *
 * @author ISIK
 */
public enum ClientType {
    SERVER,
    TEMP_CONTROL,
    HAVUZ_KONTROL,
    WATCHER;
    public static ClientType getClientType(int clientId){
        if(clientId==1000)
            return SERVER;
        if(clientId<1100)
            return TEMP_CONTROL;
        if(clientId<1200)
            return HAVUZ_KONTROL;
        if(clientId<3000)
            return WATCHER;
        return null;
    }
}
