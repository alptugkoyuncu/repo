
import data.ClientPackage;
import data.NetworkPackage;
import data.ServerPackage;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ISIK
 */
public class TestNetworkPackage {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        NetworkPackage pck = new ClientPackage();
        String msg = "<1000:12:98.65>";

        System.out.println("Original message :" + msg);

        try {
            byte[] b = msg.getBytes();
            for (byte b1 : b) {
                System.out.print("try to add : " + (char) b1);
                System.out.println(" -> " + pck.addByte(b1));
            }
            pck.setTargetId(1001);
//            pck.setMessage(msg, 1001);
            if (pck.isFinished()) {
                System.out.println("Message:" + pck.getMessage());
                System.out.println(
                        "hOST ID  :" + pck.getHostId()
                        + " targetId : " + pck.getTargetId()
                        + " Action code : " + pck.getActionCode());

            }
        } catch (Exception ex) {
            Logger.getLogger(TestNetworkPackage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
