/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author ISIK
 */
public enum ActionCode {

    AC_INVALID_CODE(0),
    AC_CLIENT_CONNECTED(1),
    AC_CLIENT_DISCONNECTED(2),
    AC_FRIEND_CONNECTED(3),
    AC_FRIEND_DISCONNECTED(4),
    AC_GET_ALL_CONNECTED_CLIENTS_ID(5),
    AC_SERVER_IS_ALIVE(6),
    AC_RESET_ARDUINO(9),
    //temp kontrol data info

    AC_SEND_TEMP0(10),
    AC_SEND_TEMP1(11),
    AC_SEND_TEMP2(12),
    AC_SEND_TEMP3(13),
    //havuz kontrol data info
    AC_SEND_HAVUZ_ESP_ALL_DATA(20),
    AC_SEND_RELAY_STATE(21),
    AC_SEND_OUTER_TIMER_STATE(22),
    AC_SEND_ARDUINO_RELAY_CONTROL_STATE(23),
    // actions for havuzkontrol

    AC_DISABLE_OUTER_TIMER(30),
    AC_ENABLE_OUTER_TIMER(31),
    AC_SET_ROLE_AKTIF(32),
    AC_SET_ROLE_INAKTIF(33), // todo first disable outer timer if outer timer enable you can not disable if outer timer starts the relay
    AC_SEND_PRESSURE_SENSOR_VALUE(34),
    AC_REQUEST_FOCUED_PROVIDERS(60),
    AC_SEND_BOARD_LAST_RESET_TIME(61),
    AC_BOARD_FREE_MEMORY(62),
    AC_BOARD_SENSOR_ACTION_CODES(63),
    //    public final static int AC_REQUEST_PING = 99;
    AC_SEND_PING(100),
    AC_SEND_WIFI_SSID(101),
    AC_SEND_PASS(102),
    AC_SEND_IP(103),
    AC_SEND_PORT(104),
    AC_SEND_ID(105),
    AC_ESP_STARTED(106),
    AC_RESET_ESP(107),
    AC_WIFI_CONNECTED(108),
    AC_WIFI_DISCONNECTED(109),
    AC_WIFI_CONNECTION_FAILED(110),
    AC_SERVER_CONNECTED(111),
    AC_SERVER_DISCONNECTED(112),
    AC_SERVER_CONNECTION_FAILED(113),
    AC_ID_REQUESTED(114),
    AC_ID_ACCEPTED(115),
    AC_ID_REFUSED(116),
    AC_DB_GET_HAVUZ_DATA(190),
    AC_ESP_DEBUG(200),
    AC_SET_WIFI_SSID(201),
    AC_SET_WIFI_PASS(202),
    AC_SET_SERVER_IP(203),
    AC_SET_SERVER_PORT(204),
    AC_SET_CLIENT_ID(205),
    AC_RESET_NETWORK_PARAMS_RECORD(206),
    AC_SAVE_NETWORK_PARAMS_RECORD(207),
    AC_LOAD_NETWORK_PARAMS_RECORD(208),
    AC_SET_PRESSURE_CALIBRATION_VALUES(219),
    AC_SAVE_PRESSURE_CALIBRATION_VALUES(220),
    AC_LOAD_PRESSURE_CALIBRATON_VALUES(221),
    AC_RESET_PRESSURE_CALIBRATION_VALUES(222);

    public static boolean isSensorActionCode(int actionCode) {
        return actionCode >= 10 && actionCode <= 20;
    }

    private final int value;

    private ActionCode(int val) {
        this.value = val;
    }

    public int getValue() {
        return value;
    }

    public static ActionCode getByValue(int value) {
        for (ActionCode ac : ActionCode.values()) {
            if (ac.value == value) {
                return ac;
            }
        }
        return null;
    }

}
