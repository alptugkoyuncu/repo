/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *Network package formatted with json object
 * @author ISIK
 */
public class NetworkPackage implements Serializable{

    private Integer hostId;
    private Integer targetId;
    private int actionCode;
    private List<String> dataArray;
    private Date packageDate;

    public Date getPackageDate() {
        return packageDate;
    }

    public void setPackageDate(Date packageDate) {
        this.packageDate = packageDate;
    }
    public NetworkPackage(){
        this.dataArray = new ArrayList<>();
        packageDate = new Date();
    }
    public Integer getHostId() {
        return hostId;
    }

    public void setHostId(Integer hostId) {
        this.hostId = hostId;
    }

    public Integer getTargetId() {
        return targetId;
    }

    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    public int getActionCode() {
        return actionCode;
    }
   
    public void setActionCode(int actionCode) {
        this.actionCode = actionCode;
    }

    public List<String> getDataArray() {
        return dataArray;
    }

    public void setDataArray(List<String> dataArray) {
        this.dataArray = dataArray;
    }
}
