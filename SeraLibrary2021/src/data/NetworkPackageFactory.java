/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ISIK
 */
public class NetworkPackageFactory {

    public static NetworkPackage clone(NetworkPackage pck) throws Exception {
        NetworkPackage temp = new NetworkPackage();
        if(pck==null)
            throw new Exception("Invalid Package");
        temp.setHostId(pck.getHostId());
        temp.setTargetId(pck.getTargetId());
        temp.setActionCode(pck.getActionCode());
        temp.getDataArray().addAll(pck.getDataArray());
        return temp;
    }

    
    private NetworkPackageFactory(){
        
    }
    public static NetworkPackage create(Integer hostId, Integer targetId, ActionCode actionCode, List<String> dataArray) throws Exception {
        NetworkPackage temp = new NetworkPackage();
        if(actionCode==null)
            throw new Exception("Invalid Package");
        temp.setHostId(hostId);
        temp.setTargetId(targetId);
        temp.setActionCode(actionCode.getValue());
        temp.setDataArray(dataArray);
        return temp;
    }
     public static NetworkPackage create(JSONObject obj) throws Exception{
        if(obj==null)
            throw new Exception("Invalid Package");
        NetworkPackage temp = new NetworkPackage();
        temp.setHostId(obj.has("hostId") ? obj.getInt("hostId") : null);
        temp.setTargetId(obj.has("targetId") ? obj.getInt("targetId") : null);
        temp.setActionCode(obj.has("actionCode") ? (obj.getInt("actionCode")) : null);
        List<String> dataArray = new ArrayList<String>();
        JSONArray arr = obj.has("dataArray") ? obj.getJSONArray("dataArray") : null;
        if(arr!=null){
            for(int i=0; i<arr.length();i++)
                dataArray.add(arr.getString(i));
        }
        temp.setDataArray(dataArray);
        return temp;
    }
}
