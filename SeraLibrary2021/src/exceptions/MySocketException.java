/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author ISIK
 */
public class MySocketException extends Exception{
    int errType;
    public final static int ERR_IP_HATASI=0;
    public final static int ERR_PORT_HATASI=1;
    public final static int ERR_CONNECTION_ID_HATASI=2;
    public MySocketException(int errType) {
        this.errType = errType;
    }

    public MySocketException(int errType, String message) {
        super(message);
        this.errType = errType;
    }

    public MySocketException(int errType, String message, Throwable cause) {
        super(message, cause);
        this.errType = errType;
    }

    public MySocketException(int errType, Throwable cause) {
        super(cause);
        this.errType = errType;
    }
    public int getErrType() {
        return errType;
    }
}
