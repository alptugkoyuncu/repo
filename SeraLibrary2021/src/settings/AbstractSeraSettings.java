/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ISIK
 */
public abstract class AbstractSeraSettings {

    protected static String PropertiesFile;
    protected String mainFilePath;
    protected static boolean pathCreated = false;
    private String propPath;
    protected Map<String, String> values;

    private final String folderName;
    private final String fileName;
    public AbstractSeraSettings(String folderName,String fileName) {
        this.folderName=folderName;
        this.fileName=fileName;
        createPropertiesFile();
        values = new HashMap<String, String>();
        this.load();
        if(isEmpty()){
            this.resetToDefault();
            this.save();
        }
    }
    protected abstract boolean isEmpty();
    protected void createPropertiesFile() {
        if (propPath!=null) {
            return;
        }
        String settingFolder = System.getProperty("user.home") + File.separator + folderName + File.separator + "settings";
        File f = new File(settingFolder);
        if (f.exists() || f.mkdirs()) {
            propPath = settingFolder + File.separator + fileName;
            f = new File(propPath);
            try {
                if (f.exists() || f.createNewFile()) {
                } else {
                    propPath = null;
                }

            } catch (IOException ex) {
                propPath = null;
                Logger.getLogger(AbstractSeraSettings.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    protected Properties getPropertiesFile() {
        createPropertiesFile();
        if (propPath==null) {
            return null;
        }
        Properties props = new Properties();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(propPath);
            props.load(fis);
            fis.close();
            return props;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AbstractSeraSettings.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AbstractSeraSettings.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean save() {
        Properties prop = getPropertiesFile();

        if (prop == null) {
            return false;
        }
        try (FileOutputStream out = new FileOutputStream(propPath)) {
            prop.putAll(this.values);
            prop.store(out, null);
            load();
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AbstractSeraSettings.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (IOException ex) {
            Logger.getLogger(AbstractSeraSettings.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }

    public boolean load() {
        Properties prop = getPropertiesFile();
        if (prop == null) {
            setDefaultValues();
            return false;
        }
        try {
            Iterator<Object> it = prop.keySet().iterator();
            while (it.hasNext()) {
                String key= (String)it.next();
                String value = prop.getProperty(key);
                this.values.put(key,value);
            }
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    
    public abstract void setDefaultValues();
    /**
     * default degerleri yukler dosyaya kaydetmez kayit icin save fonksiyonu cagirilmalidir
     */
    public void resetToDefault(){
        this.setDefaultValues();
    }


    @Override
    public String toString() {
        return this.values.toString();
    }

}
