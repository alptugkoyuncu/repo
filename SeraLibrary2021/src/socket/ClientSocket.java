package socket;

import data.ActionCode;
import data.GeneralSystemConstant;
import data.NetworkPackage;
import data.NetworkPackageFactory;
import exceptions.MySocketException;
import java.io.BufferedReader;
import log.LogEvent;
import log.LogListener;
import socket.events.ClientDataEvent;
import socket.events.ClientSocketListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventObject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class ClientSocket {

    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
     */
    Set<ClientSocketListener> clientSocketListeners;
    Set<LogListener> logListeners;
    Socket socket;
    private AtomicBoolean running = new AtomicBoolean(false);
//    private long lastReceived, lastSent;
    private String ip;
    private Integer port;
    private Integer clientId;
    private final static int PING_WAIT_MS = 60000;
    public final static long INCOME_MAX_WAIT_SECOND = 180000;
    ReaderThread readerThread;
    WriterThread writerThread;
    private boolean connectionTerminatedByServer;

    public Integer getClientId() {
        return clientId;
    }

    public ClientSocket(String ip, Integer port, Integer clientId) {
        this.clientId = clientId;
        this.ip = ip;
        this.port = port;
        clientSocketListeners = new HashSet<>();
        this.logListeners = new HashSet<>();
        connectionTerminatedByServer = false;
    }

    public boolean isAlive() {
        return !this.socket.isClosed() && this.socket.isConnected();
    }

    public ClientSocket() {
        this(null, null, null);
    }

    public void setConnectionValues(String ip, int port, Integer clientId) {
        this.clientId = clientId;
        this.ip = ip;
        this.port = port;
    }

    public void setConnectionID(Integer clientId) {
        this.clientId = clientId;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public String getIp() {
        return ip;
    }

    public void start() throws Exception {
        running.set(true);
        if (ip == null || port == null || clientId == null) {
            String msg = "Connection argumensts Hatali! ip:" + ip + " port:" + port + "Connection Type: " + clientId;
//                fireLogOccuredEvent(LogEvent.ERROR, msg);
            running.set(false);
            throw new MySocketException((ip == null ? MySocketException.ERR_IP_HATASI : (port == null ? MySocketException.ERR_PORT_HATASI : MySocketException.ERR_CONNECTION_ID_HATASI)), msg);
        }
        try {
            socket = new Socket(ip, port);
            socket.setSoTimeout(60000);
            socket.setKeepAlive(true);
            socket.setTcpNoDelay(true);
//            fireLogOccuredEvent(LogEvent.INFO, "Socket cration completed at" + ip + "\\" + port);
            fireClientConnectedEvent();
            running.set(true);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            running.set(false);

//            fireLogOccuredEvent(LogEvent.ERROR, "Socket creation failed ip = " + ip + "\\" + port + "\n" + e.getMessage());
//            fireClientDisconnectedEvent();
            this.fireClientConnectionFailedEvent();
        }
        if (running.get()) {
            readerThread = new ReaderThread(socket.getInputStream());
            readerThread.start();
            writerThread = new WriterThread(socket.getOutputStream());
            writerThread.start();
        }
    }

    public void terminate() throws InterruptedException, IOException, Exception {
        if (!connectionTerminatedByServer) {
            List<String> dataArr = new ArrayList<>();
            dataArr.add(getClientId() + "");
            NetworkPackage pck = NetworkPackageFactory.create(getClientId(), GeneralSystemConstant.SERVER_ID, ActionCode.AC_CLIENT_DISCONNECTED, dataArr);
            writerThread.asyncSent(pck);
        }
        running.set(false);
        readerThread.interrupt();
        readerThread.join(3000);
        readerThread = null;
        writerThread.interrupt();
        writerThread.join(3000);
        writerThread = null;
        socket.close();
        fireLogOccuredEvent(LogEvent.INFO, "Client disconnected");
        fireClientDisconnectedEvent();
    }

    private void fireClientIdAcceptedEvent() {
        for (ClientSocketListener l : clientSocketListeners) {
            l.clientIdAccepted(new EventObject(this));
        }
    }

    private void fireClientIdRefusedEvent() {
        for (ClientSocketListener l : clientSocketListeners) {
            l.clientIdRefused(new EventObject(this));
        }
    }

    private void fireLogOccuredEvent(int type, String message) {
        for (LogListener l : logListeners) {
            l.logOccured(new LogEvent(this, type, message));
        }
    }

    private void fireDataReceivedEvent(NetworkPackage pck) {
        for (ClientSocketListener l : clientSocketListeners) {
            l.dataReceived(new ClientDataEvent(pck));
        }
    }

    private void fireDataSentEvent(NetworkPackage pck) {
        for (ClientSocketListener l : clientSocketListeners) {
            l.dataSent(new ClientDataEvent(pck));
        }
    }

    private void fireClientConnectedEvent() {
        for (ClientSocketListener l : clientSocketListeners) {
            l.clientConnected(new EventObject(this));
        }
    }

    private void fireClientDisconnectedEvent() {
        for (ClientSocketListener l : clientSocketListeners) {
            l.clientDisconnected(new EventObject(this));
        }
    }

    private void fireClientConnectionFailedEvent() {
        for (ClientSocketListener l : clientSocketListeners) {
            l.clientConnectionFailed(new EventObject(this));
        }
    }

    public void addClientSocketListener(ClientSocketListener l) {
        this.clientSocketListeners.add(l);
    }

    public void removeClientSocketListener(ClientSocketListener l) {
        this.clientSocketListeners.remove(l);
    }

    public void addLogListener(LogListener l) {
        this.logListeners.add(l);
    }

    public void removeLogListener(LogListener l) {
        this.logListeners.remove(l);
    }

    class ReaderThread extends Thread {

        BufferedReader reader;
        Date lastRecv;

        private ReaderThread(InputStream inputStream) {
            reader = new BufferedReader(new InputStreamReader(inputStream));
            lastRecv = null;
        }

        public void run() {
            boolean waitId = true;
            lastRecv = new Date();
            connectionTerminatedByServer = false;
            while (running.get() && waitId) {
                try {
                    Date now = new Date();
                    if (now.getTime() - lastRecv.getTime() > GeneralSystemConstant.MAX_CLIENT_ID_COMFIRMATION_TIME) {
                        terminate();
                        fireLogOccuredEvent(LogEvent.ERROR, "Id bekleme de zaman asimi");
                        break;
                    }
                    String line = reader.readLine();
                    JSONObject jsonObj = new JSONObject(line);
                    NetworkPackage pck = NetworkPackageFactory.create(jsonObj);
                    lastRecv = new Date();
                    fireDataReceivedEvent(pck);
                    switch (ActionCode.getByValue(pck.getActionCode())) {
                        case AC_ID_REQUESTED: {
//                        serverPackage.reset();
                            List<String> dataArray = new ArrayList<String>();
                            dataArray.add(getClientId() + "");
                            NetworkPackage np = NetworkPackageFactory.create(GeneralSystemConstant.SERVER_ID, getClientId(), ActionCode.AC_SEND_ID, dataArray);
                            writerThread.asyncSent(np);
                            break;
                        }
                        case AC_ID_ACCEPTED: {
                            fireClientIdAcceptedEvent();
                            waitId = false;
                            break;
                        }
                        case AC_ID_REFUSED: {
                            fireClientIdRefusedEvent();
                            terminate();
                            break;

                        }
                        default:
                            fireLogOccuredEvent(LogEvent.DEBUG, "id onaylama isleminde hatali bir action code geldi : " + pck.getActionCode());
                            break;
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
//                Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            while (running.get()) {
                try {
                    Thread.sleep(10);
                    long curr = new Date().getTime();
                    if (curr - lastRecv.getTime() > INCOME_MAX_WAIT_SECOND) {
                        fireLogOccuredEvent(LogEvent.INFO, "Ping bekleme zaman asimina ugradi Client thread kapatiliyor");
                        terminate();
                        break;
                    }
                    String line = reader.readLine();
                    NetworkPackage pck = NetworkPackageFactory.create(new JSONObject(line));
                    switch (ActionCode.getByValue(pck.getActionCode())) {
                        case AC_CLIENT_DISCONNECTED: {
                            connectionTerminatedByServer = true;
                            terminate();
                            break;
                        }
                        default:
                            break;
                    }

                } catch (InterruptedException ex) {
                    Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }

    }

    class WriterThread extends Thread {

        private NetworkPackage pck;
        private final PrintWriter writer;
        private final AtomicBoolean lock = new AtomicBoolean();
        Date lastSent;

        private WriterThread(OutputStream outputStream) {
            writer = new PrintWriter(outputStream);
            pck = null;
            lastSent = null;
            lock.set(false);
        }

        @Override
        public void run() {
            lastSent = new Date();
            while (running.get()) {
                while (lock.get()) {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException ex) {
//                        Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                lock.set(true);
                
                if (pck != null) {
                    sent();
                }else{
                    if (new Date().getTime() - lastSent.getTime() > PING_WAIT_MS) {
                    try {
                        this.pck = NetworkPackageFactory.create(getClientId(), GeneralSystemConstant.SERVER_ID, ActionCode.AC_SEND_PING, new ArrayList<>());
                        this.sent();
                    } catch (Exception ex) {
                        this.pck = null;
                        Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                }

                lock.set(false);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
//                    Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            lock.set(false);
        }

        public void asyncSent(NetworkPackage pck) {
            while (lock.get()) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            lock.set(true);
            this.pck = pck;
            lock.set(false);
            this.interrupt();
        }

        private void sent() {
            writer.println(new JSONObject(pck));
            writer.flush();
            lastSent = new Date();
            fireDataSentEvent(pck);
            pck = null;
        }
    }
}
