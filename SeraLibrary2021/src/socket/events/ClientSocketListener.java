/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket.events;

import java.util.EventObject;

/**
 *
 * @author ISIK
 */
public interface ClientSocketListener {
    public void clientConnected(EventObject obj);
    public void clientConnectionFailed(EventObject obj);
    public void clientDisconnected(EventObject obj);
    public void dataReceived(ClientDataEvent evt);
    public void dataSent(ClientDataEvent evt);
    public void clientIdAccepted(EventObject obj);
    public void clientIdRefused(EventObject obj);
}
