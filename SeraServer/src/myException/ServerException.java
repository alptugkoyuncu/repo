/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myException;

/**
 *
 * @author ISIK
 */
public class ServerException extends Exception{
     public enum ErrorCode{
         NEGATIF_PORT
     }
     private final ErrorCode errorCode;
     public ServerException(String message,ErrorCode code){
         super(message);
         this.errorCode = code;
     }
     ErrorCode getErrorCode(){
         return errorCode;
     }
}
