/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import myException.ServerException;
import seraserver.socket.listeners.ServerListener;

/**
 *
 * @author ISIK
 */
public class SeraServer {

    

    private static Integer port = null;
    private final static int POOL_SIZE = 500;
    private final static Set<ClientHandler> clientSet = new HashSet<>();
    private final static Object lock1 = new Object();
    private final static Set<ServerListener> listeners = new HashSet<>();
    private static boolean RUN_SERVER =false;
    private static String getIp() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private static void fireServerStarted(){
        String ip = SeraServer.getIp();
        for(ServerListener l : listeners)
            l.serverStarted(ip, port);
    }
    private static void fireServerTerminated(){
        for(ServerListener l : listeners)
            l.serverTerminated();
    }
    public static void start(int port) throws ServerException, IOException {
        System.out.println("The chat server is running...");
        listeners.clear();
        clientSet.clear();
        RUN_SERVER = true;
        var pool = Executors.newFixedThreadPool(POOL_SIZE);
        try (var listener = new ServerSocket(port)) {
            fireServerStarted();
            while (RUN_SERVER) {
                synchronized (lock1) {
                    ClientHandler ch = new ClientHandler(listener.accept());
                    pool.execute(ch);
                    clientSet.add(ch);
                }
            }
        }
        synchronized(lock1){
            for (Iterator<ClientHandler> it = clientSet.iterator(); it.hasNext();) {
                it.next().terminate();
            }
        }
        fireServerTerminated();
    }
    public static void addListener(ServerListener sl){
        listeners.add(sl);
    }
    public static void removeListener(ServerListener sl){
        listeners.remove(sl);
    }
    public static synchronized void broadcast(String msg, ClientHandler ch) {
        synchronized (lock1) {
            Set<ClientHandler> receivers = getReceiverList(ch);
            for (Iterator<ClientHandler> it = receivers.iterator(); it.hasNext();) {
                it.next().sent(msg);
            }
        }
    }
    public static synchronized void terminate(){
        RUN_SERVER=false;
    }
    private static Set<ClientHandler> getReceiverList(ClientHandler ch) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
