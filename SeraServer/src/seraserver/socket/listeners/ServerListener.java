/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver.socket.listeners;

import seraserver.socket.ClientHandler;

/**
 *
 * @author ISIK
 */
public interface ServerListener {
    public void serverStarted(String ip,int port);
    public void serverTerminated();
    public void clientConnected();
    public void clientIdAccepted(int clientId);
    public void clientDisconnected(int clientID);
}
