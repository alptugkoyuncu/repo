
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import sera_server.SeraServer;
import sera_server.listeners.DataTransferEvent;
import sera_server.listeners.DataTransferListener;
import sera_server.listeners.ServerPrintEvent;
import sera_server.listeners.ServerStatuChangeEvent;
import sera_server.listeners.ServerStatuChangeListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author WIN7
 */
public class ServerMainFrame extends javax.swing.JFrame {

    private boolean connected = false;
    private final static String STR_CONNECT = "Bağlan";
    private final static String STR_DISCONEECT = "Bağlantıyı Kes";
    SeraServer server;
    private int portN0 = 5555;
    final ServerEventListener l = new ServerEventListener();
    DefaultListModel listModel = null;
    TrayIcon trayIcon;
    final SystemTray tray = SystemTray.getSystemTray();
    final PopupMenu popup = new PopupMenu();
    MenuItem frameItem, connectionItem, cikisItem;

    private void createTrayIcon() {
        trayIcon = new TrayIcon(createImage("images/bulb.gif", "tray icon"), "0 makina bağlı");

        trayIcon.setImageAutoSize(true);
        // Create a popup menu components
        frameItem = new MenuItem("Göster");
        connectionItem = new MenuItem();
        cikisItem = new MenuItem("Çıkış");

        //Add components to popup menu
        popup.add(frameItem);
        popup.add(connectionItem);
        popup.add(cikisItem);

        trayIcon.setPopupMenu(popup);

        trayIcon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ServerMainFrame.this.setVisible(true);
            }
        });
        frameItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ServerMainFrame.this.setVisible(true);
            }
        });
        final ServerMainFrame f = this;
        connectionItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (f.isConnected()) {
                f.resetServer();
            } else {
                f.initializeServer();
            } }
        });
        cikisItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            f.exit();
            }
        });
    }

    protected static Image createImage(String path, String description) {
        URL imageURL = ServerMainFrame.class.getResource(path);

        if (imageURL == null) {
            System.err.println("Resource not found: " + path);
            return null;
        } else {
            return (new ImageIcon(imageURL, description)).getImage();
        }
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b); //To change body of generated methods, choose Tools | Templates.

        if (b) {
            if (this.trayIcon == null) {
                for (TrayIcon ic : this.tray.getTrayIcons()) {
                    this.tray.remove(ic);
                }
            } else {
                this.tray.remove(trayIcon);
            }
            this.setState(JFrame.NORMAL);
        } else {
            if (this.trayIcon == null) {
                this.createTrayIcon();
            }
            try {
                this.tray.add(trayIcon);
            } catch (AWTException ex) {
                Logger.getLogger(ServerMainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        String txt = connected ? STR_DISCONEECT : STR_CONNECT;
        jMenuItem1.setText(txt);
        this.connectionItem.setLabel(txt);
        this.connected = connected;
    }

    public void toggleConnection() {
        this.setConnected(!connected);
    }

    /**
     * Creates new form MainFrame
     */
    public ServerMainFrame() {
        createTrayIcon();
        listModel = new DefaultListModel();
        initComponents();
        this.connected = false;
        jMenuItem1.setText(STR_CONNECT);

        this.jTabbedPane1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
           if (jTabbedPane1.getSelectedIndex() == 1) {
                updateClientList();
            } }
        });
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowIconified(WindowEvent e) {
                ServerMainFrame.this.setVisible(false);
            }

        });
        this.initializeServer();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jpHistory = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        logger = new javax.swing.JTextPane();
        jPanel2 = new javax.swing.JPanel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jpList = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jlClientList = new javax.swing.JList();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jpHistory.setLayout(new java.awt.BorderLayout(5, 5));

        jButton1.setText("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jpHistory.add(jButton1, java.awt.BorderLayout.PAGE_END);

        jScrollPane3.setViewportView(logger);

        jpHistory.add(jScrollPane3, java.awt.BorderLayout.CENTER);

        jPanel2.setLayout(new java.awt.BorderLayout());

        jCheckBox1.setSelected(true);
        jCheckBox1.setText("AutoScroll");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });
        jPanel2.add(jCheckBox1, java.awt.BorderLayout.CENTER);

        jpHistory.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jTabbedPane1.addTab("Log", jpHistory);

        jpList.setLayout(new java.awt.BorderLayout());

        jlClientList.setModel(listModel);
        jScrollPane1.setViewportView(jlClientList);

        jpList.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("Kullanıcı Listesi", jpList);

        getContentPane().add(jTabbedPane1, java.awt.BorderLayout.CENTER);
        jTabbedPane1.getAccessibleContext().setAccessibleName("Log");

        jMenu1.setText("File");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Bağlan");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Çıkış");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:

        if (this.isConnected()) {
            if (JOptionPane.showConfirmDialog(this, "Bağlantıyı kesmek istediğinizden emin misiniz?", "Bağlantıyı Kes", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                this.resetServer();
            }
        } else {
            try {
                this.portN0 = Integer.parseInt(JOptionPane.showInputDialog(this, "Port Numarasını Giriniz", (this.portN0 + "")));
            } catch (Exception e) {
                this.portN0 = 4444;
            }
            this.initializeServer();
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed

        // TODO add your handling code here:
        if (JOptionPane.showConfirmDialog(rootPane, "Çıkış Yapmak İstediğinizden Emin misiniz", "Çıkış", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            this.exit();
        }

    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void resetServer() {
        try {
            server.terminate();
            server.join();
        } catch (Exception e) {

        } finally {
            server = null;
        }
    }

    private void initializeServer() {
        if (server != null && server.isAlive() && server.isWorking()) {
            server.addDataTransferListener(l);
            server.addServerStatuChangeListener(l);
            return;
        }
        server = new SeraServer(this.portN0);
        server.addDataTransferListener(l);
        server.addServerStatuChangeListener(l);
        server.start();

    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        if (server == null) {
            this.initializeServer();
        } else {
            this.resetServer();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_jCheckBox1ActionPerformed

    public void exit() {
        this.resetServer();
        System.exit(0);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ServerMainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
             new ServerMainFrame().setVisible(true);  
            }
        });
    }

    private void appendToPane(String msg, Color c) {

        JTextPane tp = this.logger;

        int oldPos = tp.getCaretPosition();
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg + "\n");
        if (jCheckBox1.isSelected()) {
            tp.setCaretPosition(tp.getDocument().getLength());
        } else {
            tp.setCaretPosition(oldPos);
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JList jlClientList;
    private javax.swing.JPanel jpHistory;
    private javax.swing.JPanel jpList;
    private javax.swing.JTextPane logger;
    // End of variables declaration//GEN-END:variables

    class ServerEventListener implements DataTransferListener, ServerStatuChangeListener {

        @Override
        public void dataTransfered(DataTransferEvent evt) {
            sera_server.Package pck = evt.getPackage();
            if (pck.getErrMessage() != null) {
                appendToPane(pck.getErrMessage(), Color.red);
            }
            appendToPane(evt.getPackage().toString(), Color.BLUE);
            String data = evt.getPackage().getReceivedPacage();
            Integer flag = 100;
            try {
                flag = Integer.valueOf(data.trim().split(" ")[0]);
            } catch (Exception ex) {
            }
//            if(flag!=3)
//                return;
            try {
                Double temp = Double.valueOf(data.trim().split(" ")[1]);
                try {
                    String file1 = "D:" + File.separator + "logger.txt";
                    String file2 = "D:" + File.separator + "logger2.txt";
                    FileWriter fw = new FileWriter(new File(file1), true);
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                    String val = format.format(new Date()) + " " + data.trim() + "\n";
                    fw.append(val);
                    fw.close();
                    fw = new FileWriter(new File(file2), true);
                    fw.append(val);
                    fw.close();
                } catch (IOException ex) {
                    Logger.getLogger(ServerMainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (Exception ex) {

            }

        }

        @Override
        public void serverStatusChanged(ServerStatuChangeEvent evt) {
            switch (evt.getConnectionStatu()) {
                case ServerStatuChangeEvent.CLIENT_CONNECTED: {
                    updateClientList();
                    break;
                }
                case ServerStatuChangeEvent.CLIENT_DISCONNECTED: {
                    updateClientList();
                    break;
                }
                case ServerStatuChangeEvent.SERVER_CONNECTED: {
                    setConnected(true);
                    break;
                }
                case ServerStatuChangeEvent.SERVER_DISCONNECTED: {
                    resetServer();
                    setConnected(false);
                    break;
                }
                case ServerStatuChangeEvent.PRINT: {
                    ServerPrintEvent event = (ServerPrintEvent) evt;
                    Color c;
                    switch (event.getType()) {
                        case ServerPrintEvent.DEBUG:
                            c = Color.GRAY;
                            break;
                        case ServerPrintEvent.ERROR:
                            c = Color.RED;
                            break;
                        case ServerPrintEvent.LOG:
                            c = Color.BLACK;
                            break;
                        default:
                            c = Color.BLACK;
                    }
                    appendToPane(event.getData(), c);
                    break;
                }

            }
        }

    }

    private void updateClientList() {
        if (listModel == null) {
            listModel = (DefaultListModel) this.jlClientList.getModel();
        }
        listModel.removeAllElements();
        if (server == null || server.clients == null) {
            return;
        }
        int clientNo = 0;
        for (SeraServer.ClientResponder t : server.clients) {
            if (t == null) {
                continue;
            }
            clientNo++;
            Date dt = t.getCreationTime().getTime();
            String tmp = new SimpleDateFormat("yy.MM.dd HH:mm:ss").format(dt);
            String str = tmp + " -->    <" + t.getThreadConnectionType() + ">     " + macToString(null);
            listModel.addElement(str);
        }
        this.trayIcon.setToolTip(clientNo + " makina bağlı");
    }

    private String macToString(int[] data) {
        if (data == null) {
            return "UNKNOWN";
        }
        String str = data[0] + "";
        for (int i = 1; i < data.length; i++) {
            str += "." + data[i];
        }
        return str;
    }
}
