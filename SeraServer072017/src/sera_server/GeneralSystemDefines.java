package sera_server;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static sera_server.GeneralSystemDefines.CONNECTION_TYPE.ARDUINO;
import static sera_server.GeneralSystemDefines.CONNECTION_TYPE.USER;

public class GeneralSystemDefines {

    public enum CONNECTION_TYPE {
        SERVER((short)0), ARDUINO((short)1), USER((short)2);
        private final short value;

        CONNECTION_TYPE(short val) {
            this.value = val;
        }
        public short getValue(){
            return value;
        }

        public static CONNECTION_TYPE getElement(short val) {
            for(CONNECTION_TYPE ct:CONNECTION_TYPE.values())
                if(ct.getValue()==val)
                    return ct;
            return null;
        }
        
    }
//	public final static byte ID_DATA_N35 = 2;
//	public final static byte ID_IZLEYICI_HAVA_DURUMU = 11;
//	public final static byte ID_IZLEYICI_N35 = 12;
//        public final static byte ID_DATA_LOGGER = 5;
    // server bagli clientlerdan gelmesi istenilen id degerler servera
    // baglanilacak olan programlar
    // bu bilgilere gore hazirlanmalidir
    // public final static byte ID_ARDUINO_WHEATHER = 1;
    // public final static byte ID_IZLEYICI_1 = 5;
    //
    public final static short REQUEST_ID = 50;
    public final static short ID_OK=51;
    public final static Map<CONNECTION_TYPE, CONNECTION_TYPE> mapping = new EnumMap<CONNECTION_TYPE, CONNECTION_TYPE>(CONNECTION_TYPE.class
    );
    // public final static Map<Byte, Byte> idTypeMapping = new HashMap<Byte,
    // Byte>();

    static {
        // idTypeMapping.put(ID_ARDUINO_WHEATHER, TYPE_DATA_HAVA_DURUMU);
        // idTypeMapping.put(ID_IZLEYICI_1, TYPE_IZLEYICI_HAVA_DURUMU);
        mapping.put(ARDUINO, USER);
        mapping.put(USER, ARDUINO);
//		mapping.put(ID_IZLEYICI_HAVA_DURUMU, ID_DATA_HAVA_DURUMU);
//		mapping.put(ID_IZLEYICI_N35, ID_DATA_N35);
    }

    public static boolean isAllowedMultipleConnection(int threadConnectionType) {
        // TODO Auto-generated method stub
        // return false;
//		return threadConnectionType != ID_DATA_HAVA_DURUMU;
        return true;
    }

    public static boolean isAllowedMultipleConnectionOnSameMachine(int threadConnectionType) {
//		return isAllowedMultipleConnection(threadConnectionType) && (threadConnectionType == ID_IZLEYICI_HAVA_DURUMU || threadConnectionType == ID_IZLEYICI_N35);
        return true;
    }
}
