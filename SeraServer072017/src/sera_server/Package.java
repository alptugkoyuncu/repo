/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera_server;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author WIN7
 */
public class Package {

    private final static String anaAyirac = ":";
    private final static String araAyirac = " ";
    private GeneralSystemDefines.CONNECTION_TYPE sender;
    private List<GeneralSystemDefines.CONNECTION_TYPE> receiver;
    private Byte action;
    private Byte errFlag;
    private String data;
    private String receivedPacage;
    private String errMessage;
    private String formalMessgae;

    public void addReceiver(GeneralSystemDefines.CONNECTION_TYPE receiver){
        if(this.receiver==null)
            this.receiver = new ArrayList<>();
        this.receiver.add(receiver);
    }
    public GeneralSystemDefines.CONNECTION_TYPE getSender() {
       return sender;
    }

    public List<GeneralSystemDefines.CONNECTION_TYPE> getReceiver() {
        return receiver==null ? new ArrayList<GeneralSystemDefines.CONNECTION_TYPE>() : receiver;
    }

    public Byte getAction() {
        return action;
    }

    public Byte getErrFlag() {
        return errFlag;
    }

    public String getData() {
        return data;
    }

    public String getReceivedPacage() {
        return this.receivedPacage==null||"".equals(receivedPacage.trim())?null :this.receivedPacage;
    }

    public String getErrMessage() {
        
        return errMessage==null || errMessage.trim().equals("")?null:this.errMessage;
    }

    public String getFormalMessgae() {
        return formalMessgae==null || formalMessgae.trim().equals("")?null :formalMessgae;
    }
    public Package(String recvData,GeneralSystemDefines.CONNECTION_TYPE sender){
        this.receiver=new ArrayList<>();
        this.sender = sender;
        this.errMessage = null;
        this.formalMessgae = null;
        StringBuilder err = new StringBuilder();
        StringBuilder frmErr = new StringBuilder();
        boolean hasErr = false;
        this.receivedPacage = recvData;
        String temp = this.receivedPacage;
        temp = temp.replace("<", "");
        temp = temp.replace(">", "");
        temp = temp.trim();
        String t1[] = temp.split(anaAyirac);
        String headers[] = t1[0].split(araAyirac);
        try {
            this.action = Byte.parseByte(headers[0]);
        } catch (Exception e) {
            err.append("Action datasi parse edilemedi");
            frmErr.append(e.getMessage());
            hasErr = true;
            this.action = 0;
        }
        this.errFlag = 0;
        if (headers.length == 2) {
            try {
                this.errFlag = Byte.parseByte(headers[1]);
            } catch (Exception e) {
                err.append("Error Flag datasi parse edilemedi");
                frmErr.append(e.getMessage());
                hasErr = true;
                this.errFlag = null;
            }
        }
        if (headers.length > 2) {
            err.append("Headerda fazla data var Uzunluk = " + headers.length);
            hasErr = true;
        }
        if (t1.length <= 1) {
            this.data = null;
        } else {
            this.data = t1[1];
             if (t1.length > 2) {
                err.append("Package fazla uzun = " + t1.length);
                hasErr = true;
            }
        }
        if (hasErr) {
            this.errMessage = err.toString();
            this.formalMessgae = frmErr.toString();
        }

    }
    public Package(String recvData, GeneralSystemDefines.CONNECTION_TYPE sender, GeneralSystemDefines.CONNECTION_TYPE receiver) {
        this(recvData,sender);
        this.receiver.add(receiver);
        
    }

    @Override
    public String toString() {
        String receiver="";
        if(this.getReceiver().size()==0)
            receiver="-";
        else{
            receiver = this.getReceiver().get(0)+"";
            for(int i=1; i<this.getReceiver().size();i++){
                receiver+=","+this.getReceiver().get(i);
            }
        }
        return SeraServer.getTime()+" : "+ this.sender+ "-->"+receiver + " ("+this.getReceivedPacage()+")";
    }

}
