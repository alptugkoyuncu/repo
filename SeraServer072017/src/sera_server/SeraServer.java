package sera_server;

import java.io.BufferedReader;
import sera_server.listeners.DataTransferListener;
import sera_server.listeners.ServerStatuChangeListener;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import sera_server.listeners.DataTransferEvent;
import sera_server.listeners.ServerPrintEvent;
import sera_server.listeners.ServerStatuChangeEvent;
import util.ByteConversion;

public class SeraServer extends Thread {

    private final static String[] EXTERNAL_IP_SORGULAMA_ADDRESI = {
        "http://checkip.amazonaws.com/",
        "http://checkip.amazonaws.com/",
        "http://icanhazip.com/",
        "http://www.trackip.net/ip",
        "http://myexternalip.com/raw",
        "http://ipecho.net/plain",
        "http://bot.whatismyipaddress.com"
    };
    // Server soket
    private ServerSocket serverSocket = null;
    // Client soket
    private Socket clientSocket = null;
    // Maximum bağlantı sayısı
    private final int maxClientSayisi = 20;
    // Her bir client için oluşturlacak Thread dizisi
    public final ClientResponder[] clients = new ClientResponder[maxClientSayisi];
    private int portNo;
    private InetAddress serverIp;
    private boolean startServer;
    private List<ServerStatuChangeListener> _statuListeners = new ArrayList<>();
    private List<DataTransferListener> _transferListener = new ArrayList<>();

    public void addDataTransferListener(DataTransferListener l) {
        if (!this._transferListener.contains(l)) {
            this._transferListener.add(l);
        }
    }

    public void addServerStatuChangeListener(ServerStatuChangeListener l) {
        if (!this._statuListeners.contains(l)) {
            this._statuListeners.add(l);
        }
    }

    public boolean removeDataTransferListener(DataTransferListener l) {
        return this._transferListener.remove(l);
    }

    public boolean removeServerStatuChangeListener(ServerStatuChangeListener l) {
        return this._statuListeners.remove(l);
    }

    private void fireDataTranferedEvent(Package pck) {
        DataTransferEvent evt = new DataTransferEvent(this, pck);
        for (DataTransferListener l : this._transferListener) {
            l.dataTransfered(evt);
        }

    }

    private void fireServerStatuChangedEvent(ServerStatuChangeEvent evt) {
        for (ServerStatuChangeListener l : this._statuListeners) {
            l.serverStatusChanged(evt);
        }
    }

    public SeraServer(int portNo) {
        this.portNo = portNo;
        try {
            this.serverIp = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            this.serverIp = null;
            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public synchronized void start() {
        try {
            if (this.serverIp != null) {
                serverSocket = new ServerSocket();
                serverSocket.bind(new InetSocketAddress(this.serverIp, portNo));
            } else {
                serverSocket = new ServerSocket(portNo);
            }
        } catch (IOException ex) {
            print("Server Error : " + ex.getMessage(), ServerPrintEvent.ERROR);
            this.startServer = false;
            try {
                this.join();
            } catch (InterruptedException ex1) {
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex1);
            }
            return;
        }
        String extIp = getExternalIP();
        print("Server basladi : " + extIp + " \\ " + portNo + "", ServerPrintEvent.LOG);

        this.startServer = true;
        super.start(); //To change body of generated methods, choose Tools | Templates.
        ServerStatuChangeEvent obj = new ServerStatuChangeEvent(this, ServerStatuChangeEvent.SERVER_CONNECTED);
        this.fireServerStatuChangedEvent(obj);
    }

    public boolean isWorking() {
        return startServer;
    }

    public synchronized void terminate() {
        if (!this.startServer) {
            return;
        }
        this.startServer = false;
        for (ClientResponder t : this.clients) {
            if (t != null) {
                t.closeConnection();
            }
        }
        try {
            this.serverSocket.close();
            this.serverSocket = null;
        } catch (IOException ex) {
            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        ServerStatuChangeEvent obj = new ServerStatuChangeEvent(this, ServerStatuChangeEvent.SERVER_DISCONNECTED);
        this.fireServerStatuChangedEvent(obj);
    }

    public int getClientCount() {
        int count = 0;
        for (int i = 0; i < maxClientSayisi; i++) {
            if (this.clients[i] != null) {
                count++;
            }
        }
        return count;
    }

    @Override
    public void run() {
        /*
         * Her bir client için ayrı soketler ve threadlerin oluşturulması
         */

        while (this.startServer) {
            while (this.getClientCount() >= this.maxClientSayisi) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                clientSocket = serverSocket.accept();

                int i = 0;
                ClientResponder t = new ClientResponder(clientSocket, clients);
                t.start();
                this.addClientToList(t);
                if (this.getClientCount() == this.maxClientSayisi) {
                    print("Server maksimum kapasiteye ulasti.", ServerPrintEvent.LOG);
                }

            } catch (IOException e) {
                print(e.getMessage(), ServerPrintEvent.ERROR);
                break;
            }
        }
        this.startServer = false;
    }

    private void removeClientFromList(ClientResponder t) {
        boolean removed = false;
        for (int i = 0; i < this.maxClientSayisi; i++) {
            if (this.clients[i] != null && this.clients[i] == t) {
                removed = true;
                this.clients[i] = null;
            }
        }
        if (!removed) {
            return;
        }
        ServerStatuChangeEvent evt = new ServerStatuChangeEvent(this, ServerStatuChangeEvent.CLIENT_DISCONNECTED);
        this.fireServerStatuChangedEvent(evt);
    }

    private void addClientToList(ClientResponder t) {
        for (int i = 0; i < maxClientSayisi; i++) {
            if (clients[i] == null) {
                clients[i] = t;
                break;
            }
        }
        ServerStatuChangeEvent evt = new ServerStatuChangeEvent(this, ServerStatuChangeEvent.CLIENT_CONNECTED);
        this.fireServerStatuChangedEvent(evt);
    }

    public void print(String s, int type) {
        ServerPrintEvent evt = new ServerPrintEvent(this, type, SeraServer.getTime() + " : " + s);
        System.out.println(evt.getData());
        this.fireServerStatuChangedEvent(evt);
    }

    private String getExternalIP() {
        URL whatismyip;
        String ip;
        int i = -1;
        while (i < EXTERNAL_IP_SORGULAMA_ADDRESI.length) {
            i++;
            ip = null;
            whatismyip = null;
            try {
                whatismyip = new URL(EXTERNAL_IP_SORGULAMA_ADDRESI[i]);
                BufferedReader in;
                try {
                    in = new BufferedReader(new InputStreamReader(
                            whatismyip.openStream()));

                    ip = in.readLine(); //you get the IP as a String
                    System.out.println(ip);
                } catch (IOException ex) {
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                    continue;
                }

            } catch (MalformedURLException ex) {
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                continue;
            }
            return ip;
        }
        return null;
    }

    public static Set<Integer> getFocusClientTypes(final ClientResponder source, final ClientResponder[] list) {
        // TODO Auto-generated method stub
        Set<Integer> set = new HashSet<Integer>();
        GeneralSystemDefines.CONNECTION_TYPE focusType = GeneralSystemDefines.mapping.get(source.getThreadConnectionType());

        for (int i = 0; i < list.length && focusType != null; i++) {
            if (list[i] != null && list[i] != source && list[i].getThreadConnectionType() == focusType) {
                set.add(i);
            }
        }

        return set;
    }

    public static String getTime() {
        Calendar c = Calendar.getInstance();
        int h = c.get(Calendar.HOUR_OF_DAY);
        int m = c.get(Calendar.MINUTE);
        int s = c.get(Calendar.SECOND);
        return "" + h + ":" + m + ":" + s;
    }

    public class ClientResponder extends Thread {

        public final static byte ACTION_PING = 0;

        public final static byte ACTION_CLIENT_STARTED = 1;
        public final static byte ACTION_REQUEST_TIME = 34;
        public final static byte ACTION_INITIALIZE_CLOCK = 31;
        public final Calendar creationTime;
        private DataInputStream dis = null;
        private PrintStream ps = null;
        private Socket clientSocket = null;
        private final ClientResponder[] threads;
        private GeneralSystemDefines.CONNECTION_TYPE clientType;
        private long lastReceived, lastSent;
        private final static long PING_WAIT_MS = 60000;
        // private byte connectionId;

        private ClientResponder(Socket clientSocket, ClientResponder[] threads) {
            this.creationTime = Calendar.getInstance();
            this.clientSocket = clientSocket;
            this.threads = threads;
            this.lastReceived = System.currentTimeMillis();
            this.lastSent = System.currentTimeMillis();

        }

        public GeneralSystemDefines.CONNECTION_TYPE requestConnectionType() {
            int headerIsteTry = 5;
            while (headerIsteTry > 0) {
                //type iste ;toplamda 5deneme
                System.out.println("type iste");
                try {
                    ps.write(ByteConversion.fromShort(GeneralSystemDefines.REQUEST_ID,false));
                } catch (IOException ex) {
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                }
                ps.flush();
                headerIsteTry--;
                try {
                    //type oku
                    System.out.print("Type oku : ");
                    short type = dis.readShort();
                    System.out.println(type);
                    int headerKontrolTry = 5;
                    do {
                        System.out.println("Type chek");
                        //type check islemi 5 kez deneme
                        headerKontrolTry--;
                        GeneralSystemDefines.CONNECTION_TYPE ct = GeneralSystemDefines.CONNECTION_TYPE.getElement(type);
                        //type yanlissa bi daha istemek icin basa git
                        if (ct == null) {
                            System.out.println("Type yanlis");
                            continue;

                        }
                        System.out.print("Type var geri gonder :");
                        //type gonder
                         ps.write(ByteConversion.fromShort(type,false));
                        System.out.println(type);
                        ps.flush();
                        System.out.print("Yeni bilgiyi oku : ");
                        //gelen bilgiyi oku 
                        short newValue = dis.readShort();
                        System.out.println(newValue);
                        //gelen bilgi ID_OK ISE islemi tamamla
                        if (newValue == GeneralSystemDefines.ID_OK) {
                            return ct;
                        }

                        //degilse yeni typedir type check islemini tekrarla
                    } while (headerKontrolTry > 0);
                } catch (IOException ex) {
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            return null;
        }

        public Calendar getCreationTime() {
            return creationTime;
        }

        @Override
        public void run() {

            try {
                dis = new DataInputStream(clientSocket.getInputStream());

                // DataOutputStream dos = new DataOutputStream(null);
                ps = new PrintStream(clientSocket.getOutputStream());
                this.clientType = this.requestConnectionType();
                if (this.clientType == null) {
                    this.closeConnection();
                    return;
                }

                SeraServer.this.print("Yeni kullanici< id:" + this.clientType + " type : " + this.clientType + ">", ServerPrintEvent.LOG);
                String tmp = "" + ACTION_CLIENT_STARTED + " 0";

                this.clientSocket.setKeepAlive(true);

                for (Integer i : getFocusClientTypes(this, this.threads)) {
                    SeraServer.this.print("<" + threads[i].getThreadConnectionType() + ">", ServerPrintEvent.LOG);
                    threads[i].sentLine("<" + this.clientType + ":" + tmp + ">");;
                }
                Package pck;
//            this.lastSent = System.currentTimeMillis();
                this.lastReceived = System.currentTimeMillis();
                while (true) {
                    if (this.clientSocket.isInputShutdown() || this.clientSocket.isOutputShutdown()
                            || this.clientSocket.isClosed()) {
                        break;
                    }

                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (System.currentTimeMillis() - this.lastSent > PING_WAIT_MS) {
                        if (this.ps != null) {
                            String d = "<" + ACTION_PING + ">";
                            this.sentLine(d);
                        }
                    }
                    if ((System.currentTimeMillis() - this.lastReceived) > 3 * PING_WAIT_MS) {
                        break;
                    }
                    String satir = this.getLine();
                    if (satir == null) {
                        continue;
                    }
                    pck = new Package(satir, this.getThreadConnectionType());

                    String tmp2 = isForServer(pck);
                    if (tmp2 != null) {
                        pck.addReceiver(GeneralSystemDefines.CONNECTION_TYPE.SERVER);
                        fireDataTranferedEvent(pck);
                        if (!tmp2.trim().equalsIgnoreCase("")) {
                            pck = new Package(tmp2, GeneralSystemDefines.CONNECTION_TYPE.SERVER, this.clientType);
                            String sentData = "<" + 0 + ":" + tmp2 + ">";
                            this.sentLine(sentData);
                        }
                    } else {
                        for (Integer i : getFocusClientTypes(this, this.threads)) {
                            threads[i].sentLine("<" + this.clientType + ":" + satir + ">");
                            pck.addReceiver(threads[i].getThreadConnectionType());
                        }
                        fireDataTranferedEvent(pck);
                    }

                }
            } catch (IOException e) {
                SeraServer.this.print(e.getMessage(), ServerPrintEvent.ERROR);
            } finally {
                this.closeConnection();
            }
        }

        private String getLine() throws IOException {
            if (dis != null && dis.available() > 0) {
                String str = dis.readLine();
                str = str.replace("<", "");
                this.lastReceived = System.currentTimeMillis();
                return str.replace(">", "").trim();
            }
            return null;
        }

        private void sentLine(String line) {
            ps.println(line);
            ps.flush();
            this.lastSent = System.currentTimeMillis();
            fireDataTranferedEvent(new Package(line, GeneralSystemDefines.CONNECTION_TYPE.SERVER, this.clientType));
        }

        private String isForServer(Package pck) {
            int act = pck.getAction();

            String str = "";
            switch (act) {
                case ACTION_REQUEST_TIME: {
                    Calendar c = Calendar.getInstance();
                    str += ACTION_INITIALIZE_CLOCK + ":" + c.get(Calendar.YEAR) % 100 + " " + (c.get(Calendar.MONTH) + 1) + " "
                            + c.get(Calendar.DATE) + " " + (c.get(Calendar.DAY_OF_WEEK)) + " " + c.get(Calendar.HOUR_OF_DAY)
                            + " " + c.get(Calendar.MINUTE) + " " + c.get(Calendar.SECOND);

                    return str;
                }
                case ACTION_INITIALIZE_CLOCK: {
                    return "";
                }
                default:
                    return null;
            }

        }

        public void closeConnection() {
            // for (int i = 0; i < maxClientSayisi; i++) {
            // if (threads[i] != null && threads[i] != this) {
            // threads[i].ps.println(this.threadConnectionType + " adlı kisi odadan
            // ayrildi.");
            // }
            // }
            // ps.println(this.threadConnectionType + " Gule Gule!");

            /*
             * Yeni bir Clientın bağlanabilmesi için aktif olan Client null yapılır
             */
            SeraServer.this.print(this.clientType + " Gule Gule!", ServerPrintEvent.LOG);
            removeClientFromList(this);
            try {
                dis.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                SeraServer.this.print(e.getMessage(), ServerPrintEvent.ERROR);
            }

            ps.close();
            try {
                clientSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                SeraServer.this.print(e.getMessage(), ServerPrintEvent.ERROR);
            }
        }

        /**
         * @return the threadConnectionType
         */
        public GeneralSystemDefines.CONNECTION_TYPE getThreadConnectionType() {
            return clientType;
        }

        private void print(String s) {
            System.out.println(s);
        }
    }

}

/*
 * Client soketler için oluşturulmuş Thread sınıfı Her bir Client ile Thread
 * eşleştirilmesi ile Multithread uygulama
 */
