/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sera_server.listeners;
import java.util.EventObject;


/**
 *
 * @author WIN7
 */

public class DataTransferEvent extends EventObject{
    
    sera_server.Package pck;
    
    public DataTransferEvent(Object source,sera_server.Package pck) {
        super(source);
        this.pck = pck;
    }
    
    public sera_server.Package getPackage(){    
        return this.pck;
    }
}
