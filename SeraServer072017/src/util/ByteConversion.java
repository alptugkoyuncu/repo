/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author alptug
 */
public class ByteConversion {

    private ByteConversion() {

    }

    public static byte[] fromShort(short x, boolean isLittleEndian) {
        // Little Endian
        byte[] ret = new byte[2];
        if (isLittleEndian) {
            ret[0] = (byte) x;
            ret[1] = (byte) (x >> 8);
        } else {
            // Big Endian generally network conversion
            ret[0] = (byte) (x >> 8);
            ret[1] = (byte) x;
        }
        return ret;
    }
}
