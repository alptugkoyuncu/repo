package main;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class ServerConfigProp {

    private final static String propFileName;
    public final static String osHome;
    public final static String mainHome;
    public final static String clientMappingFileLocation;
    private static boolean flag=false;
    static {

        System.out.println("create file");
        osHome = new JFileChooser().getCurrentDirectory().getPath();
        File folder;
        int noTry = 10;
        while (noTry > 0) {
            noTry--;
            folder = new File(osHome + File.separator + "Sera Server");
            if (folder.exists()) {

                break;
            } else {
                folder.mkdirs();
            }
        }
        if (noTry <= 0) {
            mainHome = null;
            clientMappingFileLocation = null;
            propFileName=null;
        } else {
            mainHome = osHome + File.separator + "Sera Server";
            clientMappingFileLocation = mainHome + File.separator + "fileMap.txt";
            noTry = 10;
            String fName = "ServerProp.properties";
            String folderName = mainHome + File.separator + "resources";
            folder = new File(folderName);

            while (noTry > 0) {
                noTry--;
                if (folder.exists()) {
                    File f = new File(folderName + File.separator + fName);
                    if (f.exists()) {
                        break;
                    } else {
                        try {
                            if (f.createNewFile()) {
                                saveServerDbProp(new ServerDbProp());
                                saveServerSocketProp(new ServerSocketProp());
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(ServerConfigProp.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                } else {
                    folder.mkdirs();
                }
            }
            propFileName = noTry > 0 ? folderName + File.separator + fName : null;

        }
        flag=true;
    }

    public static void init(){
        while(!flag){
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(ServerConfigProp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println(propFileName);
    }
    private ServerConfigProp() {
    }

    private static Properties LoadPropertiesFile() throws FileNotFoundException, IOException {
        Properties props = new Properties();

        FileInputStream fis = new FileInputStream(propFileName);
        props.load(fis);
        fis.close();
        return props;
    }

    public static ServerDbProp getServerDbProp() throws FileNotFoundException, IOException {
        ServerDbProp sp = new ServerDbProp();
        Properties prop = LoadPropertiesFile();
        sp.setDriver(prop.getProperty(ServerDbProp.DRIVER_PROP));
        sp.setPassword(prop.getProperty(ServerDbProp.PASSWORD_PROP));
        sp.setUrl(prop.getProperty(ServerDbProp.URL_PROP));
        sp.setUserName(prop.getProperty(ServerDbProp.USERNAME_PROP));
        return sp;
    }

    public static ServerSocketProp getServerSocketProp() throws FileNotFoundException, IOException {
        ServerSocketProp sp = new ServerSocketProp();
        Properties prop = LoadPropertiesFile();
        int num;
        try {
            String val;
            val = prop.getProperty(ServerSocketProp.PROP_PORT, "5555");
            num = Integer.parseInt(val);
        } catch (NumberFormatException ex) {
            num = 5555;
        }
        sp.setPort(num);
        return sp;
    }

    public static void saveServerSocketProp(ServerSocketProp sp) throws IOException {
        Properties prop = LoadPropertiesFile();
        try (FileOutputStream out = new FileOutputStream(propFileName)) {
            prop.put(ServerSocketProp.PROP_PORT, sp.getPort() + "");
            prop.store(out, null);
        } catch (IOException ex) {
            Logger.getLogger(ServerConfigProp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void saveServerDbProp(ServerDbProp sp) throws IOException {
        Properties prop = LoadPropertiesFile();
        try (FileOutputStream out = new FileOutputStream(propFileName)) {
            prop.put(ServerDbProp.DRIVER_PROP, sp.getDriver());
            prop.put(ServerDbProp.PASSWORD_PROP, sp.getPassword());
            prop.put(ServerDbProp.URL_PROP, sp.getUrl());
            prop.put(ServerDbProp.USERNAME_PROP, sp.getUserName());
            prop.store(out, null);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ServerConfigProp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
