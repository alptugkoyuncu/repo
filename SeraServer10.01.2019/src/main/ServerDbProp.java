/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.File;

/**
 *
 * @author alptug
 */
public class ServerDbProp {

    final static String DRIVER_PROP = "db.driver";
    final static String PASSWORD_PROP = "db.password";
    final static String URL_PROP = "db.url";
    final static String USERNAME_PROP = "db.username";
    private String driver;
    private String password;
    private String url;
    private String userName;

    ServerDbProp() {
        resetDefault();
    }

    public void resetDefault() {
        this.driver = "com.mysql.jdbc.Driver";
        this.url = "jdbc:mysql://localhost:3306/sera?useUnicode=true&characterEncoding=utf8";
        this.userName = "root";
        this.password = "19791981";
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        if (driver != null) {
            this.driver = driver;
        }
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (password != null) {
            this.password = password;
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        if (url != null) {
            this.url = url;
        }
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        if (userName != null) {
            this.userName = userName;
        }
    }

}
