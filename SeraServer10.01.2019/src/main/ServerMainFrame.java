package main;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import socket.SeraServer;
import data.DataTransferEvent;
import data.DataTransferListener;
import java.awt.Component;
import javax.swing.JComponent;
import settings.PanelSettings;
import socket.listeners.ServerEventListener;
import socket.listeners.ServerLoggerEvent;
import socket.listeners.ServerStatuChangeEvent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author WIN7
 */
public class ServerMainFrame extends javax.swing.JFrame {

    private boolean connected = false;
    private final static String STR_CONNECT = "Bağlan";
    private final static String STR_DISCONEECT = "Bağlantıyı Kes";
    SeraServer server;
    final ServerListener l = new ServerListener();
    DefaultListModel listModel = null;
    TrayIcon trayIcon;
    final SystemTray tray = SystemTray.getSystemTray();
    final PopupMenu popup = new PopupMenu();
    MenuItem frameItem, connectionItem, cikisItem;
    ServerSocketProp socketProp;

    private void createTrayIcon() {
        trayIcon = new TrayIcon(createImage("images/bulb.gif", "tray icon"), "0 makina bağlı");

        trayIcon.setImageAutoSize(true);
        // Create a popup menu components
        frameItem = new MenuItem("Göster");
        connectionItem = new MenuItem();
        cikisItem = new MenuItem("Çıkış");

        //Add components to popup menu
        popup.add(frameItem);
        popup.add(connectionItem);
        popup.add(cikisItem);

        trayIcon.setPopupMenu(popup);

        trayIcon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ServerMainFrame.this.setVisible(true);
            }
        });
        frameItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ServerMainFrame.this.setVisible(true);
            }
        });
        final ServerMainFrame f = this;
        connectionItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (f.isConnected()) {
                    f.resetServer();
                } else {
                    f.initializeServer();
                }
            }
        });
        cikisItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.exit();
            }
        });
    }

    protected static Image createImage(String path, String description) {
        URL imageURL = ServerMainFrame.class.getResource(path);

        if (imageURL == null) {
            System.err.println("Resource not found: " + path);
            return null;
        } else {
            return (new ImageIcon(imageURL, description)).getImage();
        }
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b); //To change body of generated methods, choose Tools | Templates.

        if (b) {
            if (this.trayIcon == null) {
                for (TrayIcon ic : this.tray.getTrayIcons()) {
                    this.tray.remove(ic);
                }
            } else {
                this.tray.remove(trayIcon);
            }
            this.setState(JFrame.NORMAL);
        } else {
            if (this.trayIcon == null) {
                this.createTrayIcon();
            }
            try {
                this.tray.add(trayIcon);
            } catch (AWTException ex) {
                Logger.getLogger(ServerMainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        String txt = connected ? STR_DISCONEECT : STR_CONNECT;
        jMenuItem1.setText(txt);
        this.connectionItem.setLabel(txt);
        this.connected = connected;
    }

    public void toggleConnection() {
        this.setConnected(!connected);
    }

    /**
     * Creates new form MainFrame
     */
    public ServerMainFrame() {
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(ServerMainFrame.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        System.out.println("main frame");
        try {
            socketProp = ServerConfigProp.getServerSocketProp();
        } catch (IOException ex) {
            Logger.getLogger(ServerMainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        createTrayIcon();
        listModel = new DefaultListModel();
        initComponents();
        this.connected = false;
        jMenuItem1.setText(STR_CONNECT);

        this.jTabbedPane1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (jTabbedPane1.getSelectedIndex() == 1) {
                    updateClientList();
                }
            }
        });
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowIconified(WindowEvent e) {
                ServerMainFrame.this.setVisible(false);
            }

        });
        this.initializeServer();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList<>();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jpHistory = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        logger = new javax.swing.JTextPane();
        jPanel2 = new javax.swing.JPanel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jpList = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jlClientList = new javax.swing.JList();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(jList1);

        jLabel1.setText("İzleyiciler");

        jLabel2.setText("Data Göndericiler");

        jList2.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane4.setViewportView(jList2);

        jButton2.setText("İzleyici Ekle");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Gönderici Ekle");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jScrollPane2)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE))
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 97, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton3))
                .addGap(99, 99, 99))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addContainerGap(68, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jpHistory.setLayout(new java.awt.BorderLayout(5, 5));

        jButton1.setText("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jpHistory.add(jButton1, java.awt.BorderLayout.PAGE_END);

        jScrollPane3.setViewportView(logger);

        jpHistory.add(jScrollPane3, java.awt.BorderLayout.CENTER);

        jPanel2.setLayout(new java.awt.BorderLayout());

        jCheckBox1.setSelected(true);
        jCheckBox1.setText("AutoScroll");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });
        jPanel2.add(jCheckBox1, java.awt.BorderLayout.CENTER);

        jpHistory.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jTabbedPane1.addTab("Log", jpHistory);

        jpList.setLayout(new java.awt.BorderLayout());

        jlClientList.setModel(listModel);
        jScrollPane1.setViewportView(jlClientList);

        jpList.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("Aktif Kullanıcı Listesi", jpList);

        getContentPane().add(jTabbedPane1, java.awt.BorderLayout.CENTER);
        jTabbedPane1.getAccessibleContext().setAccessibleName("Log");

        jMenu1.setText("File");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Bağlan");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Çıkış");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");

        jMenuItem3.setText("Settings");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:

        if (this.isConnected()) {
            if (JOptionPane.showConfirmDialog(this, "Bağlantıyı kesmek istediğinizden emin misiniz?", "Bağlantıyı Kes", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                this.resetServer();
            }
        } else {
            int val;
            try {
                String val2 = JOptionPane.showInputDialog(this, "Port Numarasını Giriniz", (socketProp.getPort() + ""));
                val = Integer.parseInt(val2);
                this.setPortN0(val);
            } catch (Exception e) {
                return;
            }
//            this
            this.initializeServer();
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed

        // TODO add your handling code here:
        if (JOptionPane.showConfirmDialog(rootPane, "Çıkış Yapmak İstediğinizden Emin misiniz", "Çıkış", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            this.exit();
        }

    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void resetServer() {
        try {
            server.terminate();
            server.join();
        } catch (Exception e) {

        } finally {
            server = null;
        }
    }

    private void initializeServer() {
        if (server != null && server.isAlive() && server.isWorking()) {
            server.addDataTransferListener(l);
            server.addServerEventListener(l);
            return;
        }
        try {
            server = new SeraServer(socketProp.getPort());
        } catch (IOException ex) {
            Logger.getLogger(ServerMainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        server.addDataTransferListener(l);
        server.addServerEventListener(l);
        server.start();

    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        if (server == null) {
            this.initializeServer();
        } else {
            this.resetServer();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
//        JListEkleyici pnl = new JListEkleyici();
//        JOptionPane.showConfirmDialog(this, pnl);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        PanelSettings pnl = new PanelSettings();
        int opt = JOptionPane.showConfirmDialog(this, pnl, "Ayarlar", JOptionPane.YES_NO_OPTION);
        if (opt == JOptionPane.YES_OPTION) {
            String res = pnl.save() ? "Kayit basarili" : "Kayit basarisiz";
            JOptionPane.showConfirmDialog(this, res);
        }
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    public void exit() {
        this.resetServer();
        System.exit(0);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ServerMainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
//        ServerConfigProp.initilaizePropertiesFiles();
//ServerConfigProp.init();
        try {
            ServerDbProp sp = ServerConfigProp.getServerDbProp();
            System.out.println(sp.getUrl());
        } catch (IOException ex) {
            Logger.getLogger(ServerMainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ServerMainFrame().setVisible(true);
            }
        });
    }

    private void appendToPane(String msg, Color c) {

        JTextPane tp = this.logger;

        int oldPos = tp.getCaretPosition();
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg + "\n");

        if (jCheckBox1.isSelected()) {
            tp.setCaretPosition(tp.getDocument().getLength());
            tp.setAutoscrolls(true);
        } else {
            tp.setCaretPosition(oldPos);
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList<String> jList1;
    private javax.swing.JList<String> jList2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JList jlClientList;
    private javax.swing.JPanel jpHistory;
    private javax.swing.JPanel jpList;
    private javax.swing.JTextPane logger;
    // End of variables declaration//GEN-END:variables

    class ServerListener implements DataTransferListener, ServerEventListener {

        @Override
        public void dataReceived(DataTransferEvent evt) {
            String data = util.BasicTime.getCurrentLongTime() + ":(" + evt.getNetworkPackage().getSender() + ")" + evt.getNetworkPackage().getNetworkData();
//            if (pck.getErrMessage() != null) {
//                appendToPane(pck.getErrMessage(), Color.red);
//            }
            appendToPane(data, Color.BLACK);
//            String data = pck.getNetworkData();
//            saveToFile(data);
        }

        @Override
        public void dataSent(DataTransferEvent evt) {
            String data = util.BasicTime.getCurrentLongTime() + ":(" + evt.getNetworkPackage().getReceiverList() + ")" + evt.getNetworkPackage().getNetworkData();
//            if (pck.getErrMessage() != null) {
//                appendToPane(pck.getErrMessage(), Color.red);
//            }
            appendToPane(data, Color.BLUE);
//            String data = pck.getNetworkData();
//            saveToFile(data);
        }

        private void saveToFile(String data) {
            try {
                try {
                    String file1 = "D:" + File.separator + "logger.txt";
                    String file2 = "D:" + File.separator + "logger2.txt";
                    FileWriter fw = new FileWriter(new File(file1), true);
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                    String val = format.format(new Date()) + " " + data.trim() + "\n";
                    fw.append(val);
                    fw.close();
                    fw = new FileWriter(new File(file2), true);
                    fw.append(val);
                    fw.close();
                } catch (IOException ex) {
                    Logger.getLogger(ServerMainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (Exception ex) {

            }
        }

        @Override
        public void serverStatusChanged(ServerStatuChangeEvent evt) {
            switch (evt.getConnectionStatu()) {
                case ServerStatuChangeEvent.CLIENT_CONNECTED: {
                    updateClientList();
                    break;
                }
                case ServerStatuChangeEvent.CLIENT_DISCONNECTED: {
                    updateClientList();
                    break;
                }
                case ServerStatuChangeEvent.SERVER_CONNECTED: {
                    setConnected(true);
                    break;
                }
                case ServerStatuChangeEvent.SERVER_DISCONNECTED: {
                    resetServer();
                    setConnected(false);
                    break;
                }
            }
        }

        @Override
        public void log(ServerLoggerEvent evt) {
            Color c;
            switch (evt.getType()) {
                case ServerLoggerEvent.DEBUG:
                    c = Color.GRAY;
                    break;
                case ServerLoggerEvent.ERROR:
                    c = Color.RED;
                    break;
                case ServerLoggerEvent.INFO:
                    c = Color.BLACK;
                    break;
                default:
                    c = Color.BLACK;
            }
            appendToPane(evt.getMessage(), c);
        }

    }

    private void updateClientList() {
        if (listModel == null) {
            listModel = (DefaultListModel) this.jlClientList.getModel();
        }
        listModel.removeAllElements();
        if (server == null || server.clients == null) {
            return;
        }
        int clientNo = 0;
        for (SeraServer.ServerClientResponder t : server.clients) {
            if (t == null) {
                continue;
            }
            clientNo++;
            Date dt = t.getCreationTime().getTime();
            String tmp = new SimpleDateFormat("yy.MM.dd HH:mm:ss").format(dt);
            String str = tmp + " -->    <" + t.getClientID() + ">     " + macToString(null);
            listModel.addElement(str);
        }
        this.trayIcon.setToolTip(clientNo + " makina bağlı");
    }

    private String macToString(int[] data) {
        if (data == null) {
            return "UNKNOWN";
        }
        String str = data[0] + "";
        for (int i = 1; i < data.length; i++) {
            str += "." + data[i];
        }
        return str;
    }

    public void setPortN0(int portN0) {
        if (portN0 != socketProp.getPort()) {
            try {
                socketProp.setPort(portN0);
                ServerConfigProp.saveServerSocketProp(socketProp);
            } catch (IOException ex) {
                Logger.getLogger(ServerMainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
