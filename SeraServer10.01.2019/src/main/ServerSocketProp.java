/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author alptug
 */
public class ServerSocketProp {
    private int port;
    final static String PROP_PORT="s.port";

     ServerSocketProp(){
        resetDefault();
    }
    public int getPort() {
        return port;
    }

    public void setPort(Integer port) {
        if(port!=null)
        this.port = port;
    }

    public void resetDefault() {
        port=5555;
    }
    
}
