package socket;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import com.sun.tools.sjavac.server.SysInfo;
import java.io.BufferedReader;
import data.DataTransferListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import my_logger.L;
import data.DataTransferEvent;
import data.NetworkClientPackage;
import data.NetworkPackage;
import data.NetworkServerPackage;
import db.DBDateRecordInterval;
import java.io.DataInputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Date;
import main.ServerConfigProp;
import main.ServerDbProp;
import socket.listeners.ServerStatuChangeEvent;
import network.ActionCode;
import static network.ActionCode.SENT_PING;
import network.ConnecterType;
import network.GeneralSystemConstant;
import static network.GeneralSystemConstant.SERVER_ID;
import socket.listeners.ServerLoggerEvent;
import util.BasicTime;
import util.ByteConversion;
import socket.listeners.ServerEventListener;

public class SeraServer extends Thread implements ActionCode {

    private final static db.DBDateRecordInterval recordInterval = DBDateRecordInterval.PER_MINUTE;
    private final static String[] EXTERNAL_IP_SORGULAMA_ADDRESI = {
        "http://checkip.amazonaws.com/",
        "http://checkip.amazonaws.com/",
        "http://icanhazip.com/",
        "http://www.trackip.net/ip",
        "http://myexternalip.com/raw",
        "http://ipecho.net/plain",
        "http://bot.whatismyipaddress.com"
    };
    // Server soket
    private ServerSocket serverSocket = null;
    // Client soket
    private Socket clientSocket = null;
    // Maximum bağlantı sayısı
    private final int maxClientSayisi = 20;
    // Her bir client için oluşturlacak Thread dizisi
    public final ServerClientResponder[] clients = new ServerClientResponder[maxClientSayisi];
    private int portNo;
    private InetAddress serverIp;
    private boolean startServer;
    private List<ServerEventListener> serverEventListener = new ArrayList<>();
    private List<DataTransferListener> _transferListener = new ArrayList<>();
    private Map<Short, HashSet<Short>> clientMapping;
    private final static MysqlDataSource dataSource = new MysqlDataSource();
    private static boolean NO_MAPPING;

    public void addDataTransferListener(DataTransferListener l) {
        if (!this._transferListener.contains(l)) {
            this._transferListener.add(l);
        }
    }

    public void addServerEventListener(ServerEventListener l) {
        if (!this.serverEventListener.contains(l)) {
            this.serverEventListener.add(l);
        }
    }

    public boolean removeDataTransferListener(DataTransferListener l) {
        return this._transferListener.remove(l);
    }

    public boolean removeServerEventListener(ServerEventListener l) {
        return this.serverEventListener.remove(l);
    }

    private void fireDataReceived(NetworkPackage pck) {
        DataTransferEvent evt = new DataTransferEvent(this, pck);
        for (DataTransferListener l : this._transferListener) {
            l.dataReceived(evt);
        }
    }

    private void fireDataSent(NetworkPackage pck) {
        DataTransferEvent evt = new DataTransferEvent(this, pck);
        for (DataTransferListener l : this._transferListener) {
            l.dataSent(evt);
        }
    }

    private void fireServerStatuChangedEvent(ServerStatuChangeEvent evt) {
        for (ServerEventListener l : this.serverEventListener) {
            l.serverStatusChanged(evt);
        }
    }

    private void fireServerLoggerEvent(String message, int type) {
        ServerLoggerEvent evt = new ServerLoggerEvent(this, message, type);
        for (ServerEventListener l : serverEventListener) {
            l.log(evt);
        }
    }

    private int dumpDataWithStatement(short idClient, int n) {
        if (!dbConnectionIsValid()) {
            return -1;
        }
        Long date = Calendar.getInstance().getTimeInMillis();
        System.out.println("Try to insert Data" + n);
        long t = 0L;
        int res;
        String sql = "INSERT IGNORE INTO client_action_value(idClient,actionCode,value,createdAt) VALUES(" + idClient + ",";
        Double[] values = {10.0, 12.0, 8.0};
        DecimalFormatSymbols sym = new DecimalFormatSymbols();
        sym.setDecimalSeparator('.');
        DecimalFormat sf = new DecimalFormat("#.##", sym);
        try ( Connection conn = dataSource.getConnection();  Statement st = conn.createStatement();) {
            ResultSet s = st.executeQuery("Select max(createdAt) as dt from client_action_value where idClient=" + idClient);
            if (s.next()) {
                if (s.getLong("dt") != 0) {
                    date = s.getLong("dt");
                }
            }
            date = recordInterval.convert(date);
            conn.setAutoCommit(false);
            t = System.currentTimeMillis();
            int additiontime;
            switch (recordInterval) {
                case PER_5_MINUTES:
                    additiontime = 300000;
                    break;
                case PER_MINUTE:
                    additiontime = 60000;
                    break;
                case PER_SECOND:
                    additiontime = 1000;
                    break;
                default:
                    additiontime = 4000;
            }
            short i;
            short actionCode;
            while (n > 0) {
                n--;
                date += additiontime;
                i = (short) (n % 3);
                values[i] = values[i] + Math.random() - 0.5;
                actionCode = (short) (i + 11);
                st.addBatch(sql + actionCode + "," + sf.format(values[i]) + "," + date + ")");

//                temp++;
//                if (temp < 100 || temp >= 200) {
//                }
            }
//            st.addBatch("UPDATE client SET name='sıcaklık ölçer ev2' WHERE (idClient=1001)");
            int nos[] = st.executeBatch();
            conn.commit();
            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            res = 0;
            for (int no : nos) {
                if (no == 1) {
                    res++;
                }
            }
        } catch (SQLException ex) {
            res = -1;
            ex.printStackTrace();
        } finally {

        }
        System.out.println("Insert comleted :" + res + "\nTotal time :  " + t);
        return res;
    }

    private int dumpDataWithPreparedStatement(short idClient, int n) {
        if (!dbConnectionIsValid()) {
            return -1;
        }
        short actionCode = 14;
        Double value = 10.0;
        Long date = Calendar.getInstance().getTimeInMillis();
        System.out.println("Try to insert Data" + n);
        long t = 0L;
        int res;
        String sql = "INSERT IGNORE INTO client_action_value(idClient,actionCode,value,createdAt) VALUES(?,?,?,?)";

        try ( Connection conn = dataSource.getConnection();  PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
            t = System.currentTimeMillis();
            int temp = 0;
            while (n > 0) {
                n--;
                pstm.setShort(1, idClient);
                pstm.setShort(2, actionCode);
                pstm.setDouble(3, value);
                pstm.setLong(4, date);
                pstm.addBatch();
//                temp++;
//                if(temp<100 || temp>=200)
                date += 4000;
            }
            int nos[] = pstm.executeBatch();
            conn.commit();
            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            res = 0;
            for (int no : nos) {
                if (no == 1) {
                    res++;
                }
            }
        } catch (SQLException ex) {
            res = -1;
            ex.printStackTrace();
        } finally {

        }
        System.out.println("Insert comleted :" + res + "\nTotal time :  " + t);
        return res;
    }

    public SeraServer(int portNo) throws IOException {
        loadDataSource();
//        long n = 0;
//        for (int i = 0; i < 10; i++) {
//            for (short actionCode = 1001; actionCode < 1011; actionCode++) {
//                n += this.dumpDataWithStatement(actionCode, 10000);
//            }
//        }
//        if (n > 0) {
//            return;
//        }

        this.clientMapping = new HashMap<Short, HashSet<Short>>();
        if (createClientMappingFromDb()) {
            NO_MAPPING = true;
        } else if (createClientMappingFromFile()) {
            NO_MAPPING = true;
        } else {
            NO_MAPPING = false;
        }
        this.portNo = portNo;
        try {
            this.serverIp = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            this.serverIp = null;
            print("Server ip bulunamadi", ServerLoggerEvent.ERROR);
        }

    }

    public static void loadDataSource() throws FileNotFoundException, IOException {
        ServerDbProp p = ServerConfigProp.getServerDbProp();
        dataSource.setURL(p.getUrl());
        dataSource.setUser(p.getUserName());
        dataSource.setPassword(p.getPassword());
    }

    @Override

    public synchronized void start() {

        try {
            if (this.serverIp != null) {
                serverSocket = new ServerSocket();
                serverSocket.bind(new InetSocketAddress(this.serverIp, portNo));
            } else {
                serverSocket = new ServerSocket(portNo);
            }
        } catch (IOException ex) {
            print("Server Error : " + ex.getMessage(), ServerLoggerEvent.ERROR);
            this.startServer = false;
            try {
                this.join();
            } catch (InterruptedException ex1) {
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex1);
            }
            return;
        }
        String extIp = getExternalIP();
        print("Server basladi : " + extIp + " \\ " + portNo + "", ServerLoggerEvent.INFO);

        this.startServer = true;
        ServerStatuChangeEvent obj = new ServerStatuChangeEvent(this, ServerStatuChangeEvent.SERVER_CONNECTED);
        this.fireServerStatuChangedEvent(obj);
        super.start(); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean isWorking() {
        return startServer;
    }

    public synchronized void terminate() {
        if (!this.startServer) {
            return;
        }
        this.startServer = false;
        for (ServerClientResponder t : this.clients) {
            if (t != null) {
                t.closeConnection();
            }
        }
        try {
            this.serverSocket.close();
            this.serverSocket = null;
        } catch (IOException ex) {
            print("Server duzgun kapatilamadi", ServerLoggerEvent.ERROR);
        }
        ServerStatuChangeEvent obj = new ServerStatuChangeEvent(this, ServerStatuChangeEvent.SERVER_DISCONNECTED);
        this.fireServerStatuChangedEvent(obj);
    }

    public int getClientIndex(ServerClientResponder cr) {
        for (int i = 0; i < maxClientSayisi; i++) {
            if (this.clients[i] != null && this.clients[i] == cr) {
                return i;
            }
        }
        return -1;
    }

    public int getClientCount() {
        int count = 0;
        for (int i = 0; i < maxClientSayisi; i++) {
            if (this.clients[i] != null) {
                count++;
            }
        }
        return count;
    }

    @Override
    public void run() {
        /*
         * Her bir client için ayrı soketler ve threadlerin oluşturulması
         */

        while (this.startServer) {
            while (this.getClientCount() >= this.maxClientSayisi) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                clientSocket = serverSocket.accept();

                int i = 0;
                ServerClientResponder t = new ServerClientResponder(clientSocket, clients);
                t.start();
                this.addClientToList(t);
                if (this.getClientCount() == this.maxClientSayisi) {
                    print("Server maksimum kapasiteye ulasti.", ServerLoggerEvent.INFO);
                }

            } catch (IOException e) {
                print(e.getMessage(), ServerLoggerEvent.ERROR);
                break;
            }
        }
        this.startServer = false;
    }

    private void removeClientFromList(ServerClientResponder t) {
        boolean removed = false;
        for (int i = 0; i < this.maxClientSayisi; i++) {
            if (this.clients[i] != null && this.clients[i] == t) {
                removed = true;
                this.clients[i] = null;
            }
        }
        if (!removed) {
            return;
        }
        ServerStatuChangeEvent evt = new ServerStatuChangeEvent(this, ServerStatuChangeEvent.CLIENT_DISCONNECTED);
        this.fireServerStatuChangedEvent(evt);
    }

    private void addClientToList(ServerClientResponder t) {
        for (int i = 0; i < maxClientSayisi; i++) {
            if (clients[i] == null) {
                clients[i] = t;
                break;
            }
        }
        ServerStatuChangeEvent evt = new ServerStatuChangeEvent(this, ServerStatuChangeEvent.CLIENT_CONNECTED);
        this.fireServerStatuChangedEvent(evt);
    }

    public void print(String s, int type) {
        String str = BasicTime.getCurrentLongTime() + "-->" + s;
        if (type == ServerLoggerEvent.ERROR) {
            System.err.println(str);
        } else {
            System.out.println(str);
        }
        this.fireServerLoggerEvent(s, type);
    }

    private String getExternalIP() {
        URL whatismyip;
        String ip;
        int i = -1;
        while (i < EXTERNAL_IP_SORGULAMA_ADDRESI.length) {
            i++;
            ip = null;
            whatismyip = null;
            try {
                whatismyip = new URL(EXTERNAL_IP_SORGULAMA_ADDRESI[i]);
                BufferedReader in;
                try {
                    in = new BufferedReader(new InputStreamReader(
                            whatismyip.openStream()));

                    ip = in.readLine(); //you get the IP as a String
                    System.out.println(ip);
                } catch (IOException ex) {
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                    continue;
                }

            } catch (MalformedURLException ex) {
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                continue;
            }
            return ip;
        }
        print("Ip adresi bulunamadi", ServerLoggerEvent.ERROR);
        return null;
    }

    public boolean dbConnectionIsValid() {
        String sql = "Select 1 From client";
        boolean isConnected = false;
        try ( Connection conn = dataSource.getConnection();  Statement st = conn.createStatement();  ResultSet rs = st.executeQuery(sql)) {
            if (rs != null) {
                rs.next();
                isConnected = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isConnected;
    }

    public boolean createClientMappingFromDb() {
        if (!this.dbConnectionIsValid()) {
            return false;
        }
        String sql = "Select idClient,idPermittedClient from clientPermissions";
        try ( Connection conn = dataSource.getConnection();  Statement st = conn.createStatement();  ResultSet rs = st.executeQuery(sql);) {
            while (rs.next()) {
                Short id = rs.getShort(1);
                Short id2 = rs.getShort(2);
                HashSet<Short> set;
                if (this.clientMapping.containsKey(id)) {
                    set = this.clientMapping.get(id);
                    set.add(id2);
                } else {
                    set = new HashSet<Short>();
                    set.add(id2);
                    this.clientMapping.put(id, set);
                }
                if (this.clientMapping.containsKey(id2)) {
                    set = this.clientMapping.get(id2);
                    set.add(id);
                } else {
                    set = new HashSet<Short>();
                    set.add(id);
                    this.clientMapping.put(id2, set);
                }
            }
            print("Database Client Mapping tamamlandi", ServerLoggerEvent.INFO);
            return true;
        } catch (SQLException ex) {
            print("Database Client Mapping hata oluştu:\n" + ex.getMessage(), ServerLoggerEvent.ERROR);
            ex.printStackTrace();
            return false;
        }
    }

    public final boolean createClientMappingFromFile() {
//        String fileName = "C:\\Users\\alptug\\Desktop\\test.txt";
        File file = new File(ServerConfigProp.clientMappingFileLocation);
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String str;
            int index = 0;
            while ((str = br.readLine()) != null) {
                index++;
                String data[] = str.split(" ");
                if (data.length != 2) {
                    print("File client Mapping hatasi data uzunlugu eksik index : " + index, ServerLoggerEvent.ERROR);
                    continue;
                }
                try {
                    Short id = Short.parseShort(data[0]);
                    Short id2 = Short.parseShort(data[1]);
                    HashSet<Short> set;
                    if (this.clientMapping.containsKey(id)) {
                        set = this.clientMapping.get(id);
                        set.add(id2);
                    } else {
                        set = new HashSet<Short>();
                        set.add(id2);
                        this.clientMapping.put(id, set);
                    }
                    if (this.clientMapping.containsKey(id2)) {
                        set = this.clientMapping.get(id2);
                        set.add(id);
                    } else {
                        set = new HashSet<Short>();
                        set.add(id);
                        this.clientMapping.put(id2, set);
                    }

                } catch (NumberFormatException ex) {
                    print("File ClientMapping hatasi index " + index + ":\n" + ex.getMessage(), ServerLoggerEvent.ERROR);
                    continue;
                }
            }
        } catch (FileNotFoundException ex) {
            print("File ClientMapping hatasi File not Found : " + ServerConfigProp.clientMappingFileLocation, ServerLoggerEvent.ERROR);
            return false;
        } catch (IOException ex) {
            print("File ClientMapping hatasi File can not parse : " + ServerConfigProp.clientMappingFileLocation, ServerLoggerEvent.ERROR);
            return false;
        }
        print("File ClientMapping tamalandi", ServerLoggerEvent.INFO);
        return true;
    }

    private boolean isValidClientID(short id) {
        if (NO_MAPPING || clientMapping.containsKey(id)) {
            return true;
        }
        return false;
    }

    public Set<Integer> getClientIndexesById(final ServerClientResponder source, Set<Short> ids, final ServerClientResponder[] list) {
        Set<Integer> focusIndex = new HashSet<Integer>();
        for (Short nextClientId : ids) {
            for (int i = 0; i < list.length; i++) {
                if (list[i] != null && list[i] != source && list[i].getClientID().compareTo(nextClientId) == 0) {
                    focusIndex.add(i);
                }
            }
        }
        return focusIndex;
    }

    public Set<Integer> getFocusClientIndexesByMap(final ServerClientResponder source, final ServerClientResponder[] list) {
        // TODO Auto-generated method stub
        Set<Short> focusIdSet = this.clientMapping.get(source.getClientID());

        return this.getClientIndexesById(source, focusIdSet, list);
    }

    public class ServerClientResponder extends Thread {

        public final Calendar creationTime;
        private DataInputStream dis = null;
        private PrintStream ps = null;
        private Socket clientSocket = null;
        private final ServerClientResponder[] threads;
        private Short clientID;
        private long lastReceived, lastSent;
        String name;
        Short clientType;
        // private byte connectionId;

        ServerClientResponder(Socket clientSocket, ServerClientResponder[] threads) throws IOException {
            this.creationTime = Calendar.getInstance();
            this.clientSocket = clientSocket;
            this.threads = threads;
            this.lastReceived = System.currentTimeMillis();
            this.lastSent = System.currentTimeMillis();
        }

        @Deprecated
        public Short requestClientID() {
            final long MAX_PROCESS_TIME = 60000;

            int pos = 0;
            final int ID_ISTE = 0;
            final int READ_ID = 1;
            final int CHECK_ID = 2;
            final int SEND_ID = 3;
            final int FINISH_PROCESS = 4;
            int errorCount = 0;
            pos = READ_ID;
            try {
                while (dis != null && dis.available() > 0) {
                    dis.read();
                }
            } catch (IOException ex) {
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            }
            short id = GeneralSystemConstant.ERROR_VALUE.shortValue();
            long startProcess = System.currentTimeMillis();
            while (System.currentTimeMillis() - startProcess < MAX_PROCESS_TIME) {
                try {
                    //client id iste
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                }
                switch (pos) {
                    case ID_ISTE: {
                        SeraServer.this.print("Client Id  iste", ServerLoggerEvent.INFO);
                        try {
                            ps.write(ByteConversion.fromShort(ActionCode.REQUEST_CLIENT_TYPE, false));
                            ps.flush();
                            pos = READ_ID;
                            errorCount = 0;
                        } catch (IOException ex) {
                            SeraServer.this.print("Request CLient type istenemedi", ServerLoggerEvent.ERROR);
                            errorCount++;
                            pos = ID_ISTE;
                        }
                        break;
                    }
                    case READ_ID: {
                        if (errorCount > 10) {
                            pos = ID_ISTE;
                            errorCount = 0;
                            break;
                        }
                        try {
                            //type oku
                            if (dis != null && dis.available() > 0) {
                                SeraServer.this.print("Client Id oku : ", ServerLoggerEvent.INFO);
                                id = dis.readShort();
                                SeraServer.this.print("Client Id  : " + id, ServerLoggerEvent.INFO);
                                pos = CHECK_ID;
                                errorCount = 0;
                                break;
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);

                        }
                        pos = READ_ID;
                        errorCount++;
                        break;
                    }
                    case CHECK_ID: {
                        if (errorCount > 10) {
                            pos = ID_ISTE;
                            errorCount = 0;
                            break;
                        }
                        //id sistemde var mi kontrol et
                        SeraServer.this.print("Client Id  check : ", ServerLoggerEvent.INFO);

                        //type yanlissa bi daha istemek icin basa git
                        if (!SeraServer.this.isValidClientID(id)) {
                            SeraServer.this.print("Kayitli id bulunamadi : ", ServerLoggerEvent.INFO);
                            pos = ID_ISTE;
                            errorCount = 0;
                            break;
                        }
                        pos = SEND_ID;
                        errorCount = 0;
                        break;
                    }
                    case SEND_ID: {
                        if (errorCount > 10) {
                            errorCount = 0;
                            pos = ID_ISTE;
                            break;
                        }
                        SeraServer.this.print("Kayitli id var.Geri Gonder Kontrol icin", ServerLoggerEvent.INFO);
                        try {
                            if (ps != null) //type gonder
                            {
                                ps.write(ByteConversion.fromShort(id, false));
                                ps.flush();
                                pos = FINISH_PROCESS;
                                errorCount = 0;
                                break;
                            } else {
                                SeraServer.this.print("Print Stream null", ServerLoggerEvent.ERROR);
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        errorCount++;
                        break;
                    }
                    case FINISH_PROCESS: {
                        if (errorCount > 10) {
                            errorCount = 0;
                            pos = SEND_ID;
                            break;
                        }
                        try {
                            //gelen bilgiyi oku
                            SeraServer.this.print("yeni bilgiyi oku: ", ServerLoggerEvent.INFO);
                            if (dis != null && dis.available() > 0) {
                                short newValue = dis.readShort();
                                System.out.println(newValue);
                                //gelen bilgi ID_OK ISE islemi tamamla
                                if (newValue == ActionCode.CLIENT_TYPE_OK) {
                                    return id;
                                }
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);

                        }
                        pos = FINISH_PROCESS;
                        errorCount++;
                    }
                }

            }
            SeraServer.this.print("Header atanamadi ", ServerLoggerEvent.ERROR);
            return null;
        }

        public Short requestClientID2() {
            System.out.println("asdasd");
            final long MAX_PROCESS_TIME = 60000;

            final long startProcess = System.currentTimeMillis();
            String str = "";
            boolean bas = false, son = false;
            boolean init = true;
            while (System.currentTimeMillis() - startProcess < MAX_PROCESS_TIME) {
                if (init) {
                    bas = false;
                    son = false;
                    str = "";
                    init = false;
                }
                try {

                    while (!son && dis.available() > 0) {
                        System.err.print(".");
                        char c = (char) dis.read();
                        System.err.print(c);
                        if (c == '<') {
                            bas = true;
                            continue;
                        }
                        if (bas) {
                            if (c == '>') {
                                son = true;
                            } else {
                                str += c;
                            }
                        }

                    }
                } catch (IOException ex) {
                    System.err.println(ex.getMessage());
                    init = true;
                    continue;
                }
                String[] val = str.split(" ");
                if (val.length < 2) {
                    init = true;
                    continue;
                }
                try {
                    short act = Short.parseShort(val[0]);
                    short id = Short.parseShort(val[1]);
                    if (act == ActionCode.AC_SEND_ID) {
                        //ilk gelen ise bir daha istek gonder dogrumu kontrol edilsin
                        if (this.clientID == null) {
                            this.clientID = id;
                            SeraServer.this.print("Client Id okundu " + id, ServerLoggerEvent.INFO);
                            ps.println("<" + act + ">");
                            ps.flush();
                        } else {
                            //ikinci gelen ise ilk ile ayni mi diye kontrol et
                            if (this.clientID == id && SeraServer.this.isValidClientID(this.clientID)) {
                                ps.println("<" + act + " " + this.clientID + ">");
                                ps.flush();
                                SeraServer.this.print("Client Id dogrulandi " + id, ServerLoggerEvent.INFO);
                                return this.clientID;
                            } else {
                                SeraServer.this.print("Client id dogrulanamadi " + this.clientID + "," + id, ServerLoggerEvent.ERROR);
                                init = true;
                            }
                        }

                    }
                } catch (Exception ex) {
                    System.err.println(ex);
                    init = true;
                    continue;
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            SeraServer.this.print("Client id alinamadi", ServerLoggerEvent.ERROR);
            return this.clientID = null;
        }

        public Short requestClientID3() {
            final long MAX_PROCESS_TIME = 75000;

            final long startProcess = System.currentTimeMillis();
            String str = "";
            boolean bas = false, son = false;
            boolean init = true;
            long lastSentTime = 0;
            final long WAIT_MS_TO_SENT = 2000;
            while (System.currentTimeMillis() - startProcess < MAX_PROCESS_TIME) {
                if (System.currentTimeMillis() - lastSentTime > WAIT_MS_TO_SENT) {
                    lastSentTime = System.currentTimeMillis();
                    ps.println("<" + AC_ID_REQUESTED + ">");
                    ps.flush();
                }
                if (init) {
                    bas = false;
                    son = false;
                    str = "";
                    init = false;
                }
                try {

                    while (!son && dis.available() > 0) {
                        char c = (char) dis.read();
                        if (c == '<') {
                            bas = true;
                            continue;
                        }
                        if (bas) {
                            if (c == '>') {
                                son = true;
                            } else {
                                str += c;
                            }
                        }

                    }
                } catch (IOException ex) {
                    System.err.println(ex.getMessage());
                    init = true;
                    continue;
                }
                String[] val = str.split(" ");
                if (val.length < 2) {
                    init = true;
                    continue;
                }
                try {
                    short act = Short.parseShort(val[0]);
                    short id = Short.parseShort(val[1]);
                    if (act == ActionCode.AC_SEND_ID) {
                        //ilk gelen ise bir daha istek gonder dogrumu kontrol edilsin                       
                        this.clientID = id;
                        SeraServer.this.print("Client Id okundu " + id, ServerLoggerEvent.INFO);
                        for (int j = 0; j < 3; j++) {
                            try {
                                Thread.sleep(1000);
                                ps.println("<" + AC_ID_ACCEPTED + ">");
                                ps.flush();
                            } catch (Exception m) {
                            }
                        }
                        return id;
                    } else {
                        SeraServer.this.print("Geçersiz Action Code : " + act, ServerLoggerEvent.ERROR);
                    }
                } catch (Exception ex) {
                    System.err.println(ex);
                    init = true;
                    continue;
                }
                try {
                    Thread.sleep(20);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            SeraServer.this.print("Client id alinamadi", ServerLoggerEvent.ERROR);
            return this.clientID = null;
        }

        public Calendar getCreationTime() {
            return creationTime;
        }

        @Override
        public void run() {

            try {
                dis = new DataInputStream(clientSocket.getInputStream());

                // DataOutputStream dos = new DataOutputStream(null);
                ps = new PrintStream(clientSocket.getOutputStream());
                //wait for client id 

                this.clientID = this.requestClientID();
                if (this.clientID == null) {
                    this.closeConnection();
                    return;
                }
                data.NetworkClientPackage pckClient = new NetworkClientPackage();

                data.NetworkServerPackage pckServer = new NetworkServerPackage();
                SeraServer.this.print("Yeni kullanici< id:" + this.clientID + " type : " + ConnecterType.getTypeById(this.clientID) + ">", ServerLoggerEvent.INFO);
                this.clientSocket.setKeepAlive(true);
                pckClient.setAction(ActionCode.CLIENT_CONNECTED);
                pckClient.setSender(GeneralSystemConstant.SERVER_ID);

                for (Integer i : SeraServer.this.getFocusClientIndexesByMap(this, this.threads)) {
                    SeraServer.this.print("<" + threads[i].getClientID() + ">", ServerLoggerEvent.INFO);
                    threads[i].sentLine(pckClient);
                }

//            this.lastSent = System.currentTimeMillis();
                this.lastReceived = System.currentTimeMillis();
                while (true) {
                    if (this.clientSocket.isInputShutdown() || this.clientSocket.isOutputShutdown()
                            || this.clientSocket.isClosed()) {
                        break;
                    }

                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (System.currentTimeMillis() - this.lastSent > GeneralSystemConstant.PING_WAIT_MS) {

                        if (this.ps != null) {
                            pckClient = new NetworkClientPackage();
                            pckClient.setSender(GeneralSystemConstant.SERVER_ID);
                            pckClient.setAction(ActionCode.AC_SEND_BOARD_LAST_RESET_TIME);
                            this.sentLine(pckClient);
                        }
                    }
                    if ((System.currentTimeMillis() - this.lastReceived) > 3 * GeneralSystemConstant.PING_WAIT_MS) {
                        SeraServer.this.print("Client dan data gelmedi Client kapatiliyor", ServerLoggerEvent.INFO);
                        break;
                    }
                    pckServer = this.getLine();

                    if (pckServer == null) {
                        continue;
                    }

                    java.util.Date ct = new Date();
                    if (ConnecterType.getTypeById(this.getClientID()) == ConnecterType.PROVIDER) {

                        switch (pckServer.getAction()) {
                            case ActionCode.AC_SEND_TEMP1:
                            case ActionCode.AC_SEND_TEMP2:
                            case ActionCode.AC_SEND_TEMP3: {
                                double data = pckServer.getData() != null && !pckServer.getData().isEmpty() ? pckServer.getData().get(0) : GeneralSystemConstant.ERROR_VALUE;
                                this.saveToDb(this.getClientID(), pckServer.getAction(), data, ct.getTime());
                                break;
                            }
                            case ActionCode.AC_SEND_BOARD_LAST_RESET_TIME:
                                try {
                                long data = pckServer.getData().get(0).longValue();
                                SeraServer.this.print("Exec. Time in sec: " + data / 1000, ServerLoggerEvent.INFO);
                            } catch (Exception e) {
                                SeraServer.this.print("Exec time in sec Error value", ServerLoggerEvent.ERROR);
                            }
                            break;
                            default:
                                SeraServer.this.print("Invalid action Code : " + pckServer.getAction(), ServerLoggerEvent.ERROR);
                                break;
                        }
                    }
                    Set idList = pckServer.getReceiverList();

                    //Server a istek gonderilmis mi ?
                    if (idList != null && idList.contains(GeneralSystemConstant.SERVER_ID)) {
                        L.debug("server'a istek gönderilmesi durumunda");
                        this.replyNetworkPackage(pckServer);
                    } else {

                        pckClient = transformToClientData(pckServer);
                        pckClient.setCreationTime(ct);
                        //target client id yok sistem dosyadan targetlari bulup gonderir
                        if (pckServer.getReceiverList() == null || pckServer.getReceiverList().isEmpty()) {
                            L.debug("target client id yok");
                            for (Integer i : getFocusClientIndexesByMap(this, this.threads)) {
                                threads[i].sentLine(pckClient);

                            }
                        } else {
                            L.debug("client tarafindan istenilen id");
                            for (Integer i : getClientIndexesById(this, pckServer.getReceiverList(), clients)) {
                                threads[i].sentLine(pckClient);
                            }
                        }

                    }
                }
            } catch (IOException e) {
                SeraServer.this.print(e.getMessage(), ServerLoggerEvent.ERROR);
            } finally {
                this.closeConnection();
            }
        }

        private int saveToDb(short idClient, short actionCode, double value, long date) {
            if (!dbConnectionIsValid()) {
                return -1;
            }
            int res;
            String sql = "Insert into client_action_value(idClient,actionCode,value,createdAt) VALUES(?,?,?,?) on duplicate key update client_action_value.value= least(?,value)";
            System.out.println("idClient : " + idClient + ", actionCode:" + actionCode + ", value:" + value + ", date:" + date);
            try ( Connection conn = dataSource.getConnection();  PreparedStatement pstm = conn.prepareStatement(sql);) {
                pstm.setShort(1, idClient);
                pstm.setShort(2, actionCode);
                pstm.setDouble(3, value);
                pstm.setLong(4, recordInterval.convert(date));
                pstm.setDouble(5, value);
                res = pstm.executeUpdate();
            } catch (SQLException ex) {
                SeraServer.this.print("Database Kayit Hatasi:\n" + ex.getMessage(), ServerLoggerEvent.ERROR);
                res = -1;
                ex.printStackTrace();
            }
            return res;
        }

        private NetworkServerPackage getLine() throws IOException {

            if (dis != null && dis.available() > 0) {
                String str = dis.readLine();
                NetworkServerPackage s = new NetworkServerPackage();
                s.setNetworkData(str);
                s.setSender(this.getClientID());
                SeraServer.this.fireDataReceived(s);
                this.lastReceived = System.currentTimeMillis();
                L.debug(s.getNetworkData());
                return s;
            }
            return null;
        }

        private synchronized void sentLine(NetworkClientPackage pck) {
            pck.setReceiver(clientID);
            ps.println(pck.getNetworkData());
            ps.flush();
            this.lastSent = System.currentTimeMillis();

            fireDataSent(pck);
        }

        public void closeConnection() {
            /*
             * Yeni bir Clientın bağlanabilmesi için aktif olan Client null yapılır
             */
            SeraServer.this.print(this.clientID + " Gule Gule!", ServerLoggerEvent.INFO);
            removeClientFromList(this);
            try {
                dis.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                SeraServer.this.print("Connection closed da input Stream kapatma hatasi :\n" + e.getMessage(), ServerLoggerEvent.ERROR);
            }

            ps.close();
            try {
                clientSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                SeraServer.this.print("Connection closed da clientSocket kapatma hatasi : \n" + e.getMessage(), ServerLoggerEvent.ERROR);
            }
        }

        /**
         * @return the threadConnectionType
         */
        public Short getClientID() {
            return clientID;
        }

        private NetworkClientPackage transformToClientData(NetworkServerPackage pckServer) {
            NetworkClientPackage cl = new NetworkClientPackage();
            cl.setAction(pckServer.getAction());
            for (Double d : pckServer.getData()) {
                cl.addData(d);
            }
            cl.setSender(pckServer.getSender());
            return cl;

        }

        private void replyNetworkPackage(NetworkServerPackage pckServer) {
            NetworkClientPackage pck = new NetworkClientPackage();
            switch (pckServer.getAction()) {
                case SENT_PING:
                    //do nothing
                    break;
                case REQUEST_CLIENT_TYPE: {
                    pck.setSender(SERVER_ID);
                    pck.setCreationTime(new Date());
                    pck.setReceiver(this.getClientID());
                    pck.setAction(REQUEST_CLIENT_TYPE);
                    for (Short id : SeraServer.this.clientMapping.get(this.getClientID())) {
                        pck.addData(id.doubleValue());
                    }
                    break;
                }
                default:
                //do nothing
            }
            this.sentLine(pck);
        }
    }

}


/*
 * Client soketler için oluşturulmuş Thread sınıfı Her bir Client ile Thread
 * eşleştirilmesi ile Multithread uygulama
 */
