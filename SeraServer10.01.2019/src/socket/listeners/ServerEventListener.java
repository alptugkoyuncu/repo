/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket.listeners;

/**
 *
 * @author WIN7
 */
public interface ServerEventListener {
    public void serverStatusChanged(ServerStatuChangeEvent evt);
    public void log(ServerLoggerEvent evt);
}
