/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket.listeners;

import java.util.Date;
import java.util.EventObject;
import util.BasicTime;

/**
 *
 * @author WIN7
 * Program içindeki basit eksiklikleri,önemli bilgileri göstermek için kullanılacak mesela dosya bulunamadıgında.
 * Daha detaylılar ıcın javanın Logger.log kullanılacak
 * 
 */
public class ServerLoggerEvent extends EventObject{
    public final static int INFO=1;
    public final static int DEBUG=2;
    public final static int ERROR=3;
    
    public int type;
    public String message;
    public java.util.Date eventTime;
    public int getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }
    public ServerLoggerEvent(Object source,String msg,int type) {
        super(source);
        this.message = msg;
        if(type>3 || type<=0)
            this.type=1;
        else
            this.type = type;
        eventTime = new Date();
        
    }

    public Date getEventTime() {
        return eventTime;
    }
}
