/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket.listeners;

/**
 *
 * @author WIN7
 */
public class ServerPrintEvent extends ServerStatuChangeEvent{
    public final static int LOG=1;
    public final static int ERROR=2;
    public final static int DEBUG=3;
    public int type;
    public String data;

    public int getType() {
        return type;
    }

    public String getData() {
        return data;
    }
    public ServerPrintEvent(Object source,int type,String data) {
        super(source,ServerStatuChangeEvent.PRINT);
        this.data = data;
        if(type>3 || type<=0)
            this.type=1;
        else
            this.type = type;
        
    }
    
}
