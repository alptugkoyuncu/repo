/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket.listeners;

import java.util.EventObject;

/**
 *
 * @author WIN7
 */
public class ServerStatuChangeEvent extends EventObject {
    private int connectionStatu;
    public final static int SERVER_CONNECTED = 1;
    public final static int SERVER_DISCONNECTED=2;
    public final static int CLIENT_CONNECTED = 3;
    public final static int CLIENT_DISCONNECTED=4;
    public final static int PRINT=5;
    public ServerStatuChangeEvent(Object source) {
        super(source);
    }

    public int getConnectionStatu() {
        return connectionStatu;
    }

    public ServerStatuChangeEvent(Object source,int connectionStatu) {
        super(source);
        this.connectionStatu = connectionStatu;
    }
    
}
