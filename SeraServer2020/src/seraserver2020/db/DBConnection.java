package seraserver2020.db;

import data.ActionCode;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ISIK
 */
public abstract class DBConnection{
    private final static long PER_SECOND=1000;
    private final static long PER_MINUTE=PER_SECOND*60;
    private final static long PER_5_MINUTE=PER_MINUTE*5;
    private final static long SENSOR_INSERT_INTERVAL= PER_5_MINUTE;
    
    protected java.sql.Date getServerDate(int actionCode,java.util.Date dt){
        Long val = dt.getTime();
        if(ActionCode.isSensorActionCode(actionCode)){
            val = val/SENSOR_INSERT_INTERVAL;
            val = val*SENSOR_INSERT_INTERVAL;
        }
        return new java.sql.Date(val);
    }
    /**
     * action code a gore database e kaydedilip kaydedilmeyecegi kontrol edilecek
     * @param actionCode
     * @return 
     */
    protected boolean isActionCodeRecordable(int actionCode){
        return !ActionCode.isEspActionCode(actionCode) &&!ActionCode.isConnectionCode(actionCode) && !ActionCode.isPing(actionCode);
    }
    protected abstract void connect();
    public abstract boolean isValid();
    public abstract int insertClientActionList(Integer[] hostId,Integer[] targetId,Integer[] actionCode,String[] value,java.util.Date[] dt);
    public abstract int insertClientActionList(Integer hostId,Integer targetId,Integer actionCode,String value,java.util.Date dt);

//    public abstract int deleteClientActionList();
    public abstract int insertClientList(Integer[] clientId);
    public abstract int insertClientList(Integer clientId);
    public abstract  int removeClientList(Integer clientId);
    public abstract int removeClientList(Integer[] clientId);
    
    public abstract int insertClientMapping(Map<Integer,Set<Integer>> map);
    public abstract int insertClientMapping(Integer client_id,Set<Integer> friendId);
    public abstract int removeClientMapping(Integer hostId,Integer targetId);
    public abstract int removeClientMapping(Integer[] hostId,Integer[] targetId );
    
    public abstract Map<Integer,Set<Integer>> getClientMapping();
    
    public abstract List<Integer> getClientList();
    
    
    public abstract void disconnect();
}
