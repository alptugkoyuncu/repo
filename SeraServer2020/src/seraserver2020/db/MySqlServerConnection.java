/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.db;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import seraserver2020.settings.SeraServerDBSettings;
import settings.ServerDBSettings;

/**
 *
 * @author ISIK
 */
public class MySqlServerConnection extends DBConnection {

    private final MysqlDataSource dataSource;
    boolean connectionError;
    public MySqlServerConnection() {
        dataSource = new MysqlDataSource();
        ServerDBSettings serverSettings = new SeraServerDBSettings();   
        dataSource.setURL(serverSettings.getUrl());
        dataSource.setUser(serverSettings.getUserName());
        dataSource.setPassword(serverSettings.getPassword());
    }

    @Override
    protected void connect() {
    }

    @Override
    public int insertClientActionList(Integer[] hostId, Integer[] targetId, Integer[] actionCode, String[] value, Date[] dt) {
        boolean flag = (hostId.length == targetId.length) && (hostId.length == actionCode.length) && actionCode.length == value.length && value.length == dt.length;

        if (!flag) {
            return -1;
        }
        int size = hostId.length;
        int index = 0;
        System.out.println("Try to insert Database size : " + size);
        int res = 0;
        String sql = "INSERT IGNORE INTO client_action_list(host_id,target_id,action_code,value,created_at) VALUES(?,?,?,?,?)";
        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
            t = System.currentTimeMillis();
            while (size > index) {
                if (this.isActionCodeRecordable(actionCode[index]) && hostId[index] != null && targetId[index] != null && actionCode[index] != null && dt[index] != null) {
                    pstm.setInt(1, hostId[index]);
                    pstm.setInt(2, targetId[index]);
                    pstm.setInt(3, actionCode[index]);
                    pstm.setString(4, value[index]);
                    pstm.setLong(5, getServerDate(actionCode[index], dt[index]).getTime());
                    pstm.addBatch();
                }
                index++;
            }
            int nos[] = pstm.executeBatch();
            conn.commit();
            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            res = 0;
            for (int no : nos) {
                if (no == 1) {
                    res++;
                }
            }
        } catch (SQLException ex) {
            t = System.currentTimeMillis() - t;
            res = -1;
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            System.out.println("Insert comleted :" + res + "\nTotal time :  " + t);
            return res;
        }

    }

    @Override
    public int insertClientActionList(Integer hostId, Integer targetId, Integer actionCode, String value, Date dt) {
        return this.insertClientActionList(new Integer[]{hostId}, new Integer[]{targetId}, new Integer[]{actionCode}, new String[]{value}, new Date[]{dt});
    }

    @Override
    public int insertClientList(Integer[] clientId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insertClientList(Integer clientId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int removeClientList(Integer clientId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int removeClientList(Integer[] clientId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insertClientMapping(Integer clientId, Set<Integer> friendId) {
        Map<Integer,Set<Integer>> map = new HashMap<>();
        map.put(clientId, friendId);
        return insertClientMapping(map);
    }

    @Override
    public int insertClientMapping(Map<Integer,Set<Integer>> map) {
        if(map==null || map.isEmpty())
            return 0;
        System.out.println("Try to insert Database size : ");
        int res = 0;
        String sql = "INSERT IGNORE INTO client_mapping(client_id,friend_id) VALUES(?,?)";
        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
            t = System.currentTimeMillis();
            for(Integer clientId : map.keySet()){
                Set<Integer> mapIds = map.get(clientId);
                if(mapIds==null)
                    continue;
                for(Integer friendId : mapIds){
                    pstm.setInt(1, clientId);
                    pstm.setInt(2, friendId);
                    pstm.addBatch();
                }
            }
            int nos[] = pstm.executeBatch();
            conn.commit();
            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            res = 0;
            for (int no : nos) {
                if (no == 1) {
                    res++;
                }
            }
        } catch (SQLException ex) {
            t = System.currentTimeMillis() - t;
            res = -1;
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            System.out.println("Insert comleted :" + res + "\nTotal time :  " + t);
            return res;
        }
    }

    @Override
    public int removeClientMapping(Integer hostId, Integer targetId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int removeClientMapping(Integer[] hostId, Integer[] targetId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void disconnect() {
        try {
            dataSource.getConnection().close();
        } catch (SQLException ex) {
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean isValid() {
        try {
            Connection conn = dataSource.getConnection();
            return conn.isValid(10) || !conn.isClosed();
        } catch (SQLException ex) {
            System.err.println("Connection is broken");
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public Map<Integer,Set<Integer>> getClientMapping(){
        String sql = "Select client_id,friend_id from client_mapping order by client_id asc";
        Map<Integer,Set<Integer>> clientMapping = new TreeMap<>();
        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(sql);
            Integer currentId=-1;
            Set<Integer> list= null;
            while(set.next()){
                int id1=set.getInt("client_id");
                if(id1!=currentId){
                    list = new HashSet<>();
                    clientMapping.put(id1, list);
                    currentId=id1;
                }
                int id2=set.getInt("friend_id");
                list.add(id2);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
            
        }finally{
            return clientMapping;
        }
    }
    
    @Override
    public List<Integer> getClientList() {
        String sql = "Select id from client_list";
        List<Integer> ids =new ArrayList<>();
        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(sql);
            while(set.next()){
                ids.add(set.getInt("id"));
            }
            return ids;
        } catch (SQLException ex) {
//            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Integer truncateClientMapping() {
        String sql = "TRUNCATE `sera_kontrol`.`client_mapping`";
        Integer index=null;
        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            index = st.executeUpdate(sql);
        } catch (SQLException ex) {
            index=null;
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            return index;
        }
    }
}
