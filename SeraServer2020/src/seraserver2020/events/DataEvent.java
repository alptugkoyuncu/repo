/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.events;

import data.NetworkPackage;
import java.util.Date;
import java.util.EventObject;

/**
 *
 * @author ISIK
 * @param <T>
 */
public class DataEvent extends EventObject{
    Date date;
    public DataEvent(NetworkPackage source) {
        super(source);
        date = new Date();
    }

    public Date getDate() {
        return date;
    }
    @Override
    public NetworkPackage getSource() {
        return (NetworkPackage) super.getSource(); //To change body of generated methods, choose Tools | Templates.
    }

}
