/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.events;

import java.util.EventObject;


/**
 *
 * @author ISIK
 */
public class ServerEvent extends EventObject{
    private final String ipAddress;
    private final int port;

    public String getIpAddress() {
        return ipAddress;
    }

    public int getPort() {
        return port;
    }

    public ServerEvent(Object source,String ipAddress, int port) {
        super(source);
        this.ipAddress = ipAddress;
        this.port = port;
    }
    
    
}
