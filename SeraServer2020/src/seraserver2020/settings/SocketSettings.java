/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.settings;

import settings.AbstractSeraSettings;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import seraserver2020.ui.main.MainFrame;

/**
 *
 * @author ISIK
 */
public class SocketSettings extends AbstractSeraSettings{
    final static String PROP_PORT="socket.port";

    public SocketSettings() {
        super(MainFrame.SETTINGS_FOLDER_NAME, MainFrame.SETTINGS_FILE_NAME);
    }
    
    public Integer getPort() {
        String port = this.values.get(PROP_PORT);
        try{
            return Integer.parseInt(port);
        }catch(NumberFormatException ex){
            return null;
        }
    }

    public void setPort(int port) {
        this.values.put(PROP_PORT, port+"");
    }


    @Override
    public void setDefaultValues() {
        this.setPort(5555); }
    
}
