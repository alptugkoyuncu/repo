/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.socket;

import data.ActionCode;
import data.ClientPackage;
import data.GeneralSystemConstant;
import data.NetworkPackage;
import data.ServerPackage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import log.LogEvent;

/**
 *
 * @author ISIK
 */
public class ClientHandler implements Runnable {

    public final static long INCOME_MAX_WAIT_SECOND = 180000;//after maximum second 
    Socket socket;
    InputStream in;
    OutputStream out;
    boolean connectionTerminatedByClient = false;
    private boolean runningClientHandler;
    private int serverClientHandlerListIndex;
    private Integer clientId;
    private boolean connected;
    String ip;
    private Date lastReceivedPackageDate, lastSentPackageDate;
    private Date connectionTime;
    NetworkPackage clientPackage;
    SeraServer server;
    //socket : client lar ile  iletisim kurulacak socket baglantisi
    //clientHandlerListIndex serverda List de tutalan clieentHandlerinin listedeki indexi

    public ClientHandler(Socket socket, SeraServer server) {
        this.server = server;
        this.lastReceivedPackageDate = new Date();
        this.ip = null;
        this.socket = socket;
        this.runningClientHandler = false;
        this.serverClientHandlerListIndex = -1;
        this.clientId = null;
        connected = false;
        if (server.clientList.add(this)) {
            serverClientHandlerListIndex = server.clientList.size() - 1;
            server.fireClientConnectedEvent(this);
            runningClientHandler = true;
            this.connectionTime = new Date();
            System.out.println("Client list size = " + server.clientList.size());
        } else {
            runningClientHandler = false;
        }
        new Thread(this).start();
    }

    public Date getLastReceivedPackageDate() {
        return lastReceivedPackageDate;
    }

    public boolean isConnected() {
        return connected;
    }

    public Integer getClientId() {
        return clientId ;
    }

    public Integer getServerClientHandlerListIndex() {
        return serverClientHandlerListIndex;
    }

    public void setServerClientHandlerListIndex(Integer serverClientHandlerListIndex) {
        this.serverClientHandlerListIndex = serverClientHandlerListIndex;
    }

    public Date getConnectionTime() {
        return connectionTime;
    }

    public String getIp() {
        return this.ip;
    }

//        private synchronized NetworkPackage readNetworkServerPackage() throws IOException, Exception {
//            if (dis.available() > 0) {
//                String line = dis.readLine();
//                this.lastReceivedPackageDate=new Date();
//                NetworkPackage pck = NetworkPackage.createNetworkServerPackage(line, this.getClientId());
//                fireDataReceivedEvent(pck);
////                server.fireLogEvent(LogEvent.INFO, pck.getSenderMessage());
//                return pck;
//            }
//            return null;
//        }
    public synchronized void sentNetworkClientPackage(NetworkPackage pck) {
        if (socket == null) {
            server.fireLogEvent(LogEvent.ERROR, "Socket null", this.serverClientHandlerListIndex);
            this.terminate();
            return;
        }
        if (socket.isClosed()) {
            server.fireLogEvent(LogEvent.ERROR, "Socket closed", this.serverClientHandlerListIndex);
            this.terminate();
        }
        if (out == null) {
            server.fireLogEvent(LogEvent.ERROR, "Print Stream null", this.serverClientHandlerListIndex);
            this.terminate();
            return;
        }

        if (pck == null) {
            server.fireLogEvent(LogEvent.ERROR, "packet null", this.serverClientHandlerListIndex);
            return;
        }
        if (!pck.isFinished()) {
            server.fireLogEvent(LogEvent.ERROR, "packet is not finished", this.serverClientHandlerListIndex);
            return;
        }
        byte[] b = pck.getMessageInByteFormat();
        if (b.length == 0) {
            server.fireLogEvent(LogEvent.ERROR, "packet bos", this.serverClientHandlerListIndex);
            return;
        }
        try {
            out.write(b, 0, b.length);
            out.flush();
        } catch (IOException ex) {
            server.fireLogEvent(LogEvent.ERROR, "Output error: " + ex.getMessage(), this.serverClientHandlerListIndex);
            this.terminate();
        }
        server.fireDataSentEvent(pck);
        this.lastSentPackageDate = new Date();

//            server.fireLogEvent(LogEvent.INFO, "Sent package : " + pck.getMessage());
    }

    private boolean readClientId() {

        //wait for client id 
        int clientIdDenemeIndex = 0;
        NetworkPackage serverPackage, clientPackage;
        serverPackage = new ServerPackage();
        clientPackage = new ClientPackage();
        clientId = null;
        while (clientId == null && runningClientHandler) {
            if (this.socket.isInputShutdown() || this.socket.isOutputShutdown()
                    || this.socket.isClosed()) {
                this.runningClientHandler = false;
                server.fireLogEvent(LogEvent.ERROR, this.serverClientHandlerListIndex + ". indexdeki Client`a id alinamadi.baglanti koparilacak.", this.serverClientHandlerListIndex);
                break;
            }
            if (clientIdDenemeIndex > GeneralSystemConstant.MAX_NUMBER_OF_SERVER_ID_REQUEST) {
                runningClientHandler = false;
                server.fireLogEvent(LogEvent.ERROR, "Client id talep suresi zaman asima ugradi", this.serverClientHandlerListIndex);
                break;
            }
            Date now = new Date();
            if (now.getTime() - lastSentPackageDate.getTime() > GeneralSystemConstant.MAX_TIME_SERVER_ID_REQUEST_INTERVAL) {
                try {
//                        clientPackage.reset();
                    clientPackage = new ClientPackage();
                    clientPackage.setHostId(GeneralSystemConstant.SERVER_ID);
                    clientPackage.setTargetId(null);
                    clientPackage.setActionCode(ActionCode.AC_ID_REQUESTED);
                    clientPackage.finishPacket();
                    sentNetworkClientPackage(clientPackage);
                } catch (Exception ex) {
                    server.fireLogEvent(LogEvent.ERROR, "client id istemede package olusturulamadi : ", this.serverClientHandlerListIndex);
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                }
                clientIdDenemeIndex++;
            }
            try {
                if (in.available() > 0) {
                    serverPackage.addByte((byte) in.read());
                    if (serverPackage.isFinished()) {
                        lastReceivedPackageDate = new Date();
                        serverPackage.setHostId(null);
                        server.fireDataReceivedEvent(serverPackage);
                        if (serverPackage.getActionCode() == ActionCode.AC_SEND_ID) {
                            List<String> pckData = serverPackage.getDataArray();
                            if (pckData.isEmpty()) {
                                server.fireLogEvent(LogEvent.ERROR, "client id datasi yok Message:" + serverPackage.getMessage(), this.serverClientHandlerListIndex);

                            }
                            try {
                                clientId = Integer.parseInt(pckData.get(0));
                                if (!server.checkId(clientId)) {
                                    clientId = null;
                                    server.fireLogEvent(LogEvent.INFO, "Client id kayitlli degil  Message : " + serverPackage.getMessage(), this.serverClientHandlerListIndex);

                                }
                            } catch (NumberFormatException ex) {
                                clientId = null;
                                server.fireLogEvent(LogEvent.ERROR, "Client id integere pars edilemiyor Messaege : " + serverPackage.getMessage(), this.serverClientHandlerListIndex);

                            }
                        }
//                            serverPackage.reset();
                        serverPackage = new ServerPackage();
                    }
                }
            } catch (IOException ex) {
                server.fireLogEvent(LogEvent.ERROR, "in.available>0 hatasi Message:" + ex.getMessage(), this.serverClientHandlerListIndex);
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                server.fireLogEvent(LogEvent.ERROR, "Thread.sleep hatasi : Message:" + ex.getMessage(), this.serverClientHandlerListIndex);
            }
        }

        clientPackage = new ClientPackage();
        clientPackage.setHostId(GeneralSystemConstant.SERVER_ID);
        clientPackage.setTargetId(clientId);
        if (clientId == null) {
            clientPackage.setActionCode(ActionCode.AC_ID_REFUSED);
            runningClientHandler = false;
        } else {
            server.fireClientIdReceived(this);
            clientPackage.setActionCode(ActionCode.AC_ID_ACCEPTED);

        }
        clientPackage.finishPacket();
        this.sentNetworkClientPackage(clientPackage);
        return clientId != null;
    }

    @Override
    public void run() {
        if (!runningClientHandler) {
            return;
        }
        lastReceivedPackageDate = new Date();
        NetworkPackage serverPackage, clientPackage;
        serverPackage = new ServerPackage();
        clientPackage = new ClientPackage();

        try {
            in = socket.getInputStream();
        } catch (IOException ex) {
//                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            server.fireLogEvent(LogEvent.ERROR, "input Stream olusturulamadi Message : " + ex.getMessage(), this.serverClientHandlerListIndex);
            runningClientHandler = false;
            in = null;
//                return;
        }
        try {
            out = socket.getOutputStream();
        } catch (IOException ex) {
            server.fireLogEvent(LogEvent.ERROR, "output Stream olusutulamadi Messaege : " + ex.getMessage(), this.serverClientHandlerListIndex);
//                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            out = null;
            runningClientHandler = false;
        }

        if (!runningClientHandler) {
            connected = false;
            return;
        }
        connected = true;
        lastSentPackageDate = new Date();
        lastReceivedPackageDate = new Date();
        this.readClientId();
//            clientPackage.reset();

        if (this.getClientId() != null) {

//                clientPackage.reset();
//client id null degilse iliskili butun client lara baglanti haberi gider
            for (ClientHandler ch : server.getClientListByMapping(this)) {
                clientPackage = new ClientPackage();
                clientPackage.setHostId(GeneralSystemConstant.SERVER_ID);
                clientPackage.setTargetId(ch.getClientId());
                clientPackage.setActionCode(ActionCode.AC_FRIEND_CONNECTED);
                clientPackage.addData(this.getClientId() + "");
                clientPackage.finishPacket();
                ch.sentNetworkClientPackage(clientPackage);
            }

            // yeni baglanti idisi unique ise tek bir client ta olabilecekse diger baglantiyi varsa kapatir
            if (server.isUniqueCientId(clientId)) {
                Iterator<ClientHandler> it = server.clientList.iterator();
                while(it.hasNext()){
                    ClientHandler ch=it.next();
                    if(ch==null)
                        it.remove();
                    else{
                        if(this.equals(ch)|| ch.getClientId()==null)
                            continue;
                        if(ch.getClientId().intValue()==this.getClientId().intValue()){
                            //terminat icinde silinecegi icin dongu icinde silinmesine hata veriyor sadece iterator ile izin veriyor
                            it.remove();
                            ch.terminate();
                        }
                    }
                }
            }
        }

        //wait for normal data
        this.lastReceivedPackageDate = new Date();
        this.lastSentPackageDate = new Date();
//            long lastPingSent = System.currentTimeMillis();
//            serverPackage.reset();
        serverPackage = new ServerPackage();
        long max_in_allocate_time = 100;//100msaniye

        while (runningClientHandler) {
            if (this.socket.isInputShutdown() || this.socket.isOutputShutdown()
                    || this.socket.isClosed()) {
                this.terminate();
                break;
            }
            long curr = new Date().getTime();
            //sent ping
            if (curr - lastSentPackageDate.getTime() > GeneralSystemConstant.PING_WAIT_MS) {
//                    clientPackage.reset();
                clientPackage = new ClientPackage();
                clientPackage.setHostId(GeneralSystemConstant.SERVER_ID);
                clientPackage.setTargetId(clientId);
                clientPackage.setActionCode(ActionCode.AC_SEND_PING);
                clientPackage.finishPacket();
                sentNetworkClientPackage(clientPackage);
            }
            if (curr - lastReceivedPackageDate.getTime() > INCOME_MAX_WAIT_SECOND) {
                this.terminate();
                break;
            }
            try {
                long inStartTime = System.currentTimeMillis();
                while (in.available() > 0 && (System.currentTimeMillis() - inStartTime < max_in_allocate_time)) {
                    serverPackage.addByte((byte) in.read());
                    if (serverPackage.isFinished()) {
                        lastReceivedPackageDate = new Date();
                        serverPackage.setHostId(this.getClientId());
                        server.fireDataReceivedEvent(serverPackage);
                        if (serverPackage.getTargetId() == GeneralSystemConstant.SERVER_ID) {
                            //server"a gonderilen data gerekli islemi yap
                            //server dan istek vardir istegi action code a gore ayalayip paketi sahibine gonderir
                            sentRespondMessage((ServerPackage) serverPackage);

//                               
                        } else {
                            boolean flag = false;
                            Iterator<ClientHandler> it = server.clientList.iterator();
                            while(it.hasNext()){
                                ClientHandler ch = it.next();
                                if(ch==null){
                                    it.remove();
                                    continue;
                                }
                                if (Objects.equals(ch.getClientId(), serverPackage.getTargetId())) {
                                    //                                clientPackage.reset();
                                    clientPackage = new ClientPackage();
                                    clientPackage.setHostId(serverPackage.getHostId());
                                    clientPackage.setTargetId(ch.getClientId());
                                    clientPackage.setActionCode(serverPackage.getActionCode());
                                    clientPackage.setDataArray(serverPackage.getDataArray());
                                    clientPackage.finishPacket();
                                    ch.sentNetworkClientPackage(clientPackage);
                                    flag = true;
                                }
                            }
                            if (!flag) {
                                server.fireLogEvent(LogEvent.ERROR, "Network package uygun bir receiver id si yok\n:" + clientPackage.getMessage(), this.serverClientHandlerListIndex);
                            }
                        }

//                            serverPackage.reset();
                        serverPackage = new ServerPackage();
                    }
                }

            } catch (IOException ex) {
                server.fireLogEvent(LogEvent.ERROR, "in.available error in main loop Message : " + ex.getMessage(), this.serverClientHandlerListIndex);
            } catch (Exception ex) {
                server.fireLogEvent(LogEvent.ERROR, ex.getMessage(), this.serverClientHandlerListIndex);
//                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                Thread.sleep(20);
            } catch (InterruptedException ex) {
                server.fireLogEvent(LogEvent.ERROR, "in general loop thread.sleep hatasi : " + ex.getMessage(), this.serverClientHandlerListIndex);
//                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        runningClientHandler = false;

        // Butun alakali clientlara baglanti kapatma bilgisi gonderir
        for (Iterator<ClientHandler> it = server.getClientListByMapping(this).iterator(); it.hasNext();) {
            ClientHandler ch = it.next();
            //            clientPackage.reset();
            clientPackage = new ClientPackage();
            clientPackage.setHostId(GeneralSystemConstant.SERVER_ID);
            clientPackage.setTargetId(ch.getClientId());
            clientPackage.setActionCode(ActionCode.AC_FRIEND_DISCONNECTED);
            clientPackage.addData(getClientId() + "");
            clientPackage.finishPacket();
            ch.sentNetworkClientPackage(clientPackage);
        }
        //kendisine client kapatma bilgisi gonderiliyor eger kendisi kapatmadiysa
        if (!connectionTerminatedByClient) {
            clientPackage = new ClientPackage();
            clientPackage.setHostId(GeneralSystemConstant.SERVER_ID);
            clientPackage.setTargetId(this.getClientId());
            clientPackage.setActionCode(ActionCode.AC_CLIENT_DISCONNECTED);
//            clientPackage.addData(getClientId()+"");
            clientPackage.finishPacket();
            this.sentNetworkClientPackage(clientPackage);
        }

        
        server.clientList.remove(this);

        if (out != null) {
            try {
                this.out.close();
            } catch (IOException ex) {
                server.fireLogEvent(LogEvent.ERROR, "in.close hatasi: Message:" + ex.getMessage(), this.serverClientHandlerListIndex);
            }
        }
        if (in != null) {
            try {
                this.in.close();
            } catch (IOException ex) {
                server.fireLogEvent(LogEvent.ERROR, "dis.close hatasi\nMessage : " + ex.getMessage(), this.serverClientHandlerListIndex);
            }
        }

        try {
            if (socket != null && !socket.isClosed()) {
                this.socket.close();
            }
        } catch (IOException ex) {
//                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            server.fireLogEvent(LogEvent.ERROR, "socket.close() hatasi\nMessaeg : " + ex.getMessage(), this.serverClientHandlerListIndex);
        }
        connected = false;
        server.fireClientDisConnectedEvent(this);

    }

    public void terminate() {
        this.runningClientHandler = false;
    }

    private void sentRespondMessage(ServerPackage pck) {
        NetworkPackage reply = null;
        int hostId = this.getClientId();
        switch (pck.getActionCode()) {
            //server icin olan mesajlar
            case ActionCode.AC_CLIENT_DISCONNECTED: {
                connectionTerminatedByClient = true;
                this.terminate();
                return;
            }
            case ActionCode.AC_SEND_PING: {
                return;
            }
            case ActionCode.AC_REQUEST_FOCUED_PROVIDERS: {
                reply = new ClientPackage();
                reply.setHostId(GeneralSystemConstant.SERVER_ID);
                reply.setTargetId(getClientId());
                reply.setActionCode(ActionCode.AC_REQUEST_FOCUED_PROVIDERS);
                for (ClientHandler ch : server.getClientListByMapping(this)) {
                    Integer id = ch.getClientId();
                    if (id != null) {
                        reply.addData(id + "");
                    }
                }
                reply.finishPacket();
                this.sentNetworkClientPackage(reply);
                return;
            }
            // serverin diger clientlar icin gonderecego mesajlar
            default: {
                int ac = pck.getActionCode();
                List<String> dataArr = pck.getDataArray();
                for (ClientHandler ch : server.getClientListByMapping(this)) {
                    reply = new ClientPackage();
                    reply.setHostId(hostId);
                    reply.setActionCode(ac);
                    reply.setTargetId(ch.getClientId());
                    reply.setDataArray(dataArr);
                    reply.finishPacket();
                    ch.sentNetworkClientPackage(reply);
                }
                return;
            }

        }
    }

}
