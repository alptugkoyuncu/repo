/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.socket;

import static data.GeneralSystemConstant.SERVER_ID;
import data.NetworkPackage;
import seraserver2020.events.ClientEvent;
import seraserver2020.events.DataEvent;
import seraserver2020.events.ServerEvent;
import seraserver2020.events.ServerListener;
import seraserver2020.util.HelperFunctions;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import log.LogEvent;
import log.LogListener;
import seraserver2020.db.MySqlServerConnection;
/**
 *
 * @author ISIK
 */
public class SeraServer implements Runnable {

    public final static int MAX_CLIENT_NUMBER = 20;
   
//    public final static long PING_WAIT_SECOND = 60000;

    public  static List<ClientHandler> clientList = new ArrayList<>();
    private final List<ServerListener> serverListeners;
    private final List<LogListener> serverLogListeners;
    private int port;
    private String externalIp;
    private String localIp;
    private ServerSocket serverSocket;
    private boolean runThread;
    private boolean alive;
    private Map<Integer, Set<Integer>> clientIdMapping;
//    private final static List<Integer> clientIdList;

    public Map<Integer, Set<Integer>> getClientIdMapping() {
        return clientIdMapping;
    }

    private boolean createClientMappingFromDb() {
        MySqlServerConnection conn = new MySqlServerConnection();
        this.clientIdMapping = conn.getClientMapping();
        return clientIdMapping != null && !clientIdMapping.isEmpty();
    }

    private boolean createClientMappingFromFile() {
        String fileName = "C:\\clientIdMapping.txt";
//        File file = new File(ServerConfigProp.clientMappingFileLocation);
        File file = new File(fileName);
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ex) {
            fireLogEvent(LogEvent.ERROR, "File Not found : " + fileName + "\n Message:" + ex.getMessage());
            return false;
        }
        String str;
        int index = 0;
        try {
            while ((str = br.readLine()) != null) {
                index++;
                String data[] = str.split(" ");
                if (data.length != 2) {
                    fireLogEvent(LogEvent.ERROR, "File client Mapping hatasi data uzunlugu eksik index : " + index + "zn File :" + file.getPath());

                    continue;
                }
                try {
                    Integer id = Integer.parseInt(data[0]);
                    Integer id2 = Integer.parseInt(data[1]);
                    Set<Integer> set;
                    if (this.clientIdMapping.containsKey(id)) {
                        set = this.clientIdMapping.get(id);
                        set.add(id2);
                    } else {
                        set = new HashSet<>();
                        set.add(id2);
                        this.clientIdMapping.put(id, set);
                    }
                    if (this.clientIdMapping.containsKey(id2)) {
                        set = this.clientIdMapping.get(id2);
                        set.add(id);
                    } else {
                        set = new HashSet<>();
                        set.add(id);
                        this.clientIdMapping.put(id2, set);
                    }

                } catch (NumberFormatException ex) {
                    fireLogEvent(LogEvent.ERROR, "File client Mapping hatasi data Integere cevrilemiyor: " + index + "zn File :" + file.getPath());
                }
            }
        } catch (IOException ex) {
            fireLogEvent(LogEvent.ERROR, fileName + " File readLine hatasi\n Error Message" + ex.getMessage());
            return false;
        }

        fireLogEvent(LogEvent.DEBUG, "File ClientMapping tamalandi");
        return true;
    }

    public boolean loadClientMapping() {
        clientIdMapping = new HashMap<>();
        return createClientMappingFromDb() || createClientMappingFromFile();
    }

    public SeraServer(int port) {

        this.serverListeners = new ArrayList<>();
        this.serverLogListeners = new ArrayList<>();
        runThread = false;
        System.err.println(clientIdMapping);
        this.port = port;
        this.updateExternalIp();
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }
    
    private void updateExternalIp() {
        this.externalIp = HelperFunctions.getIP();
    }


    public String getExternalIp() {
        return externalIp;
    }

    public synchronized void start() {
        loadClientMapping();
        try {
            serverSocket = new ServerSocket(port);
            runThread = true;
            this.serverSocket.setSoTimeout(100);
            fireServerConnectedEvent();
        } catch (IOException ex) {
            this.fireLogEvent(LogEvent.ERROR, "Server baglanamadi(socket baglanti hatasi)");
            runThread = false;
            fireServerDisconnectedEvent();
            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (runThread) {
            new Thread(this).start();
        }
    }

    @Override
    public void run() {
        alive = true;
        while (runThread) {
            while (clientList.size() >= MAX_CLIENT_NUMBER) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                    this.fireLogEvent(LogEvent.ERROR, "Client list maximum sayiya ulasan thread.sleep icinde hata olustu");
                }
            }
            try {
                Socket clientSocket = serverSocket.accept();

                int i = 0;
                ClientHandler t = new ClientHandler(clientSocket,this);
//                this.addClientToList(t);
                if (clientList.size() == MAX_CLIENT_NUMBER) {
                    fireLogEvent(LogEvent.INFO, "Server maksimum kapasiteye ulasti.");
                }

            } catch (SocketTimeoutException ex) {
                continue;

            } catch (IOException e) {
                fireLogEvent(LogEvent.INFO, "Socket.accept hatasi\n" + e.getMessage());

                break;
            }
        }
        //server kapanmadan once butun client handler threadlerini sonlandiriyor.
        Iterator<ClientHandler> it = clientList.iterator();
        while(it.hasNext()){
            ClientHandler ch = it.next();
            if(ch!=null){
                it.remove();// terminate fonksiyununda silinmesinde java.util.ConcurrentModificationException hatasi vereceginden onceden siliniyor dongu icinde sadece it.remove da hata vermez
                ch.terminate();
            }
        }
        fireServerDisconnectedEvent();
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        //server butun listenerlari kaldiriyor
        serverListeners.clear();
        serverLogListeners.clear();
        alive = false;
    }

    public boolean isRunning() {
        return alive;
    }

    public void terminate() {
        runThread = false;

    }

    public boolean checkId(Integer id) {
        return this.clientIdMapping.containsKey(id);
    }

    Set<ClientHandler> getClientListByMapping(ClientHandler ch) {
//        int id = senderId == 1001 ? 2001 : 1001;
Set<ClientHandler> s = new HashSet<>();
if(ch==null || ch.getClientId()==null)
    return s;
        Set<Integer> targetIds = clientIdMapping.get(ch.getClientId());

        
        if (targetIds == null) {
            return s;
        }
        for (ClientHandler c : clientList) {
            if (c==null || c == ch) {
                continue;
            }
            if (targetIds.contains(c.getClientId())) {
                s.add(c);
            }
        }
        return s;
    }

    /**
     * provider id ler uniqu id dir ve 1000 ile 2000 arasindadir.
     *
     * @param clientId
     * @return deger 1000 ile 2000 arasindaysa(1000 ve 2000 haric) true yoksa
     * false dondurur
     */
     boolean isUniqueCientId(Integer clientId) {
        return clientId < 2000 && clientId > 1000;
    }

// mapping den receiver id ler alinacak bunun icin sender id kullanilarak mapping de karsilastirilma yapilarak client list te indexler alinir

    
    public void addServerListener(ServerListener l) {
        this.serverListeners.add(l);
    }

    public void removeServerListener(ServerListener l) {
        this.serverListeners.remove(l);
    }

    public void addServerLogListener(LogListener l) {
        this.serverLogListeners.add(l);
    }

    public void removeServerLogListener(LogListener l) {
        this.serverLogListeners.remove(l);
    }
 
    void fireDataSentEvent(NetworkPackage pck) {
        for (ServerListener l : serverListeners) {
            l.dataSent(new DataEvent(pck));
        }
    }

    void fireDataReceivedEvent(NetworkPackage pck) {
        for (ServerListener l : serverListeners) {
            l.dataReceived(new DataEvent(pck));
        }
    }

    private void fireServerConnectedEvent() {
        ServerEvent evt = new ServerEvent(this, this.externalIp, port);
        for (ServerListener l : serverListeners) {
            l.serverConnected(evt);
        }
    }

    private void fireServerDisconnectedEvent() {
        ServerEvent evt = new ServerEvent(this, this.externalIp, port);
        for (ServerListener l : serverListeners) {
            l.serverDisconnected(evt);
        }
    }
    private void fireLogEvent(int eventType,String message){
        for (LogListener l : serverLogListeners) {
            l.logOccured(new LogEvent(this, eventType, message));
        }
    }
    void fireLogEvent(int eventType, String message,int clientIndex) {
        
        ClientHandler ch = clientIndex>=0 ? clientList.get(clientIndex):null;
        if(ch==null || ch.getClientId()==null){
            message = "("+clientIndex+")"+message;
        }else
            message = "("+ch.getClientId()+")"+message;
        
        
        fireLogEvent(eventType, message);
    }

    void fireClientConnectedEvent(ClientHandler ch) {
        ClientEvent evt = new ClientEvent(ch);
        for (ServerListener l : serverListeners) {
            l.clientConnected(evt);
        }
    }

    void fireClientDisConnectedEvent(ClientHandler ch) {
        ClientEvent evt = new ClientEvent(ch);
        for (ServerListener l : serverListeners) {
            l.clientDisconnected(evt);
        }
    }

    void fireClientIdReceived(ClientHandler ch) {
        ClientEvent evt = new ClientEvent(ch);
        for (ServerListener l : serverListeners) {
            l.clientIdReceived(evt);
        }

    }
}
