/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.ui.settings;

import java.awt.Component;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractListModel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import seraserver2020.db.MySqlServerConnection;

/**
 *
 * @author ISIK
 */
public class ClientListMappingPanel extends JPanel implements SaveableSettingsPanel {

    JScrollPane pane1, pane2;
    JList<Integer> jlClientList;
    JList<CheckBoxListItem> jlMappingList;
    JLabel lbl;
    List<Integer> ids;
    Map<Integer, Set<Integer>> idMapping;

    public ClientListMappingPanel() {
        MySqlServerConnection conn = new MySqlServerConnection();
        ids = conn.getClientList();
        idMapping = conn.getClientMapping();
        lbl = new JLabel("LOADING");
        this.setLayout(new GridLayout(1, 0, 40, 40));
    }

    @Override
    public ButtonPanelEvent save() {
        MySqlServerConnection conn = new MySqlServerConnection();
        conn.truncateClientMapping();
        conn.insertClientMapping(idMapping);
        this.load();
        return new ButtonPanelEvent(ButtonPanelEvent.EVENT_SUCCESS, "Client Mapping başarıyle kaydedildi");
    }

    @Override
    public ButtonPanelEvent load() {
        this.removeAll();
        this.add(lbl);

        CheckBoxListItem[] items = new CheckBoxListItem[ids.size()];
        for (int i = 0; i < items.length; i++) {
            items[i] = new CheckBoxListItem(ids.get(i));
        }
        jlClientList = new JList<Integer>(ids.toArray(new Integer[0]));
        jlClientList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jlMappingList = new JList<>(items);
        jlMappingList.setCellRenderer(new CheckboxItemCellRenderer());
        jlMappingList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        jlClientList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    return;
                }
                // clear list2 elements
                jlMappingList.setEnabled(false);
                jlMappingList.clearSelection();
                CheckBoxListItem item;
                for (int i = 0; i < jlMappingList.getModel().getSize(); i++) {
                    item = jlMappingList.getModel().getElementAt(i);
                    item.setSelected(false);
                }
                int index = jlClientList.getSelectedIndex();
                if (index < 0) {
                    return;
                }
                int selectedId = jlClientList.getModel().getElementAt(index);
                Set<Integer> mappedItems = idMapping.get(selectedId);
                System.out.println(mappedItems);
                if (mappedItems != null && !mappedItems.isEmpty()) {
                    for (int i = 0; i < jlMappingList.getModel().getSize(); i++) {
                        item = jlMappingList.getModel().getElementAt(i);
                        if (mappedItems.contains(item.getId())) {
                            item.setSelected(true);
                        }
                    }
                }
                jlMappingList.setEnabled(true);
            }
        });

        jlMappingList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                Integer mapKeyIndex = jlClientList.getSelectedIndex();
                if(mapKeyIndex<0){
                    jlMappingList.clearSelection();
                    return;
                }
                Integer mapKey = jlClientList.getModel().getElementAt(mapKeyIndex);
                if (e.getValueIsAdjusting()) {
                    return;
                }

                int index = jlMappingList.getSelectedIndex();
                System.err.println("list selection listener " + index);
                if (index < 0) {
                    return;
                }
                CheckBoxListItem item = jlMappingList.getModel().getElementAt(index);
                item.setSelected(!item.isSelected());
                Integer id = item.getId();
                
                Set<Integer> set = idMapping.get(mapKey);
                if (item.isSelected()) {
                    if (set == null) {
                        set = new HashSet<>();
                        idMapping.put(mapKey, set);
                    }
                    set.add(id);
                } else {
                    set.remove(id);
                }
//                idMapping.put(mapKey, l);
                jlMappingList.clearSelection();
            }
        });
        this.remove(this.lbl);
        this.add(new JScrollPane(jlClientList));
        this.add(new JScrollPane(jlMappingList));
        this.validate();
        jlMappingList.setEnabled(false);
        return new ButtonPanelEvent(ButtonPanelEvent.EVENT_SUCCESS, "client maping list başarıyla yüklendi");
    }

    @Override
    public ButtonPanelEvent resetToDefault() {
        MySqlServerConnection conn = new MySqlServerConnection();
        ids = conn.getClientList();
        idMapping = conn.getClientMapping();
        return this.load();
    }

    @Override
    public ButtonPanelEvent reset() {
        return this.load();
    }

    /**
     * @param <T>
     */
    class MyListModel<T> extends AbstractListModel {

        T[] ids;

        public MyListModel(T[] ids) {
            this.ids = ids;
        }

        @Override
        public int getSize() {
            return ids.length;
        }

        @Override
        public T getElementAt(int index) {
            return ids[index];
        }

    }

    class CheckBoxListItem {

        private int id;
        private boolean selected;

        public int getId() {
            return id;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public CheckBoxListItem(int id) {
            this(id, false);
        }

        public CheckBoxListItem(int id, boolean selected) {
            this.id = id;
            this.selected = selected;
        }
    }

    class CheckboxItemCellRenderer implements ListCellRenderer<CheckBoxListItem> {

        @Override
        public Component getListCellRendererComponent(JList<? extends CheckBoxListItem> list, CheckBoxListItem value, int index, boolean isSelected, boolean cellHasFocus) {
            JCheckBox chk = new JCheckBox(value.getId() + "", value.isSelected());
            chk.setEnabled(list.isEnabled());
            setFont(list.getFont());
            chk.setOpaque(value.isSelected());
            if (value.isSelected()) {
                chk.setBackground(list.getSelectionBackground());
                chk.setForeground(list.getSelectionForeground());
            } else {
                chk.setBackground(list.getBackground());
                chk.setForeground(list.getForeground());
            }
            return chk;
        }

    }
}
