/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.ui.settings;

/**
 *
 * @author ISIK
 */
public interface SaveableSettingsPanel {
    public ButtonPanelEvent save();
    public ButtonPanelEvent load();
    public ButtonPanelEvent resetToDefault();
    public ButtonPanelEvent reset();
}
