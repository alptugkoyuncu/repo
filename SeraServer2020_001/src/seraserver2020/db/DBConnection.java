package seraserver2020.db;

import db.bean.TempBean;
import db.bean.SulamaBean;
import data.ActionCode;
import db.bean.AlanBean;
import db.bean.BolgeBean;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ISIK
 */
public abstract class DBConnection {

    private final static long PER_SECOND = 1000;
    private final static long PER_MINUTE = PER_SECOND * 60;
    private final static long PER_5_MINUTE = PER_MINUTE * 5;
    private final static long SENSOR_INSERT_INTERVAL = PER_5_MINUTE;
    private static boolean connectionEnable;

    static {
        connectionEnable = false;
    }

    protected boolean isConnectionEnable() {
        return DBConnection.connectionEnable;
    }

    public static void setConnectionEnabled(boolean connectionEnable) {
        DBConnection.connectionEnable = connectionEnable;
    }

    protected java.sql.Date getServerDate(int actionCode, java.util.Date dt) {
        Long val = dt.getTime();
        if (ActionCode.isSensorActionCode(actionCode)) {
            val = val / SENSOR_INSERT_INTERVAL;
            val = val * SENSOR_INSERT_INTERVAL;
        }
        return new java.sql.Date(val);
    }

    /**
     * action code a gore database e kaydedilip kaydedilmeyecegi kontrol
     * edilecek
     *
     * @param actionCode
     * @return
     */
    protected abstract void connect();

    public abstract boolean isValid();

    public abstract int insertClientActionList(Integer[] hostId, Integer[] targetId, Integer[] actionCode, String[] value, java.util.Date[] dt);

    public abstract int insertClientActionList(Integer hostId, Integer targetId, Integer actionCode, String value, java.util.Date dt);

    public abstract int removeClientActionListByHostId(Integer hostId);

    public abstract int removeClientActionListByTargetId(Integer targetId);

//    public abstract int deleteClientActionList();
    /**
     * Tablo tamamen yeni degerleri barindiracak sekilde ayarlanir Foreign
     * keylere gore diger tablolardaki datalarda deleteExistingRecord degerine
     * gore silinir database ile kontrol edilir eger clientlistte silinmesi
     * gereken record var ise ve bu recordlar baska tabloda kullanildiysa
     * deleteExitingRecords degerine gore silinir veya sadece disable yapilir
     *
     * @param clientList
     * @param deleteExitingRecords
     * @return
     */
    public abstract int updateClientList(SortedMap<Integer, Boolean> clientList, boolean deleteExitingRecords);

    /**
     * tablo da id varsa aktiflik degeri update yapilir yoksa eklenir
     *
     * @param clientList
     * @return
     */
    public abstract int insertOrUpdateClientList(SortedMap<Integer, Boolean> clientList);

    public abstract int insertClientList(Integer clientId, boolean aktif);

    public abstract int removeClientList(Integer clientId);

    public abstract int removeClientList(Integer[] clientId);

    public abstract SortedMap<Integer, Boolean> getClientList();

    public abstract int insertClientMapping(SortedMap<Integer, SortedSet<Integer>> map);

    public abstract int insertClientMapping(Integer hostId, SortedSet<Integer> targetId);

    public abstract int removeClientMapping(Integer hostId, Integer targetId);

    public abstract int removeClientMapping(Integer[] hostId, Integer[] targetId);

    public abstract int removeClientMappingByHostId(Integer hostId);

    public abstract int removeClientMappingBytargetId(Integer targetId);

    public abstract SortedMap<Integer, SortedSet<Integer>> getClientMapping();

    public abstract void disconnect();

    public abstract int insertSulamaTable(List<SulamaBean> beans);

    public abstract int insertSulamaTable(SulamaBean bean);

    public abstract List<SulamaBean> getSulama(Date date1, Date date2, boolean ascendingOrder);

    public abstract List<SulamaBean> getSulama(int bas, int miktar, boolean ascendingOrder);

    public abstract int insertTemperatureTable(List<TempBean> list);

    public abstract int insertTemperatureTable(TempBean beans);

    public abstract List<AlanBean> getAllAlan();

    /**
     * IMPLEMENT EDILMEDI
     *
     * @param alanList silinecek liste
     * @return silinmis liste
     */
    public abstract List<AlanBean> deleteFromAlan(List<AlanBean> alanList);

    public abstract void updateAlan(List<AlanBean> alanList);

    
    /**
     *
     * @param alanList eklenecek gubre listesi
     * @return eklenen gubre listesi
     */
    public abstract List<AlanBean> insertIntoAlan(List<AlanBean> alanList);
    
    public abstract List<BolgeBean> getAllBolge();

}
