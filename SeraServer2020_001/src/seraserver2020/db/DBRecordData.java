/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package seraserver2020.db;

import data.NetworkPackage;
import seraserver2020.socket.ClientType;

/**
 *
 * @author alptug
 */
public abstract class DBRecordData {

    DBConnection conn;
    protected int clientId;
    protected DBRecordData(int clientId){
        this.clientId = clientId;
        conn = new MySqlServerConnection();
    }
    public static DBRecordData createDBRecord(int clientId) {
    switch(ClientType.getClientType(clientId)){
            case TEMP_CONTROL: return new TempData(clientId);
            case HAVUZ_KONTROL: return new HavuzData(clientId);
            case WATCHER : return new IzleyiciData(clientId);
            default: return null;
        } }
    
    public abstract int saveToDB(NetworkPackage pck) throws Exception;
}
