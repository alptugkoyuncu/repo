/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.db;

import java.util.Locale;
import seraserver2020.db.*;

/**
 *
 * @author ISIK
 */
public final class DB_Values {
    enum TempPrecision{
        PRECISION_1,//DB 0.1 DEGERLERLE KAYDEDER
        PRECISION_25,//DB 0.25 DEGERLERLE KAYDEDER
        PRECISION_50,//DB 0.5 DEGERLERLE KAYDEDER
        PRECISION_100//DB ONDALIKSIZ KAYDEDER
    }
    private static double PRESSURE_NONE_BORDER;
    private static double PRESSURE_LOW_BORDER;
    private static  TempPrecision TEMP_PRECION;
    private static int TEMP_BOTTOM_BORDER=-20;
    private static int TEMP_TOP_BORDER=120;
    private DB_Values() {

    }

    static {
        getPressureBorderValues();
        getTempPrecision();
    }

    public enum BasincSeviyesi {
        NONE("0"),
        LOW("1"),
        NORMAL("2");
        private final String DB_RECORD_VALUE;

        private BasincSeviyesi(String val) {
            DB_RECORD_VALUE = val;
        }

        public String getDB_RECORD_VALUE() {
            return DB_RECORD_VALUE;
        }

    }
    private static void getTempPrecision(){
        TEMP_PRECION = TempPrecision.PRECISION_1;
    }

    private static void getPressureBorderValues() {
        PRESSURE_LOW_BORDER = 2.5;
        PRESSURE_NONE_BORDER = 1.8;
    }

    public static String getDBValueOf_TEMP_VALUE(String arduinoValue){
        
        try{
            Double val = Double.parseDouble(arduinoValue);
            if(val.intValue()>TEMP_BOTTOM_BORDER && val.intValue()<TEMP_TOP_BORDER)
            
                switch(TEMP_PRECION){
                    case PRECISION_1: 
                        return String.format(Locale.US,"%.1f", val);
                    case PRECISION_25:{
                        int valInt = val.intValue();
                        double valFloat = val-valInt;
                        if(valFloat<0.125)
                            return valInt+"";
                        else if (valFloat<0.375)
                            return valInt+".25";
                        else if(valFloat<0.625)
                            return valInt+".5";
                        else if(valFloat<0.875)
                            return valInt+".75";
                        else 
                            return (valInt+1)+"";
                    }
                    case PRECISION_50: {
                        double d = val-val.intValue();
                        if(d<0.25)
                            return val.intValue()+"";
                        else if(d<0.75)
                            return val.intValue()+".5";
                        else 
                            return (val.intValue()+1)+"";
                    }
                    case PRECISION_100:{
                        double d = val-val.intValue();
                        if(d<0.5)
                            return val.intValue()+"";
                        else
                            return (val.intValue()+1)+"";
                    }
                    default:
                        return String.format(Locale.US,"%."+TEMP_PRECION+"f", val);
                }
                
            else
                return "";
        }catch(NumberFormatException ex){
            return "";
        }
    }
    public static BasincSeviyesi getDBValueOf_BASINC_VALUE(String arduinoValue) {
        try {
            double dVal = Double.parseDouble(arduinoValue);
            if (dVal < PRESSURE_NONE_BORDER) {
                return DB_Values.BasincSeviyesi.NONE;
            }
            if (dVal < PRESSURE_LOW_BORDER) {
                return DB_Values.BasincSeviyesi.LOW;
            }
            return DB_Values.BasincSeviyesi.NORMAL;
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public static String getDbValueOf_STATE_CONTROL(String arduinoValue) {
        try {
            int intVal = Integer.parseInt(arduinoValue) > 0 ? 1 : 0;
            return intVal + "";
        } catch (NumberFormatException ex) {
            return "";
        }
    }

}
