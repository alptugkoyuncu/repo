/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.db;

import db.bean.SulamaBean;
import data.ActionCode;
import data.NetworkPackage;
import data.ServerPackage;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ISIK
 */
public class HavuzData extends DBRecordData {

    private final static String errorHeader = "HavuzKontrolDbRecordData Dosyasi ";
    KayitSulamaBean bean;
    String arduinoKontrolEnable;
    String outerTimerKontrolEnable;
    Map<Integer, String> values;

    HavuzData(int clientId) {
        super(clientId);
        values = new HashMap<>();
        arduinoKontrolEnable = "";
        outerTimerKontrolEnable = "";
        bean = null;
    }

    public void setArduinoKontrolEnable(String arduinoKontrolEnable) {
        String oldVal = this.arduinoKontrolEnable;
        this.arduinoKontrolEnable = arduinoKontrolEnable;
        if (oldVal == null ? this.arduinoKontrolEnable != null : !oldVal.equals(this.arduinoKontrolEnable)) {
            values.put(ActionCode.AC_SEND_ARDUINO_RELAY_CONTROL_STATE, this.arduinoKontrolEnable);
        }
    }

    public void setOuterTimerKontrolEnable(String outerTimerKontrolEnable) {
        String oldVal = this.outerTimerKontrolEnable;
        this.outerTimerKontrolEnable = outerTimerKontrolEnable;
        if (oldVal == null ? this.outerTimerKontrolEnable != null : !oldVal.equals(this.outerTimerKontrolEnable)) {
            values.put(ActionCode.AC_SEND_OUTER_TIMER_STATE, this.outerTimerKontrolEnable);
        }
    }

    @Override
    public int saveToDB(NetworkPackage pck) throws Exception {
        
        List<String> datas = pck.getDataArray();

        switch (pck.getActionCode()) {
            // Sulamkayit i sadece burada kaydedecegiz cunku sulama islemi sirasinda belirli zaman araliklarinda bu action code lu data gelir
            //
            case ActionCode.AC_SEND_HAVUZ_ESP_ALL_DATA: {
                if (datas == null || datas.size() != 4) {
                    throw new Exception(errorHeader + " data uzunlugu 4 olmak zorunda Data array :" + pck.getDataArray());
                }
                boolean roleAktif = !(datas.get(0).equals("0"));
                if (roleAktif) {
                    if (bean == null) {
                        bean = new KayitSulamaBean();
                        bean.setBaslama(pck.getMessageDate().getTime());
                        bean.lastMessageDate = bean.getBaslama();
                    } else {
                        int passedTime = (int) (pck.getMessageDate().getTime() - bean.lastMessageDate);
                        bean.lastMessageDate = pck.getMessageDate().getTime();
                        switch (DB_Values.getDBValueOf_BASINC_VALUE(datas.get(3))) {
                            case NONE: {
                                bean.setNone(bean.getNone() + passedTime);
                                break;
                            }
                            case LOW: {
                                bean.setLow(bean.getLow() + passedTime);
                                break;
                            }
                            case NORMAL: {
                                bean.setNormal(bean.getNormal() + passedTime);
                            }
                        }
                    }
                } else {
                    if (bean != null) {
                        int passedTime = (int) (pck.getMessageDate().getTime() - bean.lastMessageDate);
                        bean.lastMessageDate = pck.getMessageDate().getTime();
                        bean.setBitis(pck.getMessageDate().getTime());
                        switch (DB_Values.getDBValueOf_BASINC_VALUE(datas.get(3))) {
                            case NONE: {
                                bean.setNone(bean.getNone() + passedTime);
                                break;
                            }
                            case LOW: {
                                bean.setLow(bean.getLow() + passedTime);
                                break;
                            }
                            case NORMAL: {
                                bean.setNormal(bean.getNormal() + passedTime);
                            }
                        }
                        saveSulamaTable(new SulamaBean(bean.getId(), bean.getBaslama(), bean.getBitis(), bean.getNone(), bean.getLow(), bean.getNormal()));
                        bean = null;
                    }

                }
                this.setOuterTimerKontrolEnable(DB_Values.getDbValueOf_STATE_CONTROL(datas.get(1)));
                this.setArduinoKontrolEnable(DB_Values.getDbValueOf_STATE_CONTROL(datas.get(2)));
            }
            case ActionCode.AC_SEND_RELAY_STATE:
                break;
            case ActionCode.AC_SEND_OUTER_TIMER_STATE:
                if (datas == null || datas.isEmpty()) {
                    throw new Exception(errorHeader + "data bos olamaz Data array : " + pck.getDataArray());
                }
                this.setOuterTimerKontrolEnable(DB_Values.getDbValueOf_STATE_CONTROL(datas.get(0)));
            case ActionCode.AC_SEND_ARDUINO_RELAY_CONTROL_STATE:
                if (datas == null || datas.isEmpty()) {
                    throw new Exception(errorHeader + "data bos olamaz Data array : " + pck.getDataArray());
                }
                this.setArduinoKontrolEnable(DB_Values.getDbValueOf_STATE_CONTROL(datas.get(0)));
            case ActionCode.AC_SEND_PRESSURE_SENSOR_VALUE:
                break;
            default: {
//                throw new Exception(errorHeader+" : hatali actionCode actionCode : "+pck.getActionCode());
                break;
            }
        }
            this.saveClientActionList(pck.getHostId(),pck.getTargetId(),pck.getMessageDate());
            return 0;
    }

    private void saveSulamaTable(SulamaBean bean) {
        conn.insertSulamaTable(bean);
    }

    private void saveClientActionList(int hostId, int targetId, Date date) {
        if(values.isEmpty())
            return;
        List<Integer> keys = new ArrayList<>(values.keySet());
        List<String> val = new ArrayList<>();
        for (int i = 0; i < keys.size(); i++) {
            String value = this.values.get(keys.get(i));
            if (value == null) {
                keys.remove(i);
            } else {
                val.add(value);
            }
        }
        if (keys.size() > 0 && keys.size() == values.size()) {
            Integer[] hostIds = new Integer[keys.size()];
            Integer[] targetIds = new Integer[keys.size()];
            Integer[] acs = new Integer[keys.size()];
            String[] vs = new String[keys.size()];
            Date[] dates = new Date[keys.size()];
            for (int i = 0; i < keys.size(); i++) {
                acs[i] = keys.get(i);
                vs[i] = val.get(i);
                hostIds[i] = hostId;
                targetIds[i] = targetId;
                dates[i] = date;
            }
            conn.insertClientActionList(hostIds, targetIds, acs, vs, dates);
            this.values.clear();
        }
    }

   

 

    class KayitSulamaBean extends SulamaBean {

        private long lastMessageDate;

    }
}
