/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.db;

import data.ActionCode;
import data.NetworkPackage;
import db.bean.AlanBean;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import db.bean.TempBean;

/**
 *
 * @author ISIK
 */
public class TempData extends DBRecordData {
   
    private final static String errorHeader="TemperatureDbRecordData Dosyasi";
    private final Map<Integer,String> recordSet;
    
    private String temp0;
    private String temp1;
    private String temp2;
    private String temp3;
    
     TempData(int clientId) {
         super(clientId);
        this.recordSet = new HashMap<>();
        this.temp0="";
        this.temp1="";
        this.temp2="";
        this.temp3="";
    }
    /**
     * Set the value of temp3
     *
     * @param temp3 new value of temp3
     */
    private void setTemp3(String temp3) {
        
        temp3 = DB_Values.getDBValueOf_TEMP_VALUE(temp3);
        if(temp3.isBlank())
            return;
        String oldVal = this.temp3;
        this.temp3 = temp3;
//        if(!this.temp3.equals(oldVal)){
            this.recordSet.put(ActionCode.AC_SEND_TEMP3, this.temp3);
//        }
    }


    /**
     * Set the value of temp2
     *
     * @param temp2 new value of temp2
     */
    private void setTemp2(String temp2) {
       
        temp2 = DB_Values.getDBValueOf_TEMP_VALUE(temp2);
        if(temp2.isBlank())
            return;
        String oldVal = this.temp2;
        this.temp2 = temp2;
//        if(!this.temp2.equals(oldVal)){
            this.recordSet.put(ActionCode.AC_SEND_TEMP2, this.temp2);
//        }
    }
    
    /**
     * Set the value of temp1
     *
     * @param temp1 new value of temp1
     */
    private void setTemp1(String temp1) {
        
        temp1 = DB_Values.getDBValueOf_TEMP_VALUE(temp1);
        if(temp1.isBlank())
            return;
        String oldVal = this.temp1;
        this.temp1 = temp1;
//        if(!this.temp1.equals(oldVal)){
            this.recordSet.put(ActionCode.AC_SEND_TEMP1, this.temp1);
//        }
    }

    /**
     * Set the value of temp0
     *
     * @param temp0 new value of temp0
     */
    private void setTemp0(String temp0) {
        temp0 = DB_Values.getDBValueOf_TEMP_VALUE(temp0);
        if(temp0.isBlank())
            return;
        String oldVal = this.temp0;
        this.temp0 = temp0;
//        if(!this.temp0.equals(oldVal)){
            this.recordSet.put(ActionCode.AC_SEND_TEMP0, this.temp0);
//        }
            
    }  
    
    @Override
    public int saveToDB(NetworkPackage pck) throws Exception {
    int actionCode = pck.getActionCode();
        var datas = pck.getDataArray();
        if (datas == null || datas.isEmpty()) {
            throw new Exception(errorHeader+" : 1.Data yok veya bos:"+pck.getDataArray());
        }
        
        switch(actionCode){
            case ActionCode.AC_SEND_TEMP_ALL:{
                if(datas.size()<4)
                   throw new Exception(errorHeader+" : data size yeterli degil:"+pck.getDataArray());
                 //role durumunu kaydet
               this.setTemp0(datas.get(0));
               this.setTemp1(datas.get(1));
               this.setTemp2(datas.get(2));
               this.setTemp3(datas.get(3));
               break;
            }
            case ActionCode.AC_SEND_TEMP0:
                this.setTemp0(datas.get(0));
                break;
            case ActionCode.AC_SEND_TEMP1:
                this.setTemp1(datas.get(0));
                break;
            case ActionCode.AC_SEND_TEMP2:
                this.setTemp2(datas.get(0));
                break;
            case ActionCode.AC_SEND_TEMP3:
                this.setTemp3(datas.get(0));
                break;
            default:
                break;
        }
        return saveTempTable(pck);
    }
    private AlanBean getAlan(int clientId,int actionCode){
        for(AlanBean a:seraserver2020.socket.SeraServer.ALAN_BEAN_LIST)
            if(a.getActionCode()==actionCode&& a.getIdClient()==clientId)
                return a;
        return null;
    }
    private int saveTempTable(NetworkPackage pck){
        List<TempBean> list = new ArrayList<>();
        System.out.println("rs: "+recordSet);
        for(Integer ac: recordSet.keySet()){
            
            String val = recordSet.get(ac);
            try{
                Double.parseDouble(val);
                AlanBean a = getAlan(this.clientId, ac);
                if(a==null)
                    continue;
                TempBean bean = new TempBean();
                bean.setAlan(a);
                bean.setDate(pck.getMessageDate().getTime());
                bean.setValue(val);
                bean.setId(-1);
                System.out.println("Alan bean : "+bean);
                list.add(bean);
            }catch(NumberFormatException ex){
                System.err.println("Number format exception");
            }
        }
        this.recordSet.clear();
        return this.conn.insertTemperatureTable(list);
    }
}
