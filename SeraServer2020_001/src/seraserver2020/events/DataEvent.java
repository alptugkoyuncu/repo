/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.events;

import data.NetworkPackage;
import java.util.Date;
import java.util.EventObject;
import seraserver2020.socket.ClientHandler;

/**
 *
 * @author ISIK
 * @param <T>
 */
public class DataEvent extends EventObject{
    Date date;
    ClientHandler owner;
    public DataEvent(NetworkPackage source,ClientHandler owner) {
        super(source);
        date = new Date();
        this.owner = owner;
    }

    public Date getDate() {
        return date;
    }

    public ClientHandler getOwner() {
        return owner;
    }
    @Override
    public NetworkPackage getSource() {
        return (NetworkPackage) super.getSource(); //To change body of generated methods, choose Tools | Templates.
    }

}
