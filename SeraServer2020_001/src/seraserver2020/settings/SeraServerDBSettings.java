/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.settings;

import seraserver2020.ui.main.MainFrame;
import settings.ServerDBSettings;

/**
 *
 * @author ISIK
 */
public class SeraServerDBSettings extends ServerDBSettings{
    
    public SeraServerDBSettings() {
        super(MainFrame.SETTINGS_FOLDER_NAME, MainFrame.SETTINGS_FILE_NAME);
    }
    
}
