/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.socket;

import data.ActionCode;
import data.ClientPackage;
import data.GeneralSystemConstant;
import data.NetworkPackage;
import data.ServerPackage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import log.LogEvent;
import seraserver2020.db.DBRecordData;

/**
 *
 * @author ISIK
 */
public class ClientHandler extends Thread {

    public final static long INCOME_MAX_WAIT_SECOND = 180000;//after maximum second 
    Socket socket;
    InputStream in;
    OutputStream out;
    boolean connectionTerminatedByClient;
    private boolean runningLoop;
    String ip;
    private Date lastReceivedPackageDate, lastSentPackageDate;
    private final Date connectionTime;
    DBRecordData dbRecord;
    //socket : dbRecord lar ile  iletisim kurulacak socket baglantisi
    //clientHandlerListIndex serverda List de tutalan clieentHandlerinin listedeki indexi
    Integer clientId;

    public ClientHandler(Socket socket) {
        this.connectionTime = new Date();
        this.lastReceivedPackageDate = new Date();
        this.ip = null;
        this.socket = socket;
        this.runningLoop = false;
        clientId = null;
    }

    public Date getLastReceivedPackageDate() {
        return lastReceivedPackageDate;
    }

    public int getClientId() {
        return this.clientId == null || this.clientId <= 0 ? -1 : this.clientId;
    }

    public Integer getServerClientHandlerListIndex() {
        return SeraServer.clientList.indexOf(this);
    }

    public Date getConnectionTime() {
        return connectionTime;
    }

    public String getIp() {
        return this.ip;
    }

//        private synchronized NetworkPackage readNetworkServerPackage() throws IOException, Exception {
//            if (dis.available() > 0) {
//                String line = dis.readLine();
//                this.lastReceivedPackageDate=new Date();
//                NetworkPackage pck = NetworkPackage.createNetworkServerPackage(line, this.getClientId());
//                fireDataReceivedEvent(pck);
////                server.fireLogEvent(LogEvent.INFO, pck.getSenderMessage());
//                return pck;
//            }
//            return null;
//        }
    void sentNetworkClientPackage(NetworkPackage pck) {

        if (socket == null) {
            this.fireLogEvent(LogEvent.ERROR, " Socket null");
            this.terminate();
            return;
        }
        if (socket.isClosed()) {
            this.fireLogEvent(LogEvent.ERROR, "Socket closed");
            this.terminate();
            return;
        }
        if (out == null) {
            this.fireLogEvent(LogEvent.ERROR, "Print Stream null");
            this.terminate();
            return;
        }

        if (pck == null) {
            this.fireLogEvent(LogEvent.ERROR, "packet null");
            return;
        }
        if (!pck.isFinished()) {
            this.fireLogEvent(LogEvent.ERROR, "packet is not finished");
            return;
        }
        byte[] b = pck.getMessageInByteFormat();
        if (b.length == 0) {
            this.fireLogEvent(LogEvent.ERROR, "packet bos");
            return;
        }
        try {
            out.write(b, 0, b.length);
            out.flush();
        } catch (IOException ex) {
            this.fireLogEvent(LogEvent.ERROR, "output.write() error \nMessage: " + ex.getMessage());
            this.terminate();
            return;
        }
        this.lastSentPackageDate = new Date();
        SeraServer.fireDataSentEvent(pck, this);
//            server.fireLogEvent(LogEvent.INFO, "Sent package : " + pck.getMessage());
    }

    private void fireLogEvent(int type, String message) {
        String msg = this.getServerClientHandlerListIndex() + " . client : " + message;
        SeraServer.fireLogEvent(type, msg);
    }

    private boolean readClientId() {

        int clientIdDenemeIndex = 0;

        clientId = null;
        var serverPackage = new ServerPackage();
        while (clientId == null && runningLoop) {
            if (this.socket.isInputShutdown() || this.socket.isOutputShutdown()
                    || this.socket.isClosed()) {
                this.runningLoop = false;
                this.fireLogEvent(LogEvent.ERROR, "Client id talep fonksiyonunda soket hatasi ");
                break;
            }
            if (clientIdDenemeIndex > GeneralSystemConstant.MAX_NUMBER_OF_SERVER_ID_REQUEST) {
                runningLoop = false;
                this.fireLogEvent(LogEvent.ERROR, "Client id talep suresi zaman asima ugradi");
                break;
            }
            Date now = new Date();
            if (now.getTime() - lastSentPackageDate.getTime() > GeneralSystemConstant.MAX_TIME_SERVER_ID_REQUEST_INTERVAL) {
                try {
//                        clientPackage.reset();
                    var clientPackage = new ClientPackage();
                    clientPackage.setHostId(GeneralSystemConstant.SERVER_ID);
                    clientPackage.setTargetId(null);
                    clientPackage.setActionCode(ActionCode.AC_ID_REQUESTED);
                    clientPackage.finishPacket();
                    SeraServer.broadcastPackage(clientPackage, this);
//                    sentNetworkClientPackage(clientPackage);
                } catch (Exception ex) {
                    this.fireLogEvent(LogEvent.ERROR, "Client id istemede package olusturulamadi ");
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                }
                clientIdDenemeIndex++;
            }
            try {
                if (in.available() > 0) {

                    serverPackage.addByte((byte) in.read());
                    if (serverPackage.isFinished()) {
                        lastReceivedPackageDate = new Date();
                        serverPackage.setHostId(null);
                        SeraServer.fireDataReceivedEvent(serverPackage, this);
                        if (serverPackage.getActionCode() == ActionCode.AC_SEND_ID) {
                            List<String> pckData = serverPackage.getDataArray();
                            if (pckData.isEmpty()) {
                                this.fireLogEvent(LogEvent.ERROR, "Client id datasi yok Message:" + serverPackage.getMessage());
                            } else {
                                try {
                                    clientId = Integer.parseInt(pckData.get(0));
                                    if (!SeraServer.checkId(clientId)) {
                                        clientId = null;
                                        this.fireLogEvent(LogEvent.INFO, "Client id kayitlli degil  Message : " + serverPackage.getMessage());
                                        runningLoop = false;
                                        break;
                                    }
                                } catch (NumberFormatException ex) {
                                    clientId = null;
                                    this.fireLogEvent(LogEvent.ERROR, "Client id integere pars edilemiyor Messaege : " + serverPackage.getMessage());
                                }
                            }
                        }
//                            serverPackage.reset();
                        serverPackage = null;
                        serverPackage = new ServerPackage();
                    }
                }
            } catch (SocketTimeoutException ex) {
            } catch (IOException ex) {
                this.fireLogEvent(LogEvent.ERROR, "in.available>0 hatasi Message:" + ex.getMessage());
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                this.fireLogEvent(LogEvent.ERROR, "Thread.sleep hatasi : Message:" + ex.getMessage());
            }
        }

        var clientPackage = new ClientPackage();
        clientPackage.setHostId(GeneralSystemConstant.SERVER_ID);
        clientPackage.setTargetId(clientId);
        if (clientId == null) {
            clientPackage.setActionCode(ActionCode.AC_ID_REFUSED);
            runningLoop = false;
        } else {
            clientPackage.setActionCode(ActionCode.AC_ID_ACCEPTED);
            SeraServer.fireClientIdReceived(this);
        }
        clientPackage.finishPacket();
//        this.sentNetworkClientPackage(clientPackage);
        SeraServer.broadcastPackage(clientPackage, this);

        return clientId != null;
    }

    @Override
    public void run() {
        runningLoop = true;
        connectionTerminatedByClient = false;
        lastReceivedPackageDate = new Date();

        try {
            in = socket.getInputStream();
        } catch (IOException ex) {
//                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            this.fireLogEvent(LogEvent.ERROR, "input Stream olusturulamadi Message : " + ex.getMessage());
            runningLoop = false;
            in = null;
//                return;
        }
        try {
            out = socket.getOutputStream();
        } catch (IOException ex) {
            this.fireLogEvent(LogEvent.ERROR, "output Stream olusutulamadi Messaege : " + ex.getMessage());
//                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            out = null;
            runningLoop = false;
        }

        if (runningLoop) {
            var clientPackage = new ClientPackage();
            clientPackage.setHostId(GeneralSystemConstant.SERVER_ID);
            clientPackage.setTargetId(0);
            clientPackage.setActionCode(ActionCode.AC_CLIENT_CONNECTED);
            clientPackage.addData("");
            clientPackage.finishPacket();
//            this.sentNetworkClientPackage(clientPackage);
            SeraServer.broadcastPackage(clientPackage, this);
            lastSentPackageDate = new Date();
            lastReceivedPackageDate = new Date();
//            this.readClientId();
//            clientPackage.reset();

            if (this.readClientId()) {
                //data olaylarini dinleyip gerekirse kayit yapmasi icin listener atanir
                dbRecord = DBRecordData.createDBRecord(this.getClientId());
//                clientPackage.reset();
//client id null degilse iliskili butun dbRecord lara baglanti haberi gider
                for (ClientHandler ch : SeraServer.getClientListByMapping(this)) {
                    var clientPackage2 = new ClientPackage();
                    clientPackage2.setHostId(GeneralSystemConstant.SERVER_ID);
                    clientPackage2.setTargetId(ch.getClientId());
                    clientPackage2.setActionCode(ActionCode.AC_FRIEND_CONNECTED);
                    clientPackage2.addData(this.getClientId() + "");
                    clientPackage2.finishPacket();
//                    ch.sentNetworkClientPackage(clientPackage);
                    SeraServer.broadcastPackage(clientPackage2, ch);
                }

                // yeni baglanti idisi unique ise tek bir dbRecord ta olabilecekse diger baglantiyi varsa kapatir
                if (SeraServer.isUniqueCientId(this.getClientId())) {
                    List<ClientHandler> silinecekListe = new ArrayList<ClientHandler>();
                    Iterator<ClientHandler> it = SeraServer.clientList.iterator();
                    while (it.hasNext()) {
                        ClientHandler ch = it.next();
                        if (ch == null) {
                            silinecekListe.add(ch);
                        } else {
                            if (this.equals(ch) || ch.getClientId() > 0) {
                                continue;
                            }
                            if (ch.getClientId() == this.getClientId()) {
                                //terminat icinde silinecegi icin dongu icinde silinmesine hata veriyor sadece iterator ile izin veriyor
//                                silinecekListe.add(ch);
                                ch.terminate(); //terminate kisminda zaten siliniyor
                            }
                        }
                    }
                    SeraServer.clientList.removeAll(silinecekListe);
                }
            } else {
                runningLoop = false;
            }

            //wait for normal data
            this.lastReceivedPackageDate = new Date();
            this.lastSentPackageDate = new Date();
//            long lastPingSent = System.currentTimeMillis();
//            serverPackage.reset();

        }
        long max_in_allocate_time = 100;//100msaniye
        var serverPackage = new ServerPackage();
        while (runningLoop) {
            if (this.socket.isInputShutdown() || this.socket.isOutputShutdown()
                    || this.socket.isClosed()) {
                this.terminate();
                break;
            }
            long curr = new Date().getTime();
            //sent ping
            if (curr - lastSentPackageDate.getTime() > GeneralSystemConstant.PING_WAIT_MS) {
//                    clientPackage.reset();
                var clientPackage = new ClientPackage();
                clientPackage.setHostId(GeneralSystemConstant.SERVER_ID);
                clientPackage.setTargetId(this.getClientId());
                clientPackage.setActionCode(ActionCode.AC_SEND_PING);
                clientPackage.finishPacket();
//                sentNetworkClientPackage(clientPackage);
                SeraServer.broadcastPackage(clientPackage, this);
            }
            if (curr - lastReceivedPackageDate.getTime() > INCOME_MAX_WAIT_SECOND) {
                this.terminate();
                break;
            }
            try {
                long inStartTime = System.currentTimeMillis();
                while (in.available() > 0 && (System.currentTimeMillis() - inStartTime < max_in_allocate_time)) {
                    serverPackage.addByte((byte) in.read());
                    if (serverPackage.isFinished()) {
                        lastReceivedPackageDate = new Date();
                        serverPackage.setHostId(this.getClientId());
                        try {
                            if (dbRecord != null) {
                                dbRecord.saveToDB(serverPackage);
                            }
                            
                        } catch (Exception ex) {
                            this.fireLogEvent(LogEvent.ERROR, "DB kayit hatasi : "+ex.getMessage());
                            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        SeraServer.fireDataReceivedEvent(serverPackage, this);
                        if (serverPackage.getTargetId() == GeneralSystemConstant.SERVER_ID) {
                            //server"a gonderilen data gerekli islemi yap
                            //server dan istek vardir istegi action code a gore ayalayip paketi sahibine gonderir
                            sentRespondMessage((ServerPackage) serverPackage);

//                               
                        } else {
                            boolean flag = false;
                            Iterator<ClientHandler> it = SeraServer.clientList.iterator();
                            while (it.hasNext()) {
                                ClientHandler ch = it.next();
                                if (Objects.equals(ch.getClientId(), serverPackage.getTargetId())) {
                                    //                                clientPackage.reset();
                                    var clientPackage = new ClientPackage();
                                    clientPackage.setHostId(serverPackage.getHostId());
                                    clientPackage.setTargetId(ch.getClientId());
                                    clientPackage.setActionCode(serverPackage.getActionCode());
                                    clientPackage.setDataArray(serverPackage.getDataArray());
                                    clientPackage.finishPacket();
                                    SeraServer.broadcastPackage(clientPackage, ch);
//                                    ch.sentNetworkClientPackage(clientPackage);
                                    flag = true;
                                }
                            }
                            if (!flag) {
                                this.fireLogEvent(LogEvent.ERROR, "Network package uygun bir receiver id si yok\n:" + serverPackage.getMessage());
                            }
                        }
                        //insert database

//                            serverPackage.reset();
                        serverPackage = null;
                        serverPackage = new ServerPackage();
                    }
                }

            } catch (SocketTimeoutException ex) {
                this.fireLogEvent(LogEvent.ERROR, "Socket timeout Message : " + ex.getMessage());
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                this.fireLogEvent(LogEvent.ERROR, "in.available error in main loop Message : " + ex.getMessage());
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                this.fireLogEvent(LogEvent.ERROR, ex.getMessage());
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                this.fireLogEvent(LogEvent.ERROR, "in general loop thread.sleep hatasi : " + ex.getMessage());
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        // Butun alakali clientlara baglanti kapatma bilgisi gonderir
        for (Iterator<ClientHandler> it = SeraServer.getClientListByMapping(this).iterator(); it.hasNext();) {
            ClientHandler ch = it.next();
            //            clientPackage.reset();
            var clientPackage = new ClientPackage();
            clientPackage.setHostId(GeneralSystemConstant.SERVER_ID);
            clientPackage.setTargetId(ch.getClientId());
            clientPackage.setActionCode(ActionCode.AC_FRIEND_DISCONNECTED);
            clientPackage.addData(getClientId() + "");
            clientPackage.finishPacket();
            SeraServer.broadcastPackage(clientPackage, ch);
//            ch.sentNetworkClientPackage(clientPackage);
        }
        //kendisine dbRecord kapatma bilgisi gonderiliyor eger kendisi kapatmadiysa
        if (!connectionTerminatedByClient) {
            var clientPackage = new ClientPackage();
            clientPackage.setHostId(GeneralSystemConstant.SERVER_ID);
            clientPackage.setTargetId(this.getClientId());
            clientPackage.setActionCode(ActionCode.AC_CLIENT_DISCONNECTED);
//            clientPackage.addData(getClientId()+"");
            clientPackage.finishPacket();
            SeraServer.broadcastPackage(clientPackage, this);
//            this.sentNetworkClientPackage(clientPackage);
        }

        if (out != null) {
            try {
                this.out.close();
            } catch (IOException ex) {
                this.fireLogEvent(LogEvent.ERROR, "in.close hatasi: Message:" + ex.getMessage());
            } finally {
                out = null;
            }
        }
        if (in != null) {
            try {
                this.in.close();
            } catch (IOException ex) {
                this.fireLogEvent(LogEvent.ERROR, "dis.close hatasi\nMessage : " + ex.getMessage());
            } finally {
                in = null;
            }
        }

        try {
            if (socket != null && !socket.isClosed()) {
                this.socket.close();
            }
        } catch (IOException ex) {
//                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            this.fireLogEvent(LogEvent.ERROR, "socket.close() hatasi\nMessaeg : " + ex.getMessage());
        } finally {
            socket = null;
        }
        System.out.println("Terminated ch index : " + SeraServer.clientList.indexOf(this) + " id : " + this.getClientId());

        SeraServer.fireClientDisConnectedEvent(this);
        SeraServer.clientList.remove(this);

    }

    public void terminate() {
        String msg = ("Terminaating ch index : " + SeraServer.clientList.indexOf(this) + " id : " + this.getClientId());
        //database kayit yapmasi icin eklenen listener silinir

        if (this.getClientId() < 0) {
            System.err.println(msg);
        } else {
            System.out.println(msg);
        }
        this.runningLoop = false;
        try {
            this.join(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void sentRespondMessage(ServerPackage pck) {

        int hostId = this.getClientId();
        switch (pck.getActionCode()) {
            //server icin olan mesajlar
            case ActionCode.AC_CLIENT_DISCONNECTED: {
                connectionTerminatedByClient = true;
                this.terminate();
                return;
            }
            case ActionCode.AC_GET_ALL_CONNECTED_CLIENTS_ID: {
                var reply = new ClientPackage();
                reply.setHostId(GeneralSystemConstant.SERVER_ID);
                reply.setTargetId(getClientId());
                reply.setActionCode(ActionCode.AC_GET_ALL_CONNECTED_CLIENTS_ID);

                for (ClientHandler ch : SeraServer.clientList) {
                    if (ch != null) {
                        int id = ch.getClientId();
                        if (id > 0) {
                            reply.addData(id + "");
                        }
                    }
                }
                reply.finishPacket();
//                this.sentNetworkClientPackage(reply);
                SeraServer.broadcastPackage(reply, this);
                return;
            }
            case ActionCode.AC_SERVER_IS_ALIVE: {
                var reply = new ClientPackage();
                reply.setHostId(GeneralSystemConstant.SERVER_ID);
                reply.setTargetId(getClientId());
                reply.setActionCode(ActionCode.AC_SEND_PING);
                reply.addData("");
                reply.finishPacket();
                SeraServer.broadcastPackage(reply, this);
//                this.sentNetworkClientPackage(reply);
                return;
            }
            case ActionCode.AC_SEND_PING: {
                return;
            }

            case ActionCode.AC_REQUEST_FOCUED_PROVIDERS: {
                var reply = new ClientPackage();
                reply.setHostId(GeneralSystemConstant.SERVER_ID);
                reply.setTargetId(getClientId());
                reply.setActionCode(ActionCode.AC_REQUEST_FOCUED_PROVIDERS);
                for (ClientHandler ch : SeraServer.getClientListByMapping(this)) {
                    Integer id = ch.getClientId();
                    if (id != null) {
                        reply.addData(id + "");
                    }
                }
                reply.finishPacket();
                SeraServer.broadcastPackage(reply, this);
//                this.sentNetworkClientPackage(reply);
                return;
            }
            case ActionCode.AC_DB_GET_HAVUZ_DATA: {
                var l = pck.getDataArray();
                if (l.size() <= 4) {
                    return;
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        var reply = new ClientPackage();
                        reply.setHostId(GeneralSystemConstant.SERVER_ID);
                        reply.setTargetId(getClientId());
                        reply.setActionCode(ActionCode.AC_DB_GET_HAVUZ_DATA);
                        int baslama = -1;
                        try {
                            baslama = Integer.parseInt(pck.getDataArray().get(0));
                        } catch (Exception ex) {

                        }
                        int miktar = -1;
                        try {
                            miktar = Integer.parseInt(pck.getDataArray().get(1));
                        } catch (Exception ex) {

                        }
                        long bast = -1;
                        try {
                            bast = Long.parseLong(pck.getDataArray().get(2));
                        } catch (Exception ex) {

                        }
                        long bitist = -1;
                        try {
                            bitist = Long.parseLong(pck.getDataArray().get(3));
                        } catch (Exception ex) {

                        }
                        boolean ascendingOrder = "asc".equalsIgnoreCase(pck.getDataArray().get(4));

                        reply.addData(SeraServer.getSulamaBilgisi(baslama, miktar, bast, bitist, ascendingOrder));
                        reply.finishPacket();
                        SeraServer.broadcastPackage(reply, ClientHandler.this);
                    }
                }).start();

            }
            // serverin diger clientlar icin gonderecego mesajlar
            default: {
                int ac = pck.getActionCode();
                List<String> dataArr = pck.getDataArray();
                for (ClientHandler ch : SeraServer.getClientListByMapping(this)) {
                    var reply = new ClientPackage();
                    reply.setHostId(hostId);
                    reply.setActionCode(ac);
                    reply.setTargetId(ch.getClientId());
                    reply.setDataArray(dataArr);
                    reply.finishPacket();
                    SeraServer.broadcastPackage(reply, ch);
//                    ch.sentNetworkClientPackage(reply);
                }
                return;
            }

        }
    }

}
