/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.socket;

/**
 *
 * @author ISIK
 */
public enum ClientType {
    SERVER,
    TEMP_CONTROL,
    HAVUZ_KONTROL,
    WATCHER;

    public static ClientType getClientType(int clientId) {
        if (clientId == 1000) {
            return SERVER;
        } else if (clientId > 1000 && clientId < 1100) {
            return TEMP_CONTROL;
        } else if (clientId >= 1100 && clientId < 2000) {
            return HAVUZ_KONTROL;
        } else if (clientId >= 2000 && clientId < 3000) {
            return WATCHER;
        } else {
            return null;
        }
    }
}
