/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package seraserver2020.ui.settings;

import db.bean.AlanBean;
import db.bean.BolgeBean;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import seraserver2020.db.DBConnection;
import seraserver2020.db.MySqlServerConnection;

/**
 *
 * @author alptug
 */
public class AlanTable extends JTable {

    protected JPopupMenu popupMenu;
    protected JMenuItem menuItemAdd, menuItemRemove, menuItemCancel, menuItemRecord;

    AlanTableModel dataModel;

    public AlanTable() {
        dataModel = new AlanTableModel();
        this.setModel(dataModel);
        DBConnection conn = new MySqlServerConnection();
        List<BolgeBean> bolgeList = conn.getAllBolge();
        JComboBox<BolgeBean> jcb = new JComboBox<>(bolgeList.toArray(new BolgeBean[0]));
        jcb.setRenderer(new ListCellRenderer<BolgeBean>() {
            @Override
            public Component getListCellRendererComponent(JList<? extends BolgeBean> list, BolgeBean value, int index, boolean isSelected, boolean cellHasFocus) {
                String val = value != null ? value.getName() : "";
                return getCellLabel(val, AlanTableModel.ORIGINAL_DATA, isSelected, true);

            }
        });
        this.setDefaultEditor(BolgeBean.class, new DefaultCellEditor(jcb));
        
        this.setDefaultRenderer(Integer.class, new TableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                String val = value != null ? value.toString() : "";
                int pos = dataModel.getRowPosition(row);
                 boolean isChanged = (pos == AlanTableModel.UPDATED_DATA)?
                        !Objects.equals(dataModel.getValueAt(row, column),dataModel.getOriginalValueAt(row, column)) : false;
                return getCellLabel(val, pos, isSelected, isChanged);
            }
        });
        this.setDefaultRenderer(String.class, new TableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                String val = value != null ? value.toString() : "";
                int pos = dataModel.getRowPosition(row);
                 boolean isChanged = (pos == AlanTableModel.UPDATED_DATA)?
                        !Objects.equals(dataModel.getValueAt(row, column),dataModel.getOriginalValueAt(row, column)) : false;
                return getCellLabel(val, pos, isSelected, isChanged);
            }
        });
        this.getColumnModel().getColumn(0).setCellRenderer(new TableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                String val = value == null || (Integer) value <= 0 ? "*" : value.toString();
                int pos = dataModel.getRowPosition(row);
                 boolean isChanged = (pos == AlanTableModel.UPDATED_DATA)?
                        !Objects.equals(dataModel.getValueAt(row, column),dataModel.getOriginalValueAt(row, column)) : false;
                return getCellLabel(val, pos, isSelected, isChanged);
            }
        });
this.setDefaultRenderer(BolgeBean.class, new TableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                String val = value != null ? ((BolgeBean) value).getName() : "";
                int pos = dataModel.getRowPosition(row);
                boolean isChanged = (pos == AlanTableModel.UPDATED_DATA)?
                        !Objects.equals(dataModel.getValueAt(row, column),dataModel.getOriginalValueAt(row, column)) : false;
                return getCellLabel(val, pos, isSelected, isChanged);
            }
        });
        popupMenu = new JPopupMenu();
        menuItemAdd = new JMenuItem("Yeni Ekle");
        menuItemRemove = new JMenuItem("Seçili satiri Sil");
        menuItemCancel = new JMenuItem("Seçili Güncellemeyi İptal Et");
        menuItemRecord = new JMenuItem("Kaydet");
//        JMenuItem menuItemAllCancel = new JMenuItem("Tüm Güncellemeri İptal et");
        this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        popupMenu.add(menuItemAdd);
        popupMenu.add(menuItemRemove);
        popupMenu.add(menuItemCancel);
        popupMenu.add(menuItemRecord);
//        popupMenu.add(menuItemAllCancel);
        super.setComponentPopupMenu(popupMenu);
        super.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {

                Point point = e.getPoint();
                int currentRow = rowAtPoint(point);
                setRowSelectionInterval(currentRow, currentRow);
            }

        });

        menuItemAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dataModel.addNewRow();
            }
        });
        menuItemCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dataModel.cancelUpdate(AlanTable.this.getSelectedRow());
            }
        });
        menuItemRemove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dataModel.removeRow(AlanTable.this.getSelectedRow());
            }
        });
        menuItemRecord.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save();
            }

        });
    }

    private void save() {
        List<AlanBean> newRows = new ArrayList<>();
        List<AlanBean> updatedRows = new ArrayList<>();
        List<AlanBean> deletedRows = new ArrayList<>();

        for (int i = 0; i < dataModel.getRowCount(); i++) {
            AlanBean a = dataModel.getValue(i);
            switch (dataModel.getRowPosition(i)) {
                case AlanTableModel.NEW_DATA: {
                    if (a.getName() != null && !a.getName().isBlank() && a.getActionCode() != null && a.getIdClient() != null && a.getBolge() != null) {
                        newRows.add(a);
                    }
                }
                break;
                case AlanTableModel.UPDATED_DATA:
                    if (a.getName() != null && !a.getName().isBlank() && a.getActionCode() != null && a.getIdClient() != null && a.getBolge() != null) {
                        updatedRows.add(a);
                    }
                    break;
                case AlanTableModel.DELETED_DATA: {
                    if (a.getId() != null && a.getId() > 0) {
                        deletedRows.add(a);
                    }
                }
                break;
                case AlanTableModel.ORIGINAL_DATA:
                    break;
                default:
                    break;
            }
        }
        DBConnection conn = new MySqlServerConnection();
        if (!newRows.isEmpty()) {
            conn.insertIntoAlan(newRows);
        }
        if (!updatedRows.isEmpty()) {
            conn.updateAlan(updatedRows);
        }
        if (!deletedRows.isEmpty()) {
            conn.deleteFromAlan(deletedRows);
        }
        dataModel = new AlanTableModel();
        this.setModel(dataModel);
    }

    private JLabel getCellLabel(String val, int pos, boolean isSelected, boolean isChanged) {
        JLabel l = new JLabel(val);
        l.setOpaque(true);
        if (isSelected) {
            l.setBackground(this.getSelectionBackground());
            l.setForeground(this.getSelectionForeground());
        } else {
            l.setBackground(this.getBackground());
            l.setForeground(this.getForeground());
        }
        l.setFont(l.getFont().deriveFont(Font.PLAIN));
        Font f = l.getFont();
        switch (pos) {
            case AlanTableModel.NEW_DATA:
                f = f.deriveFont(Font.BOLD);
                break;

            case AlanTableModel.DELETED_DATA:
                l.setForeground(Color.red);
                break;

            case AlanTableModel.ORIGINAL_DATA:
                f = f.deriveFont(Font.PLAIN);
                break;

            case AlanTableModel.UPDATED_DATA:
                if (isChanged) {
                    f = f.deriveFont(Font.BOLD);
                }
                break;

            default:
        }
        l.setFont(f);
        return l;
    }

    class AlanTableModel extends AbstractTableModel {

        public final static int NEW_DATA = 0;
        public final static int ORIGINAL_DATA = 1;
        public final static int DELETED_DATA = 2;
        public final static int UPDATED_DATA = 3;
        List<AlanBean> aList;
        List<AlanBean> originalList;
        List<Integer> pos;
        final String[] cols;

        public AlanTableModel() {
            cols = new String[]{"id", "Bolge", "Alan Adi", "Client id", "Action Code"};
            DBConnection conn = new MySqlServerConnection();
            originalList = conn.getAllAlan();
            aList = new ArrayList<>();
            pos = new ArrayList<>();
            for (AlanBean b : originalList) {
                aList.add(b.getClone());
                pos.add(ORIGINAL_DATA);
            }
            if (originalList.isEmpty()) {
                addNewRow();
            }
        }

        public void addNewRow() {
            AlanBean b = new AlanBean();
            aList.add(b);
            pos.add(NEW_DATA);
            fireTableRowsInserted(0, 0);
        }

        @Override
        public int getRowCount() {
            return aList.size();
        }

        @Override
        public int getColumnCount() {
            return cols.length;
        }

        @Override
        public String getColumnName(int column) {
            return cols[column];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return switch (columnIndex) {
                case 0 ->
                    Integer.class;
                case 1 ->
                    BolgeBean.class;
                case 2 ->
                    String.class;
                case 3 ->
                    Integer.class;
                case 4 ->
                    Integer.class;
                default ->
                    super.getColumnClass(columnIndex);
            };
        }

        public Object getOriginalValueAt(int rowIndex, int columnIndex) {
            AlanBean b = this.originalList.get(rowIndex);
            return switch (columnIndex) {
                case 0 ->
                    b.getId();
                case 1 ->
                    b.getBolge();
                case 2 ->
                    b.getName();
                case 3 ->
                    b.getIdClient();
                case 4 ->
                    b.getActionCode();
                default ->
                    null;
            };
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            AlanBean b = aList.get(rowIndex);
            return switch (columnIndex) {
                case 0 ->
                    b.getId();
                case 1 ->
                    b.getBolge();
                case 2 ->
                    b.getName();
                case 3 ->
                    b.getIdClient();
                case 4 ->
                    b.getActionCode();
                default ->
                    null;
            };
        }

        public AlanBean getValue(int rowIndex) {
            return aList.get(rowIndex);
        }

        public AlanBean getOriginalValue(int rowIndex) {
            return originalList.get(rowIndex);
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return pos.get(rowIndex) != DELETED_DATA && columnIndex != 0;
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            AlanBean b = aList.get(rowIndex);
            switch (columnIndex) {
                case 1 ->
                    b.setBolge((BolgeBean) aValue);
                case 2 ->
                    b.setName((String) aValue);
                case 3 ->
                    b.setIdClient((int) aValue);
                case 4 ->
                    b.setActionCode((int) aValue);
                default -> {
                }
            }
            updateRowPostition(rowIndex, columnIndex);
        }

        public int getRowPosition(int row) {
            return pos.get(row);
        }

        private void updateRowPostition(int rowIndex, int columnIndex) {
            if (pos.get(rowIndex) == NEW_DATA || pos.get(rowIndex) == DELETED_DATA) {
                return;
            }
            AlanBean alan = aList.get(rowIndex);
            AlanBean orj = this.originalList.get(rowIndex);
            switch (columnIndex) {
                case 1 ->
                    pos.set(rowIndex, alan.getBolge().equals(orj.getBolge()) ? ORIGINAL_DATA : UPDATED_DATA);
                case 2 ->
                    pos.set(rowIndex, alan.getName().equals(orj.getName()) ? ORIGINAL_DATA : UPDATED_DATA);
                case 3 ->
                    pos.set(rowIndex, Objects.equals(alan.getIdClient(), orj.getIdClient()) ? ORIGINAL_DATA : UPDATED_DATA);
                case 4 ->
                    pos.set(rowIndex, Objects.equals(alan.getActionCode(), orj.getActionCode()) ? ORIGINAL_DATA : UPDATED_DATA);
            }
        }

        private void cancelUpdate(int selectedRow) {
            int p = pos.get(selectedRow);
            if (p == DELETED_DATA) {
                pos.set(selectedRow, ORIGINAL_DATA);
                fireTableRowsUpdated(selectedRow, selectedRow);
            } else if (p == NEW_DATA) {
                removeRow(selectedRow);
            } else if (p == UPDATED_DATA) {
                AlanBean orj = this.originalList.get(selectedRow);
                AlanBean alan = this.aList.set(selectedRow, orj.getClone());
                pos.set(selectedRow, ORIGINAL_DATA);
                fireTableRowsUpdated(selectedRow, selectedRow);
            }
        }

        private void removeRow(int selectedRow) {
            if (pos.get(selectedRow) == NEW_DATA) {
                aList.remove(selectedRow);
                pos.remove(selectedRow);
                fireTableRowsDeleted(selectedRow, selectedRow);
            } else {
                pos.set(selectedRow, DELETED_DATA);
                fireTableRowsUpdated(selectedRow, selectedRow);
            }
            if (aList.isEmpty()) {
                addNewRow();
            }
        }

    }
}
