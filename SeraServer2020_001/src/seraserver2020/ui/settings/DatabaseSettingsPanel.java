/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.ui.settings;

import seraserver2020.settings.SeraServerDBSettings;
import seraserver2020.socket.SeraServer;
import settings.ServerDBSettings;

/**
 *
 * @author ISIK
 */
public class DatabaseSettingsPanel extends javax.swing.JPanel implements SaveableSettingsPanel {

    /**
     * Creates new form DatabaseSettingsPanel
     */
    ServerDBSettings dbSettings;

    public DatabaseSettingsPanel() {
//        dbSettings = new SeraServerDBSettings();
dbSettings = SeraServer.DATABASE_SETTINGS;
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jpReNewPassword = new javax.swing.JPasswordField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jtxtUrl = new javax.swing.JTextField();
        jtxtDriver = new javax.swing.JTextField();
        jtxtUsername = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jpOldpassword = new javax.swing.JPasswordField();
        jpNewPassword = new javax.swing.JPasswordField();

        jLabel1.setText("URL");

        jLabel2.setText("Driver");

        jLabel3.setText("User Name");

        jLabel4.setText("Old Password");

        jLabel5.setText("new Password");

        jLabel6.setText("New Password");

        jpNewPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jpNewPasswordActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jpNewPassword, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jpOldpassword, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jtxtUsername, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jtxtDriver, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jtxtUrl, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jpReNewPassword, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jtxtUrl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2))
                    .addComponent(jtxtDriver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jtxtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jpOldpassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jpNewPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jpReNewPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jpNewPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jpNewPasswordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jpNewPasswordActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPasswordField jpNewPassword;
    private javax.swing.JPasswordField jpOldpassword;
    private javax.swing.JPasswordField jpReNewPassword;
    private javax.swing.JTextField jtxtDriver;
    private javax.swing.JTextField jtxtUrl;
    private javax.swing.JTextField jtxtUsername;
    // End of variables declaration//GEN-END:variables

    @Override
    public ButtonPanelEvent save() {
        char[] oldPassword = this.jpOldpassword.getPassword();
        if (oldPassword.length == 0) {
            this.jpOldpassword.setText("");
            this.jpNewPassword.setText("");
            this.jpReNewPassword.setText("");
            return new ButtonPanelEvent(ButtonPanelEvent.EVENT_FAILED, "Herhangi bir işlem için eski şifre giriniz");
        }
        boolean flag = oldPassword.length == dbSettings.getPassword().length();
        for (int i = 0; i < oldPassword.length && flag; i++) {
            if (oldPassword[i] != dbSettings.getPassword().charAt(i)) {
                this.jpOldpassword.setText("");
                this.jpNewPassword.setText("");
                this.jpReNewPassword.setText("");
                return new ButtonPanelEvent(ButtonPanelEvent.EVENT_FAILED, "Hatalı şifre girildi");
            }
        }

        char[] newPassword = this.jpNewPassword.getPassword();
        if (newPassword.length > 0) {
            char[] rePassword = this.jpReNewPassword.getPassword();
            flag = rePassword.length == newPassword.length;
            for (int i = 0; i < newPassword.length && flag; i++) {
                if (rePassword[i] != newPassword[i]) {
                    flag = false;
                    break;
                }
            }
            if (!flag) {
                this.jpOldpassword.setText("");
                this.jpNewPassword.setText("");
                this.jpReNewPassword.setText("");
                return new ButtonPanelEvent(ButtonPanelEvent.EVENT_FAILED, "Yeni şifre hatalı");

            }
        }
        char[] tmp = newPassword.length > 0 ? newPassword : oldPassword;
        StringBuilder password = new StringBuilder();
        password.append(tmp, 0, tmp.length);
        dbSettings.setDriver(this.jtxtDriver.getText().trim());
        dbSettings.setPassword(password.toString().trim());
        dbSettings.setUrl(this.jtxtUrl.getText().trim());
        dbSettings.setUserName(this.jtxtUsername.getText().trim());
        if (dbSettings.save()) {
            this.load();
            return new ButtonPanelEvent(ButtonPanelEvent.EVENT_SUCCESS, "Database bigileri işlemi başarıyla kaydedildi");

        } else {
            return new ButtonPanelEvent(ButtonPanelEvent.EVENT_FAILED, "Database bilgileri kayıt sırasında hata oluştu");
        }
    }

    @Override
    public ButtonPanelEvent load() {
        dbSettings.load();
        this.jtxtUrl.setText(dbSettings.getUrl());
        this.jtxtDriver.setText(dbSettings.getDriver());
        this.jtxtUsername.setText(dbSettings.getUserName());
        this.jpOldpassword.setText(dbSettings.getPassword());
        this.jpNewPassword.setText("");
        this.jpReNewPassword.setText("");
        return new ButtonPanelEvent(ButtonPanelEvent.EVENT_SUCCESS, "Database bilgileri başarıyle yüklendi");

    }

    @Override
    public ButtonPanelEvent resetToDefault() {
        dbSettings.resetToDefault();
        return this.load();
    }

    @Override
    public ButtonPanelEvent reset() {
        return this.load();
    }
}
