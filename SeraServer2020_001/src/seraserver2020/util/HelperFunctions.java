/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2020.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ISIK
 */
public class HelperFunctions {

    public static String getIP() {
        String[] EXTERNAL_IP_SORGULAMA_ADDRESI = {
            "http://checkip.amazonaws.com/",
            "http://checkip.amazonaws.com/",
            "http://icanhazip.com/",
            "http://www.trackip.net/ip",
            "http://myexternalip.com/raw",
            "http://ipecho.net/plain",
            "http://bot.whatismyipaddress.com"
        };
        URL whatismyip;
        String ip;
        int i = 0;
        while (i < EXTERNAL_IP_SORGULAMA_ADDRESI.length) {

            ip = null;
            whatismyip = null;
            try {
                whatismyip = new URL(EXTERNAL_IP_SORGULAMA_ADDRESI[i]);
                BufferedReader in;
                try {
                    in = new BufferedReader(new InputStreamReader(
                            whatismyip.openStream()));

                    ip = in.readLine(); //you get the IP as a String
                    System.out.println(ip);
                } catch (IOException ex) {
                    Logger.getLogger(HelperFunctions.class.getName()).log(Level.SEVERE, null, ex);
                }

            } catch (MalformedURLException ex) {
                Logger.getLogger(HelperFunctions.class.getName()).log(Level.SEVERE, null, ex);

            }
            i++;
            return ip;
        }
        return null;
    }

    public static DecimalFormat getDecimalFormat() {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormatSymbols.setGroupingSeparator(',');
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00", decimalFormatSymbols);
        
//        System.out.println(decimalFormat.format(1237516.2548)); //1,237,516.25
        return decimalFormat;
    }
}
