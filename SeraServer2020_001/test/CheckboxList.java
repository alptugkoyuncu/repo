/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ISIK
 */
package com.learnjava.swing;
 
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
 
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
 
public class CheckboxList {
 
   public static void main(String args[]) {
      JFrame frame = new JFrame();
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
      // Create a list containing CheckboxListItem's
 
      JList<CheckboxListItem> list = new JList<CheckboxListItem>(
            new CheckboxListItem[] { new CheckboxListItem("apple"),
                  new CheckboxListItem("orange"),
                  new CheckboxListItem("mango"),
                  new CheckboxListItem("paw paw"),
                  new CheckboxListItem("banana") });
 
      // Use a CheckboxListRenderer (see below)
      // to renderer list cells
 
      list.setCellRenderer(new CheckboxListRenderer());
      list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
 
      // Add a mouse listener to handle changing selection
 
     list.addListSelectionListener(new ListSelectionListener() {
          @Override
          public void valueChanged(ListSelectionEvent e) {
              if(!e.getValueIsAdjusting()){
                  int index = list.getSelectedIndex();
                  if(index<0)
                      return;
                  CheckboxListItem item = list.getModel().getElementAt(index);
                  item.setSelected(!item.isSelected());
                  list.clearSelection();
              }
          }
     });
      frame.getContentPane().add(new JScrollPane(list));
      frame.pack();
      frame.setVisible(true);
   }
}
 
// Represents items in the list that can be selected
 
class CheckboxListItem {
   private String label;
   private boolean isSelected = false;
 
   public CheckboxListItem(String label) {
      this.label = label;
   }
 
   public boolean isSelected() {
      return isSelected;
   }
 
   public void setSelected(boolean isSelected) {
      this.isSelected = isSelected;
   }
 
   public String toString() {
      return label;
   }
}
 
// Handles rendering cells in the list using a check box
 
class CheckboxListRenderer extends JCheckBox implements
      ListCellRenderer<CheckboxListItem> {
 
   @Override
   public Component getListCellRendererComponent(
         JList<? extends CheckboxListItem> list, CheckboxListItem value,
         int index, boolean isSelected, boolean cellHasFocus) {
      setEnabled(list.isEnabled());
      setSelected(value.isSelected());
      setFont(list.getFont());
      if(value.isSelected()){
          setBackground(list.getSelectionBackground());
          setForeground(list.getSelectionForeground());
      }else{
      setBackground(list.getBackground());
      setForeground(list.getForeground());
      }setText(value.toString());
      return this;
   }
}
