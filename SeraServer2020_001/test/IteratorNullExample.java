
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ISIK
 */
public class IteratorNullExample {
    public int a;

    @Override
    public String toString() {
        return a+""; //To change body of generated methods, choose Tools | Templates.
    }
    public IteratorNullExample(int a) {
        this.a = a;
    }
    public static void main(String[] args) {
        List<IteratorNullExample> l = new ArrayList<>();
        IteratorNullExample[] l2 = new IteratorNullExample[10];
        for(int i=0; i<10;i++){
            l2[i] = new IteratorNullExample(i);
            if(i==5)
                l.add(null);
            else
            l.add(l2[i]);
        }
        int j=0;
        Iterator<IteratorNullExample> it = l.iterator();
        while(it.hasNext()){
            if(it.next()==null)
                it.remove();
        }
//        for(IteratorNullExample it:l){
//            if(it==null)
//                l.remove(it);
//        }
        System.err.println(j);
        
        System.err.println(l);
        Set s =new HashSet();
        s.add(null);
        s.add("asd");
        s.add(null);
        System.out.println("set size : " + s.size()+" set :"+s);
    }
}
