
import db.bean.AlanBean;
import db.bean.BolgeBean;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import db.bean.SulamaBean;
import db.bean.TempBean;
import java.math.BigDecimal;
import seraserver2020.db.DB_Values;
import seraserver2020.db.MySqlServerConnection;
import seraserver2020.settings.SeraServerDBSettings;
import settings.ServerDBSettings;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ISIK
 */
public class TestMySqlDriver {
    static ServerDBSettings sett = new SeraServerDBSettings();
    static MySqlServerConnection conn = new MySqlServerConnection();
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    
//        conn.insertClientActionList(1001, 1000, 10, "10.20", new Date());
//long now = System.currentTimeMillis();
//System.out.println("Start to retrieve data from client action list");
//List<SulamaBean> list = conn.getSulama(0, 3000, false);
//System.out.println("Total time : "+(System.currentTimeMillis()-now)/1000+" s");
//System.out.println("Total Size : "+list.size());
//JSONArray  jArr = new JSONArray (list);
//for(SulamaBean bean:list){
//    JSONObject obj = new JSONObject(bean);
//    jArr.put(obj);
//}
//       System.out.println(jArr);
//        System.out.println(jArr.toString().length());
//        SocketSettings sett2 = new SocketSettings();
//        sett2.resetToDefault();
        

        insertTempTable();
        getAllBolgeAndAlan();
//        insertHavuzData();
    }
    public static void getAllBolgeAndAlan(){
        List<BolgeBean> bolgeList = conn.getAllBolge();
        System.out.println("Bolge list : "+bolgeList);
        List<AlanBean> alanList = conn.getAllAlan();
        System.out.println("Alan List : "+alanList);
    }
    public static void insertTempTable() {
        List<TempBean> beans = new ArrayList<>();
        int clientId = 1001;
        int actionCode;
            double val = 0.1;
            System.out.println(val);
        long now = System.currentTimeMillis();
        now = now/60000;
        now = now*60000;
        boolean arttir=true;
        for (int i = 0; i < 12000; i++) {
            now += 60000;
            if(val>20){
                arttir=false;
            }
            if(val<0){
                arttir=true;
            }
            if(arttir){
                val=val+0.1;
            }else
                val=val-0.1;
            TempBean bean = new TempBean();
            actionCode = 13;
            AlanBean tempAlan = new AlanBean(-1 , null,null,clientId , actionCode);
            bean.setAlan(tempAlan);
            bean.setDate(now);
            bean.setValue(DB_Values.getDBValueOf_TEMP_VALUE(val+""));
            beans.add(bean);
        }
        conn.insertTemperatureTable(beans);
    }
    public static void insertHavuzData(){
        SulamaBean bean = new SulamaBean();
        bean.setBaslama(0);
        bean.setBitis(999999);
        bean.setLow(333333);
        bean.setNone(333333);
        bean.setNormal(333333);
        conn.insertSulamaTable(bean);
        
    }

}
