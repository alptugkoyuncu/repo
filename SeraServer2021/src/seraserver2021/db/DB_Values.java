/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.db;

/**
 *
 * @author ISIK
 */
public final class DB_Values {

    private static double PRESSURE_NONE_BORDER;
    private static double PRESSURE_LOW_BORDER;
    private static int TEMP_PRECION;

    private DB_Values() {

    }

    static {
        getPressureBorderValues();
        getTempPrecision();
    }

    private enum BasincSeviyesi {
        NONE("0"),
        LOW("1"),
        ENOUGH("2");
        private String DB_RECORD_VALUE;

        private BasincSeviyesi(String val) {
            DB_RECORD_VALUE = val;
        }

        public String getDB_RECORD_VALUE() {
            return DB_RECORD_VALUE;
        }

    }
    private static void getTempPrecision(){
        TEMP_PRECION = 1;
    }

    private static void getPressureBorderValues() {
        PRESSURE_LOW_BORDER = 2.5;
        PRESSURE_NONE_BORDER = 2.0;
    }

    public static String getDBValueOf_TEMP_VALUE(String arduinoValue){
        try{
            Double val = Double.parseDouble(arduinoValue);
            return String.format("%."+TEMP_PRECION+"f", val);
        }catch(Exception ex){
            return "";
        }
    }
    public static String getDBValueOf_BASINC_VALUE(String arduinoValue) {
        try {
            double dVal = Double.parseDouble(arduinoValue);
            if (dVal < PRESSURE_NONE_BORDER) {
                return DB_Values.BasincSeviyesi.NONE.getDB_RECORD_VALUE();
            }
            if (dVal < PRESSURE_LOW_BORDER) {
                return DB_Values.BasincSeviyesi.LOW.getDB_RECORD_VALUE();
            }
            return DB_Values.BasincSeviyesi.ENOUGH.getDB_RECORD_VALUE();
        } catch (Exception ex) {
            return "";
        }
    }

    public static String getDbValueOf_STATE_CONTROL(String arduinoValue) {
        try {
            int intVal = Integer.parseInt(arduinoValue) > 0 ? 1 : 0;
            return intVal + "";
        } catch (Exception ex) {
            return "";
        }
    }

}
