/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.db;

import data.NetworkPackage;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Map;
import seraserver2021.socket.ClientType;

/**
 *
 * @author ISIK
 */
public abstract class DbRecordData {
   protected  Map<Integer,String> values;
    private final Map<Integer,String> recordMap;

    public static final String PROP_UPDATEDVALUE = "updatedValue";

    /**
     * Get the value of recordMap
     *
     * @return the value of recordMap
     */
    public Map<Integer,String> getRecordMap() {
        return recordMap;
    }

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    /**
     * Add PropertyChangeListener.
     *
     * @param listener
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Remove PropertyChangeListener.
     *
     * @param listener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    int clientId;// gelen client id ye gore record data ayiklanir
    protected DbRecordData(int clientId){
        this.clientId=clientId;
        this.recordMap=new HashMap<>();
        this.values = new HashMap<>();
    }
    public static DbRecordData createDbRecordData(int clientId){
        switch(ClientType.getClientType(clientId)){
            case TEMP_CONTROL: return new TemperatureDbRecordData(clientId);
            case HAVUZ_KONTROL: return new HavuzKontrolDbRecordData(clientId);
            case WATCHER : return new IzleyiciKontrolDbRecordData(clientId);
            default: return null;
        }
    }
   
    public abstract boolean setValues(NetworkPackage pck) throws Exception;

    protected boolean setValues(Integer actionCode,String value){
        String oldVal = this.values.getOrDefault(actionCode, "");
        if(oldVal.equals(value))
            return false;
        this.values.put(actionCode, value);
        this.recordMap.put(actionCode, value);
        return true;
    }
    public int getClientId() {
        return clientId;
    }

    @Override
    public String toString() {
        return "Client id : "+clientId+" Values : "+values+" Updated Values :"+recordMap;
    }


    public void clearRecordMap() {
        this.recordMap.clear();
    }
    
}
