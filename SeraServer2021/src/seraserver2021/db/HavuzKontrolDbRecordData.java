package seraserver2021.db;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import seraserver2021.db.DbRecordData;
import data.ActionCode;
import data.NetworkPackage;
import java.util.List;

/**
 *
 * @author ISIK
 */
public class HavuzKontrolDbRecordData extends DbRecordData{
    private final static String errorHeader="HavuzKontrolDbRecordData Dosyasi ";
    
 
    
    
    public HavuzKontrolDbRecordData(int clientId) {
        super(clientId);
    }
    
   
    @Override
    public boolean setValues(NetworkPackage pck) throws Exception{
        boolean flag=false;
        List<String> datas = pck.getDataArray();
       ActionCode code = ActionCode.getByValue(pck.getActionCode());
        switch(code){
            case AC_SEND_HAVUZ_ESP_ALL_DATA:{
                if(datas ==null || datas.size()!=4){
                   throw new Exception(errorHeader+" data uzunlugu 4 olmak zorunda Data array :"+pck.getDataArray());
                }
                //role durumunu kaydet
                flag = this.setValues(ActionCode.AC_SEND_RELAY_STATE.getValue(), DB_Values.getDbValueOf_STATE_CONTROL(datas.get(0)));
                //outer timer durumunu kaydet
                flag|=this.setValues(ActionCode.AC_SEND_OUTER_TIMER_STATE.getValue(), DB_Values.getDbValueOf_STATE_CONTROL(datas.get(1)));
                //arduino relay kontrol durumunu kaydet
                flag|=this.setValues(ActionCode.AC_SEND_ARDUINO_RELAY_CONTROL_STATE.getValue(), DB_Values.getDbValueOf_STATE_CONTROL(datas.get(2)));
                //pressure bar kontrol et
                flag|=this.setValues(ActionCode.AC_SEND_PRESSURE_SENSOR_VALUE.getValue(),DB_Values.getDBValueOf_BASINC_VALUE(datas.get(3)));
                return flag;
            }
            case AC_SEND_RELAY_STATE:
            case AC_SEND_OUTER_TIMER_STATE:
            case AC_SEND_ARDUINO_RELAY_CONTROL_STATE:
                if(datas==null || datas.isEmpty())
                    throw new Exception(errorHeader+ "data bos olamaz Data array : "+pck.getDataArray());
                return this.setValues(pck.getActionCode(), DB_Values.getDbValueOf_STATE_CONTROL(datas.get(0)));
            case AC_SEND_PRESSURE_SENSOR_VALUE:
                if(datas==null || datas.isEmpty())
                    throw new Exception(errorHeader+ "data bos olamaz Data array : "+pck.getDataArray());
                return this.setValues(pck.getActionCode(), DB_Values.getDBValueOf_BASINC_VALUE(datas.get(0)));
            default:{
//                throw new Exception(errorHeader+" : hatali actionCode actionCode : "+pck.getActionCode());
                return false;
            }
        }
    }
    
}
