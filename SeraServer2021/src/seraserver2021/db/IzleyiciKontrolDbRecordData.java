/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.db;

import seraserver2021.db.DbRecordData;
import data.ActionCode;
import data.NetworkPackage;

/**
 *
 * @author ISIK
 */
public class IzleyiciKontrolDbRecordData extends DbRecordData{
    private final static String errorHeader="IzleyiciKontrolDbRecordData dosyasi";
    public IzleyiciKontrolDbRecordData(int clientId) {
        super(clientId);
    }

    @Override
    public boolean setValues(NetworkPackage pck) {
        int actionCode = pck.getActionCode();
        switch(actionCode){
//            case ActionCode.AC_DISABLE_OUTER_TIMER:
//            case ActionCode.AC_ENABLE_OUTER_TIMER:
//            case ActionCode.AC_SET_ROLE_AKTIF:
//            case ActionCode.AC_SET_ROLE_INAKTIF:{
                // action kodlarda data olmadigi icin her islem eskisiyle ayni olacaginda setValues islemi yapilmaz ve geriye false degeri doner 
                // bu hatayi endellemek icin value deger her seferinde silinirki her islem kaydedilebilsin
//                this.values.clear();
//                return this.setValues(actionCode, "1");
//            }
            default:
                return false;
        }
    }
    
}
