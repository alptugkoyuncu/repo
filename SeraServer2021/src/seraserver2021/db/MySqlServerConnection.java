/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.db;

import com.mysql.cj.jdbc.CallableStatement;
import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import seraserver2021.socket.SeraServer;
import settings.ServerDBSettings;

/**
 *
 * @author ISIK
 */
public class MySqlServerConnection extends DBConnection {

    private final MysqlDataSource dataSource;
    boolean connectionError;

    public MySqlServerConnection() {
        dataSource = new MysqlDataSource();
        ServerDBSettings serverSettings = SeraServer.DATABASE_SETTINGS;
        dataSource.setURL(serverSettings.getUrl());
        dataSource.setUser(serverSettings.getUserName());
        dataSource.setPassword(serverSettings.getPassword());
    }

    @Override
    protected void connect() {
    }

    @Override
    public int insertClientActionList(Integer[] hostId, Integer[] targetId, Integer[] actionCode, String[] value, Date[] dt) {
        if (!this.isConnectionEnable()) {
            return -1;
        }
        boolean flag = (hostId.length == targetId.length) && (hostId.length == actionCode.length) && actionCode.length == value.length && value.length == dt.length;

        if (!flag) {
            return -1;
        }
        int size = hostId.length;
        int index = 0;
        System.out.println("Try to insert Database(client_action_list) size : " + size);
        int res = 0;
        String sql = "INSERT IGNORE INTO client_action_list(host_id,target_id,action_code,value,created_at) VALUES(?,?,?,?,?)";
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            while (size > index) {
                if (hostId[index] != null && targetId[index] != null && actionCode[index] != null && dt[index] != null) {
                    pstm.setInt(1, hostId[index]);
                    pstm.setInt(2, targetId[index]);
                    pstm.setInt(3, actionCode[index]);
                    pstm.setString(4, value[index]);
                    pstm.setLong(5, dt[index].getTime());
                    pstm.addBatch();
                }
                index++;
            }
            int nos[] = pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            res = 0;
            for (int no : nos) {
                if (no == 1) {
                    res++;
                }
            }
            System.out.println("Insert comleted to Database(client_action_list):" + res);

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            res = -1;
            System.err.println("Insert failed on Database(client_action_list");
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return res;
        }

    }

    @Override
    public int insertClientActionList(Integer hostId, Integer targetId, Integer actionCode, String value, Date dt) {
        return this.insertClientActionList(new Integer[]{hostId}, new Integer[]{targetId}, new Integer[]{actionCode}, new String[]{value}, new Date[]{dt});
    }

    @Override
    public int updateClientList(SortedMap<Integer, Boolean> clientList, boolean deleteExistingRecord) {
        if (!this.isConnectionEnable()) {
            return -1;
        }
        Set<Integer> newClients = clientList.keySet();
        Set<Integer> silinecekClient = getClientList().keySet();
        silinecekClient.removeAll(newClients);
        SortedMap<Integer, Boolean> toRecord = new TreeMap<>(clientList);

        if (!silinecekClient.isEmpty()) {
            //hepsini sil
            if (deleteExistingRecord) {
                for (Integer clientId : silinecekClient) {
                    removeClientActionListByHostId(clientId);
                    removeClientActionListByTargetId(clientId);
                    removeClientMappingByHostId(clientId);
                    removeClientMappingBytargetId(clientId);
                    removeClientList(clientId);
                }
            } else {
                //database de hepsinin sadece aktiligini kapatmak icin ayarla
                for (Integer k : silinecekClient) {
                    toRecord.put(k, false);
                }
            }
        }
        return insertOrUpdateClientList(toRecord);
    }

    @Override
    public int insertOrUpdateClientList(SortedMap<Integer, Boolean> sm) {
        if (!this.isConnectionEnable()) {
            return -1;
        }
        //just insert or update
        int res = 0;
        System.out.println("Try to insert or update Database(client_list) size : " + sm.size());
        String sql = "insert into client_list(id,aktif) VALUES(?,?) ON DUPLICATE KEY UPDATE aktif=?";
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
            for (Integer id : sm.keySet()) {
                pstm.setInt(1, id);
                int val = sm.get(id) ? 1 : 0;
                pstm.setInt(2, val);
                pstm.setInt(3, val);
                pstm.addBatch();
            }
            int nos[] = pstm.executeBatch();
            conn.commit();
            conn.setAutoCommit(true);
            for (int no : nos) {
                if (no == 1) {
                    res++;
                }
            }
            System.out.println(" insert or update completed to Database(client_list) size : " + res);
        } catch (SQLException ex) {
            res = -1;
            System.err.println("Failed to insert or updated to Database(client_list) Value : " + sm);
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return res;
        }
    }

    @Override
    public int insertClientList(Integer clientId, boolean aktif) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int removeClientList(Integer clientId) {
        if (!this.isConnectionEnable()) {
            return -1;
        }
        System.out.println("Try to remove Database(client_list table) at clientId = " + clientId);
        String query = "DELETE FROM `sera_kontrol`.`client_list` WHERE id=" + clientId;
        int res = -1;
        try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement()) {
            res = st.executeUpdate(query);
            System.out.println("Remove completed from Database(client_list table at clientId = " + clientId);
        } catch (SQLException ex) {
            res = -1;
            System.err.println("failed to remove Database(client_list table at clientId = " + clientId);
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return res;
        }
    }

    @Override
    public int removeClientList(Integer[] clientId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insertClientMapping(Integer hostId, SortedSet<Integer> targetId) {
        if (!this.isConnectionEnable()) {
            return -1;
        }
        SortedMap<Integer, SortedSet<Integer>> map = new TreeMap<>();
        map.put(hostId, targetId);
        return insertClientMapping(map);
    }

    @Override
    public int insertClientMapping(SortedMap<Integer, SortedSet<Integer>> map) {
        if (!this.isConnectionEnable()) {
            return -1;
        }
        if (map == null || map.isEmpty()) {
            return 0;
        }
        System.out.println("Try to insert Database (client_mapping table) size : ");
        int res = 0;
        String sql = "INSERT IGNORE INTO client_mapping(host_id,target_id) VALUES(?,?)";
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (Integer hostId : map.keySet()) {
                Set<Integer> targetIds = map.get(hostId);
                if (targetIds == null || targetIds.isEmpty()) {
                    continue;
                }
                for (Integer targetId : targetIds) {
                    pstm.setInt(1, hostId);
                    pstm.setInt(2, targetId);
                    pstm.addBatch();
                }
            }
            int nos[] = pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            res = 0;
            for (int no : nos) {
                if (no == 1) {
                    res++;
                }
            }
            System.out.println("Insert comleted to Database (client_mapping table) :" + res);
        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            res = -1;
            System.err.println("Failed to insert to Database(client_mapping table )");
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return res;
        }
    }

    @Override
    public int removeClientMapping(Integer hostId, Integer targetId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int removeClientMapping(Integer[] hostId, Integer[] targetId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void disconnect() {
        if (!this.isConnectionEnable()) {
            return;
        }
        try {
            dataSource.getConnection().close();
        } catch (SQLException ex) {
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean isValid() {
        if (!this.isConnectionEnable()) {
            return false;
        }
        try {
            Connection conn = dataSource.getConnection();
            return conn.isValid(10) || !conn.isClosed();
        } catch (SQLException ex) {
            System.err.println("Connection is broken");
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public SortedMap<Integer, SortedSet<Integer>> getClientMapping() {
        if (!this.isConnectionEnable()) {
            return null;
        }
        System.out.println("Try to retrive datas from Database(client_mapping)");
        String sql = "Select host_id,target_id from client_mapping order by host_id asc";
        SortedMap<Integer, SortedSet<Integer>> clientMapping;
        clientMapping = new TreeMap<>();
        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(sql);
            Integer currentId = -1;
            SortedSet<Integer> list = null;
            while (set.next()) {
                int id1 = set.getInt("host_id");
                if (id1 != currentId) {
                    list = new TreeSet<>();
                    clientMapping.put(id1, list);
                    currentId = id1;
                }
                int id2 = set.getInt("target_id");
                list.add(id2);
            }
            System.out.println("Retreive success from Database(client_mapping) Datas : " + clientMapping);
        } catch (SQLException ex) {
            System.err.println("Failed to retreive datas from Database(client_mapping table)");
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            return clientMapping;
        }
    }

    @Override
    public SortedMap<Integer, Boolean> getClientList() {
        if (!this.isConnectionEnable()) {
            return null;
        }
        System.out.println("try to retreive datas From Database(client_list)");
        String sql = "Select id,aktif from client_list";
        SortedMap<Integer, Boolean> ids = new TreeMap<>();
        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(sql);
            while (set.next()) {
                ids.put(set.getInt("id"), set.getBoolean("aktif"));
            }
            System.out.println("retrevied succress(client_list) Data : " + ids);
        } catch (SQLException ex) {
            System.err.println("Failed to retreive data from Database(client_list)");
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
            ids = null;
        } finally {
            return ids;
        }
    }

    public Integer truncateClientMapping() {
        if (!this.isConnectionEnable()) {
            return -1;
        }
        System.out.println("Try to erase table from Database(client_mapping)");
        String sql = "TRUNCATE `sera_kontrol`.`client_mapping`";
        Integer index = -1;
        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            index = st.executeUpdate(sql);
            System.out.println("Erase table success at Database(client_mapping) res = " + index);
        } catch (SQLException ex) {
            index = -1;
            System.err.println("Failed to erase table from Database(client_mapping)");
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return index;
        }
    }

    @Override
    public int removeClientActionListByHostId(Integer hostId) {
        if (!this.isConnectionEnable()) {
            return -1;
        }
        String query = "DELETE FROM `sera_kontrol`.`client_action_list` WHERE host_id=" + hostId;
        System.out.println("Try to remove form databas(client_action_list) host_id=" + hostId);
        int rowCount = -1;
        try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement();) {
            rowCount = st.executeUpdate(query);
            System.out.println("Remove success from database(client_action_list)( hostid=" + hostId + ") " + rowCount + " miktarinda satir silindi");
        } catch (SQLException ex) {
            rowCount = -1;
            System.err.println("Failed to remove from Database(client_action_list)");
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return rowCount;
        }
    }

    @Override
    public int removeClientActionListByTargetId(Integer targetId) {
        if (!this.isConnectionEnable()) {
            return -1;
        }
        System.out.println("Try to remove data from Database(client_action_list) where target_id = " + targetId);
        String query = "DELETE FROM `sera_kontrol`.`client_action_list` WHERE target_id=" + targetId;
        int rowCount = -1;

        try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement();) {
            rowCount = st.executeUpdate(query);
            System.out.println("Remove success from Database(client_action_list) where (targetid = " + targetId + ") : " + rowCount + " miktarinda satir silindi");

        } catch (SQLException ex) {
            rowCount = -1;
            System.err.println("Remove failed from Database(client_action_list)");
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return rowCount;
        }
    }

    @Override
    public int removeClientMappingByHostId(Integer hostId) {
        if (!this.isConnectionEnable()) {
            return -1;
        }
        System.out.println("Try to remove data from Database(client_mapping) where host_id = " + hostId);
        String query = "DELETE FROM sera_kontrol.client_mapping WHERE host_id=" + hostId;
        int rowCount = -1;
        try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement();) {
            rowCount = st.executeUpdate(query);
            System.out.println("Remove success from Database(client_mapping) where (host_id = " + hostId + ") : " + rowCount + " miktarinda satir silindi");
        } catch (SQLException ex) {
            rowCount = -1;
            System.err.println("Remove failed from Database(client_mapping)");
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return rowCount;
        }
    }

    @Override
    public int removeClientMappingBytargetId(Integer targetId) {
        if (!this.isConnectionEnable()) {
            return -1;
        }
        String query = "DELETE FROM sera_kontrol.client_mapping WHERE target_id=" + targetId;
        System.out.println("Try to remove form databas(client_mapping) target_id=" + targetId);
        int rowCount = -1;
        try (Connection conn = dataSource.getConnection(); Statement st = conn.createStatement();) {
            rowCount = st.executeUpdate(query);
            System.out.println("Remove success from Database(client_mapping) where (target_id = " + targetId + ") : " + rowCount + " miktarinda satir silindi");
        } catch (SQLException ex) {
            rowCount = -1;
            System.err.println("Remove failed from Database(client_mapping)");
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return rowCount;
        }
    }
 @Override
    public int insertSulamaTable(SulamaBean bean) {
        List<SulamaBean> beans = new ArrayList<>();
        beans.add(bean);
        return insertSulamaTable(beans);
    }
    @Override
    public int insertSulamaTable(List<SulamaBean> beans) {
        if (!this.isConnectionEnable()) {
            return -1;
        }
//        boolean flag = (hostId.length == targetId.length) && (hostId.length == actionCode.length) && actionCode.length == value.length && value.length == dt.length;

        if (beans.isEmpty()) {
            return -1;
        }
        int size = beans.size();
        System.out.println("Try to insert Database(sulama table) size : " + size);
        int res = 0;
        String sql = "INSERT INTO sulama(baslama,bitis,none,low,normal) VALUES(?,?,?,?,?)";
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (SulamaBean bean : beans) {
                pstm.setLong(1, bean.getBaslama());
                pstm.setLong(2, bean.getBitis());
                pstm.setInt(3, ((Long) bean.getBasincsiz()).intValue());
                pstm.setInt(4, ((Long) bean.getAzBasincli()).intValue());
                pstm.setInt(5, ((Long) bean.getBasincli()).intValue());
                pstm.addBatch();
            }

            int nos[] = pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            res = 0;
            for (int no : nos) {
                if (no == 1) {
                    res++;
                }
            }
            System.out.println("Insert comleted to Database(client_action_list):" + res);

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            res = -1;
            System.err.println("Insert failed on Database(client_action_list");
            Logger.getLogger(MySqlServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return res;
        }
    }
    @Override
    public List<SulamaBean> getSulama(Date date1, Date date2,boolean ascendingOrder) {
        return getSulama(-1,-1,date1.getTime(), date2.getTime(),ascendingOrder);
    }

    private List<SulamaBean> getSulama(int bas,int miktar,long date1,long date2,boolean ascendingOrder){
         List<SulamaBean> sulamaBilgisi = new ArrayList<>();
        if (!this.isConnectionEnable()) {
            return sulamaBilgisi;
        }
        String limit=null,where=null;
        if(bas>=0 && miktar>0){
            limit = "limit "+bas+","+miktar;
        }
        if(date1>0 && date2>0){
            where ="where baslama between "+date1 + " and "+ date2;
        }else if( date1>0 && date2<0){
            where = "where baslama > "+ date1;
        }else if( date1<0 && date2>0){
            where= "where baslama <"+date2;
        }
        String order = ascendingOrder ? "ASC" : "DESC";
        System.out.println("try to retreive sulama bilgisi  From Database(sulama)");
        String sql = "Select id,baslama,bitis,none,low,normal from sulama ";
        if(where!=null)
            sql = sql+" "+where;
        sql = sql+ " order by baslama "+order;
        if(limit!=null)
            sql = sql+ " "+limit;
        System.out.println("Sql : "+sql);
        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(sql);
            while (set.next()) {
                SulamaBean bean = new SulamaBean(set.getInt("id"), set.getLong("baslama"), set.getLong("bitis"), set.getInt("none"), set.getInt("low"),set.getInt("normal"));
                sulamaBilgisi.add(bean);
            }
            System.out.println("retrevied succress(sulama bilgisi) Data size: " + sulamaBilgisi.size());
        } catch (SQLException ex) {
            System.err.println("Failed to retreive data from Database(client_list)");
            Logger.getLogger(MySqlServerConnection.class
                    .getName()).log(Level.SEVERE, null, ex);
            
        } finally {
            return sulamaBilgisi;
        }
    }
    @Override
    public List<SulamaBean> getSulama(int bas, int miktar,boolean ascendingOrder) {
        return getSulama(bas, miktar,0, 0,ascendingOrder);
   }

}
