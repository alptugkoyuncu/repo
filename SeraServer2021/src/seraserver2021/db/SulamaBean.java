/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.db;

import org.json.JSONObject;

/**
 *
 * @author ISIK
 */
public  class SulamaBean implements java.io.Serializable{

    private int id;
    private long baslama;
    private long bitis;
    private int none;
    private int low;
    private int normal;

    public SulamaBean() {
    }

    public SulamaBean(int id, long baslama, long bitis, int none, int low, int normal) {
        this.id = id;
        this.baslama = baslama;
        this.bitis = bitis;
        this.none = none;
        this.low = low;
        this.normal = normal;
    }

    public long getBasincsiz() {
        return none;
    }

    public long getBasincli() {
        return normal;
    }

    public long getAzBasincli() {
        return low;
    }

    public long getBitis() {
        return bitis;
    }

    public long getBaslama() {
        return baslama;
    }

    public int getId() {
        return id;
    }

//        @Override
//        public String toString() {
//            SimpleDateFormat f = new SimpleDateFormat("DD-MM-YY HH:mm:ss");
//            return "id : "+id+" ST:"+f.format(baslama)+" FT:"+f.format(bitis)+" Sure:"+sure+" Basincsiz:"+none+" AzBasincli:"+low+" Basincli:"+normal;
//        }
//        public JSONObject getJSONObject(){
//            JSONObject obj = new JSONObject();
//            obj.put("id", id);
//            obj.put("ST", baslama);
//            obj.put("FT", bitis);
//            obj.put("B",none);
//            obj.put("AB", low);
//            obj.put("TB", normal);
//            return obj;
//        }
}