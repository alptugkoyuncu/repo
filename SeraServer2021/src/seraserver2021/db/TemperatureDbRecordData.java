/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.db;

import seraserver2021.db.DbRecordData;
import data.ActionCode;
import data.NetworkPackage;
import java.util.HashMap;

/**
 *
 * @author ISIK
 */
public class TemperatureDbRecordData extends DbRecordData {
    private final static String errorHeader="TemperatureDbRecordData Dosyasi";
    public TemperatureDbRecordData(int clientId) {
        super(clientId);
    }

    @Override
    public boolean setValues(NetworkPackage pck) throws Exception {
        ActionCode actionCode = ActionCode.getByValue(pck.getActionCode());
        var datas = pck.getDataArray();
        if (datas == null || datas.isEmpty()) {
            throw new Exception(errorHeader+" : 1.Data yok veya bos:"+pck.getDataArray());
        }
        String data = datas.get(0);
        switch(actionCode){
            case AC_SEND_TEMP0:
            case AC_SEND_TEMP1:
            case AC_SEND_TEMP2:
            case AC_SEND_TEMP3:
                return this.setValues(actionCode.getValue(), DB_Values.getDBValueOf_TEMP_VALUE(data));
            default:
                return false;
        }
    }

}
