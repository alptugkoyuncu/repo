/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.events;

import seraserver2021.socket.ClientHandler;


/**
 *
 * @author ISIK
 */
public interface ServerListener {
    public void serverConnected(ServerEvent ev);
    public void serverConnectionFailed(ServerEvent ev);
    public void serverDisconnected(ServerEvent ev);
    public void clientIdReceived(ClientEvent ev);
    public void clientConnected(ClientEvent ev);
    public void clientConnectionFailed(ClientEvent ev);
    public void clientDisconnected(ClientEvent ev);
    public void dataReceived(DataEvent ev);
    public void dataSent(DataEvent ev);
}
