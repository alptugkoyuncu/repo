/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.images;

import java.awt.Image;
import java.net.URL;
import javax.swing.ImageIcon;
import seraserver2021.ui.main.MainFrame;

/**
 *
 * @author ISIK
 */
public class ResourceFactory {
    private static Image getImage(String fileName,String description){
        URL imageURL = ResourceFactory.class.getResource(fileName);

        if (imageURL == null) {
            System.err.println("Resource not found: " + fileName);
            return null;
        } else {
            return (new ImageIcon(imageURL,description).getImage());
        }
    }
    public static Image getBulbImage(String description){
        return getImage("bulb.gif", description);
    }
    public static Image getDbConnectedImage(){
       return getImage("dnconnected.jpg", "Database Connected").getScaledInstance(25, 25, 0);
    }
    public static Image getDbDisConnectedImage(){
        return getImage("dbdisconnect.jpg","No Database connection").getScaledInstance(25, 25, 0);
    }
}
