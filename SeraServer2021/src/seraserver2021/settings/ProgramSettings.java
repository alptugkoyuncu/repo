/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.settings;

import seraserver2021.ui.main.MainFrame;
import settings.AbstractSeraSettings;

/**
 *
 * @author ISIK
 */
public class ProgramSettings extends AbstractSeraSettings{
    private final static String PROP_SERVER_AUTO_CONNECT="server_auto_connect";
    private final static String PROP_DB_CONNECTION_ENABLE="db_connection_enable";
    
    public ProgramSettings() {
        super(MainFrame.SETTINGS_FOLDER_NAME, MainFrame.SETTINGS_FILE_NAME);
    }
    public void setDBConnectionEnable(boolean connEnable){
        this.values.put(PROP_DB_CONNECTION_ENABLE, connEnable?"1":"0");
    }
    public boolean isDbConnectionEnable(){
        try{
            return Integer.parseInt(this.values.getOrDefault(PROP_DB_CONNECTION_ENABLE,"0"))!=0;
        }catch(NumberFormatException ex){
            return false;
        }
    
    }
    public boolean isServerAutoConnectable(){
        try{
            return Integer.parseInt(this.values.getOrDefault(PROP_SERVER_AUTO_CONNECT,"0"))!=0;
        }catch(NumberFormatException ex){
            return false;
        }
    }
    public void setServerAutoConnectable(boolean autoConnectable){
        String val = autoConnectable  ? "1" : "0";
        this.values.put(PROP_SERVER_AUTO_CONNECT, val);
    }
    @Override
    public void setDefaultValues() {
        this.setServerAutoConnectable(false);
    }

    @Override
    protected boolean isEmpty() {
        return !(this.values.containsKey(PROP_SERVER_AUTO_CONNECT)&& this.values.containsKey(PROP_DB_CONNECTION_ENABLE));
    }
    
}
