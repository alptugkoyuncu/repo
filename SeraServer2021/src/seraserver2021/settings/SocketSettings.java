/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.settings;

import settings.AbstractSeraSettings;
import seraserver2021.ui.main.MainFrame;

/**
 *
 * @author ISIK
 */
public class SocketSettings extends AbstractSeraSettings{
    final static String PROP_PORT="socket.port";

    public SocketSettings() {
        super(MainFrame.SETTINGS_FOLDER_NAME, MainFrame.SETTINGS_FILE_NAME);
    }
    
    public Integer getPort() {
        String port = this.values.get(PROP_PORT);
        try{
            return Integer.parseInt(port);
        }catch(NumberFormatException ex){
            return 0;
        }
    }

    public void setPort(int port) {
        this.values.put(PROP_PORT, port+"");
    }


    @Override
    public void setDefaultValues() {
        this.setPort(5555); 
    }

    @Override
    protected boolean isEmpty() {
        return !this.values.containsKey(PROP_PORT);
    }
    
}
