/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.socket;

import seraserver2021.db.DbRecordData;
import data.ActionCode;
import data.GeneralSystemConstant;
import data.NetworkPackage;
import data.NetworkPackageFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import log.LogEvent;
import org.json.JSONObject;

/**
 *
 * @author ISIK
 */
public class ClientHandler {

    public final static long INCOME_MAX_WAIT_SECOND = 180000;//after maximum second 
    Socket socket;
    boolean connectionTerminatedByClient;
    String ip;
    private final Date connectionTime;
    DbRecordData dbRecord;
    //socket : dbRecord lar ile  iletisim kurulacak socket baglantisi
    //clientHandlerListIndex serverda List de tutalan clieentHandlerinin listedeki indexi
    Integer clientId;
    ReaderThread rThread;
    WriterThread wThread;
    private AtomicBoolean running = new AtomicBoolean(false);
    private AtomicBoolean stopped = new AtomicBoolean(true);

    public synchronized Date getLastReceivedPackageDate() {
        return lastReceivedPackageDate;
    }

    private synchronized void setLastReceivedPackageDate(Date lastReceivedPackageDate) {
        this.lastReceivedPackageDate = lastReceivedPackageDate;
    }

    public synchronized Date getLastSentPackageDate() {
        return lastSentPackageDate;
    }

    private synchronized void setLastSentPackageDate(Date lastSentPackageDate) {
        this.lastSentPackageDate = lastSentPackageDate;
    }

    private Date lastReceivedPackageDate, lastSentPackageDate;

    public ClientHandler(Socket socket) {
        this.connectionTime = new Date();
        this.ip = null;
        this.socket = socket;
        rThread = null;
        wThread = null;
        clientId = null;
        this.lastReceivedPackageDate = null;
        this.lastSentPackageDate = null;
    }

    public void start() {
        if (stopped.get()) {
            if (rThread != null || wThread != null) {
                terminate();
                return;
            }
            try {
                InputStream in = socket.getInputStream();
                OutputStream out = socket.getOutputStream();
                rThread = new ReaderThread(in);
                wThread = new WriterThread(out);
                stopped.set(false);
                running.set(true);
                rThread.start();
                wThread.start();
            } catch (IOException ex) {
                terminate();

//                Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    public Integer getClientId() {
        return this.clientId;
    }

    public Integer getServerClientHandlerListIndex() {
        return SeraServer.clientList.indexOf(this);
    }

    public Date getConnectionTime() {
        return connectionTime;
    }

    public String getIp() {
        return this.ip;
    }

    void sentNetworkClientPackage(NetworkPackage pck) {
        if(wThread!=null){
            wThread.sent(pck);
        }
        SeraServer.fireDataSentEvent(pck, this);
//            server.fireLogEvent(LogEvent.INFO, "Sent package : " + pck.getMessage());
    }

    private void fireLogEvent(int type, String message) {
        String msg = this.getServerClientHandlerListIndex() + " . client : " + message;
        SeraServer.fireLogEvent(type, msg);
    }

  
    public void terminatedByClient() {
        this.connectionTerminatedByClient = true;
        terminate();
    }

    public boolean terminate() {
        String msg = ("Terminaating ch index : " + SeraServer.clientList.indexOf(this) + " id : " + this.getClientId());
        //database kayit yapmasi icin eklenen listener silinir

        if (this.getClientId() < 0) {
            System.err.println(msg);
        } else {
            System.out.println(msg);
        }
        running.set(false);
        boolean flag = true;
        if (rThread != null) {
            rThread.interrupt();
            try {
                rThread.join(3000);
                rThread = null;
            } catch (InterruptedException ex) {
                flag = false;
                Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        if (wThread != null) {
            wThread.interrupt();
            wThread = null;
            try {
                wThread.join(3000);
                wThread = null;
            } catch (InterruptedException ex) {
                flag = false;
                Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return flag;
    }

    private class ReaderThread extends Thread {

        BufferedReader reader;

        private ReaderThread(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        @Override
        public void run() {
            ClientHandler.this.clientId = readClientId();
            if (clientId == null) {
                terminate();
            } else {
                SeraServer.fireClientIdReceived(ClientHandler.this);
            }
            while (running.get()) {
                if (socket.isInputShutdown()
                        || socket.isClosed()) {
                    terminate();
                    break;
                }

                if (new Date().getTime() - getLastReceivedPackageDate().getTime() > INCOME_MAX_WAIT_SECOND) {
                    terminate();
                    break;
                }
                String line = "";
                NetworkPackage pck;
                try {
                    line = reader.readLine();
                    JSONObject jsonObj = new JSONObject(line);
                    pck = NetworkPackageFactory.create(jsonObj);
                    pck.setHostId(getClientId());
                    setLastReceivedPackageDate(new Date());
                    SeraServer.fireDataReceivedEvent(pck, ClientHandler.this);
                    SeraServer.broadcastPackage(pck, ClientHandler.this);
                    if (dbRecord.setValues(pck)) {
                        Map<Integer, String> val = dbRecord.getRecordMap();
                        for (Integer ac : val.keySet()) {
                            SeraServer.saveDbValue(dbRecord.getClientId(), pck.getTargetId(), ac, val.get(ac), pck.getPackageDate());
                        }
                        System.err.println(dbRecord);
                        dbRecord.clearRecordMap();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    fireLogEvent(LogEvent.ERROR, "Network package olusturulamadi:" + line);
                    Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            // Butun alakali clientlara baglanti kapatma bilgisi gonderir
            List<String> dataArray = new ArrayList<>();
            dataArray.add(getClientId() + "");
            
            
            try {
                NetworkPackage pck = NetworkPackageFactory.create(GeneralSystemConstant.SERVER_ID, 0, ActionCode.AC_FRIEND_DISCONNECTED, dataArray);
                SeraServer.broadcastPackage(pck, ClientHandler.this);
            } catch (Exception ex) {
                Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
            
//            ch.sentNetworkClientPackage(clientPackage);
            
            //kendisine dbRecord kapatma bilgisi gonderiliyor eger kendisi kapatmadiysa
            if (!connectionTerminatedByClient) {
                NetworkPackage np;
                try {
                    np = NetworkPackageFactory.create(GeneralSystemConstant.SERVER_ID, getClientId(), ActionCode.AC_CLIENT_DISCONNECTED, new ArrayList<>());
                    sentNetworkClientPackage(np);
                } catch (Exception ex) {
                    Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ex) {
                    Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }

        private Integer readClientId() {
            int clientIdDenemeIndex = 0;
            Integer id = null;
            Date lastIDReq = new Date();
            while (id == null && running.get()) {
                if (socket.isInputShutdown()
                        || socket.isClosed()) {
                    fireLogEvent(LogEvent.ERROR, "Client id talep fonksiyonunda soket hatasi ");
                    terminate();
                    break;
                }
                if (clientIdDenemeIndex > GeneralSystemConstant.MAX_NUMBER_OF_SERVER_ID_REQUEST) {
                    fireLogEvent(LogEvent.ERROR, "Client id talep suresi zaman asima ugradi");
                    terminate();
                    break;
                }
                Date now = new Date();
                if (now.getTime() - lastIDReq.getTime() > GeneralSystemConstant.MAX_TIME_SERVER_ID_REQUEST_INTERVAL) {
                    try {
//                        clientPackage.reset();
                        NetworkPackage pck = NetworkPackageFactory.create(GeneralSystemConstant.SERVER_ID, null, ActionCode.AC_ID_REQUESTED, new ArrayList<String>());

                        SeraServer.broadcastPackage(pck, ClientHandler.this);
//                    sentNetworkClientPackage(clientPackage);
                    } catch (Exception ex) {
                        fireLogEvent(LogEvent.ERROR, "Client id istemede package olusturulamadi ");
                        Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    clientIdDenemeIndex++;
                }
                try {
                    String line = reader.readLine();
                    JSONObject jsonObject = new JSONObject(line);
                    NetworkPackage pck = NetworkPackageFactory.create(jsonObject);
                    ClientHandler.this.setLastReceivedPackageDate(new Date());
                    pck.setHostId(null);
                    SeraServer.fireDataReceivedEvent(pck, ClientHandler.this);
                    List<String> pckData = pck.getDataArray();
                    if (pckData.isEmpty()) {
                        fireLogEvent(LogEvent.ERROR, "Client id datasi yok Message:" + pck.toString());
                    } else {
                        try {
                            id = Integer.parseInt(pckData.get(0));
                            if (!SeraServer.checkId(id)) {
                                id = null;
                                fireLogEvent(LogEvent.INFO, "Client id kayitlli degil  Message : " + pck.toString());
                                terminate();
                                break;
                            }
                        } catch (NumberFormatException ex) {
                            id = null;
                            fireLogEvent(LogEvent.ERROR, "Client id integere pars edilemiyor Messaege : " + pck.toString());
                        }
                    }

                } catch (IOException ex) {
                    Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
                    fireLogEvent(LogEvent.ERROR, "Thread.sleep hatasi : Message:" + ex.getMessage());
                }
            }
            NetworkPackage pck;
            try {
                pck = NetworkPackageFactory.create(GeneralSystemConstant.SERVER_ID, id, id == null ? ActionCode.AC_ID_REFUSED : ActionCode.AC_ID_ACCEPTED, null);
                SeraServer.broadcastPackage(pck, ClientHandler.this);
            } catch (Exception ex) {
                Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
            }

            return id;
        }

    }

    private class WriterThread extends Thread {

        private PrintWriter writer;
        private AtomicBoolean lock = new AtomicBoolean();
        private NetworkPackage pck;

        private WriterThread(OutputStream out) {
            writer = new PrintWriter(out);
            lock.set(false);

        }

        @Override
        public void run() {
            while (running.get()) {
                if (pck != null) {
                    writer.println(new JSONObject(pck));
                    writer.flush();
                    SeraServer.fireDataSentEvent(pck, ClientHandler.this);
//            server.fireLogEvent(LogEvent.INFO, "Sent package : " + pck.getMessage());
                    pck = null;
                }
                lock.set(false);
                try {
                    Thread.sleep(0);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (writer != null) {
                writer.close();
            }
        }

        public void sent(NetworkPackage pck) {
            while (lock.get()) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            lock.set(true);
            this.pck = pck;
            this.interrupt();
        }

    }
}
