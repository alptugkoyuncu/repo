/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.socket;

/**
 *
 * @author ISIK
 */
public enum ClientType {
    SERVER,
    TEMP_CONTROL,
    HAVUZ_KONTROL,
    WATCHER;
    public static ClientType getClientType(int clientId){
        switch(clientId){
            case 1000:return SERVER;
            case 1001: return TEMP_CONTROL;
            case 1002: return TEMP_CONTROL;
            case 1003: return HAVUZ_KONTROL;
            case 1101: return HAVUZ_KONTROL;
            default:{
                    if(clientId<3000 && clientId>=2000) return WATCHER;
            }
        }
        return null;
    }
}
