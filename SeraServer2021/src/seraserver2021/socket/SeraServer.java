/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.socket;

import data.ActionCode;
import data.GeneralSystemConstant;
import data.NetworkPackage;
import data.NetworkPackageFactory;
import seraserver2021.events.ClientEvent;
import seraserver2021.events.DataEvent;
import seraserver2021.events.ServerEvent;
import seraserver2021.events.ServerListener;
import seraserver2021.util.HelperFunctions;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.Objects;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import log.LogEvent;
import log.LogListener;
import org.json.JSONArray;
import seraserver2021.db.DBConnection;
import seraserver2021.db.MySqlServerConnection;
import seraserver2021.db.SulamaBean;
import seraserver2021.settings.ProgramSettings;
import seraserver2021.settings.SeraServerDBSettings;
import seraserver2021.settings.SocketSettings;
import seraserver2021.ui.main.MainFrame;

/**
 *
 * @author ISIK
 */
public final class SeraServer implements Runnable {

    public final static int MAX_CLIENT_NUMBER = 20;

//    public final static long PING_WAIT_SECOND = 60000;
    public static List<ClientHandler> clientList = new ArrayList<>();
    private static final Set<ServerListener> serverListeners;
    private static final Set<LogListener> serverLogListeners;
    private static int port;
    private static String externalIp;
    private static String localIp;
    private static ServerSocket serverSocket;
    private static SortedMap<Integer, SortedSet<Integer>> clientIdMapping;
    private static SortedMap<Integer, Boolean> clientIds; //clientId aktif veya degil
    
    public final static ProgramSettings PROGRAM_SETTINGS;
    public final static SeraServerDBSettings DATABASE_SETTINGS;
    public final static SocketSettings SOCKET_SETTINGS;
//    private final static List<Integer> clientIdList;
    public static DBConnection DATABASE_CONNECTION;

    private static Thread runningThread;
    private static AtomicBoolean running = new AtomicBoolean();
    private static AtomicBoolean stopped = new AtomicBoolean();
    static {

        runningThread = null;
        serverListeners = new HashSet<>();
        serverLogListeners = new HashSet<>();
        clientIds = new TreeMap<>();
        clientIdMapping = new TreeMap<>();
        SOCKET_SETTINGS = new SocketSettings();
        PROGRAM_SETTINGS = new ProgramSettings();
        DATABASE_SETTINGS = new SeraServerDBSettings();
        DATABASE_CONNECTION = new MySqlServerConnection();
        MySqlServerConnection.setConnectionEnabled(PROGRAM_SETTINGS.isDbConnectionEnable());
//        System.err.println(clientIdMapping);
    }

    static synchronized void saveDbValue(int hostId, int targetId, int actionCode, String data, java.util.Date dt) {
        DATABASE_CONNECTION.insertClientActionList(hostId, targetId, actionCode, data, dt);
    }

    static synchronized List<SulamaBean> getSulamaBilgisi(int baslama, int miktar, long bast, long bitist, boolean ascendingOrder) {
        if (baslama >= 0 && miktar > 0) {
            return DATABASE_CONNECTION.getSulama(baslama, miktar, ascendingOrder);
        }
        if (bast > 0 && bitist > bast) {
            return DATABASE_CONNECTION.getSulama(new Date(bast), new Date(bitist), ascendingOrder);
        }
        return new ArrayList<SulamaBean>();
    }

    private SeraServer() {
    }

    public static SortedMap<Integer, SortedSet<Integer>> getClientIdMapping() {
        return clientIdMapping;
    }

    private static SortedMap<Integer, SortedSet<Integer>> loadClientMappingFromDb() {
        MySqlServerConnection conn = new MySqlServerConnection();
        return conn.getClientMapping();
//        return !clientIdMapping.isEmpty();
    }

    private static SortedMap<Integer, SortedSet<Integer>> loadClientMappingFromFile() {
        SortedMap<Integer, SortedSet<Integer>> map = new TreeMap<>();
        String folderName = System.getProperty("user.home") + File.separator + MainFrame.SETTINGS_FOLDER_NAME;
        File file;
        file = new File(folderName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String fileName = folderName + File.separator + "clientIdMapping.txt";
        file = new File(fileName);
        if (!file.exists()) {
            try {
                file.createNewFile();

                return null;
            } catch (IOException ex) {
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ex) {
            fireLogEvent(LogEvent.ERROR, "File Not found : " + fileName + "\n Message:" + ex.getMessage());
            return null;
        }
        String str;
        int index = 0;
        try {
            while ((str = br.readLine()) != null) {
                index++;
                String data[] = str.split(" ");
                if (data.length != 2) {
                    fireLogEvent(LogEvent.ERROR, "File client Mapping hatasi data uzunlugu eksik index : " + index + "zn File :" + file.getPath());

                    continue;
                }
                try {
                    Integer id = Integer.parseInt(data[0]);
                    Integer id2 = Integer.parseInt(data[1]);
                    SortedSet<Integer> set;
                    if (map.containsKey(id)) {
                        set = map.get(id);
                        set.add(id2);
                    } else {
                        set = new TreeSet<>();
                        set.add(id2);
                        map.put(id, set);
                    }

                } catch (NumberFormatException ex) {
                    fireLogEvent(LogEvent.ERROR, "File client Mapping hatasi data Integere cevrilemiyor: " + index + "zn File :" + file.getPath());
                }
            }
        } catch (IOException ex) {
            fireLogEvent(LogEvent.ERROR, fileName + " File readLine hatasi\n Error Message" + ex.getMessage());
            return null;
        }

        fireLogEvent(LogEvent.DEBUG, "File ClientMapping tamalandi");
        return map;
    }

    private static void saveClientIdMappingToDb() {
        MySqlServerConnection conn = new MySqlServerConnection();
        conn.truncateClientMapping();
        conn.insertClientMapping(clientIdMapping);
    }

    private static void saveClientIdMappingToFile() {
        String folderName = System.getProperty("user.home") + File.separator + MainFrame.SETTINGS_FOLDER_NAME;
        File file;
        file = new File(folderName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String fileName = folderName + File.separator + "clientIdMapping.txt";
        file = new File(fileName);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
        }
        try (FileWriter fw = new FileWriter(file, false)) {

            StringBuilder sb = new StringBuilder();
            for (Integer key : clientIdMapping.keySet()) {
                SortedSet<Integer> values = clientIdMapping.get(key);
                for (Integer value : values) {
                    sb.append(key);
                    sb.append(" ");
                    sb.append(value);
                    sb.append(System.getProperty("line.separator"));
                }
            }
            fw.write(sb.toString().trim());
            fw.flush();
        } catch (FileNotFoundException ex) {
            fireLogEvent(LogEvent.ERROR, "File Not found : " + fileName + "\n Message:" + ex.getMessage());
        } catch (IOException ex) {
            fireLogEvent(LogEvent.ERROR, "File error : " + fileName + "\n Message:" + ex.getMessage());

            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void saveClientMapping() {
        saveClientIdMappingToDb();
        saveClientIdMappingToFile();
    }

    public static void loadClientMapping() {
        SortedMap<Integer, SortedSet<Integer>> map1, map;
        map = loadClientMappingFromFile();
        if (map == null || map.isEmpty()) {
            map1 = loadClientMappingFromDb();
            if (map1 != null && !map1.isEmpty()) {
                clientIdMapping = map1;
                saveClientIdMappingToFile();
            }
        } else {
            clientIdMapping = map;
            saveClientIdMappingToDb();
        }
    }

    private static SortedMap<Integer, Boolean> loadClientIdFromFile() {
        SortedMap<Integer, Boolean> map = new TreeMap<>();
        String folderName = System.getProperty("user.home") + File.separator + MainFrame.SETTINGS_FOLDER_NAME;
        File file;
        file = new File(folderName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String fileName = folderName + File.separator + "clientIdList.txt";
        file = new File(fileName);
        if (!file.exists()) {
            try {
                file.createNewFile();

                return null;
            } catch (IOException ex) {
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ex) {
            fireLogEvent(LogEvent.ERROR, "File Not found : " + fileName + "\n Message:" + ex.getMessage());
            return null;
        }
        String str;
        try {
            while ((str = br.readLine()) != null) {
                String[] datas = str.split(" ");
                if (datas.length != 2) {
                    fireLogEvent(LogEvent.ERROR, "File client list hatasi satir index hatasi" + datas.length + " : \n File :" + file.getPath());
                    continue;
                }
                try {
                    Integer id = Integer.parseInt(datas[0]);
                    Boolean aktif = Integer.parseInt(datas[1]) != 0;
                    map.put(id, aktif);
                } catch (NumberFormatException ex) {
                    fireLogEvent(LogEvent.ERROR, "File client list hatasi data Integere cevrilemiyor:  \n File :" + file.getPath());
                }
            }
        } catch (IOException ex) {
            fireLogEvent(LogEvent.ERROR, fileName + " File readLine hatasi\n Error Message" + ex.getMessage());
            return null;
        }

        fireLogEvent(LogEvent.DEBUG, "File clientList tamalandi");
        return map;
    }

    private static void saveClientIdToFile() {
        String folderName = System.getProperty("user.home") + File.separator + MainFrame.SETTINGS_FOLDER_NAME;
        File file;
        file = new File(folderName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String fileName = folderName + File.separator + "clientIdList.txt";
        file = new File(fileName);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
        }

        try (FileWriter fw = new FileWriter(file, false)) {
            StringBuilder sb = new StringBuilder();
            Iterator<Integer> it = clientIds.keySet().iterator();
            while (it.hasNext()) {
                Integer key = it.next();
                Boolean value = clientIds.get(key);
                sb.append(key);
                sb.append(" ");
                sb.append(value ? "1" : "0");
                sb.append(System.getProperty("line.separator"));
            }
//            int len = sb.length();
//            String str = sb.toString().trim();
//            System.err.println(str);
            fw.write(sb.toString().trim());

        } catch (FileNotFoundException ex) {
            fireLogEvent(LogEvent.ERROR, "File Not found : " + fileName + "\n Message:" + ex.getMessage());
        } catch (IOException ex) {
            fireLogEvent(LogEvent.ERROR, "File error : " + fileName + "\n Message:" + ex.getMessage());
            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

    private static SortedMap<Integer, Boolean> loadClientIdFromDb() {
        MySqlServerConnection conn = new MySqlServerConnection();
        return conn.getClientList();
    }

    private static void saveClientIdToDb() {
        MySqlServerConnection conn = new MySqlServerConnection();
        conn.updateClientList(clientIds, true);
    }

    public static SortedMap<Integer, Boolean> getClientId() {
        return clientIds;
    }

    public static void saveClientId() {
        saveClientIdToDb();
        saveClientIdToFile();
    }

    private static boolean comparesIdMap(SortedMap<Integer, Boolean> map1, SortedMap<Integer, Boolean> map2) {
        if (map1 == null || map2 == null) {
            return false;
        }
        Set<Integer> s1 = map1.keySet();
        Set<Integer> s2 = map2.keySet();
        boolean flag = s1.size() == s2.size() && s1.containsAll(s2);
        if (!flag) {
            return false;
        }
        for (Integer id : s1) {
            if (!Objects.equals(map1.get(id), map2.get(id))) {
                return false;
            }
        }
        return true;
    }

    public static void loadClientId() {
        SortedMap<Integer, Boolean> s1, s2;
        s1 = loadClientIdFromFile();
        s2 = loadClientIdFromDb();
        if (s2 == null || s2.isEmpty()) {
            if (s1 != null && !s1.isEmpty()) {
                clientIds = s1;
                saveClientIdToDb();
            }
        } else {
            clientIds = s2;
            if (!comparesIdMap(s1, s2)) {
                saveClientIdToFile();
            }
        }
    }

    public static void setPort(int port) {
        if (port <= 0) {
            SeraServer.fireLogEvent(LogEvent.ERROR, "port 0 dan küçük olamaz");
        } else {
            SeraServer.port = port;
        }
    }

    public static boolean isTerminated() {
        return stopped.get();
    }

    public static int getPort() {
        return SeraServer.port;
    }

    private static void updateExternalIp() {
        SeraServer.externalIp = HelperFunctions.getIP();
    }

    public static String getExternalIp() {
        return externalIp;
    }

    public static synchronized void start(int port) {
        SeraServer.setPort(port);
        SeraServer.start();
    }

    public static synchronized void start() {
//        SeraServer.addServerListener(new DbServerListener());
        if (port <= 0) {
            port = SeraServer.SOCKET_SETTINGS.getPort();
        }
        if (runningThread != null) {
            if (runningThread.getState() == Thread.State.TERMINATED) {
                runningThread = null;
            } else {
                SeraServer.terminate();
//                return;
            }
        }

        updateExternalIp();
        loadClientId();
        loadClientMapping();
        try {
            if (port <= 0) {
                running.set(false);
                stopped.set(true);
                throw new Exception("Port 0 veya 0 dan küçük olamaz");
            }
            serverSocket = new ServerSocket(port);
            runningThread = new Thread(new SeraServer());
            runningThread.start();
            fireServerConnectedEvent();
        } catch (IOException ex) {
            fireLogEvent(LogEvent.ERROR, "Server baglanamadi(socket baglanti hatasi)");
            fireServerConnectionFailedEvent();
            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            terminate();

        } catch (Exception ex) {
            fireLogEvent(LogEvent.ERROR, ex.getMessage());
            fireServerConnectionFailedEvent();
            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            terminate();
        }
    }

  

    public static void terminate() {
        if (SeraServer.runningThread != null) {
            try {
                SeraServer.running.set(false);
                SeraServer.runningThread.interrupt();
                SeraServer.runningThread.join(5000);
            } catch (InterruptedException ex) {
//                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                fireLogEvent(LogEvent.ERROR, "Server thread.join hatasi \nMessage:" + ex.getMessage());
            } finally {
                SeraServer.runningThread = null;
                stopped.set(true);
            }
        }
    }

    public synchronized static void broadcastPackage(NetworkPackage pck, ClientHandler sender) {
        //targetid==serverId ise serverdan istekte bulunulmustur
        //targetId==0 ise server kimlere gonderilecegini sececektir
        // targetId== spesifik bir id ise gidecegi yer bellidir
        // 1. kime gonderilecegini bul
        if (pck.getTargetId() == GeneralSystemConstant.SERVER_ID) {
            sendRespondMessage(pck, sender);
        } else if (pck.getTargetId() == 0) {
            int ac = pck.getActionCode();
            for (ClientHandler ch : SeraServer.getClientListByMapping(sender)) {
                pck.setTargetId(ch.getClientId());
                ch.sentNetworkClientPackage(pck);
            }
        } else {
            // ozel bir sender belirtilmisse
            Iterator<ClientHandler> it = SeraServer.clientList.iterator();
            while (it.hasNext()) {
                ClientHandler ch = it.next();
                if (Objects.equals(ch.getClientId(), pck.getTargetId())) {
                    //                                clientPackage.reset();
                    ch.sentNetworkClientPackage(pck);
                }
            }
        }
    }

    private static void sendRespondMessage(NetworkPackage pck, ClientHandler source) {
        // cevap gerektirmeyen islemler icin thread olusturmadan fonsiyonda halledecegiz
        switch (ActionCode.getByValue(pck.getActionCode())) {
            case AC_CLIENT_DISCONNECTED: {
                source.terminatedByClient();
                return;
            }
            case AC_SEND_PING: {
                return;
            }
            default:
                break;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<String> dataArray = new ArrayList<>();
                ActionCode ac = ActionCode.getByValue(pck.getActionCode());
                switch (ac) {
                    case AC_GET_ALL_CONNECTED_CLIENTS_ID: {
                        for (ClientHandler ch : SeraServer.clientList) {
                            if (ch != null && ch != source) {
                                Integer id = ch.getClientId();
                                if (id != null) {
                                    dataArray.add(id + "");
                                }
                            }
                        }
                        break;
                    }
                    case AC_SERVER_IS_ALIVE: {
                        ac = ActionCode.AC_SEND_PING;
                        break;
                    }

                    case AC_REQUEST_FOCUED_PROVIDERS: {
                        for (ClientHandler ch : SeraServer.getClientListByMapping(source)) {
                            Integer id = ch.getClientId();
                            if (id != null) {
                                dataArray.add(id + "");
                            }
                        }
                        break;
                    }
                    case AC_DB_GET_HAVUZ_DATA: {

                        var l = pck.getDataArray();
                        if (l.size() <= 4) {
                            return;
                        }
                        int baslama = -1, miktar = -1;
                        try {
                            baslama = Integer.parseInt(pck.getDataArray().get(0));
                            miktar = Integer.parseInt(pck.getDataArray().get(1));
                        } catch (NumberFormatException ex) {

                        }
                        long baslamaTarihi = -1, bitisTarihi = -1;
                        try {
                            baslamaTarihi = Long.parseLong(pck.getDataArray().get(2));
                            bitisTarihi = Long.parseLong(pck.getDataArray().get(2));
                        } catch (NumberFormatException ex) {

                        }
                        boolean ascendingOrder = "asc".equalsIgnoreCase(pck.getDataArray().get(4));
                        List<SulamaBean> result = SeraServer.getSulamaBilgisi(baslama, miktar, baslamaTarihi, bitisTarihi, ascendingOrder);
                        JSONArray arr = new JSONArray(result);
                        dataArray.add(arr.toString());
                    }
                    default:
                        return;
                }
                try {
                    NetworkPackage respond = NetworkPackageFactory.create(GeneralSystemConstant.SERVER_ID, source.getClientId(), ac, dataArray);
                    source.sentNetworkClientPackage(respond);
                } catch (Exception ex) {
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }).start();

    }

    @Override
    public void run() {
        running.set(true);
        stopped.set(false);
        while (running.get()) {
            while (clientList.size() >= MAX_CLIENT_NUMBER) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
                    fireLogEvent(LogEvent.ERROR, "Client list maximum sayiya ulasan thread.sleep icinde hata olustu");
                }
            }
            try {
                Socket clientSocket = serverSocket.accept();
                clientSocket.setKeepAlive(true);;
                ClientHandler t = new ClientHandler(clientSocket);
                clientList.add(t);
                fireClientConnectedEvent(t);
                t.start();
//                is.addClientToList(t);
                if (clientList.size() == MAX_CLIENT_NUMBER) {
                    fireLogEvent(LogEvent.INFO, "Server maksimum kapasiteye ulasti.");
                }

            } catch (SocketTimeoutException ex) {
            } catch (IOException e) {
                fireLogEvent(LogEvent.INFO, "Socket.accept hatasi\n" + e.getMessage());
                break;
            }
        }
        running.set(false);
        Iterator<ClientHandler> it = clientList.iterator();

        while (it.hasNext()) {
            ClientHandler ch = it.next();
            it.remove();
            if (ch != null) {
                ch.terminate();
            }
        }
//        clientList.clear();

        fireServerDisconnectedEvent();
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch (IOException ex) {
//            Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            fireLogEvent(LogEvent.ERROR, "Server Socket kapatma hatasi \nMessage : " + ex.getMessage());
        }

        //server butun listenerlari kaldiriyor
        serverListeners.clear();
        serverLogListeners.clear();
        stopped.set(true);
    }

    public static boolean checkId(Integer id) {
        return clientIds.getOrDefault(id, false);
    }

    static Set<ClientHandler> getClientListByMapping(ClientHandler ch) {
//        int id = senderId == 1001 ? 2001 : 1001;
        Set<ClientHandler> s = new HashSet<>();
        if (ch == null || ch.getClientId() <= 0) {
            return s;
        }
        Set<Integer> targetIds = clientIdMapping.get(ch.getClientId());

        if (targetIds == null) {
            return s;
        }
        for (ClientHandler c : clientList) {
            if (c == null || c == ch) {
                continue;
            }
            if (targetIds.contains(c.getClientId())) {
                s.add(c);
            }
        }
        return s;
    }

    /**
     * provider id ler uniqu id dir ve 1000 ile 2000 arasindadir.
     *
     * @param clientId
     * @return deger 1000 ile 2000 arasindaysa(1000 ve 2000 haric) true yoksa
     * false dondurur
     */
    static boolean isUniqueCientId(int clientId) {
        return clientId < 2000 && clientId > 1000;
    }

// mapping den receiver id ler alinacak bunun icin sender id kullanilarak mapping de karsilastirilma yapilarak client list te indexler alinir
    public static void addServerListener(ServerListener l) {
        serverListeners.add(l);
    }

    public static void removeServerListener(ServerListener l) {
        serverListeners.remove(l);
    }

    public static void addServerLogListener(LogListener l) {
        serverLogListeners.add(l);
    }

    public static void removeServerLogListener(LogListener l) {
        serverLogListeners.remove(l);
    }

    private static void fireServerConnectedEvent() {
//        System.out.println("Server connected event");
        ServerEvent evt = new ServerEvent(SeraServer.class, externalIp, port);
        for (ServerListener l : serverListeners) {
            l.serverConnected(evt);
        }
    }

    private static void fireServerConnectionFailedEvent() {
        ServerEvent evt = new ServerEvent(SeraServer.class, externalIp, port);
        for (ServerListener l : serverListeners) {
            l.serverConnectionFailed(evt);
        }
    }

    private static void fireServerDisconnectedEvent() {
        ServerEvent evt = new ServerEvent(SeraServer.class, externalIp, port);
        for (ServerListener l : serverListeners) {
            l.serverDisconnected(evt);
        }
    }

    static synchronized void fireDataSentEvent(NetworkPackage pck, ClientHandler ch) {
        
//        System.out.println("Data sent : " + pck.getMessage());
        for (ServerListener l : serverListeners) {
            try {
                NetworkPackage np = NetworkPackageFactory.clone(pck);
                 l.dataSent(new DataEvent(pck, ch));
            } catch (Exception ex) {
                Logger.getLogger(SeraServer.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        }
    }

    static synchronized void fireDataReceivedEvent(NetworkPackage pck, ClientHandler ch) {
//        System.out.println("Data received : " + pck.getMessage());
        for (ServerListener l : serverListeners) {
            l.dataReceived(new DataEvent(pck, ch));
        }
    }

    static synchronized void fireLogEvent(int eventType, String message) {
        for (LogListener l : serverLogListeners) {
            l.logOccured(new LogEvent(SeraServer.class, eventType, message));
        }
    }

    synchronized static void fireClientConnectedEvent(ClientHandler ch) {
        ClientEvent evt = new ClientEvent(ch);
        for (ServerListener l : serverListeners) {
            l.clientConnected(evt);
        }
    }

    synchronized static void fireClientConnectionFailedEvent(ClientHandler ch) {
        ClientEvent evt = new ClientEvent(ch);
        for (ServerListener l : serverListeners) {
            l.clientConnectionFailed(evt);
        }
    }

    synchronized static void fireClientDisConnectedEvent(ClientHandler ch) {
        ClientEvent evt = new ClientEvent(ch);
        for (ServerListener l : serverListeners) {
            l.clientDisconnected(evt);
        }
    }

    synchronized static void fireClientIdReceived(ClientHandler ch) {
        ClientEvent evt = new ClientEvent(ch);
        for (ServerListener l : serverListeners) {
            l.clientIdReceived(evt);
        }

    }
}
