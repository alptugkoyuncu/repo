/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.ui.main;

import data.NetworkPackage;
import java.awt.AWTException;
import seraserver2021.socket.SeraServer;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import seraserver2021.db.MySqlServerConnection;
import seraserver2021.events.ClientEvent;
import seraserver2021.events.DataEvent;
import seraserver2021.events.ServerEvent;
import seraserver2021.events.ServerListener;
import seraserver2021.images.ResourceFactory;
import seraserver2021.settings.SocketSettings;
import seraserver2021.ui.settings.SettingsPanel;

/**
 *
 * @author ISIK
 */
public class MainFrame extends javax.swing.JFrame implements ServerListener {

    public final static String SETTINGS_FOLDER_NAME = "Sera Server";
    public final static String SETTINGS_FILE_NAME = "settings.properties";
    /**
     * Creates new form MainFrame
     */

    LogPanel logPanel;
    KullaniciListesiPanel kullaniciListesiPaneli;
//    MySqlServerConnection dbConnection;
    TrayIcon trayIcon;
    final SystemTray tray = SystemTray.getSystemTray();
    final PopupMenu popup = new PopupMenu();
    MenuItem frameItem, connectionItem, disconnectItem, cikisItem;
//    SocketSettings socketSettings;
    boolean autoConnectServer;

    public MainFrame() {
//        dbConnection = new MySqlServerConnection();
//        socketSettings = new SocketSettings();
        initComponents();
        this.jlConnected.setVisible(false);

        this.jlDisconnected.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                SeraServer.DATABASE_CONNECTION = new MySqlServerConnection();
                checkDbConnection();
            }

        });
        logPanel = new LogPanel();
        kullaniciListesiPaneli = new KullaniciListesiPanel();
        this.jTabbedPane1.add("Log ", logPanel);
        this.jTabbedPane1.add("Kullanici Listesi", kullaniciListesiPaneli);
        createTrayIcon();
        this.pack();
//        this.setSize(new Dimension(400, 300));
        //5 saniye de bir database connection kontrol eder
        new Thread(new Runnable() {
            @Override
            public void run() {
//                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                while (true) {

                    checkDbConnection();

                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        ).start();
        startAutoServerConnection();
    }
    private void startAutoServerConnection() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        if (SeraServer.PROGRAM_SETTINGS.isServerAutoConnectable() && SeraServer.isTerminated()) {
                            startServer();
                        }
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
             
                    }
                }
            }
        }).start();
}
private void checkDbConnection() {
        boolean flag;
        flag = SeraServer.DATABASE_CONNECTION.isValid();

        jlConnected.setVisible(flag);
        jlDisconnected.setVisible(!flag);
    }

    public void startServer() {
        this.jMenuItemConnect.setVisible(false);
        this.connectionItem.setEnabled(false);
        int port = SeraServer.SOCKET_SETTINGS.getPort();
       
        if (SeraServer.isTerminated()) {
            SeraServer.addServerListener(logPanel);
            SeraServer.addServerLogListener(logPanel);
            SeraServer.addServerListener(kullaniciListesiPaneli);
            SeraServer.addServerListener(this);
            SeraServer.start(port);
        }
    }

    public void stopServer() {
        this.jMenuItemDisconnect.setVisible(false);
        this.disconnectItem.setEnabled(false);
        SeraServer.terminate();
        this.jMenuItemConnect.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jlConnected = new javax.swing.JLabel();
        jlDisconnected = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItemConnect = new javax.swing.JMenuItem();
        jMenuItemDisconnect = new javax.swing.JMenuItem();
        jMenuItemCikis = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItemSettings = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowIconified(java.awt.event.WindowEvent evt) {
                formWindowIconified(evt);
            }
        });

        java.awt.FlowLayout flowLayout1 = new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT, 0, 0);
        flowLayout1.setAlignOnBaseline(true);
        jPanel1.setLayout(flowLayout1);

        jlConnected.setIcon(new ImageIcon(ResourceFactory.getDbConnectedImage()));
        jPanel1.add(jlConnected);

        jlDisconnected.setIcon(new ImageIcon(ResourceFactory.getDbDisConnectedImage()));
        jPanel1.add(jlDisconnected);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_START);
        getContentPane().add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        jMenu1.setText("File");

        jMenuItemConnect.setText("Bağlan");
        jMenuItemConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemConnectActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemConnect);

        jMenuItemDisconnect.setText("Bağlantıyı Kes");
        jMenuItemDisconnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemDisconnectActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemDisconnect);

        jMenuItemCikis.setText("Çıkış");
        jMenuItemCikis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemCikisActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemCikis);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");

        jMenuItemSettings.setText("Ayarlar");
        jMenuItemSettings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSettingsActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItemSettings);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        stopServer();

    }//GEN-LAST:event_formWindowClosing

    private void jMenuItemSettingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSettingsActionPerformed
        // TODO add your handling code here:
        this.setEnabled(false);
        JFrame f = new JFrame();
        f.add(new SettingsPanel());
        f.pack();
//        f.setSize(400, 400);
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        f.addWindowListener(new WindowAdapter() {
            @Override
        public void windowClosing(WindowEvent e) {
                MainFrame.this.setEnabled(true);
                MainFrame.this.toFront();
            }

        });
        f.setVisible(true);
    }//GEN-LAST:event_jMenuItemSettingsActionPerformed

    private void formWindowIconified(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowIconified
        // TODO add your handling code here:
        this.setVisible(false);
    }//GEN-LAST:event_formWindowIconified

    private void jMenuItemCikisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemCikisActionPerformed
        // TODO add your handling code here:
        this.exit();
    }//GEN-LAST:event_jMenuItemCikisActionPerformed

    private void jMenuItemConnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemConnectActionPerformed
        // TODO add your handling code here:
        this.startServer();
    }//GEN-LAST:event_jMenuItemConnectActionPerformed

    private void jMenuItemDisconnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemDisconnectActionPerformed
        // TODO add your handling code here:
        this.stopServer();
    }//GEN-LAST:event_jMenuItemDisconnectActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame



.class  


.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame



.class  


.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame



.class  


.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame



.class  


.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItemCikis;
    private javax.swing.JMenuItem jMenuItemConnect;
    private javax.swing.JMenuItem jMenuItemDisconnect;
    private javax.swing.JMenuItem jMenuItemSettings;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel jlConnected;
    private javax.swing.JLabel jlDisconnected;
    // End of variables declaration//GEN-END:variables
private void createTrayIcon() {
        Image img = null;
        try {
            img = ResourceFactory.getBulbImage("tray icon");
        } catch (Exception ex) {
            img = null;
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        trayIcon = new TrayIcon(img, "0 makina bağlı");

        trayIcon.setImageAutoSize(true);
        // Create a popup menu components
        frameItem = new MenuItem("Göster");
        connectionItem = new MenuItem("Bağlan");
        disconnectItem = new MenuItem("Bağlantıyı Kes");
        cikisItem = new MenuItem("Çıkış");

        //Add components to popup menu
        popup.add(frameItem);
        popup.add(connectionItem);
        popup.add(disconnectItem);
        popup.add(cikisItem);

        trayIcon.setPopupMenu(popup);

        trayIcon.addActionListener(new ActionListener() {
            @Override
        public void actionPerformed(ActionEvent e) {
                MainFrame.this.setVisible(true);
            }
        });
        connectionItem.addActionListener(new ActionListener() {
            @Override
        public void actionPerformed(ActionEvent e) {
                startServer();
            }
        });
        disconnectItem.addActionListener(new ActionListener() {
            @Override
        public void actionPerformed(ActionEvent e) {
                stopServer();
            }
        });
        frameItem.addActionListener(new ActionListener() {
            @Override
        public void actionPerformed(ActionEvent e) {
                MainFrame.this.setVisible(true);
            }
        });
        final MainFrame f = this;

        cikisItem.addActionListener(new ActionListener() {
            @Override
        public void actionPerformed(ActionEvent e) {
                f.exit();
            }
        });
    }

    public void exit() {
        stopServer();
        System.exit(0);
    }

    @Override
        public void setVisible(boolean b) {
        super.setVisible(b); //To change body of generated methods, choose Tools | Templates.

        if (b) {
            if (this.trayIcon == null) {
                for (TrayIcon ic : this.tray.getTrayIcons()) {
                    this.tray.remove(ic);
                }
            } else {
                this.tray.remove(trayIcon);
            }
            this.setState(JFrame.NORMAL);
        } else {
            if (this.trayIcon == null) {
                this.createTrayIcon();
            }
            try {
                this.tray.add(trayIcon);
            } catch (AWTException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
        public void serverConnected(ServerEvent ev) {
        this.jMenuItemConnect.setVisible(false);
        this.jMenuItemDisconnect.setVisible(true);
        this.connectionItem.setEnabled(false);
        this.disconnectItem.setEnabled(true);
    }

    @Override
        public void serverDisconnected(ServerEvent ev) {
        this.jMenuItemDisconnect.setVisible(false);
        this.jMenuItemConnect.setVisible(true);
        this.disconnectItem.setEnabled(false);
        this.connectionItem.setEnabled(true);
    }

    @Override
        public void serverConnectionFailed(ServerEvent ev) {
        this.jMenuItemDisconnect.setVisible(false);
        this.jMenuItemConnect.setVisible(true);
        this.disconnectItem.setEnabled(false);
        this.connectionItem.setEnabled(true);

    }

    @Override
        public void clientIdReceived(ClientEvent ev) {
    }

    @Override
        public void clientConnected(ClientEvent ev) {
        int clientNo = SeraServer.clientList.size();
        this.trayIcon.setToolTip(clientNo + " makina Bağlı");
    }

    @Override
        public void clientDisconnected(ClientEvent ev) {
        int clientNo = SeraServer.clientList.size();
        this.trayIcon.setToolTip(clientNo + " makina Bağlı");
    }

    @Override
        public void clientConnectionFailed(ClientEvent ev) {
        int clientNo = SeraServer.clientList.size();
        this.trayIcon.setToolTip(clientNo + " makina Bağlı");
    }

    @Override
        public void dataReceived(DataEvent ev) {
  
    }

    @Override
        public void dataSent(DataEvent ev) {
    }

}
