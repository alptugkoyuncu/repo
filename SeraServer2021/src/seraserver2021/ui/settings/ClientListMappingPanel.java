/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seraserver2021.ui.settings;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.swing.AbstractListModel;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import seraserver2021.db.MySqlServerConnection;
import seraserver2021.socket.SeraServer;

/**
 *
 * @author ISIK
 */
public class ClientListMappingPanel extends JPanel implements SaveableSettingsPanel {
    
    JScrollPane pane1, pane2;
    JList<CheckBoxListItem> jlClientList;
    JList<CheckBoxListItem> jlMappingList;
    JLabel lbl;
    SortedMap<Integer, Boolean> ids;
    SortedMap<Integer, SortedSet<Integer>> idMapping;
//    CheckBoxListItem[] idItems;
    JPopupMenu popup;
    JMenuItem jmEkle, jmSil, jmAktifEet, jmPasifEt;
//    MyListModel<CheckBoxListItem> mdlId, mdlMapping;
    DefaultListModel<CheckBoxListItem> mdlId, mdlMapping;
    
    public ClientListMappingPanel() {
        lbl = new JLabel("LOADING");
        this.setLayout(new GridLayout(1, 0, 40, 40));
        
        init();
    }
    
    @Override
    public ButtonPanelEvent save() {
        SeraServer.saveClientId();
        SeraServer.saveClientMapping();
        this.load();
        return new ButtonPanelEvent(ButtonPanelEvent.EVENT_SUCCESS, "Client Mapping başarıyle kaydedildi");
    }
    
    public void init() {
//        mdlId = new MyListModel<>();
//        mdlMapping = new MyListModel<>();
        mdlId = new DefaultListModel<>();
        mdlMapping = new DefaultListModel<>();
        jlClientList = new JList<>(mdlId);
        jlClientList.setCellRenderer(new MyItemCellRenderer());
        jlClientList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jlMappingList = new JList<>(mdlMapping);
        jlMappingList.setCellRenderer(new CheckboxItemCellRenderer());
        jlMappingList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        jlClientList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    return;
                }
                // clear list2 elements
                jlMappingList.setEnabled(false);
                jlMappingList.clearSelection();
                CheckBoxListItem item;
                for (int i = 0; i < jlMappingList.getModel().getSize(); i++) {
                    item = jlMappingList.getModel().getElementAt(i);
                    item.setSelected(false);
                }
                int index = jlClientList.getSelectedIndex();
                if (index < 0) {
                    return;
                }
                int selectedId = jlClientList.getModel().getElementAt(index).getId();
                Set<Integer> mappedItems = idMapping.get(selectedId);
                System.out.println(mappedItems);
                if (mappedItems != null && !mappedItems.isEmpty()) {
                    for (int i = 0; i < jlMappingList.getModel().getSize(); i++) {
                        item = jlMappingList.getModel().getElementAt(i);
                        if (mappedItems.contains(item.getId())) {
                            item.setSelected(true);
                        }
                    }
                }
                jlMappingList.setEnabled(true);
            }
        });
        
        jlMappingList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                Integer mapKeyIndex = jlClientList.getSelectedIndex();
                if (mapKeyIndex < 0) {
                    jlMappingList.clearSelection();
                    return;
                }
                Integer mapKey = jlClientList.getModel().getElementAt(mapKeyIndex).getId();
                if (e.getValueIsAdjusting()) {
                    return;
                }
                
                int index = jlMappingList.getSelectedIndex();
//                System.err.println("list selection listener " + index);
                if (index < 0) {
                    return;
                }
                CheckBoxListItem item = jlMappingList.getModel().getElementAt(index);
                item.setSelected(!item.isSelected());
                Integer id = item.getId();
                
                SortedSet<Integer> set = idMapping.get(mapKey);
                if (item.isSelected()) {
                    if (set == null) {
                        set = new TreeSet<>();
                        idMapping.put(mapKey, set);
                    }
                    set.add(id);
                } else {
                    set.remove(id);
                }
//                idMapping.put(mapKey, l);
                jlMappingList.clearSelection();
            }
        });
        
        popup = new JPopupMenu();
        jmEkle = new JMenuItem("ekle");
        jmEkle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String val = JOptionPane.showInputDialog("id  yi giriniz");
                if (val != null) {
                    try {
                        int i = Integer.parseInt(val);
//                        mdlId.add(new CheckBoxListItem(i,false));
                        mdlId.addElement(new CheckBoxListItem(i, false));
                        mdlMapping.addElement(new CheckBoxListItem(i,false));
                        ids.put(i, Boolean.FALSE);
//                        jlClientList.repaint();
                    } catch (NumberFormatException ex) {
                        
                    }
                }
            }
        });
        jmSil = new JMenuItem("Sil");
        jmSil.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int opt=JOptionPane.showConfirmDialog(null, "Silme işleminde bağlı olduğu datalarda silinir\nSilme işlemini onayloıyor musunuz","",JOptionPane.YES_NO_OPTION);
                if(opt==JOptionPane.NO_OPTION)
                    return;
                int selectedIndex = jlClientList.getSelectedIndex();
                CheckBoxListItem chk = mdlId.remove(selectedIndex);
                int id= chk.getId();
                int ind=-1;
                for(int i=mdlMapping.size()-1; i>=0; i--){
                    if(mdlMapping.get(i).getId()==id){
                        ind=i;
                        break;
                    }
                }
                if(ind>=0 && ind<mdlMapping.size())
                    mdlMapping.remove(ind);
                ids.remove(id);
                idMapping.remove(id);
            }
        });
        jmAktifEet = new JMenuItem("Aktif et");
        jmAktifEet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIndex = jlClientList.getSelectedIndex();
                CheckBoxListItem item = mdlId.get(selectedIndex);
                item.setSelected(true);
                ids.put(item.getId(), true);
            }
        });
        jmPasifEt = new JMenuItem("Pasit et");
        jmPasifEt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIndex = jlClientList.getSelectedIndex();
                CheckBoxListItem item = mdlId.get(selectedIndex);
                item.setSelected(false);
                ids.put(item.getId(), false);
            }
        });
        popup.add(jmAktifEet);
        popup.add(jmPasifEt);
        popup.add(jmSil);
        popup.add(jmEkle);
        this.jlClientList.addMouseListener(new MouseListener() {
            
            @Override
            public void mouseClicked(MouseEvent e) {
                showPopup(e);
            }
            
            @Override
            public void mousePressed(MouseEvent e) {
                showPopup(e);
            }
            
            @Override
            public void mouseReleased(MouseEvent e) {
                showPopup(e);
            }
            
            @Override
            public void mouseEntered(MouseEvent e) {
            }
            
            @Override
            public void mouseExited(MouseEvent e) {
            }
            
        });
        
    }
    
    private void showPopup(MouseEvent e) {
        if (!e.isPopupTrigger()) {
            return;
        }
        popup.removeAll();
        int selectedIndex = this.jlClientList.getSelectedIndex();
        popup.add(jmEkle);
        if (selectedIndex >= 0) {
            
            CheckBoxListItem item = mdlId.getElementAt(selectedIndex);
            String name = item.getId() + "";
            jmSil.setText(name + " sil");
            popup.add(jmSil);
            if (item.isSelected()) {
                jmPasifEt.setText(name + " pasif et");
                popup.add(jmPasifEt);
            } else {
                jmAktifEet.setText(name + " aktif et");
                popup.add(jmAktifEet);
            }
        }
        popup.show(e.getComponent(), e.getX(), e.getY());
    }
    
    @Override
    public ButtonPanelEvent load() {
        
        mdlId.clear();
        mdlMapping.clear();
        SeraServer.loadClientId();
        SeraServer.loadClientMapping();
        
        ids = SeraServer.getClientId();
        idMapping = SeraServer.getClientIdMapping();
        this.removeAll();
        this.add(lbl);
        int index = 0;
//        idItems = new CheckBoxListItem[ids.size()];
//        CheckBoxListItem[] mappingItems = new CheckBoxListItem[ids.size()];
//        for (Integer id : ids.keySet()) {
//            idItems[index] = new CheckBoxListItem(id, ids.get(id));
//            mappingItems[index++] = new CheckBoxListItem(id);
//        }
//        mdlId.addAll(idItems);

//        mdlMapping.addAll(mappingItems);
//System.err.println("idss: " +ids);
        for (Integer id : ids.keySet()) {
            mdlId.addElement(new CheckBoxListItem(id, ids.get(id)));
            mdlMapping.addElement(new CheckBoxListItem(id));
        }
        this.remove(this.lbl);
        JPanel left = new JPanel(new BorderLayout());
        JPanel rigth = new JPanel(new BorderLayout());
        
        left.add(new JLabel("Gönderen ID:"),BorderLayout.PAGE_START);
        
        rigth.add(new JLabel("İlgilenen ID:"),BorderLayout.PAGE_START);
        left.add(new JScrollPane(jlClientList),BorderLayout.CENTER);
        rigth.add(new JScrollPane(jlMappingList),BorderLayout.CENTER);
        this.add(left);
        this.add(rigth);
        this.validate();
        this.jlMappingList.clearSelection();
        this.jlClientList.clearSelection();
        jlMappingList.setEnabled(false);
        return new ButtonPanelEvent(ButtonPanelEvent.EVENT_SUCCESS, "client maping list başarıyla yüklendi");
    }
    
    @Override
    public ButtonPanelEvent resetToDefault() {
        return this.load();
    }
    
    @Override
    public ButtonPanelEvent reset() {
        return this.load();
    }

    /**
     * @param <T>
     */
    class MyListModel<T> extends AbstractListModel {
        
        List<T> ids;
        
        public MyListModel() {
            this.ids = new ArrayList<>();
        }
        
        public MyListModel(T[] ids) {
            addAll(ids);
        }
        
        public void add(T element) {
            if (ids.contains(element)) {
                return;
            }
            ids.add(element);
            ids.sort(null);
        }
        
        public void addAll(T[] ids) {
            
            for (int i = 0; i < ids.length; i++) {
                this.ids.add(ids[i]);
            }
            this.ids.sort(null);
        }
        
        @Override
        public int getSize() {
            return ids.size();
        }
        
        @Override
        public T getElementAt(int index) {
            return ids.get(index);
        }
        
        public void clear() {
            ids.clear();
        }
        
    }
    
    class CheckBoxListItem implements Comparable<CheckBoxListItem> {
        
        private int id;
        private boolean selected;
        
        public Integer getId() {
            return id;
        }
        
        public boolean isSelected() {
            return selected;
        }
        
        public void setSelected(boolean selected) {
            this.selected = selected;
        }
        
        public CheckBoxListItem(int id) {
            this(id, false);
        }
        
        public CheckBoxListItem(int id, boolean selected) {
            this.id = id;
            this.selected = selected;
        }
        
        @Override
        public int compareTo(CheckBoxListItem o) {
            if (o == null) {
                return 1;
            }
            return this.getId().compareTo(o.getId());
        }
        
        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (obj instanceof CheckBoxListItem) {
                CheckBoxListItem c = (CheckBoxListItem) obj;
                return this.getId() == c.getId();
            }
            return false;
        }
        
    }
    
    class MyItemCellRenderer implements ListCellRenderer<CheckBoxListItem> {
        
        @Override
        public Component getListCellRendererComponent(JList<? extends CheckBoxListItem> list, CheckBoxListItem value, int index, boolean isSelected, boolean cellHasFocus) {
            JCheckBox lbl = new JCheckBox(value.getId() + "", value.isSelected());
            setFont(list.getFont());
            lbl.setOpaque(isSelected);
            if (isSelected) {
                lbl.setBackground(list.getSelectionBackground());
                lbl.setForeground(list.getSelectionForeground());
            } else {
                lbl.setBackground(list.getBackground());
                lbl.setForeground(list.getForeground());
            }
            return lbl;
        }
        
    }
    
    class CheckboxItemCellRenderer implements ListCellRenderer<CheckBoxListItem> {
        
        @Override
        public Component getListCellRendererComponent(JList<? extends CheckBoxListItem> list, CheckBoxListItem value, int index, boolean isSelected, boolean cellHasFocus) {
            JCheckBox chk = new JCheckBox(value.getId() + "", value.isSelected());
            chk.setEnabled(list.isEnabled());
            setFont(list.getFont());
            chk.setOpaque(value.isSelected());
            if (value.isSelected()) {
                chk.setBackground(list.getSelectionBackground());
                chk.setForeground(list.getSelectionForeground());
            } else {
                chk.setBackground(list.getBackground());
                chk.setForeground(list.getForeground());
            }
            return chk;
        }
        
    }
}
