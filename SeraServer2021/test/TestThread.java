
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ISIK
 */
public class TestThread extends Thread{
    boolean flag=false;

    public TestThread() {
        flag = true;
    }
    
    public void interrrupt(){
        flag=false;
        this.interrupt();
    }
    @Override
    public void run() {
        System.out.println("before loop");
        while(flag){
            System.out.println("running");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
//                Thread.currentThread().interrupt();
                System.out.println("Thread interrupted");
//                Logger.getLogger(TestThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Thread dead");
    }
    public static void main(String[] args) {
        TestThread t = new TestThread();
        t.start();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            Logger.getLogger(TestThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        t.interrrupt();
    }
    
}
