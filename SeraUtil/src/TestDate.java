
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alptug
 */
public class TestDate {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(0);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar c2= Calendar.getInstance();
        Calendar c3 = Calendar.getInstance();
        long l = c2.getTimeInMillis();
        l=l/60000;
        l=l*60000;
        c2.set(Calendar.SECOND, 0);
        c2.set(Calendar.MILLISECOND, 0);
        c3.setTimeInMillis(l);
        c.set(Calendar.YEAR, 2019);
        System.out.println(df.format(c2.getTime()));
        System.out.println(df.format(c3.getTime())); 
        System.out.println(c2.getTimeInMillis()-c.getTimeInMillis());
    }
    
}
