/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data;
import java.util.EventObject;


/**
 *
 * @author WIN7
 */

public class DataTransferEvent extends EventObject{
    
    data.NetworkPackage pck;
    
    public DataTransferEvent(Object source,data.NetworkPackage pck) {
        super(source);
        this.pck = pck;
    }
    
    public data.NetworkPackage getNetworkPackage(){    
        return this.pck;
    }
}
