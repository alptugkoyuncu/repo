/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import static data.NetworkPackage.BASLANGIC_AYIRACI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import my_logger.L;
import network.GeneralSystemConstant;
import static network.GeneralSystemConstant.ERROR_VALUE;

/**
 *
 * @author alptug (serverdan) clienta gelen yada gönderilen data ()yapisi <h1:ac1 n1 n2>
 * seklindedir.'h' headeri temsil eder.'n' datalardir 'ac' actioncode dur. Eger
 * client tarafindan hazirlanan data ise h1 : client ID dir Eger server
 * tarafinda hazrilanan data ise h1:server id dir. Eger h1 yoksa serverdan
 * gelmis kabul edilir
 *
 * TODO:json string olacak
 */
public class NetworkClientPackage extends NetworkPackage {
private Date creationTime;

    public NetworkClientPackage() {
        super();
        creationTime=null;
    }


    @Override
    public void reset() {
        super.reset(); //To change body of generated methods, choose Tools | Templates.
        creationTime=null;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
    @Override
    public void setNetworkData(String networkData) {
        this.receiverList = new HashSet<Short>();
        this.networkData = networkData;
        StringBuilder err = new StringBuilder();
        StringBuilder frmErr = new StringBuilder();
        boolean hasErr = false;
        String temp = this.networkData;
        temp = temp.replace(BASLANGIC_AYIRACI, "");
        temp = temp.replace(BITIS_AYIRACI, "");
        temp = temp.trim();
        String t1[] = temp.split(HEADER_AYIRACI);
        String tmp;
        if (t1.length > 1) {
            String[] headers = t1[0].split(DATA_AYIRACI);
            this.sender = Short.parseShort(headers[0].trim());
            tmp = t1[1];
            if(t1.length>2){
                try{
                    this.creationTime = new Date(Long.parseLong(t1[2].trim()));
                }catch(NumberFormatException ex){
                    this.creationTime=null;
                }
            }
        } else {
            this.sender = GeneralSystemConstant.SERVER_ID;
            tmp = t1[0];
        }
        String[] strData = tmp.split(DATA_AYIRACI);

        try {
            this.action = Short.parseShort(strData[0]);
        } catch (Exception e) {
            err.append("Action datasi parse edilemedi");
            frmErr.append(e.getMessage());
            hasErr = true;
            this.action =ERROR_VALUE.shortValue();
        }
        this.dataList = new ArrayList<Double>();
        for (int i = 1; i < strData.length; i++) {
            Double val = null;
            String str = strData[i].trim();
            str = str.replace(',', '.');
            try {
                val = Double.parseDouble(str);
            } catch (NumberFormatException ex) {
                L.err("Number Format Exception :" + val);
                val = ERROR_VALUE;
            }
            this.dataList.add(val);
        }
    }

    @Deprecated
    public void addReceiverList(Short receiver) {
        this.setReceiver(receiver);
     }
    public void setReceiver(Short receiver){
        this.receiverList.clear();
        this.receiverList.add(receiver);
    }

    @Deprecated
    public Set<Short> getReceiverList() {
        return this.receiverList;
    }
    public Short getReceiver(){
        
        return this.receiverList.iterator().next();
    }
    @Override
    protected void createNetworkData() {
        this.networkData = BASLANGIC_AYIRACI+(this.sender==null? GeneralSystemConstant.SERVER_ID : this.sender)+HEADER_AYIRACI+this.action;
        for (Double d : dataList) {
            this.networkData += " " + d;
        }
        if(creationTime!=null){
            this.networkData+=HEADER_AYIRACI+creationTime.getTime();
        }
        this.networkData += BITIS_AYIRACI;
    }

    @Override
    public String toString() {
        return "Server to Client Package:"+super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public Date getCreationTime() {
        return creationTime;
    }
}
