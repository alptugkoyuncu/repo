/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author alptug
 */
public abstract class NetworkPackage {

    protected final static String BASLANGIC_AYIRACI = "<";
    protected final static String BITIS_AYIRACI = ">";
    protected final static String HEADER_AYIRACI = ":";
    protected final static String DATA_AYIRACI = " ";
    protected integer sender;
    protected Set<Short> receiverList;
    protected Short action;
    protected List<Double> dataList;
    protected String networkData;


    public NetworkPackage() {
        this.sender = null;
        this.receiverList = new HashSet<Short>();
        this.networkData = null;
        this.dataList = new ArrayList<Double>();
        this.action = null;
    }

    public void reset() {
        this.sender = null;
        this.networkData = null;
        this.action = null;
        if (this.receiverList != null) {
            this.receiverList.clear();
        }
        if (this.dataList != null) {
            this.dataList.clear();
        }
    }

    public void setAction(Short action) {
        this.action = action;
    }

    public Short getAction() {
        return action;
    }

    public void addData(Double data) {
        if (data.isNaN()) {
            return;
        }
        if (this.dataList == null) {
            this.dataList = new ArrayList<Double>();
        }
        this.dataList.add(data);
    }

    public List<Double> getData() {
        return dataList;
    }

    public void setSender(Short sender) {
        this.sender = sender;
    }

    public Short getSender() {
        return sender;
    }

    public void addReceiverList(Short receiver) {
        if (this.receiverList == null) {
            this.receiverList = new HashSet<Short>();
        }
        this.receiverList.add(receiver);
    }

    public Set<Short> getReceiverList() {
        return receiverList == null ? new HashSet<Short>() : receiverList;
    }

    public abstract void setNetworkData(String networkData);

    protected abstract void createNetworkData();

    public String getNetworkData() {
        if (this.networkData == null) {
            createNetworkData();
        }
        return networkData;
    }

    @Override
    public String toString() {
        /*
        String send = "<" + (this.sender == null ? "-" : this.sender) + "-->";
        String recv = "";
        for (Short r : this.getReceiverList()) {
            recv += r + ",";
        }
        if (recv.length() > 0) {
            recv = recv.substring(0, recv.length() - 1);
        }else
            recv="-";
        String data = "";
        for (Double d : this.getData()) {
            data += String.format("%.2f ", d);
        }
        if (data.length() > 0) {
            data = data.substring(0, data.length() - 1);
        }else
            data="-";
        return send+recv+":"+this.action+data;
        */
        return "Sender : "+this.sender
                +"\nReceiver : "+this.receiverList.toString()
                +"\nAction:"+this.action
                +"\nData:"+this.dataList.toString()
                +"\nNetwork Data : "+this.getNetworkData();
}

}
