/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import my_logger.L;
import static network.GeneralSystemConstant.ERROR_VALUE;
import util.BasicTime;

/**
 *
 * @author alptug
 * (Clienttan) servera gelen (clienttan)server' gönderilecek data 
 * yapisi <h1 h2 ...:ac1 n1 n2> seklindedir.'h' headeri temsil eder.'n' datalardir 'ac' actioncode dur.
 * header kismi bos ise serverdan gonderilecek kisimlar otomatik bulunur.
 * header kismi dolu ise client ozellikle bir yere gondermis demektir ve oraya gonderilir.
 *
 * TODO:json string olacak
 */
public class NetworkServerPackage extends NetworkPackage {

 
    
/** 
 * @param recvData clientin gonderdigi orjinal data dir <h1,h2..hn:ac1,d1 d2 ..dn> şeklindedir 
 */
    @Override
    public void setNetworkData(String recvData) {
        this.receiverList = new HashSet<Short>();
        this.networkData = recvData;
        StringBuilder err = new StringBuilder();
        StringBuilder frmErr = new StringBuilder();
        boolean hasErr = false;
        String temp = this.networkData;
        temp = temp.replace(BASLANGIC_AYIRACI, "");
        temp = temp.replace(BITIS_AYIRACI, "");
        temp = temp.trim();
        String t1[] = temp.split(HEADER_AYIRACI);
        String tmp;
        if (t1.length > 1) {
            String[] headers = t1[0].split(DATA_AYIRACI);
            for (String recv : headers) {
                this.receiverList.add(Short.parseShort(recv.trim()));
            }
            tmp = t1[1];
        } else {
            tmp = t1[0];
        }
        String[] strData = tmp.split(DATA_AYIRACI);

        try {
            this.action = Short.parseShort(strData[0]);
        } catch (Exception e) {
            err.append("Action datasi parse edilemedi");
            frmErr.append(e.getMessage());
            hasErr = true;
            this.action = ERROR_VALUE.shortValue();
        }
        this.dataList = new ArrayList<Double>();
        for (int i = 1; i < strData.length; i++) {
            Double val = null;
            String str = strData[i].trim();
            str = str.replace(',', '.');
            try {
                val = Double.parseDouble(str);
            } catch (NumberFormatException ex) {
                L.err("Number Format Exception :" + val);
                val = ERROR_VALUE;
            }
            this.dataList.add(val);
        }
    }

    @Override
    public void createNetworkData() {
//        if (this.action == null || this.dataList == null) {
//            throw new Exception("Action veya data null");
//        }
        this.networkData = "<";
        if (this.receiverList != null && !this.receiverList.isEmpty()) {
            for (Short cl : this.receiverList) {
                this.networkData += cl + " ";
            }
            this.networkData = this.networkData.substring(0, this.networkData.length() - 1)+":";
        }
        this.networkData += this.action+"";
        for (Double d : dataList) {
            this.networkData += " " + d;
        }
        this.networkData += ">";
    }

    @Override
    public String toString() {
        return "Client to Server Package : \n"+super.toString(); //To change body of generated methods, choose Tools | Templates.
    }
    
}