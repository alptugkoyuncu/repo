/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

/**
 *
 * @author alptug
 */
public enum DBDateRecordInterval {
        PER_MILLISECOND(1), PER_SECOND(1000), PER_MINUTE(60000), PER_5_MINUTES(300000);
        private final int sabit;

        private DBDateRecordInterval(int sabit) {
            this.sabit = sabit;
        }

        public long convert(long value) {
            return (value / sabit) * sabit;
        }
    }
