/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my_logger;

import util.BasicTime;

/**
 *
 * @author alptug
 */
public class L {

    private static final boolean DEBUG = true;

    private L() {

    }

    public static void debug(String m) {
        if (DEBUG) {
            System.out.println(BasicTime.getCurrentLongTime() + " ---> " + m);
        }
    }

    public static void info(String m) {
        System.out.println(BasicTime.getCurrentLongTime() + " ---> " + m);
    }

    public static void err(String err) {
        System.err.println(BasicTime.getCurrentLongTime() + " ---> " + err);
    }
}
