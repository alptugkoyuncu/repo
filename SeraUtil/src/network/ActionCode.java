/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

/**
 *
 * @author alptug
 */
public interface ActionCode {

    public final static short SENT_PING = 99;
    public final static short CLIENT_CONNECTED = 1;
    public final static short CLIENT_DISCONNECTED = 2;
    
    public final static short REQUEST_CLIENT_TYPE = 50;
    @Deprecated
    public final static short CLIENT_TYPE_OK = 51;
    public final static short REQUEST_FOCUED_PROVIDERS = 60;

    public final static short AC_INVALID_CODE = 0;

    public final static short AC_SEND_TEMP0 = 10;
    public final static short AC_SEND_TEMP1 = 11;
    public final static short AC_SEND_TEMP2 = 12;
    public final static short AC_SEND_TEMP3 = 13;
    public final static short AC_SEND_BOARD_LAST_RESET_TIME=20;
    @Deprecated
    public final static short AC_REQUEST_ID = 50;

    public final static short AC_SEND_WIFI_SSID = 101;
    public final static short AC_SEND_PASS = 102;
    public final static short AC_SEND_IP = 103;
    public final static short AC_SEND_PORT = 104;
    public final static short AC_SEND_ID = 105;

    public final static short AC_ESP_STARTED = 106;
    public final static short AC_RESET_ESP = 107;

    public final static short AC_WIFI_CONNECTED = 108;
    public final static short AC_WIFI_DISCONNECTED = 109;
    public final static short AC_WIFI_CONNECTION_FAILED = 110;

    public final static short AC_SERVER_CONNECTED = 111;
    public final static short AC_SERVER_DISCONNECTED = 112;
    public final static short AC_SERVER_CONNECTION_FAILED = 113;

    public final static short AC_ID_REQUESTED = 113;
    public final static short AC_ID_ACCEPTED = 114;
    public final static short AC_ID_REFUSED = 115;

}
