/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

/**
 *
 * @author alptug
 */
public enum ConnecterType {
    SERVER(0),PROVIDER(1),WATCHER(2);
    private int type;
    ConnecterType(int type){
        this.type=type;
    }
    public int getType() {
        return type;
    }
    //id nin binler basamigi type dir gerisi id degeridir
    public static ConnecterType  getTypeById(short id){
        for(ConnecterType t:ConnecterType.values())
            if(t.type==id/1000)
                return t;
        return null;
    }
}
