/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.Serializable;
import java.util.Objects;
import sera.IdNamePair;
import sera.NewInstancable;

/**
 *
 * @author ISIK
 */
public class Gubre implements Serializable,NewInstancable,IdNamePair {

    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private String adi;
    private GubreCesidi gubreCesidi;
    private UrunMarkasi gubreMarkasi;

    public Gubre() {
        id = -1;
        adi = "";
        gubreCesidi = new GubreCesidi();
        gubreMarkasi = new UrunMarkasi();
    }

    public Gubre(Integer id, String adi, GubreCesidi gubreCesidi, UrunMarkasi gubreMarkasi) {
        this.id = id;
        this.adi = adi;
        this.gubreCesidi = gubreCesidi;
        this.gubreMarkasi = gubreMarkasi;
    }
    
     @Override
    public Gubre getClone() {
        return new Gubre(id,this.adi,this.gubreCesidi.getClone(),this.gubreMarkasi.getClone());
    }
   

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }

    public GubreCesidi getGubreCesidi() {
        return gubreCesidi;
    }

    public void setGubreCesidi(GubreCesidi gubreCesidi) {
        this.gubreCesidi = gubreCesidi;
    }

    public UrunMarkasi getGubreMarkasi() {
        return gubreMarkasi;
    }

    public void setGubreMarkasi(UrunMarkasi gubreMarkasi) {
        this.gubreMarkasi = gubreMarkasi;
    }
     @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gubre)) {
            return false;
        }
        Gubre other = (Gubre) object;
        if ((this.id == null && other.getId() != null) || (this.id != null && !this.id.equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Gubre{" + "id=" + id + ", adi=" + adi + ", gubreCesidi=" + gubreCesidi + ", gubreMarkasi=" + gubreMarkasi + '}';
    }

    @Override
    public String getName() {
        return this.getAdi();
    }

    @Override
    public void setName(String name) {
        this.setAdi(adi);
    }
}
