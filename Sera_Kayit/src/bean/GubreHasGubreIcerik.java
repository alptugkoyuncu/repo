/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import sera.HasId;
import sera.NewInstancable;

/**
 *
 * @author ISIK
 */
public class GubreHasGubreIcerik implements Serializable, NewInstancable, HasId{

    private Integer id;
    private Integer gubreId;
    private GubreIcerik gubreIcerik;
    private BigDecimal oran;

    public GubreHasGubreIcerik(Integer id, Integer gubreId, GubreIcerik gubreIcerik, BigDecimal oran) {
        this.id = id;
        this.gubreId = gubreId;
        this.gubreIcerik = gubreIcerik;
        this.oran = oran;
    }

    public GubreHasGubreIcerik() {
        this.id=null;
        this.gubreId=null;
        this.gubreIcerik=null;
        this.oran=null;
    }
    
    @Override
    public Object getClone() {
        return new GubreHasGubreIcerik(id, gubreId, gubreIcerik, oran);
    }

    @Override
    public void setId(Integer id) {
        this.id=id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GubreHasGubreIcerik other = (GubreHasGubreIcerik) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public Integer getGubreId() {
        return gubreId;
    }

    public void setGubreId(Integer gubreId) {
        this.gubreId = gubreId;
    }

    public BigDecimal getOran() {
        return oran;
    }

    public void setOran(BigDecimal oran) {
        this.oran = oran;
    }

    public GubreIcerik getGubreIcerik() {
        return gubreIcerik;
    }

    public void setGubreIcerik(GubreIcerik gubreIcerik) {
        this.gubreIcerik = gubreIcerik;
    }

    @Override
    public String toString() {
        return "GubreHasGubreIcerik{" + "id=" + id + ", gubreId=" + gubreId + ", gubreIcerik=" + gubreIcerik + ", oran=" + oran + '}';
    }


}
