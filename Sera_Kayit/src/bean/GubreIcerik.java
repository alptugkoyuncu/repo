/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.Serializable;
import java.util.Objects;
import sera.IdNamePair;
import sera.NewInstancable;

/**
 *
 * @author ISIK
 */
public class GubreIcerik implements IdNamePair,Serializable,NewInstancable{
    private Integer id;
    private String adi;

    public GubreIcerik() {
        this.id=null;
        this.adi=null;
    }
    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "GubreIcerik{" + "id=" + id + ", adi=" + adi + '}';
    }

    public GubreIcerik(Integer id, String adi) {
        this.id = id;
        this.adi = adi;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GubreIcerik other = (GubreIcerik) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    

    
    public void setId(int id) {
        this.id = id;
    }

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }

    @Override
    public String getName() {
        return adi;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public GubreIcerik getClone() {
        return new GubreIcerik(id, adi);
    }

    @Override
    public void setName(String name) {
        this.setAdi(adi);
    }

   
}
