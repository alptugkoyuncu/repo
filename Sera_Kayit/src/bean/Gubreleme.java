/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;
import sera.HasId;
import sera.NewInstancable;

/**
 *
 * @author ISIK
 */
public class Gubreleme implements Serializable, NewInstancable,HasId {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private Integer sure;
    private Sera sera;
    private java.util.Date date;
    private String oran;
    private BigDecimal kilo;

    public Gubreleme() {
        id=-1;
        sure=0;
        sera= new Sera();
        date=null;
        oran="";
        this.kilo=null;
    }

    public Gubreleme(Integer id, Integer sure, Sera sera, Date date, String oran, BigDecimal kilo) {
        this.id = id;
        this.sure = sure;
        this.sera = sera;
        this.date = date;
        this.oran = oran;
        this.kilo = kilo;
    }

    

    public java.util.Date getDate() {
        return date;
    }

    public void setDate(java.util.Date date) {
        this.date = date;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSure() {
        return sure;
    }

    public void setSure(Integer sure) {
        this.sure = sure;
    }

    public Sera getSera() {
        return sera;
    }

    public void setSera(Sera sera) {
        this.sera = sera;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Gubreleme{");
        sb.append("id=").append(id);
        sb.append(", sure=").append(sure);
        sb.append(", sera=").append(sera);
        sb.append(", date=").append(date);
        sb.append(", oran=").append(oran);
        sb.append(", kilo=").append(kilo);
        sb.append('}');
        return sb.toString();
    }


    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Gubreleme other = (Gubreleme) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public String getOran() {
        return oran;
    }

    public void setOran(String oran) {
        this.oran = oran;
    }

    public BigDecimal getKilo() {
        return kilo;
    }

    public void setKilo(BigDecimal kilo) {
        this.kilo = kilo;
    }

    @Override
    public Object getClone() {
        return new Gubreleme(id, sure, sera, date, oran, kilo);
    }


}
