/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import sera.HasId;
import sera.NewInstancable;

/**
 *
 * @author ISIK
 */
public class GubrelemeHasGubre implements Serializable, NewInstancable, HasId {

    private Integer id;
    private Integer gubrelemeId;
    private Gubre gubre;
    private BigDecimal miktar;

    public GubrelemeHasGubre() {
        this.id=null;
        this.gubrelemeId=null;
        this.gubre = null;
        this.miktar = null;
    }

    public GubrelemeHasGubre(Integer id, Integer idGubreleme, Gubre gubre, BigDecimal miktar) {
        this.id = id;
        this.gubrelemeId = idGubreleme;
        this.gubre = gubre;
        this.miktar = miktar;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGubrelemeId() {
        return gubrelemeId;
    }

    public void setGubrelemeId(Integer gubrelemeId) {
        this.gubrelemeId = gubrelemeId;
    }

    public Gubre getGubre() {
        return gubre;
    }

    public void setGubre(Gubre gubre) {
        this.gubre = gubre;
    }

    public BigDecimal getMiktar() {
        return miktar;
    }

    public void setMiktar(BigDecimal miktar) {
        this.miktar = miktar;
    }

    @Override
    public Object getClone() {
        return new GubrelemeHasGubre(id, gubrelemeId, gubre, miktar);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GubrelemeHasGubre other = (GubrelemeHasGubre) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "GubrelemeHasGubre{" + "id=" + id + ", idGubreleme=" + gubrelemeId + ", gubre=" + gubre + ", miktar=" + miktar + '}';
    }



}
