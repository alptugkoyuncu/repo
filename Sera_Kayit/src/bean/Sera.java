/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.Serializable;
import java.util.Objects;
import sera.IdNamePair;
import sera.NewInstancable;

/**
 *
 * @author ISIK
 */
public class Sera implements Serializable, NewInstancable, IdNamePair {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String name;

    public Sera() {
    }

    public Sera(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sera other = (Sera) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Sera{" + "id=" + id + ", name=" + name + '}';
    }

    @Override
    public Sera getClone() {
        return new Sera(id, name);
    }

}
