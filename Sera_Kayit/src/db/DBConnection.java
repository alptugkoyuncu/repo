/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import bean.*;
import db.restriction.DBRestriction;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

/**
 *
 * @author ISIK
 */
public abstract class DBConnection {

    protected static LocalDateTime toLocalDateTime(long millis) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), TimeZone.getDefault().toZoneId());
    }

    protected static Long toMillis(LocalDateTime dt) {
        return dt.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
    
    /**
     *
     * @param res
     * @return
     */
    public abstract List<Sera> selectSera(DBRestriction res);
    
    /**
     * IMPLEMENT EDILMEDI
     * @param sera eklenecek sera listesi
     * @return eklenmis sera listesi 
     */
    public abstract List<Sera> insertIntoSera(List<Sera> sera);
    
    /**
     * IMPLEMENT EDILMEDI
     * @param sera guncellenecek sera listesi
     * @return guncellenmis sera listesi
     */
    public abstract List<Sera> updateSera(List<Sera> sera);

    /**
     * IMPLEMENT EDILMEDI
     * @param sera silinecek sera listesi
     * @return silinmis sera listesi
     */
    public abstract List<Sera> deleteFromSera(List<Sera> sera);
    
    /**
     *
     * @param res
     * @return
     */
    public abstract List<Gubre> selectGubre(DBRestriction res);

    /**
     *
     * @param gubre eklenecek gubre listesi
     * @return eklenen gubre listesi
     */
    public abstract List<Gubre> insertIntoGubre(List<Gubre> gubre);

    /**
     *
     * @param gubre guncellenecek gubre listesi
     * @return guncellenen gubre listesi
     */
    public abstract List<Gubre> updateGubre(List<Gubre> gubre);

    /**
     *
     * @param gubre silinecek gubre listesi
     * @return silinen gubre listesi
     */
    public abstract List<Gubre> deleteFromGubre(List<Gubre> gubre);

    /**
     *
     * @param res
     * @return
     */
    public abstract List<Gubreleme> selectGubreleme(DBRestriction res);

    /**
     *
     * @param glist eklenecek gubreler
     * @return eklenmis gubreler database tarafindan atanmis idleri ile birlikte
     */
    public abstract List<Gubreleme> InsertIntoGubreleme(List<Gubreleme> glist);

    /**
     *
     * @param glist
     * @return
     */
    public abstract List<Gubreleme> updateGubreleme(List<Gubreleme> glist);

    /**
     *
     * @param glist silinecek gubre listesi
     * @return silinen gubre listesi
     */
    public abstract List<Gubreleme> deleteFromGubreleme(List<Gubreleme> glist);

    /**
     * 
     * @param res
     * @return 
     */
     public abstract List<GubrelemeHasGubre> selectGubrelemeHasGubres(DBRestriction res);

    /**
     *
     * @param glist eklenecek liste
     * @return eklenmis liste
     */
    public abstract List<GubrelemeHasGubre> ReplaceIntoGubrelemeHasGubres(List<GubrelemeHasGubre> glist);

    /**
     * 
     * @param glist silinecek liste
     * @return silinmis liste
     */
    public abstract List<GubrelemeHasGubre> deleteFromGubrelemeHasGubres(List<GubrelemeHasGubre> glist);

   /**
     *
     * @param res
     * @return
     */
    public abstract List<UrunMarkasi> selectUrunMarkasi(DBRestriction res);

    /**
     *
     * @param urunList eklenecek urun markasi listesi
     * @return eklenen Urun Markasi listesi
     */
    public abstract List<UrunMarkasi> InsertIntoUrunMarkasi(List<UrunMarkasi> urunList);

    /**
     *IMPLEMENT EDILMEDI
     * @param urunList guncellenecek urun Marka listesi
     * @return guncellenen urun Marka listesi
     */
    public abstract List<UrunMarkasi> updateUrunMarkasi(List<UrunMarkasi> urunList);

    /**
     *IMPLEMENT EDILMEDI
     * @param urunList silinecek urun listesi
     * @return silinen urun listesi
     */
    public abstract List<UrunMarkasi> deleteFromUrunMarkasi(List<UrunMarkasi> urunList);

    /**
     *
     * @param res
     * @return
     */
    public abstract List<GubreCesidi> selectGubreCesidi(DBRestriction res);

    /**
     *
     * @param list eklenecek gubre cesidi listesi
     * @return list eklenen gubre cesidi listesi
     */
    public abstract List<GubreCesidi> InsertIntoGubreCesidi(List<GubreCesidi> list);

    /**
     *IMPLEMENT EDILMEDI
     * @param list guncellenecek gubre listesi
     * @return guncellenen gubre listesi
     */
    public abstract List<GubreCesidi> updateGubreCesidi(List<GubreCesidi> list);

    /**
     *IMPLEMENT EDILMEDI
     * @param list silinecek gubre listesi
     * @return silinmeyen gubre listesi
     */
    public abstract List<GubreCesidi> deleteGubreCesidi(List<GubreCesidi> list);
 
    /**
     *
     * @param res secim classi
     * @return
     */
    public abstract List<GubreIcerik> selectGubreIcerik(DBRestriction res);

    /**
     *IMPLEMENT EDILMEDI
     * @param list eklenecek gubre Icerik listesi
     * @return eklenen gubre icerik listesi
     */
    public abstract List<GubreIcerik> insertIntoGubreIcerik(List<GubreIcerik> list);

    /**
     *IMPLEMENT EDILMEDI
     * @param list guncellenecek gubre icerik
     * @return guncellenen gubre icerik listesi
     */
    public abstract List<GubreIcerik> updateGubreIcerik(List<GubreIcerik> list);

    /**
     *IMPLEMENT EDILMEDI
     * @param list silinecen gubre icerik listesi
     * @return silinenmeyen gubre icerik listesi
     */
    public abstract List<GubreIcerik> deleteGubreIcerik(List<GubreIcerik> list);

    /**
     *IMPLEMENT EDILMEDI
     * @param res
     * @return secilen liste
     */
    public abstract List<GubreHasGubreIcerik> selectGubreHasGubreIcerik(DBRestriction res);

    /**
     *
     * @param list eklenecek veya degistirilecek liste
     * @return eklenmis veya degistrilmis liste
     */
    public abstract List<GubreHasGubreIcerik> replaceIntoGubreHasGubreIcerik(List<GubreHasGubreIcerik> list);

    /**
     * IMPLEMENT EDILMEDI
     * @param list silinecek liste
     * @return silinmis liste
     */
    public abstract List<GubreHasGubreIcerik> deleteFromGubreHasGubreIcerik(List<GubreHasGubreIcerik> list);
    
}
