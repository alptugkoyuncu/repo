/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import bean.Gubre;
import bean.GubreCesidi;
import bean.GubreHasGubreIcerik;
import bean.GubreIcerik;
import bean.Gubreleme;
import bean.GubrelemeHasGubre;
import bean.Sera;
import bean.UrunMarkasi;
import com.mysql.cj.jdbc.MysqlDataSource;
import db.restriction.DBRestriction;
import db.restriction.DortIslem;
import db.restriction.table_column.GubreTableColumn;
import db.restriction.table_column.GubrelemeTableColumn;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ISIK
 */
public class MySQLDBConnection extends DBConnection {

    private final static String URL = "jdbc:mysql://localhost:3306/sera_kayit?useUnicode=true&characterEncoding=utf8&useLegacyDatetimeCode=false&serverTimezone=Turkey";
    private final static String USER_NAME = "root";
    private final static String PASSWORD = "19791981";
    private final MysqlDataSource dataSource;

    public MySQLDBConnection() {
        dataSource = new MysqlDataSource();
        dataSource.setURL(URL);
        dataSource.setUser(USER_NAME);
        dataSource.setPassword(PASSWORD);

    }

    @Override
    public List<Sera> selectSera(DBRestriction res) {
        List<Sera> list = new ArrayList<>();
        System.out.println("Try to get data Database(sera) ");

        String query = "SELECT `sera`.`idsera`, "
                + "    `sera`.`sera_adi` "
                + "FROM `sera_kayit`.`sera`";

        String where = res != null ? res.getWhereClause() : "";
        String order = res != null ? res.getOrderByClause() : "";
        String lmt = res != null ? res.getLimitClause() : "";

        query = query + " " + where + " " + order + " " + lmt;
        System.out.println(query);
        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(query);
            Sera s;
            while (set.next()) {
                s = new Sera(set.getInt(1), set.getString(2));
                list.add(s);
            }
            System.out.println("Data from  Database(sera) size: " + list.size());
        } catch (SQLException ex) {
            System.err.println("Failed to retreive datas from `sera_kayit`.`sera` = " + query);
            Logger.getLogger(MySQLDBConnection.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        } finally {
            return list;
        }
    }

    @Override
    public List<Sera> insertIntoSera(List<Sera> sera) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Sera> updateSera(List<Sera> sera) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Sera> deleteFromSera(List<Sera> sera) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Gubre> selectGubre(DBRestriction res) {
        List<Gubre> list = new ArrayList<>();
        System.out.println("Try to get data Database(gubre) ");

        String query = "SELECT "
                + "`gubre`.`idgubre`,"
                + "`gubre`.`gubre_adi`,"
                + "`gubre`.`idgubre_cesidi`,"
                + "`gubre`.`idurun_markasi`,"
                + "`gubre_cesidi`.`cesit_adi`,"
                + "`urun_markasi`.`marka_adi`"
                + "FROM `sera_kayit`.`gubre` "
                + "INNER JOIN `sera_kayit`.`gubre_cesidi` "
                + "ON `gubre`.`idgubre_cesidi`= `gubre_cesidi`.`idgubre_cesidi` "
                + "INNER JOIN `sera_kayit`.`urun_markasi` "
                + "ON `gubre`.`idurun_markasi`=`urun_markasi`.`idurun_markasi`";

        String where = res != null ? res.getWhereClause() : "";
        String order = res != null ? res.getOrderByClause() : "";
        String lmt = res != null ? res.getLimitClause() : "";

        query = query + " " + where + " " + order + " " + lmt;
        System.out.println(query);
        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(query);
            Gubre g;
            while (set.next()) {
                g = new Gubre();
                g.setId(set.getInt(1));
                g.setAdi(set.getString(2));
                g.setGubreCesidi(new GubreCesidi(set.getInt(3), set.getString(5)));
                g.setGubreMarkasi(new UrunMarkasi(set.getInt(4), set.getString(6)));
                list.add(g);
            }
            System.out.println("Data from  Database(gubre) size: " + list.size());
        } catch (SQLException ex) {
            System.err.println("Failed to retreive datas from `sera_kayit`.`gubre` = " + query);
            Logger.getLogger(MySQLDBConnection.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        } finally {
            return list;
        }
    }

    @Override
    public List<Gubre> insertIntoGubre(List<Gubre> list) {
        List<Gubre> before = selectGubre(null);
        System.out.println("Try to insert Database(gubre) size : " + list.size());
        String sql = "INSERT INTO `sera_kayit`.`gubre`"
                + "(`gubre_adi`,"
                + "`idgubre_cesidi`,"
                + "`idurun_markasi`)"
                + " VALUES(?,?,?)";
        int maximumBatch = 100;
        int index = 0;
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (Gubre g : list) {
                System.out.println(g);

                pstm.setString(1, g.getAdi());
                pstm.setInt(2, g.getGubreCesidi().getId());
                pstm.setInt(3, g.getGubreMarkasi().getId());
                pstm.addBatch();
                index++;
                if (index == maximumBatch) {
                    index = 0;
                    pstm.executeBatch();
                }
            }

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            System.out.println("Insert comleted to Database(`sera_kayit`.`gubre`):" + list.size());

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("Insert failed on Database(`sera_kayit`.`gubre_cesidi`");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            List<Gubre> all = selectGubre(null);
            all.removeAll(before);
            return all;
        }
    }

    @Override
    public List<Gubre> updateGubre(List<Gubre> gubreList) {

        System.out.println("Try to update Database(gubre) size : " + gubreList.size());
        String sql = " UPDATE `sera_kayit`.`gubre`"
                + "SET "
                + "`gubre_adi` = ?, "
                + "`idgubre_cesidi` = ?, "
                + "`idurun_markasi` = ? "
                + "WHERE `idgubre` =?";
        int maximumBatch = 100;
        int index = 0;
        Integer[] ids = new Integer[gubreList.size()];
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (Gubre g : gubreList) {
                System.out.println(g);
                ids[index++] = g.getId();
                pstm.setString(1, g.getAdi());
                pstm.setInt(2, g.getGubreCesidi().getId());
                pstm.setInt(3, g.getGubreMarkasi().getId());
                pstm.setInt(4, g.getId());
                pstm.addBatch();
                index++;
                if (index == maximumBatch) {
                    index = 0;
                    pstm.executeBatch();
                }
            }

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            System.out.println("update comleted froem Database(`sera_kayit`.`gubre`):" + gubreList.size());

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("update failed on Database(`sera_kayit`.`gubre`");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBRestriction res = new DBRestriction();
            res.setWhereClause(GubreTableColumn.idgubre, ids, DortIslem.IN);
            return selectGubre(res);
        }
    }

    @Override
    public List<Gubre> deleteFromGubre(List<Gubre> gubreList) {

        List<Gubre> before = selectGubre(null);
        System.out.println("Try to deleye Database(gubre) size : " + gubreList.size());
        String sql = " DELETE FROM "
                + "`sera_kayit`.`gubre` "
                + "WHERE (`idgubre` = ?)";
        int maximumBatch = 100;
        int index = 0;
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (Gubre g : gubreList) {
                System.out.println(g);

                pstm.setInt(1, g.getId());
                pstm.addBatch();
                index++;
                if (index == maximumBatch) {
                    index = 0;
                    pstm.executeBatch();
                }
            }

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            System.out.println("Delete comleted froem Database(`sera_kayit`.`gubre`):" + gubreList.size());

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("Insert failed on Database(`sera_kayit`.`gubre_cesidi`");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            List<Gubre> all = selectGubre(null);
            before.removeAll(all);
//            all.removeAll(before);
            return before;
        }
    }

    @Override
    public List<Gubreleme> selectGubreleme(DBRestriction res) {
        List<Gubreleme> list = new ArrayList<>();
        System.out.println("Try to get data Database(Gubreleme) ");

        String query = "SELECT `gubreleme`.`idgubreleme`, "
                + "    `gubreleme`.`sure`, "
                + "    `gubreleme`.`idsera`, "
                + "    `gubreleme`.`date`, "
                + "    `gubreleme`.`kilo`, "
                + "    `gubreleme`.`oran`, "
                + "    `sera`.`sera_adi` "
                + "FROM `sera_kayit`.`gubreleme` "
                + "INNER JOIN `sera_kayit`.`sera` "
                + "ON `gubreleme`.`idsera` = `sera`.`idsera`";

        String where = res != null ? res.getWhereClause() : "";
        String order = res != null ? res.getOrderByClause() : "";
        String lmt = res != null ? res.getLimitClause() : "";

        query = query + " " + where + " " + order + " " + lmt;
        System.out.println(query);
        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(query);
            Gubreleme g;
            while (set.next()) {
                g = new Gubreleme();
                g.setId(set.getInt(1));
                g.setSure(set.getInt(2));
                g.setSera(new Sera(set.getInt(3), set.getString(7)));
                g.setDate(new java.util.Date(set.getLong(4)));
                g.setKilo(set.getBigDecimal(5));
                g.setOran(set.getString(6));
                list.add(g);
            }
            System.out.println("Data from  Database(gubreleme) size: " + list.size());
        } catch (SQLException ex) {
            System.err.println("Failed to retreive datas from `sera_kayit`.`gubreleme` = " + query);
            Logger.getLogger(MySQLDBConnection.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        } finally {
            return list;
        }
    }

    @Override
    public List<Gubreleme> InsertIntoGubreleme(List<Gubreleme> glist) {
        List<Gubreleme> before = selectGubreleme(null);

        System.out.println("Try to insert Database(gubreleme) size : " + glist.size());
        String sql = "INSERT INTO "
                + "`sera_kayit`.`gubreleme`(`sure`,`idsera`,`date`,`kilo`) "
                + "VALUES(?,?,?,?)";
        int maximumBatch = 100;
        int index = 0;
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql); java.sql.Statement st = conn.createStatement();) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (Gubreleme g : glist) {
                pstm.setInt(1, g.getSure());
                pstm.setInt(2, g.getSera().getId());
                pstm.setLong(3, g.getDate().getTime());
                pstm.setBigDecimal(4, g.getKilo());
//                pstm.setString(5, g.getOran());
                pstm.addBatch();
                index++;
                if (index == maximumBatch) {
                    index = 0;
                    pstm.executeBatch();
                }
            }

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            System.out.println("Insert comleted to Database(`sera_kayit`.`gubreleme`):" + glist.size());
        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("Insert failed on Database(`sera_kayit`.`gubreleme`");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            List<Gubreleme> all = selectGubreleme(null);
            all.removeAll(before);
            return all;
        }
    }

    @Override
    public List<Gubreleme> updateGubreleme(List<Gubreleme> glist) {

        System.out.println("Try to update Database(gubreleme) size : " + glist.size());
        String sql = " UPDATE `sera_kayit`.`gubreleme` "
                + "SET "
                + "`sure` = ?, "
                + "`idsera` = ?, "
                + "`date` = ?, "
                + "`kilo` = ?, "
                + "`oran` = ? "
                + "WHERE `gubreleme`.`idgubreleme` = ?";
        int maximumBatch = 100;
        int batchIndex = 0;
//        Long t = 0l;

        Integer[] ids = new Integer[glist.size()];
        int idsIndex = 0;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (Gubreleme g : glist) {
                System.out.println(g);
                ids[idsIndex++] = g.getId();
                pstm.setInt(1, g.getSure());
                pstm.setInt(2, g.getSera().getId());
                pstm.setLong(3, g.getDate().getTime());
                pstm.setBigDecimal(4, g.getKilo());
                pstm.setString(5, g.getOran());
                pstm.setInt(6, g.getId());
                pstm.addBatch();
                batchIndex++;
                if (batchIndex == maximumBatch) {
                    batchIndex = 0;
                    pstm.executeBatch();
                }
            }

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            System.out.println("update comleted froem Database(`sera_kayit`.`gubreleme`):" + glist.size());

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("update failed on Database(`sera_kayit`.`gubreleme`");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBRestriction res = new DBRestriction();
            res.setWhereClause(GubrelemeTableColumn.idgubreleme, ids, DortIslem.IN);
            return selectGubreleme(res);
        }
    }

    @Override
    public List<Gubreleme> deleteFromGubreleme(List<Gubreleme> glist) {
        List<Gubreleme> before = selectGubreleme(null);
        System.out.println("Try to delete Database(gubreleme) size : " + glist.size());
        String sql = " DELETE FROM "
                + " `sera_kayit`.`gubreleme` "
                + "WHERE (`idgubreleme` = ?)";
        int maximumBatch = 100;
        int index = 0;
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (Gubreleme g : glist) {
                System.out.println(g);
                pstm.setInt(1, g.getId());
                pstm.addBatch();
                index++;
                if (index == maximumBatch) {
                    index = 0;
                    pstm.executeBatch();
                }
            }

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            System.out.println("Delete comleted froem Database(`sera_kayit`.`gubreleme`):" + glist.size());

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("Insert failed on Database(`sera_kayit`.`gubreleme`");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            List<Gubreleme> all = selectGubreleme(null);
            before.removeAll(all);
//            all.removeAll(before);
            return before;
        }
    }

    @Override
    public List<GubrelemeHasGubre> selectGubrelemeHasGubres(DBRestriction res) {
        List<GubrelemeHasGubre> list = new ArrayList<>();
        System.out.println("Try to get data Database(GubrelemeHasGure) ");

        String query = "SELECT "
                + "`gubreleme_has_gubre`.`idgubreleme_has_gubre`,"
                + "    `gubreleme_has_gubre`.`idgubreleme`,"
                + "    `gubreleme_has_gubre`.`idgubre`,"
                + "    `gubre`.`gubre_adi`,"
                + "    `gubre`.`idgubre_cesidi`,"
                + "    `gubre_cesidi`.`cesit_adi`,"
                + "    `gubre`.`idurun_markasi`,"
                + "    `urun_markasi`.`marka_adi`,"
                + "    `gubreleme_has_gubre`.`miktar` "
                + "FROM `sera_kayit`.`gubreleme_has_gubre` "
                + "	INNER JOIN `sera_kayit`.`gubre` ON  `gubre`.`idgubre` = `gubreleme_has_gubre`.`idgubre`"
                + "    INNER JOIN `sera_kayit`.`gubre_cesidi` ON `gubre_cesidi`.`idgubre_cesidi`= `gubre`.`idgubre_cesidi`"
                + "    INNER JOIN `sera_kayit`.`urun_markasi` ON `urun_markasi`.`idurun_markasi`=`gubre`.`idurun_markasi` ";

        String where = res != null ? res.getWhereClause() : "";
        String order = res != null ? res.getOrderByClause() : "";
        String lmt = res != null ? res.getLimitClause() : "";

        query = query + " " + where + " " + order + " " + lmt;
        System.out.println(query);
        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(query);
            GubrelemeHasGubre gg;
            while (set.next()) {
                gg = new GubrelemeHasGubre();
                gg.setId(set.getInt(1));
                gg.setGubrelemeId(set.getInt(2));
                GubreCesidi gc = new GubreCesidi(set.getInt(5), set.getString(6));
                UrunMarkasi um = new UrunMarkasi(set.getInt(7), set.getString(8));
                Gubre g = new Gubre(set.getInt(3), set.getString(4), gc, um);
                gg.setGubre(g);
                gg.setMiktar(set.getBigDecimal(9));
                list.add(gg);
            }
            System.out.println("Data from  Database(GubrelemeHasGure) size: " + list.size());
        } catch (SQLException ex) {
            System.err.println("Failed to retreive datas from `sera_kayit`.`GubrelemeHasGure` = " + query);
            Logger.getLogger(MySQLDBConnection.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        } finally {
            return list;
        }
    }

    @Override
    public List<GubrelemeHasGubre> ReplaceIntoGubrelemeHasGubres(List<GubrelemeHasGubre> glist) {
        List<GubrelemeHasGubre> before = selectGubrelemeHasGubres(null);
        System.out.println("Try to insert Database(gubre) size : " + before.size());
        String sql = "Replace INTO "
                + "`sera_kayit`.`gubreleme_has_gubre` "
                + "(`idgubreleme`, `idgubre`, `miktar`) "
                + "VALUES (?,?,?)";
        int maximumBatch = 100;
        int index = 0;
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (GubrelemeHasGubre g : glist) {
                System.out.println(g);

                pstm.setInt(1, g.getGubrelemeId());
                pstm.setInt(2, g.getGubre().getId());
                pstm.setBigDecimal(3, g.getMiktar());
                pstm.addBatch();
                index++;
                if (index == maximumBatch) {
                    index = 0;
                    pstm.executeBatch();
                }
            }

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            System.out.println("Insert comleted to Database(`sera_kayit`.`gubre`):" + glist.size());

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("Insert failed on Database(`sera_kayit`.`gubre_cesidi`");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            List<GubrelemeHasGubre> all = selectGubrelemeHasGubres(null);
            all.removeAll(before);
            return all;
        }
    }

    @Override
    public List<GubrelemeHasGubre> deleteFromGubrelemeHasGubres(List<GubrelemeHasGubre> glist) {
        List<GubrelemeHasGubre> before = selectGubrelemeHasGubres(null);
        System.out.println("Try to delete Database(GubrelemeHasGubre) size : " + glist.size());
        String sql = " DELETE FROM "
                + " `sera_kayit`.`gubreleme_has_gubre` "
                + "WHERE `gubreleme_has_gubre`.`idgubreleme_has_gubre` = ?";
        int maximumBatch = 100;
        int index = 0;
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (GubrelemeHasGubre g : glist) {
                System.out.println(g);
                pstm.setInt(1, g.getId());
                pstm.addBatch();
                index++;
                if (index == maximumBatch) {
                    index = 0;
                    pstm.executeBatch();
                }
            }

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            System.out.println("Delete comleted from Database(`sera_kayit`.`GubrelemeHasGubre`):" + glist.size());

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("Delete failed on Database(`sera_kayit`.`GubrelemeHasGubre`");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            List<GubrelemeHasGubre> all = selectGubrelemeHasGubres(null);
            before.removeAll(all);
//            all.removeAll(before);
            return before;
        }
    }

    @Override
    public List<UrunMarkasi> selectUrunMarkasi(DBRestriction res) {
        List<UrunMarkasi> list = new ArrayList<>();
        System.out.println("Try to get data Database(`sera_kayit`.`urun_markasi`) size : ");

        String query = "SELECT "
                + "`urun_markasi`.`idurun_markasi`,"
                + "`urun_markasi`.`marka_adi`"
                + "FROM "
                + "`sera_kayit`.`urun_markasi`;";

        String where = res != null ? res.getWhereClause() : "";
        String order = res != null ? res.getOrderByClause() : "";
        String lmt = res != null ? res.getLimitClause() : "";

        query = query + " " + where + " " + order + " " + lmt;

        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(query);
            UrunMarkasi um;
            while (set.next()) {
                um = new UrunMarkasi(set.getInt(1), set.getString(2));
                list.add(um);
            }

        } catch (SQLException ex) {
            System.err.println("Failed to retreive datas from `sera_kayit`.`urun_markasi` = " + query);
            Logger.getLogger(MySQLDBConnection.class
                    .getName()).log(Level.SEVERE, null, ex);

        } finally {
            return list;
        }
    }

    @Override
    public List<UrunMarkasi> InsertIntoUrunMarkasi(List<UrunMarkasi> markaList) {
        List<UrunMarkasi> before = selectUrunMarkasi(null);

        System.out.println("Try to insert Database(urun_markasi) size : " + markaList.size());
        String sql = "INSERT INTO `sera_kayit`.`urun_markasi`\n"
                + "(`marka_adi`)"
                + "VALUES(?)";
        int maximumBatch = 100;
        int index = 0;
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql); java.sql.Statement st = conn.createStatement();) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (UrunMarkasi um : markaList) {
                pstm.setString(1, um.getAdi());
                pstm.addBatch();
                index++;
                if (index == maximumBatch) {
                    index = 0;
                    pstm.executeBatch();
                }
            }

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            System.out.println("Insert comleted to Database(`sera_kayit`.`urun_markasi`):" + markaList.size());
        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("Insert failed on Database(`sera_kayit`.`urun_markasi`");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            List<UrunMarkasi> all = selectUrunMarkasi(null);
            all.removeAll(before);
            return all;
        }
    }

    @Override
    public List<UrunMarkasi> updateUrunMarkasi(List<UrunMarkasi> urunList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<UrunMarkasi> deleteFromUrunMarkasi(List<UrunMarkasi> urunList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<GubreCesidi> selectGubreCesidi(DBRestriction res) {
        List<GubreCesidi> list = new ArrayList<>();
        System.out.println("Try to get data Database(`sera_kayit`.`gubre_cesidi`) size : ");

        String query = "SELECT"
                + " `gubre_cesidi`.`idgubre_cesidi`,"
                + " `gubre_cesidi`.`cesit_adi`"
                + "FROM `sera_kayit`.`gubre_cesidi`";

        String where = res != null ? res.getWhereClause() : "";
        String order = res != null ? res.getOrderByClause() : "";
        String lmt = res != null ? res.getLimitClause() : "";

        query = query + " " + where + " " + order + " " + lmt;

        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(query);
            GubreCesidi gc;
            while (set.next()) {
                gc = new GubreCesidi(set.getInt(1), set.getString(2));
                list.add(gc);
            }

        } catch (SQLException ex) {
            System.err.println("Failed to retreive datas from `sera_kayit`.`gubre_cesidi` = " + query);
            Logger.getLogger(MySQLDBConnection.class
                    .getName()).log(Level.SEVERE, null, ex);

        } finally {
            return list;
        }
    }

    @Override
    public List<GubreCesidi> InsertIntoGubreCesidi(List<GubreCesidi> list) {
        List<GubreCesidi> before = selectGubreCesidi(null);
        System.out.println("Try to insert Database(gubre cesidi) size : " + list.size());
        String sql = "INSERT INTO `sera_kayit`.`gubre_cesidi`\n"
                + "(`cesit_adi`)"
                + "VALUES(?)";
        int maximumBatch = 100;
        int index = 0;
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql); java.sql.Statement st = conn.createStatement();) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (GubreCesidi gc : list) {
                pstm.setString(1, gc.getAdi());
                pstm.addBatch();
                index++;
                if (index == maximumBatch) {
                    index = 0;
                    pstm.executeBatch();
                }
            }

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            System.out.println("Insert comleted to Database(`sera_kayit`.`gubre_cesidi`):" + list.size());

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("Insert failed on Database(`sera_kayit`.`gubre_cesidi`");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            List<GubreCesidi> all = selectGubreCesidi(null);
            all.removeAll(before);
            return all;
        }
    }

    @Override
    public List<GubreCesidi> updateGubreCesidi(List<GubreCesidi> list) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<GubreCesidi> deleteGubreCesidi(List<GubreCesidi> list) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<GubreIcerik> selectGubreIcerik(DBRestriction res) {
        List<GubreIcerik> list = new ArrayList<>();
        System.out.println("Try to get data Database(GubreIcerik) ");

        String query = "SELECT "
                + "`gubre_icerik`.`idgubre_icerik`,\n"
                + " `gubre_icerik`.`name`\n"
                + " FROM "
                + "`sera_kayit`.`gubre_icerik` ";

        String where = res != null ? res.getWhereClause() : "";
        String order = res != null ? res.getOrderByClause() : "";
        String lmt = res != null ? res.getLimitClause() : "";

        query = query + " " + where + " " + order + " " + lmt;
        System.out.println(query);
        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(query);
            GubreIcerik gi;
            while (set.next()) {
                gi = new GubreIcerik();
                gi.setId(set.getInt(1));
                gi.setAdi(set.getString(2));
                list.add(gi);
            }
            System.out.println("Data from  Database(GubreIcerik) size: " + list.size());
        } catch (SQLException ex) {
            System.err.println("Failed to retreive datas from `sera_kayit`.`GubrelemeIcerik` = " + query);
            Logger.getLogger(MySQLDBConnection.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        } finally {
            return list;
        }
    }

    @Override
    public List<GubreIcerik> insertIntoGubreIcerik(List<GubreIcerik> list) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<GubreIcerik> updateGubreIcerik(List<GubreIcerik> list) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<GubreIcerik> deleteGubreIcerik(List<GubreIcerik> list) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<GubreHasGubreIcerik> selectGubreHasGubreIcerik(DBRestriction res) {
        List<GubreHasGubreIcerik> list = new ArrayList<>();
        System.out.println("Try to get data Database(GubrelemeHasGubreIcerik) ");

        String query = "SELECT "
                + "`gubre_has_gubre_icerik`.`idgubre_has_gubre_icerik`,"
                + "    `gubre_has_gubre_icerik`.`idgubre`,"
                + "    `gubre_icerik`.`idgubre_icerik`,"
                + "    `gubre_icerik`.`name`,"
                + "    `gubre_has_gubre_icerik`.`oran`"
                + " FROM `sera_kayit`.`gubre_has_gubre_icerik` "
                + " INNER JOIN gubre_icerik ON `gubre_has_gubre_icerik`.`idgubre_icerik`= `gubre_icerik`.`idgubre_icerik`";

        String where = res != null ? res.getWhereClause() : "";
        String order = res != null ? res.getOrderByClause() : "";
        String lmt = res != null ? res.getLimitClause() : "";

        query = query + " " + where + " " + order + " " + lmt;
        System.out.println(query);
        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(query);
            GubreHasGubreIcerik g;
            while (set.next()) {
                g = new GubreHasGubreIcerik();
                g.setId(set.getInt(1));
                g.setGubreId(set.getInt(2));
                GubreIcerik gi = new GubreIcerik(set.getInt(3),set.getString(4));
                g.setGubreIcerik(gi);
                g.setOran(set.getBigDecimal(5));
                list.add(g);
            }
            System.out.println("Data from  Database(GubrelemeHasGubreIcerik) size: " + list.size());
        } catch (SQLException ex) {
            System.err.println("Failed to retreive datas from `sera_kayit`.`GubrelemeHasGubreIcerik` = " + query);
            Logger.getLogger(MySQLDBConnection.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        } finally {
            return list;
        }
    }

    @Override
    public List<GubreHasGubreIcerik> replaceIntoGubreHasGubreIcerik(List<GubreHasGubreIcerik> list) {
     List<GubreHasGubreIcerik> before = selectGubreHasGubreIcerik(null);
        System.out.println("Try to replace data on  Database(gubre_has_gubre_icerik) size : " + before.size());
        String sql = "Replace INTO "
                + "`sera_kayit`.`gubre_has_gubre_icerik` "
                + "(`idgubre`, `idgubre_icerik`,`oran`) "
                + "VALUES (?,?,?)";
        int maximumBatch = 100;
        int index = 0;
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (GubreHasGubreIcerik g : list) {
                System.out.println(g);

                pstm.setInt(1, g.getGubreId());
                pstm.setInt(2, g.getGubreIcerik().getId());
                pstm.setBigDecimal(3, g.getOran());
                pstm.addBatch();
                index++;
                if (index == maximumBatch) {
                    index = 0;
                    pstm.executeBatch();
                }
            }

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            System.out.println("Insert comleted to Database(`sera_kayit`.`gubre_has_gubre_icerik`):" + list.size());

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("Insert failed on Database(`sera_kayit`.`gubre_has_gubre_icerik`");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            List<GubreHasGubreIcerik> all = selectGubreHasGubreIcerik(null);
            all.removeAll(before);
            return all;
        }
    }

    @Override
    public List<GubreHasGubreIcerik> deleteFromGubreHasGubreIcerik(List<GubreHasGubreIcerik> list) {
    List<GubreHasGubreIcerik> before = selectGubreHasGubreIcerik(null);
        System.out.println("Try to delete Database(gubre_has_gubre_icerik) size : " + list.size());
        String sql = " DELETE FROM "
                + " `sera_kayit`.`gubre_has_gubre_icerik` "
                + "WHERE `gubre_has_gubre_icerik`.`idgubre_has_gubre_icerik` = ?";
        int maximumBatch = 100;
        int index = 0;
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql);) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (GubreHasGubreIcerik g : list) {
                System.out.println(g);
                pstm.setInt(1, g.getId());
                pstm.addBatch();
                index++;
                if (index == maximumBatch) {
                    index = 0;
                    pstm.executeBatch();
                }
            }

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            System.out.println("Delete comleted from Database(`sera_kayit`.`gubre_has_gubre_icerik`):" + list.size());

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("Delete failed on Database(`sera_kayit`.`gubre_has_gubre_icerik`");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            List<GubrelemeHasGubre> all = selectGubrelemeHasGubres(null);
            before.removeAll(all);
//            all.removeAll(before);
            return before;
        }
    }

}
