/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.restriction;

import db.restriction.table_column.TableColumn;

/**
 *
 * @author ISIK
 */
public class DBRestriction {

    public final static String ASCENDING = "ASC";
    public final static String DESCENDING = "DESC";
    private String whereClause;
    private String orderByClause;
    private String limitClause;

    public void addWhereClause(TableColumn col, Object value, DortIslem islem) {
        if (value == null || col == null) {
            return;
        }
        if (whereClause == null || whereClause.isBlank()) {
            whereClause = "where ";
        } else {
            whereClause = whereClause + " and ";
        }
        whereClause += "`"+col.getTableName()+"`.`" + col.getColumnName() + "` " + islem.getSign()+" ";
        if (islem == DortIslem.IN) {
            if (col.isNumeric()) {
                Number[] vals = (Number[]) value;
                whereClause += "(" + vals[0];
                for (int i = 1; i < vals.length; i++) {
                    whereClause += "," + vals[i];
                }
                whereClause += ")";
            } else {
                String[] vals = (String[]) value;
                whereClause += "('" + vals[0] + "'";
                for (int i = 1; i < vals.length; i++) {
                    whereClause += ",'" + vals[i] + "'";
                }
                whereClause += ")";
            }
        } else {
            whereClause += col.isNumeric() ? value.toString() : "'" + value.toString() + "'";
        }
    }

    public void addOrderByClause(TableColumn col, String order) {
        if (col == null) {
            return;
        }
        String o = order.equalsIgnoreCase(ASCENDING) || order.equalsIgnoreCase(DESCENDING) ? order : ASCENDING;
        if (orderByClause == null || order.isBlank()) {
            orderByClause = " order by ";
        } else {
            orderByClause += " ,";
        }
        orderByClause += col.getColumnName() + " " + o;
        System.err.println(order);
    }

    public String getWhereClause() {
        return whereClause == null || whereClause.isBlank() ? "" : whereClause;
    }

    public String getLimitClause() {
        return limitClause == null || limitClause.isBlank() ? "" : limitClause;
    }

    public String getOrderByClause() {
        return orderByClause == null || orderByClause.isBlank() ? "" : orderByClause;
    }

    public void setWhereClause(TableColumn col, Object value, DortIslem islem) {
        this.whereClause = null;
        this.addWhereClause(col, value, islem);
    }

    public void setOrderByClause(TableColumn col, String order) {
        this.orderByClause = null;
        this.addOrderByClause(col, order);
    }

    public void setLimitClause(int bas, int miktar) {
        if (bas < 0) {
            limitClause = null;
            return;
        }
        if (miktar <= 0) {
            miktar = 1000;
        }
        limitClause = "LIMIT " + bas + "," + miktar;
    }
}
