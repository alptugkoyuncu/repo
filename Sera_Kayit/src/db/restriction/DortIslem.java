/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.restriction;

/**
 *
 * @author ISIK
 */
public enum DortIslem {
   EQUAL("="),NOT_EQUAL("<>"),SMALLER("<"),SMALLERANDEGUAL("<="),BIGGER(">"),BIGGERANDEQUAL(">="),IN("IN");
   private String sign;
   private DortIslem(String val){
       this.sign = val;
   }
   public String getSign(){
       return this.sign;
   }
}
