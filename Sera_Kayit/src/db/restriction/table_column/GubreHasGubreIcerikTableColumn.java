/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.restriction.table_column;

import java.math.BigDecimal;

/**
 *
 * @author ISIK
 */
public enum GubreHasGubreIcerikTableColumn implements TableColumn {
    idgubre_has_gubre_icerik(Integer.class),idgubre(Integer.class),idgubre_icerik(Integer.class),oran(BigDecimal.class);
        private Class c;

    private GubreHasGubreIcerikTableColumn(Class c) {
        this.c = c;
    }

    @Override
    public String getColumnName() {
        return this.toString();
    }

    @Override
    public boolean isNumeric() {
        return true;
    }

    @Override
    public Class getColumnClass() {
        return c;
    }

    @Override
    public String getTableName() {
        return "gubre_has_gubre_icerik";
    }

}
