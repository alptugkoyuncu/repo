/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.restriction.table_column;

import java.math.BigDecimal;

/**
 *
 * @author ISIK
 */
public enum GubreIcerikTableColumn implements TableColumn {
    idgubre_icerik(Integer.class),name(Integer.class);
        private Class c;

    private GubreIcerikTableColumn(Class c) {
        this.c = c;
    }

    @Override
    public String getColumnName() {
        return this.toString();
    }

    @Override
    public boolean isNumeric() {
        return this==GubreIcerikTableColumn.idgubre_icerik;
    }

    @Override
    public Class getColumnClass() {
        return c;
    }

    @Override
    public String getTableName() {
        return "gubre_icerik";
    }
}