/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.restriction.table_column;

import db.restriction.table_column.TableColumn;

/**
 *
 * @author ISIK
 */
public enum GubreTableColumn implements TableColumn{
    idgubre(Integer.class),
    gubre_adi(String.class),
    idgubre_cesidi(Integer.class),
    idgubre_markasi(String.class);

    private Class c;
    private GubreTableColumn(Class c){
        this.c = c;
    }
    @Override
    public String getColumnName() {
        return this.toString();
    }

    @Override
    public boolean isNumeric() {
        return Number.class.isAssignableFrom(c);
    }

    @Override
    public Class getColumnClass() {
        return c;
    }

    @Override
    public String getTableName() {
        return "gubre";
    }
    
}
