/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.restriction.table_column;

/**
 *
 * @author ISIK
 */

import java.math.BigDecimal;

public enum GubrelemeHasGubreTableColumn implements TableColumn{
    idgubreleme(Integer.class),idgubre(Integer.class),miktar(BigDecimal.class);
    ;

    private Class c;
    private GubrelemeHasGubreTableColumn(Class c){
        this.c = c;
    }
    @Override
    public String getColumnName() {
        return this.toString();
    }

    @Override
    public boolean isNumeric() {
        return true;
    }

    @Override
    public Class getColumnClass() {
        return c;
    }

    @Override
    public String getTableName() {
        return "gubreleme_has_gubre";
    }
    
}
