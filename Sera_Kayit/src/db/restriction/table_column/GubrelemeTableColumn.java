/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.restriction.table_column;

/**
 *
 * @author ISIK
 */
public enum GubrelemeTableColumn implements TableColumn{
    idgubreleme(Integer.class),sure(Integer.class),idsera(Integer.class),date(Long.class),oran(String.class);
    Class c;

    private GubrelemeTableColumn(Class c) {
        this.c = c;
    }
    
    
    @Override
    public String getColumnName() {
        return this.toString();
    }

    @Override
    public boolean isNumeric() {
        return this!=oran;
    }

    @Override
    public Class getColumnClass() {
        return c;
    }

    @Override
    public String getTableName() {
        return "gubreleme";
    }
}
