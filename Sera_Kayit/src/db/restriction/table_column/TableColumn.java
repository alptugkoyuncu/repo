/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.restriction.table_column;

/**
 *
 * @author ISIK
 */
public interface TableColumn {
    public String getColumnName();
    public boolean isNumeric();
    public Class getColumnClass();
    public String getTableName();
}
