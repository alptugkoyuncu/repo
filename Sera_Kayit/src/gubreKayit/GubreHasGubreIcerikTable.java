/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gubreKayit;

import bean.Gubre;
import bean.GubreHasGubreIcerik;
import bean.GubreIcerik;
import db.DBConnection;
import db.MySQLDBConnection;
import db.restriction.DBRestriction;
import db.restriction.table_column.*;
import db.restriction.DortIslem;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import org.jdesktop.swingx.autocomplete.ComboBoxCellEditor;
import sera.DatabaseKayitTable;
import sera.DatabaseKayitTableCellRenderer;
import sera.DatabaseKayitTableModel;
import sera.IdNamePairComboBoxRenderer;
import sera.IdNamePairTableCellRenderer;
import sera.IdTableCellRenderer;

/**
 *
 * @author ISIK
 */
public class GubreHasGubreIcerikTable extends DatabaseKayitTable<DatabaseKayitTableModel> {

    private Integer gubreId;
    private List<GubreIcerik> allGubreIcerik;
    private GubreIcerikTableModel tableModel;
    public GubreHasGubreIcerikTable(Integer gubreId) {
        DBConnection conn = new MySQLDBConnection();
        DBRestriction res = new DBRestriction();
        res.setOrderByClause(GubreIcerikTableColumn.name, DBRestriction.ASCENDING);
        allGubreIcerik = conn.selectGubreIcerik(res);
        JComboBox<GubreIcerik> jcbGubreIcerik = new JComboBox<>(allGubreIcerik.toArray(new GubreIcerik[0]));
        jcbGubreIcerik.setRenderer(new IdNamePairComboBoxRenderer());
        super.setDefaultRenderer(Integer.class, new IdTableCellRenderer());
        super.setDefaultEditor(GubreIcerik.class, new ComboBoxCellEditor(jcbGubreIcerik));
        super.setDefaultRenderer(GubreIcerik.class, new IdNamePairTableCellRenderer());
        super.setDefaultRenderer(BigDecimal.class, new DatabaseKayitTableCellRenderer());

        this.setGubreId(gubreId);
    }
    

    public GubreHasGubreIcerikTable() {
        this(null);
    }

    public Integer getGubreId() {
        return gubreId;
    }

    public void setGubreId(Integer gubreId) {
        this.gubreId = gubreId;
        if (this.gubreId == null) {
            this.setModel(new AbstractTableModel() {
                @Override
                public int getRowCount() {
                    return 0;
                }

                @Override
                public int getColumnCount() {
                    return 3;
                }

                @Override
                public Object getValueAt(int rowIndex, int columnIndex) {
                    return null;
                }
            });
        } else {
            DBConnection conn = new MySQLDBConnection();
            DBRestriction res = new DBRestriction();
            res.setWhereClause(GubreHasGubreIcerikTableColumn.idgubre, this.gubreId, DortIslem.EQUAL);
            tableModel = new GubreIcerikTableModel(conn.selectGubreHasGubreIcerik(res));
            super.setModel(tableModel);
        }
    }

    @Override
    public void save() {
        List<GubreHasGubreIcerik> replace = new ArrayList<>();
        List<GubreHasGubreIcerik> delete = new ArrayList<>();
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            var pos = tableModel.getPosition(i);
            GubreHasGubreIcerik g = tableModel.getValue(i);
            g.setGubreId(gubreId);
            switch (pos) {
                case GUBRE_ADDED:
                    replace.add(g);
                    break;
                case GUBRE_CHANGED:
                    GubreHasGubreIcerik orj = tableModel.getOriginalValue(i);
                    if (!g.getGubreIcerik().equals(orj.getGubreIcerik())) {
                        delete.add(orj);
                    }
                    replace.add(g);
                    break;
                case GUBRE_DELETED:
                    delete.add(g);
                    break;
                case GUBRE_ORIGINAL:
                    break;
                default:
                    break;
            }
        }
        DBConnection conn = new MySQLDBConnection();
        if (!replace.isEmpty()) {
            conn.replaceIntoGubreHasGubreIcerik(replace);
        }
        if (!delete.isEmpty()) {
            conn.deleteFromGubreHasGubreIcerik(delete);
        }
        this.setGubreId(gubreId);
    }

    class GubreIcerikTableModel extends DatabaseKayitTableModel<GubreHasGubreIcerik> {

        private String cols[];

        public GubreIcerikTableModel(List<GubreHasGubreIcerik> originalValues) {
            super(originalValues);
            cols = new String[]{"id", "Cesit", "Oran"};
        }

        @Override
        public String getColumnName(int column) {
            return cols[column];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return Integer.class;
                case 1:
                    return GubreIcerik.class;
                case 2:
                    return BigDecimal.class;
                default:
                    return super.getColumnClass(columnIndex);
            }
        }

        @Override
        public int getColumnCount() {
            return cols.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            GubreHasGubreIcerik ghi = this.getValue(rowIndex);
            if (ghi == null) {
                return null;
            }
            switch (columnIndex) {
                case 0:
                    return ghi.getId();
                case 1:
                    return ghi.getGubreIcerik();
                case 2:
                    return ghi.getOran();
                default:
                    return null;
            }
        }

        @Override
        public Object getOriginalValueAt(int row, int column) {
            GubreHasGubreIcerik ghi = super.getOriginalValue(row);
            if (ghi == null) {
                return null;
            }
            switch (column) {
                case 0:
                    return ghi.getId();
                case 1:
                    return ghi.getGubreIcerik();
                case 2:
                    return ghi.getOran();
                default:
                    return null;
            }
        }

        @Override
        public void addNewRow() {
            GubreHasGubreIcerik g = new GubreHasGubreIcerik();
            this.values.add(g);
            this.positions.add(DBKayitPosition.GUBRE_ADDED);
            int index = this.values.size() - 1;
            this.fireTableRowsInserted(index, index);
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return this.getPosition(rowIndex) != DBKayitPosition.GUBRE_DELETED && columnIndex != 0;
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            if (aValue == null) {
                return;
            }
            GubreHasGubreIcerik val = this.getValue(rowIndex);
            switch (columnIndex) {
                case 0: {
                    val.setId((Integer) aValue);
                }
                case 1: {
                    val.setGubreIcerik((GubreIcerik) aValue);
                    break;
                }
                case 2:
                    val.setOran((BigDecimal) aValue);
                    break;
                default:
                    break;
            }
            this.fireTableCellUpdated(rowIndex, columnIndex);
        }

    }
}
