/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gubreKayit;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author ISIK
 */
public class GubreKayitPaneli extends JPanel{
    
    GubreKayitTable gTable;
    GubreHasGubreIcerikTable giTable;
    public GubreKayitPaneli() {
        gTable = new GubreKayitTable();
        giTable = new GubreHasGubreIcerikTable();
        super.setLayout(new BorderLayout());
        super.add(new JScrollPane(gTable),BorderLayout.CENTER);
        super.add(new JScrollPane(giTable),BorderLayout.EAST);
        
        gTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(e.getValueIsAdjusting())
                    return;
                int selectedRow = gTable.getSelectedRow();
                if(selectedRow<0)
                    giTable.setGubreId(null);
                else{
                    Integer id = (Integer) gTable.getValueAt(selectedRow, 0);
                    giTable.setGubreId(id);
                }
            }
        });
    }
    
}
