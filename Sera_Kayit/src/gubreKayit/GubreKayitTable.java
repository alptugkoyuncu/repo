/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gubreKayit;

import bean.Gubre;
import bean.GubreCesidi;
import bean.UrunMarkasi;
import db.MySQLDBConnection;
import db.restriction.DBRestriction;
import db.restriction.table_column.GubreTableColumn;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JList;
import sera.DatabaseKayitTable;
import sera.DatabaseKayitTableModel;
import sera.IdNamePairComboBoxRenderer;
import sera.IdNamePairTableCellRenderer;
import sera.IdTableCellRenderer;
import sera.DatabaseKayitTableCellRenderer;

/**
 *
 * @author ISIK
 */
public class GubreKayitTable extends DatabaseKayitTable {
    DBRestriction gubreRes;
    DatabaseKayitTableModel<Gubre> mdl;
    Gubre copy=null;
    public GubreKayitTable() {
        gubreRes = new DBRestriction();
        gubreRes.setOrderByClause(GubreTableColumn.idgubre_cesidi,DBRestriction.ASCENDING);
        gubreRes.addOrderByClause(GubreTableColumn.gubre_adi, DBRestriction.ASCENDING);
        db.DBConnection conn = new MySQLDBConnection();
        List<GubreCesidi> gubreCesidiList = conn.selectGubreCesidi(null);
        List<UrunMarkasi> urunMarkasiList = conn.selectUrunMarkasi(null);
        mdl = new GubreKayitTableModel(conn.selectGubre(gubreRes));
        super.setModel(mdl);
        JComboBox<GubreCesidi> gccb = new JComboBox<>(gubreCesidiList.toArray(new GubreCesidi[0]));
        JComboBox<UrunMarkasi> gmcb = new JComboBox<>(urunMarkasiList.toArray(new UrunMarkasi[0]));
        gccb.setRenderer(new IdNamePairComboBoxRenderer());
        gmcb.setRenderer(new IdNamePairComboBoxRenderer());
        super.setDefaultEditor(GubreCesidi.class, new DefaultCellEditor(gccb));
        super.setDefaultEditor(UrunMarkasi.class, new DefaultCellEditor(gmcb));
//        super.setDefaultEditor(JList.class, new JTableJList());
        super.setDefaultRenderer(GubreCesidi.class, new IdNamePairTableCellRenderer());
        super.setDefaultRenderer(UrunMarkasi.class, new IdNamePairTableCellRenderer());
        super.getColumnModel().getColumn(0).setCellRenderer(new IdTableCellRenderer());
        super.setDefaultRenderer(String.class, new DatabaseKayitTableCellRenderer());
    }


   

    @Override
    public void save() {
        List<Gubre> newRows = new ArrayList<>();
        List<Gubre> updatedRows = new ArrayList<>();
        List<Gubre> deletedRows = new ArrayList<>();
        
        for (int i = 0; i < mdl.getRowCount(); i++) {
            Gubre g = mdl.getValue(i);
            switch (mdl.getPosition(i)) {
                case GUBRE_ADDED: {
                    if (g.getAdi() != null && !g.getAdi().isBlank() && g.getGubreCesidi() != null && g.getGubreMarkasi() != null) {
                        newRows.add(g);
                    }
                }
                break;
                case GUBRE_CHANGED:
                    if (g.getAdi() != null && !g.getAdi().isBlank() && g.getGubreCesidi() != null && g.getGubreMarkasi() != null) {
                        updatedRows.add(g);
                    }
                    break;
                case GUBRE_DELETED: {
                    if (g.getId() != null && g.getId() > 0) {
                        deletedRows.add(g);
                    }
                }
                break;
                case GUBRE_ORIGINAL:
                    break;
                default:
                    break;
            }
        }
        db.DBConnection conn = new MySQLDBConnection();
        if (!newRows.isEmpty()) {
            conn.insertIntoGubre(newRows);
        }
        if (!updatedRows.isEmpty()) {
            conn.updateGubre(updatedRows);
        }
        if (!deletedRows.isEmpty()) {
            conn.deleteFromGubre(deletedRows);
        }
        mdl.setOriginalValues(conn.selectGubre(gubreRes));
    }


    class GubreKayitTableModel extends DatabaseKayitTableModel {

        private final String[] cols = {"id", "Gubre adi", "Gubre cesidi", "Gubre Markasi"};
     
        public GubreKayitTableModel(List<Gubre> originalValues) {
            super(originalValues);
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            Class c;
            switch (columnIndex) {
                case 0:
                    c = Integer.class;
                    break;
                case 1:
                    c = String.class;
                    break;
                case 2:
                    c = GubreCesidi.class;
                    break;
                case 3:
                    c = UrunMarkasi.class;
                    break;
                default:
                    c = super.getColumnClass(columnIndex);
                    break;
            }
            return c;
        }

        @Override
        public String getColumnName(int column) {
            return cols[column];
        }

        @Override
        public int getColumnCount() {
            return cols.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {

            Gubre g =  (Gubre) this.getValue(rowIndex);
            
            
            switch (columnIndex) {
                case 0:
                    return g.getId();
                case 1:
                    return g.getAdi();
                case 2:
                    return g.getGubreCesidi();
                case 3:
                    return g.getGubreMarkasi();

                default:
                    return null;
            }
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            Gubre g = (Gubre) this.getValue(rowIndex);
            return positions.get(rowIndex) != DBKayitPosition.GUBRE_DELETED && columnIndex > 0;
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            Gubre g = (Gubre) this.getValue(rowIndex);
            switch (columnIndex) {
                case 1:
                    g.setAdi((String) aValue);
                    break;
                case 2:
                    g.setGubreCesidi((GubreCesidi) aValue);
                    break;
                case 3:
                    g.setGubreMarkasi((UrunMarkasi) aValue);
                    break;
                default:
                    super.setValueAt(aValue, rowIndex, columnIndex);
            }
            this.fireTableCellUpdated(rowIndex, columnIndex);
        }

     

        @Override
        public Object getOriginalValueAt(int row, int column) {
            Gubre g = (Gubre) originalValues.get(row);
            switch (column) {
                case 0:
                    return g.getId();
                case 1:
                    return g.getAdi();
                case 2:
                    return g.getGubreCesidi();
                case 3:
                    return g.getGubreMarkasi();
                default:
                    return null;
            }
        }

        @Override
        public void addNewRow() {
            values.add(new Gubre());
            positions.add(DBKayitPosition.GUBRE_ADDED);
//            this.fireTableRowsInserted(gubreler.size() - 1, gubreler.size() - 1);

            fireTableRowsInserted(values.size() - 1, values.size() - 1);
        }

    }
}
