/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gubrelemeKayit;

import bean.Gubre;
import bean.GubreCesidi;
import bean.GubrelemeHasGubre;
import db.DBConnection;
import db.MySQLDBConnection;
import db.restriction.DBRestriction;
import db.restriction.DortIslem;
import db.restriction.table_column.GubreTableColumn;
import db.restriction.table_column.GubrelemeHasGubreTableColumn;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.text.Bidi;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.table.AbstractTableModel;
import org.jdesktop.swingx.autocomplete.ComboBoxCellEditor;
import sera.DatabaseKayitTable;
import sera.DatabaseKayitTableModel;
import sera.DatabaseKayitTableCellRenderer;
import sera.IdNamePairComboBoxRenderer;
import sera.IdNamePairTableCellRenderer;
import sera.IdTableCellRenderer;

/**
 *
 * @author ISIK
 */
public class GubrelemeHasGubreKayitTable extends DatabaseKayitTable<DatabaseKayitTableModel> {

    KayitTableModel mdl;
    List<Gubre> allGubres;
    JMenuItem jmOranla;

    Integer gubrelemeId;
    GubreCesidi gubrelemeCesidi;

    public GubrelemeHasGubreKayitTable(GubreCesidi gc) {
        this(null, gc);
    }

    public GubrelemeHasGubreKayitTable(Integer gubrelemeId, GubreCesidi gc) {
        DBConnection conn = new MySQLDBConnection();
        DBRestriction res = new DBRestriction();
        this.gubrelemeCesidi = gc;
        if (gc != null) {
            res.addWhereClause(GubreTableColumn.idgubre_cesidi, gc.getId(), DortIslem.EQUAL);
        }
        res.setOrderByClause(GubreTableColumn.gubre_adi, DBRestriction.ASCENDING);
        allGubres = conn.selectGubre(res);
        this.gubrelemeId = gubrelemeId;
        this.setGubrelemeId(gubrelemeId);
        JComboBox<Gubre> g = new JComboBox<>(allGubres.toArray(new Gubre[0]));
        g.setRenderer(new IdNamePairComboBoxRenderer());
        this.setDefaultRenderer(Integer.class, new IdTableCellRenderer());
        this.setDefaultEditor(Gubre.class, new ComboBoxCellEditor(g));
        this.setDefaultRenderer(Gubre.class, new IdNamePairTableCellRenderer());

//        this.getColumn(1).setCellRenderer(new DatabaseKayitTableCellRenderer());
        this.jmOranla = new JMenuItem("Oranla");
        this.jmOranla.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (getGubrelemeId() == null || getGubrelemeId() < 0) {
                    return;
                }
                if (mdl.getRowCount() == 0) {
                    return;
                }
                String str = JOptionPane.showInputDialog(null, "Oranı giriniz", "1.0");
                oranla(new BigDecimal(str));
            }
        });
        super.popupMenu.add(new JSeparator());
        super.popupMenu.add(this.jmOranla);
    }

    public void oranla(BigDecimal oran) {
        for (int i = 0; i < mdl.getRowCount(); i++) {
            BigDecimal val = (BigDecimal) mdl.getValueAt(i, 2);
            val = val.multiply(oran);
            mdl.setValueAt(val, i, 2);
        }
    }

    public GubreCesidi getGubrelemeCesidi() {
        return gubrelemeCesidi;
    }

    public Integer getGubrelemeId() {
        return gubrelemeId;
    }

    public void setGubrelemeId(Integer gubrelemeId) {
        this.gubrelemeId = gubrelemeId;
        if (this.gubrelemeId == null || this.gubrelemeId < 0) {
            super.setModel(new AbstractTableModel() {
                @Override
                public int getRowCount() {
                    return 0;
                }

                @Override
                public int getColumnCount() {
                    return 0;
                }

                @Override
                public Object getValueAt(int rowIndex, int columnIndex) {
                    return null;
                }
            });
        } else {
            DBConnection conn = new MySQLDBConnection();
            DBRestriction res = new DBRestriction();
            res.setWhereClause(GubrelemeHasGubreTableColumn.idgubreleme, gubrelemeId, DortIslem.EQUAL);
            if (gubrelemeCesidi != null) {
                res.addWhereClause(GubreTableColumn.idgubre_cesidi, gubrelemeCesidi.getId(), DortIslem.EQUAL);
            }
            List<GubrelemeHasGubre> l = conn.selectGubrelemeHasGubres(res);
            res.setOrderByClause(GubreTableColumn.gubre_adi, DBRestriction.ASCENDING);
            System.out.println(l);
            this.mdl = new KayitTableModel(l);
            super.setModel(this.mdl);
        }
    }

    @Override
    public void save() {
        List<GubrelemeHasGubre> replace = new ArrayList<>();
        List<GubrelemeHasGubre> delete = new ArrayList<>();
        for (int i = 0; i < mdl.getRowCount(); i++) {
            var pos = mdl.getPosition(i);
            GubrelemeHasGubre g = mdl.getValue(i);
            g.setGubrelemeId(gubrelemeId);
            switch (pos) {
                case GUBRE_ADDED:
                    replace.add(g);
                    break;
                case GUBRE_CHANGED:
                    GubrelemeHasGubre orj = mdl.getOriginalValue(i);
                    if (!g.getGubre().equals(orj.getGubre())) {
                        delete.add(orj);
                    }
                    replace.add(g);
                    break;
                case GUBRE_DELETED:
                    delete.add(g);
                    break;
                case GUBRE_ORIGINAL:
                    break;
                default:
                    break;
            }
        }
        DBConnection conn = new MySQLDBConnection();
        if (!replace.isEmpty()) {
            conn.ReplaceIntoGubrelemeHasGubres(replace);
        }
        if (!delete.isEmpty()) {
            conn.deleteFromGubrelemeHasGubres(delete);
        }
        this.setGubrelemeId(gubrelemeId);
    }

//    private Gubre getGubre(Integer gubreId) {
//        //To change body of generated methods, choose Tools | Templates.
//        for (Gubre g : allGubres) {
//            if (g.getId() == gubreId) {
//                return g;
//            }
//        }
//        return null;
//    }
    class KayitTableModel extends DatabaseKayitTableModel<GubrelemeHasGubre> {

        private final String cols[] = new String[]{"id", "Gubre", "Miktar"};

        public KayitTableModel(List<GubrelemeHasGubre> originalValues) {
            super(originalValues);
            System.err.println(originalValues);
        }

        @Override
        public String getColumnName(int column) {
            return cols[column];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return Integer.class;
                case 1:
                    return Gubre.class;
                case 2:
                    return BigDecimal.class;
                default:
                    return super.getColumnClass(columnIndex);
            }
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            if (aValue == null) {
                return;
            }
            GubrelemeHasGubre val = this.getValue(rowIndex);
            switch (columnIndex) {
                case 0: {
                    val.setId((Integer) aValue);
                }
                case 1: {
                    val.setGubre((Gubre) aValue);
                    break;
                }
                case 2:
                    val.setMiktar((BigDecimal) aValue);
                    break;
                default:
                    break;
            }
            this.fireTableCellUpdated(rowIndex, columnIndex);
        }
//
//        private DBKayitPosition getCurrentPosition(int row) {
//            DBKayitPosition pos = this.positions.get(row);
//            if (pos == DBKayitPosition.GUBRE_DELETED || pos == DBKayitPosition.GUBRE_ADDED) {
//                return pos;
//            }
//            GubrelemeHasGubre val = this.getValue(row);
//            GubrelemeHasGubre orj = this.getOriginalValue(row);
//            if (orj.equals(val) && orj.getMiktar().equals(val.getMiktar())) {
//                return DBKayitPosition.GUBRE_ORIGINAL;
//            } else {
//                return DBKayitPosition.GUBRE_CHANGED;
//            }
//        }

        @Override
        public Object getOriginalValueAt(int row, int column) {
            GubrelemeHasGubre g = this.getOriginalValue(row);
            switch (column) {
                case 0:
                    return g.getId();
                case 1:
                    return g.getGubre();
                case 2:
                    return g.getMiktar();
                default:
                    return null;
            }
        }

        @Override
        public void addNewRow() {
            GubrelemeHasGubre g = new GubrelemeHasGubre();
            this.values.add(g);
            this.positions.add(DBKayitPosition.GUBRE_ADDED);
            int index = this.values.size() - 1;
            this.fireTableRowsInserted(index, index);
        }

        @Override
        public int getColumnCount() {
            return cols.length;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return this.getPosition(rowIndex) != DBKayitPosition.GUBRE_DELETED && columnIndex != 0;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            GubrelemeHasGubre g = this.values.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    return g.getId();
                case 1:
                    return g.getGubre();
                case 2:
                    return g.getMiktar();
                default:
                    return null;
            }
        }

    }
}
