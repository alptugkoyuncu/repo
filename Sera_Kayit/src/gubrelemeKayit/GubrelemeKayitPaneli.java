/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gubrelemeKayit;

import bean.GubreCesidi;
import db.DBConnection;
import db.MySQLDBConnection;
import gubreKayit.GubreHasGubreIcerikTable;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author ISIK
 */
public class GubrelemeKayitPaneli extends JPanel{
    
    GubrelemeKayitTable gTable;
    GubrelemeHasGubreKayitTable[] ggTable;
    public GubrelemeKayitPaneli() {
        gTable = new GubrelemeKayitTable();
        JPanel pnl = createGGTablePanel();
        gTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int selectedRow=gTable.getSelectedRow();
                if(!e.getValueIsAdjusting() )
                    return;
                Integer gubrelemeId = selectedRow>=0 ? (Integer)gTable.getModel().getValueAt(selectedRow,0) : null;
                for(int i=0; i<ggTable.length; i++)
                    ggTable[i].setGubrelemeId(gubrelemeId);
            }
        });
        super.setLayout(new BorderLayout());
        super.add(new JScrollPane(gTable),BorderLayout.CENTER);
        super.add(pnl,BorderLayout.EAST);
    }
    private JPanel createGGTablePanel(){
        DBConnection conn = new MySQLDBConnection();
        List<GubreCesidi> allCesids = conn.selectGubreCesidi(null);
        JPanel pnl = new JPanel(new GridLayout(allCesids.size(), 1));
        ggTable = new GubrelemeHasGubreKayitTable[allCesids.size()];
        for(int i=0; i<allCesids.size();i++){
            ggTable[i] = new GubrelemeHasGubreKayitTable(allCesids.get(i));
            JScrollPane pane = new JScrollPane(ggTable[i]);
            pane.setBorder(BorderFactory.createTitledBorder(ggTable[i].getGubrelemeCesidi().getAdi()));
            pnl.add(pane);
        }
        return pnl;
    }
}
