/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gubrelemeKayit;

import bean.Gubre;
import bean.Gubreleme;
import bean.GubrelemeHasGubre;
import bean.Sera;
import db.DBConnection;
import db.MySQLDBConnection;
import db.restriction.DBRestriction;
import db.restriction.DortIslem;
import db.restriction.table_column.GubreTableColumn;
import db.restriction.table_column.GubrelemeHasGubreTableColumn;
import db.restriction.table_column.GubrelemeTableColumn;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.autocomplete.ComboBoxCellEditor;
import org.jdesktop.swingx.table.DatePickerCellEditor;
import sera.DatabaseKayitTable;
import sera.DatabaseKayitTableModel;
import sera.DatabaseKayitTableCellRenderer;
import sera.IdNamePairComboBoxRenderer;
import sera.IdNamePairTableCellRenderer;
import sera.IdTableCellRenderer;

/**
 *
 * @author ISIK
 */
public class GubrelemeKayitTable extends DatabaseKayitTable {

    GubrelemeTableModel mdl;
    private DBRestriction gubrelemeRestriction;
    JMenuItem menuItemClone;
    private int tableDateOrder;
    public final static int TABLE_DATE_ASCENDING_ORDER=1;
    public final static int TABLE_DATE_DESCENDING_ORDER=2;
    
    public GubrelemeKayitTable() {
        this.menuItemClone = new JMenuItem("Kopya olustur");
        super.popupMenu.add(menuItemClone);
        gubrelemeRestriction = new DBRestriction();
        this.setTableDateOrder(TABLE_DATE_ASCENDING_ORDER);
        DBConnection conn = new MySQLDBConnection();
        List<Gubreleme> gl = conn.selectGubreleme(gubrelemeRestriction);
        mdl = new GubrelemeTableModel(gl);
        super.setModel(mdl);
        List<Sera> slist = conn.selectSera(null);
        JComboBox<Sera> combo = new JComboBox<>(slist.toArray(new Sera[0]));
        combo.setRenderer(new IdNamePairComboBoxRenderer());
        this.menuItemClone.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = getSelectedRow();
                if(selectedRow<0)
                    return;
                Gubreleme g = mdl.getValue(selectedRow);
                JXDatePicker pck = new JXDatePicker(new Date());// pck.getDate() fonk saat kismini sifirladigi icin JXDatePicker kullandim
                g.setDate(pck.getDate());
                if(g==null)
                    return;
                DBRestriction r = new DBRestriction();
                r.setWhereClause(GubrelemeHasGubreTableColumn.idgubreleme, g.getId(), DortIslem.EQUAL);
                List<GubrelemeHasGubre> ggl = conn.selectGubrelemeHasGubres(r);
                
                DBConnection conn = new MySQLDBConnection();
                List<Gubreleme> gl = new ArrayList<>();
                gl.add(g);
                gl = conn.InsertIntoGubreleme(gl);
                if(gl.isEmpty()){
                    return;
                }
                g = gl.get(0);
                for(GubrelemeHasGubre gg:ggl)
                    gg.setGubrelemeId(g.getId());
                conn.ReplaceIntoGubrelemeHasGubres(ggl);
                mdl.setOriginalValues(conn.selectGubreleme(getGubrelemeRestriction()));
            }
        });
        this.getColumnModel().getColumn(0).setCellRenderer(new IdTableCellRenderer());
        this.setDefaultRenderer(String.class, new DatabaseKayitTableCellRenderer());
        this.setDefaultRenderer(Integer.class, new DatabaseKayitTableCellRenderer());
        this.setDefaultRenderer(Sera.class, new IdNamePairTableCellRenderer());
        this.setDefaultEditor(Sera.class, new ComboBoxCellEditor(combo));
        this.setDefaultEditor(Date.class, new DatePickerCellEditor());
        this.setDefaultRenderer(Date.class, new DatabaseKayitTableCellRenderer(){

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); //To change body of generated methods, choose Tools | Templates.
                if(value==null)
                    return this;
                Date dt = (Date) value;
                SimpleDateFormat format = new SimpleDateFormat("dd MMM YYYY");
                this.setText(format.format(dt));
                return this;
            }

        });
    }
    public void refreshTable(){
        mdl.setOriginalValues( new MySQLDBConnection().selectGubreleme(gubrelemeRestriction));
    }
    
    @Override
    public void save() {
        List<Gubreleme> newRows = new ArrayList<>();
        List<Gubreleme> updatedRows = new ArrayList<>();
        List<Gubreleme> deletedRows = new ArrayList<>();

        for (int i = 0; i < mdl.getRowCount(); i++) {
            Gubreleme g =   mdl.getValue(i);
            switch (mdl.getPosition(i)) {
                case GUBRE_ADDED: 
                    newRows.add(g);
                
                break;
                case GUBRE_CHANGED:
                    updatedRows.add(g);
                    break;
                case GUBRE_DELETED: {
                    deletedRows.add(g);
                }
                break;
                case GUBRE_ORIGINAL:
                    break;
                default:
                    break;
            }
        }
        db.DBConnection conn = new MySQLDBConnection();
        if (!newRows.isEmpty()) {
            conn.InsertIntoGubreleme(newRows);
        }
        if (!updatedRows.isEmpty()) {
            conn.updateGubreleme(updatedRows);
        }
        if (!deletedRows.isEmpty()) {
            conn.deleteFromGubreleme(deletedRows);
        }
        refreshTable();
        
    }

    public int getTableDateOrder() {
        return tableDateOrder;
    }

    public void setTableDateOrder(int tableDateOrder) {
        if(tableDateOrder!=TABLE_DATE_ASCENDING_ORDER && tableDateOrder!=TABLE_DATE_DESCENDING_ORDER)
            return;
        this.tableDateOrder = tableDateOrder;
        this.gubrelemeRestriction.setOrderByClause(GubrelemeTableColumn.date, (this.tableDateOrder==TABLE_DATE_ASCENDING_ORDER ? DBRestriction.ASCENDING : DBRestriction.DESCENDING));
    }

    public DBRestriction getGubrelemeRestriction() {
        return gubrelemeRestriction;
    }

    public void setGubrelemeRestriction(DBRestriction gubrelemeRestriction) {
        this.gubrelemeRestriction = gubrelemeRestriction;
    }

    class GubrelemeTableModel extends DatabaseKayitTableModel<Gubreleme> {

        private final String[] cols = new String[]{"ID", "Sera", "Sure", "Tarih","kilo", "Oran"};

        public GubrelemeTableModel(List<Gubreleme> orj) {
            super(orj);
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            DBKayitPosition pos =  positions.get(rowIndex);
            if (pos == DBKayitPosition.GUBRE_DELETED) {
                return false;
            }
            return !(columnIndex == 0 || columnIndex == 5);
        }

        @Override
        public String getColumnName(int column) {
            return cols[column];
        }

        @Override
        public int getColumnCount() {
            return cols.length;
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return Integer.class;
                case 1:
                    return Sera.class;
                case 2:
                    return Integer.class;
                case 3:
                    return Date.class;
                case 4:
                    return BigDecimal.class;
                case 5:
                    return String.class;
                default:
                    return super.getColumnClass(columnIndex);
            }
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Gubreleme g =  values.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    return g.getId();
                case 1:
                    return g.getSera();
                case 2:
                    return g.getSure();
                case 3:
                    return g.getDate();
                case 4:
                    return g.getKilo();
                case 5:
                    return g.getOran();
                default:
                    return null;
            }
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            Gubreleme g =  values.get(rowIndex);
            switch (columnIndex) {
                case 1:
                    g.setSera((Sera) aValue);
                    break;
                case 2: {
                    try {
//                        Integer val = Integer.parseInt((String) aValue);
                        g.setSure((Integer) aValue);
                    } catch (NumberFormatException ex) {
//                        this.setValueAt(g.getSera(), rowIndex, columnIndex);
                    }
                    break;
                }
                case 3: {
                    g.setDate((Date) aValue);
                    break;
                }
                case 4:{
                    g.setKilo((BigDecimal)aValue);
                    break;
                }
                case 5: {
                    g.setOran((String) aValue);
                    break;
                }
                default:
                    break;
            }
            this.fireTableCellUpdated(rowIndex, columnIndex);
        }

        @Override
        public Object getOriginalValueAt(int row, int column) {
            Gubreleme g =  originalValues.get(row);
            switch (column) {
                case 0:
                    return g.getId();
                case 1:
                    return g.getSera();
                case 2:
                    return g.getSure();
                case 3:
                    return g.getDate();
                case 4:
                    return g.getKilo();
                case 5:
                    return g.getOran();
                default:
                    return null;
            }
        }

        @Override
        public void addNewRow() {
            int pos = tableDateOrder == TABLE_DATE_ASCENDING_ORDER ? values.size() : 0;
            values.add(pos,new Gubreleme());
            positions.add(pos,DBKayitPosition.GUBRE_ADDED);
            fireTableRowsInserted(pos, pos);
            
        }
    }
}
