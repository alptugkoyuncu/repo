/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/**
 *
 * @author ISIK
 * @param <T>
 */
public abstract class DatabaseKayitTable<T extends DatabaseKayitTableModel> extends JTable {
    protected JPopupMenu popupMenu;
    protected Object copyValue=null;
    protected JMenuItem menuItemAdd,menuItemRemove,menuItemCancel,menuItemRecord;
    public DatabaseKayitTable() {
        popupMenu = new JPopupMenu();
         menuItemAdd = new JMenuItem("Yeni Ekle");
         menuItemRemove = new JMenuItem("Seçili satiri Sil");
         menuItemCancel = new JMenuItem("Seçili Güncellemeyi İptal Et");
         menuItemRecord = new JMenuItem("Kaydet");
//        JMenuItem menuItemAllCancel = new JMenuItem("Tüm Güncellemeri İptal et");
        super.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        popupMenu.add(menuItemAdd);
        popupMenu.add(menuItemRemove);
        popupMenu.add(menuItemCancel);
        popupMenu.add(menuItemRecord);
//        popupMenu.add(menuItemAllCancel);
        super.setComponentPopupMenu(popupMenu);
        this.setDefaultRenderer(BigDecimal.class, new DatabaseKayitTableCellRenderer());
        super.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                
                Point point = e.getPoint();
                int currentRow = rowAtPoint(point);
                
                DatabaseKayitTable.this.getSelectionModel().setValueIsAdjusting(true);
                setRowSelectionInterval(currentRow, currentRow);
                
                DatabaseKayitTable.this.getSelectionModel().setValueIsAdjusting(false);
            }

           

        });
        
        menuItemAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DatabaseKayitTableModel mdl = (DatabaseKayitTableModel) getModel();
                mdl.addNewRow();
            }
        });
        menuItemCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DatabaseKayitTableModel mdl = (DatabaseKayitTableModel) getModel();
                mdl.cancelUpdate(DatabaseKayitTable.this.getSelectedRow());
            }
        });
        menuItemRemove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DatabaseKayitTableModel mdl = (DatabaseKayitTableModel) getModel();
                mdl.removeRow(DatabaseKayitTable.this.getSelectedRow());
            }
        });
        menuItemRecord.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save();
            }
        });
    }

    
    public abstract void save();
}
