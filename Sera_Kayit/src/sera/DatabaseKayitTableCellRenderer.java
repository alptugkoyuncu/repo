/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera;

import gubreKayit.GubreKayitTable;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author ISIK
 */
public class DatabaseKayitTableCellRenderer extends DefaultTableCellRenderer{

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); //To change body of generated methods, choose Tools | Templates.
        DatabaseKayitTableModel mdl = (DatabaseKayitTableModel) table.getModel();
//        String val = (String) value;
        setFont(getFont().deriveFont(Font.PLAIN));
        switch (mdl.getPosition(row)) {
            case GUBRE_ADDED: {
                if (value == null) {
                    setText("");
                    return this;
                } else {
                    setForeground(Color.RED);
                }
                break;
            }
            case GUBRE_CHANGED: {
                Object orj =  mdl.getOriginalValueAt(row, column);
                
                if ((orj!=null )&& orj.equals(value)) {
                    setForeground(Color.BLACK);
                } else {
                    setForeground(Color.RED);
                }
                break;
            }
            case GUBRE_ORIGINAL: {
                setForeground(Color.BLACK);
                break;
            }
            case GUBRE_DELETED: {
                setFont(getFont().deriveFont(Font.ITALIC));
                setForeground(Color.RED);
                break;
            }
            default:
                break;
        }
        setText(value!=null ? value.toString():"");
        return this;
    }
}
