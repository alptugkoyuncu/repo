/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera;

import bean.Gubre;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ISIK
 * @param <T>
 */
public abstract class DatabaseKayitTableModel<T extends NewInstancable & HasId> extends AbstractTableModel {

    protected List<T> values;
    protected List<T> originalValues;
    protected List<DBKayitPosition> positions;

    public enum DBKayitPosition {
        GUBRE_ADDED, GUBRE_DELETED, GUBRE_CHANGED, GUBRE_ORIGINAL;
    }

    public DatabaseKayitTableModel(List<T> originalValues) {
        values = new ArrayList<>();
        positions = new ArrayList<>();
        setOriginalValues(originalValues);

    }

    public List<T> getOriginalValues() {
        return originalValues;
    }

    public List<T> getValues() {
        return values;
    }
    public void setOriginalValues(List<T> originalValues) {
        this.originalValues = originalValues;
        this.positions.clear();
        this.values.clear();
        for (int i = 0; i < originalValues.size(); i++) {
            positions.add(i, DBKayitPosition.GUBRE_ORIGINAL);
            this.values.add((T) this.originalValues.get(i).getClone());
        }
        if (originalValues.isEmpty()) {
            addNewRow();
        }
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return values.size();
    }

    public DBKayitPosition getPosition(int row) {
        return positions.get(row);
    }

    @Override
    public void fireTableCellUpdated(int row, int column) {
        this.positions.set(row, checkPosition(row));
        super.fireTableCellUpdated(row, column); //To change body of generated methods, choose Tools | Templates.
    }

    protected DBKayitPosition checkPosition(int index) {
        if (positions.get(index) == DBKayitPosition.GUBRE_ADDED || positions.get(index) == DBKayitPosition.GUBRE_DELETED) {
            return (DBKayitPosition) positions.get(index);
        }
        for (int i = 0; i < this.getColumnCount(); i++) {
            if (!this.getValueAt(index, i).equals(this.getOriginalValueAt(index, i))) {
                return DBKayitPosition.GUBRE_CHANGED;
            }

        }
        return DBKayitPosition.GUBRE_ORIGINAL;
    }

    public void removeRow(int selectedRow) {
        if (positions.get(selectedRow) == DBKayitPosition.GUBRE_ADDED) {
            values.remove(selectedRow);
            positions.remove(selectedRow);
            fireTableRowsDeleted(selectedRow, selectedRow);
            if (values.isEmpty()) {
                addNewRow();
            }
        } else {
            positions.set(selectedRow, DBKayitPosition.GUBRE_DELETED);
            fireTableRowsUpdated(selectedRow, selectedRow);
        }
    }

    public void cancelUpdate(int selectedRow) {
        switch (getPosition(selectedRow)) {
            case GUBRE_ADDED:
                removeRow(selectedRow);
                break;
            case GUBRE_CHANGED:
            case GUBRE_DELETED: {
                T g = originalValues.get(selectedRow);
                positions.set(selectedRow, DBKayitPosition.GUBRE_ORIGINAL);
                values.set(selectedRow, (T) g.getClone());
                fireTableRowsUpdated(selectedRow, selectedRow);
                break;
            }
            case GUBRE_ORIGINAL:
            default:
                break;
        }
    }

    public T getValue(int row) {
        return this.values.get(row);
    }

    public T getOriginalValue(int row) {
        return this.originalValues.get(row);
    }
    public void addNewRow(T value){
        int index = this.getRowCount();
        this.values.add(value);
        this.positions.add(DBKayitPosition.GUBRE_ADDED);
        this.fireTableRowsInserted(index, index);
    }
    public abstract Object getOriginalValueAt(int row, int column);

    public abstract void addNewRow();
}
