/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera;

import bean.GubreCesidi;
import java.awt.Component;
import javax.swing.JList;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import sera.IdNamePair;

/**
 *
 * @author ISIK
 */
public class IdNamePairComboBoxRenderer extends BasicComboBoxRenderer {

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected,
                cellHasFocus);

        IdNamePair item = (IdNamePair) value;
        String txt = item!=null ?item.getName() : "";
        setText(txt);
        return this;
    }
}
