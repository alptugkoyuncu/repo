/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sera;

import bean.GubreCesidi;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author ISIK
 */
public class IdNamePairTableCellRenderer extends DefaultTableCellRenderer {
     @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        DatabaseKayitTableModel mdl = (DatabaseKayitTableModel)table.getModel();
        IdNamePair pair = (IdNamePair) value;
         setFont(getFont().deriveFont(Font.PLAIN));
        switch (mdl.getPosition(row)) {
            case GUBRE_ADDED: {
                if (value == null) {
                    setText("");
                    return this;
                } else {
                    pair = (IdNamePair) value;
                    setForeground(Color.RED);
                    break;
                }
            }
            case GUBRE_CHANGED: {
                IdNamePair orj = (IdNamePair) mdl.getOriginalValueAt(row, column);
                setForeground(orj.getName().equalsIgnoreCase(pair.getName()) ? Color.black : Color.RED);
                break;
            }
            case GUBRE_DELETED: {
                setForeground(Color.red);
                setFont(getFont().deriveFont(Font.ITALIC));
                break;
            }
            case GUBRE_ORIGINAL: {
                setForeground(Color.black);
                break;
            }
            default:
                return this;
        }
        setText(pair.getName());
        return this;
    }
}
