
import bean.Gubre;
import bean.GubreCesidi;
import bean.UrunMarkasi;
import db.DBConnection;
import db.MySQLDBConnection;
import db.restriction.DBRestriction;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ISIK
 */
public class TestDb {

    public static db.restriction.DBRestriction res = new DBRestriction();
    public static db.DBConnection conn = new db.MySQLDBConnection();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//        System.out.println("Gubre dosyalari cekiliyor");
//        getGubre();
//    insertUrunMarkasi();
//    insertGubreCesidi();
//insertGubre();
//getSera();
getGubreIcerik();
    }

    public static void getGubreIcerik(){
        System.out.println("Gubre Icerik :");
        System.out.println(conn.selectGubreIcerik(null));
    }
    public static void getSera() {
        System.out.println("Sera list : ");
        System.out.println(conn.selectSera(res));
    }

    public static void getGubre() {
        System.out.println(conn.selectGubre(null));

    }

    public static void insertGubre() {
        DBConnection conn = new MySQLDBConnection();
        List<GubreCesidi> gc = conn.selectGubreCesidi(null);
        List<UrunMarkasi> um = conn.selectUrunMarkasi(null);
        Gubre g = new Gubre(-1, "asd", gc.get(0), um.get(0));
        List<Gubre> gl = new ArrayList<>();
        gl.add(g);
        conn.insertIntoGubre(gl);
    }

    public static void insertGubreCesidi() {
        DBConnection conn = new MySQLDBConnection();
        List<GubreCesidi> l = new ArrayList<>();
        l.add(new GubreCesidi(null, "Izelemnt"));
        l.add(new GubreCesidi(null, "Bitki Besleme"));
        l.add(new GubreCesidi(null, "Ham Gubre"));
        l.add(new GubreCesidi(null, "NPK"));
        conn.InsertIntoGubreCesidi(l);
    }

    public static void insertUrunMarkasi() {
        DBConnection conn = new MySQLDBConnection();
        List<UrunMarkasi> l = new ArrayList<>();
        l.add(new UrunMarkasi(0, "Haifa"));
        l.add(new UrunMarkasi(0, "Doktor Tarsa"));
        System.out.println(l);
        System.out.println(conn.InsertIntoUrunMarkasi(l));
    }
}
