
import bean.GubreCesidi;
import java.util.HashSet;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ISIK
 */
public class TestNullValueSet {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Set<GubreCesidi> set = new HashSet<>();
        set.add(new GubreCesidi(0, null));
        set.add(new GubreCesidi(1, null));
        System.err.println("size "+set.size());
        set.add(new GubreCesidi(3, "s"));
        set.add(new GubreCesidi(0, "s"));
        System.err.println("size "+set.size());
    }
    
}
