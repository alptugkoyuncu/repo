/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testmysqlserver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alptug
 */
public class TestMySqlServer {

    private Connection conn = null; //Bağlantı nesnemiz
    private String url = "jdbc:mysql://localhost:3306/";//veritabanının adresi ve portu
    private String dbName = "sera"; //veritabanının ismi
    private String properties = "?useUnicode=true&characterEncoding=utf8"; //Türkçe karakter problemi yaşamamak için
//    private String driver = "com.mysql.jdbc.Driver";//MySQL-Java bağlantısını sağlayan JDBC sürücüsü
    private String userName = "root"; //veritabanı için kullanıcı adı
    private String password = "19791981"; //kullanıcı şifresi
    private ResultSet res; // sorgulardan dönecek kayıtlar (sonuç kümesi) bu nesne içerisinde tutulacak

    /**
     * @param args the command line arguments
     */
    public Connection baglantiAc() throws Exception {
        Properties prop = new Properties();
//        prop.s
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conn = DriverManager.getConnection(url + dbName + properties, userName, password);//bağlantı açılıyor
        
        return conn;
        //return conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

    }

    public void baglantiKapat() throws Exception {
        conn.close();
    }

    public void read() {
        try {
            String[] tableName = {"client", "client_action_value", "client_permissions"};
            //        st = baglantiAc(); //veritabanına bağlanılıyor
            res = baglantiAc().createStatement().executeQuery("SELECT count(*) as toplam FROM " + tableName[1]); //tablodaki kayıtlar getiriliyor
            res.next();
            System.out.println("Toplam row : " + res.getInt(1));
            res = baglantiAc().createStatement().executeQuery("Select * from " + tableName[1]);
//            while (res.next()) {

//                String str = "";
//                int i = 1;
//                while (i<=5) {
//                    try {
//                        if(i==5)
//                            str+=res.getLong(i++)+" ";
//                        else
//                        str += res.getString(i++)+" ";
//                    } catch (Exception ex) {
//                        ex.printStackTrace();
//                        break;
//                    }
//                }
//                System.out.print(res.getLong(5) + "\n");
//            }
        } catch (Exception ex) {
            Logger.getLogger(TestMySqlServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            this.baglantiKapat();
        } catch (Exception ex) {
            Logger.getLogger(TestMySqlServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private double getNearRandom(double val){
        return val+Math.random()-0.5;
    }
    public int dumpClientActionValue(int dumpNum) {
//        res = baglantiAc()
//        String sql = "Insert into client_action_value(idclient,action_code,value,date) VALUES(?,?,?,?)";
        String sql2 = "Replace into client_action_value(idclient,action_code,value,date) VALUES";
        StringBuilder sb = new StringBuilder();
        try {
//            PreparedStatement ps = this.baglantiAc().prepareStatement(sql);
        
            int sec = 4 * dumpNum;
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(0);
//            c.add(Calendar.MILLISECOND, sec+sec/2);
//            c.add(Calendar.SECOND, -sec);
            short action = 11;
            double[] value ={9.0,11.0};
            
            for (int i = 0; i < dumpNum; i++) {
                if (action > 13) {
                    action = 11;
                }
                for (short j = 1001; j < 1003; j++) {
                    sb.append("("+j+",");
//                    ps.setShort(1, j);
//                    ps.setShort(2,action);
                    sb.append(action+",");
                    value[j-1001]=getNearRandom(value[j-1001]);
//                    ps.setDouble(3, value[j-1001]);
                    Double v = value[j-1001];
sb.append(String.format("%d.%02d,",1,(int)(v.floatValue()*100)));
//                    ps.setLong(4, c.getTimeInMillis());
sb.append(c.getTimeInMillis()+"),");
//                    ps.executeUpdate();
                }
                action++;
                c.add(Calendar.SECOND, 4);
            }
            sql2=sql2+sb.substring(0, sb.length()-1)+";";
            return this.baglantiAc().createStatement().executeUpdate(sql2);
            
        } catch (Exception ex) {
            System.out.println(sql2);
            Logger.getLogger(TestMySqlServer.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }

    }

    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Started TestMySqlServer program");
       
        int number=10000;
        System.out.println("Started to dump into db ("+number+") values");
        long now = System.currentTimeMillis();
        int processed = new TestMySqlServer().dumpClientActionValue(number);
        long pass = System.currentTimeMillis();
        pass =pass-now;
        double val = pass/1000.0;
        System.out.println("Dumping finished in "+val+"sec with "+processed+"successfull inserts");
       
        System.out.println("Retrieving Data from db :");
        now = System.currentTimeMillis();
        new TestMySqlServer().read();
        pass = System.currentTimeMillis();
        val = (pass-now)/1000.0;
        System.out.println("Retrieving finished in "+val+"sec");
       
    }

}
