/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trycatchfinally;

/**
 *
 * @author ISIK
 */
public class TryCatchFinally {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Try to convert \"5\" to int :"+toInt("5"));
        System.out.println("Try to convert  \"asd\"to int : "+ toInt("asd"));
    }
    public static int toInt(String val){
        try{
            int v = Integer.parseInt(val);
            return v;
        }catch(Exception ex){
            return 0;
        }finally{
            System.out.println("finally block");
        }
    }
    
}
