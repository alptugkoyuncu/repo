/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package villacedrus;

/**
 *
 * @author ISIK
 */
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class VillaCedrus {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // TODO code application logic here
            String page = "https://www.villareyonu.com/villa-cedrus-1#musaitlikTakvimi";
            //Connecting to the web page
            Connection conn = Jsoup.connect(page);
            //executing the get request
            Document doc = conn.get();
            //Retrieving the contents (body) of the web page
            Element result = doc.getElementById("calendar");
            System.out.println(result);
        } catch (IOException ex) {
            Logger.getLogger(VillaCedrus.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
