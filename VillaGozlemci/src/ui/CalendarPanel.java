/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.swing.JLabel;

/**
 *
 * @author ISIK
 */
public class CalendarPanel extends javax.swing.JPanel {

    /**
     * Creates new form CalendarPanel
     */
    List<JLabel> daysTitle;
    List<DayLabel> days;
    YearMonth yearMonth;
    private Color getDaysLabelColor() {
        return Color.YELLOW;
    }
   
    private void setYearMonth(YearMonth ym) {
        
        this.yearMonth = ym;
       
        LocalDate dt = this.yearMonth.atDay(1);
        this.jlMonthYear.setText(dt.format(DateTimeFormatter.ofPattern("MMM YYYY")));
        
        int baslamaGunu = (dt.getDayOfWeek().getValue()-1);
        int i=0;
        //fill empty label
        for(i=0; i<baslamaGunu;i++){
            this.days.get(i).setValue(null);
        }
        for(int j=1; j<=this.yearMonth.lengthOfMonth();j++,i++ ){    
            this.days.get(i).setValue(j);
            this.days.get(i).setState(RezervationState.EMPTY);
        }
        //fill empty label
        for(; i<42;i++)
            this.days.get(i).setValue(null);
       
        
    }
    public void setRezervation(int day,RezervationState st){
        int baslamaGunu = day+this.yearMonth.atDay(1).getDayOfWeek().getValue()-1;
        days.get(baslamaGunu-1).setState(st);
    }
    
    public void resetRezervation() {
        for(int i=1; i<=this.yearMonth.lengthOfMonth();i++){
            setRezervation(i, RezervationState.EMPTY);
        }
    }
    public CalendarPanel(YearMonth ym) {
        daysTitle = new ArrayList<>();
        days = new ArrayList<>();
        initComponents();

        daysTitle.add(new JLabel("Pzt"));
        daysTitle.add(new JLabel("Sal"));
        daysTitle.add(new JLabel("Çar"));
        daysTitle.add(new JLabel("Per"));
        daysTitle.add(new JLabel("Cum"));
        daysTitle.add(new JLabel("Ctz"));
        daysTitle.add(new JLabel("Paz"));
         for (JLabel l : daysTitle) {
            this.jPanel2.add(l);
            l.setBackground(getDaysLabelColor());
            l.setOpaque(true);
        }
         
        for(int i=0; i<42; i++){
            DayLabel lbl = new DayLabel();
            this.days.add(lbl);
            this.jPanel2.add(lbl);
        }
        
        setYearMonth(ym);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jlMonthYear = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        setMaximumSize(new java.awt.Dimension(219, 197));
        setMinimumSize(new java.awt.Dimension(219, 197));
        setLayout(new java.awt.BorderLayout());

        jlMonthYear.setText("MONTH   YEAR");
        jPanel1.add(jlMonthYear);

        add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel2.setMaximumSize(new java.awt.Dimension(400, 276));
        jPanel2.setLayout(new java.awt.GridLayout(7, 7, 2, 2));
        add(jPanel2, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel jlMonthYear;
    // End of variables declaration//GEN-END:variables

}
