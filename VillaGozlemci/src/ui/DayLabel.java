/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 *
 * @author ISIK
 */
public class DayLabel extends JLabel {

    RezervationState state = RezervationState.EMPTY;
    Integer value;

    public DayLabel(Integer value) {
        this.value = value;
        this.setHorizontalAlignment(SwingConstants.CENTER);
        this.setVerticalAlignment(SwingConstants.CENTER);
        this.state = RezervationState.EMPTY;
        this.setOpaque(false);
        super.setText(this.value!=null ? this.value+"":"  ");
    }

    public DayLabel() {
        this(null);
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
        super.setText(this.value!=null?this.value+"" : "  ");
    }

    private Color getColor(RezervationState state) {
        switch (state) {
            case EMPTY:
                return Color.lightGray;
            case OPTIONAL:
                return Color.yellow;
            case FULL:
                return Color.RED;
            case ENTRANCE:
                return Color.LIGHT_GRAY;
            case EXIT:
                return Color.LIGHT_GRAY;
            case ENTRANCE_AND_EXIT:
                return Color.RED;
            default:
                return Color.lightGray;
        }
    }

    public void setState(RezervationState state) {
        RezervationState oldState = this.state;
        this.state = state;
        if (oldState == RezervationState.ENTRANCE && this.state == RezervationState.EXIT || oldState == RezervationState.EXIT && this.state == RezervationState.ENTRANCE) {
            this.state = RezervationState.ENTRANCE_AND_EXIT;
        }
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (getText() != null && !getText().isBlank()) {
            Dimension d = this.getBounds().getSize();
            g.setColor(getColor(state));
            g.fillRect(0, 0, d.width, d.height);

            switch (state) {
                case EMPTY:
                    break;
                case OPTIONAL:
                    break;
                case FULL:
                    break;
                case ENTRANCE: {
                    g.setColor(Color.RED);
                    int[] x = {0, d.width, d.width};
                    int[] y = {d.height, d.height, 0};
                    g.fillPolygon(x, y, 3);
                    break;
                }
                case EXIT: {
                    g.setColor(Color.RED);
                    int[] x = {0, 0, d.width};
                    int[] y = {d.height, 0, 0};
                    g.fillPolygon(x, y, 3);
                    break;
                }
                case ENTRANCE_AND_EXIT:
                    g.setColor(Color.BLACK);
                    g.drawLine(0, d.height, d.width, 2);
                    break;
                default:
                    break;
            }
        }
        super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.

    }

}
