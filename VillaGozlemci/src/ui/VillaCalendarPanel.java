/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import bean.RezervationBean;
import bean.VillaBean;
import db.DBConnection;
import db.OrderBy;
import db.rezervation_table.RezervationTableColumn;
import db.rezervation_table.RezervationTableRestriction;
import java.awt.GridLayout;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author ISIK
 */
public class VillaCalendarPanel extends JPanel {

    private VillaBean villa;
    Map<Month, CalendarPanel> calendarMap;
    Month startMonth, endMonth;
    int year;

    /**
     *
     * @param villa
     * @param baslangic inclusive
     * @param bitis exclusive
     * @param year
     */
  
    public VillaCalendarPanel(VillaBean villa, Month baslangic, Month bitis, int year) {
        this.startMonth = baslangic;
        this.endMonth = bitis;
        this.year = year;
        this.villa = villa;
        this.calendarMap = new TreeMap<>();
        for (int i = baslangic.getValue(); i < bitis.getValue(); i++) {
            Month m = Month.of(i);
            this.calendarMap.put(m, new CalendarPanel(YearMonth.of(year, m)));
        }
        this.setBorder(BorderFactory.createTitledBorder(villa.getName()));
        createUI();
        updateRezervationPanel();
    }
    
    public void updateRezervationPanel(){
        for(Month m : this.calendarMap.keySet()){
            CalendarPanel pnl = this.calendarMap.get(m);
            if(pnl!=null)
                pnl.resetRezervation();
        }
        for (RezervationBean rez : getRezervationForVilla()) {
            LocalDate baslangic = rez.getEntranceDate();
            LocalDate bitis = rez.getExitDate();

            Iterator<LocalDate> dates = baslangic.datesUntil(bitis).iterator();
            LocalDate rdate;
            // birinci gune gel
            if (dates.hasNext()) {
                rdate = dates.next();
            } else {
                continue;
            }
            Month rMonth = baslangic.getMonth();
            CalendarPanel calPnl = calendarMap.get(rMonth);
            calPnl.setRezervation(rdate.getDayOfMonth(), RezervationState.ENTRANCE);

            while (dates.hasNext()) {
                rdate = dates.next();
                if (rdate.getMonth() != rMonth) {
                    rMonth = rdate.getMonth();
                    calPnl = calendarMap.get(rMonth);
                }
                calPnl.setRezervation(rdate.getDayOfMonth(), RezervationState.FULL);
            }
            calPnl = calendarMap.get(bitis.getMonth());
            calPnl.setRezervation(bitis.getDayOfMonth(), RezervationState.EXIT);

        }
    }
    private void createUI() {
//        this.removeAll();
        GridLayout l = new GridLayout(1, this.endMonth.getValue() - this.startMonth.getValue(), 10, 10);
        this.setLayout(l);

        for(Month m : this.calendarMap.keySet()){
            JPanel pnl = this.calendarMap.get(m);
            if(pnl!=null)
            this.add(pnl);
        }
        
//        JScrollPane p = new JScrollPane();
//        p.add(pnl);
    }

    private List<RezervationBean> getRezervationForVilla() {
//        return new ArrayList<>();
        
        LocalDate bas = LocalDate.of(year, this.startMonth, 1);
        LocalDate bitis = LocalDate.of(year, this.endMonth.getValue(), 1);
        RezervationTableRestriction res = new RezervationTableRestriction();
        res.addCheckinRestriction(bas.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), bitis.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        res.addVillaRestriction(this.villa.getName());
        OrderBy orderBy = OrderBy.createOrderBy(RezervationTableColumn.CHECK_IN);
        List<RezervationBean> l = mainFrame.DB_CONNECTION.getAllRezervationsByRestriction(res, orderBy, null);
//         System.out.println(l);
         return l;
    }
}
