/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.rezervation_table;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author ISIK
 */
public class PagePanel extends JPanel{
    private final JButton jbtBas,jbtGeri,jbtSon,jbtIleri;
    private final JLabel lblPage = new JLabel();
    private int currentPage;
    private int maxPage;
    public PagePanel(int currentPage,int maxPage) {

        this.jbtBas = new JButton("<<");
        this.jbtGeri = new JButton("<");
        this.jbtIleri = new JButton(">");
        this.jbtSon = new JButton(">>");
        this.lblPage.setFont(new java.awt.Font("Tahoma", 0, 18));
        this.setPages(currentPage, maxPage);
        this.setLayout(new FlowLayout(FlowLayout.CENTER,10,10));
        this.add(jbtBas);
        this.add(jbtGeri);
        this.add(lblPage);
        this.add(jbtIleri);
        this.add(jbtSon);
    }
    public PagePanel(){
        this(0,0);
    }
    public void setPages(int currentPage,int maximumPage){
        this.currentPage = currentPage;
        this.maxPage = maximumPage;
        this.lblPage.setText(String.format("%2d/%2d",currentPage,maximumPage));
    }
    public void setCurrentPage(int currentPage){
        this.setPages(currentPage,this.maxPage);
    }
    public void setMaximumPage(int maximumPage){
        this.setPages(currentPage, maximumPage);
    }
    class PageActionEvent extends ActionEvent{

        public PageActionEvent(Object source, int id, String command) {
            super(source, id, command);
        }
        
    }
}
