/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.rezervation_table;

import bean.VillaBean;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import ui.mainFrame;

/**
 *
 * @author ISIK
 */
public class SearchPanel extends JPanel{
    private final JCheckBox[] villaList;
    private JButton jbtClear,jbtSelect;
    public SearchPanel(){
        this.setLayout(new BorderLayout(5,5));
        JPanel villaPanel = new JPanel(new FlowLayout(FlowLayout.LEFT,10,10));
        villaList = new JCheckBox[mainFrame.VILLA_SET.size()];
        int i=0;
        for(VillaBean villa:mainFrame.VILLA_SET){
            villaList[i] = new JCheckBox(villa.getName(),true);
            villaPanel.add(villaList[i++]);
        }
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT,10,10));
        this.jbtClear = new JButton("Hepsini kaldır");
        this.jbtSelect = new JButton("Hepsini seç");
        buttonPanel.add(this.jbtClear);
        buttonPanel.add(this.jbtSelect);
        this.add(BorderLayout.CENTER,villaPanel);
        this.add(BorderLayout.PAGE_END,buttonPanel);
    }
}
