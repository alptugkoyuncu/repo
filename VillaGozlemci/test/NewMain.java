
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ISIK
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        YearMonth ym = YearMonth.of(2022, Month.JUNE);
        LocalDate dt = ym.atDay(1);
        DayOfWeek dow = dt.getDayOfWeek();
        System.out.println(dow.getValue());
    }
    
}
