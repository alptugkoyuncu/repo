/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.time.LocalDateTime;

/**
 *
 * @author ISIK
 */
public class KontrolTableBean {
    private LocalDateTime updateDate;
    private String lastUpdateId;
    private String tableName;

    public KontrolTableBean(String id, String tableName, LocalDateTime dt) {
        this.lastUpdateId =id;
        this.tableName = tableName;
        this.updateDate = dt;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public String getLastUpdateId() {
        return lastUpdateId;
    }

    public void setLastUpdateId(String lastUpdateId) {
        this.lastUpdateId = lastUpdateId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    
}
