/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 *
 * @author ISIK
 */
public class RezervationBean implements Serializable,Comparable<RezervationBean>{

    private String code;
    private String customerName;
    private String customerTC;
    private String villaName;
    private LocalDateTime rezervationDate;
    private LocalDate entranceDate;
    private LocalDate exitDate;
    private BigDecimal prepayment;
    private BigDecimal postPayment;

    public RezervationBean() {
    }

    public RezervationBean(String code, String customerName, String customerTC, String villaName, LocalDateTime rezervationDate, LocalDate entranceDate, LocalDate exitDate, BigDecimal prepayment, BigDecimal postpayment) {
        this.code = code;
        this.customerName = customerName;
        this.customerTC = customerTC;
        this.villaName = villaName;
        this.rezervationDate = rezervationDate;
        this.entranceDate = entranceDate;
        this.exitDate = exitDate;
        this.prepayment = prepayment;
        this.postPayment = postpayment;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerTC() {
        return customerTC;
    }

    public void setCustomerTC(String customerTC) {
        this.customerTC = customerTC;
    }

    public String getVillaName() {
        return villaName;
    }

    public void setVillaName(String villaName) {
        this.villaName = villaName;
    }

    public LocalDateTime getRezervationDate() {
        return rezervationDate;
    }

    public void setRezervationDate(LocalDateTime rezervationDate) {
        this.rezervationDate = rezervationDate;
    }

    public LocalDate getEntranceDate() {
        return entranceDate;
    }

    public void setEntranceDate(LocalDate entranceDate) {
        this.entranceDate = entranceDate;
    }

    public LocalDate getExitDate() {
        return exitDate;
    }

    public void setExitDate(LocalDate exitDate) {
        this.exitDate = exitDate;
    }

    public BigDecimal getPrepayment() {
        return prepayment;
    }

    public void setPrepayment(BigDecimal prepayment) {
        this.prepayment = prepayment;
    }

    public BigDecimal getPostPayment() {
        return postPayment;
    }

    public void setPostPayment(BigDecimal payment) {
        this.postPayment = payment;
    }

    @Override
    public int compareTo(RezervationBean o) {
        return this.code.compareTo(o.getCode());
    
    }

    @Override
    public String toString() {
        return "RezervationBean{" + "code=" + code + ", customerName=" + customerName + ", customerTC=" + customerTC + ", villaName=" + villaName + ", rezervationDate=" + rezervationDate + ", entranceDate=" + entranceDate + ", exitDate=" + exitDate + ", prepayment=" + prepayment + ", postPayment=" + postPayment + '}';
    }

    
}
