/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.Serializable;

/**
 *
 * @author ISIK
 */
public class VillaBean implements Serializable,Comparable<VillaBean>{
    private String name;

    public VillaBean(String name) {
        this.name = name;
    }

    public VillaBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(VillaBean o) {
        return this.name.compareTo(o.getName());
    }

    @Override
    public String toString() {
        return this.name;
    }
    
    
}
