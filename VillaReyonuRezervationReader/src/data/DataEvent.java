/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import bean.RezervationBean;
import java.awt.event.ActionEvent;
import java.util.EventObject;

/**
 *
 * @author ISIK
 * @param <RezervationBean>
 */

public class DataEvent extends EventObject{
    RezervationBean rezervation;
    
    public DataEvent(Object source,RezervationBean rb) {
        super(source);
        this.rezervation = rb;
    }
    public RezervationBean getRezervation() {
        return rezervation;
    }

}
