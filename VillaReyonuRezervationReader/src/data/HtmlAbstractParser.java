/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import bean.RezervationBean;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 *
 * @author ISIK
 */
public interface HtmlAbstractParser {
  
    public void checkValid() throws SiteFormatException;

    public String getCustomerName();


    public String getCustomerTC();

    public String getCustomerPhone();

    public String getCustomerMail();

    public String getCustomerAddress();
    
    public String getVIllaName();
    
    public String getRezervationCode();
    public LocalDateTime getRezervationDate();
    public LocalDate getCheckin();
    
    public LocalDate getCheckout();
    
    public BigDecimal getRent();
    
    public BigDecimal getPrepayment();
    
    public BigDecimal getPostPayment();
    
    public BigDecimal getDeposit();
    
    public BigDecimal getCleanerCost();
    
    public BigDecimal getElectricityBill();
    public RezervationBean getRezervationBean();
}
