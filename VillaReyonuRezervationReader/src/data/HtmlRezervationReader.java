/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import bean.RezervationBean;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author ISIK
 */
public class HtmlRezervationReader {

    int site = 1;
    List<DataListener> listener;
    int lastRezervasyonKodu = 0;

    public HtmlRezervationReader(int site, int lastRezervationCode) {
        this.site = site;
        this.lastRezervasyonKodu = lastRezervationCode;
        listener = new ArrayList<>();
    }

    public int getLastRezervasyonKodu() {
        return lastRezervasyonKodu;
    }

    public void setLastRezervasyonKodu(int lastRezervasyonKodu) {
        this.lastRezervasyonKodu = lastRezervasyonKodu;
    }

    public HtmlAbstractParser readData() throws IOException, Exception {

        // TODO code application logic here
        String page = "https://www.villareyonu.com/boceksoft-vr/belgeler/sozlesme.asp?id=" + getLastRezervasyonKodu() + "&site=" + site;
//        Connecting to the web page
        Connection conn = Jsoup.connect(page);
//        executing the get request
        Document doc = conn.get();

//        File page = new File("C:/Users/ISIK/Downloads/rezervasyonlar/"+lastRezervasyonKodu+".htm");
//        if(!page.exists()){
//            throw new Exception("Site not found");
//        }
//        Document doc = Jsoup.parse(page, "UTF-8");
// Internetten indirdigimiz dosyayi bilgisayara kaydediyoruz
        String folder = "C:/Users/ISIK/Downloads/rezervasyonlar";
        File f = new File(folder);
        if (!f.exists()) {
            f.mkdirs();
        }
        f = new File(folder + File.separatorChar + lastRezervasyonKodu + ".htm");
        f.createNewFile();
        FileWriter fw = new FileWriter(f);
        fw.write(doc.toString());
        fw.close();

        /*
Element table = doc.select("table").get(0); //select the first table.
            Elements rows = table.select("tr");
//
        for (int i = 0; i < rows.size(); i++) { //first row is the col names so skip it.
            Element row = rows.get(i);
            System.out.println("<" + i + ">" + row.toString());
    Elements cols = row.select("td");
            
        }
         */
        switch (site) {
            case 1:
                return new Site1TableParser(doc);
            case 2:
                return new Site2TableParser(doc);
            default:
                return null;
        }
    }

    public void addDataListener(DataListener l) {
        this.listener.add(l);
    }

    public void removeDataListener(DataListener l) {
        this.listener.remove(l);
    }

    private void fireListener(RezervationBean bean) {
        for (DataListener reader : listener) {
            reader.dataReceived(new DataEvent(this, bean));
        }
    }

}
