/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import bean.RezervationBean;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * site1 icin turkce olani icin table parser
 *
 * @author ISIK
 */
public class Site1TableParser implements HtmlAbstractParser {
    //name-surname formati

    public final static String TITLE_OF_NAME = "İsim-Soyisim";

    //id formati
    public final static String TITLE_OF_ID = "Pasaport/TC Kimlik No";

    //phone formati
    public final static String TITLE_OF_PHONE = "Telefon";

    //mail formati
    public final static String TITLE_OF_MAIL = "Mail Adresi";

    //address formati
    public final static String TITLE_OF_ADDRESS = "Adres";

    //villa name formati
    public final static String TITLE_OF_VILLA = "Villa Adı";

    //REZERVASYON KODU formati
    public final static String TITLE_OF_RCODE = "Rezervasyon Kodu";
    //REZERVASYON TARIHI
    public final static String TITLE_OF_REZERVATION_DATE = "Rezervasyon Tarihi";
    //checkIn formati
    public final static String TITLE_OF_CHECKIN = "Giriş Tarihi ve Saati";

    //checkOUT formati
    public final static String TITLE_OF_CHECKOUT = "Çıkış Tarihi ve Saati";

    //rent formati
    public final static String TITLE_OF_TOTAL_PAYMENT = "Kiralama Ücreti";

    //PREPAYMENT formati
    public final static String TITLE_OF_PREPAYMENT = "1. Ödeme (Ön Ödeme - İade Edilmez)";

    //POSTPAYMENT formati
    public final static String TITLE_OF_POSTPAYMENT = "2. Ödeme (Villaya Girişte Ev Sahibine ya da Yetkili Kişiye Yapılacak Ödeme)";

    public final static String TITLE_OF_DEPOSIT = "Hasar Depozitosu (Villada Hasar/Ziyan Olmadığı Takdirde İadesi Yapılmaktadır)";

    public final static String TITLE_OF_MAID_PAY = "Temizlik Ücreti";

    public final static String TITLE_OF_ELECTRICTY_BILL = "Elektrik Ücreti";

    public final static int TABLE_COUNT = 1;
    public final static int ROW_COUNT = 42;

    //name-surname formati
    public final static int TR_INDEX_NAME = 8;
    public final static int TD_INDEX_TITLE_NAME = 0;
    public final static int TD_INDEX_VALUE_NAME = 2;
    public final static int TOTAL_TD_NAME = 3;

    //id formati
    public final static int TR_INDEX_TC = 9;
    public final static int TD_INDEX_TITLE_TC = 0;
    public final static int TD_INDEX_VALUE_TC = 2;
    public final static int TOTAL_TD_TC = 3;

    //phone formati
    public final static int TR_INDEX_PHONE = 10;
    public final static int TD_INDEX_TITLE_OF_PHONE = 0;
    public final static int TD_INDEX_VALUE_PHONE = 2;
    public final static int TOTAL_TD_PHONE = 3;

    //mail formati
    public final static int TR_INDEX_MAIL = 11;
    public final static int TD_INDEX_TITLE_MAIL = 0;
    public final static int TD_INDEX_VALUE_MAIL = 2;
    public final static int TOTAL_TD_MAIL = 3;

    //address formati
    public final static int TR_INDEX_ADDRESS = 12;
    public final static int TD_INDEX_TITLE_ADDRESS = 0;
    public final static int TD_INDEX_VALUE_ADDRESS = 2;
    public final static int TOTAL_TD_ADDRESS = 3;

    //villa name formati
    public final static int TR_INDEX_VILLA = 14;
    public final static int TD_INDEX_TITLE_VILLA = 0;
    public final static int TD_INDEX_VALUE_VILLA = 2;
    public final static int TOTAL_TD_VILLA = 3;

    //REZERVASYON KODU formati
    public final static int TR_INDEX_RCODE = 15;
    public final static int TD_INDEX_TITLE_RCODE = 0;
    public final static int TD_INDEX_VALUE_RCODE = 2;
    public final static int TOTAL_TD_RCODE = 3;

    //REZERVASYON Tarihi formati
    public final static int TR_INDEX_RDATE = 17;
    public final static int TD_INDEX_TITLE_RDATE = 0;
    public final static int TD_INDEX_VALUE_RDATE = 2;
    public final static int TOTAL_TD_RDATE = 3;
    //checkIn formati
    public final static int TR_INDEX_CHECKIN = 18;
    public final static int TD_INDEX_TITLE_CHECKIN = 0;
    public final static int TD_INDEX_VALUE_CHECKIN = 2;
    public final static int TOTAL_TD_CHECKIN = 4;

    //checkOUT formati
    public final static int TR_INDEX_CHECKOUT = 19;
    public final static int TD_INDEX_TITLE_CHECKOUT = 0;
    public final static int TD_INDEX_VALUE_CHECKOUT = 2;
    public final static int TOTAL_TD_CHECKOUT = 4;

    //rent formati
    public final static int TR_INDEX_TOTAL_PAYMENT = 22;
    public final static int TD_INDEX_TITLE_TOTAL_PAYMENT = 0;
    public final static int TD_INDEX_VALUE_TOTAL_PAYMENT = 2;
    public final static int TOTAL_TD_TOTAL_PAYMENT = 4;

    //PREPAYMENT formati
    public final static int TR_INDEX_PREPAYMENT = 23;
    public final static int TD_INDEX_TITLE_PREPAYMENT = 0;
    public final static int TD_INDEX_VALUE_PREPAYMENT = 2;
    public final static int TOTAL_TD_PREPAYMENT = 4;

    //POSTPAYMENT formati
    public final static int TR_INDEX_POSTPAYMENT = 24;
    public final static int TD_INDEX_TITLE_POSTPAYMENT = 0;
    public final static int TD_INDEX_VALUE_POSTPAYMENT = 2;
    public final static int TOTAL_TD_POSTPAYMENT = 4;

    //DEPOSIT formati
    public final static int TR_INDEX_DEPOSIT = 25;
    public final static int TD_INDEX_TITLE_DEPOSIT = 0;
    public final static int TD_INDEX_VALUE_DEPOSIT = 2;
    public final static int TOTAL_TD_DEPOSIT = 4;

    //MAID PAY formati
    public final static int TR_INDEX_MAID = 26;
    public final static int TD_INDEX_TITLE_MAID = 0;
    public final static int TD_INDEX_VALUE_MAID = 2;
    public final static int TOTAL_TD_MAID = 4;

    //MAID PAY formati
    public final static int TR_INDEX_EBILL = 27;
    public final static int TD_INDEX_TITLE_EBILL = 0;
    public final static int TD_INDEX_VALUE_EBILL = 2;
    public final static int TOTAL_TD_EBILL = 4;
    private final Document doc;
    private final Elements rows;

    public Site1TableParser(Document doc) throws SiteFormatException {
        this.doc = doc;
        this.checkValid();
        this.rows = ((Element)doc.select("table").get(0)).select("tr");
    }

    @Override
    public void checkValid() throws SiteFormatException {

        //table sayisini kontrol et
        if (doc.select("table").size() != TABLE_COUNT) {
            throw new SiteFormatException("Table sayisi hatasi", TABLE_COUNT, doc.select("table").size());
        }

        Element table = doc.select("table").get(0); //select the first table.
        Elements rows = table.select("tr");

        //row sayisini kontrol et
        if (rows.size() != ROW_COUNT) {
            throw new SiteFormatException("Toplam minimum row hatasi", ROW_COUNT, rows.size());
        }
        //name surname formati dogru mu 
        Elements cols = rows.get(TR_INDEX_NAME).select("td");
        if (cols.size() != TOTAL_TD_NAME) {
            throw new SiteFormatException("Toplam isim satirindaki td sayisi hatali", TOTAL_TD_NAME, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_NAME).text().equalsIgnoreCase(TITLE_OF_NAME)) {
            throw new SiteFormatException("isim soyisim title tutmuyor", TITLE_OF_NAME, cols.get(0).text());
        }

        // tc numarasi fortmati dogru mu
        cols = rows.get(TR_INDEX_TC).select("td");
        if (cols.size() != TOTAL_TD_TC) {
            throw new SiteFormatException("Toplam id satirindaki td sayisi hatali", TOTAL_TD_TC, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_TC).text().equalsIgnoreCase(TITLE_OF_ID)) {
            throw new SiteFormatException("id title tutmuyor", TITLE_OF_ID, cols.get(0).text());
        }

        //phone formati dogru mu
        cols = rows.get(TR_INDEX_PHONE).select("td");
        if (cols.size() != TOTAL_TD_PHONE) {
            throw new SiteFormatException("Toplam phone satirindaki td sayisi hatali", TOTAL_TD_PHONE, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_OF_PHONE).text().equalsIgnoreCase(TITLE_OF_PHONE)) {
            throw new SiteFormatException("phone title tutmuyor", TITLE_OF_PHONE, cols.get(0).text());
        }

        //maiL adresi formati dogrumu
        cols = rows.get(TR_INDEX_MAIL).select("td");
        if (cols.size() != TOTAL_TD_MAIL) {
            throw new SiteFormatException("Toplam mail satirindaki td sayisi hatali", TOTAL_TD_MAIL, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_MAIL).text().equalsIgnoreCase(TITLE_OF_MAIL)) {
            throw new SiteFormatException("mail title tutmuyor", TITLE_OF_MAIL, cols.get(0).text());
        }

        // adres formati dogrumu
        cols = rows.get(TR_INDEX_ADDRESS).select("td");
        if (cols.size() != TOTAL_TD_ADDRESS) {
            throw new SiteFormatException("Toplam adres satirindaki td sayisi hatali", TOTAL_TD_ADDRESS, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_ADDRESS).text().equalsIgnoreCase(TITLE_OF_ADDRESS)) {
            throw new SiteFormatException("adres title tutmuyor", TITLE_OF_ADDRESS, cols.get(0).text());
        }

        // Villa name formati dogrumu
        cols = rows.get(TR_INDEX_VILLA).select("td");
        if (cols.size() != TOTAL_TD_VILLA) {
            throw new SiteFormatException("Villa adi satirindaki td sayisi hatali", TOTAL_TD_VILLA, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_VILLA).text().equalsIgnoreCase(TITLE_OF_VILLA)) {
            throw new SiteFormatException("villa adi title tutmuyor", TITLE_OF_VILLA, cols.get(0).text());
        }
        // rezervasyon kodu
        cols = rows.get(TR_INDEX_RCODE).select("td");
        if (cols.size() != TOTAL_TD_RCODE) {
            throw new SiteFormatException("rezervasyon kodu satirindaki td sayisi hatali", TOTAL_TD_RCODE, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_RCODE).text().equalsIgnoreCase(TITLE_OF_RCODE)) {
            throw new SiteFormatException("rezervasyon kodu title tutmuyor", TITLE_OF_RCODE, cols.get(0).text());
        }
        // rezervasyon tarihi
        cols = rows.get(TR_INDEX_RDATE).select("td");
        if (cols.size() != TOTAL_TD_RDATE) {
            throw new SiteFormatException("rezervasyon tarihi satirindaki td sayisi hatali", TOTAL_TD_RDATE, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_RDATE).text().equalsIgnoreCase(TITLE_OF_REZERVATION_DATE)) {
            throw new SiteFormatException("rezervasyon tarihi title tutmuyor", TITLE_OF_REZERVATION_DATE, cols.get(0).text());
        }
        // checkin formatini kontrol et
        cols = rows.get(TR_INDEX_CHECKIN).select("td");
        if (cols.size() != TOTAL_TD_CHECKIN) {
            throw new SiteFormatException("Check in satirindaki td sayisi hatali", TOTAL_TD_CHECKIN, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_CHECKIN).text().equalsIgnoreCase(TITLE_OF_CHECKIN)) {
            throw new SiteFormatException("Check in title tutmuyor", TITLE_OF_CHECKIN, cols.get(0).text());
        }
        // checkout formatini kontrol et
        cols = rows.get(TR_INDEX_CHECKOUT).select("td");
        if (cols.size() != TOTAL_TD_CHECKOUT) {
            throw new SiteFormatException("Checkout satirindaki td sayisi hatali", TOTAL_TD_CHECKIN, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_CHECKOUT).text().equalsIgnoreCase(TITLE_OF_CHECKOUT)) {
            throw new SiteFormatException("Checkout title tutmuyor", TITLE_OF_CHECKOUT, cols.get(0).text());
        }
        // total payment formatini kontrol et
        cols = rows.get(TR_INDEX_TOTAL_PAYMENT).select("td");
        if (cols.size() != TOTAL_TD_TOTAL_PAYMENT) {
            throw new SiteFormatException("Toplam odeme satirindaki td sayisi hatali", TOTAL_TD_TOTAL_PAYMENT, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_TOTAL_PAYMENT).text().equalsIgnoreCase(TITLE_OF_TOTAL_PAYMENT)) {
            throw new SiteFormatException("Toplam odeme title tutmuyor", TITLE_OF_TOTAL_PAYMENT, cols.get(0).text());
        }

        // prepayment formatini kontrol et
        cols = rows.get(TR_INDEX_PREPAYMENT).select("td");
        if (cols.size() != TOTAL_TD_PREPAYMENT) {
            throw new SiteFormatException("on odeme satirindaki td sayisi hatali", TOTAL_TD_PREPAYMENT, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_PREPAYMENT).text().equalsIgnoreCase(TITLE_OF_PREPAYMENT)) {
            throw new SiteFormatException("on odeme title tutmuyor", TITLE_OF_PREPAYMENT, cols.get(0).text());
        }

        // postpayment formatini kontrol et
        cols = rows.get(TR_INDEX_POSTPAYMENT).select("td");
        if (cols.size() != TOTAL_TD_POSTPAYMENT) {
            throw new SiteFormatException("son odeme satirindaki td sayisi hatali", TOTAL_TD_POSTPAYMENT, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_POSTPAYMENT).text().equalsIgnoreCase(TITLE_OF_POSTPAYMENT)) {
            throw new SiteFormatException("son odeme title tutmuyor", TITLE_OF_POSTPAYMENT, cols.get(0).text());
        }

        // deposit formatini kontrol et
        cols = rows.get(TR_INDEX_DEPOSIT).select("td");
        if (cols.size() != TOTAL_TD_DEPOSIT) {
            throw new SiteFormatException("deposit satirindaki td sayisi hatali", TOTAL_TD_DEPOSIT, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_DEPOSIT).text().equalsIgnoreCase(TITLE_OF_DEPOSIT)) {
            throw new SiteFormatException("deposit title tutmuyor", TITLE_OF_DEPOSIT, cols.get(0).text());
        }

        //Temizlikci formatini kontrol et
        cols = rows.get(TR_INDEX_MAID).select("td");
        if (cols.size() != TOTAL_TD_MAID) {
            throw new SiteFormatException("temizlik odemesi satirindaki td sayisi hatali", TOTAL_TD_MAID, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_MAID).text().equalsIgnoreCase(TITLE_OF_MAID_PAY)) {
            throw new SiteFormatException("temizlik odemesi title tutmuyor", TITLE_OF_MAID_PAY, cols.get(0).text());
        }

        //elektrik faturasi formatini kontrol et
        cols = rows.get(TR_INDEX_EBILL).select("td");
        if (cols.size() != TOTAL_TD_EBILL) {
            throw new SiteFormatException("elektrik faturasi satirindaki td sayisi hatali", TOTAL_TD_EBILL, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_EBILL).text().equalsIgnoreCase(TITLE_OF_ELECTRICTY_BILL)) {
            throw new SiteFormatException("elektrik faturasi title tutmuyor", TITLE_OF_ELECTRICTY_BILL, cols.get(0).text());
        }
    }

    @Override
    public String getCustomerName() {
        return rows.get(TR_INDEX_NAME).select("td").get(TD_INDEX_VALUE_NAME).text().trim();
    }

    @Override
    public String getCustomerTC() {
        return rows.get(TR_INDEX_TC).select("td").get(TD_INDEX_VALUE_TC).text().trim();
    }

    @Override
    public String getCustomerPhone() {
        return rows.get(TR_INDEX_PHONE).select("td").get(TD_INDEX_VALUE_PHONE).text().trim();
    }

    @Override
    public String getCustomerMail() {
        return rows.get(TR_INDEX_MAIL).select("td").get(TD_INDEX_VALUE_MAIL).text().trim();
    }

    @Override
    public String getCustomerAddress() {
        return rows.get(TR_INDEX_ADDRESS).select("td").get(TD_INDEX_VALUE_ADDRESS).text().trim();
    }

    @Override
    public String getVIllaName() {
        return rows.get(TR_INDEX_VILLA).select("td").get(TD_INDEX_VALUE_VILLA).text().trim();
    }

    @Override
    public String getRezervationCode() {
        return rows.get(TR_INDEX_RCODE).select("td").get(TD_INDEX_VALUE_RCODE).text().trim();
    }

    @Override
    public LocalDateTime getRezervationDate() {
        LocalDateTime dt =null;
        try{
        String[] dateTime = rows.get(TR_INDEX_RDATE).select("td").get(TD_INDEX_VALUE_RDATE).text().trim().split(" ");
        String[] date = dateTime[0].split("[.]",0);  
        String time[] = dateTime[1].split(":");
        dt = LocalDateTime.of(Integer.parseInt(date[2]), Integer.parseInt(date[1]),Integer.parseInt(date[0]), Integer.parseInt(time[0]),Integer.parseInt(time[1]),Integer.parseInt(time[2]));
        }catch(Exception ex){
         
            dt  =LocalDateTime.now();
        }
        finally{
            return dt;
        }
    }

    @Override
    public LocalDate getCheckin() {
        String[] date = rows.get(TR_INDEX_CHECKIN).select("td").get(TD_INDEX_VALUE_CHECKIN).text().trim().split("-");
        try {
            int year = Integer.parseInt(date[0]);
            int month = Integer.parseInt(date[1]);
            int day = Integer.parseInt(date[2]);
            return LocalDate.of(year, Month.of(month), day);
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    @Override
    public LocalDate getCheckout() {
        String[] date = rows.get(TR_INDEX_CHECKOUT).select("td").get(TD_INDEX_VALUE_CHECKOUT).text().trim().split("-");
        try {
            int year = Integer.parseInt(date[0]);
            int month = Integer.parseInt(date[1]);
            int day = Integer.parseInt(date[2]);
            return LocalDate.of(year, Month.of(month), day);
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    @Override
    public BigDecimal getRent() {
        String money = rows.get(TR_INDEX_TOTAL_PAYMENT).select("td").get(TD_INDEX_VALUE_TOTAL_PAYMENT).text().trim().split(" ")[0].trim();
        money = money.replaceAll("[.]", "");
        money = money.replaceAll("[,]", "[.]");
        return new BigDecimal(money);
    }

    @Override
    public BigDecimal getPrepayment() {
        String money = rows.get(TR_INDEX_PREPAYMENT).select("td").get(TD_INDEX_VALUE_PREPAYMENT).text().trim().split(" ")[0].trim();
       money = money.replaceAll("[.]", "");
        money = money.replaceAll("[,]", "[.]");
            return new BigDecimal(money);
    }

    @Override
    public BigDecimal getPostPayment() {
        String money = rows.get(TR_INDEX_POSTPAYMENT).select("td").get(TD_INDEX_VALUE_POSTPAYMENT).text().trim().split(" ")[0].trim();
        money = money.replaceAll("[.]", "");
        money = money.replaceAll("[,]", "[.]");
             return new BigDecimal(money);
    }

    @Override
    public BigDecimal getDeposit() {
        String money = rows.get(TR_INDEX_DEPOSIT).select("td").get(TD_INDEX_VALUE_DEPOSIT).text().trim().split(" ")[0].trim();
        money = money.replaceAll("[.]", "");
        money = money.replaceAll("[,]", "[.]");
        return new BigDecimal(money);
    }

    @Override
    public BigDecimal getCleanerCost() {
        String money = rows.get(TR_INDEX_MAID).select("td").get(TD_INDEX_VALUE_MAID).text().trim().split(" ")[0].trim();
        money = money.replaceAll("[.]", "");
        money = money.replaceAll("[,]", "[.]");
        return new BigDecimal(money);
    }

    @Override
    public BigDecimal getElectricityBill() {
        String money = rows.get(TR_INDEX_EBILL).select("td").get(TD_INDEX_VALUE_EBILL).text().trim().split(" ")[0].trim();
        money = money.replaceAll("[.]", "");
        money = money.replaceAll("[,]", "[.]");
        return new BigDecimal(money);
    }

    @Override
    public RezervationBean getRezervationBean() {   
        RezervationBean r = new RezervationBean();
        r.setCode(getRezervationCode());
        r.setCustomerName(getCustomerName());
        r.setCustomerTC(getCustomerTC());
        r.setEntranceDate(getCheckin());
        r.setExitDate(getCheckout());
        r.setPostPayment(getPostPayment());
        r.setPrepayment(getPrepayment());
        r.setRezervationDate(getRezervationDate());
        r.setVillaName(getVIllaName());
        return r;
    }

}
