/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import bean.RezervationBean;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *site1 icin turkce olani icin table parser
 * @author ISIK
 */
public class Site2TableParser implements HtmlAbstractParser {
      //name-surname formati
    public final static String TITLE_OF_NAME = "Name surname";

    //id formati
    public final static String TITLE_OF_TC = "Passport / TC ID Number";

    //phone formati
    public final static String TITLE_OF_PHONE = "Telephone";

    //mail formati
    public final static String TITLE_OF_MAIL = "Mail Address";

    //address formati
    public final static String TITLE_OF_ADDRESS = "Address";
    
    //villa name formati
    public final static String TITLE_OF_VILLA = "Villa Name";
    
    //REZERVASYON KODU formati
    public final static String TITLE_OF_RCODE = "Reservation Code";
    
    //checkIn formati
    public final static String TITLE_OF_CHECKIN = "Check-in Date and Time";
    
    //checkOUT formati
    public final static String TITLE_OF_CHECKOUT = "Check-out Date and Time";
    
    //rent formati
    public final static String TITLE_OF_TOTAL_PAYMENT = "Rental Fee";
    
    //PREPAYMENT formati
    public final static String TITLE_OF_PREPAYMENT = "1. Payment (Prepayment - Non Refundable)";
    
    //POSTPAYMENT formati
    public final static String TITLE_OF_POSTPAYMENT = "2. Payment (Payment to the Host or Authorized Person to the Villa)";
    //deposit formati
    public final static String TITLE_OF_DEPOSIT = "Damage Deposit (If the villa is not damaged / returned)";
  
public final static int TABLE_COUNT = 1;
    public final static int ROW_COUNT = 39;

    //name-surname formati
    public final static int TR_INDEX_NAME = 8;
    public final static int TD_INDEX_TITLE_NAME = 0;
    public final static int TD_INDEX_VALUE_NAME = 2;
    public final static int TOTAL_TD_NAME = 3;

    //id formati
    public final static int TR_INDEX_TC = 9;
    public final static int TD_INDEX_TITLE_TC = 0;
    public final static int TD_INDEX_VALUE_TC = 2;
    public final static int TOTAL_TD_TC = 3;

    //phone formati
    public final static int TR_INDEX_PHONE = 10;
    public final static int TD_INDEX_TITLE_OF_PHONE = 0;
    public final static int TD_INDEX_VALUE_PHONE = 2;
    public final static int TOTAL_TD_PHONE = 3;

    //mail formati
    public final static int TR_INDEX_MAIL = 11;
    public final static int TD_INDEX_TITLE_MAIL = 0;
    public final static int TD_INDEX_VALUE_MAIL = 2;
    public final static int TOTAL_TD_MAIL = 3;

    //address formati
    public final static int TR_INDEX_ADDRESS = 12;
    public final static int TD_INDEX_TITLE_ADDRESS = 0;
    public final static int TD_INDEX_VALUE_ADDRESS = 2;
    public final static int TOTAL_TD_ADDRESS = 3;
    
    //villa name formati
    public final static int TR_INDEX_VILLA = 14;
    public final static int TD_INDEX_VILLA_TITLE = 0;
    public final static int TD_INDEX_VILLA_VALUE = 2;
    public final static int TOTAL_TD_VILLA = 3;
    
    //REZERVASYON KODU formati
    public final static int TR_INDEX_RCODE = 15;
    public final static int TD_INDEX_TITLE_RCODE = 0;
    public final static int TD_INDEX_VALUE_RCODE = 2;
    public final static int TOTAL_TD_RCODE = 3;
    
    //checkIn formati
    public final static int TR_INDEX_CHECKIN = 17;
    public final static int TD_INDEX_TITLE_CHECKIN = 0;
    public final static int TD_INDEX_VALUE_CHECKIN = 2;
    public final static int TOTAL_TD_CHECKIN = 4;
    
    //checkOUT formati
    public final static int TR_INDEX_CHECKOUT = 18;
    public final static int TD_INDEX_TITLE_CHECKOUT = 0;
    public final static int TD_INDEX_VALUE_CHECKOUT = 2;
    public final static int TOTAL_TD_CHECKOUT = 4;
    
    //rent formati
    public final static int TR_INDEX_TOTAL_PAYMENT = 21;
    public final static int TD_INDEX_TITLE_TOTAL_PAYMENT = 0;
    public final static int TD_INDEX_VALUE_TOTAL_PAYMENT = 2;
    public final static int TOTAL_TD_TOTAL_PAYMENT = 4;
    
    //PREPAYMENT formati
    public final static int TR_INDEX_PREPAYMENT = 22;
    public final static int TD_INDEX_TITLE_PREPAYMENT = 0;
    public final static int TD_INDEX_VALUE_PREPAYMENT = 2;
    public final static int TOTAL_TD_PREPAYMENT = 4;
    
    //POSTPAYMENT formati
    public final static int TR_INDEX_POSTPAYMENT = 23;
    public final static int TD_INDEX_TITLE_POSTPAYMENT = 0;
    public final static int TD_INDEX_VALUE_POSTPAYMENT = 2;
    public final static int TOTAL_TD_POSTPAYMENT = 4;
    
     //DEPOSIT formati
    public final static int TR_INDEX_DEPOSIT = 24;
    public final static int TD_INDEX_TITLE_DEPOSIT = 0;
    public final static int TD_INDEX_VALUE_DEPOSIT = 2;
    public final static int TOTAL_TD_DEPOSIT = 4;
    private Document doc;
    private Elements rows;

    public Site2TableParser(Document doc) throws SiteFormatException{
        this.doc = doc;
        this.checkValid();
        this.rows = ((Element)doc.select("table").get(0)).select("tr");
    }
    @Override
    public void checkValid() throws SiteFormatException {

        //table sayisini kontrol et
        if (doc.select("table").size() != TABLE_COUNT) {
            throw new SiteFormatException("Table sayisi hatasi", TABLE_COUNT, doc.select("table").size());
        }

        Element table = doc.select("table").get(0); //select the first table.
        Elements rows = table.select("tr");

        //row sayisini kontrol et
        if (rows.size() != ROW_COUNT) {
            throw new SiteFormatException("Toplam minimum row hatasi", ROW_COUNT, rows.size());
        }
        //name surname formati dogru mu 
        Elements cols = rows.get(TR_INDEX_NAME).select("td");
        if (cols.size() != TOTAL_TD_NAME) {
            throw new SiteFormatException("Toplam isim satirindaki td sayisi hatali", TOTAL_TD_NAME, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_NAME).text().equalsIgnoreCase(TITLE_OF_NAME)) {
            throw new SiteFormatException("isim soyisim title tutmuyor", TITLE_OF_NAME, cols.get(0).text());
        }

        // tc numarasi fortmati dogru mu
        cols = rows.get(TR_INDEX_TC).select("td");
        if (cols.size() != TOTAL_TD_TC) {
            throw new SiteFormatException("Toplam id satirindaki td sayisi hatali", TOTAL_TD_TC, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_TC).text().equalsIgnoreCase(TITLE_OF_TC)) {
            throw new SiteFormatException("id title tutmuyor", TITLE_OF_TC, cols.get(0).text());
        }

        //phone formati dogru mu
        cols = rows.get(TR_INDEX_PHONE).select("td");
        if (cols.size() != TOTAL_TD_PHONE) {
            throw new SiteFormatException("Toplam phone satirindaki td sayisi hatali", TOTAL_TD_PHONE, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_OF_PHONE).text().equalsIgnoreCase(TITLE_OF_PHONE)) {
            throw new SiteFormatException("phone title tutmuyor", TITLE_OF_PHONE, cols.get(0).text());
        }

        //maiL adresi formati dogrumu
        cols = rows.get(TR_INDEX_MAIL).select("td");
        if (cols.size() != TOTAL_TD_MAIL) {
            throw new SiteFormatException("Toplam mail satirindaki td sayisi hatali", TOTAL_TD_MAIL, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_MAIL).text().equalsIgnoreCase(TITLE_OF_MAIL)) {
            throw new SiteFormatException("mail title tutmuyor", TITLE_OF_MAIL, cols.get(0).text());
        }
        
        // adres formati dogrumu
        cols = rows.get(TR_INDEX_ADDRESS).select("td");
        if (cols.size() != TOTAL_TD_ADDRESS) {
            throw new SiteFormatException("Toplam adres satirindaki td sayisi hatali", TOTAL_TD_ADDRESS, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_ADDRESS).text().equalsIgnoreCase(TITLE_OF_ADDRESS)) {
            throw new SiteFormatException("adres title tutmuyor", TITLE_OF_ADDRESS, cols.get(0).text());
        }
        
        // Villa name formati dogrumu
        cols = rows.get(TR_INDEX_VILLA).select("td");
        if (cols.size() != TOTAL_TD_VILLA) {
            throw new SiteFormatException("Villa adi satirindaki td sayisi hatali", TOTAL_TD_VILLA, cols.size());
        }
        if (!cols.get(TD_INDEX_VILLA_TITLE).text().equalsIgnoreCase(TITLE_OF_VILLA)) {
            throw new SiteFormatException("villa adi title tutmuyor", TITLE_OF_VILLA, cols.get(0).text());
        }
        // rezervasyon kodu
        cols = rows.get(TR_INDEX_RCODE).select("td");
        if (cols.size() != TOTAL_TD_RCODE) {
            throw new SiteFormatException("rezervasyon kodu satirindaki td sayisi hatali", TOTAL_TD_RCODE, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_RCODE).text().equalsIgnoreCase(TITLE_OF_RCODE)) {
            throw new SiteFormatException("rezervasyon kodu title tutmuyor", TITLE_OF_RCODE, cols.get(0).text());
        }
         // checkin formatini kontrol et
        cols = rows.get(TR_INDEX_CHECKIN).select("td");
        if (cols.size() != TOTAL_TD_CHECKIN) {
            throw new SiteFormatException("Check in satirindaki td sayisi hatali", TOTAL_TD_CHECKIN, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_CHECKIN).text().equalsIgnoreCase(TITLE_OF_CHECKIN)) {
            throw new SiteFormatException("Check in title tutmuyor", TITLE_OF_CHECKIN, cols.get(0).text());
        }
         // checkout formatini kontrol et
        cols = rows.get(TR_INDEX_CHECKOUT).select("td");
        if (cols.size() != TOTAL_TD_CHECKOUT) {
            throw new SiteFormatException("Checkout satirindaki td sayisi hatali", TOTAL_TD_CHECKIN, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_CHECKOUT).text().equalsIgnoreCase(TITLE_OF_CHECKOUT)) {
            throw new SiteFormatException("Checkout title tutmuyor", TITLE_OF_CHECKOUT, cols.get(0).text());
        }
         // total payment formatini kontrol et
        cols = rows.get(TR_INDEX_TOTAL_PAYMENT).select("td");
        if (cols.size() != TOTAL_TD_TOTAL_PAYMENT) {
            throw new SiteFormatException("Toplam odeme satirindaki td sayisi hatali", TOTAL_TD_TOTAL_PAYMENT, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_TOTAL_PAYMENT).text().equalsIgnoreCase(TITLE_OF_TOTAL_PAYMENT)) {
            throw new SiteFormatException("Toplam odeme title tutmuyor", TITLE_OF_TOTAL_PAYMENT, cols.get(0).text());
        }
        
         // prepayment formatini kontrol et
        cols = rows.get(TR_INDEX_PREPAYMENT).select("td");
        if (cols.size() != TOTAL_TD_PREPAYMENT) {
            throw new SiteFormatException("on odeme satirindaki td sayisi hatali", TOTAL_TD_PREPAYMENT, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_PREPAYMENT).text().equalsIgnoreCase(TITLE_OF_PREPAYMENT)) {
            throw new SiteFormatException("on odeme title tutmuyor", TITLE_OF_PREPAYMENT, cols.get(0).text());
        }
        
         // postpayment formatini kontrol et
        cols = rows.get(TR_INDEX_POSTPAYMENT).select("td");
        if (cols.size() != TOTAL_TD_POSTPAYMENT) {
            throw new SiteFormatException("son odeme satirindaki td sayisi hatali", TOTAL_TD_POSTPAYMENT, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_POSTPAYMENT).text().equalsIgnoreCase(TITLE_OF_POSTPAYMENT)) {
            throw new SiteFormatException("son odeme title tutmuyor", TITLE_OF_POSTPAYMENT, cols.get(0).text());
        }
         // deposit formatini kontrol et
        cols = rows.get(TR_INDEX_DEPOSIT).select("td");
        if (cols.size() != TOTAL_TD_DEPOSIT) {
            throw new SiteFormatException("deposit satirindaki td sayisi hatali", TOTAL_TD_DEPOSIT, cols.size());
        }
        if (!cols.get(TD_INDEX_TITLE_DEPOSIT).text().equalsIgnoreCase(TITLE_OF_DEPOSIT)) {
            throw new SiteFormatException("deposit title tutmuyor", TITLE_OF_DEPOSIT, cols.get(0).text());
        }
    }

    @Override
    public String getCustomerName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public String getCustomerTC() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCustomerPhone() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCustomerMail() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCustomerAddress() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getVIllaName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getRezervationCode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LocalDate getCheckin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LocalDate getCheckout() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public RezervationBean getRezervationBean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LocalDateTime getRezervationDate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BigDecimal getRent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BigDecimal getPrepayment() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BigDecimal getPostPayment() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BigDecimal getDeposit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BigDecimal getCleanerCost() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BigDecimal getElectricityBill() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
