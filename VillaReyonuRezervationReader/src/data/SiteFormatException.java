/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author ISIK
 */
public class SiteFormatException extends Exception {
    public SiteFormatException(String message,String olmasiGereken,String olan) {
        super(message+"-->"+"Olmasi gereken : "+olmasiGereken+" Olan : "+olan);
    }
    public SiteFormatException(String message,int olmasiGereken,int olan) {
        super(message+"-->"+"Olmasi gereken : "+olmasiGereken+" Olan : "+olan);
    }
}
