/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import db.rezervation_table.RezervationTableRestriction;
import bean.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

/**
 *
 * @author ISIK
 */
public abstract class DBConnection {
    
    /**
     *
     * @param res
     * @param order
     * @param limit
     * @return
     */
    public abstract List<RezervationBean> getAllRezervationsByRestriction(RezervationTableRestriction res,OrderBy order,Limit limit);
    /**
     * 
     * @return butun villalari dondurur
     */
    public abstract Set<VillaBean> getAllVilla();

    /**
     * 
     * @param dt  hangi tablonun idsi gerekliyse o
     * @return Kontrol TableBean de en son db eklenen id ve hangi tabloysa tablonun adini ve en son yuklenme tarihini dondurur
     *  ornek en son yuklenen rezervasyon kodunu id olarak alir table name de rezervation table vardir tarihte ise yuklneme tarihi vardir
     */
    public abstract KontrolTableBean getKontrolTableData(TableInfo dt);

    /**
     * 
     * @param kontrolData getKontrolTableData aciklamasini oku bu sefer bu datalari update eder
     */
    public abstract void updateKontrolTableData(KontrolTableBean kontrolData);

    public abstract boolean addRezervation(RezervationBean rezervation);

    /**
     *
     * @param rezervations
     * @return database eklenen rezervasyonlarin rezervasyon kodlari
     */
    public abstract Set<String> addRezervationBean(List<RezervationBean> rezervations);
    protected static LocalDateTime toLocalDateTime(long millis) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), TimeZone.getDefault().toZoneId());
    }

    protected static Long toMillis(LocalDateTime dt) {
        return dt.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
}
