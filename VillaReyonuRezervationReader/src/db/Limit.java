/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

/**
 *
 * @author ISIK
 */
public class Limit {
    private int start;
    private int count;

    public Limit(int start, int count) {
        this.start = start;
        this.count = count;
    }

    public Limit() {
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
    public String toSqlString(){
        return "LIMIT "+start+","+count;
    }
}
