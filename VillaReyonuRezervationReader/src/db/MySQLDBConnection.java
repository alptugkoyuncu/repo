/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import bean.KontrolTableBean;
import db.rezervation_table.RezervationTableRestriction;
import bean.RezervationBean;
import bean.VillaBean;
import com.mysql.cj.jdbc.MysqlDataSource;
import db.rezervation_table.RezervationTableColumn;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ISIK
 */
public class MySQLDBConnection extends DBConnection {

    private final static String URL = "jdbc:mysql://localhost:3306/villa_rezervasyon?useUnicode=true&characterEncoding=utf8";
    private final static String USER_NAME = "root";
    private final static String PASSWORD = "19791981";
    private final MysqlDataSource dataSource;

    public MySQLDBConnection() {
        dataSource = new MysqlDataSource();
        dataSource.setURL(URL);
        dataSource.setUser(USER_NAME);
        dataSource.setPassword(PASSWORD);
    }

    

    

    @Override
    public Set<VillaBean> getAllVilla() {
        Set<VillaBean> villaSet = new TreeSet<>();
        String query = "SELECT `villa`.`name` "
                + "FROM `villa_rezervasyon`.`villa` ";

        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(query);
            while (set.next()) {
                villaSet.add(new VillaBean(set.getString(1)));
            }

        } catch (SQLException ex) {
            System.err.println("Failed to retreive datas form villa table villa_rezervasyon query = " + query);
            Logger.getLogger(MySQLDBConnection.class
                    .getName()).log(Level.SEVERE, null, ex);

        } finally {
            return villaSet;
        }
    }

    @Override
    public KontrolTableBean getKontrolTableData(TableInfo table) {
        KontrolTableBean data=null;
        String query = "SELECT `kontroltable`.`lastUpdateId`,`kontroltable`.`update_date` "
                + "FROM `villa_rezervasyon`.`kontroltable` "
                + "where `kontroltable`.`tableName` = '" + table.getTableName() + "'";

        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(query);
            if (set.next()) {
                String id = set.getString(1);
                LocalDateTime dt = DBConnection.toLocalDateTime(set.getLong(2));
                data= new KontrolTableBean(id,table.getTableName(),dt);
            } else {
                data= null;
            }

        } catch (SQLException ex) {
            System.err.println("Failed to retreive datas form kontroltable of villa_rezervasyon query = " + query);
            Logger.getLogger(MySQLDBConnection.class
                    .getName()).log(Level.SEVERE, null, ex);
            data = null;

        }finally{
            return data;
        }
    }

    @Override
    public boolean addRezervation(RezervationBean rezervation) {
        List<RezervationBean> list = new ArrayList<>();
        Set<String> rcodes = this.addRezervationBean(list);
        if (rcodes == null || rcodes.isEmpty()) {
            return false;
        }
        Iterator<String> it = rcodes.iterator();
        return rezervation.getCode().equalsIgnoreCase(it.next());
    }

    @Override
    public Set<String> addRezervationBean(List<RezervationBean> rezervations) {

        Set<String> rcodes = new HashSet<>();

        System.out.println("Try to insert Database(rezervation) size : " + rezervations.size());
        String sql = "INSERT IGNORE INTO villa_rezervasyon.rezervation "
                + "(code,"
                + "rezervationDate,"
                + "checkin,"
                + "checkout,"
                + "prepayment,"
                + "payment,"
                + "villa_name,"
                + "customer_name,"
                + "customer_tc) VALUES(?,?,?,?,?,?,?,?,?)";
        int maximumBatch = 100;
        int index = 0;
//        Long t = 0l;
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(sql); java.sql.Statement st = conn.createStatement();) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            for (RezervationBean r : rezervations) {
                rcodes.add(r.getCode());
                pstm.setString(1, r.getCode());
                pstm.setLong(2, r.getRezervationDate().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
                pstm.setLong(3, r.getEntranceDate().atTime(10, 0).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
                pstm.setLong(4, r.getExitDate().atTime(16, 0).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
                pstm.setBigDecimal(5, r.getPrepayment());
                pstm.setBigDecimal(6, r.getPostPayment());
                pstm.setString(7, r.getVillaName());
                pstm.setString(8, r.getCustomerName());
                pstm.setString(9, r.getCustomerTC());
                pstm.addBatch();
                index++;
                if (index == maximumBatch) {
                    index = 0;
                    pstm.executeBatch();
                }
            }

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);
            var rs = st.executeQuery("Select code from villa_rezervasyon.rezervation");
            Set<String> allCodes = new HashSet<>();
            while (rs.next()) {
                allCodes.add(rs.getString(1));
            }
            rcodes.retainAll(allCodes);
            System.out.println("Insert comleted to Database(villa_rezervasyon.rezervation):" + rcodes.size());

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("Insert failed on Database(villa_rezervasyon.rezervation");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return rcodes;
        }

    }

    @Override
    public void updateKontrolTableData(KontrolTableBean dt) {
        String query = "REPLACE INTO `villa_rezervasyon`.`kontroltable`(`lastUpdateId`,`tableName`,`update_date`)"
                + "VALUES(?,?,?)";
        try (Connection conn = dataSource.getConnection(); PreparedStatement pstm = conn.prepareStatement(query);) {
            conn.setAutoCommit(false);
//            t = System.currentTimeMillis();
            pstm.setString(1, dt.getLastUpdateId());
            pstm.setString(2, dt.getTableName());
            pstm.setLong(3, dt.getUpdateDate().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
            pstm.addBatch();

            pstm.executeBatch();
            conn.commit();
//            t = System.currentTimeMillis() - t;
            conn.setAutoCommit(true);

        } catch (SQLException ex) {
//            t = System.currentTimeMillis() - t;
            System.err.println("Insert failed on update last reservation code");
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<RezervationBean> getAllRezervationsByRestriction(RezervationTableRestriction res,OrderBy orderby,Limit limit) {
   List<RezervationBean> list = new ArrayList<>();
        String query = "SELECT "
                + "`rezervation`.`code`,"
                + "    `rezervation`.`rezervationDate`,"
                + "    `rezervation`.`checkin`,"
                + "    `rezervation`.`checkout`,"
                + "    `rezervation`.`prepayment`,"
                + "    `rezervation`.`payment`,"
                + "    `rezervation`.`villa_name`,"
                + "    `rezervation`.`customer_name`,"
                + "    `rezervation`.`customer_tc`"
                + " FROM `villa_rezervasyon`.`rezervation`";
        
        String where = res!=null ? res.getWhereClause():"";
        String order=orderby!=null?orderby.toSqlString() : "";
        String lmt = limit!=null ? limit.toSqlString() : "";
        
        query = query+" "+where+" "+ order+" "+lmt;

        try (Connection conn = dataSource.getConnection(); java.sql.Statement st = conn.createStatement();) {
            ResultSet set = st.executeQuery(query);
            RezervationBean r;
            while (set.next()) {
                r = new RezervationBean();
                r.setCode(set.getString(1));
                r.setRezervationDate(toLocalDateTime(set.getLong(2)));
                r.setEntranceDate(toLocalDateTime(set.getLong(3)).toLocalDate());
                r.setExitDate(toLocalDateTime(set.getLong(4)).toLocalDate());
                r.setPrepayment(set.getBigDecimal(5));
                r.setPostPayment(set.getBigDecimal(6));
                r.setVillaName(set.getString(7));
                r.setCustomerName(set.getString(8));
                r.setCustomerTC(set.getString(9));
                list.add(r);
//                villaSet.add(new VillaBean(set.getString(1)));
            }

        } catch (SQLException ex) {
            System.err.println("Failed to retreive datas form  rezervation table of villa_rezervasyon query = " + query);
            Logger.getLogger(MySQLDBConnection.class
                    .getName()).log(Level.SEVERE, null, ex);

        } finally {
            return list;
        }
    }
}
