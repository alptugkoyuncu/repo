/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

/**
 *
 * @author ISIK
 */
public class OrderBy {

    public enum OrderType {
        ASC, DESC

    }
    private OrderType type;
    private final TableInfo tableColumn;

    public static OrderBy createOrderBy(TableInfo colName) {
        return new OrderBy(colName, OrderType.ASC);
    }
    public static OrderBy createOrderBy(TableInfo colName, OrderType orderType) {
        return new OrderBy(colName, orderType);
    }

    private OrderBy(TableInfo colname, OrderType type) {
        this.type = type;
        this.tableColumn = colname;
    }

    /**
     * 
     * @return "ORDER BY " + this.tableColumn.getTableColumnName() + " " + this.type" seklinde dondurur 
     */
    public String toSqlString() {
        return "ORDER BY " + this.tableColumn.getTableColumnName() + " " + this.type;
    }
}
