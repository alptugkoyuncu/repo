/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.rezervation_table;

import db.TableInfo;

/**
 *
 * @author ISIK
 */
public enum RezervationTableColumn implements TableInfo{
    REZERVATION_CODE("`rezervation`.`code`"),
    REZERVATION_DATE("`rezervation`.`rezervationDate`"),
    CHECK_IN("`rezervation`.`checkin`"),
    CHECK_OUT("`rezervation`.`checkout`"),
    VILLA_NAME("`rezervation`.`villa_name`");

    private final String columnName;
    
    private RezervationTableColumn(String columnName) {
        this.columnName = columnName;
    }

    @Override
    public String getTableName() {
        return "rezervation";
    }

    @Override
    public String getTableColumnName() {
        return columnName;
    }
}
