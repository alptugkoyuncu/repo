/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.rezervation_table;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ISIK
 */
public class RezervationTableRestriction {

    class MinMax {

        Long min, max;

        public MinMax(Long min, Long max) {
            this.max = max;
            this.min = min;
        }

        public String toString(String aralik) {
            if (min == null && max == null) {
                return "";
            }
            if (min != null && max != null) {
                return aralik + " >=" + min + " and " + aralik + " < " + max;
            } else if (min != null) {
                return aralik + " >= " + min;
            } else {
                return aralik + " <" + max;
            }
        }
    }

  
    Map<RezervationTableColumn, Object> restrictionMap = new EnumMap<>(RezervationTableColumn.class);

    public RezervationTableRestriction() {
    }

    /**
     *
     * @param val
     */
    public void addCodeRestriction(String... val) {
        restrictionMap.put(RezervationTableColumn.REZERVATION_CODE, val);
    }

    public void addVillaRestriction(String... val) {
        restrictionMap.put(RezervationTableColumn.VILLA_NAME, val);
    }

    public void addRezervationDateRestriction(Long min, Long max) {
        restrictionMap.put(RezervationTableColumn.REZERVATION_DATE, new MinMax(min, max));
    }

    public void addCheckinRestriction(Long min, Long max) {
        restrictionMap.put(RezervationTableColumn.CHECK_IN, new MinMax(min, max));
    }

    public void addCheckoutRestriction(Long min, Long max) {
        restrictionMap.put(RezervationTableColumn.CHECK_OUT, new MinMax(min, max));
    }

    public String getWhereClause() {
        String where = "";
        if (restrictionMap.containsKey(RezervationTableColumn.VILLA_NAME)) {
            String[] villaString = (String[]) restrictionMap.get(RezervationTableColumn.VILLA_NAME);
            where = "where " + RezervationTableColumn.VILLA_NAME.getTableColumnName()+" ";
            if (villaString.length == 1) {
                 where += "= '" + villaString[0] + "' ";
            } else {
                where += "in ('" + villaString[0] + "' ";
                for (int i = 1; i < villaString.length; i++) {
                    where += ",'" + villaString[i] + "'";
                }
                where += ") ";
            }
        }
        if (restrictionMap.containsKey(RezervationTableColumn.REZERVATION_CODE)) {
            if (where.isBlank()) {
                where = "where ";
            } else {
                where += "and ";
            }
            String[] codeString = (String[]) restrictionMap.get(RezervationTableColumn.REZERVATION_CODE);
            where += RezervationTableColumn.REZERVATION_CODE.getTableColumnName();
            if (codeString.length == 1) {
                where += " = '" + codeString[0] + "' ";
            } else {
                where += " in('" + codeString[0] + "' ";
                for (int i = 1; i < codeString.length; i++) {
                    where += ",'" + codeString[i] + "'";
                }
                where += ") ";
            }
        }
        if (restrictionMap.containsKey(RezervationTableColumn.REZERVATION_DATE)) {

            MinMax minMax = (MinMax) restrictionMap.get(RezervationTableColumn.REZERVATION_DATE);
            String colName = RezervationTableColumn.REZERVATION_DATE.getTableColumnName();
            String minMaxString = minMax.toString(colName);
            if(minMaxString.isBlank()){
                
            }else{
                if (where.isBlank()) {
                    where = "where ";
                } else {
                    where += "and ";
                }
                where +=minMaxString+" ";
            }
        }
        if(restrictionMap.containsKey(RezervationTableColumn.CHECK_IN)){
            MinMax minMax = (MinMax)restrictionMap.get(RezervationTableColumn.CHECK_IN);
            String colName = RezervationTableColumn.CHECK_IN.getTableColumnName();
            String minMaxString = minMax.toString(colName);
            if(minMaxString.isBlank()){
                
            }else{
                if (where.isBlank()) {
                    where = "where ";
                } else {
                    where += "and ";
                }
                where +=minMaxString+" ";
            }
        }
        if(restrictionMap.containsKey(RezervationTableColumn.CHECK_OUT)){
            MinMax minMax = (MinMax)restrictionMap.get(RezervationTableColumn.CHECK_OUT);
            String colName = RezervationTableColumn.CHECK_OUT.getTableColumnName();
            String minMaxString = minMax.toString(colName);
            if(minMaxString.isBlank()){
                
            }else{
                if (where.isBlank()) {
                    where = "where ";
                } else {
                    where += "and ";
                }
                where +=minMaxString+" ";
            }
        }
        return where;
    }
}
