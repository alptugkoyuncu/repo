/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package villareyonurezervationreader;

import bean.KontrolTableBean;
import bean.RezervationBean;
import data.HtmlAbstractParser;
import data.HtmlRezervationReader;
import db.DBConnection;
import db.MySQLDBConnection;
import db.rezervation_table.RezervationTableColumn;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.text.html.HTML;

/**
 *
 * @author ISIK
 */
public class NewJFrame extends javax.swing.JFrame {

    /**
     * Creates new form NewJFrame
     */
    public NewJFrame() {
        initComponents();
    }

    public void load() {
        DBConnection conn = new MySQLDBConnection();
        Integer site = 1;
        KontrolTableBean dt = conn.getKontrolTableData(RezervationTableColumn.CHECK_IN);
        Integer lastReadCode;
        try {
            lastReadCode = dt != null ? Integer.parseInt(dt.getLastUpdateId()) : 0;
        } catch (NumberFormatException ex) {
            lastReadCode = 0;
        }

        int error = 0;
        int maxError = 50;
        List<RezervationBean> list = new ArrayList<>();
        int lastSuccessfulReadCode = 0;
        int totalRead = 0;
        int totalCedrusReading = 0;
        // 1 saat once kontrol edildiyse etme
        LocalDateTime dt2 = dt.getUpdateDate().plusHours(1);
        while (error < maxError) {
            if (dt2.isAfter(LocalDateTime.now())) {
                this.jLabel1.setText("En son guncelleme 1 saatten once yapilmis");
                return;
            }
            lastReadCode++;
            try {
                this.jLabel1.setText(lastReadCode + " reading");
                HtmlAbstractParser parser = new HtmlRezervationReader(site, lastReadCode).readData();
                parser.checkValid();
                if (parser.getVIllaName().contains("Cedrus")) {
                    System.out.println(parser.getRezervationCode() + " : " + parser.getRezervationDate());
                    list.add(parser.getRezervationBean());
                    totalCedrusReading++;
                }
                totalRead++;
                lastSuccessfulReadCode = lastReadCode;
                error = 0;
            } catch (Exception ex) {
                this.jLabel1.setText(ex.getMessage());
                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
                error++;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (lastSuccessfulReadCode != 0) {
            dt.setLastUpdateId(lastSuccessfulReadCode + "");
        }
        dt.setUpdateDate(LocalDateTime.now());
        conn.updateKontrolTableData(dt);

        this.jLabel1.setText("last reading code is " + lastReadCode);
        this.jLabel2.setText("Total Cedrus reading = " + totalCedrusReading);
        this.jLabel3.setText(" Total reading  = " + totalRead);
        conn.addRezervationBean(list);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jProgressBar1 = new javax.swing.JProgressBar();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Connecting the site");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText(" ");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText(" ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        final NewJFrame updateFrame = new NewJFrame();
        updateFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                updateFrame.setVisible(true);
                updateFrame.load();
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
            }
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JProgressBar jProgressBar1;
    // End of variables declaration//GEN-END:variables

}
