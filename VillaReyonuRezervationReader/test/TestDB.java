
import bean.VillaBean;
import db.DBConnection;
import db.Limit;
import db.MySQLDBConnection;
import db.OrderBy;
import db.rezervation_table.RezervationTableColumn;
import db.rezervation_table.RezervationTableRestriction;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.List;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ISIK
 */
public class TestDB {
    public static void main(String[] args) {
        DBConnection db = new MySQLDBConnection();
//        String code="12347";
//        System.out.println("Setting last rezervation code : "+code);
//        updateKontrolTableData(db, code);
//        System.out.println("Getting last rezervationCOde : " + getKontrolTableData(db));

//        System.out.println("All villas : " + getVillaNames(db));
    LocalDate baslangic = LocalDate.of(2022,5, 1);
    LocalDate bitis = LocalDate.of(2022, 5,12);
    String villaName = "Villa Cedrus 5";
    long bas = baslangic.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    long son = bitis.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        RezervationTableRestriction res = new RezervationTableRestriction();
//        res.addCheckinRestriction(bas, son);
//        res.addCheckoutRestriction(0L,System.currentTimeMillis());
//        res.addRezervationDateRestriction(0L, System.currentTimeMillis());
        res.addVillaRestriction("Villa Cedrus 4");
//        res.addCodeRestriction("9073");
    Limit lim = new Limit(0,5);
    List l = db.getAllRezervationsByRestriction(res,OrderBy.createOrderBy(RezervationTableColumn.REZERVATION_CODE),lim);
    System.out.println("Size = " + l.size()+" Rezervation to "+baslangic +" : "+l);
//    System.out.println("Total rezervation from "+baslangic +" : "+db.getAllRezervationListByVillaAndCheckIn(villaName, baslangic,bitis,DBConnection.ORDER_BY.REZERVATION_TABLE_CHECKIN.setType(DBConnection.ORDER_BY.Type.DSC)).size());
    }
    public static void setLastRezervationCode(DBConnection conn,String code){
        conn.updateKontrolTableData(code);
    }
    public static String getLastRezervationCode(DBConnection conn){
        return conn.getKontrolTableData();
    }
    public static Set<VillaBean> getVillaNames(DBConnection conn){
        return conn.getAllVilla();
    }
    
}
