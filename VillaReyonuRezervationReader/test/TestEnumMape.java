
import java.util.EnumMap;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ISIK
 */
public class TestEnumMape {
     public enum TableColumns{
        REZERVATION_TABLE_CODE("`rezervation`.`code`"),
        REZERVATION_TABLE_REZERVATION_DATE("`rezervation`.`rezervationDate`"),
        REZERVATION_TABLE_CHECKIN("`rezervation`.`checkin`"),
        REZERVATION_TABLE_CHECKOUT("`rezervation`.`checkout`"),
        REZERVATION_TABLE_VILLA_NAME("`rezervation`.`villa_name`");
        
        private final String tableName;
        
        private TableColumns(String tableName){
            this.tableName = tableName;
        }
        public String getTableName() {
            return tableName;
        }
    }
    public static void main(String[] args) {
        Map<TableColumns,Integer> map = new EnumMap<>(TableColumns.class);
        map.put(TableColumns.REZERVATION_TABLE_CODE, 2);
        System.out.println(map.containsKey(TableColumns.REZERVATION_TABLE_CHECKIN));
        System.out.println(map.get(TableColumns.REZERVATION_TABLE_CHECKIN));
    }
}
