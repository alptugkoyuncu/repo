
import db.rezervation_table.RezervationTableRestriction;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ISIK
 */
public class TestRezervationTableRestriction {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        RezervationTableRestriction res = new RezervationTableRestriction();
        System.out.println(res.getWhereClause());
        res.addCodeRestriction("1003","1001");
        System.out.println(res.getWhereClause());
        res.addVillaRestriction("villa 1","villaa2");
        System.out.println(res.getWhereClause());
        res.addCheckinRestriction(null,100L);
        System.out.println(res.getWhereClause());
    }
    
}
