/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data;

import db.DatabaseOperationResult;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author alptug
 */

public abstract class DatabaseObject implements Comparable<DatabaseObject>{
    public static List<Gubre> listGubre = new ArrayList<Gubre>();
    public static List<GubreIcerik> listGubreIcerik = new ArrayList<GubreIcerik>();
    public static List<GubreSinifi> listGubreSinif = new ArrayList<GubreSinifi>();
    public static List<Sera> listSera = new ArrayList<Sera>();
    public static List<Sulama> listSulama  =  new ArrayList<Sulama>();
    
    private int id;
    private boolean changed;
    
/**
 * 
 * @return Database işlem sonucunu döndürür DataabaseOperatıonResult enumunu döndürür 
 *
 */
    public abstract DatabaseOperationResult insert();
    /**
 * 
 * @return Database işlem sonucunu döndürür DataabaseOperatıonResult enumunu döndürür 
 */
    public abstract DatabaseOperationResult update();
   /**
 * 
 * @return Database işlem sonucunu döndürür DataabaseOperatıonResult enumunu döndürür 
 */
    public abstract DatabaseOperationResult delete();
    
    /**
     *
     * @return
     */
    public boolean isChanged() {
        return changed;
    }
    protected void setChanged(boolean val){
        changed=val;
    }
    /**
     *
     * @param commit
     */
    public void commit() {
        changed=false;
    }
    
    /**
     *
     * @param id
     */
    public DatabaseObject(int id) {
        this.id = id;
        changed=false;
    }

    /**
     *
     */
    public DatabaseObject() {
    }

    /**
     *
     * @return
     */
    public abstract String getTableName();

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "table name:"+getTableName()+","
                + "id:"+id;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof DatabaseObject){
            DatabaseObject d = (DatabaseObject)obj;
            return d.getTableName().equalsIgnoreCase(this.getTableName()) && d.getId()==this.getId();
        }
        return false;
    }

    @Override
    public int compareTo(DatabaseObject o) {
        int cmp=this.getTableName().compareTo(o.getTableName());
        if(cmp!=0)
            return cmp;
        if(this.id==o.getId())
            return 0;
        if(this.id>o.getId())
            return 1;
        return -1;
    }

    /**
     *
     * @param l
     * @param id
     * @return
     */
    public static DatabaseObject findByDbId(final List<? extends DatabaseObject> l,int id){
        DatabaseObject obj = new DatabaseObject(id) {
            @Override
            public String getTableName() {
                return l.get(0).getTableName();
            }

            @Override
            public DatabaseOperationResult insert() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public DatabaseOperationResult update() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public DatabaseOperationResult delete() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            
        };
        int index=Collections.binarySearch(l, obj);
        return index>=0 ?  l.get(index):null; 
        
    }
}
