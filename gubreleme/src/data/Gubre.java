package data;

import db.DatabaseConnection;
import db.DatabaseOperationResult;
import db.DatabaseOperationResultList;
import java.util.Map;
import java.util.TreeMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class Gubre extends DatabaseObject {

    private String name;
    private String kisaltma;
    private GubreSinifi sinifi;
    private Map<GubreIcerik, Double> icerik;
    boolean aktif;

    public boolean isAktif() {
        return aktif;
    }

    public void setAktif(boolean aktif) {
        this.aktif = aktif;
        this.setChanged(true);
        
    }

    public Gubre() {
        super(-1);
        icerik = new TreeMap<GubreIcerik, Double>();
        name=null;
        kisaltma=null;
        sinifi=null;
        aktif=false;
    }

    public static Gubre createNewGubre(int dbId,String name, String kisaltma, GubreSinifi sinif, boolean aktif) throws Exception {
       Gubre g = new Gubre();
       g.setId(dbId);
       g.setName(name);
       g.setKisaltma(kisaltma);
       g.setSinifi(sinif);
       g.setAktif(aktif);
       g.setChanged(false);
        return g;
    }

    @Override
    public String toString() {
        return "{" + super.toString() + ", Ad:" + this.getName() + ", Kisaltma:" + this.getKisaltma() + ", Sinifi:" + this.getSinifi() + "}";
    }

    public void addIcerik(GubreIcerik gubreIcerik, Double value) {
        icerik.put(gubreIcerik, value);
    }

    public Double removeIcerik(GubreIcerik gubreIcerik) {
        return icerik.remove(gubreIcerik);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws Exception {
        String n = name == null ? "" : name.trim();
        if (n.isEmpty()) {
            throw new Exception("İsim kısmı boş bırakılamaz");
        }
        for (Gubre g : listGubre) {
            if(g==this)
                continue;
            if (g.getName().equalsIgnoreCase(n)) {
                throw new Exception("Bu isimde kayıtlı başka bir gübre var");
            }
        }
        this.name=n;
    }

    public String getKisaltma() {

        return kisaltma;
    }

    public void setKisaltma(String kisaltma) {
        String n = kisaltma == null ? "" : kisaltma.trim();
        this.kisaltma = n;
    }

    public GubreSinifi getSinifi() {
        return sinifi;
    }

    public void setSinifi(GubreSinifi sinifi) throws Exception {
        if (sinifi == null) {
            throw new Exception("Gübre Sınıfı boş bırakılamaz");
        }
        boolean flag=false;
        for(GubreSinifi gs:listGubreSinif){
            if(gs.getId()==sinifi.getId()){
                flag=true;
                break;
            }
        }
        if(!flag){
            throw new Exception("Sinif degeri database de kayitli olmak zorundadir");
        }
        this.sinifi = sinifi;
    }

    public Map<GubreIcerik, Double> getIcerik() {
        return icerik;
    }

    public void setIcerik(Map<GubreIcerik, Double> icerik) {
        this.icerik = icerik;
    }

    @Override
    public String getTableName() {
        return "gubre";
    }

    @Override
    public DatabaseOperationResult insert() {
        DatabaseOperationResultList.clear();
        if(!this.isChanged())
            return DatabaseOperationResult.NoOperation;
        DatabaseOperationResult opt = DatabaseConnection.insertGubre(this);
        if (opt != DatabaseOperationResult.Error) {
            this.commit();
        }
        return opt;
    }

    @Override
    public DatabaseOperationResult update() {
        DatabaseOperationResultList.clear();
        if(!this.isChanged())
            return DatabaseOperationResult.NoOperation;
        DatabaseOperationResult opt = DatabaseConnection.insertGubre(this);
        if (opt != DatabaseOperationResult.Error) {
            this.commit();
        }
        return opt;
    }

    @Override
    public DatabaseOperationResult delete() {
        DatabaseOperationResultList.clear();
        DatabaseOperationResult opt = DatabaseConnection.deleteGubre(this);
        if (opt != DatabaseOperationResult.Error||opt!=DatabaseOperationResult.NoOperation) {
            listGubre.remove(this);
        }
        return opt;
    }

    public void clearIcerik() {
        this.icerik.clear();
    }
}
