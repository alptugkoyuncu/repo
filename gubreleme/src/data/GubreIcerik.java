package data;

import db.DatabaseConnection;
import db.DatabaseOperationResult;
import db.DatabaseOperationResultList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class GubreIcerik extends DatabaseObject {

    private String icerikAdi;
    private String molekulAdi;
    private GubreIcerik parent;
    private int parentId;
    public GubreIcerik(){
        super(-1);
        this.parent=null;
        this.icerikAdi=null;
        this.molekulAdi=null;
    }
    public GubreIcerik(int dbId, String icerikAdi, String molekulAdi, Integer parentId) {
        super(dbId);
        this.setParentId(parentId);
        this.icerikAdi = icerikAdi;
        this.molekulAdi = molekulAdi;
    }

    public boolean isParent() {
        return this.getParent()==null;
    }

    public GubreIcerik getParent() {
        return (GubreIcerik) DatabaseObject.findByDbId(listGubreIcerik, parentId);
    }
    public int getParentId(){
        return this.parentId;
    }
    public void setParentId(Integer parentId) {
        this.parentId=parentId; 
    }

    public String getIcerikAdi() {
        return icerikAdi;
    }

    public void setIcerikAdi(String icerikAdi) {
        this.icerikAdi = icerikAdi;
    }

    public String getMolekulAdi() {
        return molekulAdi;
    }

    public void setMolekulAdi(String molekulAdi) {
        this.molekulAdi = molekulAdi;
    }

    @Override
    public String getTableName() {
        return "gubre_icerik";
    }

    @Override
    public DatabaseOperationResult insert() 
    {
        DatabaseOperationResultList.clear();
//        if (!this.isChanged()) {
//            return DatabaseOperationResult.NoOperation;
//        }
        
        DatabaseOperationResult opt = DatabaseConnection.insertGubreIcerik(this);
        if (opt != DatabaseOperationResult.Error) {
            this.commit();
        }
        return opt;
    }

    @Override
    public DatabaseOperationResult update() {
        DatabaseOperationResultList.clear();
//        if (!this.isChanged()) {
//            return DatabaseOperationResult.NoOperation;
//        }
        DatabaseOperationResult opt = DatabaseConnection.updateGubreIcerik(this);

        if (opt != DatabaseOperationResult.Error) {
            this.commit();
        }
        return opt;
    }

    @Override
    public DatabaseOperationResult delete() {
        DatabaseOperationResultList.clear();
        DatabaseOperationResult opt = DatabaseConnection.deleteGubreIcerik(this);
        if (opt != DatabaseOperationResult.Error || opt != DatabaseOperationResult.NoOperation) {
            listGubreIcerik.remove(this);
        }
        return opt;
    }

    public String toString() {
        return "{"+super.toString() + ", Icerik Adi: "+icerikAdi+", Molekul Adi: "+molekulAdi+", Parent :"+this.getParent()+"}"; //To change body of generated methods, choose Tools | Templates.
    }
}
