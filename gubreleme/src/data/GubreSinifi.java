package data;

import db.DatabaseOperationResult;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alptug
 */
public class GubreSinifi extends DatabaseObject{
    private String name;
    private String kisaltma;

    public GubreSinifi(int dbId, String name, String kisaltma) {
        super(dbId);
        this.name = name;
        this.kisaltma = kisaltma;
    }

    public String getKisaltma() {
        return kisaltma;
    }

    public void setKisaltma(String kisaltma) {
        this.kisaltma = kisaltma;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        return super.toString()+",Name:"+name+",kisaltma:"+kisaltma;
    }
    @Override
    public String getTableName() {
        return "gubre_sinifi";
    }

    @Override
    public DatabaseOperationResult insert() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DatabaseOperationResult update() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DatabaseOperationResult delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
