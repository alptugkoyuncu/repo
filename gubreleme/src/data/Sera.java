/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import db.DatabaseOperationResult;

/**
 *
 * @author alptug
 */
public class Sera extends DatabaseObject{
    private String adi;
    private String kisaltma;
    private int alan;

    public static Sera createNewSera(int dbId, String adi, String kisaltma, int alan) {
        Sera s = new Sera();
        s.setId(dbId);
        s.adi = adi;
        s.kisaltma = kisaltma;
        s.alan = alan;
        return s;
    }
    public Sera(){
        super(-1);
        adi=kisaltma=null;
        alan=0;
    }
  

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }

    public String getKisaltma() {
        return kisaltma;
    }

    public void setKisaltma(String kisaltma) {
        this.kisaltma = kisaltma;
    }

    public int getAlan() {
        return alan;
    }

    public void setAlan(int alan) {
        this.alan = alan;
    }

    @Override
    public String getTableName() {
        return "sera";
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public DatabaseOperationResult insert() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DatabaseOperationResult update() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DatabaseOperationResult delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
