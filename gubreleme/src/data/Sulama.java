/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import db.DatabaseConnection;
import db.DatabaseOperationResult;
import db.DatabaseOperationResultList;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author alptug
 */
public class Sulama extends DatabaseObject {

    java.util.Date dt;
    Sera sera;
    int sulam_suresi;//dk olarak
    Map<Gubre, Double> gubreMiktari;
    Double toplmaGubreKilosu;

    public Double getToplmaGubreKilosu() {
        return toplmaGubreKilosu;
    }

    public void setToplmaGubreKilosu(Double toplmaGubreKilosu) {
        this.toplmaGubreKilosu = toplmaGubreKilosu;
    }

    public java.util.Date getDt() {
        return dt;
    }

    public Sera getSera() {
        return sera;
    }

    public int getSulam_suresi() {
        return sulam_suresi;
    }

    public Map<Gubre, Double> getGubreMiktari() {
        return gubreMiktari;
    }

    public void setDt(Date dt) {
        this.dt = dt;
    }

    public void setSera(Sera sera) {
        this.sera = sera;
    }

    public void setSulam_suresi(int sulam_suresi) {
        this.sulam_suresi = sulam_suresi;
    }

    public void setGubreMiktari(Map<Gubre, Double> gubreMiktari) {
        this.gubreMiktari = gubreMiktari;
    }

    public Sulama() {
        super(-1);
        dt = null;
        sera = null;
        sulam_suresi = 0;
        gubreMiktari = null;
        toplmaGubreKilosu = 0.0;
    }

    public static Sulama createNewSulama(int id, Date dt, Sera sera, int sulam_suresi, Map<Gubre, Double> gubreMiktari, Double toplamGubreKilosu) {
        Sulama s = new Sulama();
        s.setId(id);
        s.dt = dt;
        s.sera = sera;
        s.sulam_suresi = sulam_suresi;
        s.gubreMiktari = gubreMiktari;
        s.toplmaGubreKilosu = toplamGubreKilosu;
        return s;
    }

    public static Sulama createNewSulama(int id, Date dt, int seraId, int sulam_suresi, Map<Gubre, Double> gubreMiktari, Double toplamGubreKilosu) {
        Sera s = (Sera) Sera.findByDbId(listSera, seraId);
        return Sulama.createNewSulama(id, dt, s, sulam_suresi, gubreMiktari, toplamGubreKilosu);
    }

    @Override
    public DatabaseOperationResult insert() {
        DatabaseOperationResultList.clear();
//        if (!this.isChanged()) {
//            return DatabaseOperationResult.NoOperation;
//        }

        DatabaseOperationResult opt = DatabaseConnection.insertSulama(this);
        if (opt != DatabaseOperationResult.Error) {
            this.commit();
        }
        return opt;
    }

    @Override
    public DatabaseOperationResult update() {
  DatabaseOperationResultList.clear();
//        if (!this.isChanged()) {

//            return DatabaseOperationResult.NoOperation;
//        }
        DatabaseOperationResult opt = DatabaseConnection.updateSulama(this);
        if (opt != DatabaseOperationResult.Error) {
            this.commit();
        }
        return opt;
    }

    @Override
    public DatabaseOperationResult delete() {
        DatabaseOperationResultList.clear();
//        if (!this.isChanged()) {

//            return DatabaseOperationResult.NoOperation;
//        }
        DatabaseOperationResult opt = DatabaseConnection.deleteSulama(this);
        if (opt != DatabaseOperationResult.Error) {
            this.commit();
        }
        return opt;
    }

    @Override
    public String getTableName() {
        return "sulama";
    }

    @Override
    public int compareTo(DatabaseObject o) {
        Sulama s = (Sulama) o;
        return this.dt.compareTo(s.getDt());
    }
}
