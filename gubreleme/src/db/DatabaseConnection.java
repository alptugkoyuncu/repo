package db;

import data.DatabaseObject;
import static data.DatabaseObject.listGubre;
import static data.DatabaseObject.listGubreIcerik;
import static data.DatabaseObject.listGubreSinif;
import static data.DatabaseObject.listSera;
import static data.DatabaseObject.listSulama;
import data.GubreSinifi;
import data.Gubre;
import data.GubreIcerik;
import data.Sera;
import data.Sulama;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class DatabaseConnection {

    private static Connection conn;
    private static boolean autoConnection = false;

    public static void setAutoConnection(boolean autoConnection) {
        DatabaseConnection.autoConnection = autoConnection;
    }

    static {
        conn = null;
        autoConnection = false;
//        LoadGubreBilgileri();
    }

    public static void connect() {
        if (conn != null) {
            return;
        }
        try {
            // db parameters
            String url = "jdbc:sqlite:C:/Users/alptu/Google Drive/gubreleme.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public static void disconnect() {
        if (conn != null) {
            try {
                conn.close();
                System.out.println("Connection closed");
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                conn = null;
            }
        }

    }

    public static DatabaseOperationResult deleteGubreIcerikOnGubre(GubreIcerik gi) {
        String query = "DELETE FROM gubre_gubre_icerik_ilişkisi\n"
                + "      WHERE gubre_icerik_id =  " + gi.getId();
        if (autoConnection) {
            connect();
        }
        DatabaseOperationResult bo = DatabaseOperationResult.BeforeDelete;
        bo.setAciklama(query);
        DatabaseOperationResultList.add(bo);
        DatabaseOperationResult af = null;
        try (Statement st = conn.createStatement()) {
            st.execute(query);
            af = DatabaseOperationResult.DeleteComplete;
        } catch (SQLException ex) {
            af = DatabaseOperationResult.Error;
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (autoConnection) {
                disconnect();
            }
            if (af != null) {
                af.setAciklama(query);
            }
            DatabaseOperationResultList.add(af);
            return af;
        }
    }

    public static DatabaseOperationResult deleteGubreIcerikOnGubre(Gubre g) {
        String query = "DELETE FROM gubre_gubre_icerik_ilişkisi\n"
                + "      WHERE gubre_id =  " + g.getId();
        if (autoConnection) {
            connect();
        }
        DatabaseOperationResult bo = DatabaseOperationResult.BeforeDelete;
        bo.setAciklama(query);
        DatabaseOperationResultList.add(bo);
        DatabaseOperationResult af = null;
        try (Statement st = conn.createStatement()) {
            st.execute(query);
            af = DatabaseOperationResult.DeleteComplete;
        } catch (SQLException ex) {
            af = DatabaseOperationResult.Error;
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (autoConnection) {
                disconnect();
            }
            if (af != null) {
                af.setAciklama(query);
            }
            DatabaseOperationResultList.add(af);
            return af;
        }
    }

    public static DatabaseOperationResult deleteGubre(Gubre g) {
        //gubre gubre icerik listesindeki gubreler temizlensin
        DatabaseOperationResult dor = deleteGubreIcerikOnGubre(g);
        DatabaseOperationResultList.add(dor);
        if (dor == DatabaseOperationResult.Error) {
            return dor;
        }
        DatabaseOperationResult r = DatabaseOperationResult.BeforeDelete;

        String query = "DELETE FROM gubre\n"
                + "      WHERE id = " + g.getId();
        System.out.println(query);
        if (autoConnection) {
            connect();
        }
        r.setAciklama(query);
        DatabaseOperationResultList.add(r);
        DatabaseOperationResult r2 = null;
        try (Statement st = conn.createStatement()) {
            st.execute(query);
            r2 = DatabaseOperationResult.DeleteComplete;
        } catch (SQLException ex) {
            r2 = DatabaseOperationResult.Error;
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            if (r2 != null) {
                r2.setAciklama(query);
            }
            if (autoConnection) {
                disconnect();
            }
            DatabaseOperationResultList.add(r2);
            return r2;
        }
    }

    public static DatabaseOperationResult insertGubreIcerikOnGubre(Gubre g) {
        DatabaseOperationResult or = DatabaseOperationResult.BeforeInsert;
        DatabaseOperationResultList.add(or);
        if (g == null || g.getIcerik() == null || g.getIcerik().isEmpty() || g.getId() <= 0) {
            DatabaseOperationResult ar = DatabaseOperationResult.NoOperation;
            DatabaseOperationResultList.add(ar);
            return ar;
        }

        Map<GubreIcerik, Double> icerikmap = g.getIcerik();
        int id = g.getId();
        int i = 0;
        DatabaseOperationResultList.add(deleteGubreIcerikOnGubre(g));
        String guery = "INSERT INTO gubre_gubre_icerik_ilişkisi (\n"
                + "                                            gubre_id,\n"
                + "                                            gubre_icerik_id,\n"
                + "                                            oran\n"
                + "                                        )\n"
                + "                                        VALUES (?,?,?) ";
        DatabaseOperationResult l;
        if (autoConnection) {
            connect();
        }
        try (PreparedStatement st = conn.prepareStatement(guery)) {
            for (GubreIcerik gi : icerikmap.keySet()) {

                st.setInt(1, id);
                st.setInt(2, gi.getId());
                st.setDouble(3, icerikmap.get(gi));
                st.addBatch();
            }
            st.executeBatch();
            l = DatabaseOperationResult.InsertComplete;
            DatabaseOperationResultList.add(l);
            return l;
        } catch (SQLException ex) {
            l = DatabaseOperationResult.Error;
            DatabaseOperationResultList.add(l);
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
            return l;
        } finally {
            if (autoConnection) {
                disconnect();
            }
        }
    }

    public static DatabaseOperationResult insertGubreIcerik(GubreIcerik gi) {
        DatabaseOperationResult bef = DatabaseOperationResult.BeforeInsert;
        DatabaseOperationResultList.add(bef);
        DatabaseOperationResult af = null;
        if (gi == null) {
            af = DatabaseOperationResult.NoOperation;
            DatabaseOperationResultList.add(af);
            return af;
        }
        String query = "INSERT INTO gubre_icerik (\n"
                + "                             icerik_adi,\n"
                + "                             molekul_adi,\n"
                + "                             parent_id\n"
                + "                         )\n"
                + "                         VALUES (?,?,?)";
        if (autoConnection) {
            connect();
        }
        try (PreparedStatement pst = conn.prepareStatement(query)) {
            pst.setString(1, gi.getIcerikAdi());
            pst.setString(2, gi.getMolekulAdi());
            if (gi.getParent() == null) {
                pst.setNull(3, java.sql.Types.NULL);
            } else {
                pst.setInt(3, gi.getParent().getId());
            }
            if (pst.executeUpdate() > 0) {
                af = DatabaseOperationResult.InsertComplete;
            } else {
                af = DatabaseOperationResult.NoOperation;
            }
            DatabaseOperationResultList.add(af);
        } catch (SQLException ex) {
            if (autoConnection) {
                disconnect();
            }
            af = DatabaseOperationResult.Error;
            DatabaseOperationResultList.add(af);
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);

            return af;
        }
        bef = af;
        try (Statement st = conn.createStatement(); ResultSet rs = st.executeQuery("select max(id) from gubre_icerik")) {
            if (rs.next()) {
                gi.setId(rs.getInt(1));
                af = DatabaseOperationResult.OperationComplete;
            } else {
                af = DatabaseOperationResult.NoOperation;
            }
        } catch (SQLException ex) {
            af = DatabaseOperationResult.Error;
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (autoConnection) {
                disconnect();
            }
            DatabaseOperationResultList.add(af);
            return af;
        }
    }

    public static DatabaseOperationResult updateGubreIcerik(GubreIcerik gi) {
        DatabaseOperationResult br = DatabaseOperationResult.BeforeUpdate;
        DatabaseOperationResultList.add(br);
        DatabaseOperationResult ar = null;
        if (gi == null) {
            ar = DatabaseOperationResult.NoOperation;
            DatabaseOperationResultList.add(ar);
            return ar;
        }
        String query = "UPDATE gubre_icerik\n"
                + "   SET "
                + "       icerik_adi = ?,\n"
                + "       molekul_adi = ?,\n"
                + "       parent_id = ? \n"
                + " WHERE id = ? \n";
        if (autoConnection) {
            connect();
        }
        try (PreparedStatement pst = conn.prepareStatement(query)) {
            pst.setString(1, gi.getIcerikAdi());
            pst.setString(2, gi.getMolekulAdi());
            if (gi.getParent() == null) {
                pst.setNull(3, java.sql.Types.NULL);
            } else {
                pst.setInt(3, gi.getParent().getId());
            }
            pst.setInt(4, gi.getId());
            if (pst.executeUpdate() > 0) {
                ar = DatabaseOperationResult.UpdateComplete;
            } else {
                ar = DatabaseOperationResult.NoOperation;
            }
        } catch (SQLException ex) {
            ar = DatabaseOperationResult.Error;
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            DatabaseOperationResultList.add(ar);
            if (autoConnection) {
                disconnect();
            }
            return ar;
        }
    }

    public static DatabaseOperationResult deleteGubreIcerik(GubreIcerik gi) {
        DatabaseOperationResult dor = deleteGubreIcerikOnGubre(gi);
        DatabaseOperationResultList.add(dor);
        if (dor == DatabaseOperationResult.Error) {
            return dor;
        }
        DatabaseOperationResult r = DatabaseOperationResult.BeforeUpdate;

        String uquery = "UPDATE gubre_icerik SET parent_id=NULL WHERE parent_id=" + gi.getId();
        r.setAciklama(uquery);
        DatabaseOperationResultList.add(r);
        if (autoConnection) {
            connect();
        }
        try (Statement st = conn.createStatement()) {
            st.execute(uquery);
            r = DatabaseOperationResult.DeleteComplete;
        } catch (SQLException ex) {
            r = DatabaseOperationResult.Error;
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            if (r != null) {
                r.setAciklama(uquery);
            }
            DatabaseOperationResultList.add(r);

        }
        String query = "DELETE FROM gubre_icerik\n"
                + "      WHERE id = " + gi.getId();
        System.out.println(query);

        r.setAciklama(query);
        DatabaseOperationResultList.add(r);
        DatabaseOperationResult r2 = null;
        try (Statement st = conn.createStatement()) {
            st.execute(query);
            r2 = DatabaseOperationResult.DeleteComplete;
        } catch (SQLException ex) {
            r2 = DatabaseOperationResult.Error;
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            if (r2 != null) {
                r2.setAciklama(query);
            }
            if (autoConnection) {
                disconnect();
            }
            DatabaseOperationResultList.add(r2);
            return r2;
        }
    }

    private static boolean loadSera() {
        if (autoConnection) {
            connect();
        }
        String query = "SELECT id,"
                + "       adi,"
                + "       kisaltma,"
                + "       alan"
                + "  FROM sera;";
        Sera s;
        try (Statement st = conn.createStatement(); ResultSet rs = st.executeQuery(query)) {
            listSera.clear();
            while (rs.next()) {
                try {
                    s = Sera.createNewSera(rs.getInt("id"), rs.getString("adi"), rs.getString("kisaltma"), rs.getInt("alan"));
                    listSera.add(s);
                } catch (Exception ex) {
                    System.err.println(ex.getMessage());
                    Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (listSera != null) {
                Collections.sort(listSera);
            }
            if (autoConnection) {
                disconnect();
            }
        }
        return true;
    }

    public static DatabaseOperationResult insertSulamaGubreleri(Map<Gubre, Double> gubreMiktari, int sulamaId) {
        if (gubreMiktari == null || gubreMiktari.isEmpty()) {
            DatabaseOperationResult opt = DatabaseOperationResult.OperationComplete;
            DatabaseOperationResultList.add(opt);
            return opt;
        }
        String query = "INSERT INTO sulama_gubre_iliskisi ("
                + "                                      gubreId,"
                + "                                      miktar,"
                + "                                      sulamaId"
                + "                                  )"
                + "                                  VALUES (?,?,?);";

        DatabaseOperationResult opt = DatabaseOperationResult.BeforeInsert;
        DatabaseOperationResultList.add(opt);
        if (autoConnection) {
            connect();
        }
        try (PreparedStatement st = conn.prepareStatement(query)) {
            st.getConnection().setAutoCommit(false);
            for (Gubre g : gubreMiktari.keySet()) {
                Double m = gubreMiktari.get(g);
                if (m == null || m <= 0) {
                    continue;
                }
                st.setInt(1, g.getId());
                st.setDouble(2, m);
                st.setInt(3, sulamaId);
                st.addBatch();
            }
            st.executeBatch();
            st.getConnection().commit();
            opt = DatabaseOperationResult.OperationComplete;
            DatabaseOperationResultList.add(opt);
            return opt;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
            opt = DatabaseOperationResult.Error;
            DatabaseOperationResultList.add(opt);
            return opt;
        } finally {
            if (conn != null) {
                try {
                    conn.setAutoCommit(true);
                } catch (SQLException ex) {
                    Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (autoConnection) {
                disconnect();
            }
        }
    }

    public static DatabaseOperationResult deleteSulama(Sulama sulama) {
        DatabaseOperationResult op = DatabaseOperationResult.NoOperation;

        if (sulama == null || sulama.getId() <= 0) {
            DatabaseOperationResultList.add(op);
            return op;

        }
        int sulamaId = sulama.getId();
        autoConnection = false;
        connect();
        op = DatabaseConnection.deleteSulamaGubreleri(sulama);
        if (op == DatabaseOperationResult.Error) {
            autoConnection = true;
            disconnect();
            return op;
        }
        String query = "DELETE FROM sulama "
                + "      WHERE id = ?;";

        if (autoConnection) {
            connect();
        }
        op = DatabaseOperationResult.BeforeDelete;
        DatabaseOperationResultList.add(op);
        try (PreparedStatement pst = conn.prepareStatement(query)) {
            pst.setInt(1, sulamaId);
            if (pst.executeUpdate() < 0) {
                op = DatabaseOperationResult.Error;
                DatabaseOperationResultList.add(op);
                return op;
            }
            op = DatabaseOperationResult.DeleteComplete;

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
            op = DatabaseOperationResult.Error;
        } finally {
            autoConnection = true;
            if (autoConnection) {
                disconnect();
            }
            DatabaseOperationResultList.add(op);
            return op;
        }
    }

    public static DatabaseOperationResult deleteSulamaGubreleri(Sulama sulama) {
        DatabaseOperationResult op = DatabaseOperationResult.NoOperation;
        if (sulama == null || sulama.getId() <= 0) {
            DatabaseOperationResultList.add(op);
            return op;

        }
        int sulamaId = sulama.getId();
        String query = "DELETE FROM sulama_gubre_iliskisi "
                + "      WHERE sulamaId = ?;";
        if (autoConnection) {
            connect();
        }
        op = DatabaseOperationResult.BeforeDelete;
        DatabaseOperationResultList.add(op);
        try (PreparedStatement pst = conn.prepareStatement(query)) {
            pst.setInt(1, sulamaId);
            if (pst.executeUpdate() < 0) {
                op = DatabaseOperationResult.Error;
                DatabaseOperationResultList.add(op);
                return op;
            }
            op = DatabaseOperationResult.DeleteComplete;

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
            op = DatabaseOperationResult.Error;
        } finally {
            if (autoConnection) {
                disconnect();
            }
            DatabaseOperationResultList.add(op);
            return op;
        }
    }

    ;
    public static DatabaseOperationResult insertSulama(Sulama sulama) {

        int seraId;
        long dt;
        {
            DatabaseOperationResult res = DatabaseOperationResult.NoOperation;
            if (sulama == null) {
                return res;
            }
            if (sulama.getSera() == null) {
                return res;
            }
            seraId = sulama.getSera().getId();
            if (seraId <= 0) {
                return res;
            }
        }
        dt = sulama.getDt() == null ? new java.util.Date().getTime() : sulama.getDt().getTime();

        if (autoConnection) {
            connect();
        }
        String query = "INSERT INTO sulama ("
                + "                       tarih,"
                + "                       sera_id,"
                + "                       sulama_suresi,"
                + "                       gubreMiktarı"
                + "                   )"
                + "                   VALUES (?,?,?,?)";
        DatabaseOperationResult br = DatabaseOperationResult.BeforeInsert;
        DatabaseOperationResultList.add(br);
        DatabaseOperationResult ar;
        try (PreparedStatement st = conn.prepareStatement(query)) {

            st.setLong(1, dt);
            st.setInt(2, seraId);
            st.setInt(3, sulama.getSulam_suresi());
            st.setDouble(4, sulama.getToplmaGubreKilosu());
            if (st.executeUpdate() <= 0) {
                if (autoConnection) {
                    disconnect();
                }
                ar = DatabaseOperationResult.Error;
                DatabaseOperationResultList.add(ar);
                return ar;
            }
            ar = DatabaseOperationResult.InsertComplete;
            DatabaseOperationResultList.add(ar);

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
            if (autoConnection) {
                disconnect();
            }
            ar = DatabaseOperationResult.Error;
            DatabaseOperationResultList.add(ar);
            return br;
        }
        br = ar;
        int id = -1;
        try (Statement st = conn.createStatement(); ResultSet res = st.executeQuery("Select max(id) from sulama")) {
            while (res.next()) {
                id = res.getInt(1);
                sulama.setId(id);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
            if (autoConnection) {
                disconnect();
            }
            ar = DatabaseOperationResult.Error;
            DatabaseOperationResultList.add(ar);
            return br;
        }
        if (autoConnection) {
            disconnect();
        }
        if (sulama.getId() <= 0) {
            ar = DatabaseOperationResult.Error;
            DatabaseOperationResultList.add(ar);
            return br;
        }
        return DatabaseConnection.insertSulamaGubreleri(sulama.getGubreMiktari(), sulama.getId());
    }

    private static boolean loadSulamaBilgiler(int lowLimit, int limitNumber) {
        String limit = "";
        if (limitNumber > 0) {
            limit = " LIMIT " + lowLimit + "," + limitNumber;
        }

        if (autoConnection) {
            connect();
        }
        String query = "SELECT s.id,"
                + "       s.tarih,"
                + "       s.sera_id,"
                + "       s.sulama_suresi,"
                + "       s.gubreMiktarı,"
                + "       sg.gubreId,"
                + "       sg.miktar "
                + "  FROM sulama as s LEFT JOIN sulama_gubre_iliskisi as sg ON s.id=sg.sulamaId order by s.id desc " + limit;
        try (Statement st = conn.createStatement(); ResultSet res = st.executeQuery(query)) {
            int lastId = 0;
            Sulama s = null;
            while (res.next()) {
                int id = res.getInt(1);
                Map<Gubre, Double> gubreMiktari;
                int gubreId = res.getInt(6);
                double gm = res.getDouble(7);
                if (id != lastId) {
                    lastId = id;
                    long d = res.getLong(2);
                    int seraId = res.getInt(3);
                    int sure = res.getInt(4);
                    double miktar = res.getDouble(5);
                    gubreMiktari = new HashMap<Gubre, Double>();
                    if (gubreId > 0) {
                        gubreMiktari.put((Gubre) Gubre.findByDbId(listGubre, gubreId), gm);
                    }
                    s = Sulama.createNewSulama(id, new java.util.Date(d), seraId, sure, gubreMiktari, miktar);
                    DatabaseObject.listSulama.add(s);
                } else {
                    if (s != null && gubreId > 0) {
                        gubreMiktari = s.getGubreMiktari();
                        gubreMiktari.put((Gubre) Gubre.findByDbId(listGubre, gubreId), gm);
                    }
                }

            }
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(query);
            return false;
        } finally {
            if (autoConnection) {
                disconnect();
            }
        }
    }

    public static DatabaseOperationResult updateSulama(Sulama sulama) {
        DatabaseOperationResult br = DatabaseOperationResult.BeforeUpdate;
        DatabaseOperationResultList.add(br);
        DatabaseOperationResult ar = null;
        if (sulama == null||sulama.getId()<=0) {
            ar = DatabaseOperationResult.NoOperation;
            DatabaseOperationResultList.add(ar);
            return ar;
        }
        String 
                query = "UPDATE sulama "
                + " SET "
                + "       tarih = ?,"
                + "       sera_id = ?,"
                + "       sulama_suresi = ?,"
                + "       gubreMiktarı = ?"
                + " WHERE id = ?";
        
        DatabaseOperationResult op=null;
        if(autoConnection)
            connect();
        try(PreparedStatement pst = conn.prepareStatement(query)){
            pst.setLong(1, sulama.getDt().getTime());
            pst.setInt(2, sulama.getSera().getId());
            pst.setInt(3, sulama.getSulam_suresi());
            pst.setDouble(4, sulama.getToplmaGubreKilosu());
            pst.setInt(5, sulama.getId());
            pst.execute();
            op=DatabaseOperationResult.UpdateComplete;
        } catch (SQLException ex) {
            op=DatabaseOperationResult.Error;
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            DatabaseOperationResultList.add(op);
            if(autoConnection)
                disconnect();
        }
        
        deleteSulamaGubreleri(sulama);
        op=insertSulamaGubreleri(sulama.getGubreMiktari(), sulama.getId());
        return op;
    }   

    private DatabaseConnection() {
    }

    public static DatabaseOperationResult insertGubre(Gubre g) {
        if (g == null) {
            return DatabaseOperationResultList.add(DatabaseOperationResult.NoOperation);
        }

        if (autoConnection) {
            connect();
        }
        //gubre degeri degismis mi
        //gubre yeni mi yeni ise insert
        if (g.getId() <= 0) {
            DatabaseOperationResult br = DatabaseOperationResult.BeforeInsert;
            DatabaseOperationResultList.add(br);
            DatabaseOperationResult ar;
            String query = "INSERT INTO gubre (\n"
                    + "                      name,\n"
                    + "                      nickname,\n"
                    + "                      gubre_sinifi\n"
                    + "                  )\n"
                    + "                  VALUES (?,?,?);";
            try (PreparedStatement st = conn.prepareStatement(query)) {
                st.setString(1, g.getName());
                st.setString(2, g.getKisaltma());
                st.setInt(3, g.getSinifi().getId());
                if (st.executeUpdate() <= 0) {
                    if (autoConnection) {
                        disconnect();
                    }
                    ar = DatabaseOperationResult.Error;
                    DatabaseOperationResultList.add(ar);
                    return ar;
                }
                ar = DatabaseOperationResult.InsertComplete;
                DatabaseOperationResultList.add(ar);

            } catch (SQLException ex) {
                Logger.getLogger(DatabaseConnection.class
                        .getName()).log(Level.SEVERE, null, ex);
                if (autoConnection) {
                    disconnect();
                }
                ar = DatabaseOperationResult.Error;
                DatabaseOperationResultList.add(ar);
                return br;
            }
            br = ar;
            int id = -1;
            try (Statement st = conn.createStatement(); ResultSet res = st.executeQuery("Select max(id) from gubre")) {
                while (res.next()) {
                    id = res.getInt(1);
                    g.setId(id);
                }
                if (autoConnection) {
                    disconnect();
                }
                br = ar;
                ar = DatabaseOperationResult.OperationComplete;
                DatabaseOperationResultList.add(ar);
                insertGubreIcerikOnGubre(g);
                return ar;

            } catch (SQLException ex) {
                Logger.getLogger(DatabaseConnection.class
                        .getName()).log(Level.SEVERE, null, ex);
                if (autoConnection) {
                    disconnect();
                }
                ar = DatabaseOperationResult.Error;
                DatabaseOperationResultList.add(ar);
                return br;
            }
        } else {
            //udate  gubre value
            DatabaseOperationResult before = DatabaseOperationResult.BeforeUpdate;
            DatabaseOperationResultList.add(before);
            DatabaseOperationResult after = null;
            String query = "UPDATE GUBRE SET name='" + g.getName() + "',nickname='" + g.getKisaltma() + "',gubre_sinifi=" + g.getSinifi().getId()
                    + " WHERE id = " + g.getId();
            try (Statement st = conn.createStatement()) {
                int i = st.executeUpdate(query);
                if (i > 0) {
                    after = DatabaseOperationResult.UpdateComplete;
                } else {
                    after = DatabaseOperationResult.NoOperation;

                }
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseConnection.class
                        .getName()).log(Level.SEVERE, null, ex);
                if (autoConnection) {
                    disconnect();
                }
                after = DatabaseOperationResult.Error;
            } finally {
                if (autoConnection) {
                    disconnect();
                }
                DatabaseOperationResultList.add(after);
                insertGubreIcerikOnGubre(g);
                return after;
            }
        }

    }

    public static boolean LoadGubreBilgileri() {
        boolean flag;
        listGubre.clear();
        listGubreIcerik.clear();
        listGubreSinif.clear();
        listSera.clear();
        setAutoConnection(false);
        connect();
        flag = loadGubreSinifi();
        flag &= loadGubreIcerik();
        flag &= loadGubre();
        flag = flag & putGubreIcerikOnGubre();
        flag = flag & loadSera();
        disconnect();
        setAutoConnection(true);
        return flag;
    }

    public static boolean loadSulamaBilgileri(int lstart, int limit) {
        boolean flag = true;
        listSulama.clear();
        setAutoConnection(false);
        connect();
        flag = loadSulamaBilgiler(lstart, limit);

        disconnect();
        setAutoConnection(true);
        return flag;
    }

    private static boolean loadGubre() {
        if (autoConnection) {
            connect();
        }
        String query = "SELECT id,"
                + "       name,"
                + "       nickname,"
                + "       gubre_sinifi,"
                + "       aktif"
                + "  FROM gubre ";
        Gubre g;
        try (Statement st = conn.createStatement(); ResultSet rs = st.executeQuery(query)) {
            listGubre.clear();
            while (rs.next()) {
                try {
                    GubreSinifi gs = (GubreSinifi) DatabaseObject.findByDbId(listGubreSinif, rs.getInt("gubre_sinifi"));
                    g = Gubre.createNewGubre(rs.getInt("id"), rs.getString("name"), rs.getString("nickname"), gs, rs.getInt("aktif") >= 1);
                    listGubre.add(g);
                } catch (Exception ex) {
                    System.err.println("asd" + ex.getMessage());
                    Logger
                            .getLogger(DatabaseConnection.class
                                    .getName()).log(Level.SEVERE, null, ex);

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (listGubre != null) {
                Collections.sort(listGubre);
            }
            if (autoConnection) {
                disconnect();
            }
        }
        return true;
    }

    private static boolean putGubreIcerikOnGubre() {
        if (listGubreIcerik.isEmpty() || listGubre.isEmpty()) {
            return false;
        }
        if (autoConnection) {
            connect();
        }
        String query1 = "SELECT gubre_id,\n"
                + "       gubre_icerik_id,\n"
                + "       oran\n"
                + "  FROM gubre_gubre_icerik_ilişkisi";
        try (Statement st = conn.createStatement()) {
            String query;
            for (Gubre g : listGubre) {
                query = query1 + " WHERE gubre_id = " + g.getId();
                try (ResultSet rs = st.executeQuery(query)) {
                    GubreIcerik gi;
                    Integer gubreIcerikId;
                    Double oran;
                    g.clearIcerik();
                    while (rs.next()) {
                        gubreIcerikId = rs.getInt("gubre_icerik_id");
                        if (gubreIcerikId == 0) {
                            continue;
                        }
                        oran = rs.getDouble("oran");
                        if (oran == 0) {
                            continue;
                        }
                        gi = (GubreIcerik) DatabaseObject.findByDbId(listGubreIcerik, gubreIcerikId);
                        g.addIcerik(gi, oran);

                    }
                } catch (SQLException ex) {
                    Logger.getLogger(DatabaseConnection.class
                            .getName()).log(Level.SEVERE, null, ex);

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class
                    .getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            if (autoConnection) {
                disconnect();
            }
        }
        return true;
    }

    private static boolean loadGubreSinifi() {
        if (autoConnection) {
            connect();
        }
        String query = "SELECT id,\n"
                + "       sinif,\n"
                + "       kisaltma\n"
                + "  FROM gubre_sinifi";
        try (Statement st = conn.createStatement(); ResultSet rs = st.executeQuery(query)) {
            GubreSinifi gs;
            while (rs.next()) {
                gs = new GubreSinifi(rs.getInt("id"), rs.getString("sinif"), rs.getString("kisaltma"));
                listGubreSinif.add(gs);

            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class
                    .getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            if (autoConnection) {
                disconnect();
            }
            Collections.sort(listGubreSinif);
        }
        return true;
    }

    private static boolean loadGubreIcerik() {
        if (autoConnection) {
            connect();
        }
        String query = "SELECT id,\n"
                + "       icerik_adi,\n"
                + "       molekul_adi,\n"
                + "       parent_id\n"
                + "  FROM gubre_icerik";
        try (Statement st = conn.createStatement(); ResultSet rs = st.executeQuery(query)) {
            GubreIcerik gi;
            while (rs.next()) {
                gi = new GubreIcerik(rs.getInt("id"), rs.getString("icerik_adi"), rs.getString("molekul_adi"), rs.getInt("parent_id"));
                listGubreIcerik.add(gi);

            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class
                    .getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            if (autoConnection) {
                disconnect();
            }
        }
        return true;
    }

}
