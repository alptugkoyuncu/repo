/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

/**
 *
 * @author alptug
 */
public enum DatabaseOperationResult {    

    /**
     *
     */
    Error("İşlem sırasında hata oluştu"),

    /**
     *
     */
    NoOperation("Hiçbir işlem gerçekleştirilmedi"),

    /**
     *
     */
    OperationComplete("İşlem tamam"),

    /**
     *
     */
    InsertComplete("Ekleme İşlemi Başarılı"),

    /**
     *
     */
    UpdateComplete("Güncelleme Başarılı"),

    /**
     *
     */
    DeleteComplete("Silme işlemi başarılı"),

    /**
     *
     */
    BeforeInsert("Ekleme işlemi deneniyor"),

    /**
     *
     */
    BeforeUpdate("Güncelleme İşlemi deneniyor"),

    /**
     *
     */
    BeforeDelete("Silme işlemi deneniyor");
    private final String msg;
    private  String aciklama;
    private DatabaseOperationResult parent;
    private DatabaseOperationResult(String msg) {
        this.msg=msg;
        this.parent=null;
        this.aciklama=null;
    }



    /**
     * kısa constructor daki mesajı verir
     * @return
     */
    public String getMsg() {
        return msg;
    }

    /**
     * 
     * @param aciklama constructor daki mesajın yetersizliği durumunda ek mesaj olarak kullanılabilir
     */
    public void setAciklama(String aciklama){
        this.aciklama=aciklama;
    }

    /**
     *
     * @return kullanicinin ek girdigi aciklama döner eger ek acıklama girilmemişse constructordaki mesaj geri gönderilir 
     */
    public String getAciklama(){
        return aciklama;
    }

}

