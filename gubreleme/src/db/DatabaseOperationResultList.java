/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.util.ArrayList;

/**
 *
 * @author alptug
 */
public class DatabaseOperationResultList {
    private final static ArrayList<DatabaseOperationResult> results=new ArrayList<DatabaseOperationResult>();
    public static DatabaseOperationResult add(DatabaseOperationResult r){
        results.add(r);
        return r;
    }
    public static void clear(){
        results.clear();
        System.gc();
    }
    public static String getMesagges(){
        String s="";
        for(DatabaseOperationResult r:results)
            s+=r.getMsg()+"\n";
        return s;
    }
}
