/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gubre_hazirlama;

import data.Gubre;
import data.GubreIcerik;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 *
 * @author alptug
 */
public abstract class AbstractKarisimGubre implements Runnable{
    private double karisimKilosu;

    public final static String GUBRE_KARISIMI_BASLADI="karisima Baslandi";
    public final static String GUBRE_KARISIMI_IPTAL="karisim Iptal Edildi";
    public final static String GUBRE_KARISIMI_HAZIRLANDI="Karisim Hazirlandi";
    
    ArrayList<ActionListener> listeners;
    String position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
    
    public AbstractKarisimGubre(){
        
        this.listeners=new ArrayList<ActionListener>();
    }
    public void setKarisimKilosu(double karisimKilosu) {
        this.karisimKilosu = karisimKilosu;
    }

    public double getKarisimKilosu() {
        return karisimKilosu;
    }
    public abstract void addKarisimGubreFormu(GubreIcerik gf, double oran);
    public abstract double getKarisimGubreFormOrani(GubreIcerik gf);
    public abstract GubreIcerik[] getKarisimGubreFormlari();
    
    public abstract void addGubre(Gubre g,double miktar);
    public abstract Gubre[] getKullanilanGubreler();
    public abstract double getKullanilanGubreMiktari(Gubre g);
    protected abstract void karisimiHazirla();

    @Override
    public void run(){
        karisimiHazirla();
    }
    public void addActionListener(ActionListener l){
        this.listeners.add(l);
    }
    public void removeActionListener(ActionListener l){
        this.listeners.remove(l);
    }
    protected void fireActionEvent(AbstractKarisimGubre source,String command){
        ActionEvent e = new ActionEvent(source, source.hashCode(), command);
        for(ActionListener l:listeners)
            l.actionPerformed(e);
    }

    public abstract void reset();

}
