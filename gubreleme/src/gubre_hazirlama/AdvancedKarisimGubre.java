/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gubre_hazirlama;

import data.Gubre;
import data.GubreIcerik;
import static gubre_hazirlama.AbstractKarisimGubre.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alptug
 */
public class AdvancedKarisimGubre extends AbstractKarisimGubre {

    private final static double MAX_FORM_ORAN_SAPMA = 0.01;
    Set<HazirlananGubre> karisimGubreListesi;
    //istenen GubreFormu orani ve istenen kilonun çarpımına eşittir
    Map<GubreIcerik, Double> istenenFormMiktari;
    //koyulan gubrelerin form orani ve kilo çarpımına eşittir
    Map<GubreIcerik, Double> hazirlananFormMiktari;

    public static AbstractKarisimGubre createAdvancedKarisimGubre(Map<Gubre, Double> karisimGubre, double kilo) {
        AdvancedKarisimGubre ag = new AdvancedKarisimGubre();
        for (Gubre g : karisimGubre.keySet()) {
            ag.addGubre(g, karisimGubre.get(g), true);
        }
        ag.setKarisimKilosu(kilo);
        ag.istenenFormMiktari.putAll(ag.hazirlananFormMiktari);
        ag.fillFinishedGubre();
        return ag;
    }

    public AdvancedKarisimGubre() {
        this.karisimGubreListesi = new HashSet<>();
        istenenFormMiktari = new HashMap<GubreIcerik, Double>();
        hazirlananFormMiktari = new HashMap<GubreIcerik, Double>();
        this.setKarisimKilosu(0);
    }

    @Override
    public void addKarisimGubreFormu(GubreIcerik gf, double oran) {
        oran = oran * this.getKarisimKilosu();
        this.istenenFormMiktari.put(gf, oran);
    }

    public Set<HazirlananGubre> getKarisimGubreListesi() {
        return karisimGubreListesi;
    }

    @Override
    public void setKarisimKilosu(double karisimKilosu) {
        if (karisimKilosu < 0) {
            super.setKarisimKilosu(0); //To change body of generated methods, choose Tools | Templates.
        } else {
            for (GubreIcerik gf : this.istenenFormMiktari.keySet()) {
                double newVal = karisimKilosu * this.istenenFormMiktari.get(gf) / this.getKarisimKilosu();
                this.istenenFormMiktari.put(gf, newVal);
            }
            super.setKarisimKilosu(karisimKilosu);
        }
    }

    @Override
    public double getKarisimGubreFormOrani(GubreIcerik gf) {
        return this.hazirlananFormMiktari.get(gf) / this.getKarisimKilosu();
    }

    @Override
    public GubreIcerik[] getKarisimGubreFormlari() {
        return this.hazirlananFormMiktari.keySet().toArray(new GubreIcerik[0]);
    }

    @Override
    public void addGubre(Gubre g, double miktar) {
        this.addGubre(g, miktar, miktar > 0);
    }

    public void addGubre(Gubre g, double miktar, boolean isFinished) {
        miktar = miktar > 0.0 ? miktar : 0.0;
        karisimGubreListesi.add(new HazirlananGubre(g, miktar, isFinished));
    }

    @Override
    public Gubre[] getKullanilanGubreler() {
        Gubre[] list = new Gubre[this.karisimGubreListesi.size()];
        int i = 0;
        for (HazirlananGubre hg : this.karisimGubreListesi) {
            list[i++] = hg.getGubre();
        }
        return list;
    }

    @Override
    public double getKullanilanGubreMiktari(Gubre g) {
        for (HazirlananGubre hg : this.karisimGubreListesi) {
            if (hg.getGubre().equals(g)) {
                return hg.miktar;
            }
        }
        return 0;
    }

// istenilen herhangi bir gubre formu icin tek bir gubre varsa o gubreyi ilk tanka ekle 
    private void firstProcess() {
        boolean restartFunc;

        //gubreIcerigi gubreler icinde kontrol et
        //gubre icerigi sadece bir gubre sagliyorsa ilk ona gore ekleme yap
        do {
            restartFunc = false;
            Map<GubreIcerik, Integer> gubreIcergiMiktari = new HashMap<GubreIcerik, Integer>();
            double sapma = getSapma();
            for (GubreIcerik gi : istenenFormMiktari.keySet()) {
                if (getHazirlanacakFormFarki(gi) < sapma) {
                    continue;
                }
                for (HazirlananGubre hg : karisimGubreListesi) {
                    if (!hg.isFinished() && hg.getGubre().getIcerik().containsKey(gi)) {
                        int val = gubreIcergiMiktari.getOrDefault(gi, 0);
                        gubreIcergiMiktari.put(gi, ++val);
                    }
                }
            }
            for (GubreIcerik gi : gubreIcergiMiktari.keySet()) {
                int val = gubreIcergiMiktari.getOrDefault(gi, 0);
                if (val == 1) {
                    restartFunc = true;
                    System.err.print("Gubreler icinde " + gi.getIcerikAdi() + " formunu bulundaran tek gubre : ");
                    for (HazirlananGubre hg : karisimGubreListesi) {
                        double icerikOrani = hg.getGubre().getIcerik().getOrDefault(gi, 0.0);
                        if (!hg.isFinished() && icerikOrani > 0) {
                            System.err.print(hg.getGubre().getName());
                            double formMiktari = this.getHazirlanacakFormFarki(gi);
                            double miktar = formMiktari / icerikOrani;
                            System.err.println(" Miktar : " + miktar);
                            karisimaKat(hg, miktar);
                            hg.increaseMiktar(miktar);
                            hg.setFinished(true);
                        }
                    }
                }
            }
        } while (restartFunc);
    }

    private void karisimaKat(HazirlananGubre hg, double miktar) {
        for (GubreIcerik gf : hg.getGubre().getIcerik().keySet()) {
            Double oldMiktar = this.hazirlananFormMiktari.getOrDefault(gf, 0.0);
            Double oran = hg.getGubre().getIcerik().get(gf);
            this.hazirlananFormMiktari.put(gf, oldMiktar + oran * miktar);
        }
    }

    private double getHazirlanacakFormFarki(GubreIcerik gf) {
        Double hazirlanmis = this.hazirlananFormMiktari.get(gf);
        Double istenen = this.istenenFormMiktari.get(gf);
        return hazirlanmis == null ? istenen : istenen - hazirlanmis;
    }

    private void fillFinishedGubre() {
        for (HazirlananGubre hg : karisimGubreListesi) {
            if (hg.isFinished()) {
                karisimaKat(hg, hg.getMiktar());
            }
        }
    }

    @Override
    public void karisimiHazirla() {

//zorunlulari tanka ekle
        fireActionEvent(AdvancedKarisimGubre.this, GUBRE_KARISIMI_BASLADI);
        fillFinishedGubre();

        if (getKarisimKilosu() <= 0) {
            fireActionEvent(AdvancedKarisimGubre.this, GUBRE_KARISIMI_IPTAL);
            return;
        }
        firstProcess();
        try {
            lastProcess();
            System.err.println("finished last process");
            fireActionEvent(AdvancedKarisimGubre.this, GUBRE_KARISIMI_HAZIRLANDI);
        } catch (Exception ex) {
            fireActionEvent(AdvancedKarisimGubre.this, GUBRE_KARISIMI_IPTAL);
            Logger.getLogger(AdvancedKarisimGubre.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(istenenFormMiktari);
        System.out.println(hazirlananFormMiktari);
        System.out.println(karisimGubreListesi);

//   kalanlari en yuksek formu az az indirerek
//   formlara 0'a ya da kabuledilebilir farka ininceye kadar devam edilir
    }

    //eksikler eklenir
//   kalanlari en yuksek formu az az indirerek
//   formlara 0'a ya da kabuledilebilir farka ininceye kadar devam edilir 
    private double getSapma() {
        return MAX_FORM_ORAN_SAPMA * this.getKarisimKilosu();
    }

    private void lastProcess() throws Exception {
        double sapma = getSapma();

        while (true) {
            //hazirlanan gubredeki form ile istenen gubre formlari karsilastirilir en fazla fark olan gubre icerigi bulunur
            //max formda tutulur
            double maxFormEksikligi = 0;
            GubreIcerik maxForm = null;
            for (GubreIcerik gf : this.istenenFormMiktari.keySet()) {
                double temp = this.getHazirlanacakFormFarki(gf);
//                System.out.println("Hazirlanacak " + gf.getIcerikAdi() + " farki : " + temp);
                if (temp > maxFormEksikligi) {
                    maxForm = gf;
                    maxFormEksikligi = temp;
                }
            }
            //bu fark sapma miktarindan az ise cikilir.
            if (maxFormEksikligi <= sapma) {
                break;
            }
            //bu formu içerisinde en fazla barındıran gübre bulunur
            HazirlananGubre hg = null;
            for (HazirlananGubre tmp : karisimGubreListesi) {
                if (!tmp.isFinished() && tmp.getGubre().getIcerik().containsKey(maxForm)) {
                    if (hg == null || hg.getGubre().getIcerik().get(maxForm) < tmp.getGubre().getIcerik().get(maxForm)) {
                        hg = tmp;
                    }
                }

            }
            if (hg == null) {
                throw new Exception("Formul iceriğini sağlayacak gübre yok");
            } else {
                double kgMktari;
                if (maxFormEksikligi > MAX_FORM_ORAN_SAPMA * 1000 * 10) {
                    kgMktari = MAX_FORM_ORAN_SAPMA * 10;
                } else if (maxFormEksikligi > MAX_FORM_ORAN_SAPMA * 1000 * 5) {
                    kgMktari = MAX_FORM_ORAN_SAPMA * 5;
                } else {
                    kgMktari = MAX_FORM_ORAN_SAPMA;
                }
                this.karisimaKat(hg, kgMktari);
                hg.increaseMiktar(kgMktari);
            }

        }
    }

    @Override
    public void reset() {
        this.karisimGubreListesi = new HashSet<>();
        istenenFormMiktari = new HashMap<GubreIcerik, Double>();
        hazirlananFormMiktari = new HashMap<GubreIcerik, Double>();
        this.setKarisimKilosu(1);
    }

    public String getKompositGubreLabel() {
        System.out.print("[");
        for (GubreIcerik gi : getKarisimGubreFormlari()) {
            System.out.print(" " + gi.getMolekulAdi() + " " + hazirlananFormMiktari.get(gi));
        }
        System.out.println("]");
        String str = getGILabel(hazirlananFormMiktari, "N", "0") + " " + getGILabel(hazirlananFormMiktari, "P2O5", "0") + " " + getGILabel(hazirlananFormMiktari, "K2O", "0");
        String val = getGILabel(hazirlananFormMiktari, "CaO", "");
        if (!val.isEmpty()) {
            str = str + " " + val + " CaO";
        }
        val = getGILabel(hazirlananFormMiktari, "SO4", "");
        if (!val.isEmpty()) {
            str = str + " " + val + " SO4";
        }
        val = getGILabel(hazirlananFormMiktari, "MgO", "");
        if (!val.isEmpty()) {
            str = str + " " + val + " MgO";
        }
        return str;
    }

    private String getGILabel(Map<GubreIcerik, Double> m, String molekulAdi, String defaultValue) {
        for (GubreIcerik gi : m.keySet()) {
            if (gi.getMolekulAdi().equals(molekulAdi)) {
                return String.format("%.0f", m.get(gi) / this.getKarisimKilosu());
            }
        }
        return defaultValue;

    }

    class HazirlananGubre {

        final Gubre gubre;
        Double miktar;
        boolean finished;

        public HazirlananGubre(Gubre gubre, Double miktar, boolean finished) {
            this.gubre = gubre;
            this.miktar = miktar;
            this.finished = finished;
        }

        public Gubre getGubre() {
            return gubre;
        }

        public Double getMiktar() {
            return miktar;
        }

        public void setMiktar(Double miktar) {
            this.miktar = miktar;
        }

        public boolean isFinished() {
            return finished;
        }

        public void setFinished(boolean finished) {
            this.finished = finished;
        }

        private void increaseMiktar(double miktar) {
            this.setMiktar(this.miktar == null ? miktar : this.miktar + miktar);
        }

        @Override
        public String toString() {
            return gubre + " : " + this.miktar + " " + finished;
        }
    }
}
