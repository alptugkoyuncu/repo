/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alptu
 */
public class Address {
    String baslik;

    public static Address createAddress(String baslik){
        Address a = new Address();
        a.setBaslik(baslik);
        return a;
    }
    public String getBaslik() {
        return baslik;
    }

    public void setBaslik(String baslik) {
        this.baslik = baslik;
    }
}
