/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author alptu
 */
public class DumpDataFactory {

    private DumpDataFactory() {

    }

    public static List<Address> createAdresData() {
        List<Address> l = new ArrayList<>();
        l.add(Address.createAddress("koy"));
        l.add(Address.createAddress("is yeri"));
        return l;
    }

    public static List<IlacKutusu> createIlacKutusu(int miktar, List<Ilac> ilaclar) {
        GregorianCalendar tmp = (GregorianCalendar) Calendar.getInstance();
        tmp.add(Calendar.YEAR, -1);

        List<IlacKutusu> ilacKutulari = new ArrayList<>();
        for (int i = 0; i < miktar; i++) {
            int randDay = (int) (Math.random() * 60)+30;
            GregorianCalendar uretimCalendar = new GregorianCalendar();
            GregorianCalendar tuketimCalendar = new GregorianCalendar();
            tuketimCalendar.add(Calendar.DATE, randDay);
            uretimCalendar.setTime(tuketimCalendar.getTime());
            uretimCalendar.add(Calendar.YEAR, -2);
            int ilacIndex = (int) (Math.random() * ilaclar.size());
            Double satisFiyati = (Double) (Math.random() * 900) + 100;
            Integer satisMiktari = (int) (Math.random() * 20) + 10;
            ilacKutulari.add(new IlacKutusu(i, ilaclar.get(ilacIndex), uretimCalendar.getTime(), tuketimCalendar.getTime(), new BigDecimal(satisFiyati), satisMiktari.longValue()));
        }
        return ilacKutulari;
    }

    public static List<IlacFirmasi> createDumpIlacFirmasi(int miktar) {
        List<IlacFirmasi> tmp = new ArrayList<>();
        for (int i = 0; i < miktar; i++) {
            String name = String.format("IlacFirmasi%2d", i);
            tmp.add(new IlacFirmasi(i, name));
        }
        return tmp;
    }

    public static List<Ilac> createDumpIlac(int miktar, List<IlacFirmasi> ilacFirmalari, List<EtkenMadde> etkenMaddeler) {
        List<Ilac> tmp = new ArrayList<>();
        int ilacId = 0;
        while (ilacId < miktar) {
            List<EtkenMadde> em = new ArrayList<>();
//            IlacFirmasi ilf;
            for (int j = 0; j < etkenMaddeler.size(); j++) {
                int rand = (int) (Math.random() * 3);
                for (int k = 0; k < rand; k++) {
                    em.add(etkenMaddeler.get((k + j) % etkenMaddeler.size()));
                }
                for (IlacFirmasi ilf : ilacFirmalari) {
                    String name = String.format("ilac%3d", ilacId);
                    tmp.add(new Ilac(ilacId++, name, ilf, em));
                }
            }
        }
        return tmp;
    }

    public static List<EtkenMadde> createDumpEtkenMadde(int miktar) {
        List<EtkenMadde> tmp = new ArrayList<>();
        for (int i = 0; i < miktar; i++) {
            String name = String.format("etkenMadde%2d", i);
            tmp.add(new EtkenMadde(i, name));
        }
        return tmp;
    }
}
