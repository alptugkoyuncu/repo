/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.List;

/**
 *
 * @author alptu
 */
public class Ilac implements Listable{
    private int id;
    private IlacFirmasi ilacFirmasi;
    private String name;
    private List<EtkenMadde> etkenMaddeler;

    public Ilac(int id, String name, IlacFirmasi ilacFirmasi,List<EtkenMadde> etkenMaddeler) {
        this.id = id;
        this.name = name;
        this.etkenMaddeler = etkenMaddeler;
        this.ilacFirmasi=ilacFirmasi;
    }

    public IlacFirmasi getIlacFirmasi() {
        return ilacFirmasi;
    }

    public void setIlacFirmasi(IlacFirmasi ilacFirmasi) {
        this.ilacFirmasi = ilacFirmasi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EtkenMadde> getEtkenMaddeler() {
        return etkenMaddeler;
    }

    public void setEtkenMaddeler(List<EtkenMadde> etkenMaddeler) {
        this.etkenMaddeler = etkenMaddeler;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ilac other = (Ilac) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String getListName() {
        return this.getName();
    }
    
}
