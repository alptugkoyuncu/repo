/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.List;

/**
 *
 * @author alptu
 */
public class IlacKutusu{
    private int ilacSatisId;
    private Ilac ilac;
    private String QRCODE;
    private Date uretimTarihi;
    private Date sonKullanmaTarihi;
    private BigDecimal satisFiyati;
    private BigDecimal marketFiyati;
    private Long miktar;

    public IlacKutusu() {
    }
    public IlacKutusu(int ilacSatisId, Ilac ilac, Date uretimTarihi, Date sonKullanmaTarihi, BigDecimal satisFiyati, Long miktar) {
        this.ilacSatisId = ilacSatisId;
        this.ilac = ilac;
        this.uretimTarihi = uretimTarihi;
        this.sonKullanmaTarihi = sonKullanmaTarihi;
        this.satisFiyati = satisFiyati;
        this.miktar = miktar;
        SimpleDateFormat format = new SimpleDateFormat("ddMMYYYY");
        this.QRCODE=String.format("%04d%s%s", ilac.getId(),format.format(uretimTarihi),format.format(sonKullanmaTarihi));
    }

   
    
    public IlacKutusu(int ilacSatisId, String QRCODE, BigDecimal satisFiyati, Long miktar) {
        this.ilacSatisId = ilacSatisId;
        this.QRCODE = QRCODE;
        this.satisFiyati = satisFiyati;
        this.miktar = miktar;
    }

    public IlacKutusu(int ilacSatisId, Ilac ilac, String QRCODE, Date uretimTarihi, Date sonKullanmaTarihi, BigDecimal satisFiyati, Long miktar) {
        this.ilacSatisId = ilacSatisId;
        this.ilac = ilac;
        this.QRCODE = QRCODE;
        this.uretimTarihi = uretimTarihi;
        this.sonKullanmaTarihi = sonKullanmaTarihi;
        this.satisFiyati = satisFiyati;
        this.miktar = miktar;
    }

    public int getIlacSatisId() {
        return ilacSatisId;
    }

    public void setIlacSatisId(int ilacSatisId) {
        this.ilacSatisId = ilacSatisId;
    }

    public Ilac getIlac() {
        return ilac;
    }

    public void setIlac(Ilac ilac) {
        this.ilac = ilac;
    }

    public BigDecimal getSatisFiyati() {
        return satisFiyati;
    }

    public void setSatisFiyati(BigDecimal satisFiyati) {
        this.satisFiyati = satisFiyati;
    }

    public BigDecimal getMarketFiyati() {
        return marketFiyati;
    }

    public void setMarketFiyati(BigDecimal marketFiyati) {
        this.marketFiyati = marketFiyati;
    }

    
    public String getQRCODE() {
        return QRCODE;
    }

    public void setQRCODE(String QRCODE) {
        this.QRCODE = QRCODE;
    }

    public Date getUretimTarihi() {
        return uretimTarihi;
    }

    public void setUretimTarihi(Date uretimTarihi) {
        this.uretimTarihi = uretimTarihi;
    }

    public Date getSonKullanmaTarihi() {
        return sonKullanmaTarihi;
    }

    public void setSonKullanmaTarihi(Date sonKullanmaTarihi) {
        this.sonKullanmaTarihi = sonKullanmaTarihi;
    }


    public Long getMiktar() {
        return miktar;
    }

    public void setMiktar(Long miktar) {
        this.miktar = miktar;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.ilacSatisId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IlacKutusu other = (IlacKutusu) obj;
        if (this.ilacSatisId != other.ilacSatisId) {
            return false;
        }
        return true;
    }
    
}
