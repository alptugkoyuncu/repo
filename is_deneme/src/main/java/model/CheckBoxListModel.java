/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import data.EtkenMadde;
import data.Listable;
import java.util.List;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author alptu
 */
public class CheckBoxListModel implements ListModel<Listable>{
    final List<? extends Listable> list;
    boolean selected[];

    public CheckBoxListModel(List<? extends Listable> l) {
        this.list=l;
        selected= new boolean[l.size()];
    }


    @Override
    public int getSize() {
        return this.list.size();
    }

    @Override
    public Listable getElementAt(int index) {
        return this.list.get(index);
    }
    public boolean isSelectedAt(int index){
        return this.selected[index];
    }
    public void setSelectedAt(int index,boolean selected){
        this.selected[index]=selected;
    }
    @Override
    public void addListDataListener(ListDataListener l) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
