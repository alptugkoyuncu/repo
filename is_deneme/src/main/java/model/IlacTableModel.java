/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import data.IlacKutusu;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author alptu
 */
public class IlacTableModel extends AbstractTableModel{

    String[] cols={"Adi","Kalan Gun","Uretim Tarihi","Tuketim Tarihi","Fiyat","Miktar"};
    List<IlacKutusu> ilacList;

    public List<IlacKutusu> getIlacList() {
        return ilacList;
    }

    public void setIlacList(List<IlacKutusu> ilacList) {
        this.ilacList = ilacList;
        this.fireTableDataChanged();
    }
    @Override
    public String getColumnName(int column) {
        return (cols[column]); //To change body of generated methods, choose Tools | Templates.
    }
    public IlacTableModel(List<IlacKutusu> il) {
        this.ilacList=il;
    }
    @Override
    
    public int getRowCount() {
        return ilacList.size();
    }
    @Override
    public Class<?> getColumnClass(int columnIndex) {
       switch(columnIndex){
           case 0: return String.class;
           case 1: return Long.class;
           case 2: return Date.class;
           case 3: return Date.class;
           case 4: return BigDecimal.class;
           case 5: return Integer.class;
           default:return String.class;
               
       }
          }

    @Override
    public int getColumnCount() {
        return cols.length;
    }
    public IlacKutusu getIlacAt(int rowIndex){
        return ilacList.get(rowIndex);
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        IlacKutusu i =ilacList.get(rowIndex);
        switch(columnIndex){
            case 0:
                return i.getIlac().getName();
            case 1:
                return i.getSonKullanmaTarihi().getTime()-new Date().getTime();
            case 2:
                return i.getUretimTarihi();
            case 3:
                return i.getSonKullanmaTarihi();
            case 4:
                return i.getSatisFiyati();
            case 5:
                return i.getMiktar();
            default:
                return null;
        }
    }

    public void setIlacAt(int index, IlacKutusu ilac) {
        this.fireTableRowsUpdated(index, index);
    }
}
