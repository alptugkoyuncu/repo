/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import data.Listable;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import model.CheckBoxListModel;
import model.CheckBoxListModel;

/**
 *
 * @author alptu
 */
public class CheckBoxList extends JList<Listable> {

    List<Boolean> selected;

    public CheckBoxList() {
        super();
        selected = new ArrayList<>();
        this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.setCellRenderer(new ListCellRenderer<Listable>() {
            @Override
            public Component getListCellRendererComponent(JList list, Listable value, int index, boolean isSelected, boolean cellHasFocus) {
                JCheckBox check = new JCheckBox();
                check.setEnabled(list.isEnabled());
                check.setOpaque(true);
                CheckBoxListModel mdl = (CheckBoxListModel) list.getModel();
                check.setSelected(mdl.isSelectedAt(index));
                check.setFont(list.getFont());
                if (check.isSelected()) {
                    check.setBackground(list.getSelectionBackground());
                    check.setForeground(list.getSelectionForeground());
                } else {
                    check.setBackground(list.getBackground());
                    check.setForeground(list.getForeground());
                }
                check.setText(value.getListName());
//                check.setBackground(Color.BLUE);
                return check;
            }
        });
        this.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent event) {
                int index = locationToIndex(event.getPoint());// Get index of item
                // clicked
                CheckBoxListModel mdl = (CheckBoxListModel) getModel();
                mdl.setSelectedAt(index, !mdl.isSelectedAt(index));
                repaint(getCellBounds(index, index));// Repaint cell
            }

        });
    }

}
