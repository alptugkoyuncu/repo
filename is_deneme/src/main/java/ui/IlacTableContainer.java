/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import data.IlacKutusu;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;
import model.IlacTableModel;

/**
 *
 * @author alptu
 */
public class IlacTableContainer extends JPanel {

    private final ButtonPanel btnPannel;
    private final SearchPanel searchPanel;
    private final IlacTable ilacTable;
    private final PagePanel pagePanel;
    private IlacTableModel dataModel;
    private List<IlacKutusu> ilacList;

    public IlacTableContainer(List<IlacKutusu> list, boolean buttonPanelAvailable) {

        dataModel = new IlacTableModel(list);

        searchPanel = new SearchPanel();
        ilacTable = new IlacTable(dataModel);
        pagePanel = new PagePanel();

        this.setLayout(new BorderLayout(5, 5));
        JPanel northPanel;
        if (buttonPanelAvailable) {
            northPanel = new JPanel(new GridLayout(2, 1));
            btnPannel = new ButtonPanel();
            northPanel.add(btnPannel);
            northPanel.add(searchPanel);
        }else{
            northPanel=searchPanel;
            btnPannel=null;
        }
        this.add(BorderLayout.NORTH, northPanel);
        this.add(BorderLayout.CENTER, new JScrollPane(ilacTable));
        this.add(BorderLayout.SOUTH, pagePanel);
    }


    public List<IlacKutusu> getIlacList() {
        return ilacList;
    }

    public void setIlacList(List<IlacKutusu> ilacList) {
        this.ilacList = ilacList;
        this.dataModel.setIlacList(ilacList);
    }

    public IlacTable getIlacTable() {
        return ilacTable;
    }

    class SearchPanel extends JPanel {

        final JTextField searchText;
        final JButton searcButton;

        SearchPanel() {
            this.searchText = new JTextField();
            this.searcButton = new JButton("Ara");
            this.setLayout(new BorderLayout(5, 5));
            this.add(searchText, BorderLayout.CENTER);
            this.add(searcButton, BorderLayout.EAST);
        }
    }

    class IlacTable extends JTable {

        IlacTable(IlacTableModel mdl) {
            this.setModel(mdl);
            this.setDefaultRenderer(BigDecimal.class, IlacTableCellRenderer.getFiyatRenderer());
            this.setDefaultRenderer(Date.class, IlacTableCellRenderer.getDateCellRenderer());
            this.getColumnModel().getColumn(1).setCellRenderer(IlacTableCellRenderer.getKalanGunRenderer());

        }

        @Override
        public void setModel(TableModel dataModel) {
            reset();
            super.setModel(dataModel); //To change body of generated methods, choose Tools | Templates.

        }
    }

    public void reset() {
    }

    class PagePanel extends JPanel {

        JButton prevButton, nextButton;
        JFormattedTextField ftxt;

        PagePanel() {
            this.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
            prevButton = new JButton("<");
            ftxt = new JFormattedTextField();
            ftxt.setPreferredSize(new Dimension(40, 30));
            ftxt.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
            nextButton = new JButton(">");
            this.add(prevButton);
            this.add(ftxt);
            this.add(nextButton);
        }
    }

    class ButtonPanel extends JPanel {

        private final JButton btnEkle, btnSil, bntGuncelle;

        public ButtonPanel() {
            this.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
            this.btnEkle = new JButton("Ekle");
            this.bntGuncelle = new JButton("Güncelle");
            this.btnSil = new JButton("Sil");
            this.add(btnEkle);
            this.add(bntGuncelle);
            this.add(btnSil);
            btnEkle.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ekleButtonClicked();
                }
            });
            this.bntGuncelle.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    guncelleButtonClicked();
                }
            });
            this.btnSil.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    silButtonClicked();
                }
            });
        }
    }

    void ekleButtonClicked() {
        IlacEklemeGuncellemePanel pnl = new IlacEklemeGuncellemePanel(null);
        String[] options = {"Ekle", "Sil", "Iptal"};
        JOptionPane.showOptionDialog(this, pnl, "Ilac Panel", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
    }

    void silButtonClicked() {

    }

    void guncelleButtonClicked() {
        int index = this.ilacTable.getSelectedRow();
        if (index < 0) {
            JOptionPane.showMessageDialog(this, "Lutfen bi urun secin");
            return;
        }
        IlacEklemeGuncellemePanel pnl = new IlacEklemeGuncellemePanel(dataModel.getIlacAt(index));
        String[] options = {"Guncelle", "Sil", "Iptal"};
        int opt = JOptionPane.showOptionDialog(this, pnl, "Ilac Panel", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
        if (opt == JOptionPane.YES_OPTION) {
            pnl.updateIlacDate();
            dataModel.fireTableRowsUpdated(index, index);
        }
    }
}
