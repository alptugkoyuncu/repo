/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.ayarlar.uyelik_bilgilerim;

import data.Address;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 *
 * @author alptu
 */
public class AddressBilgileriTabbedPane extends JPanel{
    List<Address> addresList;
    JButton newButton,deleteButton;
    JTabbedPane pane;
    public AddressBilgileriTabbedPane(List<Address> list) {
        newButton=new JButton("Yeni Ekle");
        deleteButton = new JButton("Sil");
        pane = new JTabbedPane();
        
        FlowLayout l =new FlowLayout(FlowLayout.LEFT);
        JPanel pnl = new JPanel(l);
        pnl.add(newButton);
        pnl.add(deleteButton);
        this.setLayout(new BorderLayout());
//        this.add(pnl,BorderLayout.NORTH);
        this.add(pane,BorderLayout.CENTER);
        this.setAddresList(list);
    }
    
    public void setAddresList(List<Address> addresList) {
        this.addresList = addresList;
        if(this.addresList==null)
            addresList =new ArrayList<>();
        for(Address addr:addresList){   
            this.pane.add(new AdresBilgileriPaneli(addr),addr.getBaslik());
        }
        this.pane.add(new AdresBilgileriPaneli(null),"Yeni Ekle");
    }
}
