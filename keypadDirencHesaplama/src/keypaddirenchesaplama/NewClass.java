package keypaddirenchesaplama;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author alptug
 */
public class NewClass {

    double rBias = 10000;
    int size = 16;
    double res[];

    public NewClass() {
        res = new double[size + 1];

    }

    private void calculate() {
        for (int i = size; i > 0; i--) {
            res[i] = rBias * (size - i) / i;
            double rt = 0;
            for (int j = size; j > i; j--) {
                rt += res[j];
            }
            res[i] -= rt;
        }
        System.out.println("value\t\t\t\ttoplam value");
        for (int i = 1; i <= size; i++) {
            double rt=0;
            for(int j=i; j<=size;j++)
                rt+=res[j];
            System.out.println(res[i]+"\t\t\t\t"+rt);
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        //</editor-fold>

        /* Create and display the form */
        NewClass f = new NewClass();
        f.calculate();

    }

}
